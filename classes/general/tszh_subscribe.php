<?

use Bitrix\Main;
use Bitrix\Main\Entity;
use Bitrix\Main\Entity\EventResult;
use Bitrix\Main\Localization\Loc;
use Citrus\Tszh\Subscribe\CTszhSubscribeTable;
use Citrus\Tszh\Subscribe\CTszhUnsubscriptionTable;

Loc::loadMessages(__FILE__);

/**
 * ����� ��� ������ � ����������
 */
class CTszhSubscribe
{
	const SUBSCRIBE_CLASS_PREFIX = "CTszhSubscribe";
	const MIN_TSZH_EDITION = "standard";
	//	const EVENT_GET_SUBSCRIBE_CLASSES = "onGetSubscribeClasses";

	/** @var array ������ ��� ������� �������� */
	private static $arSubscribeClasses;

	/**
	 * ��������� ��� ������ ��������
	 *
	 * @return void
	 */
	private static function loadSubscribeClasses()
	{
		$path = $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszh/classes/general/subscribe/";
		$arList = scandir($path);
		foreach ($arList as $key => $item)
		{
			if (is_file($path.$item))
			{
				require_once($path.$item);
			}
		}
	}

	/**
	 * ���������� ��� ������ ������������ ������ �������� � ������ ��������
	 *
	 * @param string $className ��� ������ ��������
	 *
	 * @return string
	 */
	private static function getExecAgentName($className)
	{
		$result = "execAgent";
		if ($className::TYPE == CTszhBaseSubscribe::TYPE_EXTERNAL)
		{
			$result = "externalExecAgent";
		}

		return $result;
	}

	private static function sanitize($buffer)
	{
		$search = array(
			'/\>[^\S ]+/s',  // strip whitespaces after tags, except space
			'/[^\S ]+\</s',  // strip whitespaces before tags, except space
			'/(\s)+/s'       // shorten multiple whitespace sequences
		);
		$replace = array(
			'>',
			'<',
			'\\1',
		);
		$buffer = preg_replace($search, $replace, $buffer);

		return $buffer;
	}

	/**
	 * ����������� html � pdf ��� �������� ���������
	 * @todo ���������� DOMPDF ����������� �������� ������ ����� � ����, ��������� �������������� ��������� ���� �������� ��������� ��� ����������� ����������� � pdf
	 *
	 * @param string $receiptHtml Html ���������
	 * @param string $fileName ��� ����� � ����������
	 * @param string $site ID �����
	 *
	 * @return bool|string ����� � PDF-����� ��� false � ������ ������
	 */
	private static function makePdf($receiptHtml, $fileName, $site)
	{
		if (!self::isPdfAvailable())
		{
			return false;
		}

		$tempPdfFile = CTempFile::GetFileName($fileName);
		CheckDirPath($tempPdfFile);

		require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/citrus.tszh/dompdf/dompdf_config.inc.php");
		$dompdf = new DOMPDF();
		// dompdf �� �����-�� ������� ����� ��������� ������ ������ �������� ��-�� �����-�� �������� ����� ������
		$_receiptHtml = self::sanitize($receiptHtml);
		if (!ToUpper($site["CHARSET"]) != 'UTF-8')
		{
			$_receiptHtml = str_ireplace("charset=" . $site["CHARSET"], 'charset=utf-8', $_receiptHtml);
			$_receiptHtml = mb_convert_encoding($_receiptHtml, 'HTML-ENTITIES', $site["CHARSET"]);
			// html5 parser dompdf ����������� ������������ utf-8
			// TODO ��������� �� ���������� � mbstring.func_overload = 0
			//$_receiptHtml = $APPLICATION->ConvertCharset($_receiptHtml, SITE_CHARSET, 'utf-8');
		}
		$dompdf->load_html($_receiptHtml);
		$dompdf->render();
		$receptPdf = $dompdf->output();
		file_put_contents($tempPdfFile, $receptPdf);

		//file_put_contents($tempHtmlFile, $arFields["RECEIPT"]);

		return $tempPdfFile;
	}

	/**
	 * ���������� HTML ������� ��������� �������� "�������� ������� 1�-�����"
	 *
	 * @return string
	 */
	public static function getVdgbContactsHtml()
	{
		$arContacts = array(
			Loc::getMessage("CITRUS_TSZH_SUBSCRIBE_VDGB_PHONE_MOSCOW_NAME") => array(
				"value" => "+7 (495) 777-25-43",
				"icon" => array("src" => "vdgb_phone.gif", "width" => "17", "height" => "17"),
			),
			Loc::getMessage("CITRUS_TSZH_SUBSCRIBE_VDGB_PHONE_REGIONS_NAME") => array(
				"value" => "+7 (836) 249-46-89",
				"icon" => array("src" => "vdgb_phone.gif", "width" => "17", "height" => "17"),
			),
			"e-mail" => array(
				"value" => '<a href="mailto:otr@rarus.ru" style="color:#ff6c00;">otr@rarus.ru</a>',
				"icon" => array("src" => "vdgb_email.gif", "width" => "12", "height" => "10"),
			),
			"skype" => array(
				"value" => '<a href="skype:otr_sale" style="color:#ff6c00;">otr_sale</a>',
				"icon" => array("src" => "vdgb_skype.gif", "width" => "19", "height" => "19"),
			),
		);
		foreach ($arContacts as $name => $arContact)
		{
			if (isset($arContact["icon"]["src"]))
			{
				$arContacts[$name]["icon"]["src"] = "/bitrix/templates/citrus_tszh_subscribe/images/" . $arContact["icon"]["src"];
			}
		}

		$result = '<table border="0" style="margin-top:8px; font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">';
		foreach ($arContacts as $name => $arContact)
		{
			$result .= "<tr><td style=\"text-align: center; vertical-align: middle;\"><img src=\"{$arContact["icon"]["src"]}\" width=\"{$arContact["icon"]["width"]}\" height=\"{$arContact["icon"]["height"]}\" alt=\"\"></td><td><b>{$name}:</b> {$arContact["value"]}</td></tr>";
		}
		$result .= "</table>";

		return $result;
	}

	/**
	 * ���������� ��� � ��� ������������������ ���� ��������� �������
	 *
	 * @param array $arTszh ������ ����� ���, ��� �������� ������������ ��������
	 * @param string $eventType �� �����: ��� �������� ��������� �������, �� ������: ��� ������������������ �������
	 * @param string $eventName �� �����: ��� �������� ��������� �������, �� ������: ��� ������������������ �������
	 *
	 * @return void
	 */
	public static function getCustomEventTypeAndName($arTszh, &$eventType, &$eventName = null)
	{
		$eventType = "{$eventType}_{$arTszh["ID"]}";
		$eventName = "{$eventName} (" . Loc::getMessage("CITRUS_TSZH_SUBSCRIBE_FOR") . " {$arTszh["NAME"]})";
	}

	/**
	 * ������� �������� ������ ��������.
	 *
	 * @return string
	 */
	public static function SubscribeAgent()
	{
		global $USER;
		$bTmpUser = false;
		if (!is_object($USER))
		{
			$bTmpUser = true;
			$USER = new CUser();
		}

		if (CTszhFunctionalityController::checkSubscribe(false, $isSubscribeEdition))
		{
			$rsSubscribes = CTszhSubscribeTable::getList(array(
				"filter" => array(
					"ACTIVE" => "Y",
					"!RUNNING" => "Y",
					array(
						"LOGIC" => "OR",
						array("<=NEXT_EXEC" => new \Bitrix\Main\Type\DateTime(convertTimeStamp(false, "FULL"))),
						array("NEXT_EXEC" => false),
					),
				),
				"order" => array("NEXT_EXEC" => "asc", "ID" => "asc"),
			));
			while ($arSubscribe = $rsSubscribes->fetch())
			{
				$className = self::getSubscribeClassName($arSubscribe["CODE"]);
				if (!self::checkSubscribeClass($className))
				{
					continue;
				}

				if ($isSubscribeEdition || in_array($className::TYPE, array(CTszhBaseSubscribe::TYPE_REQUIRED, CTszhBaseSubscribe::TYPE_PRODUCT)))
				{
					try
					{
						CTszhSubscribeTable::update($arSubscribe["ID"], array("RUNNING" => "Y"));
					}
					catch (Exception $e)
					{
						self::reportError("Could not set RUNNING for subscribe [{$arSubscribe["ID"]}]: " . $e->getMessage());
					}

					try
					{
						if (!is_array($arSubscribe["PARAMS"]))
						{
							$arSubscribe["PARAMS"] = array();
						}
						$className::exec($arSubscribe);
					}
					catch (Exception $e)
					{
						self::reportError("Error in {$className}::exec(): " . $e->getMessage());
					}
				}
				else
				{
					/*try
					{
						CTszhSubscribeTable::update($arSubscribe["ID"], array("ACTIVE" => "N"));
					}
					catch (Exception $e)
					{
						self::reportError("Could not deactivate subscribe [{$arSubscribe["ID"]}]: " . $e->getMessage());
					}*/
				}
			}
		}

		if ($bTmpUser)
		{
			unset($USER);
		}

		return __METHOD__ . "();";
	}

	/**
	 * ���������� ����������������� ��� ��������� ������� � ��������
	 *
	 * @param int $tszhId Id ���, ��� �������� ������������ ��� �������
	 * @param string $baseEventType ��� �������� ��������� �������, �� �������� �������������� ������������ ��� �������
	 *
	 * @return array
	 */
	public static function getCustomEvent($tszhId, $baseEventType)
	{
		$eventType = $baseEventType;
		self::getCustomEventTypeAndName(array("ID" => $tszhId), $eventType);

		return CEventType::GetListEx(
			array(),
			array("TYPE_ID" => $eventType),
			array("type" => "full")
		)->fetch();
	}

	/**
	 * ������ ����������������� ��� ��������� ������� � ��� ������
	 *
	 * @param array $arTszh ������ ����� ���, ��� �������� �������� ��� �������
	 * @param string $baseEventType ��� �������� ��������� �������, ������������� �� �������� �������� ��� �������
	 *
	 * @return array
	 * @throws \Exception
	 */
	public static function addCustomEvent($arTszh, $baseEventType)
	{
		global $APPLICATION;

		$arSite = CSite::getById($arTszh["SITE_ID"])->fetch();
		if (!in_array($arSite["LANGUAGE_ID"], array("ru", "ua")))
		{
			throw new \Exception(Loc::getMessage("CITRUS_TSZH_SUBSCRIBE_addCustomEvent_ERROR_UNSUPPORTED_LANGUAGE", array("#LANG#" => $arSite["LANGUAGE_ID"])));
		}

		// ����������� �������� ��������� ��� ������� �����
		IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"] . '/bitrix/modules/citrus.tszh/install/events/set_events.php', $arSite["LANGUAGE_ID"]);
		$arEventTypes = citrus_tszh::__GetEventTypes();

		$arEventType = $arEventTypes[$baseEventType];
		if (!is_array($arEventType) || empty($arEventType))
		{
			throw new \Exception(Loc::getMessage("CITRUS_TSZH_SUBSCRIBE_addCustomEvent_ERROR_UNKNOWN_EVENT", array("#EVENT_TYPE#" => $baseEventType)));
		}

		$eventType = $baseEventType;
		//$eventName = Loc::getMessage($baseEventType . "_TITLE");
		$eventName = getMessage($baseEventType . "_TITLE");
		self::getCustomEventTypeAndName($arTszh, $eventType, $eventName);

		// �������� ��� ��������� �������
		$obEventType = new CEventType();
		$arEventTypeFields = Array(
			"LID" => $arSite["LANGUAGE_ID"],
			"EVENT_NAME" => $eventType,
			"NAME" => $eventName,
			//"DESCRIPTION" => Loc::getMessage($baseEventType . "_TEXT"),
			"DESCRIPTION" => getMessage($baseEventType . "_TEXT"),
		);
		// ������ ������������ ��� ��������� �������
		$arOldEventType = CEventType::GetList(array("EVENT_NAME" => $eventType))->fetch();
		if (is_array($arOldEventType))
		{
			$obEventType->delete($eventType);
		}

		$bSuccess = $obEventType->Add($arEventTypeFields) > 0;
		if (!$bSuccess)
		{
			throw new Exception(Loc::getMessage("CITRUS_TSZH_SUBSCRIBE_addCustomEvent_ERROR_ADD_EVENT", array(
				"#EVENT_TYPE#" => $eventType,
				"#ERROR#" => ""
				/*$obEventType->LAST_ERROR*/
			)));
		}

		// �������� �������� �������
		$obEventMessage = new CEventMessage();
		foreach ($arEventType as $arTemplate)
		{
			$arTemplate["EVENT_NAME"] = $eventType;
			$arTemplate["LID"] = $arSite["ID"];

			if (!array_key_exists("EMAIL_FROM", $arTemplate))
			{
				$arTemplate["EMAIL_FROM"] = "#DEFAULT_EMAIL_FROM#";
			}
			if (!array_key_exists("EMAIL_TO", $arTemplate))
			{
				$arTemplate["EMAIL_TO"] = "#DEFAULT_EMAIL_FROM#";
			}
			if (!array_key_exists("BODY_TYPE", $arTemplate))
			{
				$arTemplate["BODY_TYPE"] = "text";
			}
			$arTemplate["ACTIVE"] = "Y";

			$bSuccess = $obEventMessage->Add($arTemplate) > 0;

			if (!$bSuccess)
			{
				throw new Exception(Loc::getMessage("CITRUS_TSZH_SUBSCRIBE_addCustomEvent_ERROR_ADD_MESSAGE", array(
					"#EVENT_TYPE#" => $eventType,
					"#ERROR#" => $obEventMessage->LAST_ERROR,
				)));
			}
		}
	}

	/**
	 * ��������� �������� ������� (�������� CEvent::sendImmediate(), ������� �������� � ������; ��� �������� ������� ������������ ������ ($arFiles) �������� CEvent::send() (��� ��������� ��������): ���� ��� � CEvent::sendImmediate() �� ����������� �������� ��������).
	 * ��� ��������� Id ��� ���� ����������������� �������� ������� ��� ������� ��� (������� ���������� ������) � ��������� ���.
	 * ���������� ������� �� ����������������� �������� ������� � �������� ����, ���� �� �������� ���������� (������� ���������� ������� ��������).
	 * ��� ������ ��������� � ���.
	 *
	 * @param string $eventType ��� ��������� �������
	 * @param string $siteId Id ����� ��������� �������
	 * @param array $arFields ������ ����� �������
	 * @param int|bool $tszhId Id ��� ��� ������ ���������� ��������� �������
	 * @param array $arFiles ������ (���������� ����� �� ������������ ������) ��������; ������������ ������ ���
	 *
	 * @return int|string|false Id ��������� ������ � b_event (��� �������� ��������� � PDF-��������� ����� CEvent::send()),
	 *          ��������� (string) CEvent::sendImmediate(),
	 *          false � ������ ������
	 */
	public static function send($eventType, $siteId, $arFields, $tszhId = false, $arFiles = array())
	{
		static $arCustomEventCache = array();
		static $arEventMessagesCache = array();

		$tszhId = intval($tszhId);
		if ($tszhId > 0)
		{
			$customEventType = $eventType;
			self::getCustomEventTypeAndName(array("ID" => $tszhId), $customEventType);

			if (!is_set($arCustomEventCache, $customEventType))
			{
				$rsEventTypes = CEventType::GetList(array("EVENT_NAME" => $customEventType));
				if ($arType = $rsEventTypes->fetch())
				{
					$arCustomEventCache[$customEventType] = true;
				}
				else
				{
					$arCustomEventCache[$customEventType] = false;
				}
			}
			if ($arCustomEventCache[$customEventType])
			{
				$eventType = $customEventType;
			}
		}

		// �������� ������ �� ������������������ ��������� ������� � �������� ����, ���� ��� ������� �������� ����������
		if (!is_set($arCustomEventCache, $eventType))
		{
			$arEventTypes = citrus_tszh::__GetEventTypes();
			$arOrigTemplates = $arEventTypes[$eventType];
			if (is_array($arOrigTemplates))
			{
				$theme = false;
				foreach ($arOrigTemplates as $arOrigTemplate)
				{
					if (is_set($arOrigTemplate, "SITE_TEMPLATE_ID") && strlen($arOrigTemplate["SITE_TEMPLATE_ID"]))
					{
						$theme = $arOrigTemplate["SITE_TEMPLATE_ID"];
						break;
					}
				}

				if (strlen($theme))
				{
					if (!is_set($arEventMessagesCache, $eventType))
					{
						$rsTemplates = CEventMessage::GetList($by, $order, array("TYPE_ID" => $eventType));
						$arEventMessagesCache[$eventType] = array();
						while ($arTemplate = $rsTemplates->fetch())
						{
							$arEventMessagesCache[$eventType][$arTemplate["ID"]] = $arTemplate;
						}
						unset($rsTemplates);
					}
					$arTemplates = $arEventMessagesCache[$eventType];
					if (is_array($arTemplates) && !empty($arTemplates))
					{
						$oEventMessage = new CEventMessage();
						foreach ($arTemplates as $templateId => $arTemplate)
						{
							if ($arTemplate["SITE_TEMPLATE_ID"] != $theme)
							{
								$res = $oEventMessage->update($templateId, array("SITE_TEMPLATE_ID" => $theme));
								if (!$res)
								{
									self::reportError("Error: could not set SITE_TEMPLATE_ID for template [{$templateId}] of event type '{$eventType}': " . $oEventMessage->LAST_ERROR);
								}
							}
						}
					}
				}
			}
		}

		if (empty($arFiles))
		{
			$oldAttachImages = \Bitrix\Main\Config\Option::get("main", "attach_images", "N");
			if ($oldAttachImages != "Y")
			{
				\Bitrix\Main\Config\Option::set("main", "attach_images", "Y");
			}
			$res = CEvent::sendImmediate($eventType, $siteId, $arFields, "Y", "", $arFiles);
			if ($oldAttachImages != "Y")
			{
				\Bitrix\Main\Config\Option::set("main", "attach_images", $oldAttachImages);
			}

			if ($res != \Bitrix\Main\Mail\Event::SEND_RESULT_SUCCESS)
			{
				self::reportError("CEvent::sendImmediate() returned '{$res}' for type {$eventType}");
			}
		}
		else
		{
			$res = CEvent::send($eventType, $siteId, $arFields, "Y", "", $arFiles);
			if (!$res)
			{
				self::reportError("CEvent::Send() failed for type {$eventType}");
			}
		}

		return $res;
	}

	/**
	 * �������� �������� ���������
	 *
	 * @param array $arFilter - ������, �������� ������ ��������� ��� ��������
	 * @param bool $notSentOnly - ����: ��������� ������ �� ������������ ���������
	 * @param string|bool $templatePath - �������������� ��������; ���� � ������� ��������� (������� ���������� citrus:tszh.receipt) �� ����� ����� (��������, "/bitrix/components/citrus/tszh.receipt/templates/post-354")
	 * @param int|bool $maxEmailsCount - max ���-�� ���������, ������� ����� ���������
	 *
	 * @return int - ID ��������� ������������ ���������
	 */
	public static function sendReceipts($arFilter, $notSentOnly = false, $templatePath = false, $maxEmailsCount = false)
	{
		global $APPLICATION;

		@set_time_limit(0);

		$arFilter["!USER_EMAIL"] = false;
		if ($notSentOnly)
		{
			$arFilter["IS_SENT"] = "N";
		}

		$tszhFilter = array();
		if (isset($arFilter["PERIOD_TSZH_ID"]))
		{
			$tszhFilter["ID"] = $arFilter["PERIOD_TSZH_ID"];
		}
		$rsTszh = CTszh::GetList(false, $tszhFilter);
		$arTszhs = array();
		$arSiteIDs = array();
		while ($arTszh = $rsTszh->Fetch())
		{
			$arTszhs[$arTszh["ID"]] = $arTszh;
			$arSiteIDs[] = $arTszh["SITE_ID"];
		}

		$rsSites = CSite::GetList($by = "sort", $order = "desc", array("ID" => array_unique($arSiteIDs)));
		$arSites = array();
		while ($arSite = $rsSites->Fetch())
		{
			$arSites[$arSite["LID"]] = $arSite;
		}

		$arTszhTemplateDirs = array();
		foreach ($arTszhs as $tszhID => $arTszh)
		{
			$siteID = $arTszh["SITE_ID"];
			$arTszhTemplateDirs[$tszhID] = CTszhReceiptTemplateProcessor::getComponentTemplateDir(isset($arSites[$siteID]) ? $arSites[$siteID] : false, $templatePath);
		}

		$rsSubscribes = CTszhSubscribeTable::getList(array(
			"filter" => array("CODE" => CTszhSubscribeReceipts::CODE, "TSZH_ID" => array_keys($arTszhs)),
		));
		$arSubscribes = array();
		while ($arSubscribe = $rsSubscribes->fetch())
		{
			if (!is_array($arSubscribe["PARAMS"]))
			{
				$arSubscribe["PARAMS"] = array();
			}
			CTszhSubscribeReceipts::checkParams($arSubscribe["PARAMS"], $arSubscribe["ID"], true);

			$arSubscribes[$arSubscribe["TSZH_ID"]] = $arSubscribe;
		}

		// ���-�� ������������ ����� ������� �������
		$curEmailCount = 0;

		$rsAccountPeriods = CTszhAccountPeriod::GetList(
			array("TYPE" => "ASC"),
			$arFilter,
			array(
				"ID",
				"ACCOUNT_ID",
				"ACCOUNT_NAME",
				"PERIOD_ID",
				"PERIOD_DATE",
				"PERIOD_TSZH_ID",
				"USER_ID",
				"USER_EMAIL",
				"USER_SITE",
			),
			//$maxEmailsCount ? array('nTopCount' => $maxEmailsCount*10) : false,
			false,
			array("*")
		);

		// cssin library 
		require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszh/cssin/src/CSSIN.php");
		$cssin = new \FM\CSSIN();

		while ($arAccountPeriod = $rsAccountPeriods->GetNext())
		{
			if (!self::isDummyMail($arAccountPeriod["USER_EMAIL"], $arAccountPeriod["USER_SITE"]))
			{
				$filter = array(
					"ID" => $arAccountPeriod["ID"],
					"ACCOUNT_ID" => $arAccountPeriod["ACCOUNT_ID"],
					"PERIOD_ID" => $arAccountPeriod["PERIOD_ID"],
				);
				$tszhID = $arAccountPeriod["PERIOD_TSZH_ID"];
				$arTszh = $arTszhs[$tszhID];
				$templateDir = isset($arTszhTemplateDirs[$tszhID]) ? $arTszhTemplateDirs[$tszhID] : "";
				$arTemplatePath = explode('/', $templateDir);
				if (strlen($arAccountPeriod["USER_EMAIL"]) > 0 && is_array($arTemplatePath))
				{
					ob_start();
					?><? list($realTemplateDir, $receiptHtml) = $APPLICATION->IncludeComponent(
					"citrus:tszh.receipt",
					array_pop($arTemplatePath),
					Array(
						"CACHE_TYPE" => "A",
						"CACHE_TIME" => "3600",
						"INLINE" => "N",
						"DISABLE_TIME_ZONE" => "Y",
						"ACCOUNT_PERIOD_FILTER" => $filter,
						"CUSTOM_TEMPLATE_PATH" => CTszhReceiptTemplateProcessor::AUTO_DETECT,
					)
				); ?><?
					//					$receiptHtml = ob_get_contents();
					//					ob_end_clean();

					if (defined("BX_UTF") && BX_UTF === true)
					{
						$receiptHtml = $APPLICATION->ConvertCharset($receiptHtml, "UTF-8", "windows-1251");
					}
					//$receiptHtml = $cssin->inlineCSS(($APPLICATION->isHttps() ? "https://" : "http://") . $_SERVER["HTTP_HOST"] . $realTemplateDir, $_receiptHtml);
					//$receiptHtml = $cssin->inlineCSS(($APPLICATION->isHttps() ? "https://" : "http://") . $_SERVER["HTTP_HOST"] . $realTemplateDir, $receiptHtml); //���� �� �������� ��� ��� ---> $receiptHtml = $cssin->inlineCSS(null, $receiptHtml);
					$receiptHtml = $cssin->inlineCSS(null, $receiptHtml);
					if (defined("BX_UTF") && BX_UTF === true)
					{
						$receiptHtml = $APPLICATION->ConvertCharset($receiptHtml, "windows-1251", "UTF-8");
					}

					$siteID = isset($arTszhSites[$tszhID]) ? $arTszhSites[$tszhID] : CSite::GetDefSite();

					$pdfFileName = false;
					$isPdf = false;
					if ($arSubscribes[$tszhID]["PARAMS"]["SEND_PDF"] == "Y")
					{
						$res = self::makePdf($receiptHtml, "receipt-" . substr($arAccountPeriod["PERIOD_DATE"], 0, 7) . ".pdf", $arSites[$siteID]);
						if (strlen($res))
						{
							$pdfFileName = $res;
							$isPdf = true;
						}
					}

					// ������� ��������� From, Sender � URL ������� �� ��������
					$from = $sender = $listUnsubscribe = true;
					self::getMailHeaders($arTszh["SITE_ID"], $from, $sender, $listUnsubscribe, $arAccountPeriod["USER_ID"], CTszhSubscribeReceipts::CODE);

					// ������� URL �������� ��������� ������� ��������
					$personalUrl = $paymentUrl = $metersUrl = false;
					$receiptUrl = true;
					if (!is_null($arTszh))
					{
						CTszhPublicHelper::getPersonalUrls($arTszh, $personalUrl, $paymentUrl, $metersUrl, $receiptUrl);
					}

					$arSendFields = array(
						"EMAIL_FROM" => $from,
						"HEADER_SENDER" => $sender,
						"EMAIL_TO" => $arAccountPeriod["USER_EMAIL"],
						"RECEIPT" => $isPdf ? Loc::getMessage("CITRUS_TSZH_SUBSCRIBE_ATTACHED_RECEIPT") : $receiptHtml,
						"FIO" => $arAccountPeriod["ACCOUNT_NAME"],
						"ORG_NAME" => $arTszh["NAME"],
						"~ORG_NAME" => $arTszh["NAME"],
						"RECEIPT_URL" => $receiptUrl,
						"UNSUBSCRIBE_URL" => $listUnsubscribe,
					);

					// �������� ��� �������
					if (CTszhFunctionalityController::isPortal())
					{
						// ��� ������ �������
						$entity_type = COption::GetOptionString('vdgb.portaltszh', 'entity_type');
						if ($entity_type == 'house')
						{
							// ������ ������� ����
							$arAccount = CTszhAccount::GetByID($arAccountPeriod["ACCOUNT_ID"]);
							// ������ ���������� �� ����
							$arHouse = \Citrus\Tszh\HouseTable::getById($arAccount['HOUSE_ID'])->fetch();
							// ���������� �� ����� � �������� �������� ���
							$arHouseSite = CSite::GetByID($arHouse['SITE_ID'])->Fetch();
							// ���������� �� ����� � �������� �������� ������������
							$arTszhSite = CSite::GetByID($arTszh['SITE_ID'])->Fetch();
							// ���� ���������� ����� ����� ���� �� ������ ����� ������������, �� ������� � ������ ������ ������
							if ($arHouseSite['SERVER_NAME'] != $arTszhSite['SERVER_NAME'])
							{
								//  � ���������
								$arSendFields['RECEIPT'] = str_replace($arTszhSite['SERVER_NAME'], $arHouseSite['SERVER_NAME'], $arSendFields['RECEIPT']);
								// � ������ �������
								$arSendFields['UNSUBSCRIBE_URL'] = str_replace($arTszhSite['SERVER_NAME'], $arHouseSite['SERVER_NAME'], $arSendFields['UNSUBSCRIBE_URL']);
								// � ������ �� ���������
								$arSendFields['RECEIPT_URL'] = str_replace($arTszhSite['SERVER_NAME'], $arHouseSite['SERVER_NAME'], $arSendFields['RECEIPT_URL']);
							}
						}
					}

					$res = self::send("TSZH_RECEIPT", $siteID, $arSendFields, $arTszh["ID"], $isPdf ? array($pdfFileName) : array());
					if ($res === \Bitrix\Main\Mail\Event::SEND_RESULT_SUCCESS || intval($res) > 0)
					{
						$dbReceipts = CTszhAccountPeriod::GetList(array(), $filter, false, false, array("ID"));
						while ($receipt = $dbReceipts->Fetch())
						{
							CTszhAccountPeriod::Update($receipt["ID"], array("DATE_SENT" => convertTimeStamp(false, "FULL", $siteID)));
						}
					}

					$curEmailCount++;
					CTszhBaseSubscribe::incGenericEmailCount();
				}
			}

			if ($maxEmailsCount !== false && $curEmailCount >= $maxEmailsCount)
			{
				break;
			}
		}

		return $arAccountPeriod["ID"];
	}

	/**
	 * ���������� true, ���� $mail �������� "���������" e-mail'��, ������������� ��� ������� �� 1�
	 *
	 * @param string $mail
	 * @param string|bool $siteId ID �����
	 *
	 * @return bool
	 */
	public static function isDummyMail($mail, $siteId = false)
	{
		$mail = trim($mail);
		if (empty($mail) || $mail === 'mail@test.ru')
		{
			return true;
		}

		static $arSiteServerNames = array();
		if (!@array_key_exists($siteId, $arSiteServerNames))
		{
			$arFilter = array();
			if (false !== $siteId)
			{
				$arFilter["ID"] = $siteId;
			}
			$rsSites = CSite::GetList($by = "sort", $order = "desc", $arFilter);
			$arServerNames = array();
			while ($arSite = $rsSites->fetch())
			{
				$serverName = $arSite["SERVER_NAME"] ? $arSite["SERVER_NAME"] : (COption::GetOptionString('main', 'server_name', $_SERVER['SERVER_NAME']));
				$serverName = '@' . CUtil::translit($serverName, 'ru', Array("replace_space" => '-', "replace_other" => '.', "safe_chars" => "-"));

				$arServerNames[] = $serverName;
				if (preg_match('#^@www\.#i', $serverName))
				{
					$arServerNames[] = preg_replace('#^@www\.#i', '@', $serverName);
				}
				else
				{
					$arServerNames[] = preg_replace('#^@#i', '@www.', $serverName);
				}

				$arSiteServerNames[$arSite["ID"]] = $arServerNames;
			}

			$arSiteServerNames[$siteId] = $arServerNames;
		}
		$arServerNames = $arSiteServerNames[$siteId];
		$arServerNames[] = "@unknown.domain";
		$arServerNames = array_unique($arServerNames);

		foreach ($arServerNames as $serverName)
		{
			if (preg_match('#' . preg_quote($serverName) . '$#i', $mail))
			{
				return true;
			}
		}

		return false;
	}

	/**
	 * ���������� true, ���� �������� �������� ��������� � PDF (��������� ����������� ������ GD � DOM)
	 * @return bool
	 */
	public static function isPdfAvailable()
	{
		global $APPLICATION;
		$APPLICATION->ResetException();

		$errors = array();

		if (!extension_loaded('dom'))
		{
			$errors[] = Loc::getMessage("CITRUS_TSZH_SUBSCRIBE_PDF_DOM_NOT_LOADED");
		}
		if (!extension_loaded('gd'))
		{
			$errors[] = Loc::getMessage("CITRUS_TSZH_SUBSCRIBE_PDF_GD_NOT_LOADED");
		}

		if (!empty($errors))
		{
			$APPLICATION->ThrowException(implode("\n", $errors));
		}

		return empty($errors);
	}

	public static function OnBeforeEventAdd(&$event, &$lid, &$arFields)
	{
		if ($event == "TSZH_FEEDBACK_FORM" && isset($_SESSION["citrus:tszh.contacts"]["HEADERS"])
		    && is_array($_SESSION["citrus:tszh.contacts"]["HEADERS"]) && !empty($_SESSION["citrus:tszh.contacts"]["HEADERS"])
		)
		{
			$arFields["EMAIL_FROM"] = $_SESSION["citrus:tszh.contacts"]["HEADERS"]["FROM"];

			$arFields["HEADER_SENDER"] = "";
			if (strlen($_SESSION["citrus:tszh.contacts"]["HEADERS"]["SENDER"]))
			{
				$arFields["HEADER_SENDER"] = $_SESSION["citrus:tszh.contacts"]["HEADERS"]["SENDER"];
			}

			$arFields["EMAIL_TO"] = $_SESSION["citrus:tszh.contacts"]["HEADERS"]["TO"];
		}
	}

	/**
	 * ���������� �������� ��������� ������ citrus.tszh, ����������� � ���������� ��������
	 *
	 * @param string $option ���������� ��� ���������
	 *
	 * @return int|bool false � ������ ������
	 */
	public static function getOption($option)
	{
		switch ($option)
		{
			case "max_emails_per_hit":
				$default = 50;
				break;

			case "posting_interval":
				$default = 5;
				break;

			default:
				return false;
		}

		$result = COption::GetOptionInt(TSZH_MODULE_ID, "subscribe_{$option}", $default);
		if ($result <= 0)
		{
			$result = $default;
		}

		return $result;
	}

	/**
	 * ��������� ��������� �� ������
	 *
	 * @param string $errMsg ��������� �� ������
	 *
	 * @return string
	 */
	public static function reportError($errMsg)
	{
		addMessage2Log($errMsg, TSZH_MODULE_ID);
		sendError($errMsg, TSZH_MODULE_ID);
	}

	/**
	 * ���������� ��� ������ �������� �� � ����������� ����
	 *
	 * @param string $code ���������� ��� ��������
	 *
	 * @return string
	 */
	public static function getSubscribeClassName($code)
	{
		return self::SUBSCRIBE_CLASS_PREFIX . $code;
	}

	/**
	 * ���������, �������� �� ����� ������� ��������
	 *
	 * @param string $className ��� ������
	 *
	 * @return bool
	 */
	public static function checkSubscribeClass($className)
	{
		self::loadSubscribeClasses();
		if (!preg_match('/^' . self::SUBSCRIBE_CLASS_PREFIX . '.+/', $className)
		    || !class_exists($className)
		    || !is_subclass_of($className, "CTszhBaseSubscribe")
		)
		{
			return false;
		}

		return true;
	}

	/**
	 * ��������� � ���������� ������ ��� ������� ��������
	 *
	 * @param int $typeMask ������� ����� ��������� ����� ��������
	 *
	 * @return array
	 */
	public static function getSubscribeClasses($typeMask = CTszhBaseSubscribe::TYPE_ALL)
	{
		if (!isset(self::$arSubscribeClasses))
		{
			self::$arSubscribeClasses = array();

			self::loadSubscribeClasses();
			/*$rsHandlers = GetModuleEvents(TSZH_MODULE_ID, self::EVENT_GET_SUBSCRIBE_CLASSES);
			while ($arHandler = $rsHandlers->fetch())
			{
				$code = ExecuteModuleEventEx($arHandler, array());
				$className = self::getSubscribeClassName($code);
				if (self::checkSubscribeClass($className))
					self::$arSubscribeClasses[] = $className;
			}*/
			$arClasses = array();
			foreach (get_declared_classes() as $className)
			{
				if (self::checkSubscribeClass($className))
				{
					$arClasses[$className] = $className::SORT;
				}
			}
			asort($arClasses);

			self::$arSubscribeClasses = array_keys($arClasses);
		}

		$arResult = array();
		foreach (self::$arSubscribeClasses as $className)
		{
			if ($typeMask & $className::TYPE)
			{
				$arResult[] = $className;
			}
		}

		return $arResult;
	}

	/**
	 * ���������� ��� ��� URL ������� ������������ �� ��������
	 *
	 * @param int $userId Id ������������
	 * @param string $subscribeCode ���������� ��� ��������
	 *
	 * @return string
	 */
	public static function calcUnsubscribeHash($userId, $subscribeCode)
	{
		return md5(md5($userId) . "|" . $subscribeCode);
	}

	/**
	 * ���������� � ������������ �� ������ ���������� �������� �������� ���������� From, Sender � List-Unsubscribe ��� ������, ������������� ���������� ������ � ����� � �������� Id
	 *
	 * ������ ����������� �������� ���������� ���������:
	 * From:
	 * �������� ����� ����� ��:
	 *     1. ��������� "E-Mail ����� �� ���������" ����� (���� ���� email �����)
	 *   2. ��������� "E-Mail �������������� ����� (����������� �� ���������)" �������� ������ (���� email ����� �� �����)
	 *   3. ���� ����� ����������� email'� �� ��������� � ������� �������� ����� (�� $_SERVER["HTTP_HOST"]), �� ������ �������������� email-����� no-reply@�����_��������_�����.
	 *
	 * Sender:
	 * �������� ����� ����� ��:
	 *     1. ��������� "E-Mail ����� �� ���������" ����� (���� ���� email �����)
	 *   2. ��������� "E-Mail �������������� ����� (����������� �� ���������)" �������� ������ (���� email ����� �� �����)
	 *   3. ���� ���������� email ��������� � From, �� ������ ������ ������.
	 *
	 * List-Unsubscribe:
	 * ������������ URL ����� � �����������, ������������ ��� ������� �� ��������
	 *
	 * @param array $siteId Id �����
	 * @param bool|string $from �� �����: ���� (bool) ������������� �������� ��������� From. �� ������: ���� true, �� ���������� �������� ��������� From
	 * @param bool|string $sender �� �����: ���� (bool) ������������� �������� ��������� Sender. �� ������: ���� true, �� ���������� �������� ��������� Sender
	 * @param bool|string $listUnsubscribe �� �����: ���� (bool) ������������� �������� ��������� List-Unsubscribe. �� ������: ���� true, �� ���������� �������� ��������� List-Unsubscribe
	 * @param int|bool $userId Id ������������ ��� List-Unsubscribe
	 * @param string|bool $subscribeCode ���������� ��� �������� ��� List-Unsubscribe
	 *
	 * @return void
	 */
	public static function getMailHeaders($siteId, &$from = false, &$sender = false, &$listUnsubscribe = false, $userId = false, $subscribeCode = false)
	{
		if ($from || $sender)
		{
			$arSite = CTszhPublicHelper::getSite($siteId);
			$defaultEmailFrom = COption::GetOptionString("main", "email_from");
		}

		if ($from)
		{
			if (strlen($arSite["EMAIL"]))
			{
				$from = $arSite["EMAIL"];
			}
			else
			{
				$from = $defaultEmailFrom;
			}
			$arFrom = explode("@", $from);
			$url = "http://" . $_SERVER["HTTP_HOST"];
			$arUrl = parse_url($url);
			if ($arUrl["host"] != $arFrom[1])
			{
				$from = "no-reply@" . $arUrl["host"];
			}
		}

		if ($sender)
		{
			if (strlen($arSite["EMAIL"]))
			{
				$sender = $arSite["EMAIL"];
			}
			else
			{
				$sender = $defaultEmailFrom;
			}
			if ($sender == $from)
			{
				$sender = "";
			}
		}

		if ($listUnsubscribe)
		{
			$siteUrl = CTszhPublicHelper::getSiteUrl($siteId);
			$hash = self::calcUnsubscribeHash($userId, $subscribeCode);
			$listUnsubscribe = $siteUrl . "?tszh_unsubscribe=y&user_id={$userId}&subscribe={$subscribeCode}&hash={$hash}";
		}
	}

	/**
	 * ���������� ������ ������ ������������ ������ execAgent() ��������
	 *
	 * @param string $className ��� ������ ��������
	 * @param array $arParams ������ ���������� ������� ������
	 *
	 * @return string
	 */
	public static function getExecAgentString($className, array $arParams)
	{
		$methodName = self::getExecAgentName($className);

		$arAgentParams = array();
		foreach ($arParams as $param)
		{
			if (is_array($param))
			{
				$param = str_replace(PHP_EOL, "", var_export($param, true));
			}

			$arAgentParams[] = $param;
		}

		return "{$className}::{$methodName}(" . implode(", ", $arAgentParams) . ");";
	}

	/**
	 * ��������� ����������� ����� �������� � ������� ��������
	 *
	 * @param string $className ��� ������ ��������
	 * @param array $arParams ������ ���������� ������� ������
	 *
	 * @return int|bool
	 */
	public static function addExecAgent($className, array $arParams)
	{
		$agentString = self::getExecAgentString($className, $arParams);

		$agentID = CAgent::AddAgent($agentString, TSZH_MODULE_ID, "N", self::getOption("posting_interval"));
		if ($agentID <= 0)
		{
			$errMsg = "ERROR: could not add agent {$agentString}";
			self::reportError($errMsg);
		}

		return $agentID;
	}

	/**
	 * ������� ����������� ������ �������� � ������� ��������
	 *
	 * @param string $className ��� ������ ��������
	 * @param int $subscribeIdOrTszhId Id �������� ��� Id ��� ��� ������� ��������
	 *
	 * @return void
	 */
	public static function deleteExecAgent($className, $subscribeIdOrTszhId)
	{
		$subscribeIdOrTszhId = intval($subscribeIdOrTszhId);
		$methodName = self::getExecAgentName($className);

		$rsAgents = CAgent::GetList(array(), array("MODULE_ID" => TSZH_MODULE_ID, "NAME" => "{$className}::{$methodName}({$subscribeIdOrTszhId},%"));
		while ($arAgent = $rsAgents->fetch())
		{
			CAgent::delete($arAgent["ID"]);
		}
	}

	/**
	 * ���������� ������ email'�� ���������������, �� ���������� �� �������� ��������
	 *
	 * @param string $subscribeCode ���������� ��� ��������
	 *
	 * @return array
	 */
	public static function getSubscribedAdminEmails($subscribeCode)
	{
		$arResult = array();

		$arUnsubscribedUserIds = self::getUnsubscribedUsers($subscribeCode);
		$rsAdmins = CUser::getList(
			$by = "id",
			$order = "asc",
			array("GROUPS_ID" => array(1), "ACTIVE" => "Y")
		);
		while ($arAdmin = $rsAdmins->fetch())
		{
			if (!in_array($arAdmin["ID"], $arUnsubscribedUserIds) && check_email($arAdmin["EMAIL"], true))
			{
				$arResult[$arAdmin["ID"]] = $arAdmin["EMAIL"];
			}
		}

		return $arResult;
	}

	/**
	 * ������������� �������� �������� (TYPE == CTszhBaseSubscribe::TYPE_PRODUCT)
	 *
	 * @return void
	 */
	public static function installProductSubscribes()
	{
		$arSubscribeClasses = array();
		foreach (self::getSubscribeClasses(CTszhBaseSubscribe::TYPE_PRODUCT) as $className)
		{
			$arSubscribeClasses[$className::CODE] = $className;
		}
		if (empty($arSubscribeClasses))
		{
			return;
		}

		$rsExistingSubscribes = CTszhSubscribeTable::getList(array(
			"select" => array("ID", "CODE"),
			"filter" => array("CODE" => array_keys($arSubscribeClasses)),
		));
		$arExistingSubscribeCodes = array();
		while ($arExistingSubscribe = $rsExistingSubscribes->fetch())
		{
			$arExistingSubscribeCodes[] = $arExistingSubscribe["CODE"];
		}
		$arExistingSubscribeCodes = array_unique($arExistingSubscribeCodes);

		foreach ($arSubscribeClasses as $code => $className)
		{
			if (in_array($code, $arExistingSubscribeCodes))
			{
				continue;
			}

			if ((new $className) instanceof ITszhSubscribeConditionalInstall)
			{
				if (!$className::isNeedInstall())
				{
					continue;
				}
			}

			try
			{
				$arFields = array(
					"CODE" => $code,
					"ACTIVE" => "Y",
					"RUNNING" => "N",
				);
				CTszhSubscribeTable::add($arFields);
			}
			catch (Exception $e)
			{
				self::reportError("Could not add product subscribe {$code}.");
			}
		}
	}

	/**
	 * ���������� ������ �������� ��� ��������� ��� �� ����� �� ����������� ��� �������������� �������
	 *
	 * @param int $tszhId Id ���
	 * @param CWizardBase $wizard ������ ������� ��������� �����
	 *
	 * @return array
	 */
	public static function getSubscribes4Admin($tszhId, CWizardBase $wizard = null)
	{
		$arResult = array();

		$arSubscribeClasses = self::getSubscribeClasses(CTszhBaseSubscribe::TYPE_OPTIONAL);
		$arSubscribeCodes = array();
		foreach ($arSubscribeClasses as $className)
		{
			$arSubscribeCodes[] = $className::CODE;
		}

		if ($wizard !== null)
		{
			$tszhId = false;
			$wizardSiteId = $wizard->GetVar("siteID");
			if (strlen($wizardSiteId))
			{
				$arTszh = CTszh::GetList(array(), array("SITE_ID" => $wizardSiteId), false, false, array("ID"))->fetch();
				if (is_array($arTszh) && !empty($arTszh))
				{
					$tszhId = $arTszh["ID"];
				}
			}
		}

		$tszhId = intval($tszhId);
		$arSubscribes = array();
		if ($tszhId > 0)
		{
			$rsSubscribes = CTszhSubscribeTable::getList(array(
				"filter" => array("TSZH_ID" => $tszhId, "CODE" => $arSubscribeCodes),
			));
			while ($arSubscribe = $rsSubscribes->fetch())
			{
				$arSubscribes[$arSubscribe["CODE"]] = $arSubscribe;
			}
		}

		foreach ($arSubscribeClasses as $className)
		{
			$arOldSettings = $className::getSettings($tszhId, false, true, $arSubscribes[$className::CODE]);
			$arNewSettings = $className::collectSettings(false, $wizard);
			foreach ($arOldSettings as $code => $arSetting)
			{
				if ($code == "PARAMS")
				{
					foreach ($arOldSettings["PARAMS"] as $paramCode => $arParam)
					{
						if (is_set($arNewSettings["PARAMS"], $paramCode))
						{
							$arOldSettings["PARAMS"][$paramCode]["value"] = $arNewSettings["PARAMS"][$paramCode];
						}
					}
				}
				else
				{
					if (is_set($arNewSettings, $code))
					{
						$arOldSettings[$code]["value"] = $arNewSettings[$code];
					}
				}
			}

			$arResultItem = array(
				"ID" => $arSubscribes[$className::CODE]["ID"],
				"NAME" => $className::getMessage("NAME"),
				"NOTE" => $className::getMessage("NOTE-ADMIN"),
				"SETTINGS" => $arOldSettings,
				"SETTINGS_COLLECTED" => $arNewSettings,
				"CUSTOM_EVENT" => array(),
			);

			if ($wizard === null && $tszhId > 0)
			{
				$arResultItem["CUSTOM_EVENT"] = self::getCustomEvent($tszhId, $className::EVENT_TYPE);
			}

			$arResult[$className] = $arResultItem;
		}

		return $arResult;
	}

	/**
	 * ���������� ������ ������������ �� �������� �������������
	 *
	 * @param int $subscribeCode ���������� ��� ��������
	 *
	 * @return array
	 * @throws Main\ArgumentException
	 */
	public static function getUnsubscribedUsers($subscribeCode)
	{
		if (strlen($subscribeCode) <= 0)
		{
			throw new Main\ArgumentException('Invalid $subscribeCode passed', '$subscribeCode');
		}

		$rsUnsubscription = CTszhUnsubscriptionTable::getList(
			array(
				"filter" => array("SUBSCRIBE_CODE" => $subscribeCode),
				"select" => array("USER_ID"),
			)
		);
		$arResult = array();
		while ($arUnsubscription = $rsUnsubscription->fetch())
		{
			$arResult[] = $arUnsubscription["USER_ID"];
		}
		unset($rsUnsubscription);

		return $arResult;
	}

	/**
	 * �����������/���������� ������������ �� ��������
	 *
	 * @param int|bool $userId Id ������������ (���� �� �����, �� ����� ����������� Id �������� ������������)
	 * @param array $arSubscription ������ ���������� ����� �������� � ��������� (bool) �������� (code => true|false): true - ���������, false - ��������
	 *
	 * @return void
	 * @throws Exception
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\Db\SqlQueryException
	 */
	public static function setSubscription($userId = false, array $arSubscription)
	{
		global $USER;

		if ($userId === false)
		{
			if (is_object($USER) && $USER->isAuthorized())
			{
				$userId = $USER->getId();
			}
			else
			{
				throw new Exception("Could not determine user Id");
			}
		}

		$userId = intval($userId);
		if ($userId <= 0)
		{
			throw new Main\ArgumentException('Invalid $userId passed', '$userId');
		}

		if (empty($arSubscription))
		{
			throw new Main\ArgumentException('Empty $arSubscription passed', '$arSubscription');
		}

		$arSubscribeClasses = array();
		foreach (self::getSubscribeClasses() as $className)
		{
			$arSubscribeClasses[$className] = $className::CODE;
		}
		$arInvalidCodes = array_diff(array_keys($arSubscription), $arSubscribeClasses);
		if (!empty($arInvalidCodes))
		{
			throw new Main\ArgumentException('Invalid subscribe codes passed: ' . implode(", ", $arInvalidCodes), '$arSubscription');
		}

		$arUnsubscription = array();
		$rsUnsubscription = CTszhUnsubscriptionTable::getList(array(
			"select" => array("SUBSCRIBE_CODE"),
			"filter" => array("USER_ID" => $userId),
		));
		while ($ar = $rsUnsubscription->fetch())
		{
			$arUnsubscription[] = $ar["SUBSCRIBE_CODE"];
		}

		$arAdd = array();
		$arDelete = array();
		foreach ($arSubscription as $code => $value)
		{
			if ($value)
			{
				if (in_array($code, $arUnsubscription))
				{
					$arDelete[] = $code;
				}
			}
			else
			{
				if (!in_array($code, $arUnsubscription))
				{
					$arAdd[] = $code;
				}
			}
		}

		foreach ($arDelete as $code)
		{
			CTszhUnsubscriptionTable::delete(array("SUBSCRIBE_CODE" => $code, "USER_ID" => $userId));
		}

		foreach ($arAdd as $code)
		{
			CTszhUnsubscriptionTable::add(array("SUBSCRIBE_CODE" => $code, "USER_ID" => $userId));
		}
	}

	/**
	 * ��������� �����������/���������� ������� ��� (�����������, �������� �� ������� ��������� �������� ���), ���� � ���� ���������
	 *
	 * @param array $arFields ������ ����� ������������/����������� �������� ���������
	 *
	 * @return bool
	 */
	public static function checkSendNewsItem($arFields)
	{
		if ($arFields["ID"] <= 0 || $arFields["IBLOCK_ID"] <= 0
		    || !CTszhFunctionalityController::checkSubscribe(false, $isSubscribeEdition) || !$isSubscribeEdition
		)
		{
			return false;
		}

		// ���������: �������� �� �������� �������� � ����� CTszhSubscribeNews::IBLOCK_PROPERTY_SEND
		$arSubscribeSendProp = CIBlock::GetProperties($arFields["IBLOCK_ID"], array(), array(
			"CODE" => CTszhSubscribeNews::IBLOCK_PROPERTY_SEND,
			"CHECK_PERMISSIONS" => "N",
		))->fetch();
		if (!is_array($arSubscribeSendProp) || empty($arSubscribeSendProp))
		{
			return false;
		}

		$propId = $arSubscribeSendProp["ID"];
		if (!isset($arFields["PROPERTY_VALUES"]) || !isset($arFields["PROPERTY_VALUES"][$propId]) || !is_array($arFields["PROPERTY_VALUES"][$propId]))
		{
			return true;
		}

		// ��������: �� ��� �� ���������� ���� "��������� ��������" � ������� ���
		$needSubscribe = false;
		foreach ($arFields["PROPERTY_VALUES"][$propId] as $arValue)
		{
			if (isset($arValue["VALUE"]) && $arValue["VALUE"] == "Y")
			{
				$needSubscribe = true;
			}
			break;
		}
		if (!$needSubscribe)
		{
			return true;
		}

		$arTszhsNewsIBlocks = CTszhPublicHelper::getTszhsNewsIBlocks();
		$arTszhIds = array();
		foreach ($arTszhsNewsIBlocks as $tszhId => $arIBlockInfo)
		{
			if ($arIBlockInfo["id"] == $arFields["IBLOCK_ID"])
			{
				$arTszhIds[] = $tszhId;
			}
		}
		if (!empty($arTszhIds))
		{
			try
			{
				CTszhSubscribeNews::externalExec(0, $arFields["ID"], array("IBLOCK_ID" => $arFields["IBLOCK_ID"], "TSZH_IDS" => $arTszhIds));
			}
			catch (Exception $e)
			{
				self::reportError("Error in CTszhSubscribeNews::externalExec(): " . $e->getMessage());
			}
		}

		return true;
	}

	/**
	 * ������� �������, ��������� � ������� �������� (�� b_tszh_subscribe), � ��������������� ����� ��������
	 *
	 * @param string $eventType ��� ������� (��� ������-����������� ������� � ������ ��������)
	 * @param Entity\Event $event ID ������ �������
	 *
	 * @return void
	 */
	public static function propagateSubscribeEvent($eventType, Entity\Event $event)
	{
		if (!in_array($eventType, array("onBeforeUpdate", "onBeforeDelete")))
		{
			return;
		}

		$arParameters = $event->getParameters();
		if (!isset($arParameters["id"]) || !is_array($arParameters["id"]) || !isset($arParameters["id"]["ID"]) || intval($arParameters["id"]["ID"]) <= 0)
		{
			return;
		}

		if ($eventType == "onBeforeUpdate")
		{
			if (!isset($arParameters["fields"]) || !is_array($arParameters["fields"]) || empty($arParameters["fields"]))
			{
				return;
			}

			if (isset($arParameters["fields"]["RUNNING"]))
			{
				return;
			}

			if (is_set($arParameters["fields"], "ACTIVE") && $arParameters["fields"]["ACTIVE"] != "Y")
			{
				$eventResult = new EventResult();
				$eventResult->modifyFields(array("RUNNING" => "N"));
				$event->addResult($eventResult);
			}
		}

		$arOldSubscribe = CTszhSubscribeTable::getRowById($arParameters["id"]["ID"]);
		if (!is_array($arOldSubscribe) || empty($arOldSubscribe))
		{
			return;
		}

		$className = self::getSubscribeClassName($arOldSubscribe["CODE"]);
		if (self::checkSubscribeClass($className))
		{
			if (!is_array($arOldSubscribe["PARAMS"]))
			{
				$arOldSubscribe["PARAMS"] = array();
			}
			call_user_func_array(array($className, $eventType), array($event, $arOldSubscribe));
		}
	}

	/**
	 * ��������� ��� ����������� �������� ��� ��������� ���.
	 * ���������� ����� ����� ��������� ���������� ��� (�� CTszh::add())
	 *
	 * @param array $arTszh ������ ����� ���
	 *
	 * @return void
	 */
	public static function onAfterTszhAdd(array $arTszh)
	{
		CTszhFunctionalityController::checkSubscribe(false, $isSubscribeEdition);

		if ($isSubscribeEdition)
		{
			$typeMask = CTszhBaseSubscribe::TYPE_REQUIRED | CTszhBaseSubscribe::TYPE_OPTIONAL;
		}
		else
		{
			$typeMask = CTszhBaseSubscribe::TYPE_REQUIRED;
		}

		$arClasses = self::getSubscribeClasses($typeMask);
		foreach ($arClasses as $className)
		{
			$arSettings = $className::collectSettings(false);
			$arSubscribe = array(
				"CODE" => $className::CODE,
				"TSZH_ID" => $arTszh["ID"],
				"ACTIVE" => $arSettings["ACTIVE"],
				"RUNNING" => "N",
				"PARAMS" => $arSettings["PARAMS"],
			);
			try
			{
				$oAddResult = CTszhSubscribeTable::add($arSubscribe);
				$subscribeId = intval($oAddResult->getId());
				if ($subscribeId <= 0)
				{
					throw new Exception("");
				}
			}
			catch (Exception $e)
			{
				self::reportError(Loc::getMessage("CITRUS_TSZH_SUBSCRIBE_ERROR_ADD_SUBSCRIBE",
					array(
						"#SUBSCRIBE_CODE#" => $arSubscribe["CODE"],
						"#SUBSCRIBE_NAME#" => $className::getMessage("NAME"),
						"#TSZH_ID#" => $arTszh["ID"],
						"#TSZH_NAME#" => $arTszh["NAME"],
						"#SUBSCRIBE_ROW#" => print_r($arSubscribe, true),
					)
				));
			}
		}
	}

	/**
	 * ������� ��� �������� � ���� ����������������� �������� ������� ��� ��������� ���.
	 * ���������� ����� ����� ��������� �������� ��� (�� CTszh::delete())
	 *
	 * @param int $tszhId Id ���
	 *
	 * @return void
	 */
	public static function onAfterTszhDelete($tszhId)
	{
		// ������ ����������������� ���� �������� ������� � �� �������
		// 1. ������� ��� ��������� ����������������� ���� �������� �������
		$arCustomSubscribeTypes = array();
		foreach (self::getSubscribeClasses() as $className)
		{
			$eventType = $className::EVENT_TYPE;
			self::getCustomEventTypeAndName(array("ID" => $tszhId), $eventType);
			$arCustomSubscribeTypes[] = $eventType;
		}
		// 2. ������ ������� ����������������� �������� �������
		$rsEventMessages = CEventMessage::GetList($v1, $v2, array(
			"EVENT_NAME" => /*implode("|",*/
				$arCustomSubscribeTypes/*)*/
		));
		while ($arEventMessage = $rsEventMessages->Fetch())
		{
			CEventMessage::Delete($arEventMessage["ID"]);
		}
		// 3. ������ ����������������� ���� �������� �������
		$oEventType = new CEventType();
		$rsEventTypes = CEventType::GetList(array());
		while ($arType = $rsEventTypes->fetch())
		{
			if (in_array($arType["EVENT_NAME"], $arCustomSubscribeTypes))
			{
				$oEventType->delete($arType["EVENT_NAME"]);
			}
		}

		$rsSubscribes = CTszhSubscribeTable::getList(array(
			"filter" => array("TSZH_ID" => $tszhId),
		));
		while ($arSubscribe = $rsSubscribes->fetch())
		{
			CTszhSubscribeTable::delete($arSubscribe["ID"]);
		}

		// �������� ����������� ������� ��������
		$arClasses = self::getSubscribeClasses(CTszhBaseSubscribe::TYPE_EXTERNAL);
		foreach ($arClasses as $className)
		{
			$className::onAfterTszhDelete($tszhId);
		}
	}

	/**
	 * ���������� �������� ������������: ������� ������� ������������
	 *
	 * @param int $userId Id ������������
	 *
	 * @return void
	 */
	public static function onAfterUserDelete($userId)
	{
		global $DB;

		CTszhFunctionalityController::checkSubscribe(false, $isSubscribeEdition);
		if (!$isSubscribeEdition)
		{
			return;
		}

		$DB->Query("DELETE FROM " . CTszhUnsubscriptionTable::getTableName() . " WHERE user_id = " . intval($userId), false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
	}
}

/**
 * ����� ���� ���������� �������� �������� ���������
 */
class CTszhIBlockPropertySubscribeSend
{
	const SEPARATOR = "|";

	private static function tszhCheckStatus($strCheck, $strStatus)
	{
		return preg_match('/' . $strCheck . '/i' . (defined("BX_UTF") && BX_UTF === true ? "u" : ""), $strStatus);
	}

	private static function tszhGetStatus($strStatus)
	{
		$strStatus = strlen($strStatus) ? $strStatus : Loc::getMessage("CITRUS_TSZH_IBLOCK_PROPERTY_SUBSCRIBE_SEND_STATUS_EMPTY");

		$color = false;
		if (self::tszhCheckStatus(Loc::getMessage("CITRUS_TSZH_IBLOCK_PROPERTY_SUBSCRIBE_SEND_STATUS_ERROR"), $strStatus)
		    || self::tszhCheckStatus(Loc::getMessage("CITRUS_TSZH_IBLOCK_PROPERTY_SUBSCRIBE_SEND_STATUS_INTERRAPTED"), $strStatus)
		)
		{
			$color = "#f00";
		}
		elseif ($strStatus == Loc::getMessage("CITRUS_TSZH_IBLOCK_PROPERTY_SUBSCRIBE_SEND_STATUS_EMPTY"))
		{
			$color = "#ed23eb";
		}
		elseif (self::tszhCheckStatus(Loc::getMessage("CITRUS_TSZH_IBLOCK_PROPERTY_SUBSCRIBE_SEND_STATUS_SENDING"), $strStatus))
		{
			$color = "#f58631";
		}
		elseif (self::tszhCheckStatus(Loc::getMessage("CITRUS_TSZH_IBLOCK_PROPERTY_SUBSCRIBE_SEND_STATUS_SENT"), $strStatus))
		{
			$color = "#44b85c";
		}

		if (strlen($color))
		{
			$strStatus = "<span style=\"color: {$color};\">{$strStatus}</span>";
		}

		return $strStatus;
	}

	function GetEditHtml($value, $strHTMLControlName)
	{
		$strStatus = self::tszhGetStatus($value["DESCRIPTION"]);

		$html = '<input name="' . $strHTMLControlName["VALUE"] . '" type="checkbox" '
		        . 'title="' . Loc::getMessage("CITRUS_TSZH_IBLOCK_PROPERTY_SUBSCRIBE_SEND_TITLE", array(
				"#EDITION#" => Loc::getMessage("CITRUS_TSZH_SUBSCRIBE_MIN_EDITION"),
				"#PRODUCT#" => CTszhPublicHelper::getProductName(),
			))
		        . '" value="Y" ' . ($value["VALUE"] == "Y" ? 'checked="checked"' : '') . '><span style="margin-left: 20px;">' . Loc::getMessage("CITRUS_TSZH_IBLOCK_PROPERTY_SUBSCRIBE_SEND_STATUS") . ': ' . $strStatus . '</span>';

		return $html;
	}

	function GetViewHtml($value, $strHTMLControlName)
	{
		$html = ($value["VALUE"] == "Y" ? Loc::getMessage("CITRUS_TSZH_IBLOCK_PROPERTY_SUBSCRIBE_SEND_VALUE_YES") : Loc::getMessage("CITRUS_TSZH_IBLOCK_PROPERTY_SUBSCRIBE_SEND_VALUE_NO")) . " (" . self::tszhGetStatus($value["DESCRIPTION"]) . ")";

		return $html;
	}

	function GetPublicViewHTML($arProperty, $value, $strHTMLControlName)
	{
		return self::GetViewHtml($value, $strHTMLControlName);
	}

	function GetPublicEditHTML($arProperty, $value, $strHTMLControlName)
	{
		return self::GetEditHtml($value, $strHTMLControlName);
	}

	function GetAdminListViewHTML($arProperty, $value, $strHTMLControlName)
	{
		return self::GetViewHtml($value, $strHTMLControlName);
	}

	function GetPropertyFieldHtml($arProperty, $value, $strHTMLControlName)
	{
		return self::GetEditHtml($value, $strHTMLControlName);
	}

	function CheckFields($arProperty, $value)
	{
		$arErrors = array();

		return $arErrors;
	}

	function ConvertToDB($arProperty, $value)
	{
		$value["VALUE"] = $value["VALUE"] == "Y" ? "Y" : "N";
		$value["DESCRIPTION"] = trim($value["DESCRIPTION"]);
		if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_REQUEST["IBLOCK_ID"]) && $_REQUEST["IBLOCK_ID"] == $arProperty["IBLOCK_ID"]
		    && isset($_REQUEST["ID"]) && $_REQUEST["ID"] > 0
		    && !isset($_SESSION[TSZH_MODULE_ID]["SUBSCRIBE"][$arProperty["CODE"]][$_REQUEST["ID"]])
		)
		{
			if ($arOldProperty = CIBlockElement::GetProperty($arProperty["IBLOCK_ID"], $_REQUEST["ID"], array(), array("ID" => $arProperty["ID"]))->fetch())
			{
				$value["DESCRIPTION"] = $arOldProperty["DESCRIPTION"];
			}
		}
		$value["VALUE"] = $value["VALUE"] . self::SEPARATOR . $value["DESCRIPTION"];

		return $value;
	}

	function ConvertFromDB($arProperty, $value)
	{
		if (strlen($value["VALUE"]) > 0)
		{
			$arValue = explode(self::SEPARATOR, $value["VALUE"]);

			$value = array(
				"VALUE" => $arValue[0],
				"DESCRIPTION" => $arValue[1],
			);
		}
		else
		{
			$value["VALUE"] = $value["DESCRIPTION"] = null;
		}

		return $value;
	}

	function GetUserTypeDescription()
	{
		return array(
			"PROPERTY_TYPE" => "S",
			"USER_TYPE" => "citrusTszhSubscribeSend",
			"DESCRIPTION" => Loc::getMessage("CITRUS_TSZH_IBLOCK_PROPERTY_SUBSCRIBE_SEND_DESCRIPTION"),
			//optional handlers
			"GetPublicViewHTML" => array(__CLASS__, "GetPublicViewHTML"),
			"GetPublicEditHTML" => array(__CLASS__, "GetPublicEditHTML"),
			"GetAdminListViewHTML" => array(__CLASS__, "GetAdminListViewHTML"),
			"GetPropertyFieldHtml" => array(__CLASS__, "GetPropertyFieldHtml"),
			"CheckFields" => array(__CLASS__, "CheckFields"),
			"ConvertToDB" => array(__CLASS__, "ConvertToDB"),
			"ConvertFromDB" => array(__CLASS__, "ConvertFromDB"),
		);
	}
}
