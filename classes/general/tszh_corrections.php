<?

IncludeModuleLangFile(__FILE__);

/**
 * ����� ��� ������ � ���������������
 */
class CTszhAccountCorrections
{
	// ������� ��, ���������� ������
	const TABLE_NAME = 'b_tszh_accounts_corrections';
	// �������� ������ � ��������� ��������, ���� ������������ ���������������� ���� �������� ������; false, ���� �� ������������
	const USER_FIELD_ENTITY = false;

	/**
	 * ��������� ���� �������������
	 *
	 * @param array $arFields
	 * @param int $ID
	 * @return bool
	 */
	private static function CheckFields(&$arFields, $ID = 0)
	{
		global $APPLICATION, $USER_FIELD_MANAGER;
		
		if (array_key_exists('ID', $arFields))
			unset($arFields['ID']);

		$arErrors = Array();
		
		if ($ID == 0 || array_key_exists("ACCOUNT_ID", $arFields))
		{
			if (IntVal($arFields['ACCOUNT_ID']) <= 0)
				$arErrors[] = Array('id' => 'TSZH_ERROR_CORRECTIONS_NO_ACCOUNT_ID', 'text' => GetMessage("TSZH_ERROR_CORRECTIONS_NO_ACCOUNT_ID"));
		}
		
		if ($ID == 0 || array_key_exists("ACCOUNT_PERIOD_ID", $arFields))
		{
			if (IntVal($arFields['ACCOUNT_PERIOD_ID']) <= 0)
				$arErrors[] = Array('id' => 'TSZH_ERROR_CORRECTIONS_NO_ACCOUNT_PERIOD_ID', 'text' => GetMessage("TSZH_ERROR_CORRECTIONS_NO_ACCOUNT_PERIOD_ID"));
		}
		
		if (!empty($arErrors))
		{
			$e = new CAdminException($arErrors);
			$APPLICATION->ThrowException($e);
			return false;
		}

		if (self::USER_FIELD_ENTITY && !$USER_FIELD_MANAGER->CheckFields(self::USER_FIELD_ENTITY, $ID, $arFields))
			return false;

		return true;
	}

	/**
	 * ���������� �������������
	 *
	 * @param array $arFields ������ � ������ ������ ����������
	 * @return int ID ������������ ����������
	 */
	public static function Add($arFields)
	{
		global $USER_FIELD_MANAGER;

		if (!self::CheckFields($arFields))
			return false;

		$ID = CDatabase::Add(self::TABLE_NAME, $arFields);
		if (self::USER_FIELD_ENTITY && $ID > 0 && CTszh::hasUserFields($arFields))
			$USER_FIELD_MANAGER->Update(self::USER_FIELD_ENTITY, $ID, $arFields);

		return $ID;
	}

	/**
	 * ���������� �������������
	 *
	 * @param int $ID ������������� ���������� ����� 
	 * @param array $arFields ������ � ������ ���������� �����
	 * @return bool � ������ ������ ����������� false, �������� ������ ����� �������� ����� $APPLICATION->GetException()
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function Update($ID, $arFields)
	{
		global $DB, $USER_FIELD_MANAGER;
		
		$ID = IntVal($ID);
		if ($ID <= 0)
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);

		if (!self::CheckFields($arFields, $ID))
			return false;

		$strUpdate = $DB->PrepareUpdate(self::TABLE_NAME, $arFields);
		$strSql =
			"UPDATE " . self::TABLE_NAME . " SET ".
				$strUpdate.
			" WHERE ID=".$ID;

		$bSuccess = (bool)$DB->Query($strSql, false, "File: ".__FILE__."<br>Line: ".__LINE__);
		if (self::USER_FIELD_ENTITY && $bSuccess && CTszh::hasUserFields($arFields))
			$$USER_FIELD_MANAGER->Update(self::USER_FIELD_ENTITY, $ID, $arFields);
		
		return $bSuccess;
	}
	
	/**
	 * �������� ���������� �����
	 *
	 * @param int $ID ������������� ���������� ����� 
	 * @return boolean � ������ ������ ����������� false, �������� ������ ����� �������� ����� $APPLICATION->GetException()
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function Delete($ID)
	{
		global $DB, $USER_FIELD_MANAGER;
		
		$ID = intval($ID);
		if ($ID <= 0)
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);

		$DB->StartTransaction();
		
		$strSql = 'DELETE FROM ' . self::TABLE_NAME . ' WHERE ID='.$ID;
		$DB->Query($strSql, false, "FILE: ".__FILE__."<br> LINE: ".__LINE__);
		
		if (self::USER_FIELD_ENTITY)
			$USER_FIELD_MANAGER->Delete(self::USER_FIELD_ENTITY, $ID);

		$DB->Commit();

		return true;
	} 
	
	/**
	 * ��������� ������ 
	 * 
	 * *������ ��������� �����:*
	 * ID � �������������
	 * XML_ID � ������� ��� (ID � 1�)
	 * ACCOUNT_ID � ID �������� �����
	 * ACCOUNT_PERIOD_ID � ID ���������
 	 * CONTRACTOR_ID � ID ����������
	 * GROUNDS � ���������
	 * SERVICE � ������
	 * SUMM � �����
 	 * *������ ��������� ����� ����������:*
	 * CONTRACTOR_XML_ID � ������� ��� (ID � 1�)
	 * CONTRACTOR_TSZH_ID � ID ������� ���������� (���)
	 * CONTRACTOR_EXECUTOR � �������� ������������ (Y/N)
	 * CONTRACTOR_NAME � ������������ �����������
	 * CONTRACTOR_SERVICES � ������ ��������������� ����� (������)
	 * CONTRACTOR_ADDRESS - �����
	 * CONTRACTOR_PHONE � ����� ��������
	 * CONTRACTOR_BILLING � ��������� ���������
	 *
	 * @param array $arOrder ����������� ����������. ������, ������� �������� �������� ���� �����, ���������� � ����������� asc ��� desc.
	 * @param array $arFilter ������, �������� ������ �� ������������ ������. ������� � ������� �������� �������� �����, � ���������� � �� ��������.
	 * @param array|bool $arGroupBy ������, �������� ����������� ��������������� ������. ���� �������� �������� ������ �������� �����, �� �� ���� ����� ����� ����������� �����������. ���� �������� �������� ������ ������, �� ����� ������ ���������� �������, ��������������� �������. �� ��������� �������� ����� false � �� ������������.
	 * @param array|bool $arNavStartParams ������, �������� ������� ������ ��� ����������� ������������ ���������.
	 * @param array $arSelectFields ������, ���������� ������ �����, ������� ������ ������������� � ���������� �������
	 * @return CDBResult|array ����� ���������� ������ ���� CDBResult, ���������� ������, ��������������� ������� �������. ��������� ������ ��� ������������� �����������
	 */
	public static function GetList($arOrder = Array(), $arFilter = Array(), $arGroupBy = false, $arNavStartParams = false, $arSelectFields = Array())
	{
		global $USER_FIELD_MANAGER;

		static $arFields = array(
			"ID" => Array("FIELD" => "T.ID", "TYPE" => "int"),
			"XML_ID" => Array("FIELD" => "T.XML_ID", "TYPE" => "string"),
			"ACCOUNT_ID" => Array("FIELD" => "T.ACCOUNT_ID", "TYPE" => "int"),
			"ACCOUNT_PERIOD_ID" => Array("FIELD" => "T.ACCOUNT_PERIOD_ID", "TYPE" => "int"),
			"CONTRACTOR_ID" => Array("FIELD" => "T.CONTRACTOR_ID", "TYPE" => "int"),
			"GROUNDS" => Array("FIELD" => "T.GROUNDS", "TYPE" => "string"),
			"SERVICE" => Array("FIELD" => "T.SERVICE", "TYPE" => "string"),
			"SUMM" => Array("FIELD" => "T.SUMM", "TYPE" => "float"),

			"CONTRACTOR_XML_ID" => Array("FIELD" => "TC.XML_ID", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_contractors TC ON (TC.ID = T.CONTRACTOR_ID)"),
			"CONTRACTOR_TSZH_ID" => Array("FIELD" => "TC.TSZH_ID", "TYPE" => "int", "FROM" => "LEFT JOIN b_tszh_contractors TC ON (TC.ID = T.CONTRACTOR_ID)"),
			"CONTRACTOR_EXECUTOR" => Array("FIELD" => "TC.EXECUTOR", "TYPE" => "char", "FROM" => "LEFT JOIN b_tszh_contractors TC ON (TC.ID = T.CONTRACTOR_ID)"),
			"CONTRACTOR_NAME" => Array("FIELD" => "TC.NAME", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_contractors TC ON (TC.ID = T.CONTRACTOR_ID)"),
			"CONTRACTOR_SERVICES" => Array("FIELD" => "TC.SERVICES", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_contractors TC ON (TC.ID = T.CONTRACTOR_ID)"),
			"CONTRACTOR_ADDRESS" => Array("FIELD" => "TC.ADDRESS", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_contractors TC ON (TC.ID = T.CONTRACTOR_ID)"),
			"CONTRACTOR_PHONE" => Array("FIELD" => "TC.PHONE", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_contractors TC ON (TC.ID = T.CONTRACTOR_ID)"),
			"CONTRACTOR_BILLING" => Array("FIELD" => "TC.BILLING", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_contractors TC ON (TC.ID = T.CONTRACTOR_ID)"),
		);

		if (count($arSelectFields) <= 0)
		{
			$arSelectFields = array("ID", "XML_ID", "ACCOUNT_ID", "ACCOUNT_PERIOD_ID", "CONTRACTOR_ID", "GROUNDS", "SERVICE", "SUMM");
		}
		elseif (in_array('*', $arSelectFields))
		{
			$arSelectFields = array_keys($arFields);
		}
		
		if (self::USER_FIELD_ENTITY)
		{
			$obUserFieldsSql = new CUserTypeSQL;
			$obUserFieldsSql->SetEntity(self::USER_FIELD_ENTITY, "T.ID");
			$obUserFieldsSql->SetSelect($arSelectFields);
			$obUserFieldsSql->SetFilter($arFilter);
			$obUserFieldsSql->SetOrder($arOrder);
			
			$dbRes = CTszh::GetListMakeQuery(self::TABLE_NAME . ' T', $arFields, $arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields,  $obUserFieldsSql, $strUserFieldsID = "T.ID");
		}
		else
			$dbRes = CTszh::GetListMakeQuery(self::TABLE_NAME . ' T', $arFields, $arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);
		
		if (self::USER_FIELD_ENTITY && is_object($dbRes))
			$dbRes->SetUserFields($USER_FIELD_MANAGER->GetUserFields(self::USER_FIELD_ENTITY));

		return $dbRes;
	}

	/**
	 * ��������� ����� ���������� � ��������������� $ID
	 *
	 * @param int $ID ������������� ����������
	 * @return array
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function GetByID($ID)
	{
		$ID = intval($ID);
		if ($ID <= 0)
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);
		return self::GetList(Array(), Array("ID" => $ID))->GetNext();
	}
	
}
