<?php

/**
 * CTszhMeterValue
 * ����� ��� ������ � ����������� ���������
 *
 * ����:
 * ----
 * ID                - ID ������
 * METER_ID            - ID ��������
 * VALUE1            - ��������� �������� �� ������ 1
 * VALUE2            - ��������� �������� �� ������ 2
 * VALUE3            - ��������� �������� �� ������ 3
 * AMOUNT1            - ������ �������� �� 1� �� ������ 1
 * AMOUNT2            - ������ �������� �� 1� �� ������ 2
 * AMOUNT3            - ������ �������� �� 1� �� ������ 3
 * MODIFIED_BY        - ID ������������, ����������� ���������
 * TIMESTAMP_X        - ���� ��������� ���������
 *
 * @package
 * @author
 * @copyright nook.yo
 * @version 2010
 * @access public
 */
class CTszhMeterValue
{

	// ������� ��, ���������� ������
	const TABLE_NAME = 'b_tszh_meters_values';
	// �������� ������ � ��������� ��������, ���� ������������ ���������������� ���� �������� ������; false, ���� �� ������������
	const USER_FIELD_ENTITY = 'TSZH_METER_VALUE';

	public static $arGetListFields = array(
		"ID" => Array("FIELD" => "TMV.ID", "TYPE" => "int"),
		"METER_ID" => Array("FIELD" => "TMV.METER_ID", "TYPE" => "int"),
		"VALUE1" => Array("FIELD" => "TMV.VALUE1", "TYPE" => "double"),
		"VALUE2" => Array("FIELD" => "TMV.VALUE2", "TYPE" => "double"),
		"VALUE3" => Array("FIELD" => "TMV.VALUE3", "TYPE" => "double"),
		"AMOUNT1" => Array("FIELD" => "TMV.AMOUNT1", "TYPE" => "double"),
		"AMOUNT2" => Array("FIELD" => "TMV.AMOUNT2", "TYPE" => "double"),
		"AMOUNT3" => Array("FIELD" => "TMV.AMOUNT3", "TYPE" => "double"),
		"TIMESTAMP_X" => Array("FIELD" => "TMV.TIMESTAMP_X", "TYPE" => "datetime"),
		"MODIFIED_BY" => Array("FIELD" => "TMV.MODIFIED_BY", "TYPE" => "int"),
		"MODIFIED_BY_OWNER" => Array("FIELD" => "TMV.MODIFIED_BY_OWNER", "TYPE" => "char"),
		"MONTH" => Array(
			"FIELD" => "DATE_FORMAT((CASE (DAY(TIMESTAMP_X) <= #DAY#) WHEN true THEN TIMESTAMP_X ELSE DATE_ADD(TIMESTAMP_X, INTERVAL 1 MONTH) END), '%Y-%m')",
			"TYPE" => "string"
		),
		"METER_ACTIVE" => Array("FIELD" => "TM.ACTIVE", "TYPE" => "char", "FROM" => "INNER JOIN b_tszh_meters TM ON (TMV.METER_ID = TM.ID)"),
		"METER_HOUSE_METER" => Array(
			"FIELD" => "TM.HOUSE_METER",
			"TYPE" => "char",
			"FROM" => "INNER JOIN b_tszh_meters TM ON (TMV.METER_ID = TM.ID)"
		),
		"XML_ID" => Array("FIELD" => "TM.XML_ID", "TYPE" => "string", "FROM" => "INNER JOIN b_tszh_meters TM ON (TMV.METER_ID = TM.ID)"),
		"NAME" => Array("FIELD" => "TM.NAME", "TYPE" => "string", "FROM" => "INNER JOIN b_tszh_meters TM ON (TMV.METER_ID = TM.ID)"),
		"NUM" => Array("FIELD" => "TM.NUM", "TYPE" => "string", "FROM" => "INNER JOIN b_tszh_meters TM ON (TMV.METER_ID = TM.ID)"),
		"SERVICE_ID" => Array("FIELD" => "TM.SERVICE_ID", "TYPE" => "int", "FROM" => "INNER JOIN b_tszh_meters TM ON (TMV.METER_ID = TM.ID)"),
		"SERVICE_NAME" => Array("FIELD" => "TM.SERVICE_NAME", "TYPE" => "string", "FROM" => "INNER JOIN b_tszh_meters TM ON (TMV.METER_ID = TM.ID)"),
		"VALUES_COUNT" => Array("FIELD" => "TM.VALUES_COUNT", "TYPE" => "int", "FROM" => "INNER JOIN b_tszh_meters TM ON (TMV.METER_ID = TM.ID)"),
		"DEC_PLACES" => Array("FIELD" => "TM.DEC_PLACES", "TYPE" => "int", "FROM" => "INNER JOIN b_tszh_meters TM ON (TMV.METER_ID = TM.ID)"),
		"CAPACITY" => Array("FIELD" => "TM.CAPACITY", "TYPE" => "int", "FROM" => "INNER JOIN b_tszh_meters TM ON (TMV.METER_ID = TM.ID)"),
		"ACCOUNT_ID" => Array(
			"FIELD" => "TMA_.ACCOUNT_ID",
			"FIELD_SELECT" => "(SELECT GROUP_CONCAT(TMAS.ACCOUNT_ID ORDER BY TMAS.ACCOUNT_ID SEPARATOR '|') FROM b_tszh_meters_accounts TMAS WHERE TMAS.METER_ID = TMV.METER_ID)",
			"TYPE" => "int",
			"FROM" => "LEFT JOIN b_tszh_meters_accounts TMA_ ON (TMA_.METER_ID = TMV.METER_ID)",
			/*"BINDING_TABLE" => "b_tszh_meters_accounts TMAW", 
			"BINDING_EXTERNAL_FIELD" => "TMV.METER_ID", 
			"BINDING_SELECT_FIELD" => "TMAW.METER_ID", 
			"BINDING_WHERE_FIELD" => "TMAW.ACCOUNT_ID"*/
		),
		"USER_ID" => Array(
			"FIELD" => "TA.USER_ID",
			"FIELD_SELECT" => "(SELECT GROUP_CONCAT(TAS.USER_ID ORDER BY TMAS.ACCOUNT_ID SEPARATOR '|') FROM b_tszh_accounts TAS LEFT JOIN b_tszh_meters_accounts TMAS ON TAS.ID = TMAS.ACCOUNT_ID WHERE TMAS.METER_ID = TMV.METER_ID)",
			"TYPE" => "int",
			"FROM" => "LEFT JOIN b_tszh_meters_accounts TMA ON (TMA.METER_ID = TMV.METER_ID) LEFT JOIN b_tszh_accounts TA ON (TA.ID = TMA.ACCOUNT_ID)",
			/*"BINDING_TABLE" => "b_tszh_accounts TAW LEFT JOIN b_tszh_meters_accounts TMAW ON TAW.ID = TMAW.ACCOUNT_ID", 
			"BINDING_EXTERNAL_FIELD" => "TMV.METER_ID", 
			"BINDING_SELECT_FIELD" => "TMAW.METER_ID", 
			"BINDING_WHERE_FIELD" => "TAW.USER_ID"*/
		),
		"TSZH_ID" => Array(
			"FIELD" => "TA.TSZH_ID",
			"FIELD_SELECT" => "(SELECT GROUP_CONCAT(TAS.TSZH_ID ORDER BY TMAS.ACCOUNT_ID SEPARATOR '|') FROM b_tszh_accounts TAS LEFT JOIN b_tszh_meters_accounts TMAS ON TAS.ID = TMAS.ACCOUNT_ID WHERE TMAS.METER_ID = TMV.METER_ID)",
			"TYPE" => "int",
			"FROM" => "LEFT JOIN b_tszh_meters_accounts TMA ON (TMA.METER_ID = TMV.METER_ID) LEFT JOIN b_tszh_accounts TA ON (TA.ID = TMA.ACCOUNT_ID)",
			/*"BINDING_TABLE" => "b_tszh_accounts TAW LEFT JOIN b_tszh_meters_accounts TMAW ON TAW.ID = TMAW.ACCOUNT_ID", 
			"BINDING_EXTERNAL_FIELD" => "TMV.METER_ID", 
			"BINDING_SELECT_FIELD" => "TMAW.METER_ID", 
			"BINDING_WHERE_FIELD" => "TAW.TSZH_ID"*/
		),
		"ACCOUNT_XML_ID" => Array(
			"FIELD" => "TA.XML_ID",
			"FIELD_SELECT" => "(SELECT GROUP_CONCAT(TAS.XML_ID ORDER BY TMAS.ACCOUNT_ID SEPARATOR '|') FROM b_tszh_accounts TAS LEFT JOIN b_tszh_meters_accounts TMAS ON TAS.ID = TMAS.ACCOUNT_ID WHERE TMAS.METER_ID = TMV.METER_ID)",
			"TYPE" => "string",
			"FROM" => "LEFT JOIN b_tszh_meters_accounts TMA ON (TMA.METER_ID = TMV.METER_ID) LEFT JOIN b_tszh_accounts TA ON (TA.ID = TMA.ACCOUNT_ID)",
			/*"BINDING_TABLE" => "b_tszh_accounts TAW LEFT JOIN b_tszh_meters_accounts TMAW ON TAW.ID = TMAW.ACCOUNT_ID", 
			"BINDING_EXTERNAL_FIELD" => "TMV.METER_ID", 
			"BINDING_SELECT_FIELD" => "TMAW.METER_ID", 
			"BINDING_WHERE_FIELD" => "TAW.XML_ID"*/
		),
		"ACCOUNT_EXTERNAL_ID" => Array(
			"FIELD" => "TA.EXTERNAL_ID",
			"FIELD_SELECT" => "(SELECT GROUP_CONCAT(TAS.EXTERNAL_ID ORDER BY TMAS.ACCOUNT_ID SEPARATOR '|') FROM b_tszh_accounts TAS LEFT JOIN b_tszh_meters_accounts TMAS ON TAS.ID = TMAS.ACCOUNT_ID WHERE TMAS.METER_ID = TMV.METER_ID)",
			"TYPE" => "string",
			"FROM" => "LEFT JOIN b_tszh_meters_accounts TMA ON (TMA.METER_ID = TMV.METER_ID) LEFT JOIN b_tszh_accounts TA ON (TA.ID = TMA.ACCOUNT_ID)",
			/*"BINDING_TABLE" => "b_tszh_accounts TAW LEFT JOIN b_tszh_meters_accounts TMAW ON TAW.ID = TMAW.ACCOUNT_ID", 
			"BINDING_EXTERNAL_FIELD" => "TMV.METER_ID", 
			"BINDING_SELECT_FIELD" => "TMAW.METER_ID", 
			"BINDING_WHERE_FIELD" => "TAW.EXTERNAL_ID"*/
		),
		"ACCOUNT_NAME" => Array(
			"FIELD" => "TA.NAME",
			"FIELD_SELECT" => "(SELECT GROUP_CONCAT(TAS.NAME ORDER BY TMAS.ACCOUNT_ID SEPARATOR '|') FROM b_tszh_accounts TAS LEFT JOIN b_tszh_meters_accounts TMAS ON TAS.ID = TMAS.ACCOUNT_ID WHERE TMAS.METER_ID = TMV.METER_ID)",
			"TYPE" => "string",
			"FROM" => "LEFT JOIN b_tszh_meters_accounts TMA ON (TMA.METER_ID = TMV.METER_ID) LEFT JOIN b_tszh_accounts TA ON (TA.ID = TMA.ACCOUNT_ID)",
			/*"BINDING_TABLE" => "b_tszh_accounts TAW LEFT JOIN b_tszh_meters_accounts TMAW ON TAW.ID = TMAW.ACCOUNT_ID", 
			"BINDING_EXTERNAL_FIELD" => "TMV.METER_ID", 
			"BINDING_SELECT_FIELD" => "TMAW.METER_ID", 
			"BINDING_WHERE_FIELD" => "TAW.NAME"*/
		),
		"ACCOUNT_FLAT" => Array(
			"FIELD" => "TA.FLAT",
			"FIELD_SELECT" => "(SELECT GROUP_CONCAT(TAS.FLAT ORDER BY TMAS.ACCOUNT_ID SEPARATOR '|') FROM b_tszh_accounts TAS LEFT JOIN b_tszh_meters_accounts TMAS ON TAS.ID = TMAS.ACCOUNT_ID WHERE TMAS.METER_ID = TMV.METER_ID)",
			"TYPE" => "string",
			"FROM" => "LEFT JOIN b_tszh_meters_accounts TMA ON (TMA.METER_ID = TMV.METER_ID) LEFT JOIN b_tszh_accounts TA ON (TA.ID = TMA.ACCOUNT_ID)",
			/*"BINDING_TABLE" => "b_tszh_accounts TAW LEFT JOIN b_tszh_meters_accounts TMAW ON TAW.ID = TMAW.ACCOUNT_ID", 
			"BINDING_EXTERNAL_FIELD" => "TMV.METER_ID", 
			"BINDING_SELECT_FIELD" => "TMAW.METER_ID", 
			"BINDING_WHERE_FIELD" => "TAW.FLAT"*/
		),
		"ACCOUNT_AREA" => Array(
			"FIELD" => "TA.FLAT",
			"FIELD_SELECT" => "(SELECT GROUP_CONCAT(TAS.AREA ORDER BY TMAS.ACCOUNT_ID SEPARATOR '|') FROM b_tszh_accounts TAS LEFT JOIN b_tszh_meters_accounts TMAS ON TAS.ID = TMAS.ACCOUNT_ID WHERE TMAS.METER_ID = TMV.METER_ID)",
			"TYPE" => "double",
			"FROM" => "LEFT JOIN b_tszh_meters_accounts TMA ON (TMA.METER_ID = TMV.METER_ID) LEFT JOIN b_tszh_accounts TA ON (TA.ID = TMA.ACCOUNT_ID)",
			/*"BINDING_TABLE" => "b_tszh_accounts TAW LEFT JOIN b_tszh_meters_accounts TMAW ON TAW.ID = TMAW.ACCOUNT_ID", 
			"BINDING_EXTERNAL_FIELD" => "TMV.METER_ID", 
			"BINDING_SELECT_FIELD" => "TMAW.METER_ID", 
			"BINDING_WHERE_FIELD" => "TAW.AREA"*/
		),
		"ACCOUNT_LIVING_AREA" => Array(
			"FIELD" => "TA.LIVING_AREA",
			"FIELD_SELECT" => "(SELECT GROUP_CONCAT(TAS.LIVING_AREA ORDER BY TMAS.ACCOUNT_ID SEPARATOR '|') FROM b_tszh_accounts TAS LEFT JOIN b_tszh_meters_accounts TMAS ON TAS.ID = TMAS.ACCOUNT_ID WHERE TMAS.METER_ID = TMV.METER_ID)",
			"TYPE" => "double",
			"FROM" => "LEFT JOIN b_tszh_meters_accounts TMA ON (TMA.METER_ID = TMV.METER_ID) LEFT JOIN b_tszh_accounts TA ON (TA.ID = TMA.ACCOUNT_ID)",
			/*"BINDING_TABLE" => "b_tszh_accounts TAW LEFT JOIN b_tszh_meters_accounts TMAW ON TAW.ID = TMAW.ACCOUNT_ID", 
			"BINDING_EXTERNAL_FIELD" => "TMV.METER_ID", 
			"BINDING_SELECT_FIELD" => "TMAW.METER_ID", 
			"BINDING_WHERE_FIELD" => "TAW.LIVING_AREA"*/
		),
		"ACCOUNT_PEOPLE" => Array(
			"FIELD" => "TA.PEOPLE",
			"FIELD_SELECT" => "(SELECT GROUP_CONCAT(TAS.PEOPLE ORDER BY TMAS.ACCOUNT_ID SEPARATOR '|') FROM b_tszh_accounts TAS LEFT JOIN b_tszh_meters_accounts TMAS ON TAS.ID = TMAS.ACCOUNT_ID WHERE TMAS.METER_ID = TMV.METER_ID)",
			"TYPE" => "int",
			"FROM" => "LEFT JOIN b_tszh_meters_accounts TMA ON (TMA.METER_ID = TMV.METER_ID) LEFT JOIN b_tszh_accounts TA ON (TA.ID = TMA.ACCOUNT_ID)",
			/*"BINDING_TABLE" => "b_tszh_accounts TAW LEFT JOIN b_tszh_meters_accounts TMAW ON TAW.ID = TMAW.ACCOUNT_ID", 
			"BINDING_EXTERNAL_FIELD" => "TMV.METER_ID", 
			"BINDING_SELECT_FIELD" => "TMAW.METER_ID", 
			"BINDING_WHERE_FIELD" => "TAW.PEOPLE"*/
		),
	);

	/**
	 * CTszhMeterValue::GetByID()
	 * ��������� ���������� ��������� �������� �� ���� ���������
	 *
	 * @param int $ID ��� ��������� ��������
	 *
	 * @return array|bool
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function GetByID($ID)
	{
		$ID = intval($ID);
		if ($ID <= 0)
		{
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);
		}

		return self::GetList(Array(), Array("ID" => $ID))->GetNext();
	}

	/**
	 * ��������� ������ ��������� ���������
	 *
	 * @param array $arOrder ������� ����������
	 * @param array $arFilter ���� �������
	 * @param bool|array $arGroupBy
	 * @param bool|array $arNavStartParams
	 * @param array $arSelectFields
	 *
	 * @return CTszhMeterValueResult
	 */
	public static function GetList($arOrder = array(), $arFilter = array(), $arGroupBy = false, $arNavStartParams = false, $arSelectFields = array())
	{
		global $USER_FIELD_MANAGER;

		$monthEndDay = COption::GetOptionInt('citrus.tszh', 'start_of_month', 25);
		self::$arGetListFields["MONTH"]["FIELD"] = str_replace('#DAY#', $monthEndDay, self::$arGetListFields["MONTH"]["FIELD"]);

		if (count($arSelectFields) <= 0)
		{
			$arSelectFields = Array(
				"ID",
				"METER_ID",
				"VALUE1",
				"VALUE2",
				"VALUE3",
				"AMOUNT1",
				"AMOUNT2",
				"AMOUNT3",
				"MODIFIED_BY",
				"TIMESTAMP_X",
				"MODIFIED_BY_OWNER",
				"MONTH",
				// ��� ���������
				"METER_HOUSE_METER",
				"UF_*",
			);
		}
		elseif (in_array('*', $arSelectFields))
		{
			$arSelectFields = array_keys(self::$arGetListFields);
		}

		if (count(array_intersect($arSelectFields, Array("VALUE1", "VALUE2", "VALUE3", "AMOUNT1", "AMOUNT2", "AMOUNT3"))) > 0)
		{
			$arSelectFields[] = "DEC_PLACES";
			$arSelectFields[] = "VALUES_COUNT";
			$arSelectFields[] = "CAPACITY";
		}

		if (self::USER_FIELD_ENTITY)
		{
			$obUserFieldsSql = new CUserTypeSQL;
			$obUserFieldsSql->SetEntity(self::USER_FIELD_ENTITY, "TMV.ID");
			$obUserFieldsSql->SetSelect($arSelectFields);
			$obUserFieldsSql->SetFilter($arFilter);
			$obUserFieldsSql->SetOrder($arOrder);

			$dbRes = CTszh::GetListMakeQuery(self::TABLE_NAME . " TMV", self::$arGetListFields, $arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields, $obUserFieldsSql, $strUserFieldsID = "TMV.ID");
		}
		else
		{
			$dbRes = CTszh::GetListMakeQuery(self::TABLE_NAME . " TMV", self::$arGetListFields, $arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);
		}

		if (self::USER_FIELD_ENTITY && is_object($dbRes))
		{
			$dbRes->SetUserFields($USER_FIELD_MANAGER->GetUserFields(self::USER_FIELD_ENTITY));
		}

		return new CTszhMeterValueResult($dbRes, self::$arGetListFields);
	}

	/**
	 * �������� �����
	 *
	 * @param array $arFields
	 * @param int $ID
	 *
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function CheckFields(&$arFields, $ID = 0)
	{
		global $APPLICATION, $DB, $USER_FIELD_MANAGER;

		$arFields['METER_ID'] = IntVal($arFields['METER_ID']);
		$arFields['MODIFIED_BY'] = IntVal($arFields['MODIFIED_BY']);
		if ($arFields['METER_ID'] <= 0)
		{
			$APPLICATION->ThrowException("CTszhMeterValue error: METER_ID == 0");

			return false;
		}

		if (isset($arFields['VALUE1']))
		{
			$arFields['VALUE1'] = DoubleVal($arFields['VALUE1']);
		}
		if (isset($arFields['VALUE2']))
		{
			$arFields['VALUE2'] = DoubleVal($arFields['VALUE2']);
		}
		if (isset($arFields['VALUE3']))
		{
			$arFields['VALUE3'] = DoubleVal($arFields['VALUE3']);
		}

		for ($i = 1; $i <= 3; $i++)
		{
			if (isset($arFields["AMOUNT{$i}"]))
			{
				if (strlen($arFields["AMOUNT{$i}"]) > 0)
				{
					$arFields["AMOUNT{$i}"] = DoubleVal($arFields["AMOUNT{$i}"]);
				}
				else
				{
					$arFields["AMOUNT{$i}"] = false;
				}
			}
		}

		global $USER;
		if (isset($arFields["MODIFIED_BY"]) && intval($arFields["MODIFIED_BY"]) > 0)
		{
			$arFields["MODIFIED_BY"] = is_object($USER) && $USER->IsAdmin() ? $arFields["MODIFIED_BY"] : $USER->GetID();
		}
		else
		{
			$arFields["MODIFIED_BY"] = is_object($USER) ? ($USER->GetID() ? $USER->GetID() : 1) : 1;
		}

		unset($arFields['ID']);
		if (!array_key_exists("TIMESTAMP_X", $arFields))
		{
			$arFields["TIMESTAMP_X"] = ConvertTimeStamp(time() + CTszh::GetTimeZoneOffset(), "FULL");
		}

		if (!array_key_exists("MODIFIED_BY_OWNER", $arFields))
		{
			$strSql = "SELECT USER_ID FROM b_tszh_accounts TA LEFT JOIN b_tszh_meters_accounts TMA ON TA.ID = TMA.ACCOUNT_ID WHERE TMA.METER_ID=" . $arFields['METER_ID'] . " AND TA.USER_ID = " . $arFields['MODIFIED_BY'];
			$res = $DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);
			if ($res->Fetch())
			{
				$arFields["MODIFIED_BY_OWNER"] = "Y";
			}
			else
			{
				$arFields["MODIFIED_BY_OWNER"] = "N";
			}
		}

		// ��������� ��������� ��� ���������� �����������
		$meter = CTszhMeter::GetByID($arFields["METER_ID"]);
		if ($meter["CAPACITY"])
		{
			$arFields["VALUE1"] = fmod($arFields["VALUE1"], $maxValue = pow(10, $meter["CAPACITY"]));
			$arFields["VALUE2"] = fmod($arFields["VALUE2"], $maxValue);
			$arFields["VALUE3"] = fmod($arFields["VALUE3"], $maxValue);
		}

		if (self::USER_FIELD_ENTITY && !$USER_FIELD_MANAGER->CheckFields(self::USER_FIELD_ENTITY, $ID, $arFields))
		{
			return false;
		}

		return true;
	}

	/**
	 * ���������� ��������� ��������
	 *
	 * @param array $arFields
	 *
	 * @return bool
	 */
	public static function Add($arFields)
	{
		global $USER_FIELD_MANAGER;

		if (!self::CheckFields($arFields))
		{
			return false;
		}

		$ID = CDatabase::Add(self::TABLE_NAME, $arFields);
		if (self::USER_FIELD_ENTITY && $ID > 0 && CTszh::hasUserFields($arFields))
		{
			$USER_FIELD_MANAGER->Update(self::USER_FIELD_ENTITY, $ID, $arFields);
		}

		return $ID;
	}

	/**
	 * ��������� �������� ����� ���������� ��������� ��������
	 *
	 * @param int $ID ��� ��������� ��������
	 * @param array $arFields ���� �������� ��������� ��������
	 *
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function Update($ID, $arFields)
	{
		global $DB, $USER_FIELD_MANAGER;

		$ID = IntVal($ID);
		if ($ID <= 0)
		{
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);
		}

		if (!self::CheckFields($arFields, $ID))
		{
			return false;
		}

		$strUpdate = $DB->PrepareUpdate(self::TABLE_NAME, $arFields);
		$strSql =
			"UPDATE " . self::TABLE_NAME . " SET " .
			$strUpdate .
			" WHERE ID=" . $ID;

		$bSuccess = (bool)$DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
		if (self::USER_FIELD_ENTITY && $bSuccess && CTszh::hasUserFields($arFields))
		{
			$USER_FIELD_MANAGER->Update(self::USER_FIELD_ENTITY, $ID, $arFields);
		}

		return $bSuccess;
	}

	/**
	 * �������� ���������
	 *
	 * @param int $ID ��� ��������� ��������
	 *
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function Delete($ID)
	{
		global $DB, $USER_FIELD_MANAGER;

		$ID = intval($ID);
		if ($ID <= 0)
		{
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);
		}

		$DB->StartTransaction();

		$strSql = 'DELETE FROM ' . self::TABLE_NAME . ' WHERE ID=' . $ID;
		$z = $DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);

		if (self::USER_FIELD_ENTITY && $z)
		{
			$USER_FIELD_MANAGER->Delete(self::USER_FIELD_ENTITY, $ID);
		}

		$DB->Commit();

		return true;
	}
}