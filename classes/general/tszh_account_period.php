<?php

IncludeModuleLangFile(__FILE__);

use Citrus\Tszh\Types\ReceiptType;

/**
 * ��������� ������� ������
 */
class CTszhAccountPeriod
{
	/** @var string ������� ��, ���������� ������ */
	const TABLE_NAME = 'b_tszh_accounts_period';

	/**
	 * ��� ���������� �������� ������������� ������������� ��� ���� ����������� �������.
	 * �������� �� ��, ��� ��� �� ������ �������, ����� ��������� ��������� ������ � ���������� ����� ������� ��� �������� �� � ������ Add() � Update()
	 */
	protected static $houseFields = array(
		"CITY",
		"DISTRICT",
		"REGION",
		"SETTLEMENT",
		"STREET",
		"HOUSE",
		"HOUSE_AREA",
		"HOUSE_ROOMS_AREA",
		"HOUSE_COMMON_PLACES_AREA",
	);

	/** @var array ���� ��� ������� � ������ GetList() */
	static $arGetListFields = array(
		"ID" => Array("FIELD" => "TAP.ID", "TYPE" => "int"),
		"PERIOD_ID" => Array("FIELD" => "TAP.PERIOD_ID", "TYPE" => "int"),
		"TYPE" => Array("FIELD" => "TAP.TYPE", "TYPE" => "int"),
		"TIMESTAMP_X" => Array("FIELD" => "TAP.TIMESTAMP_X", "TYPE" => "datetime"),
		"BARCODE" => Array("FIELD" => "TAP.BARCODE", "TYPE" => "string"),
		"DEBT_BEG" => Array("FIELD" => "TAP.DEBT_BEG", "TYPE" => "float"),
		"DEBT_END" => Array("FIELD" => "TAP.DEBT_END", "TYPE" => "float"),
		"DEBT_PREV" => Array("FIELD" => "TAP.DEBT_PREV", "TYPE" => "float"),
		"PREPAYMENT" => Array("FIELD" => "TAP.PREPAYMENT", "TYPE" => "float"),
		"CREDIT_PAYED" => Array("FIELD" => "TAP.CREDIT_PAYED", "TYPE" => "float"),
		"SUMM_TO_PAY" => Array("FIELD" => "TAP.SUMM_TO_PAY", "TYPE" => "float"),
		"SUM_PAYED" => Array("FIELD" => "TAP.SUM_PAYED", "TYPE" => "float"),
		"LAST_PAYMENT" => Array("FIELD" => "TAP.LAST_PAYMENT", "TYPE" => "date"),
		"DATE_SENT" => Array("FIELD" => "TAP.DATE_SENT", "TYPE" => "datetime"),
		"IS_SENT" => Array("FIELD" => "(CASE (TAP.DATE_SENT IS NULL) WHEN true THEN 'N' ELSE 'Y' END)", "TYPE" => "char"),
		"PERIOD_DATE" => Array("FIELD" => "TP.DATE", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_period TP ON (TAP.PERIOD_ID=TP.ID)"),
		"PERIOD_TSZH_ID" => Array("FIELD" => "TP.TSZH_ID", "TYPE" => "int", "FROM" => "LEFT JOIN b_tszh_period TP ON (TAP.PERIOD_ID=TP.ID)"),
		"PERIOD_ACTIVE" => Array("FIELD" => "TP.ACTIVE", "TYPE" => "char", "FROM" => "LEFT JOIN b_tszh_period TP ON (TAP.PERIOD_ID=TP.ID)"),
		"PERIOD_ONLY_DEBT" => Array("FIELD" => "TP.ONLY_DEBT", "TYPE" => "char", "FROM" => "LEFT JOIN b_tszh_period TP ON (TAP.PERIOD_ID=TP.ID)"),
		"ACCOUNT_ID" => Array("FIELD" => "TA.ID", "TYPE" => "int", "FROM" => "INNER JOIN b_tszh_accounts TA ON (TAP.ACCOUNT_ID=TA.ID)"),
		"ACCOUNT_NAME" => Array("FIELD" => "TA.NAME", "TYPE" => "string", "FROM" => "INNER JOIN b_tszh_accounts TA ON (TAP.ACCOUNT_ID=TA.ID)"),
		"USER_ID" => Array("FIELD" => "TA.USER_ID", "TYPE" => "int", "FROM" => "INNER JOIN b_tszh_accounts TA ON (TAP.ACCOUNT_ID=TA.ID)"),
		"XML_ID" => Array("FIELD" => "TA.XML_ID", "TYPE" => "string", "FROM" => "INNER JOIN b_tszh_accounts TA ON (TAP.ACCOUNT_ID=TA.ID)"),
		"EXTERNAL_ID" => Array("FIELD" => "TA.EXTERNAL_ID", "TYPE" => "string", "FROM" => "INNER JOIN b_tszh_accounts TA ON (TAP.ACCOUNT_ID=TA.ID)"),

		// b_tszh
		"TSZH_RECEIPT_TEMPLATE" => Array("FIELD" => "T.RECEIPT_TEMPLATE", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh T ON (TP.TSZH_ID = T.ID)"),

		// b_tszh_accounts
		"FLAT" => Array("FIELD" => "TA.FLAT", "TYPE" => "string", "FROM" => "INNER JOIN b_tszh_accounts TA ON (TAP.ACCOUNT_ID=TA.ID)"),
		"FLAT_ABBR" => Array("FIELD" => "TA.FLAT_ABBR", "TYPE" => "string", "FROM" => "INNER JOIN b_tszh_accounts TA ON (TAP.ACCOUNT_ID=TA.ID)"),
		"FLAT_INT" => Array(
			"FIELD" => "CAST(TA.FLAT as UNSIGNED)",
			"TYPE" => "int",
			"FROM" => "INNER JOIN b_tszh_accounts TA ON (TAP.ACCOUNT_ID=TA.ID)"
		),
		"FLAT_TYPE" => Array("FIELD" => "TA.FLAT_TYPE", "TYPE" => "string", "FROM" => "INNER JOIN b_tszh_accounts TA ON (TAP.ACCOUNT_ID=TA.ID)"),
		"AREA" => Array("FIELD" => "TA.AREA", "TYPE" => "double", "FROM" => "INNER JOIN b_tszh_accounts TA ON (TAP.ACCOUNT_ID=TA.ID)"),
		"LIVING_AREA" => Array("FIELD" => "TA.LIVING_AREA", "TYPE" => "double", "FROM" => "INNER JOIN b_tszh_accounts TA ON (TAP.ACCOUNT_ID=TA.ID)"),
		"PEOPLE" => Array("FIELD" => "TA.PEOPLE", "TYPE" => "int", "FROM" => "INNER JOIN b_tszh_accounts TA ON (TAP.ACCOUNT_ID=TA.ID)"),
		"REGISTERED_PEOPLE" => Array(
			"FIELD" => "TA.REGISTERED_PEOPLE",
			"TYPE" => "int",
			"FROM" => "INNER JOIN b_tszh_accounts TA ON (TAP.ACCOUNT_ID=TA.ID)"
		),
		"EXEMPT_PEOPLE" => Array("FIELD" => "TA.EXEMPT_PEOPLE", "TYPE" => "int", "FROM" => "INNER JOIN b_tszh_accounts TA ON (TAP.ACCOUNT_ID=TA.ID)"),

		// b_houses
		"CITY" => Array("FIELD" => "H.CITY", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = TA.HOUSE_ID)"),
		"DISTRICT" => Array("FIELD" => "H.DISTRICT", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = TA.HOUSE_ID)"),
		"REGION" => Array("FIELD" => "H.REGION", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = TA.HOUSE_ID)"),
		"SETTLEMENT" => Array("FIELD" => "H.SETTLEMENT", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = TA.HOUSE_ID)"),
		"STREET" => Array("FIELD" => "H.STREET", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = TA.HOUSE_ID)"),
		"HOUSE" => Array("FIELD" => "H.HOUSE", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = TA.HOUSE_ID)"),
		"ZIP" => Array("FIELD" => "H.ZIP", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = TA.HOUSE_ID)"),
		"HOUSE_BANK" => Array("FIELD" => "H.BANK", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = TA.HOUSE_ID)"),
		"HOUSE_BIK" => Array("FIELD" => "H.BIK", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = TA.HOUSE_ID)"),
		"HOUSE_RS" => Array("FIELD" => "H.RS", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = TA.HOUSE_ID)"),
		"HOUSE_KS" => Array("FIELD" => "H.KS", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = TA.HOUSE_ID)"),
		"HOUSE_OVERHAUL_BANK" => Array(
			"FIELD" => "H.OVERHAUL_BANK",
			"TYPE" => "string",
			"FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = TA.HOUSE_ID)"
		),
		"HOUSE_OVERHAUL_BIK" => Array("FIELD" => "H.OVERHAUL_BIK", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = TA.HOUSE_ID)"),
		"HOUSE_OVERHAUL_RS" => Array("FIELD" => "H.OVERHAUL_RS", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = TA.HOUSE_ID)"),
		"HOUSE_OVERHAUL_KS" => Array("FIELD" => "H.OVERHAUL_KS", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = TA.HOUSE_ID)"),
		"HOUSE_AREA" => Array("FIELD" => "H.AREA", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = TA.HOUSE_ID)"),
		"HOUSE_ROOMS_AREA" => Array("FIELD" => "H.ROOMS_AREA", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = TA.HOUSE_ID)"),
		"HOUSE_COMMON_PLACES_AREA" => Array(
			"FIELD" => "H.COMMON_PLACES_AREA",
			"TYPE" => "string",
			"FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = TA.HOUSE_ID)"
		),
		"ADDRESS_FULL" => Array(
			"FIELD" => "CONCAT(H.CITY,' ',H.STREET,' ',H.HOUSE,' ',TA.FLAT)",
			"TYPE" => "string",
			"CONCAT" => true,
			"FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = TA.HOUSE_ID)"
		),

		// b_users
		"USER_LOGIN" => Array("FIELD" => "U.LOGIN", "TYPE" => "string", "FROM" => "LEFT JOIN b_user U ON (TA.USER_ID = U.ID)"),
		"USER_NAME" => Array("FIELD" => "U.NAME", "TYPE" => "string", "FROM" => "LEFT JOIN b_user U ON (TA.USER_ID = U.ID)"),
		"USER_LAST_NAME" => Array("FIELD" => "U.LAST_NAME", "TYPE" => "string", "FROM" => "LEFT JOIN b_user U ON (TA.USER_ID = U.ID)"),
		"USER_SECOND_NAME" => Array("FIELD" => "U.SECOND_NAME", "TYPE" => "string", "FROM" => "LEFT JOIN b_user U ON (TA.USER_ID = U.ID)"),
		"USER_FULL_NAME" => Array(
			"FIELD" => "CONCAT_WS(' ', U.LAST_NAME, U.NAME, U.SECOND_NAME)",
			"TYPE" => "string",
			"FROM" => "LEFT JOIN b_user U ON (TA.USER_ID = U.ID)",
			"CONCAT" => true
		),
		"USER_EMAIL" => Array("FIELD" => "U.EMAIL", "TYPE" => "string", "FROM" => "LEFT JOIN b_user U ON (TA.USER_ID = U.ID)"),
		"USER_SITE" => Array("FIELD" => "U.LID", "TYPE" => "string", "FROM" => "LEFT JOIN b_user U ON (TA.USER_ID = U.ID)"),

		// b_barcodes
		"BARCODES" => Array(
			"SEPARATOR" => '|!|',
			"FIELD" => "TB.VALUE",
			"FIELD_SELECT" => "(SELECT GROUP_CONCAT(TB.VALUE ORDER BY TB.ID SEPARATOR '|!|') FROM b_tszh_barcode TB WHERE TB.ACCOUNT_PERIOD_ID = TAP.ID AND TB.IS_INSURANCE = 'N')",
			"TYPE" => "string",
			"FROM" => "LEFT JOIN b_tszh_barcode TB ON (TB.ACCOUNT_PERIOD_ID = TAP.ID)",
			/*"BINDING_TABLE" => "b_tszh_barcodes TB",
			"BINDING_EXTERNAL_FIELD" => "TAP.ID",
			"BINDING_SELECT_FIELD" => "TB.VALUE",
			"BINDING_WHERE_FIELD" => "TB.ID"*/
		),
        "BARCODES_INSURANCE" => Array(
            "SEPARATOR" => '|!|',
            "FIELD" => "TB.VALUE",
            "FIELD_SELECT" => "(SELECT GROUP_CONCAT(TB.VALUE ORDER BY TB.ID SEPARATOR '|!|') FROM b_tszh_barcode TB WHERE TB.ACCOUNT_PERIOD_ID = TAP.ID AND TB.IS_INSURANCE = 'Y')",
            "TYPE" => "string",
            "FROM" => "LEFT JOIN b_tszh_barcode TB ON (TB.ACCOUNT_PERIOD_ID = TAP.ID)",
            /*"BINDING_TABLE" => "b_tszh_barcodes TB",
            "BINDING_EXTERNAL_FIELD" => "TAP.ID",
            "BINDING_SELECT_FIELD" => "TB.VALUE",
            "BINDING_WHERE_FIELD" => "TB.ID"*/
        ),
		"BARCODES_TYPE" => Array(
			"SEPARATOR" => '|!|',
			"FIELD" => " TB.TYPE",
			"FIELD_SELECT" => "(SELECT GROUP_CONCAT(TB.TYPE ORDER BY TB.ID SEPARATOR '|!|') FROM b_tszh_barcode TB WHERE TB.ACCOUNT_PERIOD_ID = TAP.ID AND TB.IS_INSURANCE = 'N')",
			"TYPE" => "string",
			"FROM" => "LEFT JOIN b_tszh_barcode TB ON (TB.ACCOUNT_PERIOD_ID = TAP.ID)",
			/*"BINDING_TABLE" => "b_tszh_barcodes TB",
			"BINDING_EXTERNAL_FIELD" => "TAP.ID",
			"BINDING_SELECT_FIELD" => "TB.TYPE",
			"BINDING_WHERE_FIELD" => "TB.ID"*/
		),
        "BARCODES_INSURANCE_TYPE" => Array(
            "SEPARATOR" => '|!|',
            "FIELD" => " TB.TYPE",
            "FIELD_SELECT" => "(SELECT GROUP_CONCAT(TB.TYPE ORDER BY TB.ID SEPARATOR '|!|') FROM b_tszh_barcode TB WHERE TB.ACCOUNT_PERIOD_ID = TAP.ID AND TB.IS_INSURANCE = 'Y')",
            "TYPE" => "string",
            "FROM" => "LEFT JOIN b_tszh_barcode TB ON (TB.ACCOUNT_PERIOD_ID = TAP.ID)",
            /*"BINDING_TABLE" => "b_tszh_barcodes TB",
            "BINDING_EXTERNAL_FIELD" => "TAP.ID",
            "BINDING_SELECT_FIELD" => "TB.TYPE",
            "BINDING_WHERE_FIELD" => "TB.ID"*/
        ),
	);

	/**
	 * ��������� ������ ���������
	 *
	 * @param array $arOrder
	 * @param array $arFilter
	 * @param bool|array $arGroupBy
	 * @param bool|array $arNavStartParams
	 * @param array $arSelectFields
	 *
	 * @return bool|CDBResult
	 */
	public static function GetList($arOrder = array(), $arFilter = array(), $arGroupBy = false, $arNavStartParams = false, $arSelectFields = array())
	{
		global $USER_FIELD_MANAGER;

		if (count($arSelectFields) <= 0)
		{
			$arSelectFields = Array(
				"ID",
				"PERIOD_ID",
				"TYPE",
				"PERIOD_DATE",
				"PERIOD_MONTH",
				"PERIOD_TSZH_ID",
				"DATE_SENT",
				"TIMESTAMP_X",
				"BARCODE",
				"DEBT_BEG",
				"DEBT_END",
				"SUM_PAYED",
				"ACCOUNT_ID",
				"USER_ID",
				"XML_ID",
				"CITY",
				"DISTRICT",
				"REGION",
				"SETTLEMENT",
				"STREET",
				"HOUSE",
				"ZIP",
				"HOUSE_BANK",
				"HOUSE_BIK",
				"HOUSE_RS",
				"HOUSE_KS",
				"HOUSE_OVERHAUL_BANK",
				"HOUSE_OVERHAUL_BIK",
				"HOUSE_OVERHAUL_RS",
				"HOUSE_OVERHAUL_KS",
				"FLAT",
				"FLAT_ABBR",
				"AREA",
				"LIVING_AREA",
				"PEOPLE",
				"UF_*"
			);
		}
		elseif (in_array('*', $arSelectFields))
		{
			$arSelectFields = array_merge(array_keys(self::$arGetListFields), array("UF_*"));
		}
		// join accounts table for user fields
		foreach ($arSelectFields as $field)
		{
			if (substr($field, 0, 5) == 'USER_' && $field != "USER_ID")
			{
				$arSelectFields = array_merge(Array("USER_ID"), $arSelectFields);
			}
		}
		// join period table for TSZH_RECEIPT_TEMPLATE
		if (in_array("TSZH_RECEIPT_TEMPLATE", $arSelectFields))
		{
			$arSelectFields = array_merge(Array("PERIOD_TSZH_ID"), $arSelectFields);
		}
		$arSelectFields = array_unique($arSelectFields);

		$obUserFieldsSql = new CUserTypeSQL;
		$obUserFieldsSql->SetEntity("TSZH_ACCOUNT_PERIOD", "TAP.ID");
		$obUserFieldsSql->SetSelect($arSelectFields);
		$obUserFieldsSql->SetFilter($arFilter);
		$obUserFieldsSql->SetOrder($arOrder);

		$dbRes = CTszh::GetListMakeQuery(self::TABLE_NAME . ' TAP', self::$arGetListFields, $arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields, $obUserFieldsSql, $strUserFieldsID = "TAP.ID");
		if (is_object($dbRes))
		{
			$dbRes->SetUserFields($USER_FIELD_MANAGER->GetUserFields("TSZH_ACCOUNT_PERIOD"));
		}

		return new CTszhAccountPeriodResult($dbRes, self::$arGetListFields);
	}

	/**
	 * ���������� ������������� ������ ����� ��������� �� ID
	 *
	 * @param mixed $ID ��� �������
	 *
	 * @return array|false
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function GetByID($ID)
	{
		$ID = IntVal($ID);
		if ($ID <= 0)
		{
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);
		}

		return self::GetList(Array(), Array("ID" => $ID), false, Array('nTopCount' => 1), Array('*'))->GetNext(false, false);
	}

	/**
	 * ��������� ������������ ��������� �����
	 *
	 * @param array $arFields
	 * @param string $strOperation
	 *
	 * @return bool
	 */
	public static function CheckFields(&$arFields, $strOperation = 'ADD')
	{
		global $APPLICATION, $USER_FIELD_MANAGER;

		if ($strOperation == 'ADD')
		{
			$arFields['ACCOUNT_ID'] = IntVal($arFields['ACCOUNT_ID']);
			if ($arFields['ACCOUNT_ID'] <= 0)
			{
				return false;
			}
		}
		else
		{
			unset($arFields['ACCOUNT_ID']);
		}

		if (is_set($arFields, 'TYPE'))
		{
			if (!ReceiptType::hasValue($arFields["TYPE"]))
			{
				$APPLICATION->ThrowException(GetMessage("CITRUS_TSZH_RECEIPT_UNKNOWN_TYPE", array("#TYPE#" => $arFields["TYPE"])));

				return false;
			}
		}

		unset($arFields['TIMESTAMP_X']);

		if (!$USER_FIELD_MANAGER->CheckFields("TSZH_ACCOUNT_PERIOD", $arFields['ID'], $arFields))
		{
			return false;
		}

		unset($arFields['ID']);

		return true;
	}

	/**
	 * ��������� ��������� � ���������� ������
	 *
	 * @param array $arFields
	 *
	 * @return bool
	 */
	public static function Add($arFields)
	{
		global $USER_FIELD_MANAGER;

		if (!self::CheckFields($arFields, 'ADD'))
		{
			return false;
		}

		unset($arFields['ID']);
		$ID = CDatabase::Add(self::TABLE_NAME, $arFields);

		if ($ID)
		{
			if (array_key_exists("BARCODES", $arFields) && is_array($arFields["BARCODES"]))
			{
				if (!CTszhBarCode::setForAccountPeriod($ID, $arFields["BARCODES"], false))
				{
					return false;
				}
			}

			$arFields["ID"] = $ID;
			if (CTszh::hasUserFields($arFields))
			{
				$USER_FIELD_MANAGER->Update("TSZH_ACCOUNT_PERIOD", $ID, $arFields);
			}
		}

        if ($ID)
        {
            if (array_key_exists("BARCODES_INSURANCE", $arFields) && is_array($arFields["BARCODES_INSURANCE"]))
            {
                if (!CTszhBarCode::setForAccountPeriod($ID, $arFields["BARCODES_INSURANCE"], true))
                {
                    return false;
                }
            }

            $arFields["ID"] = $ID;
            if (CTszh::hasUserFields($arFields))
            {
                $USER_FIELD_MANAGER->Update("TSZH_ACCOUNT_PERIOD", $ID, $arFields);
            }
        }

		return $ID;
	}

	/**
	 * �������� ���������
	 *
	 * @param int $ID ID ���������
	 * @param mixed $arFields ���� ���������
	 *
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function Update($ID, $arFields)
	{
		global $DB, $USER_FIELD_MANAGER;

		$ID = IntVal($ID);
		if ($ID <= 0)
		{
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);
		}

		if (!self::CheckFields($arFields, 'UPDATE', $ID))
		{
			return false;
		}

		unset($arFields['ID']);
		unset($arFields['ACCOUNT_ID']);

		$strUpdate = $DB->PrepareUpdate(self::TABLE_NAME, $arFields);
		$strSql =
			"UPDATE " . self::TABLE_NAME . " SET " .
			$strUpdate .
			" WHERE ID=" . $ID;
		if ($DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__))
		{
			if (array_key_exists("BARCODES", $arFields) && is_array($arFields["BARCODES"]))
			{
				if (!CTszhBarCode::setForAccountPeriod($ID, $arFields["BARCODES"], false))
				{
					return false;
				}
			}

            if (array_key_exists("BARCODES_INSURANCE", $arFields) && is_array($arFields["BARCODES_INSURANCE"]))
            {
                if (!CTszhBarCode::setForAccountPeriod($ID, $arFields["BARCODES_INSURANCE"],true))
                {
                    return false;
                }
            }

			if (CTszh::hasUserFields($arFields))
			{
				$arFields["ID"] = $ID;
				$USER_FIELD_MANAGER->Update("TSZH_ACCOUNT_PERIOD", $ID, $arFields);
			}
		}

		return true;
	}

	/**
	 * ������� ��������� �� ����� ���������� � ��� �������
	 *
	 * @param mixed $ID ID ���������
	 *
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function Delete($ID)
	{
		global $DB, $USER_FIELD_MANAGER;
		$ID = intval($ID);
		if ($ID <= 0)
		{
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);
		}

		$strSql = "DELETE FROM b_tszh_accounts_contractors WHERE ACCOUNT_PERIOD_ID=" . $ID;
		$DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);

		$strSql = "DELETE FROM b_tszh_accounts_corrections WHERE ACCOUNT_PERIOD_ID=" . $ID;
		$DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);

		// ������ � ��������� �������
		$strSql = "DELETE FROM b_tszh_accounts_installments WHERE ACCOUNT_PERIOD_ID=" . $ID;
		$DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);

		$strSql = "DELETE FROM b_tszh_barcode WHERE ACCOUNT_PERIOD_ID=" . $ID;
		$DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);

		// ���������� �� ��������� ������� ����� API ��� �������� �������� ���������������� ����� � �������� ���������
		$rs = CTszhCharge::GetList(Array(), Array("ACCOUNT_PERIOD_ID" => $ID), false, false, Array("ID", "PERIOD_TSZH_ID"));
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($ar = $rs->Fetch())
		{
			CTszhCharge::Delete($ar['ID']);
		}

		$strSql = "DELETE FROM " . self::TABLE_NAME . ' WHERE ID=' . $ID;
		$DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);

		$USER_FIELD_MANAGER->Delete("TSZH_ACCOUNT_PERIOD", $ID);

		return true;
	}

	/**
	 * ���������� ������ ������� ������, � ������� ����� �������������� �� ���������� �� ������ ������ ���������
	 *
	 * @param $arFilter
	 *
	 * @return bool|CDBResult
	 */
	public static function GetTotalDebt($arFilter, $arOrder)
	{
		$arSelectFilter = array(
			'PERIOD_ID' => $arFilter['PERIOD_ID'],
			'>SUM_DEBT_END' => $arFilter['>DEBT_END'],
		);

		if (isset($arFilter['@HOUSE_ID']))
		{
			$arSelectFilter['@HOUSE_ID'] = $arFilter['@HOUSE_ID'];
		}

		if (isset($arFilter['!@USER_ID']) && count($arFilter['!@USER_ID']) > 0)
		{
			$arSelectFilter['!@USER_ID'] = $arFilter['!@USER_ID'];
		}

		if (isset($arFilter['!USER_EMAIL']))
		{
			$arSelectFilter['!USER_EMAIL'] = $arFilter['!USER_EMAIL'];
		}

		if (isset($arFilter['>ACCOUNT_ID']))
		{
			$arSelectFilter['>ACCOUNT_ID'] = $arFilter['>ACCOUNT_ID'];
		}

		if (isset($arFilter['@TYPE']))
		{
			$arSelectFilter['@TYPE'] = $arFilter['@TYPE'];
		}

		if (isset($arOrder['DEBT_END']))
		{
			$arOrder['SUM_DEBT_END'] = $arOrder['DEBT_END'];
			unset($arOrder['DEBT_END']);
		}

		if (isset($arOrder['DEBT_BEG']))
		{
			$arOrder['SUM_DEBT_BEG'] = $arOrder['DEBT_BEG'];
			unset($arOrder['DEBT_BEG']);
		}

		if (empty($arOrder))
		{
			$arOrder = array('SUM_DEBT_END' => 'DESC');
		}

		try
		{
			$result = \Citrus\Tszh\AccountsPeriodTable::getList(
				array(
					'select' => array(
						'ACCOUNT_ID',
						'PERIOD_ID',
						'TIMESTAMP_X',
						'PERIOD_DATE' => 'PERIOD.DATE',
						'ACCOUNT_NAME' => 'ACCOUNT.NAME',
						'XML_ID' => 'ACCOUNT.XML_ID',
						'FLAT' => 'ACCOUNT.FLAT',
						'FLAT_ABBR' => 'ACCOUNT.FLAT_ABBR',
						'FLAT_TYPE' => 'ACCOUNT.FLAT_TYPE',
						'AREA' => 'ACCOUNT.AREA',
						'LIVING_AREA' => 'ACCOUNT.LIVING_AREA',
						'PEOPLE' => 'ACCOUNT.PEOPLE',
						'REGISTERED_PEOPLE' => 'ACCOUNT.REGISTERED_PEOPLE',
						'EXEMPT_PEOPLE' => 'ACCOUNT.EXEMPT_PEOPLE',
						'HOUSE_ID' => 'ACCOUNT.HOUSE_ID',
						'CITY' => 'ACCOUNT.HOUSE.CITY',
						'DISTRICT' => 'ACCOUNT.HOUSE.DISTRICT',
						'REGION' => 'ACCOUNT.HOUSE.REGION',
						'SETTLEMENT' => 'ACCOUNT.HOUSE.SETTLEMENT',
						'STREET' => 'ACCOUNT.HOUSE.STREET',
						'HOUSE' => 'ACCOUNT.HOUSE.HOUSE',
						'ADDRESS_FULL' => 'ACCOUNT.FULL_ADDRESS',
						'USER_ID' => 'ACCOUNT.USER.ID',
						'USER_EMAIL' => 'ACCOUNT.USER.EMAIL',
						'SUM_DEBT_END',
						'SUM_DEBT_BEG',
						'SUM_DEBT_PREV',
						'SUM_PREPAYMENT',
						'SUM_CREDIT_PAYED',
						'SUM_SUMM_TO_PAY',
					),
					'filter' => $arSelectFilter,
					'order' => $arOrder,
					'runtime' => array(
						new Bitrix\Main\Entity\ExpressionField('SUM_DEBT_END', 'SUM(%s)', array('DEBT_END')),
						new Bitrix\Main\Entity\ExpressionField('SUM_DEBT_BEG', 'SUM(%s)', array('DEBT_BEG')),
						new Bitrix\Main\Entity\ExpressionField('SUM_DEBT_PREV', 'SUM(%s)', array('DEBT_PREV')),
						new Bitrix\Main\Entity\ExpressionField('SUM_PREPAYMENT', 'SUM(%s)', array('PREPAYMENT')),
						new Bitrix\Main\Entity\ExpressionField('SUM_CREDIT_PAYED', 'SUM(%s)', array('CREDIT_PAYED')),
						new Bitrix\Main\Entity\ExpressionField('SUM_SUMM_TO_PAY', 'SUM(%s)', array('SUMM_TO_PAY')),
					),
				)
			);
			
			$arResult = $result->fetchAll();

			foreach ($arResult as &$el)
			{
				$el['DEBT_END'] = $el['SUM_DEBT_END'];
				$el['DEBT_BEG'] = $el['SUM_DEBT_BEG'];
				$el['DEBT_PREV'] = $el['SUM_DEBT_PREV'];
				$el['PREPAYMENT'] = $el['SUM_PREPAYMENT'];
				$el['CREDIT_PAYED'] = $el['SUM_CREDIT_PAYED'];
				$el['SUMM_TO_PAY'] = $el['SUM_SUMM_TO_PAY'];
			}
		}
		catch (\Bitrix\Main\ObjectPropertyException $e)
		{
			\Bitrix\Main\Diag\Debug::dump($e->getMessage());
			// todo add event handling
		}
		catch (\Bitrix\Main\ArgumentException $e)
		{
			\Bitrix\Main\Diag\Debug::dump($e->getMessage());
			// todo add event handling
		}
		catch (\Bitrix\Main\SystemException $e)
		{
			\Bitrix\Main\Diag\Debug::dump($e->getMessage());
			// todo add event handling
		}

		$result = new CDBResult();
		$result->InitFromArray($arResult);

		return $result;
	}
}