<?php
/**
 * ���������� �������������� ��������� ����������� ������� ��������� �� ��
 */
class CTszhAccountPeriodResult extends CTszhMultiValuesResult
{
	protected static $separator = '|!|';

	/**
	 * @param CDBResult $dbResult
	 * @param array $arGetListFields
	 */
	function __construct($dbResult, &$arGetListFields)
	{
		parent::CTszhMultiValuesResult($dbResult, $arGetListFields);
	}

	/**
	 * ��������� ����-��������� �����:
	 * ��������� ������ � ������ � ���������� �����-����� � ������������� ������.
	 *
	 * @param array|bool $arResult ������ � ������ ����������
	 */
	protected function processResult(&$arResult)
	{
		if (!is_array($arResult))
		{
			return;
		}

		// BARCODES �������� ������ � ����������� ������� �� ����������� ������� �� ����������� ���������
		if (isset($arResult["~BARCODES"]) && is_array($arResult["~BARCODES"]))
		{
			foreach ($arResult["~BARCODES"] as $k => $v)
			{
				$arResult["BARCODES"][$k] = array(
					"VALUE" => $v,
					"TYPE" => $arResult["BARCODES_TYPE"][$k],
				);
			}
			unset($arResult["BARCODES_TYPE"]);
		}
		elseif (isset($arResult["BARCODES"]) && is_array($arResult["BARCODES"]))
		{
			foreach ($arResult["BARCODES"] as $k => $v)
			{
				$arResult["BARCODES"][$k] = array(
					"VALUE" => $v,
					"TYPE" => $arResult["BARCODES_TYPE"][$k],
				);
			}
			unset($arResult["BARCODES_TYPE"]);
		}

        if (isset($arResult["~BARCODES_INSURANCE"]) && is_array($arResult["~BARCODES_INSURANCE"]))
        {
            foreach ($arResult["~BARCODES_INSURANCE"] as $k => $v)
            {
                $arResult["BARCODES_INSURANCE"][$k] = array(
                    "VALUE" => $v,
                    "TYPE" => $arResult["BARCODES_INSURANCE_TYPE"][$k],
                );
            }
            unset($arResult["BARCODES_INSURANCE_TYPE"]);
        }
        elseif (isset($arResult["BARCODES_INSURANCE"]) && is_array($arResult["BARCODES_INSURANCE"]))
        {
            foreach ($arResult["BARCODES_INSURANCE"] as $k => $v)
            {
                $arResult["BARCODES_INSURANCE"][$k] = array(
                    "VALUE" => $v,
                    "TYPE" => $arResult["BARCODES_INSURANCE_TYPE"][$k],
                );
            }
            unset($arResult["BARCODES_INSURANCE_TYPE"]);
        }

		// ������� �����-���� �� ������� �������� �� ��������
		if (is_set($arResult, "BARCODE") && !empty($arResult["BARCODE"]))
		{
			$arResult["BARCODES"][] = array(
				"TYPE" => "code128",
				"VALUE" => $arResult["BARCODE"],
			);
		}
		// ��� ������������� �� ������� ���������, ������ � ����� BARCODE ������ �������� ���� code128
		elseif ((!is_set($arResult, "BARCODE") || empty($arResult["BARCODE"])) && !empty($arResult["BARCODES"]) && is_array($arResult["BARCODES"])) {
                foreach ($arResult["BARCODES"] as $barcode) {
                    if ($barcode["TYPE"] == "code128") {
                        $arResult["BARCODE"] = $barcode["VALUE"];
                    }
		    }
		}
	}

	/**
	 * @param bool $bTextHtmlAuto
	 * @param bool $use_tilda
	 *
	 * @return array|false
	 */
	public function GetNext($bTextHtmlAuto = true, $use_tilda = true)
	{
		$arResult = parent::GetNext($bTextHtmlAuto, $use_tilda);
		$this->processResult($arResult);

		return $arResult;
	}

	/**
	 * @param bool $bSetGlobalVars
	 * @param string $strPrefix
	 * @param bool $bDoEncode
	 * @param bool $bSkipEntities
	 *
	 * @return array|false
	 */
	public function NavNext($bSetGlobalVars = true, $strPrefix = "str_", $bDoEncode = true, $bSkipEntities = true)
	{
		$arResult = parent::NavNext($bSetGlobalVars, $strPrefix, $bDoEncode, $bSkipEntities);
		$this->processResult($arResult);

		return $arResult;
	}
}