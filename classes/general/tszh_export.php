<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

IncludeModuleLangFile(__FILE__);

/**
 * �������� ������ ��� �������� ��������� ��������� � �������� � ������� ������
 */
class CTszhExport
{
	static public $limit = 1000;
	// static public $limit = 100;
	/**
	 * ����������� ����� ��� �������� � 1�
	 *
	 * @param $number
	 *
	 * @return string
	 */
	static public function num($number)
	{
		return str_replace('.', ',', $number);
	}

	/**
	 * �������� ��������� ���������
	 *
	 * @param string $orgName ������������ ����������� (����������� � �����)
	 * @param string $orgINN ��� ����������� (����������� � �����)
	 * @param string $filename ���������� ���� � �����, ���� ����� �������� ������
	 * @param array $arFilter ���� ������� ��� ������� ���������, ���������� � ���� (@link CTszhMeter::GetList())
	 * @param int $step_time ����� ������ ������ � ��������. ���� == 0, �������� ������� �� ���� ���
	 * @param int $lastID ID ��������� ������������ ������ �� ���������� ��� (����� ����� �������� ��� ��������� ������)
	 * @param array $arValueFilter ���� ������� �� ���������� ��������� (@link CTszhMeterValue::GetList())
	 *
	 * @return bool|int true ���� �������� ��������� ��������� ��� ID ��������� ������������ ������ (��� �������� ��� ������ �� ��������� ����)
	 */
	static public function DoExport($orgName, $orgINN, $filename, $arFilter = Array(), $step_time = 0, &$lastID = 0, $arValueFilter = Array(), &$state = array())
	{
		$startTime = microtime(1);

		@set_time_limit(0);
		@ignore_user_abort(true);

		if (!is_array($arFilter))
		{
			$arFilter = Array();
		}

		$bNextStep = false;
		$attr_multipart = '';

		if ($lastID <= 0)
		{
			$xml = '<?xml version="1.0" encoding="' . SITE_CHARSET . '"?>' . "\n";

			if ($state['MULTIPART'])
			{
				$attr_multipart .= ' multipart="1"';
			}
			else
			{
				$attr_multipart .= ' ';
			}

			$filedate = date('d.m.Y H:i:s');

			$orgName = htmlspecialcharsbx($orgName);
			$orgINN = htmlspecialcharsbx($orgINN);
			$xml .= '<ORG name="' . $orgName . '" inn="' . $orgINN . '" filedate="' . $filedate . '" filetype="meters" version="' . TSZH_EXCHANGE_CURRENT_VERSION . '"' . $attr_multipart . '>' . "\n";
		}
		else
		{
			$xml = '';
			$bNextStep = true;
		}

		/*
		do
		{
		*/
		$users = Array();
		$limit = self::$limit;

		$accFilter = array_merge($arFilter, Array(">ID" => $lastID));
		$rsAccount = CTszhAccount::GetList(
			Array("ID" => "ASC"),
			$accFilter,
			false,
			Array('nPageSize' => $limit, 'iNumPage' => 1),
			Array("*", "UF_*")
		);

		$total = CTszhAccount::GetCount();
		$last = CTszhAccount::GetCount($accFilter) - $limit;
		$last = ($last < 0) ? 0 : $last;

		/** @noinspection PhpAssignmentInConditionInspection */
		while ($arAccount = $rsAccount->Fetch())
		{
			$lastID = $arAccount['ID'];

			$users[$arAccount['ID']] = Array(
				'attributes' => Array(
					'kod_ls' => $arAccount['EXTERNAL_ID'],
				),
				'meters' => Array(),
				'meters_history' => Array(),
			);
			foreach ($arAccount as $key => $value)
			{
				if (strpos($key, 'UF_') === 0 && !empty($value))
				{
					$users[$arAccount['ID']]['attributes'][strtolower($key)] = htmlspecialcharsbx($value);
				}
			}
		}
		unset($rsAccount);

		$rsMeters = CTszhMeter::GetList(
			array(),
			array("@ACCOUNT_ID" => array_keys($users), "ACTIVE" => "Y", "HOUSE_METER" => "N"),
			false,
			false,
			array("ID", "XML_ID", "NAME", "VALUES_COUNT", "ACCOUNT_ID")
		);
		$arMeters = array();
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($arMeter = $rsMeters->getNext())
		{
			$arMeters[$arMeter["ID"]] = $arMeter;
		}
		unset($rsMeters);
		/*
		}
		while (count($arMeters) <= 0);
		*/

		$rsMetersValues = CTszhMeterValue::GetList(
			array("TIMESTAMP_X" => "ASC", "ID" => "ASC"),
			array_merge($arValueFilter, Array("@METER_ID" => array_keys($arMeters))),
			false,
			false,
			array("ID", "VALUE1", "VALUE2", "VALUE3", "TIMESTAMP_X", "METER_ID")
		);

		/** @noinspection PhpAssignmentInConditionInspection */
		while ($arValue = $rsMetersValues->fetch())
		{
			$arMeter = $arMeters[$arValue["METER_ID"]];
			$arFields = Array(
				"value_count" => $arMeter["VALUES_COUNT"],
				"kod" => $arMeter["XML_ID"],
				"name" => $arMeter["NAME"],
				"date" => $arValue["TIMESTAMP_X"],
			);
			for ($i = 1; $i <= max($arMeter["VALUES_COUNT"], 2); $i++)
			{
				$arFields["indicbef" . $i] = $arFields["indiccur" . $i] = self::num($arValue["VALUE" . $i]);
			}
			foreach ($arMeter["ACCOUNT_ID"] as $accountID)
			{
				if (is_set($users, $accountID))
				{
					$users[$accountID]["meters"][$arValue["METER_ID"]] = $arFields;
				}
			}
		}
		unset($rsMetersValues);

		foreach ($users as $userID => $arUser)
		{
			if (count($arUser['meters']) <= 0)
			{
				continue;
			}

			$xml .= "\t<PersAssoc";
			foreach ($arUser['attributes'] as $name => $val)
			{
				$val = htmlspecialcharsbx($val);
				$xml .= " $name=\"$val\"";
			}
			$xml .= ">\n";

			foreach ($arUser['meters'] as $arMeter)
			{

				$xml .= "\t\t<meter";
				foreach ($arMeter as $name => $val)
				{
					$val = htmlspecialcharsbx($val);
					$xml .= " $name=\"$val\"";
				}
				$xml .= " />\n";

			}

			$xml .= "\t</PersAssoc>\n";
		}

		if ($last <= 0)
		{
			$xml .= "</ORG>\n";
			$state['FINAL'] = true;
		}
		/*
		else
		{
			if (!$state['MULTIPART'])
			{
				$xml .= "</ORG>\n";
			}
		}
		*/

		// �������� ������ �� ��������� ����
		file_put_contents($filename, $xml, $bNextStep ? FILE_APPEND : 0);

		$lastID = ($last > 0) ? $lastID : true;

		return array($total, $last, $limit, $lastID);
	}

	/**
	 * �������� �������� � ������� ������
	 *
	 * @param string $orgINN ��� �����������
	 * @param string $filename ���������� ���� � �����, ���� ����� ������� ���������
	 * @param array $arFilter ���� ������� �� ������� ������ (@link CTszhAccount::GetList())
	 * @param int $step_time ����� ������ ������ ����
	 * @param int $lastID ��������� ������������ ������
	 *
	 * @return bool|int true ���� �������� ��������� ��������� ��� ID ��������� ������������ ������ (��� �������� ��� ������ �� ��������� ����)
	 */
	static public function DoExportAccess($orgINN, $filename, $arFilter = Array(), $step_time = 0, $lastID = 0)
	{
		$startTime = microtime(1);

		@set_time_limit(0);
		@ignore_user_abort(true);

		if (!is_array($arFilter))
		{
			$arFilter = Array();
		}

		$bNextStep = false;
		if ($lastID <= 0)
		{
			$xml = '<?xml version="1.0" encoding="' . SITE_CHARSET . '"?>' . "\n";

			$filedate = date('d.m.Y H:i:s');

			$orgINN = htmlspecialcharsbx($orgINN);
			$xml .= '<ORG inn="' . $orgINN . '" filedate="' . $filedate . '" filetype="accexistence" version="' . TSZH_EXCHANGE_CURRENT_VERSION . '">' . "\n";
		}
		else
		{
			$xml = '';
			$bNextStep = true;
		}

		$rsAccount = CTszhAccount::GetList(
			array("ID" => "ASC"),
			array_merge(
				$arFilter,
				array(
					">ID" => $lastID,
				)
			),
			false,
			false,
			array("ID", "EXTERNAL_ID", "USER_LOGIN", "USER_EMAIL", "TSZH_SITE")
		);

		global $cnt, $total;
		$cnt = 0;
		$total = $rsAccount->SelectedRowsCount();
		$arAccounts = array();
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($arAccount = $rsAccount->Fetch())
		{
			$lastID = $arAccount["ID"];

			$arAccounts[$arAccount["ID"]] = array(
				"kod_ls" => $arAccount["EXTERNAL_ID"],
				"login" => $arAccount["USER_LOGIN"],
				"email" => CTszhSubscribe::isDummyMail($arAccount["USER_EMAIL"], $arAccount["TSZH_SITE"]) ? '' : $arAccount["USER_EMAIL"],
			);

			$cnt++;

			if ($step_time && microtime(1) - $startTime >= $step_time)
			{
				break;
			}
		}

		foreach ($arAccounts as $accountID => $arAccount)
		{
			$xml .= "\t<PersAcc";
			foreach ($arAccount as $name => $val)
			{
				$val = htmlspecialcharsbx($val);
				$xml .= " $name=\"$val\"";
			}
			$xml .= "></PersAcc>\n";
		}

		if ($cnt == $total)
		{
			$xml .= "</ORG>\n";
		}

		file_put_contents($filename, $xml, $bNextStep ? FILE_APPEND : 0);

		if ($cnt == $total)
		{
			return true;
		}
		else
		{
			return $lastID;
		}
	}
}
