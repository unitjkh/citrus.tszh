<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

IncludeModuleLangFile(__FILE__);

/**
 * ��������� �������� ��������� ��������� � ������� CSV
 */
class CTszhExportCSV
{
	static public $limit = 2000;
	/**
	 * ����������� �����
	 *
	 * @param float $number
	 * @return string
	 */
	public static function num($number)
	{
		return str_replace('.', ',', $number);
	}

	/**
	 * �������� ��������� ���������
	 *
	 * @param string $orgName ������������ ����������� (����������� � �����)
	 * @param string $orgINN ��� ����������� (����������� � �����)
	 * @param string $filename ���������� ���� � �����, ���� ����� �������� ������
	 * @param array $arFilter ���� ������� ��� ������� ���������, ���������� � ���� (@link CTszhMeter::GetList())
	 * @param int $step_time ����� ������ ������ � ��������. ���� == 0, �������� ������� �� ���� ���
	 * @param int $lastID ID ��������� ������������ ������ �� ���������� ��� (����� ����� �������� ��� ��������� ������)
	 * @param array $arValueFilter ���� ������� �� ���������� ��������� (@link CTszhMeterValue::GetList())
	 * @return bool|int true ���� �������� ��������� ��������� ��� ID ��������� ������������ ������ (��� �������� ��� ������ �� ��������� ����)
	 */
	public static function DoExport($orgName, $orgINN, $filename, $arFilter = Array(), $step_time = 0, $lastID = 0, $arValueFilter = Array())
	{
		global $APPLICATION;

		$startTime = microtime(1);

		@set_time_limit(0);
		@ignore_user_abort(true);

		if (!is_array($arFilter))
		{
			$arFilter = Array();
		}

		require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/classes/general/csv_data.php');
		$obCSV = new CCSVData();

		$arFields = Array(
			GetMessage("TSZH_EXPORT_ACCOUNT"),
			GetMessage("TSZH_EXPORT_MNAME"),
			GetMessage("TSZH_EXPORT_MCODE"),
			GetMessage("TSZH_EXPORT_MVALUES"),
			GetMessage("TSZH_EXPORT_MDATE"),
			GetMessage("TSZH_EXPORT_MVAL1"),
			GetMessage("TSZH_EXPORT_MVAL2"),
			GetMessage("TSZH_EXPORT_MVAL3")
		);
		$arMetersIdx = Array(
			'USER_EXTERNAL_ID' => 0,
			'METER_NAME' => 1,
			'METER_ID' => 2,
			'VALUES_COUNT' => 3,
			'TIMESTAMP_X' => 4,
			'VALUE1' => 5,
			'VALUE2' => 6,
			'VALUE3' => 7,
		);
		
		// $bNextStep === false �� ������ ����
		$bNextStep = $lastID > 0;
		$limit = self::$limit;

		/// �������� �������� �� ���� ���
		$accFilter = array_merge( $arFilter, Array(">ID" => $lastID) );
		$rsAccount = CTszhAccount::GetList(Array("ID" => "ASC"), $accFilter, false, Array( 'nPageSize' => $limit, 'iNumPage' => 1), Array("*"));

		$total = CTszhAccount::GetCount();
		$last = CTszhAccount::GetCount($accFilter) - $limit;
		$last = ($last < 0)?0:$last;

		if (!$bNextStep && $total == 0)
		{
			$APPLICATION->ThrowException(GetMessage("TSZH_ERROR_EXPORT_NO_DATA"), "TSZH_EXPORT_NO_DATA");
			return false;
		}

		// ������ ������ � ������� ��������
		if (!$bNextStep)
		{
			if (SITE_CHARSET !== 'windows-1251')
			{
				$arFields = $APPLICATION->ConvertCharsetArray($arFields, SITE_CHARSET, 'windows-1251');
			}
			$obCSV->SaveFile($filename, $arFields);
		}

		/// ��������� ������ ���������
		while ($arAccount = $rsAccount->Fetch()){
			$lastID = $arAccount['ID'];
			$users[$arAccount['ID']] = $arAccount;
		}
		unset($rsAccount);
		
		/// �������� �������� ��� ��������� ���������
		$rsMeters = CTszhMeter::GetList(
			array(),
			array("@ACCOUNT_ID" => array_keys($users), "ACTIVE" => "Y"),
			false,
			false,
			array("ID", "XML_ID", "NAME", "VALUES_COUNT", "ACCOUNT_ID")
		);
		$arMeters = array();

		/// ��������� ������ ���������
		while ($arMeter = $rsMeters->getNext()) $arMeters[$arMeter["ID"]] = $arMeter;
		unset($rsMeters);

		/// �������� �������� ���������
		$rsMetersValues = CTszhMeterValue::GetList(
			array("TIMESTAMP_X" => "ASC", "ID" => "ASC"),
			array_merge($arValueFilter, Array("@METER_ID" => array_keys($arMeters))),
			false,
			false,
			array("ID", "VALUE1", "VALUE2", "VALUE3", "TIMESTAMP_X", "METER_ID")
		);
		
		/// ������ � ����.
		while ($arValue = $rsMetersValues->fetch())
		{
			if(!isset($arMeters[$arValue["METER_ID"]])) continue;
			$arMeter = $arMeters[$arValue["METER_ID"]];
//			self::logMessage('csvExport', serialize($arMeter));

			foreach ($arMeter["ACCOUNT_ID"] as $accountID){
				if(!isset($users[$accountID])) continue;

				$arUser = $users[$accountID];
				
				$arValues = array(
					$arMetersIdx['USER_EXTERNAL_ID'] => $arUser['EXTERNAL_ID'],
					$arMetersIdx['METER_NAME'] => $arMeter['NAME'],
					$arMetersIdx['METER_ID'] => $arMeter['XML_ID'],
					$arMetersIdx['VALUES_COUNT'] => $arMeter['VALUES_COUNT'],
					$arMetersIdx['TIMESTAMP_X'] => $arValue['TIMESTAMP_X'],
					$arMetersIdx['VALUE1'] => self::num($arValue["VALUE1"]),
					$arMetersIdx['VALUE2'] => self::num($arValue["VALUE2"]),
					$arMetersIdx['VALUE3'] => self::num($arValue["VALUE3"]),
				);

				if (SITE_CHARSET !== 'windows-1251')
				{
					$arValues = $APPLICATION->ConvertCharsetArray($arValues, SITE_CHARSET, 'windows-1251');
				}
				$obCSV->SaveFile($filename, $arValues);

			}
		}
		unset($rsMetersValues);

		$lastID = ($last > 0) ? $lastID : true;
		return array($total, $last, $limit, $lastID);
	}
}

?>