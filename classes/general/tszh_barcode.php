<?

IncludeModuleLangFile(__FILE__);

/**
 * ����� ��� ������ �� �����-������ ���������
 */
class CTszhBarCode
{
	// ������� ��, ���������� ������
	const TABLE_NAME = 'b_tszh_barcode';
	// �������� ������ � ��������� ��������, ���� ������������ ���������������� ���� �������� ������; false, ���� �� ������������
	const USER_FIELD_ENTITY = false;

	/**
	 * @param array $arFields
	 * @param int $ID
	 * @return bool
	 */
	protected static function CheckFields(&$arFields, $ID = 0)
	{
		global $APPLICATION;

		if (array_key_exists('ID', $arFields))
			unset($arFields['ID']);

		$arErrors = Array();

		if ($ID == 0 || array_key_exists("ACCOUNT_PERIOD_ID", $arFields))
		{
			if (IntVal($arFields['ACCOUNT_PERIOD_ID']) <= 0)
				$arErrors[] = Array('id' => 'TSZH_ERROR_BARCODE_NO_ACCOUNT_PERIOD_ID', 'text' => GetMessage("TSZH_ERROR_BARCODE_NO_ACCOUNT_PERIOD_ID"));
		}

		if ($ID == 0 || array_key_exists("TYPE", $arFields))
		{
			if (strlen($arFields["TYPE"]))
				$arFields["TYPE"] = strtolower($arFields["TYPE"]);
			if (!in_array($arFields["TYPE"], array("qr", "code128")))
				$arErrors[] = Array('id' => 'TSZH_ERROR_BARCODE_WRONG_TYPE', 'text' => GetMessage("TSZH_ERROR_BARCODE_WRONG_TYPE"));
		}

		if (!empty($arErrors))
		{
			$e = new CAdminException($arErrors);
			$APPLICATION->ThrowException($e);
			return false;
		}

		global $USER_FIELD_MANAGER;
		if (self::USER_FIELD_ENTITY && !$USER_FIELD_MANAGER->CheckFields(self::USER_FIELD_ENTITY, $ID, $arFields))
			return false;

		return true;
	}

	/**
	 * ���������� �����-����
	 *
	 * @param array $arFields ������ � ������ ������ �����-����
	 * @return int ID ������������ �����-����
	 */
	public static function Add($arFields)
	{
		if (!self::CheckFields($arFields))
			return false;

		$ID = CDatabase::Add(self::TABLE_NAME, $arFields);
		if (self::USER_FIELD_ENTITY && $ID > 0 && CTszh::hasUserFields($arFields))
		{
			global $USER_FIELD_MANAGER;
			$USER_FIELD_MANAGER->Update(self::USER_FIELD_ENTITY, $ID, $arFields);
		}

		return $ID;
	}

	/**
	 * ���������� �����-����
	 *
	 * @param int $ID ������������� �����-����
	 * @param array $arFields ������ � ������ �����-����
	 * @return bool|CDBResult � ������ ������ ����������� false, �������� ������ ����� �������� ����� $APPLICATION->GetException()
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function Update($ID, $arFields)
	{
		global $DB;

		$ID = IntVal($ID);
		if ($ID <= 0)
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);

		if (!self::CheckFields($arFields, $ID))
			return false;

		$strUpdate = $DB->PrepareUpdate(self::TABLE_NAME, $arFields);
		$strSql =
			"UPDATE " . self::TABLE_NAME . " SET ".
			$strUpdate.
			" WHERE ID=".$ID;

		$bSuccess = $DB->Query($strSql, false, "File: ".__FILE__."<br>Line: ".__LINE__);
		if (self::USER_FIELD_ENTITY && $bSuccess && CTszh::hasUserFields($arFields))
		{
			global $USER_FIELD_MANAGER;
			$USER_FIELD_MANAGER->Update(self::USER_FIELD_ENTITY, $ID, $arFields);
		}

		return $bSuccess;
	}

	/**
	 * �������� �����-����
	 *
	 * @param int $ID ������������� �����-����
	 * @return bool � ������ ������ ����������� false, �������� ������ ����� �������� ����� $APPLICATION->GetException()
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function Delete($ID)
	{
		global $DB;

		$ID = intval($ID);
		if ($ID <= 0)
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);

		$DB->StartTransaction();

		$strSql = 'DELETE FROM ' . self::TABLE_NAME . ' WHERE ID='.$ID;
		$DB->Query($strSql, false, "FILE: ".__FILE__."<br> LINE: ".__LINE__);

		if (self::USER_FIELD_ENTITY)
		{
			global $USER_FIELD_MANAGER;
			$USER_FIELD_MANAGER->Delete(self::USER_FIELD_ENTITY, $ID);
		}

		$DB->Commit();

		return true;
	}

	/**
	 * ��������� ������
	 *
	 * *������ ��������� �����:*
	 * ID � �������������
	 * ACCOUNT_PERIOD_ID � ID ��������� (�� ������� b_tszh_accounts_period
	 * TYPE � ��� ��������� (�� ������ ������ �������������� code128 � qr)
	 * VALUE � �������� �����-����
	 *
	 * @param array $arOrder ����������� ����������. ������, ������� �������� �������� ���� �����, ���������� � ����������� asc ��� desc.
	 * @param array $arFilter ������, �������� ������ �� ������������ ������. ������� � ������� �������� �������� �����, � ���������� � �� ��������.
	 * @param array|bool $arGroupBy ������, �������� ����������� ��������������� ������. ���� �������� �������� ������ �������� �����, �� �� ���� ����� ����� ����������� �����������. ���� �������� �������� ������ ������, �� ����� ������ ���������� �������, ��������������� �������. �� ��������� �������� ����� false � �� ������������.
	 * @param array|bool $arNavStartParams ������, �������� ������� ������ ��� ����������� ������������ ���������.
	 * @param array $arSelectFields ������, ���������� ������ �����, ������� ������ ������������� � ���������� �������
	 * @return CDBResult ����� ���������� ������ ���� CDBResult, ���������� ������, ��������������� ������� �������.
	 */
	public static function GetList($arOrder = Array(), $arFilter = Array(), $arGroupBy = false, $arNavStartParams = false, $arSelectFields = Array())
	{
		static $arFields = array(
			"ID" => Array("FIELD" => "T.ID", "TYPE" => "int"),
			"ACCOUNT_PERIOD_ID" => Array("FIELD" => "T.ACCOUNT_PERIOD_ID", "TYPE" => "int"),
			"TYPE" => Array("FIELD" => "T.TYPE", "TYPE" => "string"),
			"VALUE" => Array("FIELD" => "T.VALUE", "TYPE" => "string"),
            "IS_INSURANCE" => Array("FIELD" => "T.IS_INSURANCE", "TYPE" => "string"),
		);

		if (count($arSelectFields) <= 0)
		{
			$arSelectFields = array_keys($arFields);
		}
		elseif (in_array('*', $arSelectFields))
		{
			$arSelectFields = array_keys($arFields);
		}

		if (self::USER_FIELD_ENTITY)
		{
			$obUserFieldsSql = new CUserTypeSQL;
			$obUserFieldsSql->SetEntity(self::USER_FIELD_ENTITY, "T.ID");
			$obUserFieldsSql->SetSelect($arSelectFields);
			$obUserFieldsSql->SetFilter($arFilter);
			$obUserFieldsSql->SetOrder($arOrder);

			$dbRes = CTszh::GetListMakeQuery(self::TABLE_NAME . ' T', $arFields, $arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields,  $obUserFieldsSql, $strUserFieldsID = "T.ID");
		}
		else
        {
            $dbRes = CTszh::GetListMakeQuery(self::TABLE_NAME . ' T', $arFields, $arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields);
        }

		if (self::USER_FIELD_ENTITY && is_object($dbRes))
		{
			global $USER_FIELD_MANAGER;
			$dbRes->SetUserFields($USER_FIELD_MANAGER->GetUserFields(self::USER_FIELD_ENTITY));
		}

		return $dbRes;
	}

	/**
	 * ��������� ����� �����-���� � ��������� ID
	 *
	 * @param int $ID ������������� ����������
	 * @return CDBResult ����� ���������� ������ ���� CDBResult, ���������� ������, ��������������� ������� �������.
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function GetByID($ID)
	{
		$ID = intval($ID);
		if ($ID <= 0)
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);
		return self::GetList(Array(), Array("ID" => $ID))->GetNext();
	}

	/**
	 * ���������� ������ �� �������� ���������� �����-����
	 *
	 * @param array $barcode ������, ���������� ������ ������ ��������� (� ������� TYPE � VALUE)
	 * @return string
	 */
	public static function getImageLink($barcode)
	{
		global $APPLICATION;

		if ($barcode["TYPE"] == "code128")
		{
			// �������� �� �������� �������
			$v = rawurlencode(preg_replace('/[^0-9]+/', '', $barcode["VALUE"]));
			return 'http://barcode.tec-it.com/barcode.ashx?code=Code128&modulewidth=fit&data=' . $v . '&dpi=300&imagetype=png&rotation=0&color=&bgcolor=&fontcolor=';
		}
		else
		{
			// ������ ��������� �����-���� ��������� ������ � ��������� utf-8
			$barcode["VALUE"] = $APPLICATION->ConvertCharset($barcode["VALUE"], SITE_CHARSET, "utf-8");
			$v = rawurlencode($barcode["VALUE"]);
			return 'https://api.qrserver.com/v1/create-qr-code/?size=300x300&ecc=Q&charset-source=UTF-8&data=' . $v;
//			return 'http://barcode.tec-it.com/barcode.ashx?code=QRCode&modulewidth=fit&data=' . $v . '&dpi=300&imagetype=png&rotation=0&color=&bgcolor=&fontcolor=&quiet=0&qunit=mm&eclevel=medium';
		}
	}

	/**
	 * @param int $accountPeriodID ID ���������
	 * @param array $values ������, ���������� ��������� �������� �����-����� (����������� ������������ ������ ::Add(), ���� array(array("TYPE" => 'xxx', "VALUE" => 'zzz'))
     * @param bool $bIsInsurance ����, ����������� �� ��, ��������� �����-��� � ����� ������������� ����������� ��� ���
	 * @return bool � ������ ������ ��������� false � ��������� �� ������ � $APPLICATION->GetException()
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function setForAccountPeriod($accountPeriodID, $values, $bIsInsurance)
	{
		if ($accountPeriodID <= 0)
			throw new \Bitrix\Main\ArgumentException('$accounPeriodID is invalid');

		$dbResult = self::GetList(array(), array("ACCOUNT_PERIOD_ID" => $accountPeriodID, "IS_INSURANCE" => ($bIsInsurance ? "Y" : "N")), false, false, array("ID"));
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($arValue = $dbResult->Fetch())
			self::Delete($arValue["ID"]);

		foreach ($values as $idx => $value)
		{
			$value["ACCOUNT_PERIOD_ID"] = $accountPeriodID;
            $value["IS_INSURANCE"] = $bIsInsurance ? "Y" : "N";
			if (!self::Add($value))
				return false;
		}

		return true;
	}

	/**
	 * ���������� ������������� ������ ��������� ����� �����-����� (��� "���" => "��������")
	 * @return array
	 */
	public static function getTypes()
	{
		return array(
			"code128" => GetMessage("CITRUS_TSZH_BARCODE_CODE128"),
			"qr" 	  => GetMessage("CITRUS_TSZH_BARCODE_QR"),
		);
	}
    public static function getTypesInsurance()
    {
        return array(
            "code128" => GetMessage("CITRUS_TSZH_BARCODE_CODE128"),
            "qr" 	  => GetMessage("CITRUS_TSZH_BARCODE_QR"),
        );
    }
}
