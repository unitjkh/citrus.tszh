<?

use Bitrix\Main;
use Bitrix\Main\Localization\Loc;
use Citrus\Tszh\Subscribe\CTszhSubscribeTable;

Loc::loadMessages(__FILE__);

/**
 * ����� �������� ����������� � ���� ������� ��������
 */
class CTszhSubscribeMeterVerifications extends CTszhBaseSubscribe implements ITszhSubscribeAgent
{
	const CODE = "MeterVerifications";
	const TYPE = self::TYPE_OPTIONAL;
	const EVENT_TYPE = "TSZH_METER_VERIFICATION_NOTICE";
	const SORT = 300;

	const EXEC_HOUR = 3;

	protected static $arParams = array(
		"CHECK_INTERVAL" => array(
			"type" => "int",
			"default" => 30,
			"readOnly" => false,
			"htmlParams" => array("size" => 5, "maxlength" => 2),
		),
	);

	protected static function calcNextExec(array $arSubscribe = null)
	{
		if (!is_set($arSubscribe, "PARAMS"))
		{
			$arSubscribe = CTszhSubscribeTable::getRowById($arSubscribe["ID"]);
			if (!is_array($arSubscribe["PARAMS"]))
			{
				$arSubscribe["PARAMS"] = array();
			}
			self::checkParams($arSubscribe["PARAMS"], $arSubscribe["ID"], true);
		}

		$nextExec = strtotime("+1 day");

		return mktime(self::EXEC_HOUR, self::EXEC_MINUTE, 0, date("n", $nextExec), date("j", $nextExec), date("Y", $nextExec));
	}

	/**
	 * ���������� ��������� �������� ����������� � ���� ������� ��������
	 *
	 * @param array $arParams ������ ���������� �������� (paramCode => value)
	 * @param int $subscribeId Id ������ ��������
	 * @param bool $useDefault ����: ������������ �������� ���������� �������� �� ���������� ����������
	 * @param bool $throwKey ����: � ���������� Main\ArgumentException ���������� ���� (key) ��������� ������ ��� ����
	 *
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Exception
	 * @return void
	 */
	public static function checkParams(array &$arParams, $subscribeId = 0, $useDefault = false, $throwKey = false)
	{
		$code = "CHECK_INTERVAL";
		$throwCode = $throwKey ? self::getSettingKey($code) : $code;
		if (is_set($arParams, $code) && strlen($arParams[$code]))
		{
			$arParams[$code] = intval($arParams[$code]);
			if ($arParams[$code] <= 0)
			{
				if ($useDefault)
				{
					unset($arParams[$code]);
				}
				else
				{
					throw new Main\ArgumentException(self::getMessage("PARAM-{$code}-ERROR-INVALID", array("#PARAM#" => self::getMessage("PARAM-{$code}-NAME"))), $throwCode);
				}
			}
		}
		elseif ($subscribeId == 0 && !$useDefault)
		{
			throw new \Exception(Loc::getMessage("Common-ERROR-PARAM-MISSING", array("#PARAM#" => self::getMessage("PARAM-{$code}-NAME"))), $throwCode);
		}

		if ($useDefault)
		{
			parent::checkParams($arParams, $subscribeId, $useDefault);
		}
	}

	/**
	 * �������-����� �������� ��������� ����������� ����������� � ���� ������� ��������
	 *
	 * @param int $subscribeId ID ������ ��������
	 * @param array $arSubscribeParams ������ ������ ����������� ���������� ��������
	 * @param array $arAgentParams ������ ���������� ������:
	 *        string HEADER_FROM - �������� � ��������� From ������
	 *        string HEADER_SENDER - �������� � ��������� Sender ������
	 *        int TSZH_ID - ID ���
	 *        string LAST_EMAIL - e-mail ������������, �������� ���� ���������� ��������� ���������� ���� �������.
	 *
	 * @return string
	 */
	public static function execAgent($subscribeId, array $arSubscribeParams, array $arAgentParams)
	{
		$subscribeId = intval($subscribeId);
		$arAgentParams["TSZH_ID"] = intval($arAgentParams["TSZH_ID"]);

		global $USER;
		$bTmpUser = false;

		if (!is_object($USER))
		{
			$bTmpUser = true;
			$USER = new CUser();
		}

		$arSubscribe = CTszhSubscribeTable::getRowById($subscribeId);
		if (!is_array($arSubscribe["PARAMS"]))
		{
			$arSubscribe["PARAMS"] = array();
		}
		self::checkParams($arSubscribe["PARAMS"], $subscribeId, true);

		// ������� ���� �����������
		$arTszh = CTszh::GetByID($arAgentParams["TSZH_ID"]);

		$checkDate = strtotime("+{$arSubscribe["PARAMS"]["CHECK_INTERVAL"]} days");
		$strCheckDate = ConvertTimeStamp($checkDate, "SHORT");

		// ���-�� ������������ ����� ������� �������
		$curEmailCount = 0;

		$lastEmail = $arAgentParams["LAST_EMAIL"];
		$maxEmailsCount = CTszhSubscribe::getOption("max_emails_per_hit");

		$rsAccounts = CTszhAccount::GetList(
			array("USER_EMAIL" => "ASC"),
			array(
				"TSZH_ID" => $arAgentParams["TSZH_ID"],
				"!USER_EMAIL" => false,
				"!@USER_ID" => CTszhSubscribe::getUnsubscribedUsers(self::CODE),
				">USER_EMAIL" => $lastEmail,
			),
			false,
			false,
			array("*")
		);
		$arAccounts = array();
		while ($arAccount = $rsAccounts->GetNext())
		{
			if (CTszhSubscribe::isDummyMail($arAccount["USER_EMAIL"], $arAccount["USER_SITE"]))
			{
				continue;
			}

			$arAccounts[$arAccount["ID"]] = $arAccount;
		}
		unset($rsAccounts);

		if (!empty($arAccounts))
		{
			CTszh::DisableTimeZone();

			// ������� �������� � �������������� ����� �������
			$rsMeters = CTszhMeter::GetList(
				array("SORT" => "ASC"),
				array(
					"ACTIVE" => "Y",
					"HOUSE_METER" => "N",
					"@ACCOUNT_ID" => array_keys($arAccounts),
					"VERIFICATION_DATE" => $strCheckDate,
				),
				false,
				false,
				array("ID", "ACCOUNT_ID", "NAME", "SERVICE_NAME")
			);
			$arMeters = array();
			while ($arMeter = $rsMeters->GetNext())
			{
				$arMeters[$arMeter["ID"]] = array(
					"ID" => $arMeter["ID"],
					"NAME" => $arMeter["NAME"],
					"SERVICE_NAME" => $arMeter["SERVICE_NAME"],
				);
				foreach ($arMeter["ACCOUNT_ID"] as $accountId)
				{
					if (isset($arAccounts[$accountId]))
					{
						$arAccounts[$accountId]["METER_IDS"][] = $arMeter["ID"];
					}
				}
			}
			unset($rsMeters);
			foreach ($arAccounts as $accountId => $arAccount)
			{
				if (!isset($arAccount["METER_IDS"]))
				{
					unset($arAccounts[$accountId]);
				}
			}

			// ������� ������ e-mail => arAccountIds
			$arEmailAccountIds = array();
			foreach ($arAccounts as $accountId => $arAccount)
			{
				$arEmailAccountIds[$arAccount["USER_EMAIL"]][] = $arAccount["ID"];
			}

			foreach ($arEmailAccountIds as $email => $arAccountIds)
			{
				if (self::getGenericEmailCount() >= $maxEmailsCount)
				{
					if ($bTmpUser)
					{
						unset($USER);
					}
					CTszh::EnableTimeZone();

					$arAgentParams["LAST_EMAIL"] = $lastEmail;

					return CTszhSubscribe::getExecAgentString(__CLASS__, array($subscribeId, array(), $arAgentParams));
				}

				$arCurMeters = array();
				foreach ($arAccountIds as $accountId)
				{
					foreach ($arAccounts[$accountId]["METER_IDS"] as $meterId)
					{
						$arCurMeters[$meterId] = $arMeters[$meterId];
					}
				}

				// ������� URL ������� �� ��������
				$listUnsubscribe = true;
				CTszhSubscribe::getMailHeaders($arTszh["SITE_ID"], $from = false, $sender = false, $listUnsubscribe, $arAccounts[$arAccountIds[0]]["USER_ID"], self::CODE);

				$arSendFields = array(
					"EMAIL_FROM" => $arAgentParams["HEADER_FROM"],
					"HEADER_SENDER" => $arAgentParams["HEADER_SENDER"],
					"EMAIL_TO" => $arAccount["USER_EMAIL"],
					"VERIFICATION_DATE" => $strCheckDate,
					"METERS" => serialize($arCurMeters),
					"ORG_NAME" => $arTszh["NAME"],
					"~ORG_NAME" => $arTszh["~NAME"],
					"ORG_ADDRESS" => $arTszh["ADDRESS"],
					"ORG_PHONE" => $arTszh["PHONE"],
					"UNSUBSCRIBE_URL" => $listUnsubscribe,
				);

				// �������� ��� �������
				if (CTszhFunctionalityController::isPortal())
				{
					// ��� ������ �������
					$entity_type = COption::GetOptionString('vdgb.portaltszh', 'entity_type');
					if ($entity_type == 'house')
					{
						// ������ ���������� �� ����
						$arHouse = \Citrus\Tszh\HouseTable::getById($arAccount['HOUSE_ID'])->fetch();
						// ���������� �� ����� � �������� �������� ���
						$arHouseSite = CSite::GetByID($arHouse['SITE_ID'])->Fetch();
						// ���������� �� ����� � �������� �������� ������������
						$arTszhSite = CSite::GetByID($arTszh['SITE_ID'])->Fetch();
						// ���� ���������� ����� ����� ���� �� ������ ����� ������������, �� ������� � ������ ������ ������
						if ($arHouseSite['SERVER_NAME'] != $arTszhSite['SERVER_NAME'])
						{
							$arSendFields['UNSUBSCRIBE_URL'] = str_replace($arTszhSite['SERVER_NAME'], $arHouseSite['SERVER_NAME'], $arSendFields['UNSUBSCRIBE_URL']);
						}
					}
				}

				CTszhSubscribe::send(self::EVENT_TYPE, $arTszh["SITE_ID"], $arSendFields, $arTszh["ID"]);

				$curEmailCount++;
				self::incGenericEmailCount();

				$lastEmail = $email;
			}
			CTszh::EnableTimeZone();
		}

		$arNewSubscribe = array(
			"ID" => $subscribeId,
		);
		parent::exec($arNewSubscribe, self::calcNextExec(array("ID" => $subscribeId)));

		if ($bTmpUser)
		{
			unset($USER);
		}

		return "";
	}

	/**
	 * ��������� ����������� ����������� � ���� ������� �������� ����� execAgent()
	 *
	 * @param array $arSubscribe ������ ����� ������ �������� (�� ������� b_tszh_subscribe)
	 * @param bool $nextExec false
	 *
	 * @return void
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\Db\SqlQueryException
	 */
	public static function exec(array $arSubscribe, $nextExec = false)
	{
		self::checkParams($arSubscribe["PARAMS"], $arSubscribe["ID"], true);

		try
		{
			// ������� ���� �����������
			$arTszh = CTszh::GetByID($arSubscribe["TSZH_ID"]);
			// ������� �������� ��������� From � Sender
			$from = $sender = true;
			CTszhSubscribe::getMailHeaders($arTszh["SITE_ID"], $from, $sender);

			$arAgentParams = array(
				"HEADER_FROM" => $from,
				"HEADER_SENDER" => $sender,
				"TSZH_ID" => $arSubscribe["TSZH_ID"],
				"LAST_EMAIL" => "",
			);
			$agentID = CTszhSubscribe::addExecAgent(__CLASS__, array($arSubscribe["ID"], array(), $arAgentParams));
			if ($agentID <= 0)
			{
				throw new Exception("", -1);
			}
		}
		catch (Exception $e)
		{
			parent::exec(array("ID" => $arSubscribe["ID"]), $e->getCode() == -1 ? false : self::calcNextExec($arSubscribe));
		}
	}
}
