<?

use Bitrix\Main\Localization\Loc;


Loc::loadMessages(__FILE__);

/**
 * ����� �������� ���������
 */
class CTszhSubscribeReceipts extends CTszhBaseSubscribe implements ITszhSubscribeAgent
{
	const CODE = "Receipts";
	const TYPE = self::TYPE_OPTIONAL;
	const INTERVAL = 1;
	const EVENT_TYPE = "TSZH_RECEIPT";
	const SORT = 400;

	protected static $arParams = array(
		"SEND_ONLY_CURRENT_PERIOD" => array(
			"type" => "checkbox",
			"default" => "Y",
			"readOnly" => false,
			"notShowInWizard" => false,
		),
		"SEND_PDF" => array(
			"type" => "checkbox",
			"default" => "N",
			"readOnly" => false,
			"notShowInWizard" => true,
		),
		"LAST_SENT_DATE" => array(
			"type" => "string",
			"default" => "",
			"readOnly" => true,
		),
	);

	protected static function calcNextExec(array $arSubscribe = null)
	{
		return strtotime("+" . self::INTERVAL . " minute");
	}

	/**
	 * ���������� ��������� �������� ���������
	 *
	 * @param array $arParams ������ ���������� �������� (paramCode => value)
	 * @param int $subscribeId Id ������ ��������
	 * @param bool $useDefault ����: ������������ �������� ���������� �������� �� ���������� ����������
	 * @param bool $throwKey ����: � ���������� Main\ArgumentException ���������� ���� (key) ��������� ������ ��� ����
	 *
	 * @return void
	 */
	public static function checkParams(array &$arParams, $subscribeId = 0, $useDefault = false, $throwKey = false)
	{
		$code = "SEND_PDF";
		$throwCode = $throwKey ? self::getSettingKey($code) : $code;
		if (is_set($arParams, $code))
		{
			if (CTszhSubscribe::isPdfAvailable())
			{
				$arParams[$code] = $arParams[$code] == "Y" ? "Y" : "N";
			}
			else
			{
				$arParams[$code] = "N";
			}
		}

		if ($useDefault)
		{
			parent::checkParams($arParams, $subscribeId, $useDefault);
		}
	}

	public static function getSettings($tszhId = false, $valuesOnly = false, $useDefaults = true, array $arSubscribe = null)
	{
		global $APPLICATION;

		$arSettings = parent::getSettings($tszhId, $valuesOnly, $useDefaults, $arSubscribe);

		$isPdfAvailable = CTszhSubscribe::isPdfAvailable();
		$hideWarning = strlen(COption::GetOptionString(TSZH_MODULE_ID, "hide_pdf_warning"));

		if (isset($arSettings["PARAMS"]))
		{
			if (is_set($arSettings["PARAMS"], "SEND_PDF"))
			{
				if (!$isPdfAvailable)
				{
					if ($valuesOnly)
					{
						$arSettings["PARAMS"]["SEND_PDF"] = "N";
					}
					else
					{
						$arSettings["PARAMS"]["SEND_PDF"]["value"] = "N";
						$arSettings["PARAMS"]["SEND_PDF"]["htmlParams"] = array("disabled" => "disabled");
						$arSettings["PARAMS"]["SEND_PDF"]["error"] = self::getMessage("PARAM-SEND_PDF-ERROR");
						if ($ex = $APPLICATION->getException())
						{
							$arSettings["PARAMS"]["SEND_PDF"]["error"] .= '<blockquote style="font-style: italic; margin-bottom:0">' . nl2br($ex->getString(), false) . '</blockquote>';
						}
					}
				}

				if (!$hideWarning)
				{
					$arSettings["PARAMS"]["SEND_PDF"]["note"] = self::getMessage("PARAM-SEND_PDF-NOTE");
				}
			}

			if (is_set($arSettings["PARAMS"], "SEND_ONLY_CURRENT_PERIOD"))
			{
				$arSettings["PARAMS"]["SEND_ONLY_CURRENT_PERIOD"]["note"] = self::getMessage("PARAM-SEND_ONLY_CURRENT_PERIOD-NOTE");
			}

			if (is_set($arSettings["PARAMS"], "LAST_SENT_DATE"))
			{
				if ($tszhId > 0 && ($valuesOnly && $arSettings["ACTIVE"] == "Y" || !$valuesOnly && $arSettings["ACTIVE"]["value"] == "Y"))
				{
					$dateSent = CTszhAccountPeriod::GetList(array("DATE_SENT" => "DESC"), array("PERIOD_TSZH_ID" => $tszhId), false, array("nTopCount" => 1), array("DATE_SENT"))->fetch();
					if (is_array($dateSent) && strlen($dateSent["DATE_SENT"]))
					{
						$arSettings["PARAMS"]["LAST_SENT_DATE"]["value"] = $dateSent["DATE_SENT"];
					}
				}
			}
		}

		return $arSettings;
	}

	/**
	 * �������-����� �������� ���������
	 *
	 * @param int $subscribeId ID ������ ��������
	 * @param array $arSubscribeParams ������ ������ ����������� ���������� ��������
	 * @param array $arAgentParams ������ ���������� ������:
	 *        int TSZH_ID - ID ���
	 *        int LAST_ID - ID ��������� ������������ ���� ������� ���������.
	 *
	 * @return string
	 */
	public static function execAgent($subscribeId, array $arSubscribeParams, array $arAgentParams)
	{
		$subscribeId = intval($subscribeId);
		$arAgentParams["TSZH_ID"] = intval($arAgentParams["TSZH_ID"]);
		$arAgentParams["LAST_ID"] = intval($arAgentParams["LAST_ID"]);

		global $USER, $DB;
		$bTmpUser = false;
		if (!is_object($USER))
		{
			$bTmpUser = true;
			$USER = new CUser();
		}

		$result = "";

		$arFilterReceipt = array(
			"PERIOD_TSZH_ID" => $arAgentParams["TSZH_ID"],
			"!@USER_ID" => CTszhSubscribe::getUnsubscribedUsers(self::CODE),
			">ID" => $arAgentParams["LAST_ID"],
			'PERIOD_ONLY_DEBT' => 'N',
		);

		if ($arSubscribeParams['SEND_ONLY_CURRENT_PERIOD'] == 'Y')
		{
			$arFilterReceipt['PERIOD_ID'] = CTszhPeriod::GetByDate(date($DB->DateFormatToPHP("YYYY-MM"), time()), $arAgentParams["TSZH_ID"])["ID"];
		}

		$lastID = CTszhSubscribe::sendReceipts(
			$arFilterReceipt,
			true,
			false,
			CTszhSubscribe::getOption("max_emails_per_hit") - self::getGenericEmailCount()
		);
		if ($lastID > 0)
		{
			$arAgentParams["LAST_ID"] = $lastID;
			$result = CTszhSubscribe::getExecAgentString(__CLASS__, array($subscribeId, array(), $arAgentParams));
		}
		else
		{
			$arNewSubscribe = array(
				"ID" => $subscribeId,
			);
			parent::exec($arNewSubscribe, self::calcNextExec());
		}

		if ($bTmpUser)
		{
			unset($USER);
		}

		return $result;
	}

	/**
	 * ��������� ����������� ��������� ����� execAgent()
	 *
	 * @param array $arSubscribe ������ ����� ������ �������� (�� ������� b_tszh_subscribe)
	 * @param bool $nextExec false
	 *
	 * @return void
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\Db\SqlQueryException
	 */
	public static function exec(array $arSubscribe, $nextExec = false)
	{
		try
		{
			$arAgentParams = array(
				"TSZH_ID" => $arSubscribe["TSZH_ID"],
				"LAST_ID" => 0,
			);
			$agentID = CTszhSubscribe::addExecAgent(__CLASS__, array($arSubscribe["ID"], $arSubscribe["PARAMS"], $arAgentParams));
			if ($agentID <= 0)
			{
				throw new Exception("", -1);
			}
		}
		catch (Exception $e)
		{
			parent::exec(array("ID" => $arSubscribe["ID"]));
		}
	}
}
