<?
use Bitrix\Main;
use Bitrix\Main\Localization\Loc;


Loc::loadMessages(__FILE__);

/**
 * ����� �������� ��������
 */
class CTszhSubscribeNews extends CTszhBaseSubscribe implements ITszhSubscribeExternal, ITszhSubscribeExternalAgent
{
	const CODE = "News";
	const TYPE = self::TYPE_EXTERNAL;
	const EVENT_TYPE = "TSZH_NEWS_ITEM";
	const SORT = 90;

	protected static $arParams = array();

	private static function setSendProperty($elementId, $iblockId, $value, $descr)
	{
		$_SESSION[TSZH_MODULE_ID]["SUBSCRIBE"][self::IBLOCK_PROPERTY_SEND][$elementId] = true;
		CIBlockElement::setPropertyValuesEx($elementId, $iblockId, array(
			self::IBLOCK_PROPERTY_SEND => array(
				"VALUE" => $value,
				"DESCRIPTION" => $descr,
			),
		));
		unset($_SESSION[TSZH_MODULE_ID]["SUBSCRIBE"][self::IBLOCK_PROPERTY_SEND][$elementId]);
	}

	/**
	 * ���������� ��������� �������� ��������
	 *
	 * @param array $arParams ������ ���������� �������� (paramCode => value)
	 * @param int $subscribeId Id ������ ��������
	 * @param bool $useDefault ����: ������������ �������� ���������� �������� �� ���������� ����������
	 * @param bool $throwKey ����: � ���������� Main\ArgumentException ���������� ���� (key) ��������� ������ ��� ����
	 *
	 * @return void
	 */
	public static function checkParams(array &$arParams, $subscribeId = 0, $useDefault = false, $throwKey = false)
	{
	}

	/**
	 * �������-����� �������� ��������
	 *
	 * @param int $tszhId Id ���, �������� �������� �������� ������������ ��������
	 * @param int $itemId Id ������������ ��������
	 * @param array $arAgentParams ������ ���������� ������
	 *        int IBLOCK_ID - Id ��������� �������
	 *        array TSZH_IDS - ������ Id ���, ������� ������� �����������
	 *        bool IS_PORTAL - ����: �������� ������� ������������ � ���������� �������
	 *        string LAST_EMAIL - e-mail ��������� �������� �����, �������� ���� ���������� ��������� ����������� ���� �������.
	 *
	 * @return string
	 */
	public static function externalExecAgent($tszhId, $itemId, array $arAgentParams)
	{
		$itemId = intval($itemId);
		$arAgentParams["IBLOCK_ID"] = intval($arAgentParams["IBLOCK_ID"]);
		foreach ($arAgentParams["TSZH_IDS"] as $key => $tszhId)
		{
			$arAgentParams["TSZH_IDS"][$key] = intval($tszhId);
		}

		global $USER;
		$bTmpUser = false;

		if (!is_object($USER))
		{
			$bTmpUser = true;
			$USER = new CUser();
		}

		if (!CModule::includeModule("iblock"))
		{
			if ($bTmpUser)
			{
				unset($USER);
			}

			return "";
		}

		// ������� �������
		$rsNewsItem = CIBlockElement::getById($itemId);
		if ($oNewsItem = $rsNewsItem->getNextElement())
		{
			$arNewsItem = $oNewsItem->getFields();
			$arNewsItem["PROPERTIES"] = $oNewsItem->getProperties();
		}
		else
		{
			if ($bTmpUser)
			{
				unset($USER);
			}

			return "";
		}

		// TimeStamp ������ ����������
		$tsActiveFrom = makeTimeStamp($arNewsItem["DATE_ACTIVE_FROM"], CSite::getDateFormat("SHORT", $arNewsItem["LID"]));

		// ���� � ������� ����� ������� "��������� ��������", �� ��������� � �� ������ "�������� ���������������" � �������� ������
		if ($arNewsItem["PROPERTIES"][self::IBLOCK_PROPERTY_SEND]["VALUE"] != "Y")
		{
			self::setSendProperty($itemId, $arNewsItem["IBLOCK_ID"], "N", self::getMessage("ELEMENT-DESCR-interrupted"));
			if ($bTmpUser)
			{
				unset($USER);
			}

			return "";
		}

		// ������� ���� �����������
		$rsTszhs = CTszh::getList(
			array(),
			array("@ID" => $arAgentParams["TSZH_IDS"]),
			false,
			false,
			array("ID", "CODE", "SITE_ID", "NAME")
		);
		$arTszhs = array();
		while ($arTszh = $rsTszhs->getNext())
		{
			$arTszhs[$arTszh["ID"]] = $arTszh;
		}
		$arAgentParams["TSZH_IDS"] = array_keys($arTszhs);

		// ���-�� ������������ ����� ������� �������
		$curEmailCount = 0;

		$lastEmail = $arAgentParams["LAST_EMAIL"];
		$maxEmailsCount = CTszhSubscribe::getOption("max_emails_per_hit");

		$rsAccounts = CTszhAccount::getList(
			array("USER_EMAIL" => "ASC"),
			array(
				"@TSZH_ID" => $arAgentParams["TSZH_IDS"],
				"!USER_EMAIL" => false,
				"!@USER_ID" => CTszhSubscribe::getUnsubscribedUsers(self::CODE),
				">USER_EMAIL" => $lastEmail,
			),
			false,
			false,
			array("*")
		);
		$arAccounts = array();
		while ($arAccount = $rsAccounts->GetNext())
		{
			if (!CTszhSubscribe::isDummyMail($arAccount["USER_EMAIL"], $arAccount["USER_SITE"]))
			{
				$arAccounts[$arAccount["USER_EMAIL"]] = $arAccount;
			}
		}
		unset($rsAccounts);

		foreach ($arAccounts as $email => $arAccount)
		{
			if (self::getGenericEmailCount() >= $maxEmailsCount)
			{
				if ($bTmpUser)
				{
					unset($USER);
				}
				$arAgentParams["LAST_EMAIL"] = $lastEmail;

				return CTszhSubscribe::getExecAgentString(__CLASS__, array(0, $itemId, $arAgentParams));
			}

			$arTszh = $arTszhs[$arAccount["TSZH_ID"]];

			// ��������� URL ��������� �������� �������
			$siteUrl = CTszhPublicHelper::getSiteUrl($arTszh["SITE_ID"]);
			if ($arAgentParams["IS_PORTAL"])
			{
				$arDetailPageUrl = explode("/", $arNewsItem["DETAIL_PAGE_URL"]);
				$detailPageUrl = $siteUrl . "org/{$arTszh["CODE"]}/news/" . array_pop($arDetailPageUrl);
			}
			else
			{
				$arSiteUrl = parse_url($siteUrl);
				$detailPageUrl = $arSiteUrl["scheme"] . "://" . $arSiteUrl["host"] . $arNewsItem["DETAIL_PAGE_URL"];
			}

			// ������� ��������� From, Sender � URL ������� �� ��������
			$from = $sender = $listUnsubscribe = true;
			CTszhSubscribe::getMailHeaders($arTszh["SITE_ID"], $from, $sender, $listUnsubscribe, $arAccount["USER_ID"], self::CODE);

			$arSendFields = array(
				"EMAIL_FROM" => $from,
				"HEADER_SENDER" => $sender,
				"EMAIL_TO" => $arAccount["USER_EMAIL"],
				"ACCOUNT_NAME" => $arAccount["NAME"],
				"ORG_NAME" => $arTszh["NAME"],
				"~ORG_NAME" => $arTszh["~NAME"],
				"NEWS_ITEM_DATE_ACTIVE_FROM" => convertTimeStamp($tsActiveFrom, "SHORT", $arTszh["SITE_ID"]),
				"NEWS_ITEM_NAME" => $arNewsItem["NAME"],
				"NEWS_ITEM_PREVIEW_TEXT" => $arNewsItem["PREVIEW_TEXT"],
				"NEWS_ITEM_DETAIL_PAGE_URL" => $detailPageUrl,
				"UNSUBSCRIBE_URL" => $listUnsubscribe,
			);
			CTszhSubscribe::send(self::EVENT_TYPE, $arTszh["SITE_ID"], $arSendFields, $arTszh["ID"]);

			$curEmailCount++;
			self::incGenericEmailCount();

			$lastEmail = $arAccount["USER_EMAIL"];
		}

		self::setSendProperty($itemId, $arNewsItem["IBLOCK_ID"], "N", self::getMessage("ELEMENT-DESCR-sent", array("#DATE#" => convertTimeStamp(false, "FULL", $arTszh["SITE_ID"]))));
		if ($bTmpUser)
		{
			unset($USER);
		}

		return "";
	}

	/**
	 * ��������� ����������� ����� externalExecAgent() ��������
	 *
	 * @param int $tszhId Id ���, �������� �������� �������� ������������ ��������
	 * @param int $itemId Id ������������ ��������
	 * @param array $arExecParams ������ ���������� ���������� ��������:
	 *        int IBLOCK_ID - Id ��������� �������
	 *        array TSZH_IDS - ������ Id ���, ������� ������� �����������
	 *
	 * @return void
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\InvalidOperationException
	 */
	public static function externalExec($tszhId, $itemId, array $arExecParams = null)
	{
		if (!CModule::includeModule("iblock"))
		{
			throw new Main\InvalidOperationException('Module "iblock" not found');
		}

		$tszhId = intval($tszhId);
		/*if ($tszhId <= 0)
		throw new Main\ArgumentException('Invalid $tszhId passed', '$tszhId');*/

		$itemId = intval($itemId);
		if ($itemId <= 0)
		{
			throw new Main\ArgumentException('Invalid $itemId passed', '$itemId');
		}

		$arExecParams["IBLOCK_ID"] = intval($arExecParams["IBLOCK_ID"]);
		if ($arExecParams["IBLOCK_ID"] <= 0)
		{
			throw new Main\ArgumentException('Invalid $arExecParams["IBLOCK_ID"] passed', '$arExecParams["IBLOCK_ID"]');
		}

		if (!is_array($arExecParams["TSZH_IDS"]))
		{
			throw new Main\ArgumentException('Invalid $arExecParams["TSZH_IDS"] passed', '$arExecParams["TSZH_IDS"]');
		}

		if (empty($arExecParams["TSZH_IDS"]))
		{
			throw new Main\ArgumentException('Empty $arExecParams["TSZH_IDS"] passed', '$arExecParams["TSZH_IDS"]');
		}

		foreach ($arExecParams["TSZH_IDS"] as $key => $id)
		{
			$id = intval($id);
			if ($id <= 0)
			{
				throw new Main\ArgumentException('Invalid element of $arExecParams["TSZH_IDS"] passed', '$arExecParams["TSZH_IDS"]' . "[$key]");
			}
		}

		// ��������: �� ������� �� ��� �����, ����������� ��� �� �������
		$rsAgents = CAgent::GetList(array(), array("MODULE_ID" => TSZH_MODULE_ID, "NAME" => __CLASS__ . "::externalExecAgent(0, {$itemId},%"));
		if ($arAgent = $rsAgents->fetch())
		{
			return;
		}

		$arAgentParams = array(
			"IBLOCK_ID" => $arExecParams["IBLOCK_ID"],
			"TSZH_IDS" => $arExecParams["TSZH_IDS"],
			"IS_PORTAL" => CModule::includeModule("vdgb.portaljkh"),
			"LAST_EMAIL" => "",
		);
		$agentID = CTszhSubscribe::addExecAgent(__CLASS__, array(0, $itemId, $arAgentParams));
		if ($agentID > 0)
		{
			$value = "Y";
			$msg = "ELEMENT-DESCR-sending";
		}
		else
		{
			$value = "N";
			$msg = "ELEMENT-ERROR-addAgent";
		}

		self::setSendProperty($itemId, $arExecParams["IBLOCK_ID"], $value, self::getMessage($msg));
	}

	/**
	 * ���������� ������� �������� ���. ���������� �� CTszhSubscribe::onAfterTszhDelete()
	 *
	 * @param int $tszhId Id ��������� ���
	 *
	 * @return void
	 */
	public static function onAfterTszhDelete($tszhId)
	{
		CTszhSubscribe::deleteExecAgent(__CLASS__, $tszhId);
	}
}
