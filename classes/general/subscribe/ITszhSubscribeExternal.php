<?

/**
 * ��������� ������� �������� (�������������� �� ������� �������� CTszhSubscribe::SubscribeAgent())
 */
interface ITszhSubscribeExternal
{
	/* ���������� ��� �������� ���������, ���������� ������� ������������� �������� �������� */
	const IBLOCK_PROPERTY_SEND = "citrusTszhSubscribeSend";

	/**
	 * ������������ ��������
	 *
	 * @param int $tszhId Id ���, �������� �������� �������� ������������ ��������
	 * @param int $itemId Id ������������ ��������
	 * @param array $arExecParams ������ ���������� ���������� ��������
	 *
	 * @return
	 */
	public static function externalExec($tszhId, $itemId, array $arExecParams = null);

	/**
	 * ���������� ������� �������� ���. ���������� �� CTszhSubscribe::onAfterTszhDelete()
	 *
	 * @param int $tszhId Id ��������� ���
	 *
	 * @return
	 */
	public static function onAfterTszhDelete($tszhId);
}