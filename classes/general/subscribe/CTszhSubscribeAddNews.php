<?

/**
 * ����� �������� ����������� � ������������ ������������ �������
 */
class CTszhSubscribeAddNews extends CTszhBaseSubscribe
{
	const CODE = "AddNews";
	const TYPE = self::TYPE_PRODUCT;
	const INTERVAL = 43200; // 30*24*60 == 30 ����
	const EVENT_TYPE = "TSZH_ADD_NEWS_NOTICE";
	const SORT = 9;

	protected static $arParams = array(
		"TSZH_CHECK_DATES" => array(
			"type" => "array",
			"default" => array(),
			"readOnly" => true,
		),
	);

	protected static function calcNextExec(array $arSubscribe = null)
	{
		if (!is_array($arSubscribe["PARAMS"]["TSZH_CHECK_DATES"]) || empty($arSubscribe["PARAMS"]["TSZH_CHECK_DATES"]))
			return strtotime("+" . self::INTERVAL . " minute");

		sort($arSubscribe["PARAMS"]["TSZH_CHECK_DATES"]);
		return $arSubscribe["PARAMS"]["TSZH_CHECK_DATES"][0];
	}

	public static function checkParams(array &$arParams, $subscribeId = 0, $useDefault = false, $throwKey = false)
	{
		if (is_set($arParams, "TSZH_CHECK_DATES"))
		{
			if (!is_array($arParams["TSZH_CHECK_DATES"]))
				$arParams["TSZH_CHECK_DATES"] = array();

			foreach ($arParams["TSZH_CHECK_DATES"] as $tszhId => $checkDatetime)
			{
				$arParams["TSZH_CHECK_DATES"][$tszhId] = intval($checkDatetime);
			}
		}

		if ($useDefault)
			parent::checkParams($arParams, $subscribeId, $useDefault);
	}

	/**
	 * ������������ �������� ����������� � ������������ ������������ �������
	 *
	 * @param array $arSubscribe ������ ����� ������ �������� (�� ������� b_tszh_subscribe)
	 * @param bool $nextExec false
	 * @return void
	 * @throws \Bitrix\Main\ArgumentException
	 * @throws \Bitrix\Main\Db\SqlQueryException
	 */
	public static function exec(array $arSubscribe, $nextExec = false)
	{
		try
		{
			self::checkParams($arSubscribe["PARAMS"], $arSubscribe["ID"], true);

			// ������� ��� ���
			$rsTszhs = CTszh::getList(
				array("ID" => "ASC"),
				array(),
				false,
				false,
				array("ID", "SITE_ID", "NAME")
			);
			$arTszhs = array();
			$arSiteFormats = array();
			while ($arTszh = $rsTszhs->getNext())
			{
				$arTszhs[$arTszh["ID"]] = $arTszh;
				$arSiteFormats[$arTszh["SITE_ID"]] = false;
			}
			if (empty($arTszhs))
			{
				$arSubscribe["PARAMS"]["TSZH_CHECK_DATES"] = array();
				throw new Exception("");
			}

			foreach ($arSubscribe["PARAMS"]["TSZH_CHECK_DATES"] as $tszhId => $checkDatetime)
			{
				if (!is_set($arTszhs, $tszhId))
					unset($arSubscribe["PARAMS"]["TSZH_CHECK_DATES"][$tszhId]);
			}

			$now = time();
			foreach ($arTszhs as $tszhId => $arTszh)
			{
				if (!is_set($arSubscribe["PARAMS"]["TSZH_CHECK_DATES"], $tszhId))
					$arSubscribe["PARAMS"]["TSZH_CHECK_DATES"][$tszhId] = $now;
			}

			foreach ($arTszhs as $tszhId => $arTszh)
			{
				if ($arSubscribe["PARAMS"]["TSZH_CHECK_DATES"][$tszhId] > $now)
					unset($arTszhs[$tszhId]);
			}
			if (empty($arTszhs))
				throw new Exception("");

			// ���� ������ ���������� �� ����������, ������� ��� �������� ��������� TSZH_CHECK_DATES � �������� ������
			if (!CModule::includeModule("iblock"))
			{
				foreach ($arSubscribe["PARAMS"]["TSZH_CHECK_DATES"] as $tszhId => $checkDatetime)
				{
					$arSubscribe["PARAMS"]["TSZH_CHECK_DATES"][$tszhId] = strtotime("+" . self::INTERVAL . " minute", $checkDatetime);
				}
				throw new Exception("");
			}

			// ��������� ��������� �������� ��� ������� ���
			$arTszhsNewsIBlocks = CTszhPublicHelper::getTszhsNewsIBlocks($arTszhs);
			$arIBlockIds = array();
			foreach ($arTszhs as $tszhId => $arTszh)
			{
				if (isset($arTszhsNewsIBlocks[$tszhId]))
				{
					$arTszhs[$tszhId]["_IBLOCK_ID"] = $arTszhsNewsIBlocks[$tszhId]["id"];
					$arTszhs[$tszhId]["_IBLOCK_TYPE"] = $arTszhsNewsIBlocks[$tszhId]["type"];
					$arIBlockIds[] = $arTszhsNewsIBlocks[$tszhId]["id"];
				}
				else
				{
					$arSubscribe["PARAMS"]["TSZH_CHECK_DATES"][$tszhId] = strtotime("+" . self::INTERVAL . " minute", $arSubscribe["PARAMS"]["TSZH_CHECK_DATES"][$tszhId]);
					unset($arTszhs[$tszhId]);
					//CTszhSubscribe::reportError("Could not find news IBlock for organization with ID=={$tszhId}");
				}
			}
			if (empty($arTszhs))
				throw new Exception("");

			// ��������� ������ ������� ����
			$rsSites = CSite::GetList($by="sort", $order="desc", array("LANGUAGE_ID" => array_keys($arSiteFormats)));
			while ($arSite = $rsSites->Fetch())
			{
				$arSiteFormats[$arSite["LID"]] = $arSite["FORMAT_DATETIME"];
			}
			foreach ($arTszhs as $tszhId => $arTszh)
			{
				$arTszhs[$tszhId]["_FULL_FORMAT"] = $arSiteFormats[$arTszh["SITE_ID"]];
			}

			// ������� � �������� ���, ������� �������, ����������� round(self::INTERVAL / (24*60)) ��� ����� ���� �����
			$arIBlockIds = array_unique($arIBlockIds);
			$rsLastNews = CIBlockElement::getList(
				array(),
				array(
					"IBLOCK_ID" => $arIBlockIds,
					">=DATE_CREATE" => convertTimeStamp(strtotime("-" . (round(self::INTERVAL / (24*60))) . " day"), "FULL")
				),
				false,
				false,
				array("ID", "IBLOCK_ID", "DATE_CREATE")
			);
			$arIBlockLastDates = array();
			while ($arNewsItem = $rsLastNews->fetch())
			{
				foreach ($arTszhs as $tszhId => $arTszh)
				{
					if ($arTszh["_IBLOCK_ID"] == $arNewsItem["IBLOCK_ID"])
					{
						$dt = makeTimeStamp($arNewsItem["DATE_CREATE"], $arTszh["_FULL_FORMAT"]);
						if ($dt > intval($arIBlockLastDates[$arNewsItem["IBLOCK_ID"]]))
							$arIBlockLastDates[$arNewsItem["IBLOCK_ID"]] = $dt;
						break;
					}
				}
			}
			unset($rsLastNews);
			$arIBlockIds = array_keys($arIBlockLastDates);
			foreach ($arTszhs as $tszhId => $arTszh)
			{
				if (in_array($arTszh["_IBLOCK_ID"], $arIBlockIds))
				{
					$arSubscribe["PARAMS"]["TSZH_CHECK_DATES"][$tszhId] = strtotime("+" . self::INTERVAL . " minute 1 second", $arIBlockLastDates[$arTszh["_IBLOCK_ID"]]);
					unset($arTszhs[$tszhId]);
				}
			}
			if (empty($arTszhs))
				throw new Exception("");

			// ��������� ����������� ��������
			$arAdminEmails = CTszhSubscribe::getSubscribedAdminEmails(self::CODE);
			if (empty($arAdminEmails))
				throw new Exception("");

			$curEmailCount = 0;
			foreach ($arTszhs as $tszhId => $arTszh)
			{
				// ������� URL � ������������ ����� ���
				CTszhPublicHelper::getOrgUrlAndSiteName($arTszh, $orgUrl, $orgSiteName);
				// ������� URL ���������� �������
				$addNewsItemUrl = ($GLOBALS["APPLICATION"]->isHttps() ? "https" : "http") . "://{$_SERVER["HTTP_HOST"]}/bitrix/admin/iblock_element_edit.php?IBLOCK_ID={$arTszh["_IBLOCK_ID"]}&type={$arTszh["_IBLOCK_TYPE"]}&ID=0&lang=" . LANGUAGE_ID . "&IBLOCK_SECTION_ID=0&find_section_section=0&from=iblock_list_admin";

				foreach ($arAdminEmails as $userId => $email)
				{
					// ������� �������� ��������� From, Sender � List-Unsubscribe
					$from = $sender = $listUnsubscribe = true;
					CTszhSubscribe::getMailHeaders($arTszh["SITE_ID"], $from, $sender, $listUnsubscribe, $userId, self::CODE);

					$arSendFields = array(
						"EMAIL_FROM" => $from,
						"HEADER_SENDER" => $sender,
						"EMAIL_TO" => $email,
						"ORG_NAME" => $arTszh["NAME"],
						"~ORG_NAME" => $arTszh["~NAME"],
						"ORG_SITE_NAME" => $orgSiteName,
						"ORG_URL" => $orgUrl,
						"ADD_NEWS_ITEM_URL" => $addNewsItemUrl,
						"VDGB_CONTACTS_HTML" => CTszhSubscribe::getVdgbContactsHtml(),
						"UNSUBSCRIBE_URL" => $listUnsubscribe,
					);
					CTszhSubscribe::send(self::EVENT_TYPE, $arTszh["SITE_ID"], $arSendFields, $arTszh["ID"]);

					$curEmailCount++;
					self::incGenericEmailCount();
				}

				$arSubscribe["PARAMS"]["TSZH_CHECK_DATES"][$tszhId] = strtotime("+" . self::INTERVAL . " minute");
			}
		}
		catch (Exception $e)
		{
		}

		$arNewSubscribe = array(
			"ID" => $arSubscribe["ID"],
			"TSZH_ID" => null,
			"PARAMS" => $arSubscribe["PARAMS"],
		);
		parent::exec($arNewSubscribe, self::calcNextExec($arSubscribe));
	}
}