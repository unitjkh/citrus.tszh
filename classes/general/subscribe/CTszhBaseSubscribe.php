<?
use Bitrix\Main;
use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
use Citrus\Tszh\Subscribe\CTszhSubscribeTable;

Loc::loadMessages(__FILE__);

/**
 * ������� ����� ��������
 */
abstract class CTszhBaseSubscribe
{
	// ���� ��������
	/* �������: ��������, �������������� �� ������� �������� CTszhSubscribe::SubscribeAgent() (�� �������������) */
	const TYPE_EXTERNAL	= 1; //0b0001
	/* �������������� (�������������) */
	const TYPE_OPTIONAL	= 2; //0b0010
	/* ������������ (�� �������������) */
	const TYPE_REQUIRED	= 4; //0b0100
	/* �������� �������� (������������, �� �������������) */
	const TYPE_PRODUCT	= 8; //0b1000
	/* ��������� ���������: ��� ���� */
	const TYPE_ALL		= 15; //0b1111


	/* ���������� ��� */
	const CODE = "";
	/* ��� */
	const TYPE = self::TYPE_OPTIONAL;
	/* ������������� (� �������) �������� */
	const INTERVAL = 0;
	/* ��� ��������� ������� */
	const EVENT_TYPE = "";
	/* ���������� */
	const SORT = 0;

	/* ��� ������� �������� */
	const EXEC_HOUR = 2;
	/* ������ ������� �������� */
	const EXEC_MINUTE = 0;

	/** @var int ���-�� ������������ e-mail'�� �� ������� ��� ����� ���������� */
	private static $genericEmailCount = 0;

	/** @var array ������ ���������� �������� */
	protected static $arParams = array();

	/**
	 * ���������� ����� (Unix timestamp) ���������� ������� ��������
	 *
 	 * @param array|null $arSubscribe ������ ����� ������ ��������
 	 * @return int
	 */
	protected static function calcNextExec(array $arSubscribe = null)
	{
		return 0;
	}

	/**
	 * ���������� �� ������� ����� �������� ����� �� � ����������� ����
	 *
	 * @param string $messageCode ���������� ��� �����
	 * @param array $arParams ������ ������������� ����������
 	 * @return string
	 */
	final public static function getMessage($messageCode, array $arParams = null)
	{
		return Loc::getMessage(static::CODE . "-" . $messageCode, $arParams);
	}

	/**
	 * ���������� ���� ��������� ��� ������� �������� ��������
	 *
	 * @param string $code ��� ���������
 	 * @return string
	 */
	final public static function getSettingKey($code)
	{
		if ($code == "ACTIVE")
			return "tszh_subscribe_" . static::CODE . "_ACTIVE";
		else
			return "tszh_subscribe_" . static::CODE . "_param_" . $code;
	}

	/**
	 * ���������� ���-�� ������������ e-mail'�� �� ������� ��� ����� ����������
	 *
 	 * @return int
	 */
	final public static function getGenericEmailCount()
	{
		return self::$genericEmailCount;
	}

	/**
	 * �������������� ���-�� ������������ e-mail'�� �� ������� ��� ����� ����������
	 *
 	 * @return void
	 */
	final public static function incGenericEmailCount()
	{
		self::$genericEmailCount++;
	}

	/**
	 * ��������� (��� $useDefault == true) ���������� �� ������ ������ ���������� �������� �������������� � ��� ����������� � ���������� ����������
	 * � �������-�������� ������ ������������ ��������� ��������
	 *
	 * @param array $arParams ������ ���������� �������� (paramCode => value)
 	 * @param int $subscribeId Id ������ ��������
	 * @param bool $useDefault ����: ������������ �������� ���������� �������� �� ���������� ����������
	 * @param bool $throwKey ����: � ���������� Main\ArgumentException ���������� ���� (key) ��������� ������ ��� ����
 	 * @return void
	 */
	public static function checkParams(array &$arParams, $subscribeId = 0, $useDefault = false, $throwKey = false)
	{
		if (!$useDefault)
			return;

		foreach (static::$arParams as $code => $arParam)
		{
			if ($arParam["readOnly"])
				continue;

			if (!is_set($arParams, $code))
				$arParams[$code] = $arParam["default"];
		}
	}

	/**
	 * ���������� ������ �������� ��������
	 *
	 * @param int|bool $tszhId Id ��� (���� ������, �� � ������ ��������� ����� ���� value, ���������� �������� ��� ����� ���)
	 * @param bool $valuesOnly ����: ���������� ������ �������� �������� (��� ���������� ���; � ����� code => value)
	 * @param bool $useDefaults ����: ���������� ��������� �������� ����������, ���� �� �������� �������� �� ������
 	 * @param array|null $arSubscribe ������ ����� ������ ��������
 	 * @return array
	 */
	public static function getSettings($tszhId = false, $valuesOnly = false, $useDefaults = true, array $arSubscribe = null)
	{
		$arResult = array(
			"ACTIVE" => array(
				"type" => "checkbox",
				"value" => null,
			),
			"PARAMS" => static::$arParams,
			"LAST_EXEC" => array (
				"readOnly" => true,
			),
			"NEXT_EXEC" => array (
				"readOnly" => true,
			)
		);

		$tszhId = intval($tszhId);
		if ($tszhId > 0)
		{
			if ($arSubscribe === null)
			{
				$arSubscribe = CTszhSubscribeTable::getRow(array(
					"filter" => array("TSZH_ID" => $tszhId, "CODE" => static::CODE),
				));
			}

			if (is_array($arSubscribe) && !empty($arSubscribe))
			{
				foreach ($arResult as $code => $arSetting)
				{
					if (!is_set($arSubscribe, $code))
						continue;

					if ($code == "PARAMS")
					{
						foreach ($arResult["PARAMS"] as $paramCode => $arParam)
						{
							if (is_set($arSubscribe["PARAMS"], $paramCode))
								$arResult["PARAMS"][$paramCode]["value"] = $arSubscribe["PARAMS"][$paramCode];
						}
					}
					else
					{
						$arResult[$code]["value"] = $arSubscribe[$code];
					}
				}
			}
		}

		foreach ($arResult as $code => $arSetting)
		{
			if ($code == "PARAMS")
			{
				foreach ($arResult["PARAMS"] as $paramCode => $arParam)
				{
					if ($useDefaults && !is_set($arParam, "value"))
					{
						$arParam["value"] = $arParam["default"];
						$arResult["PARAMS"][$paramCode]["value"] = $arParam["default"];
					}
		            
		            if ($valuesOnly)
						$arResult["PARAMS"][$paramCode] = $arParam["value"];
					else
					{
						$arResult["PARAMS"][$paramCode]["name"] = static::getMessage("PARAM-{$paramCode}-NAME");
						$arResult["PARAMS"][$paramCode]["key"] = static::getSettingKey($paramCode);

						if (is_set($arResult["PARAMS"][$paramCode], "value"))
						{
							$arResult["PARAMS"][$paramCode]["~value"] = $arResult["PARAMS"][$paramCode]["value"];
							if ($paramCode == "LAST_PERIOD" && strlen($arResult["PARAMS"][$paramCode]["value"]))
								$arResult["PARAMS"][$paramCode]["value"] = CTszhPeriod::Format($arResult["PARAMS"][$paramCode]["value"]);
						}
					}
				}
			}
			else
			{
				if ($valuesOnly)
				{
					$arResult[$code] = $arSetting["value"];
				}
				else
				{
					$arResult[$code]["name"] = static::getMessage("SETTING-{$code}-NAME");
					$arResult[$code]["name-wizard"] = static::getMessage("SETTING-{$code}-NAME-WIZARD");
					$arResult[$code]["key"] = static::getSettingKey($code);
				}
			}
		}

		return $arResult;
	}

	/**
	 * ���������� ������ �������� ��������, ���������� �� POST-������� ��� �� ������� ��������� �����
	 *
	 * @param bool $useDefaultSettings ����: ��� ������������ ��������� �������� (� ����� ��� �� ����������) ������������ ��������� ��������
 	 * @param CWizardBase $wizard ������ ������� ��������� �����
 	 * @return array
	 */
	final public static function collectSettings($useDefaultSettings = true, CWizardBase $wizard = null)
	{
	    if ($wizard === null && $_SERVER["REQUEST_METHOD"] != "POST")
	    	return array("PARAMS" => array());

		$arSettings = static::getSettings(false, false, $useDefaultSettings);
		foreach ($arSettings as $code => $arSetting)
		{
			if ($code == "PARAMS")
			{
				foreach ($arSettings["PARAMS"] as $paramCode => $arParam)
				{
					$key = $arParam["key"];
		    
					if ($arParam["readOnly"])
						unset($arSettings["PARAMS"][$paramCode]);
					elseif ($wizard === null && is_set($_POST, $key) || $wizard !== null && $wizard->getVar($key, true) !== null)
					{
						if ($wizard === null)
							$arSettings["PARAMS"][$paramCode] = $_POST[$key];
						else
							$arSettings["PARAMS"][$paramCode] = $wizard->getVar($key, true);
					}
					elseif ($wizard === null && $arSettings["PARAMS"][$paramCode]["type"] == "checkbox")
						$arSettings["PARAMS"][$paramCode] = "N";
					else
						unset($arSettings["PARAMS"][$paramCode]);
				}
			}
			else
			{
				$key = $arSetting["key"];
				if ($wizard === null && is_set($_POST, $key) || $wizard !== null && $wizard->getVar($key, true) !== null)
				{
					if ($wizard === null)
						$arSettings[$code] = $_POST[$key];
					else
						$arSettings[$code] = $wizard->getVar($key, true);
				}
				elseif ($wizard === null && $arSetting["type"] == "checkbox")
					$arSettings[$code] = "N";
				else
					unset($arSettings[$code]);
			}
		}

		if ($useDefaultSettings)
			static::checkParams($arSettings["PARAMS"], 0, $useDefaultSettings);

		return $arSettings;
	}

	/**
	 * ������������� (��������� � ������� b_tszh_subscribe) ��������� ��������
	 *
	 * @param int $subscribeId Id ������ ��������
	 * @param array $arSettings ������ ��������������� �������� (code => array)
 	 * @return \Bitrix\Main\Entity\UpdateResult | false
	 */
	public static function saveSettings($subscribeId, array $arSettings)
	{
		$subscribeId = intval($subscribeId);
		if ($subscribeId <= 0)
			return false;

		if (!is_array($arSettings) || empty($arSettings))
			return false;

		foreach ($arSettings as $code => $arSetting)
		{
			if ($code == "PARAMS")
			{
				foreach ($arSetting as $paramCode => $arParam)
				{
					if ($arParam["readOnly"])
					{
						if (strlen($arParam["~value"]))
							$arSettings["PARAMS"][$paramCode] = $arParam["~value"];
						else
							unset($arSettings["PARAMS"][$paramCode]);
					}
					else
						$arSettings["PARAMS"][$paramCode] = $arParam["value"];
				}
			}
			else
				$arSettings[$code] = $arSetting["value"];
		}

		return CTszhSubscribeTable::update($subscribeId, $arSettings);
	}

	/**
	 * ���������� ������� onGetSubscribeClasses. ���������� ���������� ��� ��������
	 *
 	 * @return string
	 */
/*	final public static function onGetSubscribeClasses()
	{ 
		return static::CODE;
	}*/

	/**
	 * ��������� (������������� LAST_EXEC, ������� RUNNING �, ���� ������, ������������� ACTIVE � ��������� PARAMS) ������ �������� (� ������� b_tszh_subscribe) ����� � ���������� � �������-��������
 	 * � �������-�������� ������ ��������� ��������
	 *
	 * @param array $arSubscribe ������ ����� ������ �������� (�� ������� b_tszh_subscribe)
	 * @param string|bool $nextExec ����-����� ���������� �������
	 * @return void
	 * @throws \Bitrix\Main\ArgumentException
 	 * @throws \Bitrix\Main\Db\SqlQueryException
	 */
	public static function exec(array $arSubscribe, $nextExec = false)
	{
		if (!isset($arSubscribe["ID"]) || intval($arSubscribe["ID"]) <= 0)
			throw new Main\ArgumentException('Invalid $arSubscribe["ID"] passed', '$arSubscribe["ID"]');

		$arUpdateSubscribe = array(
			"LAST_EXEC" => new \Bitrix\Main\Type\DateTime(convertTimeStamp(false, "FULL")), 
			"RUNNING" => "N"
		);

		if ($nextExec !== false)
		{
			$nextExec = intval($nextExec);
			if ($nextExec <= 0)
				throw new Main\ArgumentException('Invalid $nextExec passed', '$nextExec');

			$arUpdateSubscribe["NEXT_EXEC"] = new \Bitrix\Main\Type\DateTime(convertTimeStamp($nextExec, "FULL"));
		}

		if (isset($arSubscribe["PARAMS"]) && is_array($arSubscribe["PARAMS"]) && !empty($arSubscribe["PARAMS"]))
		{
			static::checkParams($arSubscribe["PARAMS"], $arSubscribe["ID"]);
			$arOldFields = CTszhSubscribeTable::getRowById($arSubscribe["ID"]);
			if (!is_array($arOldFields["PARAMS"]))
				$arOldFields["PARAMS"] = array();
			$arUpdateSubscribe["PARAMS"] = array_merge($arOldFields["PARAMS"], $arSubscribe["PARAMS"]);
		}

		if (isset($arSubscribe["ACTIVE"]) && in_array($arSubscribe["ACTIVE"], array("Y", "N")))
			$arUpdateSubscribe["ACTIVE"] = $arSubscribe["ACTIVE"];

		$res = CTszhSubscribeTable::update($arSubscribe["ID"], $arUpdateSubscribe);
	}

	/**
	 * ���������� ������� ��������� ������ ��������. ���������� �� CTszhSubscribeTable::onBeforeUpdate()
	 *
	 * @param Entity\Event $event ������ �������
	 * @param array $arOldSubscribe ������ ����� ������������ ������ ��������
  	 * @return void
	 */
	public static function onBeforeUpdate(Entity\Event $event, array $arOldSubscribe)
	{ 
		try
		{
			$className = get_called_class();
			if (!((new $className) instanceof ITszhSubscribeAgent))
				return;

			$arParameters = $event->getParameters();
			$arFields = $event->mergeFields($arParameters["fields"]);

			if (is_set($arFields, "ACTIVE") && $arFields["ACTIVE"] != "Y")
				CTszhSubscribe::deleteExecAgent($className, $arParameters["id"]["ID"]);
		}
		catch (Exception $e)
		{
		}
	}

	/**
	 * ���������� ������� �������� ������ ��������. ���������� �� CTszhSubscribeTable::onBeforeDelete()
	 *
 	 * @param Entity\Event $event ������ �������
	 * @param array $arOldSubscribe ������ ����� ������������ ������ ��������
  	 * @return void
	 */
	public static function onBeforeDelete(Entity\Event $event, array $arOldSubscribe)
	{ 
		try
		{
			$className = get_called_class();
			if (!((new $className) instanceof ITszhSubscribeAgent))
				return;

			$arParameters = $event->getParameters();

			CTszhSubscribe::deleteExecAgent($className, $arParameters["id"]["ID"]);
		}
		catch (Exception $e)
		{
		}
	}
}

/*foreach (get_declared_classes() as $className)
{
	if (CTszhSubscribe::checkSubscribeClass($className))
		addEventHandler(TSZH_MODULE_ID, CTszhSubscribe::EVENT_GET_SUBSCRIBE_CLASSES, Array($className, CTszhSubscribe::EVENT_GET_SUBSCRIBE_CLASSES), $className::SORT);
}*/
