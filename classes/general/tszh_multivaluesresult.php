<?php
require_once ($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/interface/admin_lib.php");

/**
 * ��������������� ����� ��� ������� ��������� ����� �� ������ ������
 */
class CTszhMultiValuesResult extends CDBResult
{
	private $arMultiFields = array();
	protected static $separator = '|';

	/**
	 * @param CDBResult $dbResult
	 * @param array $arGetListFields
	 */
	function CTszhMultiValuesResult($dbResult, &$arGetListFields)
	{
		foreach ($arGetListFields as $fieldName => $arField)
		{
			if (isset($arField["BINDING_TABLE"]) || isset($arField["FIELD_SELECT"]))
				$this->arMultiFields[$fieldName] = $arField["TYPE"];
		}
		parent::CDBResult($dbResult);
	}

	/**
	 * @param array $arResult
	 * @param array $arMultiFields
	 * @return array|false
	 */
	public static function processMultiFields($arResult, $arMultiFields)
	{
		if ($arResult && count(array_intersect_key($arMultiFields, $arResult)) > 0)
		{
			foreach ($arMultiFields as $field => $type)
			{
				if (array_key_exists($field, $arResult))
				{
					$arValues = array();
					if (isset($arResult[$field]) && $arResult[$field])
					{
						$arValues = explode(static::$separator, $arResult[$field]);
						if (!in_array($type, array("string", "char")))
						{
							foreach ($arValues as $key => $value)
							{
								switch ($type)
								{
									case "int":
										$arValues[$key] = intval($value);
										break;

									case "double":
										$arValues[$key] = floatval($value);
										break;
								}
							}
						}
					}
					if (isset($arResult['~' . $field]) && $arResult['~' . $field])
						$arResult['~' . $field] = explode(static::$separator, $arResult['~' . $field]);
					$arResult[$field] = $arValues;
				}
			}
		}
		return $arResult;
	}

	/**
	 * @param bool $bTextHtmlAuto
	 * @param bool $use_tilda
	 * @return array|false
	 */
	public function GetNext($bTextHtmlAuto=true, $use_tilda=true)
	{
		$arResult = parent::GetNext($bTextHtmlAuto, $use_tilda);
		return self::processMultiFields($arResult, $this->arMultiFields);
	}

	/**
	 * @param bool $bSetGlobalVars
	 * @param string $strPrefix
	 * @param bool $bDoEncode
	 * @param bool $bSkipEntities
	 * @return array|false
	 */
	public function NavNext($bSetGlobalVars=true, $strPrefix="str_", $bDoEncode=true, $bSkipEntities=true)
	{
		$arResult = parent::NavNext($bSetGlobalVars, $strPrefix, $bDoEncode, $bSkipEntities);
		return self::processMultiFields($arResult, $this->arMultiFields);
	}
}