<?php

/**
 * ���������� �������������� ��������� ����������� �������� ��������� ���������
 *
 * �������� ���������� ������ ����� ������� ��� �������� ��������� � ������� � ����������� ���� DEC_PLACES (���-�� ������ ����� ������� ��� ��������)
 */
class CTszhMeterValueResult extends CTszhMultiValuesResult
{
	/**
	 * @param array $arResult
	 *
	 * @return array
	 */
	public static function processMeterPrecision($arResult)
	{
		if ($arResult && count(array_intersect_key($arResult, Array(
				"VALUE1" => 1,
				"VALUE2" => 1,
				"VALUE3" => 1,
				"AMOUNT1" => 1,
				"AMOUNT2" => 1,
				"AMOUNT3" => 1
			))) > 0
		)
		{
			// $decimalPlaces = is_set($arResult, "DEC_PLACES") ? $arResult["DEC_PLACES"] : 2;
			for ($i = 1; $i <= 3; $i++)
			{
				if (array_key_exists("VALUE{$i}", $arResult))
				{
					// $arResult["VALUE{$i}"] = number_format($arResult["VALUE{$i}"], $decimalPlaces, ".", "");
					$arResult["VALUE{$i}"] = (float)$arResult["VALUE{$i}"];
				}
				if (array_key_exists("AMOUNT{$i}", $arResult) && strlen($arResult["AMOUNT{$i}"]) > 0)
				{
					// $arResult["AMOUNT{$i}"] = number_format($arResult["AMOUNT{$i}"], $decimalPlaces, ".", "");
					$arResult["AMOUNT{$i}"] = (float)$arResult["AMOUNT{$i}"];
				}
			}
		}

		return $arResult;
	}

	/**
	 * @param bool $bTextHtmlAuto
	 * @param bool $use_tilda
	 *
	 * @return array
	 */
	public function GetNext($bTextHtmlAuto = true, $use_tilda = true)
	{
		$arResult = parent::GetNext($bTextHtmlAuto, $use_tilda);

		return self::processMeterPrecision($arResult);
	}

	/**
	 * @param bool $bSetGlobalVars
	 * @param string $strPrefix
	 * @param bool $bDoEncode
	 * @param bool $bSkipEntities
	 *
	 * @return array
	 */
	public function NavNext($bSetGlobalVars = true, $strPrefix = "str_", $bDoEncode = true, $bSkipEntities = true)
	{
		$arResult = parent::NavNext($bSetGlobalVars, $strPrefix, $bDoEncode, $bSkipEntities);

		return self::processMeterPrecision($arResult);
	}
}