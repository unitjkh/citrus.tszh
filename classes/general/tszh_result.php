<?php
/**
 * ���������� �������������� ��������� ����������� ������� �������� ����������
 */
class CTszhResult extends CDBResult
{
	/**
	 * @param array $arResult
	 *
	 * @return array
	 */
	private static function processOfficeHoursField($arResult)
	{
		if ($arResult && is_set($arResult, "OFFICE_HOURS"))
		{
			$value = htmlspecialcharsBack($arResult["OFFICE_HOURS"]);
			$arResult["OFFICE_HOURS"] = @unserialize($value);
		}

		return $arResult;
	}

	/**
	 * @param bool $bTextHtmlAuto
	 * @param bool $use_tilda
	 *
	 * @return array|false
	 */
	public function GetNext($bTextHtmlAuto = true, $use_tilda = true)
	{
		$arResult = parent::GetNext($bTextHtmlAuto, $use_tilda);

		return self::processOfficeHoursField($arResult);
	}

	/**
	 * @param bool $bSetGlobalVars
	 * @param string $strPrefix
	 * @param bool $bDoEncode
	 * @param bool $bSkipEntities
	 *
	 * @return array|false
	 */
	public function NavNext($bSetGlobalVars = true, $strPrefix = "str_", $bDoEncode = true, $bSkipEntities = true)
	{
		$arResult = parent::NavNext($bSetGlobalVars, $strPrefix, $bDoEncode, $bSkipEntities);

		return self::processOfficeHoursField($arResult);
	}
}