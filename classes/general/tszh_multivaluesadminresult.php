<?php
require_once ($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/interface/admin_lib.php");

/**
 * Class CTszhMultiValuesAdminResult
 */
class CTszhMultiValuesAdminResult extends CAdminResult
{
	var $arMultiFields = array();

	/**
	 * @param CDBResult $res
	 * @param string $table_id
	 * @param array $arGetListFields
	 */
	function CTszhMultiValuesAdminResult($res, $table_id, &$arGetListFields)
	{
		foreach ($arGetListFields as $fieldName => $arField)
		{
			if (isset($arField["BINDING_TABLE"]) || isset($arField["FIELD_SELECT"]))
				$this->arMultiFields[$fieldName] = $arField["TYPE"];
		}
		parent::CAdminResult($res, $table_id);
	}

	/**
	 * @param bool $bTextHtmlAuto
	 * @param bool $use_tilda
	 * @return array|false
	 */
	public function GetNext($bTextHtmlAuto=true, $use_tilda=true)
	{
		$arResult = parent::GetNext($bTextHtmlAuto, $use_tilda);
		return CTszhMultiValuesResult::processMultiFields($arResult, $this->arMultiFields);
	}

	/**
	 * @param bool $bSetGlobalVars
	 * @param string $strPrefix
	 * @param bool $bDoEncode
	 * @param bool $bSkipEntities
	 * @return array|false
	 */
	public function NavNext($bSetGlobalVars=true, $strPrefix="str_", $bDoEncode=true, $bSkipEntities=true)
	{
		$arResult = parent::NavNext($bSetGlobalVars, $strPrefix, $bDoEncode, $bSkipEntities);
		return CTszhMultiValuesResult::processMultiFields($arResult, $this->arMultiFields);
	}
}
