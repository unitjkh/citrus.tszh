<?php

/**
 * ���������� �������������� ��������� ����������� �������� ��������� ���������
 */
class CTszhMeterValueAdminResult extends CTszhMultiValuesAdminResult
{
	/**
	 * @param bool $bTextHtmlAuto
	 * @param bool $use_tilda
	 *
	 * @return array
	 */
	public function GetNext($bTextHtmlAuto = true, $use_tilda = true)
	{
		$arResult = parent::GetNext($bTextHtmlAuto, $use_tilda);

		return CTszhMeterValueResult::processMeterPrecision($arResult);
	}

	/**
	 * @param bool $bSetGlobalVars
	 * @param string $strPrefix
	 * @param bool $bDoEncode
	 * @param bool $bSkipEntities
	 *
	 * @return array
	 */
	public function NavNext($bSetGlobalVars = true, $strPrefix = "str_", $bDoEncode = true, $bSkipEntities = true)
	{
		$arResult = parent::NavNext($bSetGlobalVars, $strPrefix, $bDoEncode, $bSkipEntities);

		return CTszhMeterValueResult::processMeterPrecision($arResult);
	}
}
