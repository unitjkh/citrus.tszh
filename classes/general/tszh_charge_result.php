<?php

IncludeModuleLangFile(__FILE__);

/**
 * ��������� �������������� ��������� ����������� ������� ����������
 */
class CTszhChargeResult extends CTszhMultiValuesResult
{
	/**
	 * @param CDBResult $dbResult
	 * @param array $arGetListFields
	 */
	function __construct($dbResult, &$arGetListFields)
	{
		parent::CTszhMultiValuesResult($dbResult, $arGetListFields);
	}

	/**
	 * ���������� ����������������� �����
	 * @param float $number �����
	 * @param bool $bCanBeEmpty ���� == true, ������ �� ����� ���������� � ������ ������� �������� (0, false, ������ ������ � �.�.)
	 * @param int $decimals ���������� ���� ����� �������. ���� ������� �������� ������ ���� � ����� ������� ��� �� �������� ���� ����� �������
	 * @return string
	 */
	protected static function formatNum($number, $bCanBeEmpty = false, $decimals = 2)
	{
		if (is_null($number))
			$result = '';
		elseif ($decimals < 0)
			$result = str_replace('.', ',', (float)$number);
		else
			$result = $number == 0 && $bCanBeEmpty ? '' : number_format($number, $decimals , ',', ' ');

		return $result;
	}

	/**
	 * ��������� ����-��������� �����
	 * @param array|bool $arResult ������ � ������ ����������
	 */
	protected function processResult(&$arResult)
	{
		if (!is_array($arResult))
		{
			return;
		}

		/**
		 * ��� �������� �������������, ������� ����� �� ����� ������, ��� �� �������� ������
		 */
		if (!$arResult["RATE"] && isset($arResult["X_FIELDS"]) && isset($arResult['SERVICE_TARIFF']))
		{
			$xFields = explode(',', $arResult["X_FIELDS"]);
			if (in_array("TARIFF", $xFields))
			{
				$arResult["RATE"] = 'X';
			}
			else
			{
				$arResult["RATE"] = self::formatNum($arResult["SERVICE_TARIFF"], false, -1);
			}
		}
		// � ������ 4-� ������ SERVICE_TARIFF �� �����������, ��� ������ �������� ��������� ������� ��� �� ���� RATE
		elseif (!isset($arResult['SERVICE_TARIFF']) && isset($arResult['RATE']))
		{
			$arResult['SERVICE_TARIFF'] = str_replace(',', '.', $arResult['RATE']);
		}

		if (is_set($arResult, "GROUP") && in_array($arResult["GROUP"], array(1, 2, 3)))
		{
			$arResult["GROUP"] = GetMessage("CITRUS_TSZH_CHARGE_GROUP_" . $arResult["GROUP"]);
		}
	}

	/**
	 * @param bool $bTextHtmlAuto
	 * @param bool $use_tilda
	 * @return array|false
	 */
	public function GetNext($bTextHtmlAuto = true, $use_tilda = true)
	{
		$arResult = parent::GetNext($bTextHtmlAuto, $use_tilda);
		$this->processResult($arResult);

		return $arResult;
	}

	/**
	 * @param bool $bSetGlobalVars
	 * @param string $strPrefix
	 * @param bool $bDoEncode
	 * @param bool $bSkipEntities
	 * @return array|false
	 */
	public function NavNext($bSetGlobalVars = true, $strPrefix = "str_", $bDoEncode = true, $bSkipEntities = true)
	{
		$arResult = parent::NavNext($bSetGlobalVars, $strPrefix, $bDoEncode, $bSkipEntities);
		$this->processResult($arResult);

		return $arResult;
	}
}