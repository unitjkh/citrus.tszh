<?php

IncludeModuleLangFile(__FILE__);

use Citrus\Tszh\HouseTable;

$GLOBALS["TSZH_ACCOUNT"] = Array();

/**
 * ������������ ���������� �� ������ � �������� �������
 *
 * ����
 * ----
 * ID                        - ID �������� �����
 * USER_ID                    - ID ������������ �������� ����� �� �����
 * TSZH_ID                    - ID ��� �������� �����
 * XML_ID                    - ����� �������� �����
 * EXTERNAL_ID                - ������� ��� �������� ����� (�� 1�)
 * NAME                        - ��� ���������
 * CITY                        - �����
 * DISTRICT                    - �����?
 * REGION                    - ������
 * SETTLEMENT                - ���������� �����
 * STREET                    - �����
 * HOUSE                    - ���
 * FLAT                        - ��������
 * FLAT_ABBR                - ����� �������� � ������������ (���� �� �������, ����� �������������� ���. <FLAT>�)
 * FLAT_TYPE                - ��� �������� (��������, �������)
 * AREA                        - ����� �������
 * LIVING_AREA                - ����� �������
 * HOUSE_AREA                - ����� ������� ������
 * HOUSE_ROOMS_AREA            - ������� ��������� ������
 * HOUSE_COMMON_PLACES_AREA    - ������� ���� ������ �����������
 * PEOPLE                    - ���-�� ����������� �����
 * REGISTERED_PEOPLE        - ���-�� ������������������ �����
 * EXEMPT_PEOPLE            - ���-�� ����������
 *
 * @todo ��������������� ����
 */
class CTszhAccount
{

	/**
	 * ��� ���������� �������� ������������� ������������� ��� ���� ����������� �������.
	 * �������� �� ��, ��� ��� �� ������ �������, ����� ��������� ��������� ������ � ���������� ����� ������� ��� �������� �� � ������ Add() � Update()
	 */
	protected static $houseFields = array(
		"CITY",
		"DISTRICT",
		"REGION",
		"SETTLEMENT",
		"STREET",
		"HOUSE",
		"HOUSE_AREA",
		"HOUSE_ROOMS_AREA",
		"HOUSE_COMMON_PLACES_AREA",
	);

	protected static $entityFields = array(
		"ID" => Array("FIELD" => "UA.ID", "TYPE" => "int"),

		"TYPE" => Array("FIELD" => "UA.TYPE", "TYPE" => "int"),
		"CURRENT" => Array("FIELD" => "UA.CURRENT", "TYPE" => "char"),
		"USER_ID" => Array("FIELD" => "UA.USER_ID", "TYPE" => "int"),
		"ACTIVE" => Array("FIELD" => "UA.ACTIVE", "TYPE" => "int"),
		"TSZH_ID" => Array("FIELD" => "UA.TSZH_ID", "TYPE" => "int"),
		"XML_ID" => Array("FIELD" => "UA.XML_ID", "TYPE" => "string"),
		"EXTERNAL_ID" => Array("FIELD" => "UA.EXTERNAL_ID", "TYPE" => "string"),
		"NAME" => Array("FIELD" => "UA.NAME", "TYPE" => "string"),
		"HOUSE_ID" => Array("FIELD" => "UA.HOUSE_ID", "TYPE" => "int"),
		"FLAT" => Array("FIELD" => "UA.FLAT", "TYPE" => "string"),
		"FLAT_ABBR" => Array("FIELD" => "UA.FLAT_ABBR", "TYPE" => "string"),
		"FLAT_TYPE" => Array("FIELD" => "UA.FLAT_TYPE", "TYPE" => "string"),
		"AREA" => Array("FIELD" => "UA.AREA", "TYPE" => "double"),
		"LIVING_AREA" => Array("FIELD" => "UA.LIVING_AREA", "TYPE" => "double"),
		"PEOPLE" => Array("FIELD" => "UA.PEOPLE", "TYPE" => "int"),
		"REGISTERED_PEOPLE" => Array("FIELD" => "UA.REGISTERED_PEOPLE", "TYPE" => "int"),
		"EXEMPT_PEOPLE" => Array("FIELD" => "UA.EXEMPT_PEOPLE", "TYPE" => "int"),

		// b_houses
		"CITY" => Array("FIELD" => "H.CITY", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = UA.HOUSE_ID)"),
		"DISTRICT" => Array("FIELD" => "H.DISTRICT", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = UA.HOUSE_ID)"),
		"REGION" => Array("FIELD" => "H.REGION", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = UA.HOUSE_ID)"),
		"SETTLEMENT" => Array("FIELD" => "H.SETTLEMENT", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = UA.HOUSE_ID)"),
		"STREET" => Array("FIELD" => "H.STREET", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = UA.HOUSE_ID)"),
		"HOUSE" => Array("FIELD" => "H.HOUSE", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = UA.HOUSE_ID)"),
		"ZIP" => Array("FIELD" => "H.ZIP", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = UA.HOUSE_ID)"),
		"HOUSE_BANK" => Array("FIELD" => "H.BANK", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = UA.HOUSE_ID)"),
		"HOUSE_BIK" => Array("FIELD" => "H.BIK", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = UA.HOUSE_ID)"),
		"HOUSE_RS" => Array("FIELD" => "H.RS", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = UA.HOUSE_ID)"),
		"HOUSE_KS" => Array("FIELD" => "H.KS", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = UA.HOUSE_ID)"),
		"HOUSE_OVERHAUL_BANK" => Array(
			"FIELD" => "H.OVERHAUL_BANK",
			"TYPE" => "string",
			"FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = UA.HOUSE_ID)"
		),
		"HOUSE_OVERHAUL_BIK" => Array("FIELD" => "H.OVERHAUL_BIK", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = UA.HOUSE_ID)"),
		"HOUSE_OVERHAUL_RS" => Array("FIELD" => "H.OVERHAUL_RS", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = UA.HOUSE_ID)"),
		"HOUSE_OVERHAUL_KS" => Array("FIELD" => "H.OVERHAUL_KS", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = UA.HOUSE_ID)"),
		"HOUSE_AREA" => Array("FIELD" => "H.AREA", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = UA.HOUSE_ID)"),
		"HOUSE_ROOMS_AREA" => Array("FIELD" => "H.ROOMS_AREA", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = UA.HOUSE_ID)"),
		"HOUSE_COMMON_PLACES_AREA" => Array(
			"FIELD" => "H.COMMON_PLACES_AREA",
			"TYPE" => "string",
			"FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = UA.HOUSE_ID)"
		),
		"ADDRESS_FULL" => Array(
			"FIELD" => "CONCAT(H.CITY,' ',H.STREET,' ',H.HOUSE,' ',UA.FLAT)",
			"TYPE" => "string",
			"CONCAT" => true,
			"FROM" => "LEFT JOIN b_tszh_house H ON (H.ID = UA.HOUSE_ID)"
		),

		// b_users
		"USER_ACTIVE" => Array("FIELD" => "U.ACTIVE", "TYPE" => "char", "FROM" => "LEFT JOIN b_user U ON (UA.USER_ID = U.ID)"),
		"USER_LOGIN" => Array("FIELD" => "U.LOGIN", "TYPE" => "string", "FROM" => "LEFT JOIN b_user U ON (UA.USER_ID = U.ID)"),
		"USER_XML_ID" => Array("FIELD" => "U.XML_ID", "TYPE" => "string", "FROM" => "LEFT JOIN b_user U ON (UA.USER_ID = U.ID)"),
		"USER_NAME" => Array("FIELD" => "U.NAME", "TYPE" => "string", "FROM" => "LEFT JOIN b_user U ON (UA.USER_ID = U.ID)"),
		"USER_LAST_NAME" => Array("FIELD" => "U.LAST_NAME", "TYPE" => "string", "FROM" => "LEFT JOIN b_user U ON (UA.USER_ID = U.ID)"),
		"USER_SECOND_NAME" => Array("FIELD" => "U.SECOND_NAME", "TYPE" => "string", "FROM" => "LEFT JOIN b_user U ON (UA.USER_ID = U.ID)"),
		"USER_FULL_NAME" => Array(
			"FIELD" => "CONCAT(U.LAST_NAME,' ',U.NAME,' ',U.SECOND_NAME)",
			"TYPE" => "string",
			"FROM" => "LEFT JOIN b_user U ON (UA.USER_ID = U.ID)",
			"CONCAT" => true
		),
		"USER_EMAIL" => Array("FIELD" => "U.EMAIL", "TYPE" => "string", "FROM" => "LEFT JOIN b_user U ON (UA.USER_ID = U.ID)"),
		"USER_SITE" => Array("FIELD" => "U.LID", "TYPE" => "string", "FROM" => "LEFT JOIN b_user U ON (UA.USER_ID = U.ID)"),

		// b_tszh
		"TSZH_NAME" => Array("FIELD" => "T.NAME", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh T ON (UA.TSZH_ID = T.ID)"),
		"TSZH_SITE" => Array("FIELD" => "T.SITE_ID", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh T ON (UA.TSZH_ID = T.ID)"),
		"TSZH_CODE" => Array("FIELD" => "T.CODE", "TYPE" => "string", "FROM" => "LEFT JOIN b_tszh T ON (UA.TSZH_ID = T.ID)"),
		"TSZH_METER_VALUES_START_DATE" => Array(
			"FIELD" => "T.METER_VALUES_START_DATE",
			"TYPE" => "int",
			"FROM" => "LEFT JOIN b_tszh T ON (UA.TSZH_ID = T.ID)"
		),
		"TSZH_METER_VALUES_END_DATE" => Array(
			"FIELD" => "T.METER_VALUES_END_DATE",
			"TYPE" => "int",
			"FROM" => "LEFT JOIN b_tszh T ON (UA.TSZH_ID = T.ID)"
		),

		// ���� �������������� ������� �����?
		"HAS_METERS" => Array(
			"FIELD" => 'if (exists (select ID from b_tszh_meters tmp_M inner join b_tszh_meters_accounts tmp_MA on (tmp_M.ID = tmp_MA.METER_ID) where tmp_M.HOUSE_METER != "Y" and tmp_MA.ACCOUNT_ID = UA.ID), "Y", "N")',
			"TYPE" => "char"
		),
	);

	/**
	 * ��������� ������ ������� ������ �� �������
	 *
	 * @param array $arOrder
	 * @param array $arFilter
	 * @param bool $arGroupBy
	 * @param bool $arNavStartParams
	 * @param array $arSelectFields
	 *
	 * @return bool|CDBResult
	 */
	public static function GetList($arOrder = array(), $arFilter = array(), $arGroupBy = false, $arNavStartParams = false, $arSelectFields = array())
	{
		global $USER_FIELD_MANAGER;

		if (count($arSelectFields) <= 0)
		{
			$arSelectFields = array_keys(static::$entityFields);
			$arSelectFields = array_diff($arSelectFields, array("HAS_METERS"));
		}
		elseif (in_array('*', $arSelectFields))
		{
			$arSelectFields = array_keys(static::$entityFields);
			$arSelectFields = array_diff($arSelectFields, array("HAS_METERS"));
		}

		$obUserFieldsSql = new CUserTypeSQL();
		$obUserFieldsSql->SetEntity("TSZH_ACCOUNT", "UA.ID");
		$obUserFieldsSql->SetSelect($arSelectFields);
		$obUserFieldsSql->SetFilter($arFilter);
		$obUserFieldsSql->SetOrder($arOrder);

		$dbRes = CTszh::GetListMakeQuery('b_tszh_accounts UA', static::$entityFields, $arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields, $obUserFieldsSql, "UA.ID");
		if (is_object($dbRes))
		{
			$dbRes->SetUserFields($USER_FIELD_MANAGER->GetUserFields("TSZH_ACCOUNT"));
		}

		return $dbRes;
	}

	public static function GetCount($arFilter = array())
	{
		$count = CSqlUtil::GetCount('b_tszh_accounts', 'UA', static::$entityFields, $arFilter);

		return $count;
	}

	/**
	 * ���������� ������������� ������ ���������� �������� ����� � ��������� ID
	 *
	 * @param int $ID ID �������� �����
	 *
	 * @return array|bool ������ �����, ���� ������� ���� ������, false � ���� ������
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function GetByID($ID)
	{
		global $DB;

		$ID = IntVal($ID);
		if ($ID <= 0)
		{
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);
		}

		if (isset($GLOBALS["TSZH_ACCOUNT"]["ACCOUNT_CACHE_" . $ID]) && is_array($GLOBALS["TSZH_ACCOUNT"]["ACCOUNT_CACHE_" . $ID]) && is_set($GLOBALS["TSZH_ACCOUNT"]["ACCOUNT_CACHE_" . $ID], "ID"))
		{
			return $GLOBALS["TSZH_ACCOUNT"]["ACCOUNT_CACHE_" . $ID];
		}
		else
		{
			$strSql =
				"SELECT TA.*, H.REGION as REGION, H.DISTRICT as DISTRICT, H.CITY as CITY, H.SETTLEMENT as SETTLEMENT, H.STREET as STREET, H.HOUSE as HOUSE, H.AREA as HOUSE_AREA, H.ROOMS_AREA as HOUSE_ROOMS_AREA, H.COMMON_PLACES_AREA as HOUSE_COMMON_PLACES_AREA " .
				"FROM b_tszh_accounts TA LEFT JOIN b_tszh_house H ON TA.HOUSE_ID = H.ID " .
				"WHERE TA.ID = " . $ID . " ";

			$dbUserAccount = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
			/** @noinspection PhpAssignmentInConditionInspection */
			if ($arUserAccount = $dbUserAccount->Fetch())
			{
				$GLOBALS["TSZH_ACCOUNT"]["ACCOUNT_CACHE_" . $ID] = $arUserAccount;
				if ($arUserAccount["CURRENT"] == "Y")
				{
					$GLOBALS["TSZH_ACCOUNT"]["USER_ACCOUNT_CACHE_" . $arUserAccount['USER_ID']] = $arUserAccount;
				}

				return $arUserAccount;
			}
		}

		return false;
	}

	/**
	 * ���������� ������������� ������ ����� �������� ����� ��� ������������ � ��������� ID
	 *
	 * @param int $USER_ID ID ������������
	 *
	 * @return array|bool
	 */
	public static function GetByUserID($USER_ID)
	{
		$USER_ID = IntVal($USER_ID);
		if ($USER_ID <= 0)
		{
			return false;
		}

		if (isset($GLOBALS["TSZH_ACCOUNT"]["USER_ACCOUNT_CACHE_" . $USER_ID]) && is_array($GLOBALS["TSZH_ACCOUNT"]["USER_ACCOUNT_CACHE_" . $USER_ID]) && is_set($GLOBALS["TSZH_ACCOUNT"]["USER_ACCOUNT_CACHE_" . $USER_ID], "ID"))
		{
			return $GLOBALS["TSZH_ACCOUNT"]["USER_ACCOUNT_CACHE_" . $USER_ID];
		}
		else
		{
			$dbUserAccount = self::GetList(Array("CURRENT" => "DESC"), Array("USER_ID" => $USER_ID), false, Array('nTopCount' => 1));
			/** @noinspection PhpAssignmentInConditionInspection */
			if ($arUserAccount = $dbUserAccount->Fetch())
			{
				$GLOBALS["TSZH_ACCOUNT"]["ACCOUNT_CACHE_" . $arUserAccount['ID']] = $arUserAccount;
				$GLOBALS["TSZH_ACCOUNT"]["USER_ACCOUNT_CACHE_" . $USER_ID] = $arUserAccount;

				return $arUserAccount;
			}
		}

		return false;
	}

	/**
	 * ������������� ������� ���� ������� (���������) ��� ������������
	 *
	 * @param int $ID ID �������� �����
	 * @param int|bool $USER_ID ID ������������ (��-��������� - ������� ������������)
	 *
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function SetCurrent($ID, $USER_ID = false)
	{
		global $DB, $USER;

		$ID = IntVal($ID);
		if ($ID <= 0)
		{
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);
		}

		$USER_ID = IntVal($USER_ID);
		if ($USER_ID <= 0)
		{
			$USER_ID = $USER->GetID();
		}
		if ($USER_ID <= 0)
		{
			return false;
		}

		$strSql =
			"UPDATE b_tszh_accounts UA SET " .
			"CURRENT = 'N' " .
			"WHERE UA.USER_ID = " . $USER_ID . " AND UA.CURRENT IS NOT NULL";
		$DB->Query($strSql, true);

		$strSql =
			"UPDATE b_tszh_accounts UA SET " .
			"CURRENT = 'Y' " .
			"WHERE UA.USER_ID = " . $USER_ID . " AND UA.ID=" . $ID;

		return (bool)$DB->Query($strSql, true);
	}

	/**
	 * ���������� ������������� ������ ���������� �������� ����� � ��������� ������� (XML_ID)
	 *
	 * @param string $XML_ID ����� �������� �����
	 *
	 * @return array|bool
	 * @throws \Bitrix\Main\ArgumentException
	 * @todo �������� �����������
	 */
	public static function GetByXmlID($XML_ID)
	{
		global $DB;

		$XML_ID = $DB->ForSql(trim($XML_ID));
		if (strlen($XML_ID) <= 0)
		{
			throw new \Bitrix\Main\ArgumentException("Empty XML_ID given", "XML_ID");
		}

		$strSql =
			"SELECT * " .
			"FROM b_tszh_accounts UA " .
			"WHERE UA.XML_ID = '" . $XML_ID . "' ";

		$dbUserAccount = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
		/** @noinspection PhpAssignmentInConditionInspection */
		if ($arUserAccount = $dbUserAccount->Fetch())
		{
			$GLOBALS["TSZH_ACCOUNT"]["ACCOUNT_CACHE_" . $arUserAccount['ID']] = $arUserAccount;
			if ($arUserAccount["CURRENT"] == "Y")
			{
				$GLOBALS["TSZH_ACCOUNT"]["USER_ACCOUNT_CACHE_" . $arUserAccount['USER_ID']] = $arUserAccount;
			}

			return $arUserAccount;
		}

		return false;
	}

	/**
	 * ���������� ������������� ������ ���������� �������� ����� �� ��� �������� ����
	 *
	 * @param string $EXTERNAL_ID ������� ��� �������� �����
	 *
	 * @return array|bool
	 * @todo �������� �����������
	 */
	public static function GetByExternalID($EXTERNAL_ID)
	{
		global $DB;

		$EXTERNAL_ID = $DB->ForSql(trim($EXTERNAL_ID));
		if (strlen($EXTERNAL_ID) <= 0)
		{
			return false;
		}

		$strSql =
			"SELECT * " .
			"FROM b_tszh_accounts UA " .
			"WHERE UA.EXTERNAL_ID = '" . $EXTERNAL_ID . "' ";

		$dbUserAccount = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
		/** @noinspection PhpAssignmentInConditionInspection */
		if ($arUserAccount = $dbUserAccount->Fetch())
		{
			$GLOBALS["TSZH_ACCOUNT"]["ACCOUNT_CACHE_" . $arUserAccount['ID']] = $arUserAccount;
			if ($arUserAccount["CURRENT"] == "Y")
			{
				$GLOBALS["TSZH_ACCOUNT"]["USER_ACCOUNT_CACHE_" . $arUserAccount['USER_ID']] = $arUserAccount;
			}

			return $arUserAccount;
		}

		return false;
	}

	/**
	 * ��������� ����� ����� ����� ���������� Add() ��� Update()
	 * � ������ ������ �-�� ��������� false � �������� $APPLICATION->ThrowException()
	 *
	 * @param array $arFields ������ � ������
	 * @param int $ID ID ������������� �������� ����� (��� �-�� Add) ��� 0
	 * @param string $strOperation ��� �������� (ADD ��� UPDATE)
	 * @param bool $bCheckUserExists ��������� �� ������������� ������������, ���������� � ���� USER_ID
	 *
	 * @return bool
	 */
	public static function CheckFields(&$arFields, $ID = 0, $strOperation = 'ADD', $bCheckUserExists = false)
	{
		global $APPLICATION, $USER_FIELD_MANAGER;

		foreach ($arFields as $key => $value)
		{
			if (strtoupper(substr($key, 0, 3)) == 'UF_')
			{
				continue;
			}

			if (in_array($key, self::$houseFields))
			{
				continue;
			}

			if (!array_key_exists($key, static::$entityFields))
			{
				throw new InvalidArgumentException("Unknown field $key");
			}
			if (isset(static::$entityFields[$key]['FROM']))
			{
				throw new InvalidArgumentException("Can't direcly change field $key. Change record in the linked table instead");
			}
		}

		if (array_key_exists("USER_ID", $arFields))
		{
			$arFields['USER_ID'] = IntVal($arFields['USER_ID']);
			if ($arFields["USER_ID"] > 0)
			{
				if ($bCheckUserExists)
				{
					if (CTszhAccount::GetList(Array(), Array("!ID" => $ID, "USER_ID" => $arFields["USER_ID"]), Array()))
					{
						$APPLICATION->ThrowException(str_replace("#USER_ID#", $arFields['USER_ID'], GetMessage("TSZH_ERROR_USER_ALREADY_HAS_ACCOUNT")), "TSZH_USER_ALREADY_HAS_ACCOUNT");

						return false;
					}
				}
			}
			else
			{
				$arFields["USER_ID"] = false;
			}
		}

		if (array_key_exists('EXTERNAL_ID', $arFields) || $strOperation == 'ADD')
		{
			$arFields['EXTERNAL_ID'] = trim($arFields['EXTERNAL_ID']);
			/*			$arCheckAccount = CTszhAccount::GetByExternalID($arFields['EXTERNAL_ID']);
						if ($arCheckAccount) {
							if ($strOperation == 'ADD' ||
								($strOperation == 'UPDATE' && $arCheckAccount['ID'] != $ID)
							) {
								$APPLICATION->ThrowException(GetMessage("TSZH_ERROR_ACCOUNT_EXTERNAL_ID_DUPLICATED"), "TSZH_ACCOUNT_EXTERNAL_ID_DUPLICATED");
								return false;
							}
						}*/
		}

		if (array_key_exists('TSZH_ID', $arFields) || $strOperation == 'ADD')
		{
			$arFields['TSZH_ID'] = IntVal($arFields['TSZH_ID']);
			if ($arFields['TSZH_ID'] <= 0)
			{
				$APPLICATION->ThrowException(GetMessage("TSZH_ERROR_NO_TSZH_ID"), "TSZH_ERROR_NO_TSZH_ID");

				return false;
			}
		}

		if (isset($arFields["TYPE"]))
		{
			$accountTypes = \Citrus\Tszh\Types\AccountType::getTitles();
			if (!isset($accountTypes[$arFields["TYPE"]]))
			{
				$APPLICATION->ThrowException(GetMessage("CITRUS_TSZH_ACCOUNT_ERROR_WRONG_TYPE", array('#TYPE#' => $arFields["TYPE"])), "CITRUS_TSZH_ACCOUNT_ERROR_WRONG_TYPE");

				return false;
			}
		}

		if (!$USER_FIELD_MANAGER->CheckFields("TSZH_ACCOUNT", $ID, $arFields))
		{
			return false;
		}

		unset($arFields['ID']);

		return true;
	}

	/**
	 * ���������� ID ���� �� ��������� ����� ����.
	 *
	 * ��������� ��� � ��������� ������� ��� �������������
	 *
	 * @param array $arFields
	 *
	 * @return array|bool|false
	 * @throws \Bitrix\Main\ArgumentException
	 */
	protected static function getHouseId($arFields)
	{
		global $APPLICATION;
		/** @var array ������ ����������, ���������� �� ������ ���� ��� ���������� ���������� �������� ��� ������� ������� ������ */
		static $cache = array();

		$houseValues = array();
		$addressFilter = array();
		foreach ($arFields as $key => $value)
		{
			if (in_array($key, static::$houseFields))
			{
				$key = str_replace('HOUSE_', '', $key);
				$houseValues[$key] = $value;

				if (in_array($key, array('CITY', 'DISTRICT', 'REGION', 'SETTLEMENT', 'STREET', 'HOUSE')))
				{
					$addressFilter['=' . $key] = $value;
				}
			}
		}

		if (empty($houseValues))
		{
			return true;
		}

		if (!array_key_exists('TSZH_ID', $arFields))
		{
			throw new InvalidArgumentException("Missing TSZH_ID key in \$arFields parameter");
		}

		$houseValues["TSZH_ID"] = $arFields['TSZH_ID'];

		trigger_error("Use \$arFields['HOUSE_ID'] key to specify account address", E_USER_DEPRECATED);

		$cacheKey = md5(implode('+', $addressFilter));
		if (array_key_exists($cacheKey, $cache))
		{
			return $cache[$cacheKey];
		}

		$houseId = HouseTable::getList(array(
			"filter" => $addressFilter,
			"limit" => 1,
			"select" => array("ID"),
		))->fetch();
		$houseId = is_array($houseId) ? $houseId["ID"] : false;

		if ($houseId)
		{
			$result = HouseTable::update($houseId, $houseValues);
		}
		else
		{
			$result = HouseTable::add($houseValues);
			if ($result->isSuccess())
			{
				$houseId = $result->getId();
			}
		}

		if ($result->isSuccess())
		{
			$cache[$cacheKey] = $houseId;

			return $houseId;
		}
		else
		{
			$APPLICATION->ThrowException(implode("\n", $result->getErrorMessages()));

			return false;
		}
	}

	/**
	 * ��������� ����� ������� ����
	 *
	 * @param array $arFields ���� �������� �����
	 * @param bool $bCheckUserExists ��������� ������������� ������������, ���� �� ��� ������
	 * @param string|bool $strPeriodDate ���� �������, � �������� ��������� ��������� (��� ����������� � ������� ���������), � ������� ���� � ������� �������� ����� ��� �����
	 * @param bool $bAddHistory ����: ��������� ������� ���� � �������
	 *
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function Add($arFields, $bCheckUserExists = true, $strPeriodDate = false, $bAddHistory = true)
	{
		global $DB, $USER_FIELD_MANAGER;

		if (!CTszhAccount::CheckFields($arFields, 0, 'ADD', false, $bCheckUserExists))
		{
			return false;
		}

		if (!isset($arFields["HOUSE_ID"]))
		{
			$detectedHouseId = static::getHouseId($arFields);
			// false ������������ � ������ ������
			if ($detectedHouseId === false)
			{
				return false;
			}
			//true ������������ � ������ ���� ���� ������ �� ���� �������
			elseif ($detectedHouseId !== true)
			{
				$arFields["HOUSE_ID"] = $detectedHouseId;
			}
		}

		$ID = CDatabase::Add("b_tszh_accounts", $arFields);

		if ($ID > 0)
		{
			$arNewFields = self::GetByID($ID);
			if ($bAddHistory)
			{
				$strPeriodDate = false !== $strPeriodDate && $DB->IsDate($strPeriodDate, false, false, "FULL") ? $strPeriodDate : ConvertTimeStamp(false, "FULL");
				// save values in history
				CTszhAccountHistory::Add(Array(
					'ACCOUNT_ID' => $ID,
					'FIELDS' => array_diff_key($arNewFields, Array("ID" => 1, "CURRENT" => 1)),
					'TIMESTAMP_X' => $strPeriodDate,
				));
			}

			if (CTszh::hasUserFields($arFields))
			{
				$USER_FIELD_MANAGER->Update("TSZH_ACCOUNT", $ID, $arFields);
			}
			$arFields["ID"] = $ID;

			$db_events = GetModuleEvents("citrus.tszh", "OnAfterAccountAdd");
			/** @noinspection PhpAssignmentInConditionInspection */
			while ($arEvent = $db_events->Fetch())
			{
				ExecuteModuleEventEx($arEvent, array(&$arFields));
			}
		}

		return $ID;
	}

	/**
	 * �������� ���������� � ������� �����
	 *
	 * @param int $ID ID �������� �����
	 * @param array $arFields ������������� ������ ����� �����
	 * @param bool|string $strPeriodDate ���� �������, � �������� ��������� ��������� (��� ����������� � ������� ���������), � ������� ���� � ������� �������� ����� ��� �����
	 *
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function Update($ID, $arFields, $strPeriodDate = false)
	{
		global $DB, $USER_FIELD_MANAGER;

		$ID = IntVal($ID);
		if ($ID <= 0)
		{
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);
		}

		if (!CTszhAccount::CheckFields($arFields, $ID, 'UPDATE'))
		{
			return false;
		}

		if (!isset($arFields["HOUSE_ID"]))
		{
			$detectedHouseId = static::getHouseId($arFields);
			// false ������������ � ������ ������
			if ($detectedHouseId === false)
			{
				return false;
			}
			//true ������������ � ������ ���� ���� ������ �� ���� �������
			elseif ($detectedHouseId !== true)
			{
				$arFields["HOUSE_ID"] = $detectedHouseId;
			}
		}

		$arOldFields = self::GetByID($ID);

		$strUpdate = $DB->PrepareUpdate("b_tszh_accounts", $arFields);
		$strSql = "UPDATE b_tszh_accounts SET $strUpdate WHERE ID=" . $ID;
		$bSuccess = $DB->Query($strSql, true, "File: " . __FILE__ . "<br>Line: " . __LINE__);
		if ($bSuccess)
		{
			$GLOBALS["TSZH_ACCOUNT"] = Array();
			$arNewFields = self::GetByID($ID);
			$strPeriodDate = false !== $strPeriodDate && $DB->IsDate($strPeriodDate, false, false, "FULL") ? $strPeriodDate : ConvertTimeStamp(false, "FULL");
			// save values in history
			if (self::WasChanged($arOldFields, $arNewFields))
			{
				CTszhAccountHistory::Add(Array(
					'ACCOUNT_ID' => $ID,
					'FIELDS' => array_diff_key($arNewFields, Array("ID" => 1, "CURRENT" => 1)),
					'TIMESTAMP_X' => $strPeriodDate,
				));
			}

			if (CTszh::hasUserFields($arFields))
			{
				$USER_FIELD_MANAGER->Update("TSZH_ACCOUNT", $ID, $arFields);
			}
		}

		return $bSuccess;
	}

	/**
	 * ������� ������� ���� (� ��� ��������� � ��� ����������)
	 *
	 * @param int $ID ID �������� �����
	 * @param bool $bDeleteUser ������� �� ������������, ������� ��� �������� � �������� �����
	 *
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function Delete($ID, $bDeleteUser = false)
	{
		global $DB, $USER_FIELD_MANAGER;
		$ID = intval($ID);
		$cnt = array(
			'meters' => 0,
			'periods' => 0,

		);
		if ($ID <= 0)
		{
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);
		}

		$arAccount = CTszhAccount::GetByID($ID);
		if (!is_array($arAccount))
		{
			return false;
		}

		$message = \Bitrix\Main\Localization\Loc::getMessage('TSZH_ACCOUNT_DELETE', array('#XML_ID#' => $arAccount["XML_ID"], '#EXTERNAL_ID#' => $arAccount["EXTERNAL_ID"], '#ID#' => $arAccount["ID"]));

		// ��������
		$rsMeters = CTszhMeter::GetList(Array(), Array("ACCOUNT_ID" => $ID), false, false, Array("ID"));
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($arMeter = $rsMeters->Fetch())
		{
			CTszhMeter::Delete($arMeter['ID'], $ID);
			$cnt['meters']++;
		}
		$message .= \Bitrix\Main\Localization\Loc::getMessage('TSZH_ACCOUNT_DELETE_METERS', array('#COUNT#' => $cnt['meters']));

		// �������
		$strSql = "DELETE FROM b_tszh_accounts_history WHERE ACCOUNT_ID=" . $ID;
		$DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);
		$message .= \Bitrix\Main\Localization\Loc::getMessage('TSZH_ACCOUNT_DELETE_HISTORY');

		// �������� ���������
		$rs = CTszhAccountPeriod::GetList(Array(), Array("ACCOUNT_ID" => $ID), false, false, Array("ID"));
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($ar = $rs->Fetch())
		{
			CTszhAccountPeriod::Delete($ar['ID']);
			$cnt['periods']++;
		}
		$message .= \Bitrix\Main\Localization\Loc::getMessage('TSZH_ACCOUNT_DELETE_PERIODS', array('#COUNT#' => $cnt['periods']));

		// ������� ����
		$strSql = "DELETE FROM b_tszh_accounts WHERE ID=" . $ID;
		$DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);

		if ($bDeleteUser)
		{
			/** @noinspection PhpUndefinedVariableInspection */
			CUser::Delete($arAccount["USER_ID"]);
			$message .= \Bitrix\Main\Localization\Loc::getMessage('TSZH_ACCOUNT_DELETE_USER', array('#USER_ID#' => $arAccount["USER_ID"]));
		}

		$USER_FIELD_MANAGER->Delete("TSZH_ACCOUNT", $ID);
		$message .= \Bitrix\Main\Localization\Loc::getMessage('TSZH_ACCOUNT_DELETE_UF_FIELDS');

		CTszhPublicHelper::logMessage($message, 'TSZH_ACCOUNT_DELETE');

		return true;
	}

	/**
	 * ���������� ������ � ������ ������� �������� �����
	 *
	 * @param array $arFields ���� �������� �����, ������ ��������� ����������� ���� ������ (REGION, DISTRICT, CITY � �.�.)
	 * @param bool|array $arExcludeFields ��������� ���� ������ �� �������� � �������������� ������
	 *
	 * @return string
	 */
	public static function GetFullAddress($arFields, $arExcludeFields = true)
	{
		$arResult = Array();

        if($arFields['FLAT_TYPE']!=GetMessage("TSZH_ACCOUNT_PRIVATE")){
            $arAddressFields = Array(
                "REGION",
                "DISTRICT",
                "CITY",
                "SETTLEMENT",
                "STREET",
                "HOUSE",
                "FLAT",
            );
        }
        else{
            $arAddressFields = Array(
                "REGION",
                "DISTRICT",
                "CITY",
                "SETTLEMENT",
                "STREET",
                "HOUSE",
            );
        }
		if (!is_array($arExcludeFields))
		{
			if ($arExcludeFields === true)
			{
				$arExcludeFields = Array();
			}
			else
			{
				$arExcludeFields = Array("FLAT");
			}
		}
		$arAddressFields = array_diff($arAddressFields, $arExcludeFields);
		foreach ($arAddressFields as $field)
		{
			if ($field == "FLAT")
			{
				if (strlen($arFields['FLAT_ABBR']) > 0)
				{
					$arResult[] = $arFields['FLAT_ABBR'];
				}
				else
				{
					$arResult[] = strlen(trim($arFields['FLAT'])) ? GetMessage("TSZH_HOUSE_FLAT_ABBR") . ' ' . $arFields['FLAT'] : '';
				}
			}
			elseif (strlen($arFields[$field]) > 0)
			{
				$arResult[] = $arFields[$field];
			}
		}

		return implode(', ', $arResult);
	}

	/**
	 * ���������� ��� ��������� �������� �����
	 *
	 * @param int $ID ID �������� ���� ��� ������������-���������
	 * @param bool $bUserID ���� true, �-�� ������ ��� ���������� ������������, ����� � ��� ��������� �� ����� �������� �����
	 *
	 * @return string
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function GetFullName($ID, $bUserID = true)
	{
		$arUser = $ID;
		if (!is_array($ID) && IntVal($ID) > 0)
		{
			if ($bUserID)
			{
				$arUser = CUser::GetByID($ID)->Fetch();
			}
			else
			{
				$arUser = self::GetByID($ID);
			}
		}

		if (!is_array($arUser))
		{
			return '';
		}

		$arResult = Array();
		if (strlen($arUser['LAST_NAME']) > 0)
		{
			$arResult[] = $arUser['LAST_NAME'];
		}

		if (strlen($arUser['ACCOUNT_NAME']) > 0)
		{
			$arResult[] = $arUser['ACCOUNT_NAME'];
		}
		elseif (strlen($arUser['NAME']) > 0)
		{
			$arResult[] = $arUser['NAME'];
		}

		if (strlen($arUser['SECOND_NAME']) > 0)
		{
			$arResult[] = $arUser['SECOND_NAME'];
		}

		if (count($arResult) <= 0 && $bUserID)
		{
			$arResult[] = $arUser['LOGIN'];
		}

		return implode(' ', $arResult);
	}

	/**
	 * ���������� ��� ������ ����� �������� ����� � ���������� true ���� ���� ���������
	 * ������������ ��� ����������� ��������� � �������, ���� ���� �������� ��������� �����
	 *
	 * @param array $arAccount1 ���������� �������� ����� �������� �����
	 * @param array $arAccount2 ����� �������� ����� �������� �����
	 *
	 * @return bool
	 */
	public static function WasChanged($arAccount1, $arAccount2)
	{
		static $arIgnoreFields = Array(
			'ID' => 1,
			'CURRENT' => 1,
		);
		foreach ($arAccount1 as $key => $val)
		{
			if (strlen($key) > 3 && strtoupper(substr($key, 0, 3)) == 'UF_')
			{
				unset($arAccount1[$key]);
			}
		}
		foreach ($arAccount2 as $key => $val)
		{
			if (strlen($key) > 3 && strtoupper(substr($key, 0, 3)) == 'UF_')
			{
				unset($arAccount2[$key]);
			}
		}

		$arAccount1 = array_diff_key($arAccount1, $arIgnoreFields);
		$arAccount2 = array_diff_key($arAccount2, $arIgnoreFields);

		return $arAccount1 != $arAccount2;
	}

	/**
	 * ���������� ������������� � ���������� (���������) �������� ����� ������ ������ ������� ������ (�� ���������, ��������� � �.�.).
	 * �������������� ������� ����� ���������, ��������� � ���� ������ ����������� �� �������� ������� ����
	 *
	 * @param int $primaryId ID ��������� �������� ����� (������� ���������� ��������)
	 * @param array|bool $mergeWithId ������ ID ������� ������ ��� ������� � ��������
	 * @param array|bool $mergeWithExternalId ������ EXTERNAL_ID ������� ������ ��� ������� � ��������
	 *
	 * @return int
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 * @throws \Bitrix\Main\ArgumentTypeException
	 */
	public static function mergeAccounts($primaryId, $mergeWithId = array(), $mergeWithExternalId = array())
	{
		global $DB;

		if (!is_array($mergeWithId) && !is_bool($mergeWithId))
		{
			throw new \Bitrix\Main\ArgumentTypeException("mergeWithId", "array|bool");
		}
		if (!is_array($mergeWithExternalId) && !is_bool($mergeWithExternalId))
		{
			throw new \Bitrix\Main\ArgumentTypeException("mergeWithExternalId", "array|bool");
		}

		$primaryAccount = self::GetByID($primaryId);

		if (!is_array($mergeWithId))
		{
			$mergeWithId = array();
		}
		foreach ($mergeWithId as $key => &$id)
		{
			$id = intval($id);
			if ($id <= 0)
			{
				unset($mergeWithId[$key]);
			}
		}
		unset($id);

		if (!empty($mergeWithExternalId))
		{
			$dbAccounts = self::GetList(array(), array(
				"TSZH_ID" => $primaryAccount["TSZH_ID"],
				"EXTERNAL_ID" => $mergeWithExternalId
			), false, false, array("ID"));
			/** @noinspection PhpAssignmentInConditionInspection */
			while ($account = $dbAccounts->Fetch())
			{
				$mergeWithId[] = $account["ID"];
			}
		}
		$mergeWithId = array_diff(array_unique($mergeWithId), array($primaryId));

		if (!empty($mergeWithId))
		{
			$primaryPeriods = array();
			$dbPrimaryPeriods = CTszhAccountPeriod::GetList(array(), array("ACCOUNT_ID" => $primaryId), false, false, array(
				"ID",
				"PERIOD_ID",
				"PERIOD_DATE"
			));
			/** @noinspection PhpAssignmentInConditionInspection */
			while ($primaryPeriod = $dbPrimaryPeriods->Fetch())
			{
				$primaryPeriods[$primaryPeriod["PERIOD_ID"]] = $primaryPeriod;
			}

			foreach ($mergeWithId as $mergeAccountId)
			{
				$dbAccountPeriods = CTszhAccountPeriod::GetList(array(), array("ACCOUNT_ID" => $mergeAccountId), false, false, array(
					"ID",
					"PERIOD_ID",
					"PERIOD_DATE"
				));
				/** @noinspection PhpAssignmentInConditionInspection */
				while ($accountPeriod = $dbAccountPeriods->Fetch())
				{
					// ������ ����� ���������� �/�, ���� � ��������� ��� ���� ������ �� ���� ������
					if (array_key_exists($accountPeriod["PERIOD_ID"], $primaryPeriods))
					{
						// ���������
						$DB->Query("delete from b_tszh_accounts_period where ACCOUNT_ID = $mergeAccountId and PERIOD_ID = {$accountPeriod['PERIOD_ID']}");
						// ����������
						$DB->Query("delete from b_tszh_services_period where ACCOUNT_PERIOD_ID = {$accountPeriod['ID']}");
						// �������������
						$DB->Query("delete from b_tszh_accounts_corrections where ACCOUNT_ID = $mergeAccountId and ACCOUNT_PERIOD_ID = {$accountPeriod['ID']}");
						// ����������
						$DB->Query("delete from b_tszh_accounts_contractors where ACCOUNT_PERIOD_ID = {$accountPeriod['ID']}");
					}
					else
					{
						// ���������
						$DB->Query("update b_tszh_accounts_period set ACCOUNT_ID = $primaryId where ACCOUNT_ID = $mergeAccountId and PERIOD_ID = {$accountPeriod['PERIOD_ID']}");
					}
				}

				// ������� ��������� �/�
				$DB->Query("update b_tszh_accounts_history set ACCOUNT_ID = $primaryId where ACCOUNT_ID = $mergeAccountId");

				// ��������
				// �������� ��������, ����� � ������������ �� ����������� ���������� ��, ������� ������� ������������� �������� ��������� � ������� ������
				if ($DB->TableExists('b_tszh_meters_accounts'))
				{
					$alreadyBoundMeters = array();
					$rs = $DB->Query("select * from b_tszh_meters_accounts where ACCOUNT_ID = $primaryId");
					/** @noinspection PhpAssignmentInConditionInspection */
					while ($ar = $rs->Fetch())
					{
						$alreadyBoundMeters[] = $ar["METER_ID"];
					}

					$DB->Query("delete from b_tszh_meters_accounts where ACCOUNT_ID = $mergeAccountId" . (empty($alreadyBoundMeters) ? '' : " and METER_ID in (" . implode(',', $alreadyBoundMeters) . ")"));
					$DB->Query("update b_tszh_meters_accounts set ACCOUNT_ID = $primaryId where ACCOUNT_ID = $mergeAccountId");
				}
				else
				{
					$DB->Query("update b_tszh_meters set ACCOUNT_ID = $primaryId where ACCOUNT_ID = $mergeAccountId");
				}

				CTszhAccount::Delete($mergeAccountId);
			}
		}

		return count($mergeWithId);
	}

	/**
	 * ������ ��������� ������� � ������� �������� ��������.
	 * �� ������ ������ �� ������� �� ��������� �������� ������ ���������� ������������� ���������������� ��������������
	 *
	 * ������������ ��������
	 * true        � ��� �������� ������� ������ ������������� ��������� ������������ ���� ������� ������, ����������� � �/�
	 * false    � ��� �������� ��������� ������ ������� �����, ������������ �������������� ��������������, ��� ��������� ������� � ������ ������� �������� �������� � �/� � ������ UF_REGCODE
	 *
	 * @return bool
	 */
	public static function isAutoRegistrationEnabled()
	{
		return (COption::GetOptionString("main", "new_user_registration", "N") == "N" ? true : (COption::GetOptionString("citrus.tszh", "1c_exchange.GenerateLogins", "N") == "N" ? false : true));
	}
}