<?php

IncludeModuleLangFile(__FILE__);

$GLOBALS["TSZH_SERVICE"] = Array();

/**
 * CTszhService
 * ����� ��� ������ � ��������
 *
 * ����
 * ----
 * ID        - ID ������
 * ACTIVE    - ���� ����������
 * UNITS    - ��. ���������
 * TSZH_ID    - ID ���
 * XML_ID    - ������� ��� ������ (�� 1�)
 * NAME        - ������������ ������
 * NORM        - �����
 * TARIFF    - �����
 * TARIFF2    - ����� 2
 * TARIFF3    - ����� 3
 *
 *
 * @package
 * @author
 * @copyright nook.yo
 * @version 2010
 * @access public
 */
class CTszhService
{

	// ������� ��, ���������� ������
	const TABLE_NAME = 'b_tszh_services';
	// �������� ������ � ��������� ��������, ���� ������������ ���������������� ���� �������� ������; false, ���� �� ������������
	const USER_FIELD_ENTITY = 'TSZH_SERVICE';

	/**
	 * ����� ���������� ������������� ������ ���������� ������ � ��������� ID
	 *
	 * @param int $ID ID ������
	 *
	 * @return array|bool
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function GetByID($ID)
	{
		global $DB;

		$ID = IntVal($ID);
		if ($ID <= 0)
		{
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);
		}

		if (isset($GLOBALS["TSZH_SERVICE"]["CACHE_" . $ID]) && is_array($GLOBALS["TSZH_SERVICE"]["CACHE_" . $ID]) && is_set($GLOBALS["TSZH_SERVICE"]["CACHE_" . $ID], "ID"))
		{
			return $GLOBALS["TSZH_SERVICE"]["CACHE_" . $ID];
		}
		else
		{
			$strSql =
				"SELECT TS.* " .
				"FROM " . self::TABLE_NAME . " TS " .
				"WHERE TS.ID = " . $ID . " ";

			$dbService = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
			/** @noinspection PhpAssignmentInConditionInspection */
			if ($arService = $dbService->Fetch())
			{
				$GLOBALS["TSZH_SERVICE"]["CACHE_" . $ID] = $arService;

				return $arService;
			}
		}

		return false;
	}

	/**
	 * ����� ���������� ������������� ������ ����� ������ � ������� ����� XML_ID
	 *
	 * @param string $XML_ID ������� ��� ������
	 *
	 * @return array|bool
	 * @throws \Bitrix\Main\ArgumentException
	 */
	public static function GetByXmlID($XML_ID)
	{
		global $DB;

		$XML_ID = trim($XML_ID);
		if (strlen($XML_ID) <= 0)
		{
			throw new \Bitrix\Main\ArgumentException("Empty XML_ID given", "ID");
		}

		$strSql =
			"SELECT TS.* " .
			"FROM " . self::TABLE_NAME . " TS " .
			"WHERE TS.XML_ID = '" . $DB->ForSql($XML_ID) . "' ";

		$dbService = $DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);
		if (is_array($arService = $dbService->Fetch()))
		{
			$GLOBALS["TSZH_SERVICE"]["CACHE_" . $arService['ID']] = $arService;

			return $arService;
		}

		return false;
	}


	/**
	 * @param array $arFields
	 * @param string $strOperation
	 *
	 * @return bool
	 */
	public static function CheckFields(&$arFields, $strOperation = 'ADD')
	{

		global $APPLICATION, $USER_FIELD_MANAGER;

		if (array_key_exists("NAME", $arFields) || $strOperation == 'ADD')
		{
			$arFields['NAME'] = trim($arFields['NAME']);
			if (strlen($arFields['NAME']) <= 0)
			{
				$APPLICATION->ThrowException(GetMessage("ERROR_TSZH_SERVICE_NO_NAME"));

				return false;
			}
		}

		if (array_key_exists("ACTIVE", $arFields) || $strOperation == 'ADD')
		{
			$arFields['ACTIVE'] = $arFields['ACTIVE'] != 'N' ? 'Y' : 'N';
		}

		if (array_key_exists("TSZH_ID", $arFields) || $strOperation == 'ADD')
		{
			$arFields["TSZH_ID"] = IntVal($arFields["TSZH_ID"]);
			if ($arFields["TSZH_ID"] <= 0)
			{
				$APPLICATION->ThrowException(GetMessage("ERROR_TSZH_SERVICE_NO_TSZH"));

				return false;
			}
		}

		if (self::USER_FIELD_ENTITY && !$USER_FIELD_MANAGER->CheckFields(self::USER_FIELD_ENTITY, $arFields['ID'], $arFields))
		{
			return false;
		}

		unset($arFields['ID']);

		return true;
	}

	/**
	 * ��������� ����� ������
	 *
	 * @param array $arFields
	 *
	 * @return bool
	 */
	public static function Add($arFields)
	{
		global $USER_FIELD_MANAGER;

		if (!CTszhService::CheckFields($arFields, 'ADD'))
		{
			return false;
		}

		$ID = CDatabase::Add(self::TABLE_NAME, $arFields);

		if (self::USER_FIELD_ENTITY && $ID > 0 && CTszh::hasUserFields($arFields))
		{
			$USER_FIELD_MANAGER->Update(self::USER_FIELD_ENTITY, $ID, $arFields);
		}

		return $ID;
	}

	/**
	 * �������� ���������� �� ������
	 *
	 * @param int $ID
	 * @param array $arFields
	 *
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function Update($ID, $arFields)
	{
		global $DB, $USER_FIELD_MANAGER;

		$ID = IntVal($ID);
		if ($ID <= 0)
		{
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);
		}

		if (!CTszhService::CheckFields($arFields, 'UPDATE'))
		{
			return false;
		}

		$strUpdate = $DB->PrepareUpdate(self::TABLE_NAME, $arFields);
		$strSql =
			"UPDATE " . self::TABLE_NAME . " SET " .
			$strUpdate .
			" WHERE ID=" . $ID;
		$bSuccess = (bool)$DB->Query($strSql, false, "File: " . __FILE__ . "<br>Line: " . __LINE__);

		if (self::USER_FIELD_ENTITY && $bSuccess && CTszh::hasUserFields($arFields))
		{
			$USER_FIELD_MANAGER->Update(self::USER_FIELD_ENTITY, $ID, $arFields);
		}

		return $bSuccess;
	}

	/**
	 * ������� ������ (� ��� ��������� � ��� ����������)
	 *
	 * @param int $ID
	 *
	 * @return bool
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public static function Delete($ID)
	{
		global $DB, $USER_FIELD_MANAGER;

		$ID = intval($ID);
		if ($ID <= 0)
		{
			throw new \Bitrix\Main\ArgumentOutOfRangeException("ID", 1);
		}

		$strSql = "DELETE FROM " . self::TABLE_NAME . " WHERE ID=" . $ID;
		$DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);

		// ��������
		$strSql = "DELETE FROM b_tszh_meters WHERE SERVICE_ID=" . $ID;
		$DB->Query($strSql, false, "FILE: " . __FILE__ . "<br> LINE: " . __LINE__);

		if (self::USER_FIELD_ENTITY)
		{
			$USER_FIELD_MANAGER->Delete(self::USER_FIELD_ENTITY, $ID);
		}

		return true;
	}

	/**
	 * ���������� ������ �����, ��������������� ���������� ��������
	 *
	 * @param array $arOrder
	 * @param array $arFilter
	 * @param array|bool $arGroupBy
	 * @param array|bool $arNavStartParams
	 * @param array $arSelectFields
	 *
	 * @return bool|CDBResult
	 */
	public static function GetList($arOrder = array(), $arFilter = array(), $arGroupBy = false, $arNavStartParams = false, $arSelectFields = array())
	{
		global $USER_FIELD_MANAGER;

		static $arFields = array(
			"ID" => Array("FIELD" => "TS.ID", "TYPE" => "int"),
			"ACTIVE" => Array("FIELD" => "TS.ACTIVE", "TYPE" => "char"),
			"TSZH_ID" => Array("FIELD" => "TS.TSZH_ID", "TYPE" => "int"),
			"XML_ID" => Array("FIELD" => "TS.XML_ID", "TYPE" => "string"),
			"NAME" => Array("FIELD" => "TS.NAME", "TYPE" => "string"),
			"NORM" => Array("FIELD" => "TS.NORM", "TYPE" => "double"),
			"TARIFF" => Array("FIELD" => "TS.TARIFF", "TYPE" => "double"),
			"TARIFF2" => Array("FIELD" => "TS.TARIFF2", "TYPE" => "double"),
			"TARIFF3" => Array("FIELD" => "TS.TARIFF3", "TYPE" => "double"),
			"UNITS" => Array("FIELD" => "TS.UNITS", "TYPE" => "string"),
            "IS_INSURANCE" => Array("FIELD" => "TS.IS_INSURANCE", "TYPE" => "char"),
		);

		if (count($arSelectFields) <= 0)
		{
			$arSelectFields = array_keys($arFields);
		}

		$obUserFieldsSql = new CUserTypeSQL;
		$obUserFieldsSql->SetEntity(self::USER_FIELD_ENTITY, "TS.ID");
		$obUserFieldsSql->SetSelect($arSelectFields);
		$obUserFieldsSql->SetFilter($arFilter);
		$obUserFieldsSql->SetOrder($arOrder);

		$dbRes = CTszh::GetListMakeQuery(self::TABLE_NAME . " TS", $arFields, $arOrder, $arFilter, $arGroupBy, $arNavStartParams, $arSelectFields, $obUserFieldsSql, "TS.ID");
		if (is_object($dbRes))
		{
			$dbRes->SetUserFields($USER_FIELD_MANAGER->GetUserFields(self::USER_FIELD_ENTITY));
		}

		return $dbRes;
	}
}