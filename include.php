<?php
IncludeModuleLangFile(__FILE__);

// ��������� ������� �������� �������� ���������� true, ���� ��� ������ ��� ����� ���������
if (!function_exists('tszhCheckMinEdition')):
	function tszhCheckMinEdition($minEdition)
	{
		$arEditionModules = Array(
			'start' => array("ru" => "citrus.tszhstart", "ua" => "vdgb.tszhstartua"),
			'standard' => array("ru" => "citrus.tszhstandard", "ua" => "vdgb.tszhstandardua"),
			'smallbusiness' => array("ru" => "citrus.tszhsb", "ua" => "vdgb.tszhsbua"),
			'expert' => array("ru" => "citrus.tszhe", "ua" => "vdgb.tszheua"),
			'business' => array("ru" => "citrus.tszhb", "ua" => "vdgb.tszhbua"),
		);
		if (!array_key_exists($minEdition, $arEditionModules))
		{
			throw new Exception('Unknown edition specified!');
		}

		$bWasMinEdition = false;
		foreach ($arEditionModules as $strEdition => $arModuleIDs)
		{
			if ($strEdition == $minEdition)
			{
				$bWasMinEdition = true;
			}
			foreach ($arModuleIDs as $strRegion => $strModuleID)
			{
				if ($bWasMinEdition && IsModuleInstalled($strModuleID) && CModule::IncludeModule($strModuleID))
				{
					return true;
				}
			}
		}

		return false;
	}

endif;

global $DBType;
if ($GLOBALS['DBType'] != 'mysql')
{
	return false;
}

$tszhDBType = 'general';

define("TSZH_MODULE_ID", 'citrus.tszh');
define("TSZH_EXCHANGE_CURRENT_VERSION", 3);

// ���������� �������� ������
IncludeModuleLangFile(__FILE__);

CModule::AddAutoloadClasses(
	TSZH_MODULE_ID, array(
		// ������� ����������
		"CTszh" => "classes/" . $tszhDBType . "/tszh.php",
		"CTszhResult" => "classes/" . $tszhDBType . "/tszh_result.php",
		// ������
		"CTszhCurrency" => "classes/" . $tszhDBType . "/tszh_currencies.php",
		// �������
		"CTszhPeriod" => "classes/" . $tszhDBType . "/tszh_period.php",
		// ������� �����
		"CTszhAccount" => "classes/" . $tszhDBType . "/tszh_account.php",
		"CTszhAccountPeriod" => "classes/" . $tszhDBType . "/tszh_account_period.php",
		"CTszhAccountHistory" => "classes/" . $tszhDBType . "/tszh_account_history.php",
		"CTszhAccountPeriodResult" => "classes/" . $tszhDBType . "/tszh_account_period_result.php",
		// ������
		"CTszhService" => "classes/" . $tszhDBType . "/tszh_service.php",
		// ���������� �� �������
		"CTszhCharge" => "classes/" . $tszhDBType . "/tszh_charge.php",
		"CTszhChargeResult" => "classes/" . $tszhDBType . "/tszh_charge_result.php",
		// ��������
		"CTszhMeter" => "classes/" . $tszhDBType . "/tszh_meter.php",
		"CTszhMeterValue" => "classes/" . $tszhDBType . "/tszh_meter_value.php",
		"CTszhMeterValueAdminResult" => "classes/" . $tszhDBType . "/tszh_meter_value_admin_result.php",
		"CTszhMeterValueResult" => "classes/" . $tszhDBType . "/tszh_meter_value_result.php",
		// ������
		"CTszhImport" => "classes/" . $tszhDBType . "/tszh_import.php",
		// ��������
		"CTszhSubscribe" => "classes/" . $tszhDBType . "/tszh_subscribe.php",
		// "CTszhSubscribeTable" => "classes/" . $tszhDBType . "/tszh_subscribe.php",
		// "CTszhUnsubscriptionTable" => "classes/" . $tszhDBType . "/tszh_subscribe.php",
		"CTszhIBlockPropertySubscribeSend" => "classes/" . $tszhDBType . "/tszh_subscribe.php",
		"CTszhBaseSubscribe" => "classes/" . $tszhDBType . "/subscribe/CTszhBaseSubscribe.php",
		"CTszhSubscribeNews" => "classes/" . $tszhDBType . "/subscribe/CTszhSubscribeNews.php",
		"CTszhSubscribeDebtors" => "classes/" . $tszhDBType . "/subscribe/CTszhSubscribeDebtors.php",
		"CTszhSubscribeMeterValues" => "classes/" . $tszhDBType . "/subscribe/CTszhSubscribeMeterValues.php",
		"CTszhSubscribeMeterVerifications" => "classes/" . $tszhDBType . "/subscribe/CTszhSubscribeMeterVerifications.php",
		"CTszhSubscribeReceipts" => "classes/" . $tszhDBType . "/subscribe/CTszhSubscribeReceipts.php",
		"CTszhSubscribeAddNews" => "classes/" . $tszhDBType . "/subscribe/CTszhSubscribeAddNews.php",
		"CTszhSubscribeImportCharges" => "classes/" . $tszhDBType . "/subscribe/CTszhSubscribeImportCharges.php",
		"CTszhSubscribeImportAccounts" => "classes/" . $tszhDBType . "/subscribe/CTszhSubscribeImportAccounts.php",
		"CTszhSubscribePaySystem" => "classes/" . $tszhDBType . "/subscribe/CTszhSubscribePaySystem.php",
		"CTszhSubscribeAfterTszhDemoExpired" => "classes/" . $tszhDBType . "/subscribe/CTszhSubscribeAfterTszhDemoExpired.php",
		"CTszhSubscribeBeforeTszhDemoExpired" => "classes/" . $tszhDBType . "/subscribe/CTszhSubscribeBeforeTszhDemoExpired.php",
		"ITszhSubscribeConditionalInstall" => "classes/" . $tszhDBType . "/subscribe/ITszhSubscribeConditionalInstall.php",
		"ITszhSubscribeExternal" => "classes/" . $tszhDBType . "/subscribe/ITszhSubscribeExternal.php",
		"ITszhSubscribeAgent" => "classes/" . $tszhDBType . "/subscribe/ITszhSubscribeAgent.php",
		"ITszhSubscribeExternalAgent" => "classes/" . $tszhDBType . "/subscribe/ITszhSubscribeExternalAgent.php",
		// �������
		"CTszhExport" => "classes/" . $tszhDBType . "/tszh_export.php",
		"CTszhExportCSV" => "classes/" . $tszhDBType . "/tszh_export_csv.php",
		// ����������
		"CTszhContractor" => "classes/" . $tszhDBType . "/tszh_contractors.php",
		"CTszhContractorResult" => "classes/" . $tszhDBType . "/tszh_contractors_result.php",
		"CTszhAccountContractor" => "classes/" . $tszhDBType . "/tszh_account_contractor.php",
		// �������������
		"CTszhAccountCorrections" => "classes/" . $tszhDBType . "/tszh_corrections.php",
		// ���������
		"CTszhAccountInstallments" => "classes/" . $tszhDBType . "/tszh_installments.php",
		// ������ ����� ������.������
		"CTszhYandexPayment" => "yandex_payment.php",
		// �������������� ���������� ������� ���
		"CTszhAutoUpdater" => "auto_updater.php",
		// ���������� ����������� �� ���������
		"CTszhFunctionalityController" => "controller.php",
		// ��������������� ������� ��� ��������� ������
		"CTszhPublicHelper" => "public.php",
		// ������ ��� ������� ������� ������
		"CTszhMultiValuesResult" => "classes/" . $tszhDBType . "/tszh_multivaluesresult.php",
		"CTszhMultiValuesAdminResult" => "classes/" . $tszhDBType . "/tszh_multivaluesadminresult.php",
		// �����-����
		"CTszhBarCode" => "classes/" . $tszhDBType . "/tszh_barcode.php",
		// ����� ��������� ������� ������
		"citrus_tszh" => "install/index.php",
		// "BaseJsonRpcClient" => "lib/external/BaseJsonRpcClient.php"
	)
);
if (!class_exists('BaseJsonRpcClient'))
{
	\Bitrix\Main\Loader::registerAutoLoadClasses(TSZH_MODULE_ID, array(
		"BaseJsonRpcClient" => "lib/external/BaseJsonRpcClient.php"
	));
}
/**
 * tszhGeneratePassword()
 * ������� ��� ��������� ������ ������������ (������������ ��� ������� ����� �������������)
 *
 * @param integer $password_min_length ����������� ����� ������
 * @param bool $password_punctuation ������������ ����� ���������� � ������ ����. ������� � ������
 *
 * @return string
 */
function tszhGeneratePassword(
	$password_min_length = 8,
	$password_punctuation = false
) {

	$password_chars = array(
		"abcdefghijknmpqrstuvwxyz",
		"ABCDEFGHIJKNMPQRSTUVWXYZ",
		"23456789",
	);
	if ($password_punctuation)
	{
		$password_chars[] = ",.<>/?;:'\"[]{}\|`~!@#\$%^&*()-_+=";
	}
	$password = randString($password_min_length, $password_chars);

	return $password;
}

function ToUpperFirstChar($string)
{
	return ToUpper(substr($string, 0, 1)) . substr($string, 1);
}

/**
 * tszhPluralForm()
 * ����� ��������
 *
 * ������ - tszhPluralForm(1001, '�����', '�����', '������');
 *
 * @param integer $n �����
 * @param string $f1 ����� ��� ������������� �����
 * @param string $f2 ����� ��� ���-�� 2,3,4 � �.�.
 * @param string $f5 ����� ��� ���-�� 5,6,7 � �.�.
 *
 * @return string
 */
function tszhPluralForm($n, $f1, $f2, $f5)
{
	$n = abs($n) % 100;
	$n1 = $n % 10;
	if ($n > 10 && $n < 20)
	{
		return $f5;
	}
	if ($n1 > 1 && $n1 < 5)
	{
		return $f2;
	}
	if ($n1 == 1)
	{
		return $f1;
	}

	return $f5;
}

include_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszh/functions.php");
function tszhSendUsage($siteID)
{
	global $DB, $APPLICATION;

	static $arModuleStatus = Array(
		MODULE_NOT_FOUND => 'Not found',
		MODULE_INSTALLED => 'Installed',
		MODULE_DEMO => 'Demo',
		MODULE_DEMO_EXPIRED => 'Expired demo',
	);

	$module = CTszhFunctionalityController::GetEdition($edition);
	if (false === $module)
	{
		return;
	}

	$arSendInfo = Array(
		'v' => 1,
		'encoding' => SITE_CHARSET,
		'code' => $module,
		'demo' => in_array(CModule::IncludeModuleEx($module), Array(
			MODULE_DEMO,
			MODULE_DEMO_EXPIRED,
		)),
	);
	$bIsTszhSite = false;

	$arSendInfo["sites"] = Array();
	$rsSites = CSite::GetList($by = "sort", $order = "desc", Array());
	while ($arSite = $rsSites->Fetch())
	{
		$strSiteInfo = "[{$arSite['LID']}] {$arSite['NAME']} ({$arSite['DIR']}) ";
		$strSiteInfo .= trim(", " . implode(', ', Array(
				$arSite["SERVER_NAME"],
				$arSite["EMAIL"],
				$arSite["DOMAINS"],
			)), ', ');
		$arSendInfo["sites"][$arSite["LID"]] = $strSiteInfo;
	}

	$arSendInfo["org"] = Array();
	$rsTszh = CTszh::GetList(Array(), Array());
	while ($arTszh = $rsTszh->Fetch())
	{
		$arSite = array_key_exists($arTszh["SITE_ID"], $arSendInfo['sites']) ? $arSendInfo['sites'][$arTszh["SITE_ID"]] : '-';
		$arSendInfo["org"][] = Array(
			"ID" => $arTszh["NAME"],
			"Site" => $arSite,
		);
		if ($arTszh["SITE_ID"] == $siteID)
		{
			$bIsTszhSite = true;
		}
		if (array_key_exists($arTszh["SITE_ID"], $arSendInfo['sites']))
		{
			unset($arSendInfo['sites'][$arTszh["SITE_ID"]]);
		}
	}
	if (!$bIsTszhSite)
	{
		return false;
	} // won't send usage info

	include_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/classes/general/update_client_partner.php");
	if (class_exists('CUpdateClientPartner'))
	{
		$arSendInfo['key_code'] = md5("BITRIX" . CUpdateClientPartner::GetLicenseKey() . "LICENCE");
		$arSendInfo['key'] = CUpdateClientPartner::GetLicenseKey();
	}

	$arSendInfo['server']['IP'] = $_SERVER['SERVER_ADDR'];
	$arSendInfo['server']['Host'] = $_SERVER['HTTP_HOST'];
	$arSendInfo['server']['E-Mail'] = $_SERVER['SERVER_ADMIN'];
	$arSendInfo['server']['Name'] = $_SERVER['SERVER_NAME'];
	$arSendInfo['server']['Software'] = $_SERVER['SERVER_SOFTWARE'];

	$arSendInfo["modules"] = Array();
	$rsInstalledModules = CModule::GetList();
	while ($ar = $rsInstalledModules->Fetch())
	{

		$moduleID = $ar["ID"];

		$arModuleInfo = Array();
		$arModuleInfo["ID"] = $moduleID;
		$arModuleInfo["Date installed"] = $ar["DATE_ACTIVE"];

		// supress Warning: Module 'citrus.tszh' is in loading progress in /bitrix/modules/main/lib/loader.php
		$nModuleStatus = @CModule::IncludeModuleEx($moduleID);
		if (array_key_exists($nModuleStatus, $arModuleStatus))
		{
			$obModule = CModule::CreateModuleObject($moduleID);
			if (is_object($obModule))
			{
				$arModuleInfo["Name"] = $obModule->MODULE_NAME . " (" . $obModule->MODULE_VERSION . ")";
			}
			$arModuleInfo["Status"] = $arModuleStatus[$nModuleStatus];
		}
		else
		{
			$arModuleInfo["Status"] = 'unknown';
		}

		$arSendInfo["modules"][] = $arModuleInfo;
	}

	$arSite = $APPLICATION->GetSiteByDir();
	$arSendInfo['admin_email'] = $arSendInfo['mail'] = strlen($arSite['EMAIL']) > 0 ? $arSite['EMAIL'] : COption::GetOptionString("main", "email_from");

	$arSendInfo['options'] = Array();
	$rsOptions = $DB->Query("select module_id, name, value from b_option where module_id = 'citrus.tszh' or (module_id='main' and (name like 'new_%' or name like 'server_%')) order by module_id, name asc", true);
	while (is_object($rsOptions) && $arOption = $rsOptions->Fetch())
	{
		$arSendInfo['options'][] = $arOption;
	}

	$http = new CHTTP();
	$http->follow_redirect = true;
	$http->http_timeout = 5;
	$result = $http->Post('http://www.vdgb-soft.ru/lic.php', Array(
		'notify' => 1,
		'info' => serialize(array_merge($arSendInfo, Array('server' => $_SERVER))),
	));

	return $result == 'ok';
}

function tszhNotify($siteID)
{
	if (strlen($siteID) <= 0)
	{
		return;
	}

	$nLastTry = abs(COption::GetOptionInt('citrus.tszh', 'tszh.notify.lasttry', 0, $siteID));
	$bNextTry = time() - $nLastTry >= 2 * 60 * 60 || $nLastTry > time() || $nLastTry == 0;

	if ($bNextTry)
	{
		COption::SetOptionInt('citrus.tszh', 'tszh.notify.lasttry', time(), '', $siteID);
		tszhSendUsage($siteID);
	}
}

/**
 * ����� ������� ��� ���������������� �����
 *
 * @param array $arFields ������ � ��������� �����
 *
 * @return array
 */
function TszhAdminFilterParams($arFields)
{
	$ar = Array("find", "find_types");
	foreach ($arFields as $arField)
	{
		switch ($arField['type'])
		{
			case 'int':
			case 'float':
			case 'date':
			case 'datetime':
				$ar[] = 'find_' . strtolower($arField['id']) . '_from';
				$ar[] = 'find_' . strtolower($arField['id']) . '_to';
				break;
			case 'string':
			default:
				$ar[] = 'find_' . strtolower($arField['id']);
				break;
		}
	}

	return $ar;
}

/**
 * ����� ������� ��� ���������������� ����� (������ 2)
 *
 * @param array $arFields ������ � ��������� �����
 *
 * @return array
 */
function TszhAdminFilterParamsV2($arFields, $arDefaults = array())
{
	$arFilter = array();
	foreach ($arFields as $arField)
	{
		$arFilterItem = array(
			'id' => $arField['id'],
			'name' => $arField['content'],
			'filterable' => '',
		);

		if (in_array($arField['id'], $arDefaults))
		{
			$arFilterItem['default'] = true;
		}

		switch ($arField['type'])
		{
			case 'date':
			case 'datetime':
				$arFilterItem['type'] = 'date';
				break;
			case 'list':
				$arFilterItem['type'] = 'list';
				// $arFilterItem['params'] = array('multiple' => 'Y');
				$arFilterItem['items'] = $arField['items'];
				break;
			case 'int':
			case 'float':
				$arFilterItem['type'] = 'number';
				break;
			case 'user':
				$arFilterItem['type'] = 'custom';
				$arFilterItem['value'] = '<form id="formFindUser" name="formFindUser">' . FindUserID($arField['id'], '', '', 'formFindUser') . '</form>';
				break;
			case 'string':
				$arFilterItem['quickSearch'] = '';
				break;
			default:
				break;
		}

		$arFilter[] = $arFilterItem;
	}

	return $arFilter;
}

/**
 * ����� ������� ��� ���������������� �����
 *
 * @param array $arFindType ������ � ��������� �����
 *
 * @return void
 */
function TszhShowShowAdminFilter($arFields)
{
	$arFindTypes = Array("ID" => "ID");
	?>
	<tr>
		<td><b><?=GetMessage("TSZH_FIND")?>:</b></td>
		<td>
			<input type="text" size="25" name="find" value="<? echo htmlspecialcharsbx($GLOBALS['find']) ?>"
			       title="<?=GetMessage("TSZH_FIND_TITLE")?>">
			<?
			$arSelectBox = Array();
			foreach ($arFindTypes as $id => $val)
			{
				$arSelectBox["REFERENCE"][] = $val;
				$arSelectBox["REFERENCE_ID"][] = $id;
			}
			echo SelectBoxFromArray("find_types", $arSelectBox, $GLOBALS['find_types'], "", "");
			?>
		</td>
	</tr>
	<?
	foreach ($arFields as $arField)
	{
		$fieldName = 'find_' . strtolower($arField['id']);
		?>
		<tr>
			<td><?=$arField['content']?></td>
			<td>
				<?
				if (is_set($arField['filterHtml']))
				{
					echo $arField['filterHtml'];
				}
				else
					switch ($arField['type'])
					{
						case 'account':
						case 'service':
							echo tszhLookup($fieldName, htmlspecialcharsbx($GLOBALS[$fieldName]), Array(
								"type" => $arField["type"],
								"formName" => 'form_filter',
							));
							break;

						case 'user':
							echo FindUserID($fieldName, htmlspecialcharsbx($GLOBALS[$fieldName]), "", "form_filter");
							break;

						case 'int':
						case 'float':
							?>
							<?=GetMessage("TSZH_FILTER_FROM")?>
							<input type="text" name="<?=$fieldName?>_from" size="10"
							       value="<? echo htmlspecialcharsbx($GLOBALS[$fieldName . '_from']) ?>">
							<?=GetMessage("TSZH_FILTER_TO")?>
							<input type="text" name="<?=$fieldName?>_to" size="10"
							       value="<? echo htmlspecialcharsbx($GLOBALS[$fieldName . '_to']) ?>">
							<?
							break;

						case 'list':
							$arItems = Array(
								"REFERENCE_ID" => Array(''),
								"REFERENCE" => Array(
									GetMessage("TSZH_FILTER_LIST_ALL"),
								),
							);
							if (is_array($arField['items']))
							{
								foreach ($arField['items'] as $id => $val)
								{
									$arItems["REFERENCE_ID"][] = $id;
									$arItems["REFERENCE"][] = $val;
								}
							}
							elseif (is_object($arField['items']))
							{
								while ($ar = $arField['items']->GetNext())
								{
									$arItems["REFERENCE_ID"][] = $ar['ID'];
									$arItems["REFERENCE"][] = '[' . $ar['ID'] . '] ' . $ar['NAME'];
								}
							}
							echo SelectBoxFromArray($fieldName, $arItems, $GLOBALS[$fieldName], "", "");
							?>

							<?
							break;

						case 'checkbox':
							$arItems = Array(
								"REFERENCE_ID" => Array('', 'Y', "N"),
								"REFERENCE" => Array(
									GetMessage("TSZH_FILTER_LIST_ALL"),
									GetMessage("TSZH_YES"),
									GetMessage("TSZH_NO"),
								),
							);
							echo SelectBoxFromArray($fieldName, $arItems, $GLOBALS[$fieldName], "", "");
							?>

							<?
							break;

						case 'date':
						case 'datetime':
							echo CalendarPeriod($fieldName . '_from', htmlspecialcharsbx($GLOBALS[$fieldName . '_from']), $fieldName . '_to', htmlspecialcharsbx($GLOBALS[$fieldName . '_to']), "form_filter", "Y");
							break;

						case 'string':
						default:
							?>
							<input type="text" name="find_<?=strtolower($arField['id'])?>" size="40"
							       value="<? echo htmlspecialchars($GLOBALS['find_' . strtolower($arField['id'])]) ?>">
							<?
							break;
					}
				?>
			</td>
		</tr>
		<?
	}
}

/**
 * ������������ ���������� ������� ��� ���������������� �����
 *
 * @param array $arFilter �������������� ������ � ��������
 * @param array $arFields ������ � ��������� �����
 *
 * @return void
 */
function TszhAdminMakeFilter(&$arFilter, $arFields)
{
	global $DB;

	if ($GLOBALS['find'] != "" && ToLower($GLOBALS['find_types']) == "id")
	{
		if (IntVal($GLOBALS['find']) > 0)
		{
			$arFilter["ID"] = IntVal($GLOBALS['find']);
		}
		elseif (IntVal($GLOBALS['find_id']) > 0)
		{
			$arFilter["ID"] = IntVal($GLOBALS['find_id']);
		}
	}
	else
	{
		if (IntVal($GLOBALS['find_id']) > 0)
		{
			$arFilter["ID"] = IntVal($GLOBALS['find_id']);
		}
	}

	foreach ($arFields as $arField)
	{
		$varName = 'find_' . strtolower($arField['id']);
		$field = $arField['id'];

		switch ($arField['type'])
		{
			case 'int':
			case 'float':
				$val = &$GLOBALS[$varName];
				if (isset($val) && strlen($val) > 0 && is_numeric($val))
				{
					$arFilter[$field] = $arField['type'] == 'float' ? FloatVal($val) : IntVal($val);
				}
				else
				{
					$val = &$GLOBALS[$varName . '_from'];
					if (isset($val) && strlen($val) > 0 && is_numeric($val))
					{
						$arFilter['>=' . $field] = $arField['type'] == 'float' ? FloatVal($val) : IntVal($val);
					}
					$val = &$GLOBALS[$varName . '_to'];
					if (isset($val) && strlen($val) > 0 && is_numeric($val))
					{
						$arFilter['<=' . $field] = $arField['type'] == 'float' ? FloatVal($val) : IntVal($val);
					}
				}
				break;

			case 'date':
			case 'datetime':
				$bFullDate = $arField['type'] == 'datetime';
				$val = &$GLOBALS[$varName . '_from'];
				if (isset($val) && strlen($val) > 0 && $DB->IsDate($val, false, false, $bFullDate ? "FULL" : "SHORT"))
				{
					$arFilter['>=' . $field] = $val;
				}
				$val = &$GLOBALS[$varName . '_to'];
				if (isset($val) && strlen($val) > 0 && $DB->IsDate($val, false, false, $bFullDate ? "FULL" : "SHORT"))
				{
					$arFilter['<=' . $field] = $val;
				}

				$val = &$GLOBALS[$varName . '_from_DAYS_TO_BACK'];
				if (isset($val) && strlen($val) > 0 && is_numeric($val))
				{
					$arFilter['>=' . $field] = ConvertTimestamp($val > 0 ? strtotime('-' . IntVal($val) . ' days') : time());
				}
				break;

			case 'string':
				$val = &$GLOBALS[$varName];
				if (isset($val) && strlen(trim($val)) > 0)
				{
					$arFilter['%' . $field] = $val;
				}
				break;

			case 'list':
			case 'checkbox':
			default:
				$val = &$GLOBALS[$varName];
				if (isset($val) && strlen(trim($val)) > 0)
				{
					$arFilter[$field] = $val;
				}
				break;
		}
	}
}

tszhNotify(SITE_ID);

if (!defined("ADMIN_SECTION"))
{
	global $arrAnnouncementsFilter;
	$arrAnnouncementsFilter = CTszhPublicHelper::MakeAnnouncementsFilter();
}

CJSCore::RegisterExt('tszh_datepicker', array(
	'js' => '/bitrix/js/citrus.tszh/tszh_datepicker.js',
	'css' => '/bitrix/js/citrus.tszh/css/tszh_datepicker.css',
	'lang' => '/bitrix/modules/citrus.tszh/lang/' . LANGUAGE_ID . '/datepicker_js.php',
	'rel' => array('popup', 'ajax', 'fx', 'ls', 'date', 'json'),
));

CJSCore::RegisterExt('tszh_tabs', array(
	'js' => '/bitrix/js/citrus.tszh/tszh_tabs.js',
	'css' => '/bitrix/js/citrus.tszh/css/tszh_tabs.css',
	'rel' => array('jquery', 'ajax'),
));

/*
 * ��������� ���������� ��� ����������� �����������; (�������, email � ��.)
 */
CJSCore::RegisterExt('tszh_fancybox', array(
	'js' => '/bitrix/js/citrus.tszh/fancybox/jquery.fancybox.pack.js',
	'css' => '/bitrix/js/citrus.tszh/fancybox/jquery.fancybox.css',
	'rel' => array('jquery'), //������������� ��������� jquery
));
?>