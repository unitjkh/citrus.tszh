<?//IncludeModuleLangFile(__FILE__);

/**
 * CTszhAutoUpdater
 * ����� ��� �������������� �������� � ��������� ���������� ���-�������
 *
 */
class CTszhAutoUpdater
{
	const UPDATE_INTERVAL = 3;

	const ERROR_EMAIL = "backup@vdgb-soft.ru";
	const ERROR_SUBJECT = "AutoUpdater error on #URL#";

	private static $classEnabled = null;

	private static $arTszhModules = array(
		"citrus.tszh", 
		"citrus.tszhpayment", 
		"citrus.tszhtickets",

		"vdgb.portaljkh",

		"vdgb.tszhvote",
		"vdgb.tszhpayu", 

		"vdgb.tszhone",
		"citrus.tszhtwo",
		"citrus.tszhthree",

		"citrus.tszhstart", "vdgb.tszhstartua",
		"citrus.tszhstandard", "vdgb.tszhstandardua",
		"citrus.tszhsb", "vdgb.tszhsbua",
		"citrus.tszhe", "vdgb.tszheua",
		"citrus.tszhb", "vdgb.tszhbua",
	);

	/**
	 * CTszhAutoUpdater::getMethodCallString()
	 * ���������� ������ � ������ ������ �, � ������� �������, ������� �������� ��� ����������� ����������
	 * ������������ ��� ������� � ���, � ����� � �������
	 *
	 * @param string $method	������������ ������
	 * @param array $arParams	������ ����������� ���������� ������ (���� � ��������)
	 * @return string
	 */
	private static function getMethodCallString($method, $arParams)
	{
		$arParamValues = array();
		foreach ($arParams as $param)
		{
			$arParamValues[] = var_export($param, true);
		}

		return $method . "(" . implode(", ", $arParamValues) . ");";
	}

	/**
	 * CTszhAutoUpdater::requireUpdateClass()
	 * ���������� ����������� ����� CUpdateClientPartner
	 *
	 * @return
	 */
	private static function requireUpdateClass()
	{
		require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/update_client_partner.php");

		if (class_exists("CUpdateClientPartner"))
		{
			self::$classEnabled = true;
		}
		else
		{
			self::$classEnabled = false;
			throw new Exception("requireUpdateClass() ERROR: class CUpdateClientPartner not exists", -1);
		}
	}

	/**
	 * CTszhAutoUpdater::addMessage2Log()
	 * �������� ����� CUpdateClientPartner::AddMessage2Log(): ����� ��������� � ��� ���� ������� ����������. ������ ���, ���� �����.
	 *
	 * @param string $sText			����� ���������
	 * @param string $sErrorCode	��� ���������
	 * @return 
	 */
	private static function addMessage2Log($sText, $sErrorCode = "")
	{
		self::requireUpdateClass();

		$sText = __CLASS__ . ": " . $sText;
		CUpdateClientPartner::addMessage2Log($sText, $sErrorCode);
	}

	/**
	 * CTszhAutoUpdater::sendError()
	 * �������� ������������� e-mail'� � ��������� ������ ������ � ����� (updater_partner.log) ������� ����������� ����������
	 *
	 * @param string $errorMessage	��������� �� ������
	 * @return bool					true � ������ ������, ����� false
	 */
	private static function sendError($errorMessage)
	{
		// TODO: ���������� ������ �� ������� � �����-�� ������, � �� �� e-mail, ����� ���� ����������� ����� ����� �� ����������� � �����������
		$charset = "windows-1251";

		if (defined(SITE_CHARSET) || defined(LANG_CHARSET))
		{
			if (defined(SITE_CHARSET))
				$siteCharset = SITE_CHARSET;
			else
				$siteCharset = LANG_CHARSET;

			$res = \Bitrix\Main\Text\Encoding::convertEncoding($errorMessage, $siteCharset, $charset);
			if ($res)
				$errorMessage = $res;
		}

		// �� ����� ���������� e-mail ��� �������� ��������� �������
		$arSkipErrors = array(
			// bitrix
			'#\bSYS_ERROR_AB_ACTIVE\b#i',	// ����� ������ ��������� ���������� � ������������ ������������
			'#\bUPSD01\b#i',				// ������ ����� ������� ����������
			'#\bGNSU02\b#i',				// ������ ����� ������� ����������
			'#\bUPSD02\b#i',				// ������������ ����� �� ������� ����������
			'#\bELVL001\b#i',				// ������ �������� ����������
			'#\bSITE_LICENSE_VIOLATION\b#i',// ��������� � ��� ���������� ������ �������� ��������

			// updater's tszhIsEditionUpdatable()
			'#\bCITRUS_TIEU_GET_UPDATES_LIST_FAIL\b#i',		// ������ ��� ��������� ������ �� ������� ����������
			'#\bCITRUS_TIEU_GET_UPDATES_LIST_INVALID\b#i',	// ������������ ����� ������� ����������
			'#\bCITRUS_TIEU_TSZH_EDITION_NOT_UPDATABLE\b#i',// ������ �������� �� ���������� ��� ���� ������ ��������� ��� ����������
		);
		foreach ($arSkipErrors as $errorPattern)
		{
			if (preg_match($errorPattern, $errorMessage))
				return true;
		}

		$siteUrl = $_SERVER["SERVER_NAME"];
		$fullSiteUrl = ($GLOBALS["APPLICATION"]->IsHTTPS() ? "https" : "http") . '://' . $siteUrl;

		$message = "Site: {$fullSiteUrl}\r\n";
		$message .= "License key: " . (self::$classEnabled ? md5("BITRIX".CUpdateClientPartner::GetLicenseKey()."LICENCE") : "<class CUpdateClientPartner not exists>") . "\r\n\r\n";
		$message .= "Error message: {$errorMessage}\r\n";

		$fileName = $_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/updater_partner.log";
		$hFile = null;
		if (!file_exists($fileName))
			$message .= "Error: log-file not exists.\r\n";
		if (!is_file($fileName) || !($hFile = fopen($fileName, "rb")))
			$message .= "Error: log-file not enabled.\r\n";

		$arFiles = array();
		if ($hFile)
		{
			$arFiles[] = array(
				"F_PATH" => $fileName,
				"F_LINK" => $hFile,
			);
		}

		// ������ � ������ ���� /bitrix/modules/updater_partner.log - http://dev.1c-bitrix.ru/community/webdev/user/11948/blog/1623/
		$from = COption::GetOptionString("main", "email_from", "admin@" . $_SERVER["SERVER_NAME"]);
		$to = self::ERROR_EMAIL;
		$subj = str_replace("#URL#", $siteUrl, self::ERROR_SUBJECT);
		$bcc = "";

		$from = trim($from, "\r\n");
		$to = trim($to, "\r\n");
		$subj = trim($subj, "\r\n");

		if (COption::GetOptionString("main", "convert_mail_header", "Y") == "Y")
		{
			$from = CAllEvent::EncodeMimeString($from, $charset);
			$to = CAllEvent::EncodeMimeString($to, $charset);
			$subj = CAllEvent::EncodeMimeString($subj, $charset);
		}

		$all_bcc = COption::GetOptionString("main", "all_bcc", "");
		if ($all_bcc != "")
		{
			$bcc = $all_bcc;
			$duplicate = "Y";
		}
		else
		{
			$duplicate = "N";
		}

		$un = strtoupper(uniqid(time()));
		$eol = CAllEvent::GetMailEOL();
		$head = $body = "";

		// header
		$head .= "Mime-Version: 1.0".$eol;
		$head .= "From: $from".$eol;
		if (COption::GetOptionString("main", "fill_to_mail", "N") == "Y")
			$header = "To: $to".$eol;
		$head .= "Reply-To: $from".$eol;
		$head .= "X-Priority: 3 (Normal)".$eol;
		if (strpos($bcc, "@") !== false)
			$head .= "BCC: $bcc".$eol;
		$head .= "Content-Type: multipart/mixed; ";
		$head .= "boundary=\"----".$un."\"".$eol.$eol;

		// body
		$body = "------".$un.$eol;
		if (true)
			$body .= "Content-Type:text/plain; charset=".$charset.$eol;
		else
			$body .= "Content-Type:text/html; charset=".$charset.$eol;
		$body .= "Content-Transfer-Encoding: 8bit".$eol.$eol;
		$body .= $message.$eol.$eol;

		foreach ($arFiles as $arF)
		{
			$body .= "------".$un.$eol;
			$body .= "Content-Type: application/octet-stream; name=\"".basename($arF["F_PATH"])."\"".$eol;
			$body .= "Content-Disposition:attachment; filename=\"".basename($arF["F_PATH"])."\"".$eol;
			$body .= "Content-Transfer-Encoding: base64".$eol.$eol;
			$body .= chunk_split(base64_encode(fread($arF["F_LINK"], filesize($arF["F_PATH"])))).$eol.$eol;
		}
		$body .= "------".$un."--";

		// send
		//if (!defined("ONLY_EMAIL") || $to == ONLY_EMAIL)
			return bxmail($to, $subj, $body, $head, COption::GetOptionString("main", "mail_additional_parameters", ""));
	}

	/**
	 * CTszhAutoUpdater::shiftModule()
	 * ���������� ������ ������� �� ������� ID �������, ������ ��� �� �������
	 * ������������ ��� ������������ �� ��������� ���������� ������
	 *
	 * @param array $arModules	������ ID �������
	 * @return ID ������ ��� false ���� $arModules ������
	 */
	private static function shiftModule(&$arModules)
	{
		if (!is_array($arModules) || empty($arModules))
			return false;

		return array_shift($arModules);
	}

	/**
	 * CTszhAutoUpdater::loadModuleUpdate()
	 * �������� ���������� ��������� ������
	 *
	 * @param string $moduleID				ID ������, ��� �������� ���� ��������� ����������
	 * @param string $errMessage			��������� �� ������
	 * @param array $arUpdateDescription	�������� ������������ ����������
	 * @return char							��� �������
	 */
	private static function loadModuleUpdate($moduleID, &$errMessage, &$arUpdateDescription)
	{
		$callString = self::getMethodCallString(__FUNCTION__, array(
			$moduleID,
		));
		self::addMessage2Log("enter {$callString}");

		if (!in_array($moduleID, self::$arTszhModules))
			throw new Exception("{$callString} ERROR: invalid moduleID '{$moduleID}'");

		if (!CModule::IncludeModule($moduleID))
			throw new Exception("{$callString} ERROR: module '{$moduleID}' not found");

		self::requireUpdateClass();

		$res = CUpdateClientPartner::LoadModulesUpdates($errMessage, $arUpdateDescription, LANGUAGE_ID, "Y", array($moduleID), true);
		self::addMessage2Log("CUpdateClientPartner::LoadModulesUpdates() result: {$res}" . ($res == "E" ? "; ERROR: {$errMessage}" : ""));

		self::addMessage2Log("exit " . __FUNCTION__);
		return $res;
	}

	/**
	 * CTszhAutoUpdater::unGzipUpdate()
	 * ���������� ������������ ����������
	 *
	 * @param string $updateDir	�������, � ������� ���������� ���� �����������
	 * @param string $strError	��������� �� ������
	 * @return bool				true � ������ ������, ����� false
	 */
	private static function unGzipUpdate(&$updateDir, &$strError)
	{
		$callString = self::getMethodCallString(__FUNCTION__, array());
		self::addMessage2Log("enter {$callString}");
		self::requireUpdateClass();

		$updateDir = "";
		$res = CUpdateClientPartner::UnGzipArchive($updateDir, $strError, true);
		if (!$res)
		{
			self::addMessage2Log("CUpdateClientPartner::UnGzipArchive() ERROR: {$strError}");
		}

		self::addMessage2Log("exit " . __FUNCTION__);
		return $res;
	}

	/**
	 * CTszhAutoUpdater::checkUnpackedUpdate()
	 * �������� �������������� ����������
	 *
	 * @param string $updateDir	�������, � ������� ���������� ���� �����������
	 * @param string $strError	��������� �� ������
	 * @return bool				true � ������ ������, ����� false
	 */
	private static function checkUnpackedUpdate($updateDir, &$strError)
	{
		$callString = self::getMethodCallString(__FUNCTION__, array(
			$updateDir,
		));
		self::addMessage2Log("enter {$callString}");
		self::requireUpdateClass();

		$res = CUpdateClientPartner::CheckUpdatability($updateDir, $strError);
		if (!$res)
		{
			self::addMessage2Log("CUpdateClientPartner::CheckUpdatability() ERROR: {$strError}");
		}

		self::addMessage2Log("exit " . __FUNCTION__);
		return $res;
	}

	/**
	 * CTszhAutoUpdater::installUpdate()
	 * ��������� �������������� ����������
	 *
	 * @param string $updateDir	�������, � ������� ���������� ���� �����������
	 * @param string $strError	��������� �� ������
	 * @return bool				true � ������ ������, ����� false
	 */
	private static function installUpdate($updateDir, &$strError)
	{
		$callString = self::getMethodCallString(__FUNCTION__, array(
			$updateDir,
		));
		self::addMessage2Log("enter {$callString}");
		self::requireUpdateClass();

		$res = CUpdateClientPartner::UpdateStepModules($updateDir, $strError);
		if ($res)
			COption::SetOptionString("main", "update_system_update", Date($GLOBALS["DB"]->DateFormatToPHP(CSite::GetDateFormat("FULL")), time()));

		self::addMessage2Log("exit " . __FUNCTION__);
		return $res;
	}

	/**
	 * CTszhAutoUpdater::updateModuleAgent()
	 * �������-����� �������������� �������� � ��������� ���������� ��������� ���-������
	 *
	 * @param array $arNextModules	������ ID �������, ��� ������� ���� �������� � ���������� ���������� � ��������� ��������
	 * @param int $moduleID			ID ������, ��� �������� ���� �������� � ���������� ���������� � ������� �������
	 * @param string $action		��������, ������� ���������� ��������� � �����������: load - ������� � �����������, install - ����������
	 * @param string $updateDir		�������, � ������� ���������� ���� ����������� (��� $action == "install")
	 * @param array $arItemsUpdated	������ ������ ��������� ���������� ���� moduleID => version (��� $action == "install")
	 * @return string
	 */
	public function updateModuleAgent($arNextModules, $moduleID, $action = "load", $updateDir = "", $arItemsUpdated = array())
	{
		global $USER;
		$bTmpUser = false;
		if (!is_object($USER)) {
			$bTmpUser = true;
			$USER = new CUser();
		}

		$resultString = false;

		$callString = self::getMethodCallString(__FUNCTION__, array(
			$arNextModules,
			$moduleID,
			$action,
			$updateDir,
		));

		try
		{
			self::requireUpdateClass();

			if (!is_array($arNextModules))
				throw new Exception("{$callString} ERROR: invalid argument \$arNextModules", -1);

			if (!in_array($moduleID, self::$arTszhModules))
				throw new Exception("{$callString} ERROR: invalid argument \$moduleID", -1);

			if (!in_array($action, array("load", "install")))
				throw new Exception("{$callString} ERROR: invalid argument \$action", -1);

			// �������� � ���������� ����������
			if ($action == "load")
			{
				$res = self::loadModuleUpdate($moduleID, $errMessageLoad, $arUpdateDescription);
				$arStepUpdateInfo = $arUpdateDescription;
				switch ($res)
				{
					case "S":
						$resultString = self::getMethodCallString(__METHOD__, array(
							$arNextModules,
							$moduleID,
							"load",
						));
						break;

					case "E":
						self::sendError($errMessageLoad);
					case "F":
						$nextModuleID = self::shiftModule($arNextModules);
						if ($nextModuleID === false)
							$resultString = "";
						else
							$resultString = self::getMethodCallString(__METHOD__, array(
								$arNextModules,
								$nextModuleID,
								"load",
							));
						break;

					case "U":
						$updateDir = "";
						$res = self::unGzipUpdate($updateDir, $errorMessage);

						if ($res)
							$res = self::checkUnpackedUpdate($updateDir, $errorMessage);

						if ($res && isset($arStepUpdateInfo["DATA"]["#"]["ERROR"]))
						{
							$res = false;
							$errorMessage = "";
							for ($i = 0, $cnt = count($arStepUpdateInfo["DATA"]["#"]["ERROR"]); $i < $cnt; $i++)
								$errorMessage .= "[".$arStepUpdateInfo["DATA"]["#"]["ERROR"][$i]["@"]["TYPE"]."] ".$arStepUpdateInfo["DATA"]["#"]["ERROR"][$i]["#"];
							self::addMessage2Log("ERRORS in update: {$errorMessage}");
						}

						if ($res && isset($arStepUpdateInfo["DATA"]["#"]["NOUPDATES"]))
						{
							CUpdateClientPartner::ClearUpdateFolder($_SERVER["DOCUMENT_ROOT"] . "/bitrix/updates/" . $updateDir);
						}

						if ($res)
						{
							$arItemsUpdated = array();
							$arItemsUpdatedDescr = array();
							if (isset($arStepUpdateInfo["DATA"]["#"]["ITEM"]))
							{
								for ($i = 0, $cnt = count($arStepUpdateInfo["DATA"]["#"]["ITEM"]); $i < $cnt; $i++)
								{
									$arItemsUpdated[$arStepUpdateInfo["DATA"]["#"]["ITEM"][$i]["@"]["NAME"]] = $arStepUpdateInfo["DATA"]["#"]["ITEM"][$i]["@"]["VALUE"];
									$arItemsUpdatedDescr[$arStepUpdateInfo["DATA"]["#"]["ITEM"][$i]["@"]["NAME"]] = $arStepUpdateInfo["DATA"]["#"]["ITEM"][$i]["@"]["DESCR"];
								}
							}

							$resultString = self::getMethodCallString(__METHOD__, array(
								$arNextModules,
								$moduleID,
								"install",
								$updateDir,
								$arItemsUpdated,
							));
						}
						else
						{
							self::sendError($errorMessage);

							$nextModuleID = self::shiftModule($arNextModules);
							if ($nextModuleID === false)
								$resultString = "";
							else
								$resultString = self::getMethodCallString(__METHOD__, array(
									$arNextModules,
									$nextModuleID,
									"load",
								));
						}
						break;
				}
			}

			// ��������� ����������
			if ($action == "install")
			{
				$res = self::installUpdate($updateDir, $errorMessage);
				if ($res)
				{
					foreach ($arItemsUpdated as $key => $value)
					{
						/*$strModuleDescr = "";
						if (strlen($arItemsUpdatedDescr[$key]) > 0)
						{
							$strModuleDescr = "<br>".htmlspecialcharsback($arItemsUpdatedDescr[$key]);
							$strModuleDescr = preg_replace("#</?pre>#i", " ", $strModuleDescr);
							$strModuleDescr = preg_replace("/[\s\n\r]+/", " ", $strModuleDescr);
							$strModuleDescr = addslashes($strModuleDescr);
						}*/
        
						self::AddMessage2Log("Updated: " . $key . ((StrLen($value) > 0) ? " (" . $value . ")" : "") /*. $strModuleDescr*/);
					}

					$resultString = self::getMethodCallString(__METHOD__, array(
						$arNextModules,
						$moduleID,
						"load",
					));
				}
				else
				{
					self::sendError($errorMessage);

					$nextModuleID = self::shiftModule($arNextModules);
					if ($nextModuleID === false)
						$resultString = "";
					else
						$resultString = self::getMethodCallString(__METHOD__, array(
							$arNextModules,
							$nextModuleID,
							"load",
						));
				}
			}
		}
		catch (Exception $e)
		{
			$eCode = $e->getCode();

			if (self::$classEnabled)
				self::addMessage2Log($e->getMessage());

			$resultString = "";
			if ($eCode != -1)
			{
				$nextModuleID = self::shiftModule($arNextModules);
				if ($nextModuleID !== false)
				{
					$resultString = self::getMethodCallString(__METHOD__, array(
						$arNextModules,
						$nextModuleID,
						"load",
					));
				}
			}
		}

		if ($bTmpUser) {
			unset($USER);
		}
		return $resultString;
	}

	/**
	 * CTszhAutoUpdater::updateAgent()
	 * �������-����� (������� �����) �������������� �������� � ��������� ���������� ���-�������
	 *
	 * @param array $arModules ������ ID �������, ��� ������� ���� �������� � ���������� ����������
	 * @return string
	 */
	public function updateAgent($arModules = array())
	{
		global $USER;
		$bTmpUser = false;
		if (!is_object($USER)) {
			$bTmpUser = true;
			$USER = new CUser();
		}

		$resultString = self::getMethodCallString(__METHOD__, array(
			$arModules,
		));

		$arModules = array("citrus.tszhpayment");

		try
		{
			if (!tszhCheckMinEdition("start"))
				throw new Exception("invalid edition");

			$arReqModules = array_unique($arModules);
			if (empty($arReqModules))
				$arReqModules = self::$arTszhModules;
    
			$arUnknownModules = array();
			$arModulesTmp = array();
			foreach ($arReqModules as $key => $moduleID)
			{
				$idx = array_search($moduleID, self::$arTszhModules);
				if ($idx === false)
				{
					$arUnknownModules[] = $moduleID;
					unset($arReqModules[$key]);
				}
				elseif (CModule::IncludeModule($moduleID))
				{
					$arModulesTmp[$idx] = $moduleID;
				}
				else
				{
					unset($arReqModules[$key]);
				}
			}
    
			if (!empty($arUnknownModules))
				self::addMessage2Log(__FUNCTION__ . " WARNING: unknown modules: " . implode(", ", $arUnknownModules));

			if (empty($arReqModules))
				return $resultString;
    
			ksort($arModulesTmp);
    
			$agentModuleID = array_shift($arModulesTmp);
			$agentName = __CLASS__ . "::updateModuleAgent";
			$agentString = self::getMethodCallString($agentName, array(
				$arModulesTmp,
				$agentModuleID,
			));

			$res = CAgent::GetList(array(), array("MODULE_ID" => "citrus.tszh", "NAME" => "{$agentName}(%"))->Fetch();
			if (!$res)
			{
				$newAgentID = CAgent::AddAgent(
					$agentString,
					"citrus.tszh", 
					"N", 
					self::UPDATE_INTERVAL
				);
				if ($newAgentID <= 0)
				{
					$errMsg = __FUNCTION__ . " ERROR: could not add {$agentString}";
					self::addMessage2Log($errMsg);
					self::sendError($errMsg);
				}
			}
			else
			{
				self::addMessage2Log(__FUNCTION__ . " NOTICE: {$agentName} allready running");
			}
		}
		catch (Exception $e)
		{
		}

		if ($bTmpUser) {
			unset($USER);
		}
		return $resultString;
	}
}

