<?IncludeModuleLangFile(__FILE__);

/**
 * CTszhYandexPayment
 * ������ ����� ��� � ��������� ������� ������.������
 */
class CTszhYandexPayment
{
	const SCID = "5512";
	const PATH_RECEIPT_PRINT_DEFAULT = "/personal/receipt/?period=#PERIOD_ID#&print=Y";

	/**
	 * CTszhYandexPayment::ShowPaymentForm()
	 * ���������� ����� ������ � ������.�������
	 *
	 * @param mixed $accountPeriodID - ID ������ �������� ����� �� ������ (���������)
	 * @param mixed $accountID - ID �������� ����� (���� �� ����� $accountPeriodID - ��������, ���� ��� �/� ��� ������������ ���������)
	 * @param mixed $paymentDescr - ������ ���������������� �������� (����������) ������� (���� �� �����, �� ����� ������������ �������� �� ���������)
	 * @param mixed $receiptPath - ������ URL ������ ��������� (���� �� �����, �� ����� ������������ �������� �� ���������)
	 * @return
	 */
	public static function ShowPaymentForm($accountPeriodID, $accountID, $paymentDescr = false, $receiptPath = false)
	{
		$accountPeriodID = intval($accountPeriodID);
		$accountID = intval($accountID);
		if ($paymentDescr === false)
			$paymentDescr = GetMessage('ORDER_DESCR_VALUE');
		if ($receiptPath === false)
			$receiptPath = self::PATH_RECEIPT_PRINT_DEFAULT;

		$funcHtmlspecialcharsArray = function ($ar)
		{
			$arResult = array();
        
			foreach($ar as $key => $value)
				$arResult[$key] = htmlspecialcharsbx($value);
        
			return $arResult;
		};

		if ($accountPeriodID)
		{
			$arAccountPeriod = CTszhAccountPeriod::GetList(
				array(),
				array(
					"ID" => $accountPeriodID,
				),
				false,
				array("nTopCount" => 1),
				array("*")
			)->GetNext();
			if (!is_array($arAccountPeriod) || empty($arAccountPeriod))
			{
				ShowError(GetMessage("TYP_ERROR_ACCOUNT_PERIOD_NOT_FOUND"));
				return;
			}
		}
		elseif ($accountID)
		{
			$arAccount = CTszhAccount::GetList(
				array(),
				array(
					"ID" => $accountID,
				),
				false,
				array("nTopCount" => 1),
				array("*")
			)->GetNext();
			if (!is_array($arAccount) || empty($arAccount))
			{
				ShowError(GetMessage("TYP_ERROR_ACCOUNT_NOT_FOUND"));
				return;
			}
		}
		else
		{
			ShowError(GetMessage("TYP_ERROR_ACCOUNTPERIODID_AND_ACCOUNTID_NOT_PASSED"));
			return;
		}

		$arTszh = CTszh::GetByID(isset($arAccountPeriod["PERIOD_TSZH_ID"]) ? $arAccountPeriod["PERIOD_TSZH_ID"] : $arAccount["PERIOD_TSZH_ID"]);
		if (!is_array($arTszh) || empty($arTszh))
		{
			ShowError(GetMessage("TYP_ERROR_TSZH_NOT_FOUND"));
			return;
		}

		if (CTszh::isDirty($arTszh['ID']))
		{
			ShowError(GetMessage("TYP_ERROR_TSZH_MISSING_REQUIRED_FIELDS"));
			return;
		}

		if (isset($arAccountPeriod["DEBT_END"]))
			$Sum = number_format($arAccountPeriod["DEBT_END"], 2, '.', '');
		else
			$Sum = '';
		$scid = self::SCID;
		$orderID = ($accountPeriodID ? $accountPeriodID : $accountID);

		$tszhName = htmlspecialcharsbx($arTszh["NAME"]);
		$tszhINN = htmlspecialcharsbx($arTszh["INN"]);
		$tszhKPP = htmlspecialcharsbx($arTszh["KPP"]);
		$tszhBankAccount = htmlspecialcharsbx($arTszh["RSCH"]);
		$tszhBankName = htmlspecialcharsbx($arTszh["BANK"]);
		$tszhBankBIK = htmlspecialcharsbx($arTszh["BIK"]);

		$customerFIO = htmlspecialcharsbx(isset($arAccountPeriod["ACCOUNT_NAME"]) ? $arAccountPeriod["ACCOUNT_NAME"] : $arAccount["NAME"]);
		$customerAddress = htmlspecialcharsbx(isset($arAccountPeriod["ADDRESS_FULL"]) ? $arAccountPeriod["ADDRESS_FULL"] : $arAccount["ADDRESS_FULL"]);

		$serverName = ($GLOBALS["APPLICATION"]->IsHTTPS() ? "https://" : "http://") . $_SERVER["HTTP_HOST"];
		$successURL = htmlspecialcharsbx($serverName . $GLOBALS["APPLICATION"]->GetCurUri());
		$paySystemID = 0;
		$notifyURL = htmlspecialcharsbx("{$serverName}/payment_receive.php");

		$paymentDescr = str_replace('#ACCOUNT_NUMBER#', isset($arAccountPeriod["XML_ID"]) ? $arAccountPeriod["XML_ID"] : $arAccount["XML_ID"], $paymentDescr);

		if (isset($arAccountPeriod["PERIOD_DATE"]))
			$arPeriods = ToLower(CTszhPeriod::Format($arAccountPeriod["PERIOD_DATE"]));
		else
		{
			$arPeriods = array();
			for ($i=-1; $i<2; $i++)
			{
				$ts = strtotime("{$i} month");
				$arPeriods[] = GetMessage('TYP_M_' . date('m', $ts)) . ' ' . date('Y', $ts);
			}
		}
		$paymentDescr = str_replace(array('#PREV_MONTH#', '#CUR_MONTH#', '#NEXT_MONTH#'), $arPeriods, $paymentDescr);

		$arErrorMessages = array(
			"TYP_ERROR_JS_EMPTY_FIELD" => GetMessage("TYP_ERROR_JS_EMPTY_FIELD"),
			"TYP_ERROR_JS_MIN_SUM" => GetMessage("TYP_ERROR_JS_MIN_SUM"),
		);
		$arCheckFields = array(
			"sum" => GetMessage("TYP_FORM_TO_PAY"),
			"narrative" => GetMessage("TYP_FORM_ORDER_DESCR"),
		);?>
		<script type="text/javascript">
			function __YandexMoneyOnSubmit()          
			{
				var arCheckFields = <?=CUtil::PhpToJSObject($arCheckFields)?>;
				var arErrorMessages = <?=CUtil::PhpToJSObject($arErrorMessages)?>;
				var arErrors = [];

		        var sum = BX.util.trim(document.__YandexMoneyForm.sum.value.replace(',', '.'));

				for (var i in arCheckFields)
				{
					var value = BX.util.trim(document.__YandexMoneyForm[i].value);
					if (!value.length)
						arErrors.push(arErrorMessages["TYP_ERROR_JS_EMPTY_FIELD"].replace('#FIELD#', arCheckFields[i]));

					if (i == 'sum' && value.length)
					{
						sum = parseFloat(sum.replace(',', '.'));
						if (!sum || sum < 1)
							arErrors.push(arErrorMessages["TYP_ERROR_JS_MIN_SUM"]);
					}
				}

                if (arErrors.length) 
				{
                    alert(arErrors.join("\n"));
                    return false;
                }

				document.__YandexMoneyForm.payment_purpose.value = document.__YandexMoneyForm.narrative.value;
				document.__YandexMoneyForm.sum.value = sum;
				document.__YandexMoneyForm.netSum.value = document.__YandexMoneyForm.sum.value;
				return true;
			}
		</script>

		<style type="text/css">
			.__YandexMoneyForm .yandex-payment-label {
				display: inline-block;
				width: 150px;
				text-align: right;
			}
			.__YandexMoneyForm .footer-table {
				width: 475px !important;
				border: none;
				border-collapse: collapse;
			}
			.__YandexMoneyForm .footer-table td {
				border: none;
			}
			.__YandexMoneyForm .footer-table .left-td {
				width: 115px !important;
				padding-left: 40px;
			}
			.__YandexMoneyForm .footer-table .center-td {
				width: 100px !important;
			}
			.__YandexMoneyForm .footer-table .right-td {
				padding-left: 20px;
				width: 200px !important;
			}
		</style>
		<h3><?=GetMessage("TYP_FORM_HEADER")?></h3>
		<form name="__YandexMoneyForm" class="__YandexMoneyForm" action="https://money.yandex.ru/eshop.xml" method="post" target="_blank" onsubmit="return __YandexMoneyOnSubmit();">

			<font class="tablebodytext">
			<p>
			<label for="narrative" class="yandex-payment-label"><?=GetMessage("TYP_FORM_ORDER_DESCR")?>:</label> <input type="text" name="narrative" id="narrative" value="<?=$paymentDescr?>" size="50"><br>
    
			<input type="hidden" name="scid" value="<?=$scid?>">
    
			<input type="hidden" name="supplierName" value="<?=$tszhName?>">
			<input type="hidden" name="CustName" value="<?=$tszhName?>">
    
			<input type="hidden" name="supplierInn" value="<?=$tszhINN?>">
			<input type="hidden" name="CustINN" value="<?=$tszhINN?>">
    
			<input type="hidden" name="CustKPP" value="<?=$tszhKPP?>">
    
			<input type="hidden" name="supplierBankAccount" value="<?=$tszhBankAccount?>">
			<input type="hidden" name="CustAccount" value="<?=$tszhBankAccount?>">
    
			<input type="hidden" name="supplierBankName" value="<?=$tszhBankName?>">
    
			<?/*<input type="hidden" name="BankINN" value="<?=$arTszh["INN"]?>">*/?>
    
			<input type="hidden" name="supplierBankBik" value="<?=$tszhBankBIK?>">
			<input type="hidden" name="BankBIK" value="<?=$tszhBankBIK?>">
    
			<input type="hidden" name="payerName" value="<?=$customerFIO?>">
			<input type="hidden" name="payerAddress" value="<?=$customerAddress?>">
    
			<input type="hidden" name="payment_purpose" value="">
    
			<label for="sum" class="yandex-payment-label"><?=GetMessage("TYP_FORM_TO_PAY")?>:</label> <input type="text" name="sum" id="sum" value="<?=$Sum?>" size="15"> <?=GetMessage("TYP_FORM_RUB_ABBR")?><br>
			<input type="hidden" name="netSum" value="<?=$Sum?>">
    
			<input type="hidden" name="budgetDocNumber" value="0">
			<?//<input type="hidden" name="WDRegistry_15" value="30">?>
    
			<input type="hidden" name="orderID" value="<?=$orderID?>">
    
			<input type="hidden" name="notifyUrl" value="<?=$notifyURL?>">
			<input type="hidden" name="shopSuccessURL" value="<?=$successURL?>">
    
			<input type="hidden" name="paySystemID" value="<?=$paySystemID?>">

			<table class="footer-table">
				<tr>
					<td class="left-td">
						<a href="http://money.yandex.ru/" target="_blank"><img src="/images/yandex_money.png" width="100"></a>
					</td>
					<td class="center-td">
						<input type="submit" value="<?=GetMessage("TYP_FORM_BUTTON")?>">
					</td>
					<?if (isset($arAccountPeriod["PERIOD_ID"])):?>
						<td class="right-td">
							<?$receiptPath = str_replace("#PERIOD_ID#", $arAccountPeriod["PERIOD_ID"], $receiptPath);?>
							<?=GetMessage("TYP_FORM_RECEIPT", array("#URL#" => $receiptPath))?>
						</td>
					<?endif?>
				</tr>
			</table>
    
			</p>
			</font>
    
			<p><font class="tablebodytext"><?=GetMessage("PYM_WARN")?></font></p>
		</form>
<?	}

	/**
	 * CTszhYandexPayment::ReceivePayment()
	 * ����� �� ���������� �� ������ �� ������.�����: ������ ��������
	 */
	public static function ReceivePayment()
	{
		$shopID = $_POST["shopID"];
		$invoiceID = $_POST["invoiceID"];
		$code = "0";
		$dateISO = date("c");

		$GLOBALS["APPLICATION"]->RestartBuffer();
		header("Content-Type: text/xml");
		header("Pragma: no-cache");
		$text = "<"."?xml version=\"1.0\" encoding=\"windows-1251\"?".">\n";
		$text .= "<paymentAvisoResponse performedDatetime=\"" . $dateISO . "\" code=\"" . $code . "\" invoiceId=\"" . $invoiceId . "\" shopId=\"" . $shopId . "\"";
		$text .= "/>";
		echo $text;
		die();
	}
}
?>