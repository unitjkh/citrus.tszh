<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/citrus.tszh/include.php");

// ��������� �������� ����
IncludeModuleLangFile(__FILE__);

// ������� ����� ������� �������� ������������ �� ������
$POST_RIGHT = $APPLICATION->GetGroupRight("citrus.tszh");

// ���� ��� ���� - �������� � ����� ����������� � ���������� �� ������
if ($POST_RIGHT == "D")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));


IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/citrus.tszh/admin/user_admin.php");
IncludeModuleLangFile(__FILE__);

$FN = preg_replace("/[^a-z0-9_\\[\\]:]/i", "", $_REQUEST["FN"]);
$FC = preg_replace("/[^a-z0-9_\\[\\]:]/i", "", $_REQUEST["FC"]);
if($FN == "")
	$FN = "find_form";
if($FC == "")
	$FC = "SERVICE_ID";

if (isset($_REQUEST['JSFUNC']))
{
	$JSFUNC = preg_replace("/[^a-z0-9_\\[\\]:]/i", "", $_REQUEST['JSFUNC']);
}
else
{
	$JSFUNC = '';
}
// ������������� �������
$sTableID = "tbl_service_popup";

// ������������� ����������
$oSort = new CAdminSorting($sTableID, "ID", "asc");
// ������������� ������
$lAdmin = new CAdminList($sTableID, $oSort);

// ������������� ���������� ������ - �������
$arFilterFields = Array(
	"find",
	"find_type",
	"find_id",
	"find_xml_id",
	"find_name",
	);

$lAdmin->InitFilter($arFilterFields);

//������������� ������� ������� ��� GetList
function CheckFilter($FilterArr) // �������� ��������� �����
{
	global $strError;
	foreach($FilterArr as $f)
		global $$f;


	$strError .= $str;
	if(strlen($str)>0)
	{
		global $lAdmin;
		$lAdmin->AddFilterError($str);
		return false;
	}

	return true;
}

$arFilter = Array();
if(CheckFilter($arFilterFields))
{
	if (strlen($find) > 0 && strlen($find_type) > 0) {
		switch ($find_type) {
			case 'id':
				$arFilter['ID'] = $find;
				break;
			case 'xml_id':
				$arFilter['XML_ID'] = $find;
				break;
			default:
		}
	}

	if (strlen(htmlspecialcharsbx($find_id)) > 0) {
		$arFilter["ID"] = htmlspecialcharsbx($find_id);
	}
	if (strlen(htmlspecialcharsbx($find_xml_id)) > 0) {
		$arFilter["XML_ID"] = htmlspecialcharsbx($find_xml_id);
	}
	if (strlen(htmlspecialcharsbx($find_name)) > 0) {
		$arFilter["%NAME"] = htmlspecialcharsbx($find_name);
	}
}

// ������������� ������ - ������� ������
$rsData = CTszhService::GetList(Array($by => $order), $arFilter, false, array("nPageSize"=>CAdminResult::GetNavSize($sTableID)));
$rsData = new CAdminResult($rsData, $sTableID);
$rsData->NavStart();

// ��������� ���������� ������
$lAdmin->NavText($rsData->GetNavPrint(GetMessage("PAGES")));

// ��������� ������
$lAdmin->AddHeaders(array(
	array("id"=>"ID",				"content"=>"ID", 	"sort"=>"id", "default"=>true),
	array("id"=>"XML_ID",				"content"=>GetMessage("TSZH_FLT_XML_ID"), 	"sort"=>"XML_ID", "default"=>false),
	array("id"=>"NAME",				"content"=>GetMessage("TSZH_FLT_NAME"), 	"sort"=>"NAME", "default"=>true),
	array("id"=>"NORM",				"content"=>GetMessage("TSZH_FLT_NORM"), 	"sort"=>"NORM", "default"=>true),
	array("id"=>"UNITS",				"content"=>GetMessage("TSZH_FLT_UNITS"), 	"sort"=>"UNITS", "default"=>true),
	array("id"=>"TARIFF",				"content"=>GetMessage("TSZH_FLT_TARIFF"), 	"sort"=>"TARIFF", "default"=>true),
));

// ���������� ������
while($arRes = $rsData->GetNext())
{

	$row =& $lAdmin->AddRow($arRes['ID'], $arRes);
	//echo '<pre>' . var_export($arRes, true) . '</pre>';

	$arActions = array();
	$arActions[] = array(
		"ICON"=>"",
		"TEXT"=>GetMessage("TSZH_SELECT"),
		"DEFAULT"=>true,
		"ACTION"=>"SetValue('".$arRes['ID']."');"
	);
	$row->AddActions($arActions);
}

// "������" ������
$lAdmin->AddFooter(
	array(
		array("title"=>GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value"=>$rsData->SelectedRowsCount()),
		array("counter"=>true, "title"=>GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value"=>"0"),
	)
);

$lAdmin->AddAdminContextMenu(array());

// �������� �� ����� ������ ������ (� ������ ������, ������ ������ ����������� �� �����)
$lAdmin->CheckListMode();

$APPLICATION->SetTitle(GetMessage("TSZH_PAGE_TITLE"));
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_popup_admin.php")
?>
<script language="JavaScript">
<!--
function SetValue(id)
{
	<?if ($JSFUNC <> ''){?>
	window.opener.SUV<?=$JSFUNC?>(id);
	<?}else{?>
	window.opener.document.<?echo $FN;?>["<?echo $FC;?>"].value=id;
	window.close();
	<?}?>
}
//-->
</script>
<form name="find_form" method="GET" action="<?echo $APPLICATION->GetCurPage()?>?">
<?
$oFilter = new CAdminFilter(
	$sTableID."_filter",
	array(
		GetMessage('TSZH_FLT_ID'),
		GetMessage('TSZH_FLT_XML_ID'),
		GetMessage('TSZH_FLT_NAME'),
	)
);

$oFilter->Begin();
?>
<tr>
	<td><b><?=GetMessage("TSZH_FLT_SEARCH")?></b></td>
	<td nowrap>
		<input type="text" size="25" name="find" value="<?echo htmlspecialcharsbx($find)?>" title="<?=GetMessage("TSZH_FLT_SEARCH_TITLE")?>">
		<select name="find_type">
			<option value="id"<?if($find_type=="id") echo " selected"?>><?=GetMessage('TSZH_FLT_ID')?></option>
			<option value="xml_id"<?if($find_type=="xml_id") echo " selected"?>><?=GetMessage('TSZH_FLT_XML_ID')?></option>
		</select>
	</td>
</tr>
<tr>
	<td><?echo GetMessage("MAIN_FLT_ID")?></td>
	<td><input type="text" name="find_id" size="47" value="<?echo htmlspecialcharsbx($find_id)?>"></td>
</tr>
<tr>
	<td><?echo GetMessage("MAIN_FLT_XML_ID")?></td>
	<td><input type="text" name="find_xml_id" size="47" value="<?echo htmlspecialcharsbx($find_xml_id)?>"></td>
</tr>
<tr>
	<td><?echo GetMessage("TSZH_FLT_NAME")?></td>
	<td><input type="text" name="find_name" size="47" value="<?echo htmlspecialcharsbx($find_name)?>"></td>
</tr>
<input type="hidden" name="FN" value="<?echo htmlspecialcharsbx($FN)?>">
<input type="hidden" name="FC" value="<?echo htmlspecialcharsbx($FC)?>">
<input type="hidden" name="JSFUNC" value="<?echo htmlspecialcharsbx($JSFUNC)?>">
<?
$oFilter->Buttons(array("table_id"=>$sTableID, "url"=>$APPLICATION->GetCurPage(), "form"=>"find_form"));
$oFilter->End();
?>
</form>
<?
// ����� ��� ������ ������
$lAdmin->DisplayList();

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_popup_admin.php");
?>
