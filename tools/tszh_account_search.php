<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/citrus.tszh/include.php");

// ��������� �������� ����
IncludeModuleLangFile(__FILE__);

// ������� ����� ������� �������� ������������ �� ������
$POST_RIGHT = $APPLICATION->GetGroupRight("citrus.tszh");

// ���� ��� ���� - �������� � ����� ����������� � ���������� �� ������
if ($POST_RIGHT == "D")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));


IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/citrus.tszh/admin/user_admin.php");
IncludeModuleLangFile(__FILE__);

$FN = preg_replace("/[^a-z0-9_\\[\\]:]/i", "", $_REQUEST["FN"]);
$FC = preg_replace("/[^a-z0-9_\\[\\]:]/i", "", $_REQUEST["FC"]);
if($FN == "")
	$FN = "find_form";
if($FC == "")
	$FC = "ACCOUNT_ID";

if (isset($_REQUEST['JSFUNC']))
{
	$JSFUNC = preg_replace("/[^a-z0-9_\\[\\]:]/i", "", $_REQUEST['JSFUNC']);
}
else
{
	$JSFUNC = '';
}
// ������������� �������
$sTableID = "tbl_account_popup";

// ������������� ����������
$oSort = new CAdminSorting($sTableID, "ID", "asc");
// ������������� ������
$lAdmin = new CAdminList($sTableID, $oSort);

// ������������� ���������� ������ - �������
$arFilterFields = Array(
	"find",
	"find_type",
	"find_id",
	"find_tszh",
	"find_xml_id",
	"find_city",
	"find_district",
	"find_region",
	"find_settlement",
	"find_street",
	"find_house",
	"find_flat",
	"find_area",
	"find_living_area",
	"find_people",
	);

$lAdmin->InitFilter($arFilterFields);

//������������� ������� ������� ��� GetList
function CheckFilter($FilterArr) // �������� ��������� �����
{
	global $strError;
	foreach($FilterArr as $f)
		global $$f;


	$strError .= $str;
	if(strlen($str)>0)
	{
		global $lAdmin;
		$lAdmin->AddFilterError($str);
		return false;
	}

	return true;
}

$arFilter = Array();
if(CheckFilter($arFilterFields))
{
	if (strlen($find) > 0 && strlen($find_type) > 0) {
		switch ($find_type) {
			case 'ID':
				$arFilter['ID'] = $find;
				break;
			case 'ACCOUNT_XML_ID':
				$arFilter['XML_ID'] = $find;
				break;
			default:
		}
	}
	if (strlen(htmlspecialcharsbx($find_tszh)) > 0) {
		$arFilter["TSZH_ID"] = htmlspecialcharsbx($find_tszh);
	}
	if (strlen(htmlspecialcharsbx($find_xml_id)) > 0) {
		$arFilter["XML_ID"] = htmlspecialcharsbx($find_xml_id);
	}
	if (strlen(htmlspecialcharsbx($find_city)) > 0) {
		$arFilter["CITY"] = htmlspecialcharsbx($find_city);
	}
	if (strlen(htmlspecialcharsbx($find_district)) > 0) {
		$arFilter["DISTRICT"] = htmlspecialcharsbx($find_district);
	}
	if (strlen(htmlspecialcharsbx($find_region)) > 0) {
		$arFilter["REGION"] = htmlspecialcharsbx($find_region);
	}
	if (strlen(htmlspecialcharsbx($find_settlement)) > 0) {
		$arFilter["SETTLEMENT"] = htmlspecialcharsbx($find_settlement);
	}
	if (strlen(htmlspecialcharsbx($find_street)) > 0) {
		$arFilter["STREET"] = htmlspecialcharsbx($find_street);
	}
	if (strlen(htmlspecialcharsbx($find_house)) > 0) {
		$arFilter["HOUSE"] = htmlspecialcharsbx($find_house);
	}
	if (strlen(htmlspecialcharsbx($find_flat)) > 0) {
		$arFilter["FLAT"] = htmlspecialcharsbx($find_flat);
	}
	if (strlen(htmlspecialcharsbx($find_area)) > 0) {
		$arFilter["AREA"] = htmlspecialcharsbx($find_area);
	}
	if (strlen(htmlspecialcharsbx($find_living_area)) > 0) {
		$arFilter["LIVING_AREA"] = htmlspecialcharsbx($find_living_area);
	}
	if (strlen(htmlspecialcharsbx($find_people)) > 0) {
		$arFilter["PEOPLE"] = htmlspecialcharsbx($find_people);
	}
}

// �������� ���� ��� �������
$arTszhFilter = $arTszhRight = $arListFilter = array();
if(CModule::IncludeModule("vdgb.portaltszh") && defined("TSZH_PORTAL") && TSZH_PORTAL === true && !$USER->IsAdmin())
{
	global $USER;
	$arGroups = $USER->GetUserGroupArray();

	$rsPerms = \Vdgb\PortalTszh\PermsTable::getList(
		array(
			"filter" => array("@GROUP_ID" => $arGroups)
		)
	);

	while($arPerms = $rsPerms->fetch())
	{
		if($arPerms["PERMS"] >= "R")
		{
			$arTszhRight[$arPerms["TSZH_ID"]] = $arPerms["PERMS"];
		}
	}

	$arTszhFilter["@ID"] = $arListFilter["@TSZH_ID"] = array_keys($arTszhRight);
}

// ������������� ������ - ������� ������
$rsData = CTszhAccount::GetList(Array($by => $order), array_merge($arFilter, $arListFilter), false, array("nPageSize"=>CAdminResult::GetNavSize($sTableID)));
$rsData = new CAdminResult($rsData, $sTableID);
$rsData->NavStart();

// ��������� ���������� ������
$lAdmin->NavText($rsData->GetNavPrint(GetMessage("PAGES")));

// ��������� ������
$lAdmin->AddHeaders(array(
	array("id"=>"ID",				"content"=>"ID", 	"sort"=>"id", "default"=>true),
	array("id"=>"XML_ID",				"content"=>GetMessage("TSZH_FLT_XML_ID"), 	"sort"=>"XML_ID", "default"=>true),

	array("id"=>"CITY",				"content"=>GetMessage("TSZH_FLT_CITY"), 	"sort"=>"CITY", "default"=>false),
	array("id"=>"DISTRICT",				"content"=>GetMessage("TSZH_FLT_DISTRICT"), 	"sort"=>"DISTRICT", "default"=>false),
	array("id"=>"REGION",				"content"=>GetMessage("TSZH_FLT_REGION"), 	"sort"=>"REGION", "default"=>false),
	array("id"=>"SETTLEMENT",				"content"=>GetMessage("TSZH_FLT_SETTLEMENT"), 	"sort"=>"SETTLEMENT", "default"=>false),
	array("id"=>"STREET",				"content"=>GetMessage("TSZH_FLT_STREET"), 	"sort"=>"STREET", "default"=>false),
	array("id"=>"HOUSE",				"content"=>GetMessage("TSZH_FLT_HOUSE"), 	"sort"=>"HOUSE", "default"=>false),
	array("id"=>"FLAT",				"content"=>GetMessage("TSZH_FLT_FLAT"), 	"sort"=>"FLAT", "default"=>false),
	array("id"=>"AREA",				"content"=>GetMessage("TSZH_FLT_AREA"), 	"sort"=>"AREA", "default"=>false),
	array("id"=>"LIVING_AREA",				"content"=>GetMessage("TSZH_FLT_LIVING_AREA"), 	"sort"=>"LIVING_AREA", "default"=>false),
	array("id"=>"PEOPLE",				"content"=>GetMessage("TSZH_FLT_PEOPLE"), 	"sort"=>"PEOPLE", "default"=>false),
));

// ���������� ������
while($arRes = $rsData->GetNext())
{

	$row =& $lAdmin->AddRow($arRes['ID'], $arRes);

	$sFullName = $arRes['NAME'];
	$row->AddViewField('ID', '[<a href="/bitrix/admin/tszh_account_edit.php?ID=' . $arRes['ID'] . '&lang=' . LANGUAGE_ID . '" title="' . GetMessage("TSZH_EDIT_ACCOUNT") . '">' . $arRes['ID'] . '</a>] ' . $sFullName);

	$row->AddViewField("XML_ID", $arRes['XML_ID']);
	$row->AddViewField("CITY", $arRes['CITY']);
	$row->AddViewField("DISTRICT", $arRes['DISTRICT']);
	$row->AddViewField("REGION", $arRes['REGION']);
	$row->AddViewField("SETTLEMENT", $arRes['SETTLEMENT']);
	$row->AddViewField("STREET", $arRes['STREET']);
	$row->AddViewField("HOUSE", $arRes['HOUSE']);
	$row->AddViewField("FLAT", $arRes['FLAT']);
	$row->AddViewField("AREA", $arRes['AREA']);
	$row->AddViewField("LIVING_AREA", $arRes['LIVING_AREA']);
	$row->AddViewField("PEOPLE", $arRes['PEOPLE']);

	$arActions = array();
	$arActions[] = array(
		"ICON"=>"",
		"TEXT"=>GetMessage("TSZH_SELECT"),
		"DEFAULT"=>true,
		"ACTION"=>"SetValue('".$arRes['ID']."');"
	);
	$row->AddActions($arActions);
}

// "������" ������
$lAdmin->AddFooter(
	array(
		array("title"=>GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value"=>$rsData->SelectedRowsCount()),
		array("counter"=>true, "title"=>GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value"=>"0"),
	)
);

$lAdmin->AddAdminContextMenu(array());

// �������� �� ����� ������ ������ (� ������ ������, ������ ������ ����������� �� �����)
$lAdmin->CheckListMode();

$APPLICATION->SetTitle(GetMessage("TSZH_PAGE_TITLE"));
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_popup_admin.php")
?>
<script language="JavaScript">
<!--
function SetValue(id)
{
	<?if ($JSFUNC <> ''){?>
	window.opener.SUV<?=$JSFUNC?>(id);
	<?}else{?>
	window.opener.document.<?echo $FN;?>["<?echo $FC;?>"].value=id;
	window.close();
	<?}?>
}
//-->
</script>
<form name="find_form" method="GET" action="<?echo $APPLICATION->GetCurPage()?>?">
<?
$oFilter = new CAdminFilter(
	$sTableID."_filter",
	array(
		GetMessage('TSZH_FLT_TSZH_ID'),
		GetMessage('TSZH_FLT_ACCOUNT_ID'),
		GetMessage('TSZH_FLT_XML_ID'),
		GetMessage('TSZH_FLT_CITY'),
		GetMessage('TSZH_FLT_DISTRICT'),
		GetMessage('TSZH_FLT_REGION'),
		GetMessage('TSZH_FLT_SETTLEMENT'),
		GetMessage('TSZH_FLT_STREET'),
		GetMessage('TSZH_FLT_HOUSE'),
		GetMessage('TSZH_FLT_FLAT'),
		GetMessage('TSZH_FLT_AREA'),
		GetMessage('TSZH_FLT_LIVING_AREA'),
		GetMessage('TSZH_FLT_PEOPLE'),
	)
);

$oFilter->Begin();
?>
<tr>
	<td><b><?=GetMessage("TSZH_FLT_SEARCH")?></b></td>
	<td nowrap>
		<input type="text" size="25" name="find" value="<?echo htmlspecialcharsbx($find)?>" title="<?=GetMessage("TSZH_FLT_SEARCH_TITLE")?>">
		<select name="find_type">
			<option value="id"<?if($find_type=="id") echo " selected"?>><?=GetMessage('TSZH_FLT_ID')?></option>
			<option value="xml_id"<?if($find_type=="xml_id") echo " selected"?>><?=GetMessage('TSZH_FLT_XML_ID')?></option>
		</select>
	</td>
</tr>
<tr>
	<td><?echo GetMessage("TSZH_FLT_TSZH_ID");?>:</td>
	<td>
		<select name="find_tszh">
			<option value=""><?= htmlspecialcharsex(GetMessage("TSZH_F_ALL")) ?></option>
			<?
			$dbTszh = CTszh::GetList(Array("NAME" => "ASC"));
			while ($arTszh = $dbTszh->Fetch())
			{
				?><option value="<?= $arTszh["ID"] ?>"<?if ($arTszh["ID"] == $find_tszh) echo " selected";?>>[<?= htmlspecialcharsex($arTszh["ID"]) ?>]&nbsp;<?= htmlspecialcharsex($arTszh["NAME"]) ?></option><?
			}
			?>
		</select>
	</td>
</tr>
<tr>
	<td><?echo GetMessage("TSZH_FLT_ID")?></td>
	<td><input type="text" name="find_id" size="47" value="<?echo htmlspecialcharsbx($find_id)?>"></td>
</tr>
<tr>
	<td><?echo GetMessage("TSZH_FLT_XML_ID")?></td>
	<td><input type="text" name="find_xml_id" size="47" value="<?echo htmlspecialcharsbx($find_xml_id)?>"></td>
</tr>
<tr>
	<td><?echo GetMessage("TSZH_FLT_CITY")?></td>
	<td><input type="text" name="find_city" size="47" value="<?echo htmlspecialcharsbx($find_city)?>"></td>
</tr>
<tr>
	<td><?echo GetMessage("TSZH_FLT_DISTRICT")?></td>
	<td><input type="text" name="find_district" size="47" value="<?echo htmlspecialcharsbx($find_district)?>"></td>
</tr>
<tr>
	<td><?echo GetMessage("TSZH_FLT_REGION")?></td>
	<td><input type="text" name="find_region" size="47" value="<?echo htmlspecialcharsbx($find_region)?>"></td>
</tr>
<tr>
	<td><?echo GetMessage("TSZH_FLT_SETTLEMENT")?></td>
	<td><input type="text" name="find_settlement" size="47" value="<?echo htmlspecialcharsbx($find_settlement)?>"></td>
</tr>
<tr>
	<td><?echo GetMessage("TSZH_FLT_STREET")?></td>
	<td><input type="text" name="find_street" size="47" value="<?echo htmlspecialcharsbx($find_street)?>"></td>
</tr>
<tr>
	<td><?echo GetMessage("TSZH_FLT_HOUSE")?></td>
	<td><input type="text" name="find_house" size="47" value="<?echo htmlspecialcharsbx($find_house)?>"></td>
</tr>
<tr>
	<td><?echo GetMessage("TSZH_FLT_FLAT")?></td>
	<td><input type="text" name="find_flat" size="47" value="<?echo htmlspecialcharsbx($find_flat)?>"></td>
</tr>
<tr>
	<td><?echo GetMessage("TSZH_FLT_AREA")?></td>
	<td><input type="text" name="find_area" size="47" value="<?echo htmlspecialcharsbx($find_area)?>"></td>
</tr>
<tr>
	<td><?echo GetMessage("TSZH_FLT_LIVING_AREA")?></td>
	<td><input type="text" name="find_living_area" size="47" value="<?echo htmlspecialcharsbx($find_living_area)?>"></td>
</tr>
<tr>
	<td><?echo GetMessage("TSZH_FLT_PEOPLE")?></td>
	<td><input type="text" name="find_people" size="47" value="<?echo htmlspecialcharsbx($find_people)?>"></td>
</tr>
<input type="hidden" name="FN" value="<?echo htmlspecialcharsbx($FN)?>">
<input type="hidden" name="FC" value="<?echo htmlspecialcharsbx($FC)?>">
<input type="hidden" name="JSFUNC" value="<?echo htmlspecialcharsbx($JSFUNC)?>">
<?
$oFilter->Buttons(array("table_id"=>$sTableID, "url"=>$APPLICATION->GetCurPage(), "form"=>"find_form"));
$oFilter->End();
?>
</form>
<?
// ����� ��� ������ ������
$lAdmin->DisplayList();

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_popup_admin.php");
?>
