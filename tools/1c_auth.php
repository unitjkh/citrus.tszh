<?
define("TSZH_CANCEL_REDIRECT", true);
require ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

IncludeModuleLangFile(__FILE__);

try {

	if (!is_set($_GET, 'login') || !is_set($_GET, 'password'))
		throw new Exception(GetMessage("CITRUS_TSZH_TOOLS_AUTH_NO_LOGIN_OR_PASSWORD"));

	if ($USER->IsAuthorized())
		$USER->Logout();

	$login = $_GET['login'];
	$password = $_GET['password'];

	$arAuthResult = $USER->Login($login, $password);
	if ($arAuthResult['TYPE'] == 'ERROR')
		throw new Exception(str_replace("<br>", "\n", $arAuthResult['MESSAGE']));

	if ($APPLICATION->GetGroupRight("citrus.tszh") < "U")
	{
		echo GetMessage("ACCESS_DENIED");
		require ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
		return;
	}

	$APPLICATION->SetShowIncludeAreas(true);

	LocalRedirect("citrus_tszh_1c_news.php");
}
catch (Exception $e)
{
	echo $e->getMessage();
	CHTTP::SetStatus("412 Precondition Failed");
}

require ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
?>