<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

IncludeModuleLangFile(__FILE__);

global $APPLICATION;

/*
 * B_ADMIN_SUBELEMENTS
 * if defined and equal 1 - working, another die
 * B_ADMIN_SUBELEMENTS_LIST - true/false
 * if not defined - die
 * if equal true - get list mode
 * 	include prolog and epilog
 * other - get simple html
 *
 * need variables
 * 		$strSubElementAjaxPath - path for ajax
 * 		$accountID - account ID
 *
 */
if ((false == defined('B_ADMIN_SUBELEMENTS')) || (1 != B_ADMIN_SUBELEMENTS))
	return '';
if (false == defined('B_ADMIN_SUBELEMENTS_LIST'))
	return '';

$strSubElementAjaxPath = trim($strSubElementAjaxPath);
$accountID = intval($accountID);
if ($accountID <= 0)
	return;
if (!CModule::IncludeModule("citrus.tszh"))
	return false;

if (!(defined('BX_PUBLIC_MODE') && BX_PUBLIC_MODE == 1))
{
	$APPLICATION->SetAdditionalCSS('/bitrix/themes/.default/sub.css');
	$APPLICATION->AddHeadScript('/bitrix/js/iblock/subelement.js');
}
if (defined('BX_PUBLIC_MODE') && BX_PUBLIC_MODE == 1)
{
	?>
	<script type="text/javascript" bxrunfirst="yes" src="/bitrix/js/iblock/subelement.js"></script>
	<script type="text/javascript" bxrunfirst="yes">BX.loadCSS('/bitrix/themes/.default/sub-public.css');</script><?
}

$sTableID = "tbl_accounts_history"; // ID �������
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/iblock/classes/general/subelement.php');

$oSort = new CAdminSubSorting($sTableID, "id", "desc",'by','order',$strSubElementAjaxPath);
$arHideFields = array();
$lAdmin = new CAdminSubList($sTableID, $oSort,$strSubElementAjaxPath,$arHideFields);
$arSort = Array();
$arSortBy = explode('|', $by);
foreach ($arSortBy as $sBy) {
	$arSort[$sBy] = $order;
}
$arFilter = Array("ACCOUNT_ID" => $accountID);

if ((true == defined('B_ADMIN_SUBELEMENTS_LIST')) && (true == B_ADMIN_SUBELEMENTS_LIST))
{
	if(($arID = $lAdmin->GroupAction()))
	{
		if($_REQUEST['action_target']=='selected')
		{
			$rsData = CTszhAccountHistory::GetList($arSort, $arFilter);
			while($arRes = $rsData->Fetch())
				$arID[] = $arRes['ID'];
		}
	
		foreach($arID as $ID)
		{
			if(strlen($ID)<=0)
				continue;
	
		   	$ID = IntVal($ID);
		   	$arRes = CTszhAccountHistory::GetByID($ID);
		   	if(!$arRes)
		   		continue;
	
			$bPermissions = $APPLICATION->GetGroupRight("citrus.tszh") > 'R';
			if(!$bPermissions)
			{
				$lAdmin->AddGroupError(GetMessage("AEF_DELETE_ERROR_PERMISSION")." (ID:".$ID.")", $ID);
				continue;
			}
	
			switch($_REQUEST['action'])
			{
				case "delete":
					@set_time_limit(0);
					$DB->StartTransaction();
					$APPLICATION->ResetException();
					if(!CTszhAccountHistory::Delete($ID))
					{
						$DB->Rollback();
						if($ex = $APPLICATION->GetException())
							$lAdmin->AddGroupError(GetMessage("AEF_DELETE_ERROR")." [".$ex->GetString()."]", $ID);
						else
							$lAdmin->AddGroupError(GetMessage("AEF_DELETE_ERROR"), $ID);
					}
					else
					{
						$DB->Commit();
					}
					break;
			}
		}
	}
}

$rsData = CTszhAccountHistory::GetList(
	$arSort,
	$arFilter,
	false,
	false,
	Array('*')
);
// ����������� ������ � ��������� ������ CAdminResult
$rsData = new CAdminResult($rsData, $sTableID);

// ���������� CDBResult �������������� ������������ ���������.
$rsData->NavStart();

// �������� ����� ������������� ������� � �������� ������ $lAdmin
$lAdmin->NavText($rsData->GetNavPrint(GetMessage("AEF_HISTORY_RECORDS")));

$arAdminListHeaders = array(
	array(
		"id"    =>"ID",
		"content"  =>"ID",
		"sort"    => "ID",
		"align"    =>"right",
		"default"  =>true,
	),
	array(
		"id"    =>"TIMESTAMP_X",
		"content"  => GetMessage("AEF_TIMESTAMP_X"),
		"sort"    => "TIMESTAMP_X",
		"align"    =>"left",
		"default"  =>true,
	),
	array(
		"id"    =>"FIELDS",
		"content"  => GetMessage("AEF_FIELDS"),
		"align"    =>"left",
		"default"  =>true,
	),
);

$lAdmin->AddHeaders($arAdminListHeaders);


while($arRes = $rsData->NavNext(true, "f_"))
{
	$row =& $lAdmin->AddRow($f_ID, $arRes);
	$s = '';
	$arFields = unserialize($arRes['FIELDS']);
	foreach ($arFields as $key => $val)
	{
		if (strlen($val) <= 0)
			continue;
		$title = GetMessage("AEF_" . $key);
		if (strlen($title) <= 0)
			$title = $key;
		$s .= '<nobr>' . $title . ': <b>' . $val . '</b></nobr>';
		if ($key != array_pop(array_keys($arFields)))
			$s .= ', ';
	}
	$row->AddViewField('FIELDS', $s);
}

// ������ �������
$lAdmin->AddFooter(
	array(
		array("title"=> GetMessage("AL_CNT_TOTAL") .  ":", "value"=>$rsData->SelectedRowsCount()), // ���-�� ���������
		array("counter"=>true, "title"=> GetMessage("AL_CNT_SELECTED") . ":", "value"=>"0"), // ������� ��������� ���������
	)
);

$arActions = array(
	"delete" => GetMessage("MAIN_ADMIN_LIST_DELETE"),
);
$lAdmin->AddGroupActionTable($arActions,array('disable_action_target' => true));

$lAdmin->CheckListMode();

$lAdmin->DisplayList(B_ADMIN_SUBELEMENTS_LIST);


?>