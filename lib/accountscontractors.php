<?php
namespace Citrus\Tszh;

use Bitrix\Main,
	Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Class AccountsContractorsTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> ACCOUNT_PERIOD_ID int mandatory
 * <li> CONTRACTOR_ID int mandatory
 * <li> SUMM double mandatory
 * <li> DEBT_BEG double optional
 * <li> SUMM_PAYED double optional
 * <li> PENALTIES double optional
 * <li> SUMM_CHARGED double optional
 * <li> RECEIPT_ORDER int optional
 * <li> SERVICES string(255) optional
 * </ul>
 *
 * @package Citrus\Tszh
 **/
class AccountsContractorsTable extends \Citrus\Tszh\Entity\DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'b_tszh_accounts_contractors';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return array(
			'ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'autocomplete' => true,
				'title' => Loc::getMessage('ACCOUNTS_CONTRACTORS_ENTITY_ID_FIELD'),
			),
			'ACCOUNT_PERIOD_ID' => array(
				'data_type' => 'integer',
				'required' => true,
				'title' => Loc::getMessage('ACCOUNTS_CONTRACTORS_ENTITY_ACCOUNT_PERIOD_ID_FIELD'),
			),
			'CONTRACTOR_ID' => array(
				'data_type' => 'integer',
				'required' => true,
				'title' => Loc::getMessage('ACCOUNTS_CONTRACTORS_ENTITY_CONTRACTOR_ID_FIELD'),
			),
			'SUMM' => array(
				'data_type' => 'float',
				'required' => true,
				'title' => Loc::getMessage('ACCOUNTS_CONTRACTORS_ENTITY_SUMM_FIELD'),
			),
			'DEBT_BEG' => array(
				'data_type' => 'float',
				'title' => Loc::getMessage('ACCOUNTS_CONTRACTORS_ENTITY_DEBT_BEG_FIELD'),
			),
			'SUMM_PAYED' => array(
				'data_type' => 'float',
				'title' => Loc::getMessage('ACCOUNTS_CONTRACTORS_ENTITY_SUMM_PAYED_FIELD'),
			),
			'PENALTIES' => array(
				'data_type' => 'float',
				'title' => Loc::getMessage('ACCOUNTS_CONTRACTORS_ENTITY_PENALTIES_FIELD'),
			),
			'SUMM_CHARGED' => array(
				'data_type' => 'float',
				'title' => Loc::getMessage('ACCOUNTS_CONTRACTORS_ENTITY_SUMM_CHARGED_FIELD'),
			),
			'RECEIPT_ORDER' => array(
				'data_type' => 'integer',
				'title' => Loc::getMessage('ACCOUNTS_CONTRACTORS_ENTITY_RECEIPT_ORDER_FIELD'),
			),
			'SERVICES' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateServices'),
				'title' => Loc::getMessage('ACCOUNTS_CONTRACTORS_ENTITY_SERVICES_FIELD'),
			),
		);
	}

	/**
	 * Returns validators for SERVICES field.
	 *
	 * @return array
	 */
	public static function validateServices()
	{
		return array(
			new Main\Entity\Validator\Length(null, 255),
		);
	}
}