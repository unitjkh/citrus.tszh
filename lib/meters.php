<?php
namespace Citrus\Tszh;

use Bitrix\Main,
	Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Class MetersTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> ACTIVE string(1) mandatory
 * <li> HOUSE_METER bool optional default 'N'
 * <li> XML_ID string(50) mandatory
 * <li> NAME string(255) mandatory
 * <li> NUM string(100) optional
 * <li> SERVICE_ID int mandatory
 * <li> SERVICE_NAME string(100) mandatory
 * <li> VALUES_COUNT int mandatory
 * <li> SORT int mandatory
 * <li> DEC_PLACES int optional default 2
 * <li> CAPACITY int optional
 * <li> VERIFICATION_DATE date optional
 * </ul>
 *
 * @package Citrus\Tszh
 **/
class MetersTable extends Main\Entity\DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'b_tszh_meters';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return array(
			'ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'autocomplete' => true,
				'title' => Loc::getMessage('METERS_ENTITY_ID_FIELD'),
			),
			'ACTIVE' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateActive'),
				'title' => Loc::getMessage('METERS_ENTITY_ACTIVE_FIELD'),
			),
			'HOUSE_METER' => array(
				'data_type' => 'boolean',
				'values' => array('N', 'Y'),
				'title' => Loc::getMessage('METERS_ENTITY_HOUSE_METER_FIELD'),
			),
			'XML_ID' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateXmlId'),
				'title' => Loc::getMessage('METERS_ENTITY_XML_ID_FIELD'),
			),
			'NAME' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateName'),
				'title' => Loc::getMessage('METERS_ENTITY_NAME_FIELD'),
			),
			'NUM' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateNum'),
				'title' => Loc::getMessage('METERS_ENTITY_NUM_FIELD'),
			),
			'SERVICE_ID' => array(
				'data_type' => 'integer',
				'required' => true,
				'title' => Loc::getMessage('METERS_ENTITY_SERVICE_ID_FIELD'),
			),
			'SERVICE_NAME' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateServiceName'),
				'title' => Loc::getMessage('METERS_ENTITY_SERVICE_NAME_FIELD'),
			),
			'VALUES_COUNT' => array(
				'data_type' => 'integer',
				'required' => true,
				'title' => Loc::getMessage('METERS_ENTITY_VALUES_COUNT_FIELD'),
			),
			'SORT' => array(
				'data_type' => 'integer',
				'required' => true,
				'title' => Loc::getMessage('METERS_ENTITY_SORT_FIELD'),
			),
			'DEC_PLACES' => array(
				'data_type' => 'integer',
				'title' => Loc::getMessage('METERS_ENTITY_DEC_PLACES_FIELD'),
			),
			'CAPACITY' => array(
				'data_type' => 'integer',
				'title' => Loc::getMessage('METERS_ENTITY_CAPACITY_FIELD'),
			),
			'VERIFICATION_DATE' => array(
				'data_type' => 'date',
				'title' => Loc::getMessage('METERS_ENTITY_VERIFICATION_DATE_FIELD'),
			),
		);
	}

	/**
	 * Returns validators for ACTIVE field.
	 *
	 * @return array
	 */
	public static function validateActive()
	{
		return array(
			new Main\Entity\Validator\Length(null, 1),
		);
	}

	/**
	 * Returns validators for XML_ID field.
	 *
	 * @return array
	 */
	public static function validateXmlId()
	{
		return array(
			new Main\Entity\Validator\Length(null, 50),
		);
	}

	/**
	 * Returns validators for NAME field.
	 *
	 * @return array
	 */
	public static function validateName()
	{
		return array(
			new Main\Entity\Validator\Length(null, 255),
		);
	}

	/**
	 * Returns validators for NUM field.
	 *
	 * @return array
	 */
	public static function validateNum()
	{
		return array(
			new Main\Entity\Validator\Length(null, 100),
		);
	}

	/**
	 * Returns validators for SERVICE_NAME field.
	 *
	 * @return array
	 */
	public static function validateServiceName()
	{
		return array(
			new Main\Entity\Validator\Length(null, 100),
		);
	}
}