<?php
namespace Citrus\Tszh;

use Bitrix\Main,
	Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Class AccountsHistoryTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> TIMESTAMP_X datetime mandatory
 * <li> ACCOUNT_ID int mandatory
 * <li> FIELDS string mandatory
 * </ul>
 *
 * @package Citrus\Tszh
 **/
class AccountsHistoryTable extends Main\Entity\DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'b_tszh_accounts_history';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return array(
			'ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'autocomplete' => true,
				'title' => Loc::getMessage('ACCOUNTS_HISTORY_ENTITY_ID_FIELD'),
			),
			'TIMESTAMP_X' => array(
				'data_type' => 'datetime',
				'required' => true,
				'title' => Loc::getMessage('ACCOUNTS_HISTORY_ENTITY_TIMESTAMP_X_FIELD'),
			),
			'ACCOUNT_ID' => array(
				'data_type' => 'integer',
				'required' => true,
				'title' => Loc::getMessage('ACCOUNTS_HISTORY_ENTITY_ACCOUNT_ID_FIELD'),
			),
			'FIELDS' => array(
				'data_type' => 'text',
				'required' => true,
				'title' => Loc::getMessage('ACCOUNTS_HISTORY_ENTITY_FIELDS_FIELD'),
			),
		);
	}
}