<?php
namespace Citrus\Tszh;

use Bitrix\Main,
	Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Class AccountsPeriodTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> ACCOUNT_ID int mandatory
 * <li> TIMESTAMP_X datetime mandatory default 'CURRENT_TIMESTAMP'
 * <li> PERIOD_ID int mandatory
 * <li> TYPE int mandatory
 * <li> BARCODE string(255) mandatory
 * <li> DEBT_BEG double mandatory
 * <li> DEBT_END double mandatory
 * <li> DEBT_PREV double optional
 * <li> PREPAYMENT double optional
 * <li> CREDIT_PAYED double optional
 * <li> SUMM_TO_PAY double optional
 * <li> SUM_PAYED double optional
 * <li> LAST_PAYMENT date optional
 * <li> DATE_SENT datetime optional
 * </ul>
 *
 * @package Citrus\Tszh
 **/
class AccountsPeriodTable extends Main\Entity\DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'b_tszh_accounts_period';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return array(
			'ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'autocomplete' => true,
				'title' => Loc::getMessage('ACCOUNTS_PERIOD_ENTITY_ID_FIELD'),
			),
			'ACCOUNT_ID' => array(
				'data_type' => 'integer',
				'required' => true,
				'title' => Loc::getMessage('ACCOUNTS_PERIOD_ENTITY_ACCOUNT_ID_FIELD'),
			),
			'TIMESTAMP_X' => array(
				'data_type' => 'datetime',
				'required' => true,
				'title' => Loc::getMessage('ACCOUNTS_PERIOD_ENTITY_TIMESTAMP_X_FIELD'),
			),
			'PERIOD_ID' => array(
				'data_type' => 'integer',
				'required' => true,
				'title' => Loc::getMessage('ACCOUNTS_PERIOD_ENTITY_PERIOD_ID_FIELD'),
			),
			'TYPE' => array(
				'data_type' => 'integer',
				'required' => true,
				'title' => Loc::getMessage('ACCOUNTS_PERIOD_ENTITY_TYPE_FIELD'),
			),
			'BARCODE' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateBarcode'),
				'title' => Loc::getMessage('ACCOUNTS_PERIOD_ENTITY_BARCODE_FIELD'),
			),
			'DEBT_BEG' => array(
				'data_type' => 'float',
				'required' => true,
				'title' => Loc::getMessage('ACCOUNTS_PERIOD_ENTITY_DEBT_BEG_FIELD'),
			),
			'DEBT_END' => array(
				'data_type' => 'float',
				'required' => true,
				'title' => Loc::getMessage('ACCOUNTS_PERIOD_ENTITY_DEBT_END_FIELD'),
			),
			'DEBT_PREV' => array(
				'data_type' => 'float',
				'title' => Loc::getMessage('ACCOUNTS_PERIOD_ENTITY_DEBT_PREV_FIELD'),
			),
			'PREPAYMENT' => array(
				'data_type' => 'float',
				'title' => Loc::getMessage('ACCOUNTS_PERIOD_ENTITY_PREPAYMENT_FIELD'),
			),
			'CREDIT_PAYED' => array(
				'data_type' => 'float',
				'title' => Loc::getMessage('ACCOUNTS_PERIOD_ENTITY_CREDIT_PAYED_FIELD'),
			),
			'SUMM_TO_PAY' => array(
				'data_type' => 'float',
				'title' => Loc::getMessage('ACCOUNTS_PERIOD_ENTITY_SUMM_TO_PAY_FIELD'),
			),
			'SUM_PAYED' => array(
				'data_type' => 'float',
				'title' => Loc::getMessage('ACCOUNTS_PERIOD_ENTITY_SUM_PAYED_FIELD'),
			),
            'SUMM_PAYED_INSURANCE' => array(
                'data_type' => 'float',
                'title' => Loc::getMessage('ACCOUNTS_PERIOD_ENTITY_SUMM_TO_PAY_FIELD'),
            ),
            'SUM_TO_PAY_WITH_INSURANCE' => array(
                'data_type' => 'float',
                'title' => Loc::getMessage('ACCOUNTS_PERIOD_ENTITY_SUMM_TO_PAY_FIELD'),
            ),
			'LAST_PAYMENT' => array(
				'data_type' => 'date',
				'title' => Loc::getMessage('ACCOUNTS_PERIOD_ENTITY_LAST_PAYMENT_FIELD'),
			),
			'DATE_SENT' => array(
				'data_type' => 'datetime',
				'title' => Loc::getMessage('ACCOUNTS_PERIOD_ENTITY_DATE_SENT_FIELD'),
			),
			new \Bitrix\Main\Entity\ReferenceField(
				'ACCOUNT',
				'Citrus\Tszh\AccountsTable',
				array('=this.ACCOUNT_ID' => 'ref.ID'),
				array('join_type' => 'INNER')
			),
			new \Bitrix\Main\Entity\ReferenceField(
				'PERIOD',
				'Citrus\Tszh\PeriodTable',
				array('=this.PERIOD_ID' => 'ref.ID'),
				array('join_type' => 'LEFT')
			),
		);
	}

	/**
	 * Returns validators for BARCODE field.
	 *
	 * @return array
	 */
	public static function validateBarcode()
	{
		return array(
			new Main\Entity\Validator\Length(null, 255),
		);
	}
}