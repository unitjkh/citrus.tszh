<?php
/**
 * @author Maxim Cherepinin <cherma@rarus.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2018 otr@rarus.ru
 * @date 02.07.2018 15:42
 */

namespace Citrus\Tszh\DemoData;

use \Bitrix\Main\Localization\Loc;

/**
 * Class House
 * @package Citrus\Tszh\DemoData
 */
class House extends DemoData
{
	/**
	 * House constructor.
	 */
	public function __construct()
	{
		$this->data = array(
			'TSZH_ID' => '',
			'EXTERNAL_ID' => 'demodata-house',
			'AREA' => '2454',
			'ROOMS_AREA' => '2388',
			'COMMON_PLACES_AREA' => '66',
			'REGION' => Loc::getMessage("CITRUS_TSZH_DEMODATA_HOUSE_REGION"),
			'DISTRICT' => '',
			'CITY' => '',
			'SETTLEMENT' => '',
			'STREET' => Loc::getMessage("CITRUS_TSZH_DEMODATA_HOUSE_STREET"),
			'HOUSE' => '67',
			'ZIP' => '113205',
			'TYPE' => '',
			'ADDRESS_USER_ENTERED' => Loc::getMessage("CITRUS_TSZH_DEMODATA_HOUSE_ADDRESS_USER_ENTERED"),
			'ADDRESS_VIEW_FULL' => Loc::getMessage("CITRUS_TSZH_DEMODATA_HOUSE_ADDRESS_VIEW_FULL"),
			'YEAR_OF_BUILT' => '2009',
			'FLOORS' => '10',
			'PORCHES' => '1',
			'BANK' => Loc::getMessage("CITRUS_TSZH_DEMODATA_HOUSE_BANK"),
			'BIK' => '044525219',
			'RS' => '40704820100320030034',
			'KS' => '30101810100000000088',
			'OVERHAUL_BANK' => Loc::getMessage("CITRUS_TSZH_DEMODATA_HOUSE_BANK"),
			'OVERHAUL_BIK' => '047308887',
			'OVERHAUL_RS' => '407028109101800037649',
			'OVERHAUL_KS' => '30101810100000000089',
		);
	}
}