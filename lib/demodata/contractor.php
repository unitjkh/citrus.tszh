<?php
/**
 * @author Maxim Cherepinin <cherma@rarus.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2018 otr@rarus.ru
 * @date 02.07.2018 15:34
 */

namespace Citrus\Tszh\DemoData;

use \Bitrix\Main\Localization\Loc;

/**
 * Class Contractors
 */
class Contractor extends DemoData
{
	/**
	 * Contractors constructor.
	 */
	public function __construct()
	{
		$this->data = array(
			'XML_ID' => '000000001',
			'TSZH_ID' => '',
			'EXECUTOR' => 'Y',
			'NAME' => Loc::getMessage("CITRUS_TSZH_DEMODATA_CONTRACTORS_NAME"),
			'SERVICES' => '',
			'ADDRESS' => Loc::getMessage("CITRUS_TSZH_DEMODATA_CONTRACTORS_ADDRESS"),
			'PHONE' => Loc::getMessage("CITRUS_TSZH_DEMODATA_CONTRACTORS_PHONE"),
			'BILLING' => Loc::getMessage("CITRUS_TSZH_DEMODATA_CONTRACTORS_BILLING"),
			'INN' => '7721122600',
			'KPP' => '123324324',
			'RSCH' => '40704820100320030032',
			'BANK' => Loc::getMessage("CITRUS_TSZH_DEMODATA_CONTRACTORS_BANK"),
			'KSCH' => '30201810500000300214',
			'BIK' => '044525219'
		);
	}

}