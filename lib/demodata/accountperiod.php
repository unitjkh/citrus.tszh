<?php
/**
 * @author Maxim Cherepinin <cherma@rarus.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2018 otr@rarus.ru
 * @date 03.07.2018 10:16
 */

namespace Citrus\Tszh\DemoData;

/**
 * Class AccountPeriod
 * @package Citrus\Tszh\DemoData
 */
class AccountPeriod extends DemoData
{
	/**
	 * @var array
	 */
	protected $data = array(
		'PERIOD_ID' => '',
		'TYPE' => '',
		'TIMESTAMP_X' => '',
		'BARCODE' => '',
		'DEBT_BEG' => '',
		'DEBT_END' => '',
		'DEBT_PREV' => '',
		'PREPAYMENT' => '',
		'CREDIT_PAYED' => '',
		'SUMM_TO_PAY' => '',
		'SUM_PAYED' => '',
		'LAST_PAYMENT' => '',
		'DATE_SENT' => '',
		'IS_SENT' => '',
	);
}