<?php
/**
 * @author Maxim Cherepinin <cherma@rarus.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2018 otr@rarus.ru
 * @date 02.07.2018 15:18
 */

namespace Citrus\Tszh\DemoData;

/**
 * Interface Demodata
 * @package Citrus\Tszh\DemoData
 */
abstract class DemoData
{
	/**
	 * @var array
	 */
	protected $data = array();

	/**
	 * @param $element
	 * @param $arDataToChange
	 */
	protected function changeElementData(&$element, $arDataToChange)
	{
		foreach ($arDataToChange as $key => $value)
		{
			if (array_key_exists($key, $element))
			{
				$element[$key] = $value;
			}
		}
	}

	/**
	 * @param array $arDataToChange
	 */
	public function prepareData($arDataToChange = array())
	{
		if (count($this->data) == count($this->data, COUNT_RECURSIVE))
		{
			$this->changeElementData($this->data, $arDataToChange);
		}
		else
		{
			foreach ($this->data as &$element)
			{
				$this->changeElementData($element, $arDataToChange);
			}
		}

		return $this;
	}

	/**
	 * @return array
	 */
	public function getData()
	{
		return $this->data;
	}
}