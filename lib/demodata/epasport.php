<?php
/**
 * @author Maxim Cherepinin <cherma@rarus.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2018 otr@rarus.ru
 * @date 02.07.2018 15:58
 */

namespace Citrus\Tszh\DemoData;

/**
 * Class EPasport
 * @package Citrus\Tszh\DemoData
 */
class EPasport extends DemoData
{

	/**
	 * EPasport constructor.
	 */
	public function __construct()
	{
		$this->data = array(
		);
	}

}