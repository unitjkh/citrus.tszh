<?php
namespace Citrus\Tszh;

use Bitrix\Main,
	Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Class AccountsCorrectionsTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> XML_ID int mandatory
 * <li> ACCOUNT_ID int mandatory
 * <li> ACCOUNT_PERIOD_ID int mandatory
 * <li> GROUNDS string(255) mandatory
 * <li> SERVICE string(255) mandatory
 * <li> SUMM double mandatory
 * <li> CONTRACTOR_ID int mandatory
 * </ul>
 *
 * @package Citrus\Tszh
 **/
class AccountsCorrectionsTable extends Main\Entity\DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'b_tszh_accounts_corrections';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return array(
			'ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'autocomplete' => true,
				'title' => Loc::getMessage('ACCOUNTS_CORRECTIONS_ENTITY_ID_FIELD'),
			),
			'XML_ID' => array(
				'data_type' => 'integer',
				'required' => true,
				'title' => Loc::getMessage('ACCOUNTS_CORRECTIONS_ENTITY_XML_ID_FIELD'),
			),
			'ACCOUNT_ID' => array(
				'data_type' => 'integer',
				'required' => true,
				'title' => Loc::getMessage('ACCOUNTS_CORRECTIONS_ENTITY_ACCOUNT_ID_FIELD'),
			),
			'ACCOUNT_PERIOD_ID' => array(
				'data_type' => 'integer',
				'required' => true,
				'title' => Loc::getMessage('ACCOUNTS_CORRECTIONS_ENTITY_ACCOUNT_PERIOD_ID_FIELD'),
			),
			'GROUNDS' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateGrounds'),
				'title' => Loc::getMessage('ACCOUNTS_CORRECTIONS_ENTITY_GROUNDS_FIELD'),
			),
			'SERVICE' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateService'),
				'title' => Loc::getMessage('ACCOUNTS_CORRECTIONS_ENTITY_SERVICE_FIELD'),
			),
			'SUMM' => array(
				'data_type' => 'float',
				'required' => true,
				'title' => Loc::getMessage('ACCOUNTS_CORRECTIONS_ENTITY_SUMM_FIELD'),
			),
			'CONTRACTOR_ID' => array(
				'data_type' => 'integer',
				'required' => true,
				'title' => Loc::getMessage('ACCOUNTS_CORRECTIONS_ENTITY_CONTRACTOR_ID_FIELD'),
			),
		);
	}

	/**
	 * Returns validators for GROUNDS field.
	 *
	 * @return array
	 */
	public static function validateGrounds()
	{
		return array(
			new Main\Entity\Validator\Length(null, 255),
		);
	}

	/**
	 * Returns validators for SERVICE field.
	 *
	 * @return array
	 */
	public static function validateService()
	{
		return array(
			new Main\Entity\Validator\Length(null, 255),
		);
	}
}