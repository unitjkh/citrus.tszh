<?

namespace Citrus\Tszh\Types;

use Bitrix\Main\Localization\Loc;
use Traversable;
use Citrus\Tszh\Exchange\FormatException;

Loc::loadMessages(__FILE__);

/**
 * ����������� ��� �������� ������������
 * @package Citrus\Tszh\Types
 */
class XmlNode implements \ArrayAccess, \IteratorAggregate
{
	private $id = null;
	private $name = null;
	private $parent = null;
	private $childs = array();
	private $attr = array();
	private $data = null;

	/**
	 * @param array $data
	 * @param null|self $parent
	 *
	 * @throws \Exception
	 */
	function __construct($data, $parent = null)
	{
		if (!is_array($data))
		{
			throw new \InvalidArgumentException('$data must be an array');
		}

		if (!isset($data["NAME"]) || strlen($data["NAME"]) <= 0)
		{
			throw new \InvalidArgumentException("\$data['NAME'] missing");
		}

		if (!isset($data["ID"]) || strlen($data["ID"]) <= 0)
		{
			throw new \InvalidArgumentException("\$data['ID'] missing");
		}

		$this->name = $data["NAME"];
		$this->id = $data["ID"];
		$this->data = $data["VALUE"];

		$this->parent = $parent;

		if (isset($data['~attr']))
		{
			$this->attr = $data['~attr'];
		}
		else
		{
			$attr = unserialize($data["ATTRIBUTES"]);
			if (!is_array($data))
			{
				throw new \Exception("Incorrect attributes for element " . $this->name);
			}
			$this->attr = $attr;
		}

		if (is_array($data["~children"]))
		{
			foreach ($data["~children"] as $childName => $childArray)
			{
				foreach ($childArray as $child)
				{
					$this->childs[$childName][] = new static($child, $this);
				}
			}
		}
	}

	/**
	 * @return string
	 */
	public function __toString()
	{
		return (string)$this->data;
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * return self
	 */
	public function getParent()
	{
		if (!isset($this->parent))
		{
			throw new \LogicException("There is no parent for " . $this->name . ' element');
		}

		return $this->parent;
	}

	/**
	 * return array
	 */
	public function getAttributes()
	{
		return $this->attr;
	}

	/**
	 * ���������� �������� ��������
	 *
	 * @param string $name
	 * @param bool $required �������� �� ������������? ���� true � ������� �����������, ����� ������� ����������
	 *
	 * @return mixed
	 * @throws FormatException
	 */
	public function get($name, $required = false)
	{
		if (array_key_exists($name, $this->attr))
		{
			return $this->attr[$name];
		}
		elseif ($required)
		{
			throw new FormatException(Loc::getMessage("CITRUS_TSZH_XML_MISSING_REQUIRED_ATTR", array("#ELEMENT#" => $this->name, "#NAME#" => $name)));
		}
		else
		{
			return false;
		}
	}

	/**
	 * ���������� �������� ��������� ��������
	 *
	 * @param string $name
	 * @param bool $required �������� �� ������������? ���� true � ������� �����������, ����� ������� ����������
	 * @param bool $allowX ��������� �� ����������� �������� X (��������� ��� ����������� � ����������)
	 *
	 * @return mixed
	 * @throws FormatException
	 */
	public function getFloat($name, $required = false, $allowX = false)
	{
		$value = $this->get($name, $required);
		if (false !== $value && !is_numeric($value))
		{
			if ($allowX && $value === 'X')
			{
				return 'X';
			}
			else
			{
				throw new FormatException(Loc::getMessage("CITRUS_TSZH_XML_WRONG_FLOAT_ATTR", array("#ELEMENT#" => $this->name, "#NAME#" => $name)));
			}
		}

		return floatval($value);
	}

	/**
	 * ���������� �������� �������������� ��������
	 *
	 * @param string $name
	 * @param bool $required �������� �� ������������? ���� true � ������� �����������, ����� ������� ����������
	 *
	 * @return mixed
	 * @throws FormatException
	 */
	public function getInt($name, $required = false)
	{
		$value = $this->get($name, $required);
		if (false !== $name && $value != intval($value))
		{
			throw new FormatException(Loc::getMessage("CITRUS_TSZH_XML_WRONG_INTEGER_ATTR", array(
				"#ELEMENT#" => $this->name,
				"#NAME#" => $name,
				"#VALUE#" => $value,
			)));
		}

		return intval($value);
	}

	/**
	 * ���������� �������� �������� ���� ���� ��� ����/�����
	 *
	 * @param string $name
	 * @param bool $required �������� �� ������������? ���� true � ������� �����������, ����� ������� ����������
	 *
	 * @return mixed
	 * @throws FormatException
	 */
	public function getDate($name, $required = false)
	{
		$value = $this->get($name, $required);
		if (false !== $value)
		{
			$value = strtotime($value);
			if (false === $value)
			{
				throw new FormatException('CITRUS_TSZH_INCORRECT_DATETIME', array('#ATTR#' => $name, '#VALUE#' => $value));
			}
		}

		return $value;

	}

	/**
	 * @param string $name
	 *
	 * @return mixed
	 * @throws FormatException
	 */
	public function __get($name)
	{
		if (isset($this->childs[$name]))
		{
			return $this->childs[$name];
		}
		throw new FormatException(Loc::getMessage("CITRUS_TSZH_XML_MISSING_REQUIRED_ELEMENT", array("#CHILD#" => $name, "#PARENT#" => $this->name)));
	}

	/**
	 * @param string $name
	 *
	 * @return bool
	 */
	public function __isset($name)
	{
		return isset($this->childs[$name]);
	}

	/**
	 * (PHP 5 &gt;= 5.0.0)<br/>
	 * Whether a offset exists
	 * @link http://php.net/manual/en/arrayaccess.offsetexists.php
	 *
	 * @param mixed $offset <p>
	 * An offset to check for.
	 * </p>
	 *
	 * @return bool true on success or false on failure.
	 * </p>
	 * <p>
	 * The return value will be casted to boolean if non-boolean was returned.
	 */
	public function offsetExists($offset)
	{
		return array_key_exists($offset, $this->attr);
	}

	/**
	 * (PHP 5 &gt;= 5.0.0)<br/>
	 * Offset to retrieve
	 * @link http://php.net/manual/en/arrayaccess.offsetget.php
	 *
	 * @param mixed $offset <p>
	 * The offset to retrieve.
	 * </p>
	 *
	 * @return mixed Can return all value types.
	 * @throws FormatException
	 */
	public function offsetGet($offset)
	{
		if (array_key_exists($offset, $this->attr))
		{
			return $this->attr[$offset];
		}
		else
		{
			throw new FormatException(Loc::getMessage("CITRUS_TSZH_XML_MISSING_REQUIRED_ATTR", array(
				"#ELEMENT#" => $this->name,
				"#NAME#" => $offset,
			)));
		}
	}

	/**
	 * (PHP 5 &gt;= 5.0.0)<br/>
	 * Offset to set
	 * @link http://php.net/manual/en/arrayaccess.offsetset.php
	 *
	 * @param mixed $offset <p>
	 * The offset to assign the value to.
	 * </p>
	 * @param mixed $value <p>
	 * The value to set.
	 * </p>
	 *
	 * @return void
	 */
	public function offsetSet($offset, $value)
	{
		throw new \LogicException("Setting offets is not supported");
	}

	/**
	 * (PHP 5 &gt;= 5.0.0)<br/>
	 * Offset to unset
	 * @link http://php.net/manual/en/arrayaccess.offsetunset.php
	 *
	 * @param mixed $offset <p>
	 * The offset to unset.
	 * </p>
	 */
	public function offsetUnset($offset)
	{
		throw new \LogicException("Unsetting offets is not supported");
	}

	/**
	 * (PHP 5 &gt;= 5.0.0)<br/>
	 * Retrieve an external iterator
	 * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
	 * @return Traversable An instance of an object implementing <b>Iterator</b> or
	 * <b>Traversable</b>
	 */
	public function getIterator()
	{
		return new \ArrayIterator($this->attr);
	}
}