<?

namespace Citrus\Tszh\Types;

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * ���� ���������
 */
final class ReceiptType extends Enum
{
	const MAIN				= 0;
	const FINES_MAIN		= 1;
	const OVERHAUL			= 2;
	const FINES_OVERHAUL	= 3;

	/**
	 * ��������� �������� ���������� ���� ���������
	 *
	 * @param string $name
	 * @return string
	 */
	public static function getTitle($name)
	{
		return Loc::getMessage("CITRUS_TSZH_RECEIPT_TYPE_" . $name);
	}

	/**
	 * ��������� ������ ���������� ��� ��������
	 *
	 * @return array
	 */
	public static function getTitles()
	{
		$titles = array();
		foreach (self::getConstants() as $name=>$val) {
			$titles[$val] = self::getTitle($name);
		}
		return $titles;
	}

	/**
	 * ���������� ��� ����������� ������
	 *
	 * @param int $currentValue ������� ��������
	 * @param string $selectName �������� <select name="">
	 * @return string
	 */
	public static function getSelect($currentValue, $selectName = 'find_type')
	{
		$items = self::getTitles();
		return SelectBoxFromArray($selectName, array(
			"REFERENCE" => array_values($items),
			"REFERENCE_ID" => array_keys($items),
		), $currentValue, Loc::getMessage("CITRUS_TSZH_RECEIPT_TYPE_ALL"));
	}

	/**
	 * @param $current_type
	 * @param $arTypes
	 *
	 * @return bool
	 */
	public static function isReceiptType($current_type, $arTypes)
	{
		foreach ($arTypes as $type)
		{
			if ($current_type == $type)
			{
				return true;
			}
		}

		return false;
	}
}
