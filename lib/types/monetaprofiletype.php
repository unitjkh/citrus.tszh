<?

namespace Citrus\Tszh\Types;

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * ���� ���������
 */
final class monetaProfileType extends Enum
{
	const OFFER			= 'offer';
	const NO_OFFER		= 'no_offer';

	/**
	 * ��������� �������� ���������� ���� ���������
	 *
	 * @param string $name
	 * @return string
	 */
	public static function getTitle($name)
	{
		return Loc::getMessage("OTR_TSZH_MONETA_PROFILE_TYPE_" . $name);
	}

	/**
	 * ��������� ������ ���������� ��� ��������
	 *
	 * @return array
	 */
	public static function getTitles()
	{
		$titles = array();
		foreach (self::getConstants() as $name=>$val) {
			$titles[$val] = self::getTitle($name);
		}
		return $titles;
	}

}
