<?

namespace Citrus\Tszh\Types;

use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * ���� ��������� ���������� � �����������
 * � XML ������ ������ -- �������� <service component=""/>
 */
final class ComponentType extends Enum
{
	const NONE		= "N";
	const TARIFFS	= "Y";
	const COMPLEX	= "Z";

	/**
	 * ��������� �������� ���������� ����
	 *
	 * @param string $name
	 * @return string
	 */
	public static function getTitle($name)
	{
		return Loc::getMessage("CITRUS_TSZH_COMPONENT_TYPE_" . $name);
	}

	/**
	 * ��������� ������ ���������� ��� ��������
	 *
	 * @return array
	 */
	public static function getTitles()
	{
		$titles = array();
		foreach (self::getConstants() as $name=>$val) {
			$titles[$val] = self::getTitle($name);
		}
		return $titles;
	}
}
