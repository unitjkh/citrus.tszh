<?php

namespace Citrus\Tszh\Types;

use ReflectionClass;
use InvalidArgumentException;
use LogicException;

/**
 * ������� ����� ��� ������������
 */
abstract class Enum
{
	/**
	 * ��������� �� �������
	 *
	 * @var array ["$class" => ["$name" => $value, ...], ...]
	 */
	private static $constants = array();

	/**
	 * �����������
	 */
	final private function __construct()
	{
		throw new LogicException("This is a static-only class");
	}

	/**
	 * �������� �������� ��������� �� � �����
	 *
	 * @param string $name ��� ���������
	 * @return static
	 * @throws InvalidArgumentException ������������ ��� �� �������� ��� ���������
	 * @throws LogicException           ������������ ��������
	 */
	final public static function getByName($name)
	{
		$name  = (string) $name;
		$class = get_called_class();

		$const = $class . '::' . $name;
		if (!defined($const)) {
			throw new InvalidArgumentException($const . ' not defined');
		}

		return constant($const);
	}

	/**
	 * @param string $value
	 * @return bool
	 */
	final public static function hasValue($value)
	{
		$values = static::getConstants();
		return in_array($value, $values);
	}

	/**
	 * ���������� ��� ��������� ����������� ������
	 *
	 * @return array
	 * @throws LogicException � ������ �� ���������� �������� ��������
	 */
	final public static function getConstants()
	{
		return self::detectConstants(get_called_class());
	}

	/**
	 * �������� ��� ��������� ���������� ������
	 *
	 * @param string $class
	 * @return array
	 * @throws LogicException � ������ �� ���������� �������� ��������
	 */
	private static function detectConstants($class)
	{
		if (!isset(self::$constants[$class])) {
			$reflection = new ReflectionClass($class);
			$constants  = $reflection->getConstants();

			// values needs to be unique
			$ambiguous = array();
			foreach ($constants as $value) {
				$names = array_keys($constants, $value, true);
				if (count($names) > 1) {
					$ambiguous[var_export($value, true)] = $names;
				}
			}
			if ($ambiguous) {
				throw new LogicException(
					'All possible values needs to be unique. The following are ambiguous: '
					. implode(', ', array_map(function ($names) use ($constants) {
						return implode('/', $names) . '=' . var_export($constants[$names[0]], true);
					}, $ambiguous))
				);
			}

			// This is required to make sure that constants of base classes will be the first
			while (($reflection = $reflection->getParentClass()) && $reflection->name !== __CLASS__) {
				$constants = $reflection->getConstants() + $constants;
			}

			self::$constants[$class] = $constants;
		}

		return self::$constants[$class];
	}

	/**
	 * Get an enumarator instance by the given name.
	 *
	 * This will be called automatically on calling a method
	 * with the same name of a defined enumerator.
	 *
	 * @param string $method The name of the enumeraotr (called as method)
	 * @param array  $args   There should be no arguments
	 * @return static
	 * @throws InvalidArgumentException On an invalid or unknown name
	 * @throws LogicException           On ambiguous constant values
	 */
	final public static function __callStatic($method, array $args)
	{
		return self::getByName($method);
	}
}