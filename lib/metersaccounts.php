<?php
namespace Citrus\Tszh;

use Bitrix\Main,
	Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Class MetersAccountsTable
 *
 * Fields:
 * <ul>
 * <li> METER_ID int mandatory
 * <li> ACCOUNT_ID int mandatory
 * </ul>
 *
 * @package Citrus\Tszh
 **/
class MetersAccountsTable extends Main\Entity\DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'b_tszh_meters_accounts';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return array(
			'METER_ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'title' => Loc::getMessage('METERS_ACCOUNTS_ENTITY_METER_ID_FIELD'),
			),
			'ACCOUNT_ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'title' => Loc::getMessage('METERS_ACCOUNTS_ENTITY_ACCOUNT_ID_FIELD'),
			),
		);
	}
}