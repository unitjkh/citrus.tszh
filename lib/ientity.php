<?

namespace Citrus\Tszh;

/**
 * ��������� ��� ��������� ������ ������
 *
 * @todo ��������� ��� ������ �� ���� ���������
 * @todo ����������� ���������������� �������� �� ������ ������, ��������������� ���� ������������
 * @package Citrus\Tszh
 */
interface IEntity
{
	/**
	 * ���������� ������ ������������� ����� ���������� ������ Bitrix\Main\Entity\DataManager::getMap()
	 * @return array
	 */
	public static function getEditableFields();

	/**
	 * ���������� ������ � ������� ����� ��������
	 * @return array
	 */
	public static function getFieldNames();

	/**
	 * @param string $fieldName
	 * @return string
	 * @throws \Exception
	 */
	public static function getFieldTitle($fieldName);
}