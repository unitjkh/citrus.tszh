<?
namespace Citrus\Tszh\Exchange\v3;

use Citrus\Tszh\Exchange\ServiceBase;
use Bitrix\Main\Localization\Loc;

/**
 * ��������� �������� ������ �� ����������
 * (������� ���������)
 *
 * @package Citrus\Tszh\Controller\v3
 */
class ImportDocumentsService extends ServiceBase
{
	/**
	 * @throws \Exception
	 */
	public function run()
	{
		require(__DIR__ . '/legacy_buildings_class.php');

		if (!\CModule::IncludeModule("citrus.tszh"))
		{
			throw new \Exception(Loc::getMessage("TSZH_1C_ERROR_MODULE"));
		}

		if (!\CModule::IncludeModule("iblock"))
		{
			throw new \Exception(Loc::getMessage("TSZH_1C_ERROR_IBLOCK_MODULE"));
		}

		$start_time = time();

		$ABS_FILE_NAME = $this->facade->getUploadedFile();

		$NS = &$_SESSION["BX_CITRUS_BUILDINGS_IMPORT"];
		if (!is_array($NS))
		{
			$NS = array(
				"STEP" => 0,
				/*"TSZH" => $arTszh['ID'],
		
				"ONLY_DEBT" => COption::GetOptionString('citrus.tszh', '1c_exchange.UpdateMode', "Y") == "D",
				"UPDATE_MODE" => COption::GetOptionString('citrus.tszh', '1c_exchange.UpdateMode', "Y") == "Y",
				"CREATE_USERS" => COption::GetOptionString('citrus.tszh', '1c_exchange.CreateUsers', "Y") == "Y",
				"UPDATE_USERS" => COption::GetOptionString('citrus.tszh', '1c_exchange.UpdateUsers', "N") != "N",
				"ACTION" => COption::GetOptionString('citrus.tszh', '1c_exchange.Action', "A"),*/
			);
		}

		$obXMLFile = new \CIBlockXMLFile;

		if ($NS["STEP"] < 1)
		{
			$_SESSION["BUILDINGS_IMPORT"] = array(
				"PROPERTY_TYPES" => Array(),
			);

			\CIBlockXMLFile::DropTemporaryTables();
			$NS["STEP"]++;

			echo "progress\n" . Loc::getMessage("TI_STEP1_DONE");
		}
		elseif ($NS["STEP"] < 2)
		{
			if (\CIBlockXMLFile::CreateTemporaryTables())
			{
				$NS["STEP"]++;
			}
			else
			{
				throw new \Exception(Loc::getMessage("TI_ERROR_STEP2"));
			}

			echo "progress\n" . Loc::getMessage("TI_STEP2_DONE");
		}
		elseif ($NS["STEP"] < 3)
		{
			if (file_exists($ABS_FILE_NAME) && is_file($ABS_FILE_NAME) && ($fp = fopen($ABS_FILE_NAME, "rb")))
			{
				if ($obXMLFile->ReadXMLToDatabase($fp, $NS, $this->facade->timelimit))
				{
					$NS["STEP"]++;
				}
				fclose($fp);
			}
			else
			{
				throw new \Exception(Loc::getMessage("TI_STEP3_ERROR"));
			}

			$file_size = file_exists($ABS_FILE_NAME) && is_file($ABS_FILE_NAME) ? filesize($ABS_FILE_NAME) : 0;
			echo "progress\n" . Loc::getMessage("STEP3_PROGRESS", Array('#PROGRESS#' => ($file_size > 0 ? round($obXMLFile->GetFilePosition() / $file_size * 100, 2) : 0)));
		}
		elseif ($NS["STEP"] < 4)
		{
			if (\CIBlockXMLFile::IndexTemporaryTables())
			{
				$NS["STEP"]++;
			}
			else
			{
				throw new \Exception(Loc::getMessage("TI_STEP4_ERROR"));
			}

			echo "progress\n" . Loc::getMessage("TI_STEP4_DONE");
		}
		elseif ($NS["STEP"] < 5)
		{
			$obImport = new \CBuildingDocumentsImport;
			$obImport->Init($NS);
			$obImport->FindDocumentsParent();

			$NS['DOCUMENTS_IMPORT_STARTED'] = time();
			$NS["STEP"]++;

			echo "progress\n" . Loc::getMessage("BDI_STEP5_DONE");
		}
		elseif ($NS["STEP"] < 6)
		{
			$obImport = new \CBuildingDocumentsImport();
			$obImport->Init($NS);
			$result = $obImport->ImportDocuments($start_time, $this->facade->timelimit);

			$counter = 0;
			foreach ($result as $key => $value)
			{
				$NS["DONE"][$key] += $value;
				$counter += $value;
			}

			if (!$counter)
			{
				echo "progress\n" . Loc::getMessage("BDI_STEP6_DONE");
				$NS["STEP"]++;
			}
			else
			{
				echo "progress\n" . Loc::getMessage("BDI_STEP6_PROGRESS", Array('#CNT#' => intval($NS["DONE"]["ADD"] + $NS["DONE"]["UPD"] + $NS["DONE"]["ERR"]), '#TOTAL#' => intval($NS["DONE"]["ALL"])));
			}
		}
		elseif ($NS["STEP"] < 7)
		{
			$NS["STEP"]++;
			echo "success\n" . Loc::getMessage("BDI_STEP7_DONE");
			$strErrors = \CBuildingDocumentsImport::GetErrors();
			if (strlen($strErrors) > 0)
			{
				echo "\n" . $strErrors;
			}
		}
		else
		{
			throw new \Exception(Loc::getMessage("TI_ERROR_ALREADY_FINISHED"));
		}

		$this->facade->textOutput = true;
	}
}