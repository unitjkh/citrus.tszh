<?

//IncludeModuleLangFile(__FILE__);

define('HOUSES_IBLOCK', 7);
define('HOUSE_DOCUMENTS_IBLOCK', 9);
define('PROP_CODE_PREFIX', '_');

//define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/citrus-log.txt");

define('BUILDINGS_IMPORT_MAX_ERRORS', 100);
define('BUILDING_DOCUMENTS_IMPORT_MAX_ERRORS', 100);

class CBuildingsImport
{

	var $next_step = false;
	var $use_crc = true;

	function Init(&$next_step)
	{
		$this->next_step = &$next_step;
		$this->use_crc = true;
	}

	function FormatNumber($value)
	{
		$val = preg_replace('/[^\d\.\-]/', '', str_replace(',', '.', $value));

		return floatval($val);
	}

	function pluralForm($n, $f1, $f2, $f5)
	{
		$n = abs($n) % 100;
		$n1 = $n % 10;
		if ($n > 10 && $n < 20) return $f5;
		if ($n1 > 1 && $n1 < 5) return $f2;
		if ($n1 == 1) return $f1;

		return $f5;
	}

	public static function GetErrors()
	{
		if (count($_SESSION['BUILDINGS_IMPORT_ERRORS']) > 0)
		{
			return implode('<br />', $_SESSION['BUILDINGS_IMPORT_ERRORS']);
		}

		return false;
	}

	function ResetErrors()
	{
		$_SESSION['BUILDINGS_IMPORT_ERRORS'] = Array();
	}

	function ImportError($strError, $bException = false)
	{
		global $APPLICATION;

		if ($ex = $APPLICATION->GetException())
		{
			$strError .= ': ' . $ex->GetString();
		}

		$strError = strip_tags($strError);

		if (count($_SESSION['BUILDINGS_IMPORT_ERRORS']) < BUILDINGS_IMPORT_MAX_ERRORS)
		{
			$_SESSION['BUILDINGS_IMPORT_ERRORS'][] = $strError;
		}
		elseif (count($_SESSION['BUILDINGS_IMPORT_ERRORS']) <= BUILDINGS_IMPORT_MAX_ERRORS)
		{
			$_SESSION['BUILDINGS_IMPORT_ERRORS'][] = GetMessage("BI_SHOWN_FIRST_ERRORS") . ' ' . BUILDINGS_IMPORT_MAX_ERRORS . ' ' . CBuildingsImport::pluralForm(BUILDINGS_IMPORT_MAX_ERRORS, GetMessage("BI_SHOWN_ERRORS1"), GetMessage("BI_SHOWN_ERRORS3"), GetMessage("BI_SHOWN_ERRORS5"));
		}

		$APPLICATION->ResetException();
	}

	function FindBuildingsParent()
	{
		global $DB;

		$rs = $DB->Query("SELECT ID FROM b_xml_tree WHERE NAME='buildingsList'");
		if ($ar = $rs->Fetch())
		{
			$this->next_step["XML_BUILDINGS_PARENT"] = $ar['ID'];
		}

		if ($this->next_step["XML_BUILDINGS_PARENT"])
		{
			$rs = $DB->Query("SELECT count(*) C FROM b_xml_tree WHERE PARENT_ID = " . intval($this->next_step["XML_BUILDINGS_PARENT"]));
			$ar = $rs->Fetch();
			$this->next_step["DONE"]["ALL"] = $ar["C"];
		}
	}

	function ImportPropertyTypes()
	{
		global $DB;
		$arPropertyTypes = array();

		$rsItems = $DB->Query("SELECT * FROM b_xml_tree WHERE NAME='property'");
		while ($arItem = $rsItems->Fetch())
		{
			$arItem = unserialize($arItem["ATTRIBUTES"]);

			$propType = 'S';
			$userType = false;
			switch (trim($arItem['type']))
			{
				case 'int':
				case 'float':
					$propType = 'N';
					break;

				case 'string':
					$propType = 'S';
					break;

				case 'date':
					$propType = 'S';
					$userType = "DateTime";
					break;
			}

			$code = trim($arItem['code']);
			$name = trim($arItem['name']);
			if (strlen($code) <= 0)
			{
				$this->ImportError(str_replace('#NAME#', $name, GetMessage("BI_WRONG_HOUSE_PROPERTY_CODE")));
				continue;
			}

			$arPropertyType = Array(
				'IBLOCK_ID' => HOUSES_IBLOCK,
				'ACTIVE' => 'Y',
				'MULTIPLE' => 'N',
				'CODE' => PROP_CODE_PREFIX . $code,
				'NAME' => $name,
				'SORT' => intval($arItem['sort']),
				"PROPERTY_TYPE" => $propType,
			);
			if ($userType)
			{
				$arPropertyType['USER_TYPE'] = $userType;
			}

			$arPropertyTypes[$arPropertyType["CODE"]] = $arPropertyType;
		}

		//AddmEssage2Log('$arPropertyTypes = ' . print_r($arPropertyTypes, true));

		foreach ($arPropertyTypes as $CODE => $arPropertyType)
		{
			$rsOldPropertyType = CIBlockProperty::GetList(
				Array(),
				Array(
					"IBLOCK_ID" => HOUSES_IBLOCK,
					"CODE" => $CODE,
					"CHECK_PERMISSIONS" => 'N',
				)
			);
			$arOldPropertyType = $rsOldPropertyType->Fetch();

			$ibp = new CIBlockProperty;
			if (is_array($arOldPropertyType))
			{
				$propertyTypeID = $arOldPropertyType['ID'];
				$res = $ibp->Update($propertyTypeID, $arPropertyType);
				if (!$res)
				{
					$propertyTypeID = false;
					$this->ImportError(str_replace(array('#CODE#', '#ERROR_TEXT#'), array($CODE, $ibp->LAST_ERROR), GetMessage("BI_HOUSE_PROPERTY_UPDATE_ERROR")));
				}
			}
			else
			{
				$propertyTypeID = $ibp->Add($arPropertyType);
				if (!$propertyTypeID)
				{
					$this->ImportError(str_replace(array('#CODE#', '#ERROR_TEXT#'), array($CODE, $ibp->LAST_ERROR), GetMessage("BI_HOUSE_PROPERTY_ADD_ERROR")));
				}
//					AddMessage2Log('$ibp->Add() Error: ' . $ibp->LAST_ERROR);
			}

			if ($propertyTypeID)
			{
				$_SESSION["BUILDINGS_IMPORT"]["PROPERTY_TYPES"][$CODE] = $arPropertyType;
			}
		}
	}

	function GetAllChildrenNested($arParent)
	{
		global $DB;

		$arResult = $arParent;

		//So we get not parent itself
		if (!is_array($arResult))
		{
			$rs = $DB->Query("SELECT ID, LEFT_MARGIN, RIGHT_MARGIN, ATTRIBUTES FROM b_xml_tree WHERE ID = " . intval($arResult));
			$arResult = $rs->Fetch();
			if (!$arResult)
			{
				return false;
			}
			$arResult["~attr"] = unserialize($arResult['ATTRIBUTES']);
			$arResult["~children"] = Array();
		}

		//Array of the references to the $arParent array members with xml_id as index.
		$arIndex = array($arResult["ID"] => &$arResult);
		$rs = $DB->Query("SELECT * FROM b_xml_tree WHERE LEFT_MARGIN BETWEEN " . ($arParent["LEFT_MARGIN"] + 1) . " AND " . ($arResult["RIGHT_MARGIN"] - 1) . " ORDER BY ID");
		while ($ar = $rs->Fetch())
		{
			$ar["~attr"] = unserialize($ar['ATTRIBUTES']);
			unset($ar["ATTRIBUTES"]);

			if (isset($ar["VALUE_CLOB"]))
			{
				$ar["VALUE"] = $ar["VALUE_CLOB"];
			}

			$parent_id = $ar["PARENT_ID"];
			if (!is_array($arIndex[$parent_id]))
			{
				$arIndex[$parent_id] = array();
			}

			/*			if(!array_key_exists($ar["NAME"], $arIndex[$parent_id]['~children']) || !is_array($arIndex[$parent_id]['~children'][$ar["NAME"]]))
						{
							$arIndex[$parent_id]['~children'][$ar["NAME"]] = Array();
						}*/
			$arIndex[$ar['ID']] = $ar;
			$arIndex[$ar["PARENT_ID"]]['~children'][$ar['NAME']][] = &$arIndex[$ar['ID']];
			/*

						$arIndex[$parent_id]['~children'][$ar["NAME"]][] = $ar;
						$lastIdx = count($arIndex[$parent_id]['~children'][$ar["NAME"]])-1;
						$arIndex[$ar["ID"]] = &$arIndex[$parent_id]['~children'][$ar["NAME"]][$lastIdx];*/
		}

		return $arResult;
	}

	function ImportHouses($start_time, $interval)
	{
		global $DB;

		$counter = array();
		if ($this->next_step["XML_BUILDINGS_PARENT"])
		{
			$rsParents = $DB->Query("SELECT ID, LEFT_MARGIN, RIGHT_MARGIN, ATTRIBUTES FROM b_xml_tree WHERE PARENT_ID = " . $this->next_step["XML_BUILDINGS_PARENT"] . " AND ID > " . intval($this->next_step["XML_LAST_ID"]) . " ORDER BY ID");
			while ($arParent = $rsParents->Fetch())
			{
				$arParent['~attr'] = unserialize($arParent["ATTRIBUTES"]);
				unset($arParent['ATTRIBUTES']);
				$arXMLElement = $this->GetAllChildrenNested($arParent);
				$ID = $this->ImportHouse($arXMLElement, $counter);

				$this->next_step["XML_LAST_ID"] = $arParent["ID"];

				if ($interval > 0 && (time() - $start_time) > $interval)
				{
					break;
				}
			}
		}

		return $counter;
	}

	function ImportHouse($arXMLElement, &$counter)
	{
		$arAttrs = $arXMLElement['~attr'];

		$XML_ID = htmlspecialcharsbx(trim($arAttrs['id']));
		$city = htmlspecialcharsbx(trim($arAttrs['city']));
		$district = htmlspecialcharsbx(trim($arAttrs['district']));
		$house = htmlspecialcharsbx(trim($arAttrs['house']));
		$region = htmlspecialcharsbx(trim($arAttrs['region']));
		$settlement = htmlspecialcharsbx(trim($arAttrs['settlement']));
		$street = htmlspecialcharsbx(trim($arAttrs['street']));

		$bError = false;
		if (strlen($XML_ID) <= 0)
		{
			$this->ImportError(GetMessage("BI_WRONG_XML_ID"));
			$bError = true;
		}
		if (strlen($house) <= 0)
		{
			$this->ImportError(GetMessage("BI_WRONG_HOUSE"));
			$bError = true;
		}
		if (strlen($street) <= 0)
		{
			$this->ImportError(GetMessage("BI_WRONG_STREET"));
			$bError = true;
		}
		if ($bError)
		{
			$counter["ERR"]++;

			return;
		}

		$arHouse = Array(
			'IBLOCK_ID' => HOUSES_IBLOCK,
			'ACTIVE' => 'Y',
			'XML_ID' => $XML_ID,
			'NAME' => "{$street}, " . GetMessage('BI_HOUSE_NUMBER') . " {$house}",
		);

		$rsOldHouse = CIBlockElement::GetList(
			Array(),
			Array(
				'IBLOCK_ID' => HOUSES_IBLOCK,
				"XML_ID" => $XML_ID
			),
			false,
			false,
			array('ID')
		);
		$arOldHouse = $rsOldHouse->Fetch();

		$el = new CIBlockElement;
		if (is_array($arOldHouse))
		{
			$houseID = $arOldHouse['ID'];
			$res = $el->Update($houseID, $arHouse);
			if ($res)
			{
				$counter["UPD"]++;
			}
			else
			{
				$houseID = false;
				$counter["ERR"]++;
				$this->ImportError(str_replace(array('#XML_ID#', '#ERROR_TEXT#'), array($arHouse['XML_ID'], $el->LAST_ERROR), GetMessage("BI_HOUSE_UPDATE_ERROR")));
			}
		}
		else
		{
			// ���� ��� ������� � ������ ���� - �������� ���
			$rsSections = CIBlockSection::GetList(
				array(),
				array(
					'IBLOCK_ID' => HOUSES_IBLOCK,
					'DEPTH_LEVEL' => 1,
					'NAME' => $street,
					'CHECK_PERMISSIONS' => 'N',
				)
			);
			if ($arSection = $rsSections->Fetch())
			{
				$sectionID = $arSection['ID'];
			}
			else
			{
				$bs = new CIBlockSection;
				$arFields = Array(
					"IBLOCK_ID" => HOUSES_IBLOCK,
					"ACTIVE" => 'Y',
					"NAME" => $street,
				);
				$sectionID = $bs->Add($arFields);
			}

			if ($sectionID)
			{
				$arHouse['IBLOCK_SECTION_ID'] = $sectionID;
			}
			else
			{
				$counter["ERR"]++;
				$this->ImportError(str_replace(array('#STREET#', '#ERROR_TEXT#'), array($street, $bs->LAST_ERROR), GetMessage("BI_STREET_ADD_ERROR")));

				return;
			}

			$houseID = $el->Add($arHouse);
			if ($houseID)
			{
				$counter["ADD"]++;
			}
			else
			{
				$counter["ERR"]++;
				$this->ImportError(str_replace(array('#XML_ID#', '#ERROR_TEXT#'), array($arHouse['XML_ID'], $el->LAST_ERROR), GetMessage("BI_HOUSE_ADD_ERROR")));
			}
		}

		// ������� �������� ����
		if ($houseID)
		{
			$arUpdateProperties = Array(
				'city' => $city,
				'district' => $district,
				'region' => $region,
				'settlement' => $settlement,
				'house' => $house,
			);

			foreach ($_SESSION["BUILDINGS_IMPORT"]["PROPERTY_TYPES"] as $CODE => $arPropertyType)
			{
				foreach ($arXMLElement['~children']['val'] as $idx => $arXMLHouseProperty)
				{
					$arHouseProperty = $arXMLHouseProperty['~attr'];
					if (PROP_CODE_PREFIX . trim($arHouseProperty['code']) == $CODE)
					{
						$value = $arXMLHouseProperty['VALUE'];
						switch ($arPropertyType["PROPERTY_TYPE"])
						{
							case 'N':
								$value = $this->FormatNumber($value);
								break;

							case 'S':
								if ($arPropertyType["USER_TYPE"] == 'DateTime')
								{
									$arDate = ParseDateTime($value, 'YYYY-MM-DD');
									$timestamp = mktime(0, 0, 0, $arDate['MM'], $arDate['DD'], $arDate['YYYY']);
									$value = ConvertTimeStamp($timestamp, 'SHORT');
								}
								break;
						}

						$arUpdateProperties[$CODE] = htmlspecialcharsbx($value);
					}
				}
			}

			$el->SetPropertyValuesEx($houseID, HOUSES_IBLOCK, $arUpdateProperties);
		}

		return $houseID;
	}
}

class CBuildingDocumentsImport
{

	var $next_step = false;
	var $files_dir = false;
	var $use_crc = true;

	function Init(&$next_step, $files_dir = false, $use_crc = true)
	{
		$this->next_step = &$next_step;
		$this->files_dir = $files_dir;
		$this->use_crc = $use_crc;
	}

	function pluralForm($n, $f1, $f2, $f5)
	{
		$n = abs($n) % 100;
		$n1 = $n % 10;
		if ($n > 10 && $n < 20) return $f5;
		if ($n1 > 1 && $n1 < 5) return $f2;
		if ($n1 == 1) return $f1;

		return $f5;
	}

	function GetErrors()
	{
		if (count($_SESSION['BUILDING_DOCUMENTS_IMPORT_ERRORS']) > 0)
		{
			return implode('<br />', $_SESSION['BUILDING_DOCUMENTS_IMPORT_ERRORS']);
		}

		return false;
	}

	function ResetErrors()
	{
		$_SESSION['BUILDING_DOCUMENTS_IMPORT_ERRORS'] = Array();
	}

	function ImportError($strError, $bException = false)
	{
		global $APPLICATION;

		if ($ex = $APPLICATION->GetException())
		{
			$strError .= ': ' . $ex->GetString();
		}

		$strError = strip_tags($strError);

		if (count($_SESSION['BUILDING_DOCUMENTS_IMPORT_ERRORS']) < BUILDING_DOCUMENTS_IMPORT_MAX_ERRORS)
		{
			$_SESSION['BUILDING_DOCUMENTS_IMPORT_ERRORS'][] = $strError;
		}
		elseif (count($_SESSION['BUILDING_DOCUMENTS_IMPORT_ERRORS']) <= BUILDING_DOCUMENTS_IMPORT_MAX_ERRORS)
		{
			$_SESSION['BUILDING_DOCUMENTS_IMPORT_ERRORS'][] = GetMessage("BI_SHOWN_FIRST_ERRORS") . ' ' . BUILDING_DOCUMENTS_IMPORT_MAX_ERRORS . ' ' . CBuildingDocumentsImport::pluralForm(BUILDING_DOCUMENTS_IMPORT_MAX_ERRORS, GetMessage("BI_SHOWN_ERRORS1"), GetMessage("BI_SHOWN_ERRORS3"), GetMessage("BI_SHOWN_ERRORS5"));
		}

		$APPLICATION->ResetException();
	}

	function FindDocumentsParent()
	{
		global $DB;

		$rs = $DB->Query("SELECT ID FROM b_xml_tree WHERE NAME='documents'");
		if ($ar = $rs->Fetch())
		{
			$this->next_step["XML_DOCUMENTS_PARENT"] = $ar['ID'];
		}

		if ($this->next_step["XML_DOCUMENTS_PARENT"])
		{
			$rs = $DB->Query("SELECT count(*) C FROM b_xml_tree WHERE PARENT_ID = " . intval($this->next_step["XML_DOCUMENTS_PARENT"]));
			$ar = $rs->Fetch();
			$this->next_step["DONE"]["ALL"] = $ar["C"];
		}
	}

	function GetAllChildrenNested($arParent)
	{
		global $DB;

		$arResult = $arParent;

		//So we get not parent itself
		if (!is_array($arResult))
		{
			$rs = $DB->Query("SELECT ID, LEFT_MARGIN, RIGHT_MARGIN, ATTRIBUTES FROM b_xml_tree WHERE ID = " . intval($arResult));
			$arResult = $rs->Fetch();
			if (!$arResult)
			{
				return false;
			}
			$arResult["~attr"] = unserialize($arResult['ATTRIBUTES']);
			$arResult["~children"] = Array();
		}

		//Array of the references to the $arParent array members with xml_id as index.
		$arIndex = array($arResult["ID"] => &$arResult);
		$rs = $DB->Query("SELECT * FROM b_xml_tree WHERE LEFT_MARGIN BETWEEN " . ($arParent["LEFT_MARGIN"] + 1) . " AND " . ($arResult["RIGHT_MARGIN"] - 1) . " ORDER BY ID");
		while ($ar = $rs->Fetch())
		{
			$ar["~attr"] = unserialize($ar['ATTRIBUTES']);
			unset($ar["ATTRIBUTES"]);

			if (isset($ar["VALUE_CLOB"]))
			{
				$ar["VALUE"] = $ar["VALUE_CLOB"];
			}

			$parent_id = $ar["PARENT_ID"];
			if (!is_array($arIndex[$parent_id]))
			{
				$arIndex[$parent_id] = array();
			}

			/*			if(!array_key_exists($ar["NAME"], $arIndex[$parent_id]['~children']) || !is_array($arIndex[$parent_id]['~children'][$ar["NAME"]]))
						{
							$arIndex[$parent_id]['~children'][$ar["NAME"]] = Array();
						}*/
			$arIndex[$ar['ID']] = $ar;
			$arIndex[$ar["PARENT_ID"]]['~children'][$ar['NAME']][] = &$arIndex[$ar['ID']];
			/*

						$arIndex[$parent_id]['~children'][$ar["NAME"]][] = $ar;
						$lastIdx = count($arIndex[$parent_id]['~children'][$ar["NAME"]])-1;
						$arIndex[$ar["ID"]] = &$arIndex[$parent_id]['~children'][$ar["NAME"]][$lastIdx];*/
		}

		return $arResult;
	}

	function ImportDocuments($start_time, $interval)
	{
		global $DB;

		$counter = array();
		if ($this->next_step["XML_DOCUMENTS_PARENT"])
		{
			$rsParents = $DB->Query("SELECT ID, LEFT_MARGIN, RIGHT_MARGIN, ATTRIBUTES FROM b_xml_tree WHERE PARENT_ID = " . $this->next_step["XML_DOCUMENTS_PARENT"] . " AND ID > " . intval($this->next_step["XML_LAST_ID"]) . " ORDER BY ID");
			while ($arParent = $rsParents->Fetch())
			{
				$arParent['~attr'] = unserialize($arParent["ATTRIBUTES"]);
				unset($arParent['ATTRIBUTES']);
				$arXMLElement = $this->GetAllChildrenNested($arParent);
				$ID = $this->ImportDocument($arXMLElement, $counter);

				$this->next_step["XML_LAST_ID"] = $arParent["ID"];

				if ($interval > 0 && (time() - $start_time) > $interval)
				{
					break;
				}
			}
		}

		return $counter;
	}

	function ImportDocument($arXMLElement, &$counter)
	{
		$arAttrs = $arXMLElement['~attr'];

		$building = htmlspecialcharsbx(trim($arAttrs['building']));
		$title = htmlspecialcharsbx(trim($arAttrs['title']));
		$quantity = htmlspecialcharsbx(trim($arAttrs['quantity']));
		$sum = htmlspecialcharsbx(trim($arAttrs['sum']));
		$contractor = htmlspecialcharsbx(trim($arAttrs['contractor']));
		$num = htmlspecialcharsbx(trim($arAttrs['num']));
		$date = htmlspecialcharsbx(trim($arAttrs['date']));
		$filename = trim($arAttrs['filename']);

		if (strlen($date) > 0)
		{
			$arDate = ParseDateTime($date, 'YYYY-MM-DD');
			$timestamp = mktime(0, 0, 0, $arDate['MM'], $arDate['DD'], $arDate['YYYY']);
			$date = ConvertTimeStamp($timestamp, 'SHORT');
		}

		$bError = false;
		if (strlen($building) <= 0)
		{
			$this->ImportError(GetMessage("BDI_WRONG_BUILDING"));
			$bError = true;
		}
		else
		{
			// ������ ID ���� ��� ���������
			$houseID = false;
			$rsHouse = CIBlockElement::GetList(
				array(),
				array(
					'IBLOCK_ID' => HOUSES_IBLOCK,
					'XML_ID' => $building,
				),
				false,
				false,
				array('ID')
			);
			if ($arHouse = $rsHouse->Fetch())
			{
				$houseID = $arHouse['ID'];
			}

			if (!$houseID)
			{
				$this->ImportError(GetMessage("BDI_HOUSE_NOT_FOUND"));
				$bError = true;
			}
		}

		if (strlen($title) <= 0)
		{
			$this->ImportError(GetMessage("BDI_WRONG_TITLE"));
			$bError = true;
		}
		if (strlen($filename) <= 0)
		{
			$this->ImportError(GetMessage("BDI_WRONG_FILENAME"));
			$bError = true;
		}
		if ($bError)
		{
			$counter["ERR"]++;

			return;
		}

		$arDocument = Array(
			'IBLOCK_ID' => HOUSE_DOCUMENTS_IBLOCK,
			'ACTIVE' => 'Y',
			'XML_ID' => $filename,
			'NAME' => $title,
		);

		$rsOldDocument = CIBlockElement::GetList(
			Array(),
			Array(
				'IBLOCK_ID' => HOUSE_DOCUMENTS_IBLOCK,
				"XML_ID" => $filename,
				"PROPERTY_house" => $houseID,
			),
			false,
			false,
			array('ID')
		);
		$arOldDocument = $rsOldDocument->Fetch();

		$documentID = false;
		$el = new CIBlockElement;
		if (is_array($arOldDocument))
		{
			$documentID = $arOldDocument['ID'];
			$res = $el->Update($documentID, $arDocument);
			if ($res)
			{
				$counter["UPD"]++;
			}
			else
			{
				$documentID = false;
				$counter["ERR"]++;
				$this->ImportError(str_replace(array('#XML_ID#', '#ERROR_TEXT#'), array($arDocument['XML_ID'], $el->LAST_ERROR), GetMessage("BDI_DOCUMENT_UPDATE_ERROR")));
			}
		}
		else
		{
			$arDocument['PROPERTY_VALUES'] = array(
				'house' => $houseID,
			);

			$documentID = $el->Add($arDocument);
			if ($documentID)
			{
				$counter["ADD"]++;
			}
			else
			{
				$counter["ERR"]++;
				$this->ImportError(str_replace(array('#XML_ID#', '#ERROR_TEXT#'), array($arDocument['XML_ID'], $el->LAST_ERROR), GetMessage("BDI_DOCUMENT_ADD_ERROR")));
			}
		}

		// ������� �������� ���������
		if ($documentID)
		{
			$arUpdateProperties = Array(
				'house' => $houseID,
				'act_number' => $num,
				'act_date' => $date,
				'quantity' => $quantity,
				'sum' => $sum,
				'contractor' => $contractor,
				'file' => CFile::MakeFileArray($this->files_dir . $filename),
			);
			$el->SetPropertyValuesEx($documentID, HOUSE_DOCUMENTS_IBLOCK, $arUpdateProperties);
		}

		return $documentID;
	}
}

?>