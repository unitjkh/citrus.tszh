<?
namespace Citrus\Tszh\Exchange\v3;

use Citrus\Tszh\Exchange\ServiceBase;
use Citrus\Tszh\Exchange\Facade;
use Bitrix\Main\Application;

/**
 * ��������� �������� ��������� ���������
 * ?mode=export&type=meters
 * http://wiki.citrus-soft.ru/dev/export-meters
 *
 * @package Citrus\Tszh\Controller\v3
 */
class ExportMetersService extends ServiceBase
{
	// �������� ��� ����������� �������� �� ������
	private static $multipart = 1;

	public function run()
	{
		global $APPLICATION;

		if (!\CModule::IncludeModule("citrus.tszh"))
		{
			throw new \Exception(GetMessage("TSZH_1C_ERROR_MODULE"));
		}

		//������� ���������
		$request = Application::getInstance()->getContext()->getRequest();

		// ��������� �������
		$NS = &$_SESSION["BX_CITRUS_TSZH_EXPORT"];
		if (!is_array($NS))
		{
			$NS = array(
				'MULTIPART' => $request->get('multipart') == self::$multipart ? true : false, // �������� �� �����
				'LAST_ID' => 0,
				'SUCCESS' => false, // ������������ ��� ������ � ��������� �� ������
				'FINAL' => false,
			);
		}

		$arTszh = Facade::getOrg();
		$dateFrom = Facade::dateFrom();
		$dateTo = Facade::dateTo();

		if (!is_array($arTszh) || IntVal($arTszh["ID"]) <= 0)
		{
			throw new \Exception(GetMessage("TSZH_1C_ERROR_ORG_NOT_FOUND"));
		}

		$filename = "/upload/tmp_export_" . date('Y-m-d_Hms') . ".xml";
		$absFilename = str_replace('//', '/', $_SERVER['DOCUMENT_ROOT'] . '/' . $filename);

		require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszh/classes/general/tszh_export.php");

		$arValueFilter = array();
		if ($dateFrom)
		{
			$arValueFilter[">=TIMESTAMP_X"] = $dateFrom;
		}
		if ($dateTo)
		{
			$arValueFilter["<=TIMESTAMP_X"] = $dateTo;
		}
		if (is_set($_GET, 'owner') && ($owner = $_GET['owner']))
		{
			if (!in_array($owner, array("0", "1"), true))
			{
				throw new \Exception(GetMessage("CITRUS_TSZH_EXCHANGE_WRONG_PARAM", array("#PARAM#" => "owner")));
			}
			$arValueFilter['MODIFIED_BY_OWNER'] = $_GET["owner"] == 1 ? "Y" : "N";
		}

		if (!$NS['FINAL'])
		{
			if (!\CTszhExport::DoExport($arTszh["NAME"], $arTszh["INN"], $_SERVER['DOCUMENT_ROOT'] . $filename, Array("TSZH_ID" => $arTszh['ID']), 0, $NS['LAST_ID'], $arValueFilter, $NS))
			{
				$ex = $APPLICATION->GetException();
				if ($ex)
				{
					throw new \Exception(GetMessage("TSZH_1C_METER_EXPORT_ERROR") . $ex->GetString());
				}
				else
				{
					throw new \Exception(GetMessage("TSZH_1C_METER_EXPORT_ERROR"));
				}
			}
		}
		else
		{
			$NS['SUCCESS'] = true;
		}

		// ������� success ��� �������� �� ������ � ������ �� ������ ���������� �� ������
		if ($NS['MULTIPART'] && $NS['SUCCESS'])
		{
			echo 'success';
			unset($_SESSION["BX_CITRUS_TSZH_EXPORT"]);
		}
		else
		{
			echo file_get_contents($absFilename);
			unlink($absFilename);

			if ($NS['MULTIPART'])
			{
				$this->facade->textOutput = true;
			}
			else
			{
				$this->facade->xmlOutput = true;
			}
		}

		// $this->facade->xmlOutput = false;
		// $this->facade->textOutput = true;
	}

}