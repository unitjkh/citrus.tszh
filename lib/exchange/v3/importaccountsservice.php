<?
namespace Citrus\Tszh\Exchange\v3;

use Citrus\Tszh\Exchange\ServiceBase;
use Bitrix\Main\Localization\Loc;

/**
 * ��������� �������� ������ ������� ������
 * ?mode=import&type=accounts
 * http://wiki.citrus-soft.ru/dev/import-accounts
 *
 * @package Citrus\Tszh\Controller\v3
 */
class ImportAccountsService extends ServiceBase
{
	/**
	 * @throws \Exception
	 */
	public function run()
	{
		global $APPLICATION;

		if (!\CModule::IncludeModule("citrus.tszh"))
		{
			throw new \Exception(Loc::getMessage("TSZH_1C_ERROR_MODULE"));
		}

		if (!\CModule::IncludeModule("iblock"))
		{
			throw new \Exception(Loc::getMessage("TSZH_1C_ERROR_IBLOCK_MODULE"));
		}

		$arTszh = $this->facade->getOrg();
		if (!is_array($arTszh) || IntVal($arTszh["ID"]) <= 0)
		{
			throw new \Exception(Loc::getMessage("TSZH_1C_ERROR_ORG_NOT_FOUND"));
		}

		$start_time = time();
		$ABS_FILE_NAME = $this->facade->getUploadedFile();
		$NS = &$_SESSION["BX_CITRUS_TSZH_IMPORT"];
		if (!is_array($NS))
		{
			$NS = array(
				"STEP" => 0,
				"TSZH" => $arTszh['ID'],
				"ONLY_DEBT" => \COption::GetOptionString('citrus.tszh', '1c_exchange.UpdateMode', "Y") == "D",
				"UPDATE_MODE" => \COption::GetOptionString('citrus.tszh', '1c_exchange.UpdateMode', "Y") == "Y",
				"CREATE_USERS" => \COption::GetOptionString('citrus.tszh', '1c_exchange.CreateUsers', "Y") == "Y",
				//"UPDATE_USERS" => COption::GetOptionString('citrus.tszh', '1c_exchange.UpdateUsers', "N") != "N",
				"ACTION" => \COption::GetOptionString('citrus.tszh', '1c_exchange.Action', "A"),
				"DEPERSONALIZE" => \COption::GetOptionString('citrus.tszh', '1c_exchange.Depersonalize', "N") == "Y",
			);
		}

		$obImport = new \CTszhImport($NS, $this->facade->timelimit, true);

		$progress = array();
		$finished = false;
		do
		{
			if ($NS["STEP"] < 1)
			{
				$NS["STEP"] = 2;

				$progress[] = Loc::getMessage("TI_STEP1_DONE");
			}
			elseif ($NS["STEP"] < 3)
			{
				if (file_exists($ABS_FILE_NAME) && is_file($ABS_FILE_NAME) && ($fp = fopen($ABS_FILE_NAME, "rb")))
				{
					if ($obImport->readXml($fp))
					{
						$NS["STEP"]++;
					}
					fclose($fp);
				}
				else
				{
					throw new \Exception(Loc::getMessage("TI_STEP3_ERROR"));
				}

				$file_size = file_exists($ABS_FILE_NAME) && is_file($ABS_FILE_NAME) ? filesize($ABS_FILE_NAME) : 0;
				$progress[] = Loc::getMessage("TI_STEP3_PROGRESS", Array('#PROGRESS#' => ($file_size > 0 ? round($obImport->getXml()->GetFilePosition() / $file_size * 100, 2) : 0)));
			}
			elseif ($NS["STEP"] < 4)
			{
				$NS["STEP"]++;
				$progress[] = Loc::getMessage("TI_STEP4_DONE");
			}
			elseif ($NS["STEP"] < 5)
			{
				$result = $obImport->ProcessPeriod();
				$obImport->ImportServices();
				if ($result === true)
				{
					$NS['ACCOUNTS_IMPORT_STARTED'] = time();
					$NS["STEP"]++;
				}
				else
				{
					$ex = $APPLICATION->GetException();
					if ($ex)
					{
						throw new \Exception(Loc::getMessage("TI_STEP5_ERROR") . ": " . $ex->GetString());
					}
					else
					{
						throw new \Exception(Loc::getMessage("TI_STEP5_ERROR"));
					}
				}

				$progress[] = Loc::getMessage("TI_STEP5_DONE");
			}
			elseif ($NS["STEP"] < 6)
			{
				$result = $obImport->ImportAccounts();

				$counter = 0;
				foreach ($result as $key => $value)
				{
					$NS["DONE"][$key] += $value;
					$counter += $value;
				}

				if (!$counter)
				{
					$progress[] = Loc::getMessage("TI_STEP6_DONE");
					$NS["STEP"]++;
				}
				else
				{
					$progress[] = Loc::getMessage("TI_STEP6_PROGRESS", Array('#CNT#' => intval($NS["DONE"]["ADD"] + $NS["DONE"]["UPD"] + $NS["DONE"]["ERR"]), '#TOTAL#' => intval($NS["DONE"]["ALL"])));
					break;
				}
			}
			elseif ($NS["STEP"] < 7)
			{
				$result = $obImport->Cleanup($NS["ACTION"], $start_time, $this->facade->timelimit);

				$counter = 0;
				foreach ($result as $key => $value)
				{
					$NS["DONE"][$key] += $value;
					$counter += $value;
				}

				if (!$counter)
				{
					$NS["STEP"]++;
					$progress[] = Loc::getMessage("TI_STEP7_DONE");
				}
				else
				{
					$progress[] = Loc::getMessage("TI_STEP7_PROGRESS", Array('#CNT#' => intval($NS["DONE"]["DEL"]) + intval($NS["DONE"]["METER_DEL"]) + intval($NS["DONE"]["DEA"]) + intval($NS["DONE"]["METER_DEA"])));
				}
			}
			elseif ($NS["STEP"] < 8)
			{
				if (!$this->facade->debug)
				{
					@unlink($ABS_FILE_NAME);
				}

				if (\CTszh::hasDemoAccounts($NS["TSZH"], $arDemoAccounts))
				{
					$res = false;
					foreach ($arDemoAccounts as $accountID => $arAccount)
					{
						$res = \CTszhAccount::delete($accountID);
					}
					if ($res)
					{
						$progress[] = Loc::getMessage("TI_STEP8_PROGRESS");
					}
				}

				$NS["STEP"]++;

				// ������� ������ ����� ���������� ������
				$obImport->clearSession();

				$strErrors = \CTszhImport::GetErrors();
				if (strlen($strErrors) > 0)
				{
					echo "warning\n" . $strErrors;
					$this->facade->textOutput = true;

					return;
				}

				echo "success\n" . Loc::getMessage("TI_STEP8_DONE");
				if ($this->facade->debug)
				{
					$this->facade->logMessage(Loc::getMessage("CITRUS_TSZH_IMPORT_FINISH") . "\n" . implode("\n", $progress));
				}
				$finished = true;
				break;
			}
			else
			{
				throw new \Exception(Loc::getMessage("TI_ERROR_ALREADY_FINISHED"));
			}
		} while ($this->facade->timelimit > 0 && (time() - $start_time) < $this->facade->timelimit);

		if ($finished)
		{
			if ($this->facade->debug)
			{
				$this->facade->logMessage(Loc::getMessage("CITRUS_TSZH_IMPORT_FINISH"));
			}
		}
		else
		{
			echo "progress\n" . implode(', ', $progress);
		}

		$this->facade->textOutput = true;
	}
}