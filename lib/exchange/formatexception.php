<?

namespace Citrus\Tszh\Exchange;

use Bitrix\Main\Localization\Loc;

/**
 * ������������� � ������ ������ ������� ������
 * @package Citrus\Tszh\Exchange\Exception
 */
class FormatException extends \Exception
{
	/**
	 * @param string $messageCode ��� ��������� ��������� �� ������
	 * @param array $args ������ ��� ������ (���������� � Loc::getMessage)
	 */
	function __construct($messageCode, $args = array())
	{
		parent::__construct(Loc::getMessage($messageCode, $args) . ' (' . $messageCode . ')');
	}
}