<?

namespace Citrus\Tszh\Exchange\v4\Formats;

use Citrus\Tszh\Exchange\FormatException;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ArgumentException;
use Citrus\Tszh\Types\XmlNode;

Loc::loadMessages(__FILE__);

/**
 * ��������� �������� ���������� ������� ������ 4
 *
 * @package Citrus\Tszh\Exchange\v4\Formats
 */
class Access extends Accounts
{
	/**
	 * @var int Tenant group ID
	 */
	protected $tenantGroup;

	/**
	 * @var bool Rewrite user data if true
	 */
	protected $rewrite;

	/**
	 * @param array $state ���������� ��� �������� �������� ���������, �������� ������ ����������� ����� ������ �������� (������)
	 * @param int $timeLimit ����� �� ����� ���������� ������ ���� � ��������
	 * @param bool $useSessions �������� ��������� ������ ��� ����������� ������������ ��������� ���������� ������
	 * @param string $tableName ��� ������� � ������� XML-�����
	 *
	 * @throws ArgumentException
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	function __construct(&$state, $timeLimit = 0, $useSessions = false, $tableName = 'b_tszh_xml_storage')
	{
		$this->tenantGroup = \CTszh::GetTenantGroup();
		parent::__construct($state, $timeLimit, $useSessions, $tableName);
		$this->filetype = 'access';
	}

	/**
	 * ������ ��� ������� � �������������
	 *
	 * @return bool
	 * @throws FormatException
	 */
	public function startImport()
	{
		global $APPLICATION, $DB;

		$this->state["SERVICES"] = array();

		$xmlRootId = intval($this->useSessions ? $this->xmlFile->GetSessionRoot() : 1);
		$dbOrg = $this->getXml()->GetList(array(), array("NAME" => "org", "ID" => $xmlRootId));
		if ($dbOrg->SelectedRowsCount() <= 0)
		{
			throw new FormatException('CITRUS_TSZH_ORG_ELEMENT_NOT_FOUND');
		}

		$org = new XmlNode($dbOrg->Fetch());
		$this->state["XML_ACCOUNTS_PARENT"] = $org->getId();

		$this->version = $org['version'];
		$this->strict = true;

		if ($this->version != $this->supportedVersion)
		{
			throw new FormatException('CITRUS_TSZH_ERROR_WRONG_VERSION', array(
				'#GIVEN#' => $this->version,
				'#MUSTBE#' => $this->supportedVersion,
			));
		}

		if ($this->filetype !== $org['filetype'])
		{
			throw new FormatException('CITRUS_TSZH_ERROR_WRONG_FILETYPE', array(
				'#GIVEN#' => $org['filetype'],
				'#MUSTBE#' => $this->filetype,
			));
		}

		$dateTimestamp = $org->getDate('filedate', true);
		$this->sPeriod = date('Y-m-d', $dateTimestamp);
		$this->state['FILE_PERIOD'] = date('Y-m', $dateTimestamp);
		$this->rewrite = isset($org['rewrite']) ? $org->get('rewrite') : false;

		// inn
		if (!preg_match('/^[\d]+$/', $org['inn']))
		{
			throw new FormatException('CITRUS_TSZH_ERROR_WRONG_INN', array('#GIVEN#' => $org['inn']));
		}

		$arTszhFilter = array("INN" => $org['inn']);

		if (strlen($this->tszhID))
		{
			$arTszhFilter['ID'] = $this->tszhID;
		}

		$arTszh = \CTszh::GetList(
			array(),
			$arTszhFilter,
			false,
			array("nTopCount" => 1)
		)->Fetch();

		if (is_array($arTszh) && !empty($arTszh) && intval($arTszh["ID"]) > 0)
		{
			$this->arTszh = $arTszh;
			$this->tszhID = $arTszh["ID"];
			$this->siteID = $arTszh["SITE_ID"];
		}
		elseif ($this->createTszh === "Y")
		{
			if (strlen($this->siteID) <= 0)
			{
				$APPLICATION->ThrowException(GetMessage("TI_ERROR_TSZH_CREATE_REQ_SITE_ID"));

				return false;
			}
			if (strlen($this->tszhName) <= 0)
			{
				$APPLICATION->ThrowException(GetMessage("TI_ERROR_TSZH_CREATE_REQ_NAME"));

				return false;
			}

			$arTszhFields = array(
				"SITE_ID" => $this->siteID,
				"NAME" => htmlspecialcharsBack($this->tszhName),
				"INN" => $org['inn'],
			);
			$tszhID = \CTszh::Add($arTszhFields);
			if ($tszhID > 0)
			{
				$this->arTszh = \CTszh::GetByID($tszhID);
				$this->tszhID = $this->arTszh["ID"];
				$this->siteID = $this->arTszh["SITE_ID"];
			}
			else
			{
				$APPLICATION->ThrowException(GetMessage("TI_ERROR_TSZH_CREATE"));

				return false;
			}
		}
		elseif (!\CModule::IncludeModule("vdgb.portaljkh"))
		{
			$this->siteID = "";
			$this->inn = htmlspecialcharsEx($org['inn']);
			$this->tszhName = "";
			if (isset($attrs["name"]))
			{
				$this->tszhName = htmlspecialcharsEx($attrs["name"]);
			}
			$this->createTszh = "Q";
			// ���������� ��� �������� �� 1� - ������ ���������� ������
			$APPLICATION->ThrowException(GetMessage("TI_ERROR_TSZH_WITH_INN_NOT_EXISTS", array("#INN#" => $org['inn'])));

			return 1;
		}
		else
		{
			$APPLICATION->ThrowException(GetMessage("TI_ERROR_TSZH_WITH_INN_NOT_EXISTS", array("#INN#" => $org['inn'])));

			return false;
		}

		if ($this->state["XML_ACCOUNTS_PARENT"])
		{
			$ar = $DB->Query("select count(*) C from " . $this->tableName . " where (NAME='acc')" . $this->andWhereThisSession)->Fetch();
			$this->state["DONE"]["ALL"] = $ar["C"];
		}
		else
		{
			throw new \LogicException("Unexpected next_step[\"XML_ACCOUNTS_PARENT\"]!");
		}

		$this->ResetErrors();

		return true;
	}

	/**
	 * ��������� ������ ������� (org->acc)
	 *
	 * @return array
	 * @throws FormatException
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	public function importAccess()
	{
		if (!$this->state["XML_ACCOUNTS_PARENT"])
		{
			throw new \LogicException("XML_ACCOUNTS_PARENT is not set");
		}

		$counter = array();
		$dbAccs = $this->getXml()->GetList(
			array(
				"ID" => "ASC",
			),
			array(
				"NAME" => "acc",
				"PARENT_ID" => $this->state["XML_ACCOUNTS_PARENT"],
				">ID" => $this->state["LAST_ID"],
			),
			array(
				"ID",
				"NAME",
				"LEFT_MARGIN",
				"RIGHT_MARGIN",
				"ATTRIBUTES",
			)
		);
		/** @noinspection PhpAssignmentInConditionInspection */
		while (!$this->timeIsUp() && $acc = $dbAccs->Fetch())
		{
			$accNode = $this->GetAllChildrenNested($acc);
			$this->processAcc($accNode, $counter);
			$this->state["LAST_ID"] = $acc["ID"];
		}

		return $counter;
	}

	/**
	 * ��������� �������� org->acc
	 *
	 * @param XmlNode $acc
	 * @param $counter
	 *
	 * @throws FormatException
	 * @throws \Bitrix\Main\ArgumentOutOfRangeException
	 */
	protected function processAcc($acc, &$counter)
	{
		// ���������� ����� �������� ����� �� ��������, ��������� �� �
		$accountNumber = $acc->get('name', true);

		$nPos = strpos($accountNumber, GetMessage("CITRUS_TSZH_ACCESS_N_SIGN"));
		if ($nPos !== false)
		{
			$accountNumber = trim(substr($accountNumber, $nPos + 1));
		}

		$accountLogin = isset($acc['login']) ? $acc->get('login') : false;
		$accountExternalID = $acc->get('id', true);
		$accountPassword = $acc->get('pin', true);

		/** �������������� ������� � email, ���� �� �� ������, ���������� ��������� ��� email� ������������ */
		$accountEmail = $acc->get('email');
		if (!$accountEmail)
		{
			$serverName = trim(\CUtil::translit($_SERVER["SERVER_NAME"], 'ru', Array(
				"safe_chars" => "-.",
				"replace_space" => '-',
				"replace_other" => '-',
			)), '-');
			$accountNumberTranslit = trim(\CUtil::translit($accountNumber, 'ru', Array(
				"replace_space" => '-',
				"replace_other" => '.',
			)), '-.');
			if (!check_email('mail@' . $serverName))
			{
				$serverName = 'unknown.domain';
			}

			$accountEmail = $accountNumberTranslit . '@' . $serverName;

			if (!self::check_email($accountEmail))
			{
				$accountEmail = 'mail@' . $serverName;
			}
		}

		$bError = false;
		if (strlen($accountNumber) <= 0)
		{
			$this->ImportError(GetMessage("TI_WRONG_ACCOUNT_NUMBER"), $acc);
			$bError = true;
		}
		if (strlen($accountExternalID) <= 0)
		{
			$this->ImportError(GetMessage("TI_WRONG_ACCOUNT_EXTERNAL_ID"), $acc);
			$bError = true;
		}
		if (!$accountLogin)
		{
		}
		else
		{
			if (strlen($accountLogin) < 3)
			{
				$this->ImportError(GetMessage("TI_WRONG_ACCOUNT_LOGIN", Array(
					'#LOGIN#' => $accountLogin,
					"#FIELD#" => 'login',
				)), $acc);
				$bError = true;
			}
		}
		if (!self::check_email($accountEmail) && !$bError)
		{
			$bError = true;
			$this->ImportError(str_replace("#MAIL#", $accountEmail, GetMessage("TI_WRONG_ACCOUNT_EMAIL")), $acc);
		}

		if ($bError)
		{
			$counter["ERR"]++;

			return;
		}

		$obUser = new \CUser();
		$userOrgGroup = 2;
		if (strlen($this->arTszh['CODE']) > 0)
		{
			$dbGroup = \CGroup::GetList($by = "", $order = "", Array("STRING_ID" => "jkh_users_" . $this->arTszh['CODE']));
			/** @noinspection PhpAssignmentInConditionInspection */
			if ($arGroup = $dbGroup->Fetch())
			{
				$userOrgGroup = $arGroup['ID'];
			}
		}

		$rsAccount = \CTszhAccount::GetList(Array("ID" => "ASC"), Array(
			"EXTERNAL_ID" => $accountExternalID,
			"TSZH_ID" => $this->tszhID,
		));
		/** @noinspection PhpAssignmentInConditionInspection */
		if ($arAccount = $rsAccount->Fetch())
		{
			$accountID = $arAccount['ID'];
			$userID = $arAccount['USER_ID'];
		}
		else
		{
			$accountID = $userID = false;
		}

		/** ��������� ���� ������������ ��� ���������� */
		if (is_array(\CUser::GetByID($userID)->Fetch()) && !$this->rewrite)
		{
			$counter["SKIPPED"]++;
			$this->ImportError(GetMessage("CITRUS_TSZH_ACCESS_ALREAD_EXISTS", array(
				'#ID#' => $accountExternalID,
				'#LOGIN#' => $accountLogin,
				'#NUMBER#' => $accountNumber,
			)));

			return;
		}

		if (!$accountLogin)
		{
			$accountFields = array(
				"TSZH_ID" => $this->tszhID,
				"EXTERNAL_ID" => $accountExternalID,
				"XML_ID" => $accountNumber,
				"USER_ID" => $userID,
				"UF_REGCODE" => $accountPassword,
			);
			// ���� ���������� �������� ���������
			if (is_array(\CUser::GetByID($userID)->Fetch()) && $this->rewrite)
			{

				$arOldUser = \CUser::GetByID($userID)->Fetch();

				$arUser = Array(
					"PASSWORD" => $accountPassword,
					"CONFIRM_PASSWORD" => $accountPassword,
					"ADMIN_NOTES" => str_replace("#PASSWORD#", $accountPassword, GetMessage("TSZH_IMPORT_USER_ADMIN_NOTES")),
				);

				if (isset($acc['email']))
				{
					$arUser["EMAIL"] = $accountEmail;
				}

				if ($obUser->Update($arOldUser['ID'], $arUser))
				{
					$userID = $arOldUser['ID'];
					//	$counter["USR_UPD"]++;
				}
				else
				{
					//	$counter["USR_ERR"]++;
					$this->ImportError(str_replace('#ID#', $userID, GetMessage("TI_ERROR_UPDATE_USER")) . ': ' . $obUser->LAST_ERROR, $arUser);
				}
			}
			if ($accountID)
			{
				if (\CTszhAccount::Update($accountID, $accountFields, ConvertTimeStamp(MakeTimeStamp($this->sPeriod, 'YYYY-MM-DD'), 'FULL')))
				{
					$counter["UPD"]++;
				}
				else
				{
					$counter["ERR"]++;
					global $APPLICATION;
					$ex = $APPLICATION->GetException();
					$this->ImportError(GetMessage("TI_ERROR_UPDATE_ACCOUNT") . ($ex ? '. ' . $ex->GetString() : ''), $acc);
				}
			}
			else
			{
				if (\CTszhAccount::Add($accountFields, false, ConvertTimeStamp(MakeTimeStamp($this->sPeriod, 'YYYY-MM-DD'), 'FULL'), false))
				{
					$counter["ADD"]++;
				}
				else
				{
					$counter["ERR"]++;
					global $APPLICATION;
					$ex = $APPLICATION->GetException();
					$this->ImportError(GetMessage("TI_ERROR_UPDATE_ACCOUNT") . ($ex ? '. ' . $ex->GetString() : ''), $acc);
				}
			}
		}
		else
		{
			$name = explode(' ', $arAccount["NAME"]);
			$arUser = Array(
				"ACTIVE" => 'Y',
				"LID" => $this->siteID,
				"LOGIN" => $accountLogin,
				"XML_ID" => $accountNumber,
				"NAME" => $name[1],
				"SECOND_NAME" => $name[2],
				"LAST_NAME" => $name[0],
				"GROUP_ID" => Array($this->tenantGroup, $userOrgGroup),
				"PERSONAL_STATE" => $arAccount['REGION'],
				"PERSONAL_CITY" => $arAccount["CITY"] ? $arAccount["CITY"] : $arAccount["SETTLEMENT"],
				"PERSONAL_STREET" => \CTszhAccount::GetFullAddress($arAccount, array(
					'REGION',
					'DISTRICT',
					'CITY',
					'SETTLEMENT',
					'FLAT',
				)),
				"EMAIL" => $accountEmail,
				"PASSWORD" => $accountPassword,
				"CONFIRM_PASSWORD" => $accountPassword,
				"ADMIN_NOTES" => str_replace("#PASSWORD#", $accountPassword, GetMessage("TSZH_IMPORT_USER_ADMIN_NOTES")),
			);

			if (is_array(\CUser::GetByID($userID)->Fetch())
			    || is_array(\CUser::GetByLogin($accountLogin)->Fetch())
			)
			{

				$arOldUser = \CUser::GetByID($userID)->Fetch();
				if (!$arOldUser)
				{
					$arOldUser = \CUser::GetByLogin($accountLogin)->Fetch();
				}

				if ($this->rewrite)
				{
					$arUser = Array(
						"LOGIN" => $accountLogin,
						"PASSWORD" => $accountPassword,
						"CONFIRM_PASSWORD" => $accountPassword,
						"ADMIN_NOTES" => str_replace("#PASSWORD#", $accountPassword, GetMessage("TSZH_IMPORT_USER_ADMIN_NOTES")),
					);
				}
				if (is_array($name) && strlen($arAccount["NAME"]) > 0)
				{
					$arUser["NAME"] = $name[1];
					$arUser["SECOND_NAME"] = $name[2];
					$arUser["LAST_NAME"] = $name[0];
				}
				if (isset($acc['email']))
				{
					$arUser["EMAIL"] = $accountEmail;
				}
				if ($obUser->Update($arOldUser['ID'], $arUser))
				{
					$userID = $arOldUser['ID'];
				}
				else
				{
					$this->ImportError(str_replace('#ID#', $userID, GetMessage("TI_ERROR_UPDATE_USER")) . ': ' . $obUser->LAST_ERROR, $arUser);
				}
			}
			else
			{
				$userID = $obUser->Add($arUser);
			}

			if ($userID)
			{
				$accountFields = array(
					"TSZH_ID" => $this->tszhID,
					"EXTERNAL_ID" => $accountExternalID,
					"XML_ID" => $accountNumber,
					"USER_ID" => $userID,
					"UF_REGCODE" => $accountPassword,
				);

				if ($accountID)
				{
					if (\CTszhAccount::Update($accountID, $accountFields, ConvertTimeStamp(MakeTimeStamp($this->sPeriod, 'YYYY-MM-DD'), 'FULL')))
					{
						$counter["UPD"]++;
					}
					else
					{
						$counter["ERR"]++;
						global $APPLICATION;
						$ex = $APPLICATION->GetException();
						$this->ImportError(GetMessage("TI_ERROR_UPDATE_ACCOUNT") . ($ex ? '. ' . $ex->GetString() : ''), $acc);
					}
				}
				else
				{
					if (\CTszhAccount::Add($accountFields, false, ConvertTimeStamp(MakeTimeStamp($this->sPeriod, 'YYYY-MM-DD'), 'FULL'), false))
					{
						$counter["ADD"]++;
					}
					else
					{
						$counter["ERR"]++;
						global $APPLICATION;
						$ex = $APPLICATION->GetException();
						$this->ImportError(GetMessage("TI_ERROR_UPDATE_ACCOUNT") . ($ex ? '. ' . $ex->GetString() : ''), $acc);
					}
				}
			}
			else
			{
				$counter["ERR"]++;
				$this->ImportError(str_replace('#LOGIN#', $arUser['LOGIN'], GetMessage("TI_ERROR_ADD_UPDATE_USER")) . ': ' . htmlspecialcharsback($obUser->LAST_ERROR), $arUser);
			}
		}
	}
}