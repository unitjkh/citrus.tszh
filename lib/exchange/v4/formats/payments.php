<?

namespace Citrus\Tszh\Exchange\v4\Formats;

use Citrus\Tszh\Exchange\Export;

/**
 * ����� ��� �������� ������ �� �������� ��������
 * http://wiki.citrus-soft.ru/dev/export-payments
 *
 * @package Citrus\Tszh\Exchange\v4\Formats
 */
class Payments extends Export
{
	protected $filter = array(
		"!ACCOUNT_ID" => false,
		"!PS_ACTION_FILE" => false,
	);

	/**
	 * ��������� ���������� ���� � �������
	 *
	 * @param array $filter
	 *
	 * @return $this
	 */
	public function filter($filter)
	{
		$this->filter = array_merge($this->filter, $filter);

		return $this;
	}

	/**
	 * @param array $filter
	 *
	 * @return $this
	 */
	public function setFilter($filter)
	{
		$this->filter = $filter;

		return $this;
	}

	/**
	 * @param null $lastId
	 *
	 * @return int|null
	 * @throws \Exception
	 */
	public function run($lastId = null)
	{
		if (!\CModule::IncludeModule("citrus.tszhpayment"))
		{
			throw new \Exception('Failed to include citrus.tszhpayment module');
		}

		$startTime = microtime(1);
		@set_time_limit(0);
		@ignore_user_abort(true);

		$this->state['tszh'] = \CTszh::GetByID($this->tszhId);
		$inn = $this->state['tszh']['INN'];

		$arTszhFilter = array(
			'INN' => $this->state['tszh']['INN'],
		);

		if (strlen($this->tszhId))
		{
			$arTszhFilter['ID'] = $this->tszhId;
		}

		$rsTshz = \CTszh::GetList(array(), $arTszhFilter, false, false, array('ID'));
		$arTszhList = array();
		while ($arTszh = $rsTshz->Fetch())
		{
			$arTszhList[] = $arTszh['ID'];
		}

		$this->filter['@TSZH_ID'] = $arTszhList;
		unset($this->filter['TSZH_ID']);

		// ������ ���
		if (!$lastId)
		{
			// ���������� ���� ������ ���������� �������
			$lastPayment = \CTszhPayment::GetList(
				Array("DATE_PAYED" => "DESC"),
				$this->filter,
				false,
				array('nTopCount' => 1),
				Array("ID", "DATE_PAYED")
			)->Fetch();
			$date = is_array($lastPayment) ? MakeTimeStamp($lastPayment["DATE_PAYED"]) : false;
			$date = Xml::formatDateTime($date);
			fputs($this->output, "<?xml version=\"1.0\" encoding=\"" . SITE_CHARSET . "\"?>\n<payments inn=\"$inn\" filetype=\"payments\" filedate=\"$date\" version=\"4\">\n");

			$count = \CTszhPayment::GetList(array(), $this->filter, Array("COUNT" => "ID"))->Fetch();
			$this->state['progress'] = Array(
				'total' => $count["ID"],
				'done' => 0,
				'remaining' => $count["ID"],
			);
			$lastId = 0;
		}

		$filter = array_merge($this->filter, Array(">ID" => $lastId));
		$dbPayments = \CTszhPayment::GetList(Array("ID" => "ASC"), $filter, false, false, Array(
			"ID",
			"DATE_PAYED",
			"SUMM",
			"ACCOUNT_XML_ID",
			"ACCOUNT_EXTERNAL_ID",
			"PSYS_FEE",
			"PS_ACTION_FILE",
			"C_ACCOUNT",
			"INSURANCE_INCLUDED",
			"IS_OVERHAUL",
			"IS_PENALTY",
		));
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($payment = $dbPayments->Fetch())
		{
			$lastId = $payment["ID"];
			$attr = Array(
				"acc_id" => $payment["ACCOUNT_EXTERNAL_ID"],
				"date" => Xml::formatDateTime(MakeTimeStamp($payment["DATE_PAYED"])),
				"sum" => $payment["SUMM"],
				"ps" => basename($payment["PS_ACTION_FILE"]),
			);
			if (is_set($payment, "PSYS_FEE") && $payment["PSYS_FEE"] > 0)
			{
				$attr["fee"] = $payment["PSYS_FEE"];
			}
			if (is_set($payment, "INSURANCE_INCLUDED"))
			{
				$attr["insurance_included"] = $payment["INSURANCE_INCLUDED"];
			}
			if (is_set($payment, "IS_OVERHAUL"))
			{
				$attr["is_overhaul"] = $payment["IS_OVERHAUL"] == 'Y' ? 1 : 0;
			}
			if (is_set($payment, "IS_PENALTY"))
			{
				$attr["is_penalty"] = $payment["IS_PENALTY"] == 'Y' ? 1 : 0;
			}
			$paymentString = "\t<payment";
			foreach ($attr as $k => $v)
			{
				$paymentString .= " $k=\"$v\"";
			}
			$paymentString .= " />\n";
			fputs($this->output, $paymentString);

			$this->state['progress']['done']++;
			$this->state['progress']['remaining']--;
			if ($this->state['stepTime'] && microtime(1) - $startTime >= $this->state['stepTime'])
			{
				break;
			}
		}
		if ($this->state['progress']['remaining'] <= 0)
		{
			$this->state['progress']['remaining'] = 0;
			fputs($this->output, "</payments>");
		}

		return $lastId;
	}
}
