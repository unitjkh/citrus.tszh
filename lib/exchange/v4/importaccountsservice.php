<?

namespace Citrus\Tszh\Exchange\v4;

use Citrus\Tszh\Exchange\v4\Formats\Accounts;
use Citrus\Tszh\Exchange\ServiceBase;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * ��������� �������� ������ ������� ������
 * ?mode=import&type=accounts
 * http://wiki.citrus-soft.ru/dev/import-accounts
 *
 * @package Citrus\Tszh\Controller\v4
 */
class ImportAccountsService extends ServiceBase
{
	/**
	 * @var float Timestamp ������� ������ ���������
	 */
	protected $startTime = null;

	/**
	 * @var bool Show debug information
	 */
	private $debug = false;

	/**
	 * @throws \Exception
	 */
	public function run()
	{
		if (!\CModule::IncludeModule("citrus.tszh"))
		{
			throw new \Exception(Loc::getMessage("TSZH_1C_ERROR_MODULE"));
		}

		if (!\CModule::IncludeModule("iblock"))
		{
			throw new \Exception(Loc::getMessage("TSZH_1C_ERROR_IBLOCK_MODULE"));
		}

		$arTszh = $this->facade->getOrg();
		if (!is_array($arTszh) || IntVal($arTszh["ID"]) <= 0)
		{
			throw new \Exception(Loc::getMessage("TSZH_1C_ERROR_ORG_NOT_FOUND"));
		}

		// $this->startTime = time();
		$this->setStartTime();
		$ABS_FILE_NAME = $this->facade->getUploadedFile();
		$NS = &$_SESSION["BX_CITRUS_TSZH_IMPORT"];
		if (!is_array($NS))
		{
			$NS = array(
				"STEP" => 0,
				"TSZH" => $arTszh['ID'],
				"ONLY_DEBT" => \COption::GetOptionString('citrus.tszh', '1c_exchange.UpdateMode', "Y") == "D",
				"UPDATE_MODE" => \COption::GetOptionString('citrus.tszh', '1c_exchange.UpdateMode', "Y") == "Y",
				"CREATE_USERS" => \COption::GetOptionString('citrus.tszh', '1c_exchange.CreateUsers', "Y") == "Y",
				//"UPDATE_USERS" => COption::GetOptionString('citrus.tszh', '1c_exchange.UpdateUsers', "N") != "N",
				"ACTION" => \COption::GetOptionString('citrus.tszh', '1c_exchange.Action', "A"),
				"DEPERSONALIZE" => \COption::GetOptionString('citrus.tszh', '1c_exchange.Depersonalize', "N") == "Y",
			);
		}

		$NS['startTime'] = $this->startTime;

		$obImport = new Accounts($NS, $this->facade->timelimit, true);

		try
		{
			$progress = array();
			$finished = false;
			do
			{
				if ($NS["STEP"] < 1)
				{
					$NS["STEP"]++;

					$progress[] = Loc::getMessage("TI_STEP1_DONE");
				}
				elseif ($NS["STEP"] < 2)
				{
					if (file_exists($ABS_FILE_NAME) && is_file($ABS_FILE_NAME) && ($fp = fopen($ABS_FILE_NAME, "rb")))
					{
						if ($obImport->readXml($fp))
						{
							$NS["STEP"]++;
						}
						fclose($fp);
					}
					else
					{
						throw new \Exception(Loc::getMessage("TI_STEP3_ERROR"));
					}

					$file_size = file_exists($ABS_FILE_NAME) && is_file($ABS_FILE_NAME) ? filesize($ABS_FILE_NAME) : 0;
					$progress[] = Loc::getMessage("TI_STEP3_PROGRESS", Array('#PROGRESS#' => ($file_size > 0 ? round($obImport->getXml()->GetFilePosition() / $file_size * 100, 2) : 0)));
				}
				elseif ($NS["STEP"] < 3)
				{
					$obImport->ProcessPeriod();
					$obImport->ImportServices();
					$NS['ACCOUNTS_IMPORT_STARTED'] = time();
					$NS["STEP"]++;
					$progress[] = Loc::getMessage("TI_STEP5_DONE");
				}
				elseif ($NS["STEP"] < 4)
				{
					if ($obImport->importHouses())
					{
						$_SESSION['BX_CITRUS_TSZH_IMPORT_RESPONSE']['details']['houses'] = true;

						$progress[] = Loc::getMessage("CITRUS_TSZH_HOUSES_IMPORTED");
						$NS["STEP"]++;
					}
					else
					{
						$progress[] = Loc::getMessage("CITRUS_TSZH_HOUSES_LOADING");
					}
				}
				elseif ($NS["STEP"] < 5)
				{
					$NS["STEP"]++;
					$progress[] = Loc::getMessage("TI_STEP4_DONE");
				}
				elseif ($NS["STEP"] < 6)
				{
					$result = $obImport->ImportAccounts();

					$counter = 0;
					foreach ($result as $key => $value)
					{
						$NS["DONE"][$key] += $value;
						$counter += $value;
					}

					if (!$counter)
					{
						$_SESSION['BX_CITRUS_TSZH_IMPORT_RESPONSE']['details']['accs'] = true;

						$progress[] = Loc::getMessage("TI_STEP6_DONE");
						$NS["STEP"]++;
					}
					else
					{
						$progress[] = Loc::getMessage("TI_STEP6_PROGRESS", Array(
							'#CNT#' => intval($NS["DONE"]["ADD"] + $NS["DONE"]["UPD"] + $NS["DONE"]["ERR"]),
							'#ERR#' => intval($NS["DONE"]["ERR"]),
							'#TOTAL#' => intval($NS["DONE"]["ALL"]),
						));
					}
				}
				elseif ($NS["STEP"] < 7)
				{
					if ($NS['ADDTITIONAL_PERIODS_EXISTS'])
					{
						$result = $obImport->ImportAdditionalPeriods();

						if ($result)
						{
							$this->nextStep();
						}
					}
					else
					{
						$this->nextStep();
					}
				}
				elseif ($NS["STEP"] < 8)
				{
					$result = $obImport->Cleanup($NS["ACTION"]);

					$counter = 0;
					foreach ($result as $key => $value)
					{
						$NS["DONE"][$key] += $value;
						$counter += $value;
					}

					if (!$counter)
					{
						$NS["STEP"]++;
						$progress[] = Loc::getMessage("TI_STEP7_DONE");
					}
					else
					{
						$progress[] = Loc::getMessage("TI_STEP7_PROGRESS", Array('#CNT#' => intval($NS["DONE"]["DEL"]) + intval($NS["DONE"]["METER_DEL"]) + intval($NS["DONE"]["DEA"]) + intval($NS["DONE"]["METER_DEA"])));
					}
				}
				elseif ($NS["STEP"] < 9)
				{
					if (!$this->facade->debug)
					{
						@unlink($ABS_FILE_NAME);
					}

					if (!$this->facade->debug && \CTszh::hasDemoAccounts($NS["TSZH"], $arDemoAccounts))
					{
						$res = false;
						foreach ($arDemoAccounts as $accountID => $arAccount)
						{
							$res = \CTszhAccount::delete($accountID);
						}
						if ($res)
						{
							$progress[] = Loc::getMessage("TI_STEP8_PROGRESS");
						}
					}

					$NS["STEP"]++;

					$strErrors = \CTszhImport::GetErrors();
					if (strlen($strErrors) > 0)
					{
						$_SESSION['BX_CITRUS_TSZH_IMPORT_RESPONSE']['status'] = 'warning';
						$_SESSION['BX_CITRUS_TSZH_IMPORT_RESPONSE']['error'] = $strErrors;

						echo "warning\n" . $strErrors;
						$this->facade->textOutput = true;

						return;
					}

					// ������� ������ ����� ���������� ������
					$obImport->clearSession();

					// $NS = false;
					$_SESSION['BX_CITRUS_TSZH_IMPORT_RESPONSE']['status'] = 'success';
					echo "success\n" . Loc::getMessage("TI_STEP8_DONE");
					if ($this->facade->debug)
					{
						$this->facade->logMessage(implode("\n", $progress));
					}
					$finished = true;
					break;
				}
				else
				{
					if ($this->debug)
					{
						$NS["STEP"] = 0;
					}
					else
					{
						throw new \Exception(Loc::getMessage("TI_ERROR_ALREADY_FINISHED"));
					}
				}
			}
			while (!$this->timeIsUp());

			$_SESSION["BX_CITRUS_TSZH_IMPORT_RESULT"] = $NS;
			$_SESSION["BX_CITRUS_TSZH_IMPORT_RESULT"]['status'] = $finished ? 'success' : 'progress';

			if ($finished)
			{
				if ($this->facade->debug)
				{
					$this->facade->logMessage(Loc::getMessage("CITRUS_TSZH_IMPORT_FINISH"));
				}
				$NS = false;
			}
			else
			{
				$_SESSION['BX_CITRUS_TSZH_IMPORT_RESPONSE']['status'] = 'progress';
				$_SESSION['BX_CITRUS_TSZH_IMPORT_RESPONSE']['details']['progress_status'] = implode("\n", $progress);

				echo "progress\n" . implode("\n", $progress);
			}
		}
		catch (\Exception $e)
		{
			$NS = false;
			throw $e;
		}

		$this->facade->textOutput = true;
	}

	/**
	 * ������������� ����� ������ ���������. �������� ������������ ��������� ���� �� ������ ����������� �������
	 */
	protected function setStartTime()
	{
		if (isset($this->startTime))
		{
			return;
		}

		if (defined("START_EXEC_PROLOG_BEFORE_1"))
		{
			list($usec, $sec) = explode(" ", START_EXEC_PROLOG_BEFORE_1);
			$this->startTime = $sec + $usec;
		}
		else
		{
			$this->startTime = microtime(true);
		}
	}

	/**
	 * ��������� �� ������� �� �����, ���������� �� ���� ��� (���) ������
	 * @return bool
	 * @throws \Exception ���� ����� ���� �� ��� ������ ����� setStartTime
	 */
	protected function timeIsUp()
	{
		return microtime(true) - (float)$this->startTime > (float)$this->facade->timelimit;
	}

	private function nextStep()
	{
		$NS = &$_SESSION["BX_CITRUS_TSZH_IMPORT"];
		$NS["STEP"]++;
	}
}