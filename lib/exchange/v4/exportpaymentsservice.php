<?
namespace Citrus\Tszh\Exchange\v4;

use Citrus\Tszh\Exchange\ServiceBase;
use Citrus\Tszh\Exchange\Facade;
use \Bitrix\Main\Localization\Loc;
use Citrus\Tszh\Exchange\v4\Formats\Payments;

Loc::loadMessages(__FILE__);

/**
 * ��������� �������� �������� ��������
 * ?mode=export&type=payments
 * http://wiki.citrus-soft.ru/dev/export-payments
 *
 * @package Citrus\Tszh\Controller\v4
 */
class ExportPaymentsService extends ServiceBase
{
	public function run()
	{
		if (!\CModule::IncludeModule("citrus.tszhpayment"))
			throw new \Exception(Loc::getMessage("CITRUS_TSZH_1C_ERROR_PAYMENT_MODULE"));

		$org = $this->facade->getOrg();
		$dateFrom = $this->facade->dateFrom();
		$dateTo = $this->facade->dateTo();

		$arFilter = Array(
			"TSZH_ID" => $org['ID'],
			"PAYED" => "Y",
		);
		if ($dateFrom)
			$arFilter[">DATE_PAYED"] = $dateFrom;
		if ($dateTo)
			$arFilter["<=DATE_PAYED"] = $dateTo;

		$state = &$_SESSION["CITRUS.TSZH.PAYMENTS.EXPORT"];
		$state = Array();

		$export = new Payments($_SESSION["CITRUS.TSZH.PAYMENTS.EXPORT"]);
		$export->filter($arFilter)->setOrg($org["ID"]);
		$export->run();

		$this->facade->xmlOutput = true;
	}
}

