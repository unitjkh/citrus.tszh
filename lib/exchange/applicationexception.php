<?

namespace Citrus\Tszh\Exchange;

use Bitrix\Main\Localization\Loc;

/**
 * ������������ $APPLICATION->GetException() ��� ��������� ��������� �� ������
 * @package Citrus\Tszh\Exchange\Exception
 */
class ApplicationException extends \Exception
{
	/**
	 * @param string $messageCode ��� ��������� ��������� �� ������
	 * @param array $args ������ ��� ������ (���������� � Loc::getMessage)
	 */
	function __construct($messageCode, $args = array())
	{
		global $APPLICATION;

		$ex = $APPLICATION->GetException();
		parent::__construct(Loc::getMessage($messageCode, $args) . ($ex ? ': ' . $ex->GetString() : '') . ' (' . $messageCode . ')');
	}
}