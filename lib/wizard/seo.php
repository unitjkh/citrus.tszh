<?

namespace Citrus\Tszh\Wizard;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Seo\SitemapTable;
use \Bitrix\Seo\SitemapIblockTable;
use \Bitrix\Seo\SitemapForumTable;

Loc::loadMessages(__FILE__);

class Seo
{
	private static $seoModuleIncluded = null;
	private static $sitemap = null;

	private static function init()
	{
		if (!isset(self::$seoModuleIncluded))
			self::$seoModuleIncluded = \CModule::IncludeModule("seo");
		
		self::$sitemap = &$_SESSION['citrus.tszh.seo.sitemap'];
		if (!is_array(self::$sitemap))
			return false;

		return self::$seoModuleIncluded;
	}

	/**
	 * �������������� ��������� ��������� ��� ���������� � Sitemap
	 *
	 * @param int $iblockId ID ���������
	 * @param bool $active �������������
	 * @param bool $list ������
	 * @param bool $section ������
	 * @param bool $element ��������
	 */
	public static function setIblockSettings($iblockId, $active = false, $list = false, $section = false, $element = false)
	{
		if (!self::init())
			return;

		self::$sitemap["SETTINGS"]["IBLOCK_ACTIVE"][$iblockId] = $active ? "Y" : "N";
		self::$sitemap["SETTINGS"]["IBLOCK_LIST"][$iblockId] = $list ? "Y" : "N";
		self::$sitemap["SETTINGS"]["IBLOCK_SECTION"][$iblockId] = $section ? "Y" : "N";
		self::$sitemap["SETTINGS"]["IBLOCK_ELEMENT"][$iblockId] = $element ? "Y" : "N";
	}

	/**
	 * �������������� ��������� ������ ��� ���������� � Sitemap
	 *
	 * @param int $forumId ID ������
	 * @param bool $active �������������
	 * @param bool $topic ����
	 */
	public static function setForumSettings($forumId, $active = false, $topic = false)
	{
		if (!self::init())
			return;

		self::$sitemap["SETTINGS"]["FORUM_ACTIVE"][$forumId] = $active ? "Y" : "N";
		self::$sitemap["SETTINGS"]["FORUM_TOPIC"][$forumId] = $topic ? "Y" : "N";
	}

	/**
	 * �������������� ��������� �� ��������� ��� ��������� �������
	 *
	 * @param string $siteId ID �����
	 * @param string $siteDir �������� ����� �����
	 * @param string $serverName ����� ����� (�� ��������� $_SERVER["SERVER_NAME"]
	 * @return array
	 */
	public static function initSettings($siteId, $siteDir, $serverName = null)
	{
		self::init();
		if (!self::$seoModuleIncluded)
			return;

		self::$sitemap = array (
			'SITE_ID' => $siteId,
			'ACTIVE' => 'Y',
			'NAME' => Loc::getMessage("CITRUS_TSZH_SEO_DEFAULT_SITEMAP_SETTINGS"),
			'SETTINGS' =>
				array (
					'FILE_MASK' => '*.php',
					'ROBOTS' => 'Y',
					'logical' => 'Y',
					'DIR' =>
						array (
							$siteDir => 'Y',
							$siteDir . 'auth' => 'N',
							$siteDir . 'debtors' => 'N',
							$siteDir . 'forum' => 'N',
							$siteDir . 'news' => 'N',
							$siteDir . 'personal' => 'N',
							$siteDir . 'photogallery' => 'N',
							$siteDir . 'questions-and-answers' => 'N',
							$siteDir . 'search' => 'N',
							$siteDir . 'video' => 'N',
							$siteDir . 'workplace' => 'N',
						),
					'FILE' =>
						array (
							$siteDir . '404.php' => 'N',
						),
					'PROTO' => '0',
					'DOMAIN' => $serverName ? $serverName : $_SERVER["SERVER_NAME"],
					'FILENAME_INDEX' => 'sitemap.xml',
					'FILENAME_FILES' => 'sitemap_files.xml',
					'FILENAME_IBLOCK' => 'sitemap_iblock_#IBLOCK_ID#.xml',
					'FILENAME_FORUM' => 'sitemap_forum_#FORUM_ID#.xml',
					'IBLOCK_ACTIVE' => array(),
					'IBLOCK_LIST' => array(),
					'IBLOCK_SECTION' => array(),
					'IBLOCK_ELEMENT' => array(),
					'IBLOCK_SECTION_SECTION' => array(),
					'IBLOCK_SECTION_ELEMENT' => array(),
					'FORUM_ACTIVE' => array(),
					'FORUM_TOPIC' => array(),
					'FILE_MASK_REGEXP' => '/^(.*?\\.php)$/i',
				),
		);
	}

	/**
	 * @param bool $autoGenerate �������� �� �������������? � �������� �� ������������: ��� ��������� � true ����� �������� � ���������/���������� �������� � ���������� �� ������ ��������� sitemap.
	 * @throws \Bitrix\Main\ArgumentException
	 */
	public static function save($autoGenerate = false)
	{
		if (!self::init())
			return;

		$existing = SitemapTable::getList(array("filter" => array("SITE_ID" => self::$sitemap["SITE_ID"], "NAME" => Loc::getMessage("CITRUS_TSZH_SEO_DEFAULT_SITEMAP_SETTINGS"))))->fetch();
		$settings = self::$sitemap["SETTINGS"];
		self::$sitemap["SETTINGS"] = serialize($settings);

		if (is_array($existing))
		{
			$result = SitemapTable::update($existing["ID"], self::$sitemap);
			$ID = $existing["ID"];
		}
		else
		{
			$result = SitemapTable::add(self::$sitemap);
			$ID = $result->getId();
		}

		if($result->isSuccess() && $autoGenerate)
		{
			SitemapIblockTable::clearBySitemap($ID);
			foreach ($settings["IBLOCK_ACTIVE"] as $iblockId => $active)
			{
				if($active === 'Y')
					SitemapIblockTable::add(array(
						'SITEMAP_ID' => $ID,
						'IBLOCK_ID' => intval($iblockId),
					));
			}

			SitemapForumTable::clearBySitemap($ID);
			foreach($settings["FORUM_ACTIVE"] as $forumId => $auto)
			{
				if($auto === 'Y')
					SitemapForumTable::add(array( "SITEMAP_ID" => $ID, "ENTITY_ID" => $forumId));
			}
		}
		else
		{
			throw new \Exception(implode('<br>', $result->getErrorMessages()));
		}
		unset($_SESSION['citrus.tszh.seo.sitemap']);
	}

}