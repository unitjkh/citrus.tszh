<?php
namespace Citrus\Tszh;

use Bitrix\Main,
	Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Class TszhTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> NAME string(100) mandatory
 * <li> SITE_ID string(2) mandatory
 * <li> CODE string(50) mandatory
 * <li> TIMESTAMP_X datetime mandatory default 'CURRENT_TIMESTAMP'
 * <li> ADDRESS string(255) mandatory
 * <li> PHONE string(255) mandatory
 * <li> PHONE_DISP string(255) mandatory
 * <li> INN string(12) mandatory
 * <li> KPP string(9) mandatory
 * <li> KBK string(20) mandatory
 * <li> OKTMO string(11) mandatory
 * <li> RSCH string(24) mandatory
 * <li> BANK string(100) mandatory
 * <li> KSCH string(24) mandatory
 * <li> BIK string(16) mandatory
 * <li> IS_BUDGET string(1) mandatory
 * <li> METER_VALUES_START_DATE int optional
 * <li> METER_VALUES_END_DATE int optional
 * <li> HEAD_NAME string(255) mandatory
 * <li> LEGAL_ADDRESS string(255) mandatory
 * <li> MONETA_ENABLED string(1) mandatory
 * <li> MONETA_OFFER string(1) mandatory
 * <li> MONETA_EMAIL string(255) mandatory
 * <li> OFFICE_HOURS string optional
 * <li> EMAIL string(255) optional
 * <li> RECEIPT_TEMPLATE string(255) optional
 * <li> ADDITIONAL_INFO_MAIN string(65535) mandatory
 * <li> ADDITIONAL_INFO_OVERHAUL string(65535) mandatory
 * <li> ANNOTATION_MAIN string(65535) mandatory
 * <li> ANNOTATION_OVERHAUL string(65535) mandatory
 * </ul>
 *
 * @package Citrus\Tszh
 **/
class TszhTable extends Main\Entity\DataManager
{
	/**
	 * ������� ��������� ������ ��� ������� ��������� ����������� ������.��
	 * @var array
	 */
	protected static $rschIncorrectPatterns = array('/^40821[\d]*/');

	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'b_tszh';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return array(
			'ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'autocomplete' => true,
				'title' => Loc::getMessage('TSZH_ENTITY_ID_FIELD'),
			),
			'NAME' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateName'),
				'title' => Loc::getMessage('TSZH_ENTITY_NAME_FIELD'),
			),
			'SITE_ID' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateSiteId'),
				'title' => Loc::getMessage('TSZH_ENTITY_SITE_ID_FIELD'),
			),
			'CODE' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateCode'),
				'title' => Loc::getMessage('TSZH_ENTITY_CODE_FIELD'),
			),
			'TIMESTAMP_X' => array(
				'data_type' => 'datetime',
				'required' => true,
				'title' => Loc::getMessage('TSZH_ENTITY_TIMESTAMP_X_FIELD'),
			),
			'ADDRESS' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateAddress'),
				'title' => Loc::getMessage('TSZH_ENTITY_ADDRESS_FIELD'),
			),
			'PHONE' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validatePhone'),
				'title' => Loc::getMessage('TSZH_ENTITY_PHONE_FIELD'),
			),
			'PHONE_DISP' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validatePhoneDisp'),
				'title' => Loc::getMessage('TSZH_ENTITY_PHONE_DISP_FIELD'),
			),
			'INN' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateInn'),
				'title' => Loc::getMessage('TSZH_ENTITY_INN_FIELD'),
			),
			'KPP' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateKpp'),
				'title' => Loc::getMessage('TSZH_ENTITY_KPP_FIELD'),
			),
			'KBK' => array(
				'data_type' => 'string',
				'required' => false,
				'validation' => array(__CLASS__, 'validateKpp'),
				'title' => Loc::getMessage('TSZH_ENTITY_KBK_FIELD'),
			),
			'OKTMO' => array(
				'data_type' => 'string',
				'required' => false,
				'validation' => array(__CLASS__, 'validateKpp'),
				'title' => Loc::getMessage('TSZH_ENTITY_OKTMO_FIELD'),
			),
			'RSCH' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateRsch'),
				'title' => Loc::getMessage('TSZH_ENTITY_RSCH_FIELD'),
			),
			'BANK' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateBank'),
				'title' => Loc::getMessage('TSZH_ENTITY_BANK_FIELD'),
			),
			'KSCH' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateKsch'),
				'title' => Loc::getMessage('TSZH_ENTITY_KSCH_FIELD'),
			),
			'BIK' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateBik'),
				'title' => Loc::getMessage('TSZH_ENTITY_BIK_FIELD'),
			),
			'IS_BUDGET' => array(
				'data_type' => 'string',
				'required' => true,
				'title' => Loc::getMessage('TSZH_ENTITY_IS_BUDGET_FIELD'),
			),
			'METER_VALUES_START_DATE' => array(
				'data_type' => 'integer',
				'title' => Loc::getMessage('TSZH_ENTITY_METER_VALUES_START_DATE_FIELD'),
			),
			'METER_VALUES_END_DATE' => array(
				'data_type' => 'integer',
				'title' => Loc::getMessage('TSZH_ENTITY_METER_VALUES_END_DATE_FIELD'),
			),
			'HEAD_NAME' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateHeadName'),
				'title' => Loc::getMessage('TSZH_ENTITY_HEAD_NAME_FIELD'),
			),
			'LEGAL_ADDRESS' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateLegalAddress'),
				'title' => Loc::getMessage('TSZH_ENTITY_LEGAL_ADDRESS_FIELD'),
			),
			'MONETA_ENABLED' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateMonetaEnabled'),
				'title' => Loc::getMessage('TSZH_ENTITY_MONETA_ENABLED_FIELD'),
			),
			'MONETA_OFFER' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateMonetaOffer'),
				'title' => Loc::getMessage('TSZH_ENTITY_MONETA_OFFER_FIELD'),
			),
			'MONETA_EMAIL' => array(
				'data_type' => 'string',
				'required' => true,
				'validation' => array(__CLASS__, 'validateMonetaEmail'),
				'title' => Loc::getMessage('TSZH_ENTITY_MONETA_EMAIL_FIELD'),
			),
			'OFFICE_HOURS' => array(
				'data_type' => 'text',
				'title' => Loc::getMessage('TSZH_ENTITY_OFFICE_HOURS_FIELD'),
			),
			'EMAIL' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateEmail'),
				'title' => Loc::getMessage('TSZH_ENTITY_EMAIL_FIELD'),
			),
			'RECEIPT_TEMPLATE' => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateReceiptTemplate'),
				'title' => Loc::getMessage('TSZH_ENTITY_RECEIPT_TEMPLATE_FIELD'),
			),
			"ADDITIONAL_INFO_MAIN" => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateAdditionalInfoMain'),
				'title' => Loc::getMessage('ADDITIONAL_INFO_MAIN'),
			),
			"ADDITIONAL_INFO_OVERHAUL" => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateAdditionalInfoOverhaul'),
				'title' => Loc::getMessage('ADDITIONAL_INFO_OVERHAUL'),
			),
			"ANNOTATION_MAIN" => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateAnnotationMain'),
				'title' => Loc::getMessage('ANNOTATION_MAIN'),
			),
			"ANNOTATION_OVERHAUL" => array(
				'data_type' => 'string',
				'validation' => array(__CLASS__, 'validateAnnotationOverHaul'),
				'title' => Loc::getMessage('ANNOTATION_OVERHAUL'),
			),
		);
	}

	/**
	 * Returns validators for NAME field.
	 *
	 * @return array
	 */
	public static function validateName()
	{
		return array(
			new Main\Entity\Validator\Length(null, 100),
		);
	}

	/**
	 * Returns validators for SITE_ID field.
	 *
	 * @return array
	 */
	public static function validateSiteId()
	{
		return array(
			new Main\Entity\Validator\Length(null, 2),
		);
	}

	/**
	 * Returns validators for CODE field.
	 *
	 * @return array
	 */
	public static function validateCode()
	{
		return array(
			new Main\Entity\Validator\Length(null, 50),
		);
	}

	/**
	 * Returns validators for ADDRESS field.
	 *
	 * @return array
	 */
	public static function validateAddress()
	{
		return array(
			new Main\Entity\Validator\Length(null, 255),
		);
	}

	/**
	 * Returns validators for PHONE field.
	 *
	 * @return array
	 */
	public static function validatePhone()
	{
		return array(
			new Main\Entity\Validator\Length(null, 255),
		);
	}

	/**
	 * Returns validators for PHONE_DISP field.
	 *
	 * @return array
	 */
	public static function validatePhoneDisp()
	{
		return array(
			new Main\Entity\Validator\Length(null, 255),
		);
	}

	/**
	 * Returns validators for INN field.
	 *
	 * @return array
	 */
	public static function validateInn()
	{
		return array(
			new Main\Entity\Validator\Length(null, 12),
		);
	}

	/**
	 * Returns validators for KPP field.
	 *
	 * @return array
	 */
	public static function validateKpp()
	{
		return array(
			new Main\Entity\Validator\Length(null, 9),
		);
	}

	/**
	 * Returns validators for RSCH field.
	 *
	 * @return array
	 */
	public static function validateRsch()
	{
		return array(
			new Main\Entity\Validator\Length(null, 24),
		);
	}

	/**
	 * Returns validators for BANK field.
	 *
	 * @return array
	 */
	public static function validateBank()
	{
		return array(
			new Main\Entity\Validator\Length(null, 100),
		);
	}

	/**
	 * Returns validators for KSCH field.
	 *
	 * @return array
	 */
	public static function validateKsch()
	{
		return array(
			new Main\Entity\Validator\Length(null, 24),
		);
	}

	/**
	 * Returns validators for BIK field.
	 *
	 * @return array
	 */
	public static function validateBik()
	{
		return array(
			new Main\Entity\Validator\Length(null, 16),
		);
	}

	/**
	 * Returns validators for HEAD_NAME field.
	 *
	 * @return array
	 */
	public static function validateHeadName()
	{
		return array(
			new Main\Entity\Validator\Length(null, 255),
		);
	}

	/**
	 * Returns validators for LEGAL_ADDRESS field.
	 *
	 * @return array
	 */
	public static function validateLegalAddress()
	{
		return array(
			new Main\Entity\Validator\Length(null, 255),
		);
	}

	/**
	 * Returns validators for MONETA_ENABLED field.
	 *
	 * @return array
	 */
	public static function validateMonetaEnabled()
	{
		return array(
			new Main\Entity\Validator\Length(null, 1),
		);
	}

	/**
	 * Returns validators for MONETA_OFFER field.
	 *
	 * @return array
	 */
	public static function validateMonetaOffer()
	{
		return array(
			new Main\Entity\Validator\Length(null, 1),
		);
	}

	/**
	 * Returns validators for MONETA_EMAIL field.
	 *
	 * @return array
	 */
	public static function validateMonetaEmail()
	{
		return array(
			new Main\Entity\Validator\Length(null, 255),
		);
	}

	/**
	 * Returns validators for EMAIL field.
	 *
	 * @return array
	 */
	public static function validateEmail()
	{
		return array(
			new Main\Entity\Validator\Length(null, 255),
		);
	}

	/**
	 * Returns validators for RECEIPT_TEMPLATE field.
	 *
	 * @return array
	 */
	public static function validateReceiptTemplate()
	{
		return array(
			new Main\Entity\Validator\Length(null, 255),
		);
	}
	
	/**
	 * Returns validators for ADDITIONAL_INFO_MAIN field.
	 *
	 * @return array
	 */
	public static function validateAdditionalInfoMain()
	{
		return array(
			new Main\Entity\Validator\Length(null, 65535),
		);
	}

	/**
	 * Returns validators for "ADDITIONAL_INFO_OVERHAUL" field.
	 *
	 * @return array
	 */
	public static function validateAdditionalInfoOverhaul()
	{
		return array(
			new Main\Entity\Validator\Length(null, 65535),
		);
	}

	/**
	 * Returns validators for ANNOTATION_MAIN field.
	 *
	 * @return array
	 */
	public static function validateAnnotationMain()
	{
		return array(
			new Main\Entity\Validator\Length(null, 65535),
		);
	}

	/**
	 * Returns validators for ANNOTATION_OVERHAUL field.
	 *
	 * @return array
	 */
	public static function validateAnnotationOverHaul()
	{
		return array(
			new Main\Entity\Validator\Length(null, 65535),
		);
	}

	/**
	 * @param $rsch
	 *
	 * @return bool
	 */
	public static function isRschCorrect($rsch)
	{
		if (!isset($rsch)
		    || strlen($rsch) == 0
		    || self::checkRschByPatterns($rsch, self::$rschIncorrectPatterns))
		{
			return false;
		}

		return true;
	}

	/**
	 * @param $rsch
	 *
	 * @return bool
	 */
	protected static function checkRschByPatterns($rsch, $patterns)
	{
		foreach ($patterns as $pattern)
		{
			if (preg_match($pattern, $rsch) > 0)
			{
				return true;
			}
		}

		return false;
	}
}