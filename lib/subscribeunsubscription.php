<?php
namespace Citrus\Tszh;

use Bitrix\Main,
	Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

/**
 * Class SubscribeUnsubscriptionTable
 *
 * Fields:
 * <ul>
 * <li> SUBSCRIBE_CODE string(50) mandatory
 * <li> USER_ID int mandatory
 * </ul>
 *
 * @package Citrus\Tszh
 **/
class SubscribeUnsubscriptionTable extends Main\Entity\DataManager
{
	/**
	 * Returns DB table name for entity.
	 *
	 * @return string
	 */
	public static function getTableName()
	{
		return 'b_tszh_subscribe_unsubscription';
	}

	/**
	 * Returns entity map definition.
	 *
	 * @return array
	 */
	public static function getMap()
	{
		return array(
			'SUBSCRIBE_CODE' => array(
				'data_type' => 'string',
				'primary' => true,
				'validation' => array(__CLASS__, 'validateSubscribeCode'),
				'title' => Loc::getMessage('SUBSCRIBE_UNSUBSCRIPTION_ENTITY_SUBSCRIBE_CODE_FIELD'),
			),
			'USER_ID' => array(
				'data_type' => 'integer',
				'primary' => true,
				'title' => Loc::getMessage('SUBSCRIBE_UNSUBSCRIPTION_ENTITY_USER_ID_FIELD'),
			),
		);
	}

	/**
	 * Returns validators for SUBSCRIBE_CODE field.
	 *
	 * @return array
	 */
	public static function validateSubscribeCode()
	{
		return array(
			new Main\Entity\Validator\Length(null, 50),
		);
	}
}