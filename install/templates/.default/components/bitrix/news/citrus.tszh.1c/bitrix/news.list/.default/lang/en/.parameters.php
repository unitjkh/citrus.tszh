<?
$MESS ['T_IBLOCK_DESC_NEWS_DATE'] = "Display element date";
$MESS ['T_IBLOCK_DESC_NEWS_NAME'] = "Display element title";
$MESS ['T_IBLOCK_DESC_NEWS_PICTURE'] = "Display element preview picture";
$MESS ['T_IBLOCK_DESC_NEWS_TEXT'] = "Display element preview text";

$MESS["PARAM_RESIZE_IMAGE_WIDTH"] = "Width of preview-images (PREVIEW_PICTURE)";
$MESS["PARAM_RESIZE_IMAGE_HEIGHT"] = "Height of preview-images (PREVIEW_PICTURE)";
?>