<?php
/**
 * @author  Maxim Cherepinin <m@vdgb-soft.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2018 otr@rarus.ru
 * @date 22.03.2018 12:44
 */

if (!function_exists('isAdaptiveTszhTemplate'))
{
	function isAdaptiveTszhTemplate()
	{
		return defined('WIZARD_TEMPLATE_ID') && (WIZARD_TEMPLATE_ID == 'citrus_tszh_adaptive');
	}
}