<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
if(!defined("WIZARD_DEFAULT_SITE_ID") && !empty($_REQUEST["wizardSiteID"]))
	define("WIZARD_DEFAULT_SITE_ID", $_REQUEST["wizardSiteID"]);

$arWizardDescription = Array(
	"NAME" => GetMessage("TSZH_WIZARD_NAME"),
	"DESCRIPTION" => GetMessage("TSZH_WIZARD_DESC"),
	"VERSION" => "1.0.0",
	"DEPENDENCIES" => Array(
		"main" => "12.0.0",
	),
	"START_TYPE" => "WINDOW",
	"WIZARD_TYPE" => "INSTALL",
	"IMAGE" => "/images/solution.png",
	"PARENT" => "wizard_sol",
	"TEMPLATES" => Array(
		Array('SCRIPT' => 'scripts/template.php', 'CLASS' => 'CCitrusTszhWizardTemplate')
	),
	"STEPS" => array(),
);
if (defined("WIZARD_DEFAULT_SITE_ID"))
	$arWizardDescription["STEPS"] = Array("SelectTemplateStep", "SelectThemeStep", "SiteSettingsStep", "TszhSettingsStep", "MonetaSettingsStep", "AdditionalSettingsStep", "DataInstallStep" ,"FinishStep");
else
	$arWizardDescription["STEPS"] = Array("SelectSiteStep", "SelectTemplateStep", "SelectThemeStep", "SiteSettingsStep", "TszhSettingsStep", "MonetaSettingsStep", "AdditionalSettingsStep", "DataInstallStep" ,"FinishStep");
?>