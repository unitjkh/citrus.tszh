<?php
$aMenuLinks = Array(
	Array(
		"Общая информация",
		"#SITE_DIR#personal/index.php",
		Array(),
		Array(),
		"",
	),
	Array(
		"История начислений и оплат",
		"#SITE_DIR#personal/circulating-sheet/",
		Array(),
		Array(),
		"",
	),
	Array(
		"Квитанция на оплату коммунальных услуг",
		"#SITE_DIR#personal/receipt/",
		Array(),
		Array("target" => "_blank"),
		"",
	),
	/*Array(
		"Оплатить коммунальные услуги через сайт",
		"#SITE_DIR#personal/payment/",
		Array(),
		Array(),
		"",
	),*/
	Array(
		"Данные счетчиков",
		"#SITE_DIR#personal/meters/",
		Array(),
		Array(),
		"",
	),
	/*Array(
		"Мои заявки",
		"#SITE_DIR#personal/support/",
		Array(),
		Array(),
		"",
	),
	Array(
		"Управление подпиской",
		"#SITE_DIR#personal/subscription/",
		Array(),
		Array(),
		"",
	),*/
);