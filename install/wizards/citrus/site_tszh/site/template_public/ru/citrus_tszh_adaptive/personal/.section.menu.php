<?php
$aMenuLinks = Array(
	Array(
		"Общая информация",
		"#SITE_DIR#personal/index.php",
		Array(),
		Array("class" => "leftmenu__img-common"),
		"",
	),
	Array(
		"История начислений и оплат",
		"#SITE_DIR#personal/circulating-sheet/",
		Array(),
		Array("class" => "leftmenu__img-history"),
		"",
	),
	Array(
		"Квитанция на оплату коммунальных услуг",
		"#SITE_DIR#personal/receipt/",
		Array(),
		Array("class" => "leftmenu__img-print", "target" => "_blank"),
		"",
	),
	/*Array(
		"Оплатить коммунальные услуги через сайт",
		"#SITE_DIR#personal/payment/",
		Array(),
		Array("class" => "leftmenu__img-payment"),
		"",
	),*/
	Array(
		"Данные счетчиков",
		"#SITE_DIR#personal/meters/",
		Array(),
		Array("class" => "leftmenu__img-meters"),
		"",
	),
	/*Array(
		"Мои заявки",
		"#SITE_DIR#personal/support/",
		Array(),
		Array("class" => "leftmenu__img-requests"),
		"",
	),*/
	/*Array(
		"Управление подпиской",
		"#SITE_DIR#personal/subscription/",
		Array(),
		Array("class" => "leftmenu__img-subscriptions"),
		"",
	),*/
);