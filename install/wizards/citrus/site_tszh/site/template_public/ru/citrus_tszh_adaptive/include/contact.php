<div class="information__block">
	<div class="contacts__title">Контакты
		<div class="bold-hr"></div>
	</div>
</div>
<div class="contacts__items">
	<div class="contacts__item">
		<div class="contacts__name">Диспетчерская</div>
		<div class="contacts__tel">+7 (495) 777-25-43 (Круглосуточно)</div>
		<div class="contacts__tel">+7 (836) 249-46-89</div>
	</div>
	<div class="contacts__item">
		<div class="contacts__name">Главный бухгалтер</div>
		<div class="contacts__tel">+7 (836) 254-78-26 доб.2 (с 10:00 до 18:00)</div>
		<div class="contacts__email">glavbuh@mail.ru</div>
	</div>
	<div class="contacts__item">
		<div class="contacts__name">Председатель УК</div>
		<div class="contacts__tel">+7 (836) 254-78-26 (с 10:00 до 14:00)</div>
		<div class="contacts__tel">+7 (937) 912-21-21</div>
		<div class="contacts__email">predsedatel@mail.ru</div>
	</div>
	<div class="contacts__item">
		<div class="contacts__name">Паспортный стол</div>
		<div class="contacts__tel">+7 (836) 254-78-26 доб.3</div>
	</div>
</div>