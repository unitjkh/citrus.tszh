<?
$MESS ['T_FAQ_QUESTION'] = "Вопрос";
$MESS ['T_FAQ_ANSWER'] = "Ответ";
$MESS ['T_FAQ_ANSWER_AUTHOR'] = "Автор ответа";
$MESS ['T_FAQ_NO_ANSWER'] = "Ответ на вопрос пока не был получен";
$MESS ['T_FAQ_ANSWER_ON_EMAIL'] = "При получении ответа на Ваш вопрос будет отправлено уведомление на E-Mail, указанный Вами.";
$MESS ['T_FAQ_QUESTION_NOT_FOUND'] = "Вопрос не найден.";
$MESS ['T_FAQ_QUESTION_ID_NOT_FOUND'] = "Вопрос с номером #ID# не найден.";
$MESS ['T_FAQ_BACK_TO_SECTION'] = "В раздел «Вопрос-Ответ»";
$MESS ['T_FAQ_CHECK_QUESTION_STATUS'] = "Проверить статус вашего вопроса";
$MESS ['T_FAQ_ENTER_QUESTION_NUMBER'] = "Введите номер вопроса";
$MESS ['T_FAQ_CHECK'] = "Проверить";
$MESS ['T_FAQ_ASK_QUESTION'] = "Задать вопрос";
?>