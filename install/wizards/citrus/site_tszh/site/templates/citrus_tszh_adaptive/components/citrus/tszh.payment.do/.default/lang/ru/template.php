<?
$MESS["TSZH_PAYMENT_DO"] = "Оплата услуг";
$MESS["TSZH_PAYMENT_AMOUNT"] = "Сумма для оплаты: ";
$MESS["TSZH_PAYMENT_BUTTON_DO"] = "Продолжить";
$MESS["TSZH_PAYMENT_PAY_WIN"] = "Если окно с платежной информацией не открылось автоматически, нажмите на ссылку <a href=\"#LINK#\" target=\"_blank\">Оплатить</a>.";
$MESS["TSZH_PAYMENT_CONFIRM"] = "Я согласен(на) и даю право  хранить и обрабатывать, направленные мною в электронном виде персональные данные, с соблюдением требований российского законодательства о персональных данных.";
$MESS["CITRUS_TSZH_PAYEE_NAME"] = "ФИО плательщика";
$MESS["CITRUS_TSZH_C_ADDRESS"] = "Адрес";
$MESS["CITRUS_TSZH_C_ADDRESS_TOOLTIP"] = "Населенный пункт, улица, дом, квартира";
$MESS["CITRUS_TSZH_TSZH_ID"] = "Получатель платежа";
$MESS["CITRUS_TSZH_SELECT"] = "(выберите из списка)";
$MESS["CITRUS_TSZH_C_ACCOUNT"] = "Номер лицевого счета";
$MESS["CITRUS_TSZH_SUMM"] = "Сумма";
$MESS["CITRUS_TSZH_C_COMMENTS"] = "Комментарии";
$MESS["CITRUS_TSZH_C_COMMENTS_TOOLTIP"] = "Можете указать текущие показания счетчиков в произвольной форме";
$MESS["CITRUS_TSZH_AUTH_NOTE"] = "Для вашей организации доступен личный кабинет, рекомендуем войти в свой личный кабинет и оплатить из него.<br>В этом случае вам нужно будет вводить меньше данных &mdash; только лишь одобрить платеж.<br><br>Если у вас нет логина и пароля от Вашего личного кабинета, обратитесь в управляющую компанию по&nbsp;<nobr>телефону <b class=\"tszh-note-phone\">#PHONE#</b></nobr>.";
$MESS["TSZH_PERSONAL_ACCOUNT"] = "Лицевой счет";
$MESS["TSZH_PERSONAL_ACCOUNT_NUMBER"] = "л/с №";
$MESS["TSZH_PAYMENT_PERIOD"] = "Период платежа";
$MESS["TSZH_SUMM_PAYMENT"] = "Сумма платежа";
$MESS["TSZH_LAST_PAYMENT"] = "Последний платеж: #DATE# на сумму #SUM#";
$MESS["TSZH_PAYMENT_IS_BUDGET_NOTE"] = "* Для оплаты на счета бюджетных организаций необходима дополнительная идентификация плательщика (для физических лиц).";
$MESS["TSZH_PAYMENT_DOCUMENT_TYPE"] = "Тип документа";
$MESS["TSZH_PAYMENT_DOCUMENT_NUMBER"] = "Серия и номер документа";
$MESS["TSZH_PAYMENT_NATIONALITY"] = "Гражданство";
?>