<?
$MESS["CITRUS_SLIDER_WIDTH"] = "Ширина картинки";
$MESS["CITRUS_SLIDER_HEIGHT"] = "Высота картинки";
$MESS["CITRUS_SLIDER_DELAY"] = "Время показа слайда (мс)";
$MESS["CITRUS_SLIDER_SPEED"] = "Скорость анимации (мс)";
$MESS["CITRUS_SLIDER_HOVER_PAUSE"] = "Останавливать при наведении курсора";
$MESS["CITRUS_SLIDER_SHOW_NEXT_PREV"] = "Показывать ссылки следующий-предыдущий";
$MESS["CITRUS_SLIDER_SHOW_PAGINATION"] = "Показывать навигацию по слайдам";
?>