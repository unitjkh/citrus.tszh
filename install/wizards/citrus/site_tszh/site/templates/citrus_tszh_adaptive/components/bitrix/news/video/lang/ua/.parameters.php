<?
$MESS["T_IBLOCK_DESC_NEWS_DATE"] = "Виводити дату";
$MESS["T_IBLOCK_DESC_NEWS_PICTURE"] = "Виводити зображення для анонса";
$MESS["SEARCH_PAGE_ELEMENTS"] = "Кількість тегів";
$MESS["SEARCH_FONT_MAX"] = "Максимальний розмір шрифта (рх)";
$MESS["SEARCH_FONT_MIN"] = "Мінімальний розмір шрифта (рх)";
$MESS["SEARCH_COLOR_OLD"] = "Колір більш раннього тега (приклад: \"FEFEFE\") ";
$MESS["SEARCH_COLOR_NEW"] = "Колір пізнішого тега (приклад: \"C0C0C0\") ";
$MESS["SEARCH_PERIOD_NEW_TAGS"] = "Період, протягом якого вважати тег новим (днів) ";
$MESS["SEARCH_WIDTH"] = "Ширина хмари тегів (приклад: \"100%\" або \"100px\", \"100pt\", \"100in\") ";
$MESS["TP_CBIV_DISPLAY_AS_RATING"] = "В якості рейтингу показувати ";
$MESS["TP_CBIV_AVERAGE"] = "Середнє значення";
$MESS["TP_CBIV_RATING"] = "Рейтинг";
$MESS["T_IBLOCK_DESC_NEWS_TITLE"] = "Виводити назву";
?>