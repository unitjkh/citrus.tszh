<?
$MESS["CITRUS_SLIDER_WIDTH"] = "Ширина картинки";
$MESS["CITRUS_SLIDER_HEIGHT"] = "висота картинки";
$MESS["CITRUS_SLIDER_DELAY"] = "Час показу слайда ( мс)";
$MESS["CITRUS_SLIDER_SPEED"] = "Швидкість анімації ( мс)";
$MESS["CITRUS_SLIDER_HOVER_PAUSE"] = "Зупиняти при наведенні курсору";
$MESS["CITRUS_SLIDER_SHOW_NEXT_PREV"] = "Показувати посилання наступний - попередній";
$MESS["CITRUS_SLIDER_SHOW_PAGINATION"] = "Показувати навігацію по слайдах";
?>