<?
$MESS ['VOTE_GROUP_TOTAL'] = "всего";
$MESS ['VOTE_QUESTION_EMPTY'] = "На данный вопрос не было ни одного голоса";
$MESS ['CVF_DATES'] = "Время проведения";
$MESS["CVF_EDIT_VOTING"] = "Редактировать голосование";
$MESS["CVF_DELETE_VOTING"] = "Удалить голосование";
$MESS["CVF_EDIT_QUESTION"] = "Редактировать вопрос";
$MESS["CVF_DELETE_QUESTION"] = "Удалить вопрос";
$MESS["CVF_NOT_VOTED"] = "(не голосовали)";
