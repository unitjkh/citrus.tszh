<?
$MESS["T_FAQ_QUESTION"] = "Питання";
$MESS["T_FAQ_ANSWER"] = "Відповідь";
$MESS["T_FAQ_ANSWER_AUTHOR"] = "Автор відповіді";
$MESS["T_FAQ_NO_ANSWER"] = "Відповідь на питання поки що не отримана";
$MESS["T_FAQ_ANSWER_ON_EMAIL"] = "При отриманні відповіді на Ваше питання буде надіслано повідомлення на E-Mail, зазначений Вами. ";
$MESS["T_FAQ_QUESTION_NOT_FOUND"] = "Питання не знайдено.";
$MESS["T_FAQ_QUESTION_ID_NOT_FOUND"] = "Питання за номером #ID# не знайдено.";
$MESS["T_FAQ_BACK_TO_SECTION"] = "До розділу “Питання-Відповідь”";
$MESS["T_FAQ_CHECK_QUESTION_STATUS"] = "Перевірити статус вашого питання";
$MESS["T_FAQ_ENTER_QUESTION_NUMBER"] = "Введіть номер питання";
$MESS["T_FAQ_CHECK"] = "Перевірити";
$MESS["T_FAQ_ASK_QUESTION"] = "Поставити питання";
?>