<?
$MESS["T_IBLOCK_DESC_NEWS_DATE"] = "Виводити дату елемента";
$MESS["T_IBLOCK_DESC_NEWS_PICTURE"] = "Виводити зображення для анонсу";
$MESS["T_IBLOCK_DESC_NEWS_TEXT"] = "Виводити текст анонса";
$MESS["T_IBLOCK_DESC_NEWS_USE_SHARE"] = "Відображати панель соц. закладок";
$MESS["T_IBLOCK_DESC_NEWS_SHARE_HIDE"] = "Не розкривати панель соц. закладок за замовчуванням";
$MESS["T_IBLOCK_DESC_NEWS_SHARE_TEMPLATE"] = "Шаблон компонента панелі соц. закладок";
$MESS["T_IBLOCK_DESC_NEWS_SHARE_SYSTEM"] = "Використовувані соц. закладки та мережі";
$MESS["T_IBLOCK_DESC_NEWS_SHARE_SHORTEN_URL_LOGIN"] = "Логін для bit.ly";
$MESS["T_IBLOCK_DESC_NEWS_SHARE_SHORTEN_URL_KEY"] = "Ключ для для bit.ly";
$MESS["PARAM_RESIZE_IMAGE_WIDTH"] = "Ширина превью - картинок ( REVIEW_PICTURE і DETAIL_PICTURE ";
$MESS["PARAM_RESIZE_IMAGE_HEIGHT"] = "Висота превью - картинок ( REVIEW_PICTURE і DETAIL_PICTURE)";
$MESS["PARAM_COLORBOX_MAXWIDTH"] = "Максимальна ширина (MaxWidth) картинки (DETAIL_PICTURE) , що відображається ColorBox'ом";
$MESS["PARAM_COLORBOX_MAXHEIGHT"] = "Максимальна висота (MaxHeight) картинки (DETAIL_PICTURE) , що відображається ColorBox'ом";
$MESS["PARAM_MORE_PHOTO_PROPERTY"] = "Виводити додаткові картинки з властивості ( на детальної сторінці)";
?>