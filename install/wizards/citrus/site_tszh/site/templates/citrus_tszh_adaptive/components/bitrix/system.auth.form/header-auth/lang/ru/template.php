<?
$MESS["AUTH_AUTH"] = "Авторизация";
$MESS["AUTH_LOGIN_BUTTON"] = "Войти";
$MESS["AUTH_LOGIN"] = "Логин";
$MESS["AUTH_PASSWORD"] = "Пароль";
$MESS["AUTH_REMEMBER_ME"] = "Запомнить меня на этом компьютере";
$MESS["AUTH_REMEMBER_ME_MOBI"] = "Запомнить меня на этом устройстве";
$MESS["AUTH_REGISTER"] = "Регистрация";
$MESS["AUTH_LOGOUT_BUTTON"] = "выйти";
$MESS["AUTH_PROFILE"] = "Мой профиль";
$MESS["AUTH_A_INTERNAL"] = "Встроенная авторизация";
$MESS["AUTH_A_OPENID"] = "OpenID";
$MESS["AUTH_OPENID"] = "OpenID";
$MESS["AUTH_A_LIVEID"] = "LiveID";
$MESS["AUTH_LIVEID_LOGIN"] = "Log In";
$MESS["AUTH_CAPTCHA_PROMT"] = "Введите слово на картинке";
$MESS["AUTH_REMEMBER_SHORT"] = "Запомнить";
$MESS["AUTH_SECURE_NOTE"] = "Перед отправкой формы авторизации пароль будет зашифрован в браузере. Это позволит избежать передачи пароля в открытом виде.";
$MESS["AUTH_NONSECURE_NOTE"] = "Пароль будет отправлен в открытом виде. Включите JavaScript в браузере, чтобы зашифровать пароль перед отправкой.";
$MESS["T_AUTH_PROMPT"] = "Войдите на сайт";
$MESS["AUTH_FORGOT_PASSWORD"] = "Забыли пароль";
$MESS["AUTH_ENTER_BY_SOCIAL"] = "Войти через профиль в соцсетях";
$MESS["AUTH_FOR_INITIAL_ACCESS"] = "Для получения первоначального доступа к личному кабинету";
$MESS["AUTH_CONTACT_OUR_OFFICE"] = "обратитесь в офис нашей компании";
$MESS["AUTH_PASSWORD_RECOVERY"] = "Восстановление пароля";
$MESS["AUTH_IF_YOU_FORGET_PASSWORD_0"] = "Если вы забыли пароль, введите логин или E-Mail.";
$MESS["AUTH_IF_YOU_FORGET_PASSWORD_1"] = "Контрольная строка для смены пароля, а также ваши регистрационные данные, будут высланы вам по E-Mail.";
$MESS["OR"] = "или";
$MESS["AUTH_SEND_BUTTON"] = "Отправить";
