<?
$MESS ['TAC_YOUR_ACCOUNT'] = "Ваш лицевой счет: <strong>#ACCOUNT#</strong>";
$MESS ['TAC_GO_TO'] = "Перейти в ";
$MESS ['TAC_PERSONAL_SECTION'] = "Личный кабинет";
$MESS ['TAC_SELECT_TSZH'] = "Выберите Вашу УК:";
$MESS ['TAC_NOT_SELECTED'] = "(не выбрано)";
$MESS ['TAC_REG_WORD'] = "Кодовое слово, выданное УК:";
$MESS ['TAC_CAPTCH_PROMPT'] = "Введите символы, указанные на картинке:";
$MESS ['TAC_REQUIRED_FIELDS'] = "&ndash; поля, обязательные для заполнения";
$MESS ['TAC_BUTTON_CAPTION'] = "Подтвердить";
$MESS ['TAC_BUTTON_PREV'] = "< Назад";
$MESS ['TAC_BUTTON_CANCEL'] = "Отмена";
$MESS ['TAC_BUTTON_NEXT'] = "Далее >";
?>