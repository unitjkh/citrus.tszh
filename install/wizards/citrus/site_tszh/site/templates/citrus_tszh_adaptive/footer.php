<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$showLeftMenu = $APPLICATION->GetProperty('show_top_left', 'N') == 'Y';
//use Bitrix\Main\Localization\Loc;
//Loc::loadMessages(__FILE__);
$APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "",
    Array(
        "AREA_FILE_SHOW" => "page",
        "AREA_FILE_SUFFIX" => "inc",
        "AREA_FILE_RECURSIVE" => "Y",
        "EDIT_TEMPLATE" => "page_inc.php",
        "EDIT_MODE" => "html",
    ),
    false
);
//if($showLeftMenu)
//    echo '</div></div>';
if (!IS_SITE_DIR)
    echo '</div></div></div></div>';
$APPLICATION->IncludeFile(
	SITE_DIR . "/include/footer-auth.php",
    Array(),
    Array("MODE"=>"html")
);
?>
        </div>
		<footer>
            <?
            /*$APPLICATION->IncludeComponent(
                "citrus:tszh.contacts",
                "block",
                Array(
                    "CONTACTS_URL" => "",
                ),
                false
            );*/
            ?>
			<div class="contacts">
				<div class="contacts__block cont-p">

					<div class="information">
                        <?$APPLICATION->IncludeFile(
	                        SITE_DIR . "/include/contact.php",
                            Array(),
                            Array("MODE"=>"html")
                        );?>
					</div>
                    <?$APPLICATION->IncludeFile(SITE_DIR . "/include/feedback.php", Array(), Array("MODE"=>"html"));?>
				</div>
			</div>
            <?
                $APPLICATION->IncludeFile(
                SITE_DIR . "/include/footer-copyright.php",
                Array(),
                Array("MODE"=>"html")
                );
            ?>
		</footer>
	</body>
</html>