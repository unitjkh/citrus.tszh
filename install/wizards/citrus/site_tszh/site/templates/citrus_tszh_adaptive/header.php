<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}
require_once($_SERVER["DOCUMENT_ROOT"] . SITE_TEMPLATE_PATH . "/functions.php");

use Bitrix\Main\Page\Asset;

define('PATH_PERSONAL', 'personal');
$curPage = $APPLICATION->GetCurPage(false);
define('ISPERSONAL', strpos($curPage, SITE_DIR . PATH_PERSONAL) !== false);
/*if (strpos($curPage, SITE_DIR . "personal") !== false)
{
	$curPage = str_replace(SITE_DIR . "personal", SITE_DIR . PATH_PERSONAL, $curPage);
	LocalRedirect($curPage);
}*/
global $USER;
$APPLICATION->SetPageProperty("top_menu", "top_adaptive");
$APPLICATION->SetPageProperty("PATH_PERSONAL", PATH_PERSONAL);

define('IS_SITE_DIR', $curPage == SITE_DIR);
define('SHOW_NAV_CHAIN_DEFAULT', !IS_SITE_DIR && $APPLICATION->GetProperty('show_nav_chain_default', 'Y') == 'Y');

if (!isset($showLeftMenu))
{
	$showLeftMenu = !IS_SITE_DIR && $APPLICATION->GetProperty('show_top_left', 'N') == 'Y';
}
if (!isset($showTitleMiddle))
{
	$showTitleMiddle = !IS_SITE_DIR && ($APPLICATION->GetProperty('show_title_middle', 'N') == 'Y' || !$showLeftMenu);
}
if (!isset($showMenuMobile))
{
	$showMenuMobile = !IS_SITE_DIR && $APPLICATION->GetProperty('show_left_menu_mobile', 'N') == 'Y';
}

?>
<!DOCTYPE html>
<html>
	<head>
		<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" >
		<?
		CJsCore::Init(Array('jquery'));
		Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/script.js");
		Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/detect.min.js");
		$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH . '/colors.css');
		$APPLICATION->ShowHead(false);
		?>
		<title><? $APPLICATION->AddBufferContent('TemplateShowTitle'); ?></title>
	</head>
<body>
	<header>
		<? $APPLICATION->ShowPanel(); ?>
        <? $APPLICATION->IncludeComponent(
            "otr:tszh.header.switcher",
            "",
            Array (),
            false
        );?>
		<div class="top-line">
			<div class="top-line__box">
				<!--noindex--><?
				$APPLICATION->IncludeComponent(
					"bitrix:system.auth.form",
					"header-auth",
					array(
						"REGISTER_URL" => SITE_DIR . "auth/?register=yes",
						"FORGOT_PASSWORD_URL" => SITE_DIR . "auth/?forgot_password=yes",
						"PROFILE_URL" => SITE_DIR . PATH_PERSONAL . "/info/",
						"SHOW_ERRORS" => "Y",
					),
					false
				);
				?><!--/noindex-->
			</div>
		</div>

		<div class="head-info">
			<?
			$APPLICATION->IncludeFile(
				SITE_DIR . "/include/title_top.php",
				Array(),
				Array("MODE" => "html")
			);
			?>
			<? $APPLICATION->IncludeFile(
				SITE_DIR . "/include/phone.php",
				Array(),
				Array("MODE" => "html")
			); ?>
		</div>
		<? $APPLICATION->IncludeComponent("bitrix:menu", "top_multilevel", array(
			"ROOT_MENU_TYPE" => "top_adaptive",
			"MENU_CACHE_TYPE" => "A",
			"MENU_CACHE_TIME" => "3600",
			"MENU_CACHE_USE_GROUPS" => "Y",
			"MENU_CACHE_GET_VARS" => Array(),
			"MAX_LEVEL" => "2",
			"CHILD_MENU_TYPE" => "show_add",
			"USE_EXT" => "Y",
			"ALLOW_MULTI_SELECT" => "N",
		),
			false
		);
		if (!SHOW_NAV_CHAIN_DEFAULT)
		{
			?>
			<div class="breadcrumbs visible-desktop">
				<?
				$APPLICATION->IncludeComponent(
					"bitrix:breadcrumb",
					"",
					Array(
						"START_FROM" => "0",
						"PATH" => "",
						"SITE_ID" => "s1",
					),
					false
				);
				?>
			</div>
		<? }
		// ����� ��
if (ISPERSONAL && $USER->IsAuthorized())
{
	?>
	<?
	$APPLICATION->IncludeComponent(
		"citrus:tszh.account.choice_adaptive",
		".default",
		Array(),
		false
	); ?>
<? }
		// ����� ���������� �������/?>
	</header>
<div class="content">
<? if (!IS_SITE_DIR): ?>
	<div class="content__page">
	<div class="content__padding">
	<?
	if (ISPERSONAL && !$USER->IsAuthorized())
	{
		ShowMessage(GetMessage('ACCESS_IS_DENIED'));
		$APPLICATION->SetTitle(GetMessage('ACCESS_IS_DENIED'));
		require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php");
		die();
	}
	if (!$showTitleMiddle)
	{
		$APPLICATION->IncludeFile(
			SITE_TEMPLATE_PATH . "/include/title-page.php",
			Array("class" => "page-title-top"),
			Array("MODE" => "html")
		);
	}
	?>
	<? if (!$showLeftMenu):?>
		<div class="breadcrumbs <?=$showMenuMobile ? 'visible-desktop' : ''?>">
			&larr; <a class="breadcrumbs__link" href="../"><?=getMessage('H_BACK')?></a>
		</div>
	<?endif; ?>
	<? if ($showMenuMobile): ?>
		<div class="visible-mobi visible-tablet">
			<div class="div-center">
				<div class="mobi-leftmenu">
					<a class="mobi-leftmenu__header" href="javascript:void(0);" tabindex="1"></a>
					<?
					if (!$showLeftMenu)
					{
						$APPLICATION->IncludeComponent("bitrix:main.include", "left-menu", array(
							"AREA_FILE_SHOW" => "sect",
							"AREA_FILE_SUFFIX" => "left-menu-inc",
							"AREA_FILE_RECURSIVE" => "Y",
							"EDIT_TEMPLATE" => "",
						),
							false
						);
					}
					else
					{
						echo '<div class="leftmenu"></div>';
					}
					?>
				</div>
			</div>
		</div>
	<?endif; ?>
	<?
endif;
// ���� �� ������ ����� �����������.
/*$APPLICATION->IncludeComponent(
	"bitrix:main.include",
	"",
	Array(
		"AREA_FILE_SHOW" => "page",
		"AREA_FILE_SUFFIX" => "top",
		"AREA_FILE_RECURSIVE" => "Y",
		"EDIT_TEMPLATE" => "page_top.php",
		"EDIT_MODE" => "html",
	),
	false
);*/
?>
<? if (!IS_SITE_DIR): ?>
	<div class="div-flex">
	<div class="visible-desktop">
		<? if ($showLeftMenu):
			$APPLICATION->IncludeComponent("bitrix:main.include", "left-menu", array(
				"AREA_FILE_SHOW" => "sect",
				"AREA_FILE_SUFFIX" => "left-menu-inc",
				"AREA_FILE_RECURSIVE" => "Y",
				"EDIT_TEMPLATE" => "",
			),
				false
			);
		else:
			if (!ISPERSONAL)
			{
				$APPLICATION->IncludeComponent("bitrix:main.include", "news-index", array(
					"AREA_FILE_SHOW" => "sect",
					"AREA_FILE_SUFFIX" => "right",
					"AREA_FILE_RECURSIVE" => "Y",
					"EDIT_TEMPLATE" => "top_side",
				),
					false
				);
			}
		endif; ?>
	</div>
	<div class="subpage">
	<?php
	if ($showTitleMiddle)
	{
		$APPLICATION->IncludeFile(
			SITE_TEMPLATE_PATH . "/include_areas/title-page.php",
			Array("class" => "page-title" . ($showMenuMobile ? " visible-desktop" : "")),
			Array("MODE" => "html")
		);
	}
endif; ?>