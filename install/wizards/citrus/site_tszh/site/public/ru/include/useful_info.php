<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<h4><a href="#SITE_DIR#useful-info/">Полезная информация</a></h4>
<ul class="small-links">
    <li><a href="#SITE_DIR#useful-info/reglament_zaseleniya.php">Регламент заселения</a></li>
    <li><a href="#SITE_DIR#useful-info/pravila_prozhivaniya_v_mnogokvartirnyh_domah.php">Правила проживания в многоквартирных домах</a></li>
    <li><a href="#SITE_DIR#useful-info/instruktsiya_po_pozharnoy_bezopasnosti.php">Инструкция по пожарной безопасности</a></li>
</ul>
