<?php
$t = ini_get_all('mbstring');
if($t["mbstring.func_overload"]["global_value"] != 0 )
{
	header('Content-type: text/html; charset=windows-1251');
}
use Bitrix\Main\Application;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

CModule::IncludeModule('citrus.tszh');
$user = \CUser::GetByID($_SESSION['SESS_AUTH']["USER_ID"])->Fetch();
$acc = \CTszhAccount::GetByUserID($user['ID']);


// Тут, товарисчи, вы можете делать что хотите. Данные из формы приходят методом POST, 
// т.е. имеют вид $_POST["varible"]

switch ($_REQUEST["action"])
{
	case "tostep2":
		$arResult['STEP2'] = $_POST['name']; ?>
		<?

		if ($arResult['STEP2'] == 'bycode')
		{
			?>
			<div>


				<input type="hidden" id="check" name="check" value="1" class="code">
				<div class="wrap"><span class="starrequired">*</span>
					<span class="title">Кодовое слово,<br class="small"> выданное вашей УК:</span>
					<input type="text" id="codr" name="regcode" value="" class="code"></div>

				<?
				unset($_REQUEST); ?>


			</div>
		<? }
		if ($arResult['STEP2'] == 'bypass')
		{
			?>

			<table style="width: 100%">
				<tr>
					<td class="span">
						<input type="hidden" id="check" name="check" value="2" class="code">
						<span class="title">Введите логин:</span></td>
					<td><input id="codr" type="text" name="regcode" value="" class="code"></td>
				</tr>
				<tr>
					<td class="span"><span class="title">Введите пароль:</span></td>
					<td><input id="pass" type="password" name="password" value="" class="code"></td>
				</tr>
			</table><br><br>

			<?
		}


		break;
	case "tostep3":
		$arResult['STEP3'] = $_REQUEST['regcode'];
		$arResult['STEP3_pass'] = $_REQUEST['password'];
		$arResult['STEP3_choise'] = $_REQUEST['check'];
		CModule::IncludeModule('citrus.tszh');
		$user_donor = CUser::GetByLogin($arResult['STEP3'])->Fetch();
		$row = $user_donor['PASSWORD'];
		$salt = substr($row, 0, strlen($row) - 32);
		$db_password = substr($row, -32); // тут переделывает в пароль с бд b_user
		$pass = $arResult['STEP3_pass'];
		$user_password = md5($salt . $pass);
		$arraccs = CTszhAccount::GetList(array(), array('UF_REGCODE' => $arResult['STEP3'],))->Fetch();


		if ($arResult['STEP3_choise'] == 1)
		{
			$APPLICATION->IncludeComponent(
				"citrus:tszh.account.confirm",
				"",
				Array(
					"REGCODE_PROPERTY" => "UF_REGCODE",
				)
			);

		}
		if ($arResult['STEP3_choise'] == 2)
		{
			$APPLICATION->IncludeComponent(
				"citrus:tszh.account.confirm_code",
				"",
				Array(
					"REGCODE_PROPERTY" => "UF_REGCODE",
				)
			);
		}

		break;


}
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");

?>
