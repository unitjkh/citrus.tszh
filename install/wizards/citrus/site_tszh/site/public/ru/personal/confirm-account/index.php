<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Привязка к лицевому счету");

?><? CModule::IncludeModule('citrus.tszh');
$user = \CUser::GetByID($_SESSION['SESS_AUTH']["USER_ID"])->Fetch();
$acc = \CTszhAccount::GetByUserID($user['ID']);

?>


	<link rel="stylesheet" type="text/css" href="/personal/confirm-account/css/animation.css">
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.2.6/jquery.min.js"></script>
	<script type="text/javascript" src="/personal/confirm-account/js/jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="/personal/confirm-account/js/ui.core.js"></script>
	<script type="text/javascript" src="/personal/confirm-account/js/ui.progressbar.js"></script>
	<script type="text/javascript" src="/personal/confirm-account/js/formToWizard.js"></script>




	<script type="text/javascript">
        $(document).ready(function () {
            $("#SignupForm").formToWizard({submitButton: 'SaveAccount'});
            $("#info").fadeIn(400);



        });
	</script>
<? if(isset($_REQUEST['action'])== 'confirm'){?>
	<script type="text/javascript">
        $(document).ready(function () {
            $("#step0").css('display', 'none');
            $("#step3").css('display', 'block');
            $("#stepDesc0").attr('class', '');
            $("#stepDesc3").attr('class', 'current');
            $("#progress").attr('aria-valuenow', '100');
            $(".ui-progressbar-value").css('width', '100%');
            $("#amount").text('100%');
        });
	</script>
<?}?>
<? if(isset($_REQUEST['word'])){?>
	<script type="text/javascript">
        $(document).ready(function () {
            $("#step0").css('display', 'none');
            $("#step2").css('display', 'block');
            $("#error").css('display', 'block');
            $("#stepDesc0").attr('class', '');
            $("#stepDesc2").attr('class', 'current');
            $("#progress").attr('aria-valuenow', '75');
            $(".ui-progressbar-value").css('width', '75%');
            $("#amount").text('75%');
        });
	</script>
<?}?>

	<div id="main">
		<div class="cssload-thecube" id="loading">
			<div class="cssload-cube cssload-c1"></div>
			<div class="cssload-cube cssload-c2"></div>
			<div class="cssload-cube cssload-c4"></div>
			<div class="cssload-cube cssload-c3"></div>
		</div>
		<div id="SignupForm">
			<div id="progress"></div>
			<!--<label id="amount" style="float: right">25%</label>-->

			<fieldset>
				<div style="display:none">
					<step >Шаг</step>
					<prev >Назад</prev>
					<next >Далее</next>
				</div>
				<legend>Выберите способ привязки</legend>
				<div style="text-align: left">
					<input type="radio" name="radio" id="1" class="radio" required><label for="1"> <span
								class="title">По лицевому счету</span></label> <br><br><span class="subscribe">
       Для привязки лицевого счета необходимо будет ввести кодовое слово выданное управляющей компанией.
    </span><br><br>
					<?// if ($acc != false) { ?>
					<input type="radio" name="radio" id="2" class="radio" required><label for="2"><span
								class="title">По пользователю </span></label><br><br><span class="subscribe">
        Для привязки лицевого счета необходимо будет ввести логин и пароль от лицевого счета.
    </span><br><br><?// } ?>
					<div style="text-align: left" id="firstblock"></div>
				</div>
				<script>
                    var select = document.getElementById('1');
                    select.onclick = function () {
                        var link_one = 'code.php?step=2';

                        var code = document.getElementById('type');
                        if (this.checked) type.name = 'bycode'; else type.name = 'bypass';
                    };
                    var select2 = document.getElementById('2');
                    select2.onclick = function () {

                        var link_two = 'account.php?step=2';
                        var code = document.getElementById('code');
                        if (this.checked) type.name = 'bypass';
                    };
                    $(document).ready(function () {


                        $("input[name='radio']").click(function(){
                            $("#step0Next").css('display', 'block').css('float','right');
                            $("#firstblock").css('display', 'none');
                        });

                        if($("#type").attr('name', '')){
                            $("#firstblock").html("<div style='color:red' >Выберите один из вариантов</div>");
                            $("#step0Next").css('display', 'none');
                        }

                    });
				</script>
				<input id="type" type="hidden" name="">
			</fieldset>
			<fieldset>
				<div style="display:none">
					<step >Шаг</step>
					<prev >Назад</prev>
					<next >Далее</next>
				</div>
				<legend>Ввод данных</legend>
				<div id="result2"></div>
			</fieldset>
			<fieldset>
				<div style="display:none">
					<step >Шаг</step>
					<prev >Назад</prev>
					<next >Далее</next>
				</div>
				<legend>Проверка данных</legend>
				<div id="result3">

					<div id="error" style="display: none">
						<?
						if ($_REQUEST['check'] == 1 || $_REQUEST['word'] == 1)
						{
							$APPLICATION->IncludeComponent(
								"citrus:tszh.account.confirm",
								"",
								Array(
									"REGCODE_PROPERTY" => "UF_REGCODE",
								)
							);
						}
						if ($_REQUEST['check'] == 2 || $_REQUEST['word'] == 2)
						{
							$APPLICATION->IncludeComponent(
								"citrus:tszh.account.confirm_code",
								"",
								Array(
									"REGCODE_PROPERTY" => "UF_REGCODE",
								)
							);
						} ?>
					</div>
				</div>
			</fieldset>
			<fieldset>
				<div style="display:none">
					<step >Шаг</step>
					<prev >Назад</prev>
					<next >Далее</next>
				</div>
				<legend>Завершение привязки</legend>
				<? if(isset($_REQUEST['action'])== 'confirm'){?>
					<script>
                        $(document).ready(function (){

                            $('html, body').animate({
                                scrollTop: $(".content__padding").offset().top
                            }, 5);

                        });
					</script>
					<div id="result4">
						<p style="text-align:center" name="center">Лицевой счет успешно подтвержден</p>
						<p><a href="/personal/" class="form-variable__button form-variable__saved link-theme-default" style="float:right; color: #fff !important; text-align:center;">
								Вернуться в личный кабинет
							</a></p></div>
				<?}?>
			</fieldset>
		</div>
	</div>


<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>