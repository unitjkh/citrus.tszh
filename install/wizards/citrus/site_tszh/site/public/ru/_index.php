<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Официальный сайт управляющей компании");
$APPLICATION->SetTitle("#SITE_NAME#");
?>
<h2>Войти в <em>личный кабинет</em></h2>
<?if ($USER->IsAuthorized()):?>
<p>В <a href="#SITE_DIR#personal/">личном кабинете</a> вы можете:</p>
<?else:?>
<p>После <a href="#SITE_DIR#auth/">авторизации</a> на сайте с помощью личного кабинета вы сможете:</p>
<?endif;?>
<?$APPLICATION->IncludeComponent("bitrix:menu", "tbl_list", array(
	"ROOT_MENU_TYPE" => "section",
	"MENU_CACHE_TYPE" => "A",
	"MENU_CACHE_TIME" => "3600",
	"MENU_CACHE_USE_GROUPS" => "Y",
	"MENU_CACHE_GET_VARS" => array(
	),
	"MAX_LEVEL" => "1",
	"CHILD_MENU_TYPE" => "",
	"USE_EXT" => "Y"
	),
	false
);?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>