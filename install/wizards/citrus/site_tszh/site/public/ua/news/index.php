<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("news", "N");
$APPLICATION->SetPageProperty("contact", "Y");
$APPLICATION->SetPageProperty("SHOW_TOP_RIGHT", "Y");
$APPLICATION->SetPageProperty ("title", "������ ������� ��������");
$APPLICATION->SetTitle("������");
?><?$APPLICATION->IncludeComponent("bitrix:news", "news", Array(
	"DISPLAY_DATE" => "Y",	// �������� ���� ��������
	"DISPLAY_PICTURE" => "Y",	// �������� ����������� ��� ������
	"DISPLAY_PREVIEW_TEXT" => "Y",	// �������� ����� ������
	"DISPLAY_AS_RATING" => "rating",	// � �������� �������� ����������
	"TAGS_CLOUD_ELEMENTS" => "150",	// ���������� �����
	"PERIOD_NEW_TAGS" => "",	// ������,  � ������� �������� ������� ��� ����� (����)
	"FONT_MAX" => "50",	// ������������ ������ ������ (px)
	"FONT_MIN" => "10",	// �����������  ������ ������ (px)
	"COLOR_NEW" => "3E74E6",	// ���� ����� �������� ���� (������: "C0C0C0")
	"COLOR_OLD" => "C0C0C0",	// ���� ����� ������� ���� (������: "FEFEFE")
	"TAGS_CLOUD_WIDTH" => "100%",	// ������ ������ ����� (������: "100%" ��� "100px", "100pt", "100in")
	"SEF_MODE" => "Y",	// �������� ��������� ���
	"AJAX_MODE" => "N",	// �������� ����� AJAX
	"IBLOCK_TYPE" => "news",	// ��� ����-�����
	"IBLOCK_ID" => "#NEWS_IBLOCK_ID#",	// ����-����
	"NEWS_COUNT" => "20",	// ���������� �������� �� ��������
	"USE_SEARCH" => "N",	// ��������� �����
	"USE_RSS" => "N",	// ��������� RSS
	"USE_RATING" => "N",	// ��������� �����������
	"USE_CATEGORIES" => "N",	// �������� ��������� �� ����
	"USE_FILTER" => "N",	// ���������� ������
	"SORT_BY1" => "ACTIVE_FROM",	// ���� ��� ������ ���������� ��������
	"SORT_ORDER1" => "DESC",	// ����������� ��� ������ ���������� ��������
	"SORT_BY2" => "SORT",	// ���� ��� ������ ���������� ��������
	"SORT_ORDER2" => "ASC",	// ����������� ��� ������ ���������� ��������
	"CHECK_DATES" => "Y",	// ���������� ������ �������� �� ������ ������ ��������
	"PREVIEW_TRUNCATE_LEN" => "",	// ������������ ����� ������ ��� ������ (������ ��� ���� �����)
	"LIST_ACTIVE_DATE_FORMAT" => "d.m.Y",	// ������ ������ ����
	"LIST_FIELD_CODE" => "",	// ����
	"LIST_PROPERTY_CODE" => "",	// ��������
	"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// �������� ������, ���� ��� ���������� ��������
	"DISPLAY_NAME" => "Y",	// �������� �������� ��������
	"META_KEYWORDS" => "KEYWORDS",	// ���������� �������� ����� �������� �� ��������
	"META_DESCRIPTION" => "DESCRIPTION",	// ���������� �������� �������� �� ��������
	"DETAIL_ACTIVE_DATE_FORMAT" => "d.m.Y",	// ������ ������ ����
	"DETAIL_FIELD_CODE" => "",	// ����
	"DETAIL_PROPERTY_CODE" => "",	// ��������
	"DETAIL_DISPLAY_TOP_PAGER" => "N",	// �������� ��� �������
	"DETAIL_DISPLAY_BOTTOM_PAGER" => "Y",	// �������� ��� �������
	"DETAIL_PAGER_TITLE" => "��������",	// �������� ���������
	"DETAIL_PAGER_TEMPLATE" => "",	// �������� �������
	"DISPLAY_PANEL" => "Y",	// ��������� � �����. ������ ������ ��� ������� ����������
	"SET_TITLE" => "Y",	// ������������� ��������� ��������
	"SET_STATUS_404" => "Y",	// ������������� ������ 404, ���� �� ������� ������� ��� ������
	"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// �������� �������� � ������� ���������
	"ADD_SECTIONS_CHAIN" => "Y",	// �������� ������ � ������� ���������
	"USE_PERMISSIONS" => "N",	// ������������ �������������� ����������� �������
	"CACHE_TYPE" => "A",	// ��� �����������
	"CACHE_TIME" => "360000",	// ����� ����������� (���.)
	"CACHE_FILTER" => "N",	// ���������� ��� ������������� �������
	"DISPLAY_TOP_PAGER" => "N",	// �������� ��� �������
	"DISPLAY_BOTTOM_PAGER" => "Y",	// �������� ��� �������
	"PAGER_TITLE" => "�������",	// �������� ���������
	"PAGER_SHOW_ALWAYS" => "Y",	// �������� ������
	"PAGER_TEMPLATE" => "",	// �������� �������
	"PAGER_DESC_NUMBERING" => "N",	// ������������ �������� ���������
	"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// ����� ����������� ������� ��� �������� ���������
	"SEF_FOLDER" => "#SITE_DIR#news/",	// ������� ��� (������������ ����� �����)
	"SEF_URL_TEMPLATES" => array(
		"detail" => "#ELEMENT_ID#.html",
		"search" => "search/",
		"rss" => "rss/",
		"rss_section" => "#SECTION_ID#/rss/",
	),
	"AJAX_OPTION_SHADOW" => "Y",	// �������� ���������
	"AJAX_OPTION_JUMP" => "N",	// �������� ��������� � ������ ����������
	"AJAX_OPTION_STYLE" => "Y",	// �������� ��������� ������
	"AJAX_OPTION_HISTORY" => "N",	// �������� �������� ��������� ��������
	"VARIABLE_ALIASES" => array(
		"detail" => "",
		"search" => "",
		"rss" => "",
		"rss_section" => "",
	)
	),
	false
);?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>