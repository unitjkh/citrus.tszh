<?
$MESS["NEWS_TYPE_NAME"] = "Новини";
$MESS["NEWS_ELEMENT_NAME"] = "Новини";
$MESS["NEWS_SECTION_NAME"] = "Розділи";
$MESS["SERVICES_TYPE_NAME"] = "Сервіси";
$MESS["SERVICES_ELEMENT_NAME"] = "Елементи";
$MESS["SERVICES_SECTION_NAME"] = "Розділи";
$MESS["ARTICLES_TYPE_NAME"] = "Статті";
$MESS["ARTICLES_ELEMENT_NAME"] = "Елементи";
$MESS["ARTICLES_SECTION_NAME"] = "Розділи";
$MESS["GALLERY_TYPE_NAME"] = "Фотогалерея";
$MESS["GALLERY_ELEMENT_NAME"] = "Фотографії";
$MESS["GALLERY_SECTION_NAME"] = "Альбом";
?>