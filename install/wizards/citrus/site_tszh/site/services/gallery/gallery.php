<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

if (!tszhCheckMinEdition('standard'))
	return;

if(!CModule::IncludeModule("iblock"))
	return;

if(COption::GetOptionString("citrus.tszh", "wizard_installed", "N", WIZARD_SITE_ID) == "Y")
	return;

if (!defined("WIZARD_SITE_DIR"))
	return;

$iblockXMLFile = WIZARD_SERVICE_RELATIVE_PATH."/xml/".LANGUAGE_ID."/gallery-photo.xml";
$iblockCode = "photo_".WIZARD_SITE_ID;
$iblockType = "gallery";

$rsIBlock = CIBlock::GetList(array(), array("XML_ID" => $iblockCode, "TYPE" => $iblockType, "SITE_ID" => WIZARD_SITE_ID));
$iblockID = false;
if ($arIBlock = $rsIBlock->Fetch())
{
	$iblockID = (int)$arIBlock["ID"];
	if (WIZARD_REINSTALL_DATA)
	{
		CIBlock::Delete($arIBlock["ID"]);
		$iblockID = false;
	}
}

if($iblockID == false)
{
	$iblockID = WizardServices::ImportIBlockFromXML(
		$iblockXMLFile,
		"photo",
		$iblockType,
		WIZARD_SITE_ID,
		$permissions = Array(
			"1" => "X",
			"2" => "R"
		)
	);

	if ($iblockID < 1)
		return;

	//IBlock fields
	$iblock = new CIBlock;

	$arFields = Array(
		"ACTIVE" => "Y",
		"FIELDS" => array ( 'IBLOCK_SECTION' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => '', ), 'ACTIVE' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => 'Y', ), 'ACTIVE_FROM' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'ACTIVE_TO' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'SORT' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'NAME' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => '', ), 'PREVIEW_PICTURE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => array ( 'FROM_DETAIL' => 'Y', 'SCALE' => 'Y', 'WIDTH' => 120, 'HEIGHT' => 120, 'IGNORE_ERRORS' => 'N', 'METHOD' => 'resample', 'COMPRESSION' => 95, 'DELETE_WITH_DETAIL' => 'Y', 'UPDATE_WITH_DETAIL' => 'Y', ), ), 'PREVIEW_TEXT_TYPE' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => 'text', ), 'PREVIEW_TEXT' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'DETAIL_PICTURE' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => array ( 'SCALE' => 'Y', 'WIDTH' => 500, 'HEIGHT' => 500, 'IGNORE_ERRORS' => 'N', 'METHOD' => 'resample', 'COMPRESSION' => 95, ), ), 'DETAIL_TEXT_TYPE' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => 'text', ), 'DETAIL_TEXT' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'XML_ID' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'CODE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => array ( 'UNIQUE' => 'N', 'TRANSLITERATION' => 'N', 'TRANS_LEN' => 100, 'TRANS_CASE' => 'L', 'TRANS_SPACE' => '_', 'TRANS_OTHER' => '_', 'TRANS_EAT' => 'Y', 'USE_GOOGLE' => 'N', ), ), 'TAGS' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'SECTION_NAME' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => '', ), 'SECTION_PICTURE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => array ( 'FROM_DETAIL' => 'N', 'SCALE' => 'N', 'WIDTH' => '', 'HEIGHT' => '', 'IGNORE_ERRORS' => 'N', 'METHOD' => 'resample', 'COMPRESSION' => 95, 'DELETE_WITH_DETAIL' => 'N', 'UPDATE_WITH_DETAIL' => 'N', ), ), 'SECTION_DESCRIPTION_TYPE' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => 'text', ), 'SECTION_DESCRIPTION' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'SECTION_DETAIL_PICTURE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => array ( 'SCALE' => 'N', 'WIDTH' => '', 'HEIGHT' => '', 'IGNORE_ERRORS' => 'N', 'METHOD' => 'resample', 'COMPRESSION' => 95, ), ), 'SECTION_XML_ID' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'SECTION_CODE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => array ( 'UNIQUE' => 'N', 'TRANSLITERATION' => 'N', 'TRANS_LEN' => 100, 'TRANS_CASE' => 'L', 'TRANS_SPACE' => '_', 'TRANS_OTHER' => '_', 'TRANS_EAT' => 'Y', 'USE_GOOGLE' => 'N', ), ), ),
		"CODE" => "photo",
		"XML_ID" => $iblockCode,
		//"NAME" => "[".WIZARD_SITE_ID."] ".$iblock->GetArrayByID($iblockID, "NAME")
	);

	$iblock->Update($iblockID, $arFields);
}

$dbSite = CSite::GetByID(WIZARD_SITE_ID);
if($arSite = $dbSite -> Fetch())
	$lang = $arSite["LANGUAGE_ID"];
if(strlen($lang) <= 0)
	$lang = "ru";

WizardServices::IncludeServiceLang("gallery.php", $lang);


$arFormFields = array (
  'tabs' => 'edit1--#--' . GetMessage('WZD_OPTION_PHOTO_1') . '--,--ACTIVE--#--' . GetMessage('WZD_OPTION_PHOTO_2') . '--,--NAME--#--' . GetMessage('WZD_OPTION_PHOTO_3') . '--,--DETAIL_PICTURE--#--' . GetMessage('WZD_OPTION_PHOTO_4') . '--,--SECTIONS--#--' . GetMessage('WZD_OPTION_PHOTO_5') . '--,--SORT--#--' . GetMessage('WZD_OPTION_PHOTO_6') . '--;--',
);

CUserOptions::SetOption("form", "form_element_".$iblockID, $arFormFields);
CUserOptions::SetOption("list", "tbl_iblock_list_".md5($iblockType.".".$iblockID), array (
  'columns' => 'NAME,ACTIVE,SORT,TIMESTAMP_X,ID,PREVIEW_PICTURE',
  'by' => 'sort',
  'order' => 'asc',
  'page_size' => '20',
));

$sourcePath = str_replace("//", "/", WIZARD_SERVICE_ABSOLUTE_PATH."/public/".LANGUAGE_ID."/gallery2.0/");
$galleryComponent = "photogallery";
$destPath = WIZARD_SITE_PATH;

// ���� ��������������� ���������� ������, �� ����� ���������� ����� � ���� � ��� �� ��������� ����
if (WIZARD_TEMPLATE_ID == 'citrus_tszh_adaptive')
{
	$destPath .= 'epassports/';
}

CopyDirFiles($sourcePath, $destPath, true, true);

$arContacts = Array(
	"SITE_DIR" => WIZARD_SITE_DIR,
	"SITE_NAME" => htmlspecialcharsbx($wizard->GetVar("siteName")),
	"SITE_EMAIL" => htmlspecialcharsbx($wizard->GetVar("siteEMail")),
	"SITE_PHONE" => htmlspecialcharsbx($wizard->GetVar("siteTelephone")),
	"SITE_ADDRESS" => htmlspecialcharsbx($wizard->GetVar("siteAddress")),
);
// ���� ��������������� ���������� ������
if (WIZARD_TEMPLATE_ID == 'citrus_tszh_adaptive')
{
	$arContacts["SITE_DIR"] .= 'epassports/';
	WizardServices::ReplaceMacrosRecursive(WIZARD_SITE_PATH."epassports/photogallery/", $arContacts);
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."epassports/photogallery/index.php", array("GALLERY_IBLOCK_ID" => $iblockID, "SITE_DIR" => WIZARD_SITE_DIR));
}
// ���� ������ ������
else
{
	WizardServices::ReplaceMacrosRecursive(WIZARD_SITE_PATH."photogallery/", $arContacts);
	CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."photogallery/index.php", array("GALLERY_IBLOCK_ID" => $iblockID, "SITE_DIR" => WIZARD_SITE_DIR));

}

// ���� ��������������� ���������� ������
if (WIZARD_TEMPLATE_ID == 'citrus_tszh_adaptive')
{
	$arMenuItem = Array(
		GetMessage("WZD_GALLERY_MENU_ITEM"),
		WIZARD_SITE_DIR . "epassports/photogallery/",
		Array(),
		Array(),
		""
	);
	WizardServices::AddMenuItem(WIZARD_SITE_DIR . '/epassports/.show_add.menu.php', $arMenuItem,  WIZARD_SITE_ID, -1);
	WizardServices::AddMenuItem(WIZARD_SITE_DIR . '/epassports/.section.menu.php', $arMenuItem,  WIZARD_SITE_ID, -1);
}
// ���� ������ ������
else
{
	$arMenuItem = Array(
		GetMessage("WZD_GALLERY_MENU_ITEM"),
		WIZARD_SITE_DIR . "photogallery/",
		Array(),
		Array(),
		""
	);
	WizardServices::AddMenuItem(WIZARD_SITE_DIR . '/photogallery/.show_add.menu.php', $arMenuItem,  WIZARD_SITE_ID, -1);
	WizardServices::AddMenuItem(WIZARD_SITE_DIR . '/photogallery/.section.menu.php', $arMenuItem,  WIZARD_SITE_ID, -1);
}

if (WIZARD_TEMPLATE_ID !== 'citrus_tszh_adaptive')
{
	$arMenuItem = Array(
		GetMessage("WZD_COMMUNICAITON_MENU_ITEM"),
		WIZARD_SITE_DIR . "photogallery/",
		Array(),
		Array(),
		""
	);
	WizardServices::AddMenuItem(WIZARD_SITE_DIR . '/.top.menu.php', $arMenuItem,  WIZARD_SITE_ID, 3);
	WizardServices::AddMenuItem(WIZARD_SITE_DIR . '/.bottom.menu.php', $arMenuItem,  WIZARD_SITE_ID, 2);
}

$arUrlRewrite = array();
if (file_exists(WIZARD_SITE_ROOT_PATH."/urlrewrite.php"))
{
	include(WIZARD_SITE_ROOT_PATH."/urlrewrite.php");
}
// ���� ��������������� ���������� ������
if (WIZARD_TEMPLATE_ID == 'citrus_tszh_adaptive')
{
	$arNewUrlRewrite = array(
		array(
			"CONDITION"	=>	"#^".WIZARD_SITE_DIR."epassports/photogallery/#",
			"RULE"	=>	"",
			"ID"	=>	$galleryComponent,
			"PATH"	=>	WIZARD_SITE_DIR."epassports/photogallery/index.php",
		),
	);
}
// ���� ������ ������
else
{
	$arNewUrlRewrite = array(
		array(
			"CONDITION"	=>	"#^".WIZARD_SITE_DIR."photogallery/#",
			"RULE"	=>	"",
			"ID"	=>	$galleryComponent,
			"PATH"	=>	WIZARD_SITE_DIR."photogallery/index.php",
		),
	);
}
foreach ($arNewUrlRewrite as $arUrl)
{
	if (!in_array($arUrl, $arUrlRewrite))
	{
		CUrlRewriter::Add($arUrl);
	}
}

$arContacts = Array(
	"SITE_DIR" => WIZARD_SITE_DIR,
	"SITE_NAME" => htmlspecialcharsbx($wizard->GetVar("siteName")),
	"SITE_EMAIL" => htmlspecialcharsbx($wizard->GetVar("siteEMail")),
	"SITE_PHONE" => htmlspecialcharsbx($wizard->GetVar("siteTelephone")),
	"SITE_ADDRESS" => htmlspecialcharsbx($wizard->GetVar("siteAddress")),
);
$wizard =& $this->GetWizard();
// ���� ��������������� ���������� ������
if (WIZARD_TEMPLATE_ID == 'citrus_tszh_adaptive')
{
	WizardServices::ReplaceMacrosRecursive(WIZARD_SITE_PATH . '/epassports/photogallery/', $arContacts);
}
else
{
	WizardServices::ReplaceMacrosRecursive(WIZARD_SITE_PATH . '/photogallery/', $arContacts);
}

\Citrus\Tszh\Wizard\Seo::setIblockSettings($iblockID, false, true);