<?

$arServices = Array(
	"main" => Array(
		"NAME" => GetMessage("SERVICE_MAIN_SETTINGS"),
		"STAGES" => Array(
			"files.php", // Copy bitrix files
			"template.php", // Install template
			"theme.php", // Install theme
			"socialservices.php", // social services
			"seo.php", // prepare SEO settings
		),
	),
	"iblock" => Array(
		"NAME" => GetMessage("SERVICE_IBLOCK_DEMO_DATA"),
		"STAGES" => Array(
			"types.php", // iBlock types
			"news.php",
			"announcements.php",
			"slides.php",
			"questions.php",
			"documents.php",
		),
	),
	"gallery" => Array(
		"NAME" => GetMessage("SERVICE_GALLERY_DEMO_DATA"),
		"MODULE_ID" => "photogallery",
		"STAGES" => Array(
			"types.php", // iBlock types
			"gallery.php",
			"video.php",
		),
	),
	"tszh" => Array(
		"NAME" => GetMessage("SERVICE_TSZH_DEMO_DATA"),
		"MODULE_ID" => "citrus.tszh",
		"STAGES" => Array(
			"step.php",
			/*
			"import_drop_tables.php", 		// import demo.xml step 0: drop temporary tables
			"import_create_tables.php", 	// import demo.xml step 1: create temporary tables
			"import_read_xml.php", 			// import demo.xml step 2: read xml to database
			"import_index_tables.php",		// import demo.xml step 3: index temporary tables
			"import_period_services.php",	// import demo.xml step 4: process period & import services
			"import_accounts.php",			// import demo.xml step 5: import accounts
			"import_cleanup.php",			// import demo.xml step 6: cleanup import
			*/
		),
	),
	"forum" => Array(
		"NAME" => GetMessage("SERVICE_FORUM_DEMO_DATA"),
		"MODULE_ID" => "forum",
		"STAGES" => Array(
			"index.php",
		),
	),
	"blog" => Array(
		"NAME" => GetMessage("SERVICE_BLOG_DEMO_DATA"),
		"MODULE_ID" => "blog",
		"STAGES" => Array(
			"index.php",
		),
	),
	"tszhvote" => Array(
		"NAME" => GetMessage("SERVICE_VOTE_DEMO_DATA"),
		"MODULE_ID" => "vdgb.tszhvote",
		"STAGES" => Array(
			"index.php",
		),
	),
	"tszhpayment" => Array(
		"NAME" => GetMessage("SERVICE_TSZHPAYMENT_DEMO_DATA"),
		"MODULE_ID" => "citrus.tszhpayment",
		"STAGES" => Array(
			"index.php",
		),
	),
	"tszhtickets" => Array(
		"NAME" => GetMessage("SERVICE_TSZHTICKETS_DEMO_DATA"),
		"MODULE_ID" => "citrus.tszhtickets",
		"STAGES" => Array(
			"index.php",
		),
	),
	"tszhsubsribe" => Array(
		"NAME" => GetMessage("SERVICE_TSZH_SUBSCRIBE"),
		"MODULE_ID" => "citrus.tszh",
		"STAGES" => Array(
			"index.php",
		),
	),
	"tszhdebtors" => Array(
		"NAME" => GetMessage("SERVICE_TSZH_DEBTORS"),
		"MODULE_ID" => "citrus.tszh",
		"STAGES" => Array(
			"index.php",
		),
	),
	"tszhoperator" => Array(
		"NAME" => GetMessage("SERVICE_TSZH_OPERATOR"),
		"MODULE_ID" => "vdgb.operator",
		"STAGES" => Array(
			"index.php",
		),
	),
	"tszhepasport" => Array(
        "NAME" => GetMessage("SERVICE_TSZH_EPASPORT"),
        "MODULE_ID" => "vdgb.tszhepasport",
        "STAGES" => Array(
            "index.php",
        ),
    ),
    "tszhhouses" => Array(
        "NAME" => GetMessage("SERVICE_TSZH_EPASPORT"),
        "MODULE_ID" => "vdgb.tszhhouses",
        "STAGES" => Array(
            "index.php",
        ),
    ),
    "tszhchart" => Array(
        "NAME" => GetMessage("SERVICE_TSZH_CHART"),
        "MODULE_ID" => "otr.tszhchart",
        "STAGES" => Array(
            "index.php",
        ),
    ),
    "final" => Array(
		"NAME" => GetMessage("SERVICE_FINISH"),
		"MODULE_ID" => "main",
		"STAGES" => Array(
			"step.php",
			"seo.php", // save SEO settings
		),
	),
);
