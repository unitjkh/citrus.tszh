<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("show_title", "N");
$APPLICATION->SetTitle("Графики");
?>

<?$APPLICATION->IncludeComponent(
	"otr:tszh.chart", 
	".default", 
	array("CHART_TYPE" => "CHART_DEBT"),
	false
);?>
<?$APPLICATION->IncludeComponent(
	"otr:tszh.chart", 
	".default", 
	array("CHART_TYPE" => "CHART_SUMM"),
	false
);?>
<?$APPLICATION->IncludeComponent(
	"otr:tszh.chart",
	"",
	Array("CHART_TYPE" => "CHART_SUMM_SUMMPAYED"),
	false
);?>
<?$APPLICATION->IncludeComponent(
	"otr:tszh.chart", 
	".default", 
	array("CHART_TYPE" => "CHART_RATE"),
	false
);?>
<?$APPLICATION->IncludeComponent(
	"otr:tszh.chart", 
	".default", 
	array("CHART_TYPE" => "CHART_PIE"),
	false
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>