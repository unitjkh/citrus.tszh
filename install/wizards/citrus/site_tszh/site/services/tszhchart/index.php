<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

if (!tszhCheckMinEdition('smallbusiness') && !CModule::includeModule('otr.tszhchartinst'))
{
    return;
}

if (CModule::IncludeModule('otr.tszhchartinst'))
{
    if (!CTszhChartInstFunctionalityController::CheckEdition()) return;
}

$wizard =& $this->GetWizard();

// copy files

$sourcePath = str_replace("//", "/", WIZARD_SERVICE_ABSOLUTE_PATH . "/public/" . LANGUAGE_ID . "/");
$destPath = WIZARD_SITE_PATH;
CopyDirFiles($sourcePath, $destPath, true, true);

//Add menu items
    WizardServices::AddMenuItem(WIZARD_SITE_DIR . "/personal/.section.menu.php", Array(
        GetMessage("OTR_TSZH_CHART_INSTALL_MENU_ITEM_VALUE"),
        WIZARD_SITE_DIR . "personal/chart/",
        Array(),
        Array("class"=>"chart_utilities leftmenu__img-chart"),
        ""
    ),WIZARD_SITE_ID);

    WizardServices::AddMenuItem(WIZARD_SITE_DIR . "/personal/.show_add.menu.php", Array(
        GetMessage("OTR_TSZH_CHART_INSTALL_MENU_ITEM_VALUE"),
        WIZARD_SITE_DIR . "personal/chart/",
        Array(),
        Array("class"=>"chart_utilities"),
        "",
    ),WIZARD_SITE_ID);

WizardServices::SetFilePermission(WIZARD_SITE_DIR . "/personal/chart/", Array("*" => "R"));


?>