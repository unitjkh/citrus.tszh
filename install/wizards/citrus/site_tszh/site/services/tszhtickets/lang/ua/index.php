<?
$MESS["F_TICKETS_MENU"] = "Мої заявки";
$MESS["F_TICKETS_HOME"] = "Подати заявку до Аварійно-диспетчерської служби";
$MESS["GROUP_TSZH_MEMBERS"] = "Мешканці";
$MESS["GROUP_TSZH_SUPPORT_ADMINISTRATORS"] = "Диспетчери «Аварійно-диспетчерської служби» ";
$MESS["GROUP_TSZH_SUPPORT_CONTRACTORS"] = "Підрядні організації «Аварійно-диспетчерської служби» ";
$MESS["SUP_DEF_LOW"] = "Низька";
$MESS["SUP_DEF_MIDDLE"] = "Середня";
$MESS["SUP_DEF_HIGH"] = "Висока";
$MESS["SUP_DEF_REQUEST_ACCEPTED"] = "Нова заявка";
$MESS["SUP_DEF_PROBLEM_SOLVING_IN_PROGRESS"] = "Прийнята";
$MESS["SUP_DEF_COULD_NOT_BE_SOLVED"] = "Неможливо виконати ";
$MESS["SUP_DEF_SUCCESSFULLY_SOLVED"] = "Закінчено ";
$MESS["SUP_DEF_ANSWER_SUITS_THE_NEEDS"] = "Результат влаштовує ";
$MESS["SUP_DEF_ANSWER_IS_NOT_COMPLETE"] = "Недостатньо повно ";
$MESS["SUP_DEF_ANSWER_DOES_NOT_SUIT"] = "Результат не влаштовує ";
$MESS["SUP_DEF_E_MAIL"] = "E-Mail";
$MESS["SUP_DEF_PHONE"] = "Телефон";
$MESS["SUP_DEF_FORUM"] = "Форум";
$MESS["SUP_DEF_EASY"] = "Легкий";
$MESS["SUP_DEF_MEDIUM"] = "Середній";
$MESS["SUP_DEF_HARD"] = "Високий";
$MESS["SUP_DEF_COMMON"] = "Загальне";
$MESS["CTT_NEW_TICKET"] = "Нова заявка";
$MESS["CTT_NEW_TICKET_D"] = "Заявка надана мешканцем через особистий кабінет на сайті
--
Заявка ще не розглянута";
$MESS["CTT_TICKET_DELEGATED"] = "Передано на виконання ";
$MESS["CTT_TICKET_DELEGATED_D"] = "Диспетчер призначив підрядну організацію
--
Заявка передана в підрядну організацію";
$MESS["CTT_TICKET_ACCEPTED"] = "Пирйнята на виконання";
$MESS["CTT_TICKET_ACCEPTED_D"] = "Диспетчер підрядної організації прийняв заявку
--
Заявка знаходится в стадії виконання";
$MESS["CTT_TICKET_DONE"] = "Виконана";
$MESS["CTT_TICKET_DONE_D"] = "Диспетчер підрядної організації звітував про виконання
--
Заявка виконана";
$MESS["CTT_TICKET_CLOSED"] = "Завершена";
$MESS["CTT_TICKET_CLOSED_D"] = "Диспетчер прийняв довідку про виконання
--
Заявка закрита";
$MESS["SUP_DEF_SLA_NAME"] = "за замовчуванням";
?>