<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

$wizard =& $this->GetWizard();

$demoTszhID = intval($_SESSION[$wizard->solutionName]['demoTszhID']);
if ($demoTszhID <= 0)
	return;

if(!CModule::IncludeModule("iblock"))
	return;

unset($_SESSION[$wizard->solutionName]['demoXmlFileName']);
$absFileName = $_SERVER["DOCUMENT_ROOT"] . $wizard->GetPath() . '/site/services/tszh/xml/demo.xml';
if (file_exists($absFileName) && is_file($absFileName))
{
	$_SESSION[$wizard->solutionName]['demoXmlFileName'] = $absFileName;
}
else
{
	unset($_SESSION[$wizard->solutionName]['demoTszhID']);
	$_SESSION[$wizard->solutionName]["importError"] = getMessage("WIZ_TSZH_IMPORT_ERROR_XML_FILE_NOT_FOUND", array("#FILE#" => $absFileName));
	return;
}

$NS = &$_SESSION["BX_CITRUS_TSZH_IMPORT"];
$NS = array(
	"STEP" => 0,
	"TSZH" => $demoTszhID,
			
	"ONLY_DEBT" => false,
	"UPDATE_MODE" => false,
	"CREATE_USERS" => false,
	"UPDATE_USERS" => false,
	"ACTION" => "A",
);

$_SESSION["TSZH_IMPORT"] = array(
	"SERVICES" => Array(),
);

CIBlockXMLFile::DropTemporaryTables();
$NS["STEP"]++;

?>