<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

$wizard =& $this->GetWizard();

$demoTszhID = intval($_SESSION[$wizard->solutionName]['demoTszhID']);
if ($demoTszhID <= 0)
	return;

if(!CModule::IncludeModule("iblock"))
	return;

$NS = &$_SESSION["BX_CITRUS_TSZH_IMPORT"];

$absFileName = $_SESSION[$wizard->solutionName]['demoXmlFileName'];
if (strlen($absFileName) > 0 && file_exists($absFileName) && is_file($absFileName) && ($fp = fopen($absFileName, "rb")))
{
	$obXMLFile = new CIBlockXMLFile;
	$interval = COption::GetOptionInt('citrus.tszh', '1c_exchange.Interval', 30);
	if ($obXMLFile->ReadXMLToDatabase($fp, $NS, $interval))
	{
		$NS["STEP"]++;
	}
	else
	{
		unset($_SESSION[$wizard->solutionName]['demoTszhID']);
		$_SESSION[$wizard->solutionName]["importError"] = getMessage("WIZ_TSZH_IMPORT_ERROR_READ_XML");
	}
	fclose($fp);
}
else
{
	unset($_SESSION[$wizard->solutionName]['demoTszhID']);
	$_SESSION[$wizard->solutionName]["importError"] = getMessage("WIZ_TSZH_IMPORT_ERROR_XML_FILE_OPEN", array("#FILE#" => $absFileName));
}
?>
