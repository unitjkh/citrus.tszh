<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

$wizard =& $this->GetWizard();

$demoTszhID = intval($_SESSION[$wizard->solutionName]['demoTszhID']);
if ($demoTszhID <= 0)
	return;

if(!CModule::IncludeModule("citrus.tszh"))
	return;

$NS = &$_SESSION["BX_CITRUS_TSZH_IMPORT"];

$absFileName = $_SESSION[$wizard->solutionName]['demoXmlFileName'];

// ��������� ��������� ���, ��������� � �������
$arVars = Array(
	"org_inn" => "inn",
	"org_kpp" => "kpp",
	"org_rsch" => "rs",
	"org_bank" => "bank",
	"org_ksch" => "ks",
	"org_bik" => "bik",
);

$arFields = Array();
foreach ($arVars as $strVar => $field) {
	$value = $wizard->GetVar($strVar);
	$arFields[$field] = $value;
}

$ar = $DB->Query("select ID, ATTRIBUTES from b_xml_tree where ID = 1 and NAME='ORG'")->Fetch();
if (is_array($ar))
{
	$info = unserialize($ar["ATTRIBUTES"]);
	$info = array_merge($info, $arFields);
	$res = $DB->Query("UPDATE b_xml_tree set ATTRIBUTES = '" . serialize($info) . "' where ID = 1");
}

$obImport = new CTszhImport($NS);
$result = $obImport->ProcessPeriod();
$obImport->ImportServices();
if ($result === true) 
{
	$NS['ACCOUNTS_IMPORT_STARTED'] = time();
	$NS["STEP"]++;
} 
else 
{
	unset($_SESSION[$wizard->solutionName]['demoTszhID']);
	$errMsg = "";
	if ($e = $APPLICATION->getException())
		$errMsg = $e->getString();
	if (empty($errMsg))
		$errMsg = getMessage("WIZ_TSZH_IMPORT_ERROR_PROCESS_PERIOD");
	$_SESSION[$wizard->solutionName]["importError"] = $errMsg;
}
?>