<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

$wizard =& $this->GetWizard();

$demoTszhID = intval($_SESSION[$wizard->solutionName]['demoTszhID']);
if ($demoTszhID <= 0)
	return;

if(!CModule::IncludeModule("citrus.tszh"))
	return;

$NS = &$_SESSION["BX_CITRUS_TSZH_IMPORT"];

$absFileName = $_SESSION[$wizard->solutionName]['demoXmlFileName'];
$interval = COption::GetOptionInt('citrus.tszh', '1c_exchange.Interval', 30);
$start_time = time();

$obImport = new CTszhImport($NS, $interval);
$result = $obImport->Cleanup($NS["ACTION"], $start_time, $interval);

$counter = 0;
foreach($result as $key=>$value)
{
	$NS["DONE"][$key] += $value;
	$counter+=$value;
}

if (!$counter) 
{
	$NS["STEP"]++;
}

/*// ������ ��������� ���, ��������� � �������
$arVars = Array(
	"org_inn" => "INN",
	"org_kpp" => "KPP",
	"org_rsch" => "RSCH",
	"org_bank" => "BANK",
	"org_ksch" => "KSCH",
	"org_bik" => "BIK",
);

$arFields = Array();
foreach ($arVars as $strVar => $field) {
	$value = $wizard->GetVar($strVar);
	if (strlen($value) > 0)
		$arFields[$field] = $value;
}

if (!empty($arFields))
	CTszh::Update($demoTszhID, $arFields);*/
?>