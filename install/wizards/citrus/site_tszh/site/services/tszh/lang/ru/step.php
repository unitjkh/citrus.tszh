<?
$MESS["TSZH_SERVICE_REGCODE"] = "Кодовое слово для регистрации";
$MESS["TSZH_SERVICE_REGCODE2"] = "Кодовое слово";
$MESS["UF_DEBTORS_EXCLUDED"] = "Исключить из списка должников";
$MESS["UF_DEBTORS_EXCLUDED2"] = "Исключить из списка должников";
$MESS["TSZH_SERIVCE_ERROR"] = "Ошибка добавления пользовательского поля";
$MESS["TSZH_NAME_DEFAULT"] = "Объект управления по-умолчанию";
$MESS["TSZH_SERVICE_MONETA_ENABLED"] = "Разрешить оплату через сервис Монета.ру";
$MESS["TSZH_SERVICE_MONETA_OFFER"] = "Я подтверждаю свое согласие на прием платежей через сервис Монета.ру (принимаю оферту)";
$MESS["TSZH_HEAD_NAME"] = "ФИО руководителя";
?>