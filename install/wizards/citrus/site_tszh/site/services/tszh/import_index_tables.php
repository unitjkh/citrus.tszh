<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

$wizard =& $this->GetWizard();

$demoTszhID = intval($_SESSION[$wizard->solutionName]['demoTszhID']);
if ($demoTszhID <= 0)
	return;

if(!CModule::IncludeModule("iblock"))
	return;

$NS = &$_SESSION["BX_CITRUS_TSZH_IMPORT"];

if (CIBlockXMLFile::IndexTemporaryTables())
{
	$NS["STEP"]++;
}
else
{
	unset($_SESSION[$wizard->solutionName]['demoTszhID']);
	$_SESSION[$wizard->solutionName]["importError"] = getMessage("WIZ_TSZH_IMPORT_ERROR_INDEX_TABLES");
}
?>
