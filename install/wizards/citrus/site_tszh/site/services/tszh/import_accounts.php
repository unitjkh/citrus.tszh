<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

$wizard =& $this->GetWizard();

$demoTszhID = intval($_SESSION[$wizard->solutionName]['demoTszhID']);
if ($demoTszhID <= 0)
	return;

if(!CModule::IncludeModule("citrus.tszh"))
	return;

$NS = &$_SESSION["BX_CITRUS_TSZH_IMPORT"];

$absFileName = $_SESSION[$wizard->solutionName]['demoXmlFileName'];
$interval = COption::GetOptionInt('citrus.tszh', '1c_exchange.Interval', 30);
$obImport = new CTszhImport($NS, $interval);
$result = $obImport->ImportAccounts();

$counter = 0;
foreach($result as $key=>$value)
{
	$NS["DONE"][$key] += $value;
	$counter+=$value;
}

if (!$counter)
{
	$NS["STEP"]++;
}
?>