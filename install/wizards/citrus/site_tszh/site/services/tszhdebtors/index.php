<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

if (!tszhCheckMinEdition('standard'))
	return;

$wizard =& $this->GetWizard();

// copy files
$sourcePath = str_replace("//", "/", WIZARD_SERVICE_ABSOLUTE_PATH."/public/".LANGUAGE_ID."/");
$destPath = WIZARD_SITE_PATH;
// ���� ��������������� ���������� ������
if (WIZARD_TEMPLATE_ID == 'citrus_tszh_adaptive')
{
	$destPath .= 'epassports/';
	$adaptive_tmp = 'epassports/';
}
// ���� ��������������� ������ ������
else
{
	$adaptive_tmp = '';
}
CopyDirFiles($sourcePath, $destPath, true, true);

//Add menu items
// ���� ��������������� ���������� ������
if (WIZARD_TEMPLATE_ID == 'citrus_tszh_adaptive')
{
	WizardServices::AddMenuItem(WIZARD_SITE_DIR . "/epassports/.show_add.menu.php", Array(
		GetMessage("F_TSZH_DEBTORS_MENUITEM"),
		WIZARD_SITE_DIR . $adaptive_tmp . "debtors/",
		Array(),
		Array(),
		""
	), WIZARD_SITE_ID, -1);
}
else
{
	WizardServices::AddMenuItem(WIZARD_SITE_DIR . "/.top.menu.php", Array(
		GetMessage("F_TSZH_DEBTORS_MENUITEM"),
		WIZARD_SITE_DIR . $adaptive_tmp . "debtors/",
		Array(),
		Array(),
		""
	), WIZARD_SITE_ID, -1);
}

$arSubscribes = CTszhSubscribe::getSubscribes4Admin(false, $wizard);
$arDebtorsParams = $arSubscribes["CTszhSubscribeDebtors"]["SETTINGS_COLLECTED"]["PARAMS"];
CTszhSubscribeDebtors::checkParams($arDebtorsParams, 0, true);

$arReplaceMacros = Array(
	"SITE_DIR" => WIZARD_SITE_DIR,
	"SITE_NAME" => htmlspecialcharsbx($wizard->GetVar("siteName")),
	"SITE_EMAIL" => htmlspecialcharsbx($wizard->GetVar("siteEmail")),
	"SITE_PHONE" => htmlspecialcharsbx($wizard->GetVar("siteTelephone")),
	"SITE_ADDRESS" => htmlspecialcharsbx($wizard->GetVar("siteAddress")),
	"DEBTORS_MIN_SUM" => $arDebtorsParams["MIN_SUM"],
	"DEBTORS_DAY" => $arDebtorsParams["DATE"],
);
WizardServices::ReplaceMacrosRecursive(WIZARD_SITE_PATH . '/' . $adaptive_tmp . 'debtors/', $arReplaceMacros);