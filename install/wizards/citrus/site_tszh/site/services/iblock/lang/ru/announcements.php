<?

$MESS ['WZD_OPTION_ANNOUNCE_1'] = "Объявление";
$MESS ['WZD_OPTION_ANNOUNCE_2'] = "Активность";
$MESS ['WZD_OPTION_ANNOUNCE_3'] = "Начало активности";
$MESS ['WZD_OPTION_ANNOUNCE_4'] = "Окончание активности";
$MESS ['WZD_OPTION_ANNOUNCE_5'] = "Заголовок";
$MESS ['WZD_OPTION_ANNOUNCE_6'] = "Краткий текст для списка";
$MESS ['WZD_OPTION_ANNOUNCE_7'] = "Текст объявления";

$MESS ['WZD_SECTION_ANNOUNCE_1'] = "Таргетинг";
$MESS ['WZD_PROPERTY_ANNOUNCE_1'] = "Район";
$MESS ['WZD_PROPERTY_ANNOUNCE_2'] = "Регион";
$MESS ['WZD_PROPERTY_ANNOUNCE_3'] = "Город";
$MESS ['WZD_PROPERTY_ANNOUNCE_4'] = "Населенный пункт";
$MESS ['WZD_PROPERTY_ANNOUNCE_5'] = "Улица";
$MESS ['WZD_PROPERTY_ANNOUNCE_6'] = "Дом";

?>