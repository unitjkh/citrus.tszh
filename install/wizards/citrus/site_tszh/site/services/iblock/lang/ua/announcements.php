<?
$MESS["WZD_OPTION_ANNOUNCE_1"] = "Оголошення";
$MESS["WZD_OPTION_ANNOUNCE_2"] = "Активність";
$MESS["WZD_OPTION_ANNOUNCE_3"] = "Початок активності";
$MESS["WZD_OPTION_ANNOUNCE_4"] = "Закінчення активності";
$MESS["WZD_OPTION_ANNOUNCE_5"] = "Заголовок";
$MESS["WZD_OPTION_ANNOUNCE_6"] = "Стислий текст для списку ";
$MESS["WZD_OPTION_ANNOUNCE_7"] = "Текст оголошення ";
$MESS["WZD_SECTION_ANNOUNCE_1"] = "Таргетинг (націлення)";
$MESS["WZD_PROPERTY_ANNOUNCE_1"] = "Район";
$MESS["WZD_PROPERTY_ANNOUNCE_2"] = "Регіон";
$MESS["WZD_PROPERTY_ANNOUNCE_3"] = "Місто";
$MESS["WZD_PROPERTY_ANNOUNCE_4"] = "Населений пункт";
$MESS["WZD_PROPERTY_ANNOUNCE_5"] = "Вулиця";
$MESS["WZD_PROPERTY_ANNOUNCE_6"] = "Дом";
?>