<?
$MESS["NEWS_TYPE_NAME"] = "Новости";
$MESS["NEWS_ELEMENT_NAME"] = "Новости";
$MESS["NEWS_SECTION_NAME"] = "Разделы";
$MESS["SERVICES_TYPE_NAME"] = "Сервисы";
$MESS["SERVICES_ELEMENT_NAME"] = "Элементы";
$MESS["SERVICES_SECTION_NAME"] = "Разделы";
$MESS["ARTICLES_TYPE_NAME"] = "Статьи";
$MESS["ARTICLES_ELEMENT_NAME"] = "Элементы";
$MESS["ARTICLES_SECTION_NAME"] = "Разделы";
$MESS["GALLERY_TYPE_NAME"] = "Фотогалерея";
$MESS["GALLERY_ELEMENT_NAME"] = "Фотографии";
$MESS["GALLERY_SECTION_NAME"] = "Альбомы";
$MESS["DOCUMENTS_TYPE_NAME"] = "Документы";
$MESS["DOCUMENTS_ELEMENT_NAME"] = "Документы";
$MESS["DOCUMENTS_SECTION_NAME"] = "Группы";
?>