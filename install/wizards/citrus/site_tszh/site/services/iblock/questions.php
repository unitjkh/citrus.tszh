<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)
	die();

if(!CModule::IncludeModule("iblock"))
	return;

if(COption::GetOptionString("citrus.tszh", "wizard_installed", "N", WIZARD_SITE_ID) == "Y")
	return;

$iblockXMLFile = WIZARD_SERVICE_RELATIVE_PATH."/xml/".LANGUAGE_ID."/services-questions.xml";
$iblockCode = "questions_".WIZARD_SITE_ID;
$iblockType = "services";

$rsIBlock = CIBlock::GetList(array(), array("XML_ID" => $iblockCode, "TYPE" => $iblockType, "SITE_ID" => WIZARD_SITE_ID));
$iblockID = false;
if ($arIBlock = $rsIBlock->Fetch())
{
	$iblockID = (int)$arIBlock["ID"];
	if (WIZARD_REINSTALL_DATA)
	{
		CIBlock::Delete($arIBlock["ID"]);
		$iblockID = false;
	}
}

if($iblockID == false)
{
	$iblockID = WizardServices::ImportIBlockFromXML(
		$iblockXMLFile,
		"questions",
		$iblockType,
		WIZARD_SITE_ID,
		$permissions = Array(
			"1" => "X",
			"2" => "R"
		)
	);

	if ($iblockID < 1)
		return;

	//IBlock fields
	$iblock = new CIBlock;

	$arFields = Array(
		"ACTIVE" => "Y",
		"FIELDS" => array ( 'IBLOCK_SECTION' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'ACTIVE' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => 'Y', ), 'ACTIVE_FROM' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'ACTIVE_TO' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'SORT' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'NAME' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => '', ), 'PREVIEW_PICTURE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => array ( 'FROM_DETAIL' => 'N', 'SCALE' => 'N', 'WIDTH' => '', 'HEIGHT' => '', 'IGNORE_ERRORS' => 'N', 'METHOD' => 'resample', 'COMPRESSION' => 95, 'DELETE_WITH_DETAIL' => 'N', 'UPDATE_WITH_DETAIL' => 'N', ), ), 'PREVIEW_TEXT_TYPE' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => 'text', ), 'PREVIEW_TEXT' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'DETAIL_PICTURE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => array ( 'SCALE' => 'N', 'WIDTH' => '', 'HEIGHT' => '', 'IGNORE_ERRORS' => 'N', 'METHOD' => 'resample', 'COMPRESSION' => 95, ), ), 'DETAIL_TEXT_TYPE' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => 'text', ), 'DETAIL_TEXT' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'XML_ID' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'CODE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => array ( 'UNIQUE' => 'N', 'TRANSLITERATION' => 'N', 'TRANS_LEN' => 100, 'TRANS_CASE' => 'L', 'TRANS_SPACE' => '_', 'TRANS_OTHER' => '_', 'TRANS_EAT' => 'Y', 'USE_GOOGLE' => 'N', ), ), 'TAGS' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'SECTION_NAME' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => '', ), 'SECTION_PICTURE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => array ( 'FROM_DETAIL' => 'N', 'SCALE' => 'N', 'WIDTH' => '', 'HEIGHT' => '', 'IGNORE_ERRORS' => 'N', 'METHOD' => 'resample', 'COMPRESSION' => 95, 'DELETE_WITH_DETAIL' => 'N', 'UPDATE_WITH_DETAIL' => 'N', ), ), 'SECTION_DESCRIPTION_TYPE' => array ( 'IS_REQUIRED' => 'Y', 'DEFAULT_VALUE' => 'text', ), 'SECTION_DESCRIPTION' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'SECTION_DETAIL_PICTURE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => array ( 'SCALE' => 'N', 'WIDTH' => '', 'HEIGHT' => '', 'IGNORE_ERRORS' => 'N', 'METHOD' => 'resample', 'COMPRESSION' => 95, ), ), 'SECTION_XML_ID' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => '', ), 'SECTION_CODE' => array ( 'IS_REQUIRED' => 'N', 'DEFAULT_VALUE' => array ( 'UNIQUE' => 'N', 'TRANSLITERATION' => 'N', 'TRANS_LEN' => 100, 'TRANS_CASE' => 'L', 'TRANS_SPACE' => '_', 'TRANS_OTHER' => '_', 'TRANS_EAT' => 'Y', 'USE_GOOGLE' => 'N', ), ), ),
		"CODE" => "questions",
		"XML_ID" => $iblockCode,
		//"NAME" => "[".WIZARD_SITE_ID."] ".$iblock->GetArrayByID($iblockID, "NAME")
	);

	$iblock->Update($iblockID, $arFields);
}

$dbSite = CSite::GetByID(WIZARD_SITE_ID);
if($arSite = $dbSite -> Fetch())
	$lang = $arSite["LANGUAGE_ID"];
if(strlen($lang) <= 0)
	$lang = "ru";

WizardServices::IncludeServiceLang("questions.php", $lang);

$arFormFields = array (
  'tabs' => 'edit1--#--' . GetMessage('WZD_OPTION_QUESTIONS_1') . '--,--ACTIVE--#--' . GetMessage('WZD_OPTION_QUESTIONS_2') . '--,--SECTIONS--#--' . GetMessage('WZD_OPTION_QUESTIONS_3') . '--,--edit1_csection1--#----' . GetMessage('WZD_OPTION_QUESTIONS_4') . '--,--NAME--#--' . GetMessage('WZD_OPTION_QUESTIONS_5') . '--,--PROPERTY_author_address--#--' . GetMessage('WZD_OPTION_QUESTIONS_6') . '--,--PROPERTY_author_phone--#--' . GetMessage('WZD_OPTION_QUESTIONS_7') . '--,--PROPERTY_author_email--#--' . GetMessage('WZD_OPTION_QUESTIONS_8') . '--,--PREVIEW_TEXT--#--' . GetMessage('WZD_OPTION_QUESTIONS_9') . '--,--edit1_csection2--#----' . GetMessage('WZD_OPTION_QUESTIONS_10') . '--,--PROPERTY_answer_author--#--' . GetMessage('WZD_OPTION_QUESTIONS_11') . '--,--DETAIL_TEXT--#--' . GetMessage('WZD_OPTION_QUESTIONS_12') . '--;--',
);

$rsProperties = CIBlockProperty::GetList(Array(), Array(
	"IBLOCK_ID" => $iblockID,
));
$arProperties = Array();
while ($arProperty = $rsProperties->Fetch()) {
	if (strlen($arProperty["CODE"]) > 0) {
		$arFormFields['tabs'] = str_ireplace('PROPERTY_' . $arProperty["CODE"], 'PROPERTY_' . $arProperty["ID"], $arFormFields['tabs']);
		$arProperties[] = $arProperty;
	}
}
CUserOptions::SetOption("form", "form_element_".$iblockID, $arFormFields);
CUserOptions::SetOption("list", "tbl_iblock_list_".md5($iblockType.".".$iblockID), array (
  'columns' => 'TIMESTAMP_X,ID,CREATED_USER_NAME,NAME,PREVIEW_TEXT',
  'by' => 'timestamp_x',
  'order' => 'desc',
  'page_size' => '20',
));


$arReplaceMacros = array("QUESTIONS_IBLOCK_ID" => $iblockID);
foreach ($arProperties as $arProperty) {
	$arReplaceMacros['PROPERTY_' . $arProperty["CODE"]] = $arProperty["ID"];
}

CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."/questions-and-answers/index.php", $arReplaceMacros);

COption::SetOptionInt('citrus.tszh', "questions.iblock", $iblockID, "", WIZARD_SITE_ID);

\Citrus\Tszh\Wizard\Seo::setIblockSettings($iblockID, true, true, true, true);