<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("SHOW_TOP_RIGHT", "N");
?>
<?$APPLICATION->IncludeComponent(
	"vdgb:tszhepasport", 
	".default", 
	array(
		"CACHE_TIME" => "300",
		"CACHE_TYPE" => "A",
		"COMPONENT_TEMPLATE" => ".default",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FILE" => "",
		"MAPCONTROLS" => array(
			0 => "searchControl",
			1 => "trafficControl",
			2 => "zoomControl",
			3 => "typeSelector",
			4 => "rulerControl",
			5 => "fullscreenControl",
		),
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Периоды",
		"YANDEX_CLUSTERIZATION" => "Y"
	),
	false
);?>
<? if (isset($_REQUEST['home-id']))
{
	$APPLICATION->IncludeComponent(
		"vdgb:documents.list",
		".default",
		array(
			"CACHE_GROUPS" => "Y",
			"CACHE_TIME" => "36000000",
			"CACHE_TYPE" => "A",
			"COUNT_PAGE" => "10",
			"DESC" => "ASC",
			"ENTITY_ID" => $_REQUEST["home-id"],
			"SORT" => "DOCUMENT.ORIGINAL_NAME",
			"COMPONENT_TEMPLATE" => ".default"
		),
		false
	);
}
?><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>