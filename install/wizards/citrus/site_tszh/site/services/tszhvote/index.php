<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (!tszhCheckMinEdition("standard"))
	return;

if (!CModule::IncludeModule("vdgb.tszhvote"))
	return;

//Add root menu items
$arPersonalMenuItems = Array(
	Array(
		GetMessage("VOTING_MENU_1"),
		WIZARD_SITE_DIR . "personal/",
		Array(),
		Array("class"=>"vote"),
		""
	),
);
foreach ($arPersonalMenuItems as $arMenuItem)
	WizardServices::AddMenuItem(WIZARD_SITE_DIR  . ".section.menu.php", $arMenuItem, WIZARD_SITE_ID, -1);

$strIndexIncVote = '<h3 class="alt">' . GetMessage("VOTING_TITLE") . '</h3>
<?$APPLICATION->IncludeComponent(
	"citrus:tszh.voting", 
	"personal", 
	array(
		"LIMIT_FIELD_NAME" => "CREATED_BY",
		"LIMIT_FIELD_VALUE" => $USER->GetID(),
		"VOTE_TYPE_DIOGRAM" => "1",
		"GROUP_ID" => "PERSONAL_' . WIZARD_SITE_ID . '",
		"SEF_MODE" => "N",
		"SEF_FOLDER" => "' . WIZARD_SITE_DIR . 'personal/",
		"VARIABLE_ALIASES" => array(
			"VOTING_ID" => "VOTING_ID",
		)
	),
	false
);?>
';
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH."personal/index.php", array('<!-- #VOTING# -->' => $strIndexIncVote), $bSkipSharp = true);

if (COption::GetOptionString("citrus.tszh", "wizard_vote_installed", "N", WIZARD_SITE_ID) == "Y")
	return;

$arFieldsPG = array(
	"LID"		=> WIZARD_SITE_ID,
	"NAME"		=> GetMessage("VOTING_INSTALL_CHANNEL_PERSONAL"),
	"CODE"		=> "PERSONAL_" . WIZARD_SITE_ID,
	"SORT"		=> "50",
	"ACTIVE"	=> "Y",
	"TITLE"		=> GetMessage("VOTING_INSTALL_CHANNEL_PERSONAL"),
);

$rsPollGroup = CCitrusPollGroup::GetList(array(), array("CODE" => "PERSONAL_" . WIZARD_SITE_ID), false, array("nTopCount" => 1), array("ID"));
if (!$rsPollGroup->Fetch())
{
	$ID = CCitrusPollGroup::Add($arFieldsPG);
	if ($ID > 0)
	{
		$arFieldsVote = array(
			"GROUP_ID"			=> $ID,
			"DATE_BEGIN"		=> ConvertTimeStamp(false, "SHORT", WIZARD_SITE_ID),
			"DATE_END"			=> ConvertTimeStamp(strtotime("+10 years"), "SHORT", WIZARD_SITE_ID),
			"NAME"				=> GetMessage("VOTING_INSTALL_VOTE_PERSONAL_VOTE_TITLE"),
			"ACTIVE"			=> "Y",
			"VOTING_METHOD"		=> "count",
			"ENTITY_TYPE"		=> "tszh_account",
			"TITLE_TEXT"		=> GetMessage("VOTING_INSTALL_VOTE_PERSONAL_VOTE_TITLE"),
			"VOTING_COMPLETED"	=> "N",
		);
		$VOTE_ID = CCitrusPoll::Add($arFieldsVote);

		$arFieldsQuestion = array(
			"ACTIVE"		=> "Y",
			"VOTING_ID"		=> $VOTE_ID,
			"TEXT"			=> GetMessage("VOTING_INSTALL_VOTE_QUESTION4"),
			"SORT"			=> "100",
			"IS_MULTIPLE"	=> "N",
		);
		$Q_ID = CCitrusPollQuestion::Add($arFieldsQuestion);

		$r = -3; $b = 18;
		for ($i = 1; $i <= 5; $i++)
		{
			$r += 3; $g = $i % 2 == 1 ? 7 : 8; $b -= 3;
			$answ = array(
				"ACTIVE"		=> "Y",
				"QUESTION_ID"	=> $Q_ID,
				"TEXT"			=> GetMessage("VOTING_INSTALL_VOTE_ANSWER4_" . $i),
				"SORT"			=> $i*100,
				"COLOR"			=> "#" . str_repeat(dechex($r), 2) . str_repeat(dechex($g), 2) . str_repeat(dechex($b), 2),
			);
			CCitrusPollAnswer::Add($answ);
		}
	}
}

COption::SetOptionString("citrus.tszh", "wizard_vote_installed", "Y", false, WIZARD_SITE_ID);
