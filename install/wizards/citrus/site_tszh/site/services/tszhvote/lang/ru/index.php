<?
$MESS ['VOTING_INSTALL_II_VOTE'] = "Опрос";
$MESS ['VOTING_INSTALL_MENU_ITEM'] = "Опросы";
$MESS ['VOTING_INSTALL_CHANNEL_PERSONAL'] = "Опрос в личном кабинете";
$MESS ['VOTING_INSTALL_VOTE_PERSONAL_VOTE_TITLE'] = "Как вы оцениваете работу лифтового хозяйства, обслуживающего наш дом?";
$MESS ['VOTING_INSTALL_VOTE_QUESTION4'] = "Как вы оцениваете работу лифтового хозяйства, обслуживающего наш дом?";
$MESS ['VOTING_INSTALL_VOTE_ANSWER4_1'] = "На “отлично”, я очень доволен";
$MESS ['VOTING_INSTALL_VOTE_ANSWER4_2'] = "Неплохо, но лифт часто не работет";
$MESS ['VOTING_INSTALL_VOTE_ANSWER4_3'] = "Я не вижу результатов работы";
$MESS ['VOTING_INSTALL_VOTE_ANSWER4_4'] = "Надо срочно менять лифтеров";
$MESS ['VOTING_INSTALL_VOTE_ANSWER4_5'] = "Надо срочно менять лифтеров";

$MESS ['VOTING_MENU_1'] = "Участвовать в голосовании и многое другое";
$MESS ['VOTING_TITLE'] = "Голосование";

?>