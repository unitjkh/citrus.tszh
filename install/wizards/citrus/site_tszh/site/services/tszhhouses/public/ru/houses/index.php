<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Список домов");
?><?$APPLICATION->IncludeComponent(
    "vdgb:tszh.houses",
    ".default",
    array(
        "CACHE_TIME" => "7200",
        "CACHE_TYPE" => "A",
        "COMPONENT_TEMPLATE" => ".default",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "DISPLAY_TOP_PAGER" => "Y",
        "PAGER_DESC_NUMBERING" => "N",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => ".default",
        "PAGER_TITLE" => "ТСЖ",
        "SELECT_FIELDS" => array(
            0 => "TYPE",
            1 => "ADDRESS_USER_ENTERED",
            2 => "YEAR_OF_BUILT",
            3 => "FLOORS",
            4 => "PORCHES",
            5 => "AREA",
        ),
        "SORT_BY" => "AREA",
        "SORT_ORDER" => "ASC",
        "TSZH_COUNT" => "20",
        "TSZH_FIELD" => "5",
        "TSZH_ID" => "0"
    ),
    false
);?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>