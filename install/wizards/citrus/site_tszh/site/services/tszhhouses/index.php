<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

$wizard =& $this->GetWizard();

if (!tszhCheckMinEdition('smallbusiness'))
{
	if (IsModuleInstalled('vdgb.tszhhouses'))
	{
        $sourcePath = str_replace("//", "/", WIZARD_SERVICE_ABSOLUTE_PATH . "/public/" . LANGUAGE_ID . "/houses/");
        $destPath = str_replace("//", "/", WIZARD_SITE_PATH . '/epassports/');
        CopyDirFiles($sourcePath, $destPath, true, true);
    }

    // ���� ��������������� ���������� ������
    if (isAdaptiveTszhTemplate())
    {
        WizardServices::AddMenuItem(WIZARD_SITE_DIR . "/epassports/.show_add.menu.php", Array(
            GetMessage("F_TSZH_EPASPORTS_SHOW_ADD_MENUITEM"),
            WIZARD_SITE_DIR . 'epassports',
            Array(),
            Array(),
            "",
        ), false, 0);
    }
// ���� ��������������� ������ ������
    else
    {
        WizardServices::AddMenuItem(WIZARD_SITE_DIR . "/documents/.show_add.menu.php", Array(
            GetMessage("F_TSZH_EPASPORTS_MENUITEM"),
            WIZARD_SITE_DIR . 'epassports/',
            Array(),
            Array(),
            "",
        ), false, -1);
    }
}

