<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

if (!tszhCheckMinEdition('standard'))
{
	return;
}

if (!CModule::IncludeModule("forum"))
{
	return;
}

if (isAdaptiveTszhTemplate())
{
    WizardServices::AddMenuItem(WIZARD_SITE_DIR . "/epassports/.show_add.menu.php", Array(
        GetMessage("F_FORUM"),
        WIZARD_SITE_DIR . "forum/",
        Array(),
        Array(),
        "",
    ), WIZARD_SITE_ID, -1);

    $fp = fopen(WIZARD_SITE_PATH . "forum/sect_right.php", "w");
    fwrite($fp, "");
    fclose($fp);
}
else
{
    // add menu item
    WizardServices::AddMenuItem(WIZARD_SITE_DIR . "/photogallery/.show_add.menu.php", Array(
        GetMessage("F_FORUM"),
        WIZARD_SITE_DIR . "forum/",
        Array(),
        Array(),
        "",
    ), WIZARD_SITE_ID, -1);
    WizardServices::AddMenuItem(WIZARD_SITE_DIR . "/photogallery/.section.menu.php", Array(
        GetMessage("F_FORUM"),
        WIZARD_SITE_DIR . "forum/",
        Array(),
        Array(),
        "",
    ), WIZARD_SITE_ID, -1);
}

if (COption::GetOptionString("citrus.tszh", "wizard_forum_installed", "N", WIZARD_SITE_ID) == "Y")
{
	// ������� ������������ ��������� ������ � sitemap
	$forums = CForumNew::GetListEx(array("SORT" => "ASC"), array("ACTIVE" => "Y", "SITE_ID" => WIZARD_SITE_ID));
	while ($forum = $forums->Fetch())
	{
		$permissions = CForumNew::GetAccessPermissions($forum["ID"], "ALL");
		\Citrus\Tszh\Wizard\Seo::setForumSettings(intval($forum["ID"]), $permissions[2] >= 'E', $permissions[2] >= 'E');
	}

	return;
}

$SITE_ID = WIZARD_SITE_ID;

// turn off name display by default for new users
COption::SetOptionString("forum", "SHOW_NAME", "N", false, WIZARD_SITE_ID);

// Get language
$arLanguages = Array();
$rsLanguage = CLanguage::GetList($by, $order, array());
while ($arLanguage = $rsLanguage->Fetch())
{
	$arLanguages[] = $arLanguage["LID"];
}

// PointS
$db_res = CForumPoints::GetListEx();
if (!$db_res)
{
	$arFieldsG = array(
		array("MIN_POINTS" => 0, "CODE" => "visitor", "VOTES" => 1, "LANG" => array()),
		array("MIN_POINTS" => 5, "CODE" => "resident", "VOTES" => 2, "LANG" => array()),
		array("MIN_POINTS" => 50, "CODE" => "user", "VOTES" => 4, "LANG" => array()),
		array("MIN_POINTS" => 200, "CODE" => "honored", "VOTES" => 7, "LANG" => array()),
	);
	foreach ($arFieldsG as $arFields)
	{
		foreach ($arLanguages as $languageID)
		{
			WizardServices::IncludeServiceLang("index.php", $languageID);
			$arFields["LANG"][$languageID] = array("LID" => $languageID, "NAME" => GetMessage("F_POINTS_" . strToUpper($arFields["CODE"])));
		}
		$res = CForumPoints::Add($arFields);
	}
}

$db_res = CForumPoints2Post::GetList();
if (!($db_res && $res = $db_res->Fetch()))
{
	$arFields = array(
		"MIN_NUM_POSTS" => 1,
		"POINTS_PER_POST" => "0.5000",
	);
	CForumPoints2Post::Add($arFields);
	$arFields = array(
		"MIN_NUM_POSTS" => 50,
		"POINTS_PER_POST" => "0.8000",
	);
	CForumPoints2Post::Add($arFields);
}
/* User */
$res = CForumUser::GetByUSER_ID(1);
if (empty($res) || !is_array($res))
{
	$arFields = array(
		"=LAST_VISIT" => $DB->GetNowFunction(),
		"USER_ID" => 1,
	);
	$ID = CForumUser::Add($arUserFields);
}
/* Vote */
$res = CForumUserPoints::GetByID(1, 1);
if (!$res)
{
	$arFields = array(
		"POINTS" => 1000,
		"FROM_USER_ID" => 1,
		"TO_USER_ID" => 1,
	);
	$ID = CForumUserPoints::Add($arFields);
}

// Forum group
$arGroup = array(
	"GENERAL" => 0,
);

$db_res = CForumGroup::GetListEx(array(), array("LID" => LANGUAGE_ID));
if ($db_res && $res = $db_res->Fetch())
{
	do
	{
		if (GetMessage("F_GROUP_GENERAL") == $res["NAME"]):
			$arGroup["GENERAL"] = intVal($res["ID"]);
		endif;
	}
	while ($res = $db_res->Fetch());
}

if (array_sum($arGroup) <= 0)
{
	// Set Group
	foreach ($arGroup as $key => $res)
	{
		if ($res > 0)
		{
			continue;
		}
		$arFields = array("SORT" => 150);
		foreach ($arLanguages as $languageID)
		{
			$name = GetMessage("F_GROUP_" . $key);
			$description = GetMessage("F_GROUP_" . $key . "_DESCRIPTION");

			$arFields["LANG"][] = array(
				"LID" => $languageID,
				"NAME" => $name,
				"DESCRIPTION" => $description,
			);
		}
		$arGroup[$key] = CForumGroup::Add($arFields);
	}
}
// Forums
$arForums = array();
$db_res = CForumNew::GetList(array(), array("SITE_ID" => $SITE_ID));
if ($db_res && $res = $db_res->Fetch())
{
	do
	{
		$arForums[] = $res["NAME"];
	}
	while ($res = $db_res->Fetch());
}
// Forum � 1
if (!in_array(GetMessage("F_FORUM_1_NAME"), $arForums)):

	$membersForumRights = Array("1" => 'Y', "2" => 'E', WIZARD_TSZH_MEMBERS_GROUP => 'M');

	$arFields = Array(
		"NAME" => GetMessage("F_FORUM_1_NAME"),
		"DESCRIPTION" => GetMessage("F_FORUM_1_DECRIPTION"),
		"SORT" => 100,
		"ACTIVE" => "Y",
		"ALLOW_HTML" => "N",
		"ALLOW_ANCHOR" => "N",
		"ALLOW_BIU" => "Y",
		"ALLOW_IMG" => "Y",
		"ALLOW_LIST" => "Y",
		"ALLOW_QUOTE" => "Y",
		"ALLOW_CODE" => "Y",
		"ALLOW_FONT" => "Y",
		"ALLOW_SMILES" => "Y",
		"ALLOW_UPLOAD" => "N",
		"ALLOW_NL2BR" => "N",
		"MODERATION" => "N",
		"ALLOW_MOVE_TOPIC" => "Y",
		"ORDER_BY" => "P",
		"ORDER_DIRECTION" => "DESC",
		"LID" => LANGUAGE_ID,
		"PATH2FORUM_MESSAGE" => "",
		"ALLOW_UPLOAD_EXT" => "",
		"FORUM_GROUP_ID" => $arGroup["PUBLIC"],
		"ASK_GUEST_EMAIL" => "N",
		"USE_CAPTCHA" => "Y",
		"SITES" => array(
			$SITE_ID => "/forum/messages/forum#FORUM_ID#/topic#TOPIC_ID#/message#MESSAGE_ID#/",
		),
		"EVENT1" => "forum",
		"EVENT2" => "message",
		"EVENT3" => "",
		"GROUP_ID" => $membersForumRights,
	);
	$FID = CForumNew::Add($arFields);
	if ($FID > 0)
	{
		$arFields = Array(
			"FORUM_ID" => $FID,
			"TITLE" => GetMessage("F_FORUM_1_TOPIC_1_TITLE"),
			"DESCRIPTION" => GetMessage("F_FORUM_1_TOPIC_1_DESCRIPTION"),
			"ICON_ID" => 0,
			"TAGS" => GetMessage("F_FORUM_1_TOPIC_1_TAGS"),
			"USER_START_ID" => 1,
			"USER_START_NAME" => GetMessage("F_FORUM_1_TOPIC_1_AUTHOR"),
			"LAST_POSTER_NAME" => GetMessage("F_FORUM_1_TOPIC_1_AUTHOR"),
			"APPROVED" => "Y",
		);
		$TID = intVal(CForumTopic::Add($arFields));
		if ($TID > 0)
		{
			$arFields = Array(
				"POST_MESSAGE" => GetMessage("F_FORUM_1_TOPIC_1_MESSAGE_1_POSTMESS"),
				"USE_SMILES" => "Y",
				"APPROVED" => "Y",
				"AUTHOR_NAME" => GetMessage("F_FORUM_1_TOPIC_1_AUTHOR"),
				"AUTHOR_EMAIL" => "",
				"FORUM_ID" => $FID,
				"TOPIC_ID" => $TID,
				"AUTHOR_IP" => "SWAMP",
				"AUTHOR_REAL_IP" => "SWAMP",
				"NEW_TOPIC" => "Y",
			);
			$MID = CForumMessage::Add($arFields, false);

			$arFields = Array(
				"POST_MESSAGE" => GetMessage("F_FORUM_1_TOPIC_1_MESSAGE_2_POSTMESS"),
				"USE_SMILES" => "Y",
				"APPROVED" => "Y",
				"AUTHOR_NAME" => GetMessage("F_FORUM_1_MESSAGE_AUTHOR_1"),
				"AUTHOR_EMAIL" => "",
				"FORUM_ID" => $FID,
				"TOPIC_ID" => $TID,
				"AUTHOR_IP" => "SWAMP",
				"AUTHOR_REAL_IP" => "SWAMP",
				"NEW_TOPIC" => "N",
			);
			$MID = CForumMessage::Add($arFields, false);

			$arFields = Array(
				"POST_MESSAGE" => GetMessage("F_FORUM_1_TOPIC_1_MESSAGE_3_POSTMESS"),
				"USE_SMILES" => "Y",
				"APPROVED" => "Y",
				"AUTHOR_NAME" => GetMessage("F_FORUM_1_TOPIC_1_AUTHOR"),
				"AUTHOR_EMAIL" => "",
				"FORUM_ID" => $FID,
				"TOPIC_ID" => $TID,
				"AUTHOR_IP" => "SWAMP",
				"AUTHOR_REAL_IP" => "SWAMP",
				"NEW_TOPIC" => "N",
			);
			$MID = CForumMessage::Add($arFields, false);
		}

		$arFields = Array(
			"FORUM_ID" => $FID,
			"TITLE" => GetMessage("F_FORUM_1_TOPIC_2_TITLE"),
			"DESCRIPTION" => GetMessage("F_FORUM_1_TOPIC_1_DESCRIPTION"),
			"ICON_ID" => 0,
			"TAGS" => GetMessage("F_FORUM_1_TOPIC_1_TAGS"),
			"USER_START_ID" => 1,
			"USER_START_NAME" => GetMessage("F_FORUM_1_TOPIC_1_AUTHOR"),
			"LAST_POSTER_NAME" => GetMessage("F_FORUM_1_TOPIC_1_AUTHOR"),
			"APPROVED" => "Y",
		);
		$TID = intVal(CForumTopic::Add($arFields));
		if ($TID > 0)
		{
			$arFields = Array(
				"POST_MESSAGE" => GetMessage("F_FORUM_1_TOPIC_2_MESSAGE_1_POSTMESS"),
				"USE_SMILES" => "Y",
				"APPROVED" => "Y",
				"AUTHOR_NAME" => GetMessage("F_FORUM_1_TOPIC_1_AUTHOR"),
				"AUTHOR_EMAIL" => "",
				"FORUM_ID" => $FID,
				"TOPIC_ID" => $TID,
				"AUTHOR_IP" => "SWAMP",
				"AUTHOR_REAL_IP" => "SWAMP",
				"NEW_TOPIC" => "Y",
			);
			$MID = CForumMessage::Add($arFields, false);

			$arFields = Array(
				"POST_MESSAGE" => GetMessage("F_FORUM_1_TOPIC_2_MESSAGE_2_POSTMESS"),
				"USE_SMILES" => "Y",
				"APPROVED" => "Y",
				"AUTHOR_NAME" => GetMessage("F_FORUM_1_MESSAGE_AUTHOR_1"),
				"AUTHOR_EMAIL" => "",
				"FORUM_ID" => $FID,
				"TOPIC_ID" => $TID,
				"AUTHOR_IP" => "SWAMP",
				"AUTHOR_REAL_IP" => "SWAMP",
				"NEW_TOPIC" => "N",
			);
			$MID = CForumMessage::Add($arFields, false);

			$arFields = Array(
				"POST_MESSAGE" => GetMessage("F_FORUM_1_TOPIC_2_MESSAGE_3_POSTMESS"),
				"USE_SMILES" => "Y",
				"APPROVED" => "Y",
				"AUTHOR_NAME" => GetMessage("F_FORUM_1_MESSAGE_AUTHOR_2"),
				"AUTHOR_EMAIL" => "",
				"FORUM_ID" => $FID,
				"TOPIC_ID" => $TID,
				"AUTHOR_IP" => "SWAMP",
				"AUTHOR_REAL_IP" => "SWAMP",
				"NEW_TOPIC" => "N",
			);
			$MID = CForumMessage::Add($arFields, false);
		}

		$arFields = Array(
			"FORUM_ID" => $FID,
			"TITLE" => GetMessage("F_FORUM_1_TOPIC_3_TITLE"),
			"DESCRIPTION" => GetMessage("F_FORUM_1_TOPIC_1_DESCRIPTION"),
			"ICON_ID" => 0,
			"TAGS" => GetMessage("F_FORUM_1_TOPIC_1_TAGS"),
			"USER_START_ID" => 1,
			"USER_START_NAME" => GetMessage("F_FORUM_1_TOPIC_1_AUTHOR"),
			"LAST_POSTER_NAME" => GetMessage("F_FORUM_1_TOPIC_1_AUTHOR"),
			"APPROVED" => "Y",
		);
		$TID = intVal(CForumTopic::Add($arFields));
		if ($TID > 0)
		{
			$arFields = Array(
				"POST_MESSAGE" => GetMessage("F_FORUM_1_TOPIC_3_MESSAGE_1_POSTMESS"),
				"USE_SMILES" => "Y",
				"APPROVED" => "Y",
				"AUTHOR_NAME" => GetMessage("F_FORUM_1_TOPIC_1_AUTHOR"),
				"AUTHOR_EMAIL" => "",
				"FORUM_ID" => $FID,
				"TOPIC_ID" => $TID,
				"AUTHOR_IP" => "SWAMP",
				"AUTHOR_REAL_IP" => "SWAMP",
				"NEW_TOPIC" => "Y",
			);
			$MID = CForumMessage::Add($arFields, false);

			$arFields = Array(
				"POST_MESSAGE" => GetMessage("F_FORUM_1_TOPIC_3_MESSAGE_2_POSTMESS"),
				"USE_SMILES" => "Y",
				"APPROVED" => "Y",
				"AUTHOR_NAME" => GetMessage("F_FORUM_1_MESSAGE_AUTHOR_1"),
				"AUTHOR_EMAIL" => "",
				"FORUM_ID" => $FID,
				"TOPIC_ID" => $TID,
				"AUTHOR_IP" => "SWAMP",
				"AUTHOR_REAL_IP" => "SWAMP",
				"NEW_TOPIC" => "N",
			);
			$MID = CForumMessage::Add($arFields, false);

			$arFields = Array(
				"POST_MESSAGE" => GetMessage("F_FORUM_1_TOPIC_3_MESSAGE_3_POSTMESS"),
				"USE_SMILES" => "Y",
				"APPROVED" => "Y",
				"AUTHOR_NAME" => GetMessage("F_FORUM_1_MESSAGE_AUTHOR_2"),
				"AUTHOR_EMAIL" => "",
				"FORUM_ID" => $FID,
				"TOPIC_ID" => $TID,
				"AUTHOR_IP" => "SWAMP",
				"AUTHOR_REAL_IP" => "SWAMP",
				"NEW_TOPIC" => "N",
			);
			$MID = CForumMessage::Add($arFields, false);
		}
	}
endif;

//Copy public files with "on the fly" translation
// copy files
$sourcePath = str_replace("//", "/", WIZARD_SERVICE_ABSOLUTE_PATH . "/public/" . LANGUAGE_ID . "/");
$destPath = WIZARD_SITE_PATH;
CopyDirFiles($sourcePath, $destPath, true, true);

// add url rewrite rule
$arUrlRewrite = array();
if (file_exists(WIZARD_SITE_ROOT_PATH . "/urlrewrite.php"))
{
	include(WIZARD_SITE_ROOT_PATH . "/urlrewrite.php");
}
$arNewUrlRewrite = array(
	array(
		"CONDITION" => "#^" . WIZARD_SITE_DIR . "forum/#",
		"RULE" => "",
		"ID" => "bitrix:forum",
		"PATH" => WIZARD_SITE_DIR . "forum/index.php",
	),
);
foreach ($arNewUrlRewrite as $arUrl)
{
	if (!in_array($arUrl, $arUrlRewrite))
	{
		CUrlRewriter::Add($arUrl);
	}
}

$arForumThemes = Array(
	'blue' => 'blue',
	'brown' => 'red',
	'green' => 'green',
	'orange' => 'orange',
	'default' => 'gray',
);
if (array_key_exists(WIZARD_THEME_ID, $arForumThemes))
{
	$strForumTheme = $arForumThemes[WIZARD_THEME_ID];
}
else
{
	$strForumTheme = 'gray';
}

// Set forum theme
$arReplace = Array(
	'FORUM_THEME' => $strForumTheme,
	'FID' => $FID,
	"SITE_DIR" => WIZARD_SITE_DIR,
);
CWizardUtil::ReplaceMacros(WIZARD_SITE_PATH . "/forum/index.php", $arReplace);

COption::SetOptionString("citrus.tszh", "wizard_forum_installed", "Y", false, WIZARD_SITE_ID);

// ������� ������������ ��������� ������ � sitemap
$forums = CForumNew::GetListEx(array("SORT" => "ASC"), array("ACTIVE" => "Y", "SITE_ID" => WIZARD_SITE_ID));
while ($forum = $forums->Fetch())
{
	$permissions = CForumNew::GetAccessPermissions($forum["ID"], "ALL");
	\Citrus\Tszh\Wizard\Seo::setForumSettings(intval($forum["ID"]), $permissions[2] >= 'E', $permissions[2] >= 'E');
}
