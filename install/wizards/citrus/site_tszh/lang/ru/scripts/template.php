<?
$MESS ['WIZARD_TITLE'] = "Настройка продукта<br>&laquo;1С: Сайт ЖКХ&raquo; #VERSION#";
$MESS ['WIZARD_TITLE_SOL'] = "";
$MESS ['COPYRIGHT'] = "&copy; 2002 &laquo;Битрикс&raquo;, 2007-#CURRENT_YEAR# &laquo;1С-Битрикс&raquo;";
$MESS ['COPYRIGHT'] = "&copy; 2009–#CURRENT_YEAR#. Разработка: <a href=\"http://vgkh.ru/\" target=\"_blank\">Компания «1С-Рарус»</a>";
$MESS ['SUPPORT'] = "<a href=\"http://www.vdgb-soft.ru/?referer1=citrus_tszh&referer2=install\" target=\"_blank\">www.vdgb-soft.ru</a> | +7 (495) 777-25-43, +7 (836) 249-46-89 | <a href=\"mailto:clients@vdgb-soft.ru\">clients@vdgb-soft.ru</a>";
$MESS ['INST_JAVASCRIPT_DISABLED'] = "Для установки продукта необходимо включить JavaScript. По-видимому, JavaScript либо не поддерживается браузером, либо отключен. Измените настройки браузера и затем <a href=\"\">повторите попытку</a>.";
