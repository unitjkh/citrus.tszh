<?
$MESS["WIZ_EMPTY_FIELD"] = "Не заповнене поле «#FIELD#»";
$MESS["WIZ_INVALID_MONTH_DAY_FIELD"] = "Число місяця в поле «#FIELD#» має бути в діапазоні від 1 до 31 включно";
$MESS["WIZ_COMPANY_NAME"] = "Назва сайту або організації";
$MESS["WIZ_COMPANY_NAME_DEF"] = "ТСЖ \"ЖилКом\"";
$MESS["WIZ_COMPANY_TELEPHONE"] = "Контактний телефон";
$MESS["WIZ_COMPANY_TELEPHONE_DEF"] = "8 (836) 230-40-09";
$MESS["WIZ_STEP_SITE_SET"] = "Інформація про сайт";
$MESS["wiz_structure_data"] = "Встановити демонстраційні дані сайту";
$MESS["wiz_restructure_data"] = "Перевстановити демонстраційні дані сайту";
$MESS["WIZ_COMPANY_EMAIL"] = "Контактний E-Mail";
$MESS["WIZ_COMPANY_ADDRESS"] = "Адреса організації";
$MESS["WIZ_COMPANY_ADDRESS_DEF"] = "Росія, 424004, м. Йошкар-Ола, вул. Волкова, б. 68";
$MESS["wiz_site_name"] = "1С: Сайт ЖКГ";
$MESS["WIZ_TSZH_OF_NAME"] = "Назва організації";
$MESS["WIZ_TSZH_OF_NAME_DEF"] = "ТСЖ \"ЖилКом\"";
$MESS["WIZ_TSZH_BANK_TITLE"] = "Банківські реквізити";
$MESS["WIZ_TSZH_BANK"] = "Банк";
$MESS["WIZ_TSZH_BANK_DEF"] = "ВАТ \"Сбербанк России\", м. Москва";
$MESS["WIZ_TSZH_INN"] = "ІПН";
$MESS["WIZ_TSZH_KPP"] = "КПП";
$MESS["WIZ_TSZH_RSCH"] = "Розрахунковий рахунок";
$MESS["WIZ_TSZH_KSCH"] = "Корр. рахунок";
$MESS["WIZ_TSZH_BIK"] = "МФО";

$MESS["WIZ_TSZH_INN_ERROR_REQ"] = "Не заповнене поле «ІПН».";
$MESS["WIZ_TSZH_INN_ERROR_INVALID"] = "В полі «ІПН» введено некоректне значення.";

$MESS["WIZ_TSZH_IMPORT_ERROR_XML_FILE_NOT_FOUND"] = "файл з демо-даними #FILE# не найден";
$MESS["WIZ_TSZH_IMPORT_ERROR_CREATE_TABLES"] = "не вдалося створити тимчасові таблиці в базі даних";
$MESS["WIZ_TSZH_IMPORT_ERROR_XML_FILE_OPEN"] = "не вдалося відкрити файл з демо-даними #FILE# для читання";
$MESS["WIZ_TSZH_IMPORT_ERROR_READ_XML"] = "не вдалося завантажити демо-дані з xml-файлу в базу даних";
$MESS["WIZ_TSZH_IMPORT_ERROR_INDEX_TABLES"] = "не вдалося проіндексувати тимчасові таблиці бази даних";
$MESS["WIZ_TSZH_IMPORT_ERROR_PROCESS_PERIOD"] = "не вдалося створити період для демонстраційних особових рахунків";
$MESS["CITRUS_WIZARD_IMPORT_ERROR"] = "<p style=\"color: red\";>Сталася помилка при імпорті демо-даних: #ERROR#.<br>Демо-дані не завантажені.</p>";

$MESS["WIZ_STEP_TSZH"] = "Інформація про \"ТСЖ\"";
$MESS["WIZ_TSZH_SUBSCRIBE"] = "Розсилки";
$MESS["WIZ_TSZH_USE_DEBT_SUBSCRIPTION"] = "Розсилати повідомлення боржникам";
$MESS["WIZ_TSZH_DEBT_SUBCRIPTION_DESC"] = "Власникам особових рахунків, які мають заборгованість, будуть вислилаться листи з повідомленням про заборгованість.";
$MESS["WIZ_TSZH_USE_RECEIPT_SUBSCRIPTION"] = "Розсилати квитанції на оплату по електронній пошті";
$MESS["WIZ_TSZH_RECEIPT_SUBCRIPTION_DESC"] = "Квитанції на оплату будуть розсилатися власникам особових рахунків щомісяця по електронній пошті. ";
$MESS["WIZ_TSZH_MIN_DEBT"] = "Мінімальна сума заборгованості ";
$MESS["WIZ_TSZH_RECEIPT_DATE"] = "День місяця, в який будуть розсилатися квитанції ";
$MESS["WIZ_TSZH_MIN_DEBT_CURRENCY"] = "грн.";
$MESS["WIZ_COMPANY_TELEPHONE_DISP"] = "Телефон диспетчерської";
$MESS["WIZ_COMPANY_TELEPHONE_DISP_DEF"] = "0 (093) 255-55-55";
$MESS["WIZ_TSZH_DEBT_DATE"] = "День місяця для розсилки повідомлень";
$MESS["WIZ_TSZH_SUBSCRIBE_METERS_ACTIVE"] = "Розсилати повідомлення про необхідність введення показань лічильників";
$MESS["WIZ_TSZH_SUBSCRIBE_METERS_DESC"] = "У зазначений день місяця всім власникам особових рахунків будуть відправлені повідомлення з нагадуванням про необхідність ввести показання лічильників на сайті.";
$MESS["WIZ_TSZH_SUBSCRIBE_METERS_DATE"] = "День місяця для розсилки нагадувань";
$MESS["TSZH_MODULE_INCLUDE_ERROR"] = "Не встановлений модуль 1С: Сайт ЖКГ";
$MESS["TSZH_FINISH_WIZARD"] = "Закінчити роботу майстра";
$MESS["WIZ_TSZH_RECEIPT"] = "шаблон квитанції";
$MESS["WIZ_TSZH_RECEIPT_NEW"] = "За постановою № ???";
$MESS["WIZ_TSZH_RECEIPT_1161"] = "За формою ЄПД для МО №1161";
$MESS["WIZ_TSZH_RECEIPT_DEFAULT"] = "за замовчуванням";
$MESS["WIZ_TSZH_PAYMENT"] = "Оплата";
$MESS["WIZ_TSZH_PAYMENT_MONETA_ENABLED"] = "<p style=\"margin-left: 25px;\">Дозволити оплату через сервіс НКО «??????????» (ТОВ)</p>";
$MESS["WIZ_TSZH_PAYMENT_MONETA_AGREEMENT"] = "<p style=\"margin-left: 25px;\">Підтверджую свою згоду на приєднання до <a href=\"https://moneta.ru/info/public/merchants/b2boffer.pdf\" target=\"_blank\"> договором НКО « Монета.ру » (ТОВ ) </ > , з <a href=\"https://www.moneta.ru/info/public/merchants/b2btariffs.pdf\" target=\"_blank\"> Тарифами </ > і <a href=\"#moneta-procedure\" style=\"text-decoration: none; border-bottom: 1px пунктирною#2770ba\"> Порядком дій </ > ознайомлений і згоден </p>";
$MESS["WIZ_TSZH_PAYMENT_MONETA_PROCEDURE"] = "
<p><strong>Порядок дій</strong></p>
<ol>
<li><p>На вказаний Вами при установці <nobr> електронної пошти </nobr> (#EMAIL#) буде направлено лист з реквізитами доступу в Особистий кабінет НКО «Монета.ру» (ТОВ). Увійдіть в Особистий кабінет і заповніть всі необхідні поля. Після першого входу рекомендуємо змінити пароль.</p></li>
<li><p>Після заповнення та підтвердження всіх обов'язкових полів у формах Особистого кабінету - буде автоматично сформовано Заява про приєднання до умов договору. Скачайте Заява в розділі «Документи», роздрукуйте, підпишіть та надішліть на нашу адресу «Заява про приєднання» за адресою: <i>424020, Російська Федерація, Республіка Марій Ел, г. <nobr>Йошкар -Ола</nobr>, вул Анциферова, будинок 7, корпус «А» для НКО «Монета.ру» (ТОВ)</i>, телефон для кур'єрської служби <nobr>(8362) 45 - 43-83</nobr>.</p></li>
<li><p>За умовчанням доступні наступні способи: <i>банківські картки</i>, <i>Монета.ру</i>, <i>Яндекс.Деньги</i>. Якщо протягом 30 календарних днів не буде отримано письмове підтвердження згоди з умовами Договору НКО «Монета.ру» (ТОВ), він може бути розірваний.</p></li>
</ol>
<p><small>Примітка: Кошти перераховуються на ваш банківський рахунок не пізніше першого робочого дня, наступного за днем отримання НКО «Монета.ру» (ТОВ) розпорядження Платника. Винагорода за здійснення Перекладів зазначено в Тарифах. У випадку вашого приєднання до Договору НКО «Монета.ру» (ТОВ): комісія НКО «Монета.ру» (ТОВ) утримується з суми, що підлягає перерахуванню на ваш розрахунковий рахунок; комісія з Платника не утримується. Математичні інструменти Розірвання Договору з НКО «Монета.ру» (ТОВ) можливе шляхом направлення відповідного письмового повідомлення.</small></p>
";
$MESS["WIZ_TSZH_HEAD_NAME"] = "ПІБ керівника";
$MESS["WIZ_TSZH_LEGAL_ADDRESS"] = "Юридична адреса";
$MESS["WIZ_STEP_MONETA"] = "Налаштування прийому платежів через сайт";
$MESS["WIZ_STEP_ADDITIONAL_SETTINGS"] = "Інші настройки";
$MESS["WIZ_TSZH_OFFICE_HOURS"] = "Розклад роботи";

$MESS["WIZ_ERROR_SUBSCRIBE_SETTING"] = "Помилка налаштування розсилки «#SUBSCRIBE#»: #ERROR#";
