<?php
/**
 * @author  Maxim Cherepinin <m@vdgb-soft.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2018 otr@rarus.ru
 * @date 22.03.2018 12:36
 */

class SiteSettingsStep extends CSiteSettingsWizardStep
{
	function InitStep()
	{
		$wizard =& $this->GetWizard();
		$wizard->solutionName = "site_tszh";
		parent::InitStep();

		$this->SetNextStep("tszh_settings");
		$this->SetNextCaption(GetMessage("NEXT_BUTTON"));
		$this->SetTitle(GetMessage("WIZ_STEP_SITE_SET"));

		$siteID = $wizard->GetVar("siteID");
		if (COption::GetOptionString("citrus.tszh", "wizard_tszh_installed", "N") == "Y")
		{
			$this->SetNextStep("data_install");
		}
		else
		{
			$this->SetNextStep("tszh_settings");
		}

		$site = CSite::GetByID($siteID)->Fetch();
		$email = $site["EMAIL"] ? $site["EMAIL"] : COption::GetOptionString("main", "email_from", "N", $siteID);
		$wizard->SetDefaultVars(
			Array(
				"siteName" => $this->GetFileContent(WIZARD_SITE_PATH . "include/company_name.php", GetMessage("WIZ_COMPANY_NAME_DEF")),
				"siteTelephone" => $this->GetFileContent(WIZARD_SITE_PATH . "include/telephone.php", GetMessage("WIZ_COMPANY_TELEPHONE_DEF")),
				"siteTelephoneDisp" => $this->GetFileContent(WIZARD_SITE_PATH . "include/telephone_disp.php", GetMessage("WIZ_COMPANY_TELEPHONE_DISP_DEF")),
				"siteEmail" => strlen($email) > 0 ? $email : 'info@' . $_SERVER["SERVER_NAME"],
				"siteAddress" => $this->GetFileContent(WIZARD_SITE_PATH . "include/address.php", GetMessage("WIZ_COMPANY_ADDRESS_DEF")),
				"serverName" => SITE_SERVER_NAME ? SITE_SERVER_NAME : (COption::GetOptionString('main', 'server_name', $_SERVER['HTTP_HOST'])),
			)
		);
	}

	function ShowStep()
	{
		$this->content .= '<div class="wizard-input-form">';
		$this->content .= '<p>' . GetMessage("WIZ_SITE_SETTINGS_NOTE") . '</p>';
		$this->content .= '
		<div class="wizard-input-form-block">
			<h4><label for="siteName">' . GetMessage("WIZ_COMPANY_NAME") . '</label></h4>
			<div class="wizard-input-form-block-content">
				<div class="wizard-input-form-field wizard-input-form-field-text">' . $this->ShowInputField('text', 'siteName', array(
				"style" => "width:100%",
				"id" => "siteName",
			)) . '</div>
			</div>
		</div>';
		$this->content .= '
		<div class="wizard-input-form-block">
			<h4><label for="siteTelephone">' . GetMessage("WIZ_COMPANY_TELEPHONE") . '</label></h4>
			<div class="wizard-input-form-block-content">
				<div class="wizard-input-form-field wizard-input-form-field-text">' . $this->ShowInputField('text', 'siteTelephone', array(
				"style" => "width:100%",
				"id" => "siteTelephone",
			)) . '</div>
			</div>
		</div>';
		$this->content .= '
		<div class="wizard-input-form-block">
			<h4><label for="siteTelephoneDisp">' . GetMessage("WIZ_COMPANY_TELEPHONE_DISP") . '</label></h4>
			<div class="wizard-input-form-block-content">
				<div class="wizard-input-form-field wizard-input-form-field-text">' . $this->ShowInputField('text', 'siteTelephoneDisp', array(
				"style" => "width:100%",
				"id" => "siteTelephoneDisp",
			)) . '</div>
			</div>
		</div>';
		$this->content .= '
		<div class="wizard-input-form-block">
			<h4><label for="siteEmail">' . GetMessage("WIZ_COMPANY_EMAIL") . '</label></h4>
			<div class="wizard-input-form-block-content">
				<div class="wizard-input-form-field wizard-input-form-field-text">' . $this->ShowInputField('text', 'siteEmail', array(
				"style" => "width:100%",
				"id" => "siteEmail",
			)) . '</div>
			</div>
		</div>';
		$this->content .= '
		<div class="wizard-input-form-block">
			<h4><label for="siteAddress">' . GetMessage("WIZ_COMPANY_ADDRESS") . '</label></h4>
			<div class="wizard-input-form-block-content">
				<div class="wizard-input-form-field wizard-input-form-field-textarea">' . $this->ShowInputField('textarea', 'siteAddress', array(
				"style" => "width:100%",
				"rows" => "2",
				"id" => "siteAddress",
			)) . '</div>
			</div>
		</div>';
		$this->content .= '
		<div class="wizard-input-form-block">
			<h4><label for="siteAddress">' . GetMessage("WIZ_COMPANY_SERVER_NAME") . '</label></h4>
			<div class="wizard-input-form-block-content">
				<p>' . GetMessage("WIZ_COMPANY_SERVER_NAME_TIP") . '</p>
				<div class="wizard-input-form-field wizard-input-form-field-textarea">' . $this->ShowInputField('text', 'serverName', array(
				"style" => "width:100%",
				"id" => "serverName",
			)) . '</div>
				<p>' . GetMessage("WIZ_COMPANY_SERVER_NAME_TEXT") . '</p>
			</div>
		</div>';
		$this->content .= '</div>';
	}

	function OnPostForm()
	{
		$wizard =& $this->GetWizard();

		static $requiredFields = Array(
			"siteName" => "WIZ_COMPANY_NAME",
			"siteTelephone" => "WIZ_COMPANY_TELEPHONE",
			"siteTelephoneDisp" => "WIZ_COMPANY_TELEPHONE_DISP",
			"siteEmail" => "WIZ_COMPANY_EMAIL",
			"siteAddress" => "WIZ_COMPANY_ADDRESS",
			"serverName" => "WIZ_COMPANY_SERVER_NAME",
		);
		$values = array();
		foreach ($requiredFields as $field => $titleMess)
		{
			$values[$field] = trim($wizard->GetVar($field));
			if (strlen($values[$field]) <= 0)
			{
				$this->SetError(GetMessage("WIZ_COMPANY_MISSING_FIELD", array("#FIELD#" => GetMessage($titleMess))), $field);
			}
		}

		$serverName = $wizard->GetVar('serverName');
		$siteUrl = (CMain::IsHTTPS() ? "https" : "http") . '://' . $serverName;
		if (preg_match('#^(http|https)://([A-Z0-9][A-Z0-9_-]*(?:.[A-Z0-9][A-Z0-9_-]*)+):?(d+)?/?$#i', $siteUrl) !== 1)
		{
			$this->SetError(GetMessage("WIZ_COMPANY_SERVER_NAME_WRONG"), "serverName");
		}
		if (preg_match('#^www\.#i', $serverName))
		{
			$this->SetError(GetMessage("WIZ_COMPANY_SERVER_NAME_WRONG"), "serverName");
		}

		if (!check_email($wizard->GetVar('siteEmail')))
		{
			$this->SetError(GetMessage("WIZ_COMPANY_SITE_EMAIL_WRONG"), "siteEmail");
		}
	}
}