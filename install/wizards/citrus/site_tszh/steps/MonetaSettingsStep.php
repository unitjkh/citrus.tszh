<?php
/**
 * @author  Maxim Cherepinin <m@vdgb-soft.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2018 otr@rarus.ru
 * @date 22.03.2018 12:37
 */

use Citrus\Tszh\TszhTable;

class MonetaSettingsStep extends CWizardStep
{
	function InitStep()
	{
		$this->SetStepID("moneta_settings");
		$this->SetTitle(GetMessage("WIZ_STEP_MONETA"));
		$this->SetNextStep("additional_settings");
		$this->SetPrevStep("tszh_settings");
		$this->SetNextCaption(GetMessage("NEXT_BUTTON"));
		$this->SetPrevCaption(GetMessage("PREVIOUS_BUTTON"));

		$wizard =& $this->GetWizard();
		if ($wizard->GetVar("MONETA_ENABLED", true) == "Y" && $wizard->GetVar("MONETA_OFFER", true) == "Y")
		{
			$wizard->SetDefaultVar("MONETA_SCHEME", 1);
		}
		else
		{
			$wizard->SetDefaultVar("MONETA_SCHEME", 2);
		}

		$wizard =& $this->GetWizard();
		$wizard->solutionName = "site_tszh";
	}

	function ShowStep()
	{
		$wizard =& $this->GetWizard();

		// ����� �� ����������� email
		$contact_email = $wizard->GetVar('siteEmail', true);

		$wizard->SetDefaultVar('MONETA_EMAIL', $contact_email);
		$wizard->SetVar('MONETA_EMAIL', $contact_email);

		$MonetaUsageTableHtml = "";

		if (CModule::IncludeModule("citrus.tszhpayment"))
		{
			$monetaEnabled = $wizard->GetVar("MONETA_ENABLED", true);

			$siteID = $wizard->GetVar("siteID");

			if ($siteID !== null)
			{
				$arTszh = CTszh::GetList(Array(), Array("SITE_ID" => $siteID), false, false, Array("ID", "MONETA_ENABLED"))->Fetch();
				if (is_array($arTszh) && !empty($arTszh))
				{
					$monetaEnabled = $arTszh["MONETA_ENABLED"];
				}
			}

			$demoFlag = \CTszhPaymentGateway::isDemo();
		}

		if ($demoFlag && function_exists('tszhMonetaDemoNotice'))
		{
			$this->content .= tszhMonetaDemoNotice($wizard->GetCurrentStep());
		}
		else
		{
			$wizard->SetVar('MONETA_ENABLED', 'Y');
			$wizard->SetVar('MONETA_NO_OFFER', 'Y');
			$this->content .=
				'<div class="wizard-input-form">
				<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
				<div class="wizard-input-form-block-content">
					<div class="wizard-input-form-block">
						<h4><label for="MONETA_EMAIL">' . GetMessage("WIZ_TSZH_MONETA_EMAIL") . '</label></h4>
						<div class="wizard-input-form-block-content">
							<div class="wizard-input-form-field wizard-input-form-field-text">'
				. $this->ShowInputField('text', 'MONETA_EMAIL', array("style" => "width:100%", "id" => "MONETA_EMAIL")) . '
							</div>
								<span style="display: block; width: 90%; font-size: 12px; color: #454545;" class="wizard-input-form-field-info">' . GetMessage("WIZ_TSZH_MONETA_EMAIL_INFO") . '</span>
						</div>
						<div>
					</div>
				</div>
			</div>' .
				'<div style="border: 1px dashed #6fbae9; padding: 20px 20px 20px 70px; background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAcCAYAAAByDd+UAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA+tpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNSAoV2luZG93cykiIHhtcDpDcmVhdGVEYXRlPSIyMDE3LTExLTI4VDExOjQwOjM2KzAzOjAwIiB4bXA6TW9kaWZ5RGF0ZT0iMjAxNy0xMS0yOFQxNjozNToyNiswMzowMCIgeG1wOk1ldGFkYXRhRGF0ZT0iMjAxNy0xMS0yOFQxNjozNToyNiswMzowMCIgZGM6Zm9ybWF0PSJpbWFnZS9wbmciIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6RkUzM0IwNjlENDQwMTFFNzk4NDBDMUUxMzE4MTExNDQiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6RkUzM0IwNkFENDQwMTFFNzk4NDBDMUUxMzE4MTExNDQiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpGRTMzQjA2N0Q0NDAxMUU3OTg0MEMxRTEzMTgxMTE0NCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpGRTMzQjA2OEQ0NDAxMUU3OTg0MEMxRTEzMTgxMTE0NCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PpL7HIoAAANfSURBVHjarJZtSFNRGMefc+/d1NrmS4ouSUoqsDKt/KBUEERCQUEfBDUsgvyiUJEkQdSHgl6EoOjtY2AQglCB1gcVe4FQRChL1LVwmNrmtrbUldvd3V3PXWe6d+9te+DP2e459/zO85xzz/MQWMV29No4bKpRR1CVqC0oHe1eRBlRA6jXqJ7RQ3lCovlIApAKm2ZUK0oP8uwHqg31CMFe2UCElWPzDFUC/2fjqHqEforsYGLAarAZTAIG9N1BOld8D+mAjlgL+U8TUbXoaWcUkIZR8iwNUmse6bAFw0tCDshIkmEMMw1DgEUH50US3NMy6SAFQ9eUStjmNAbqMnmwz3wHv5cP7mlzwEP6nU2h1qcCVp2tgrpCFhp7J2DRJwLHsMDl6YGo1GbsLgp+1CmBnd2QDjUbM+Bk92gAJpkg+gBsZgmqR2g1R2+QpCwT9+t6iQYOFKTBxT4DjC16wvqDUCZ73VEJWJUMbFs6Czd36qBYx8KTETO8nJ2POS4Addr3cvRuXDbpgSATdjhHDVdKNaBVMTAwuwC3P06v8lWKm6RTqg19puMItOI+5HEk4bvnijLg1i5dAGbGELa8M8pZqCbqRnEIfjC6fNBZmQXHc9VRb2SxBB6XauHM1rWAWwduQYSW/q9g5X2yoiIBFyIfvrDz8NkpwLVyXWDyIvW/dZVksPC0IhP25a9cRm0fJmHIuSR3y39zNJ/tiey5YXBBWY4qMHkHtl3TbjiGx16jWgnK8zELtJscsg8YQ4iJofdnlFkwtHfHXYHf0j7VF68Jg32xuuDq8LSym5yQIYZm6pgmhfaN2RP13LHkhQtvjeAW/YqADGFeScAelDneICm0Do+4/N+HkEv9RjD98SqEkTmf61cXQ2uQtngDQ0Mr2f2hKei3uZQnRkLuTTYdXM4WD2kKiRva93Me6Jt0wIMJq2IYevdN9PJ3FCXgQrwInJYZ+On2KOXxHKfab2jYPRRW09CM3EDLgiibxdC6svNBrVZUEIiYnhqDsKgiitYetbQsiA4NywGbWyAXyiPstOFURXvyZSLe/D67BXjeE3fPcHEnQj2LWyaGhLcMdZ4Wt5GuxvQUQVZgmMuC4N0eC5aw8o5T6lfRdKalni7wTptJ9PuH8V93eq6+e7VS/68AAwBZLTwr8V/S3gAAAABJRU5ErkJggg==); background-repeat: no-repeat; background-position: 20px 20px;">
				<div style="font-size: 14px; color: #6fbae9;">' . GetMessage("WIZ_MONETA_ENABLED_AUTO_INFO") . '</div>
				<div style="clear: both; width: 100%;"></div>
				<div>' . GetMessage("WIZ_MONETA_ENABLED_HOW_TO_DISABLE") . '</div>';
		}


	}

	function OnPostForm()
	{
		$wizard =& $this->GetWizard();

		if (CModule::IncludeModule("citrus.tszhpayment")
		    && !CTszhFunctionalityController::isUkrainianVersion()
		    && method_exists("CTszhPaymentGateway", "isDemo")
		    && !CTszhPaymentGateway::isDemo()
		    && function_exists("tszhPaymentMonetaUsageTable"))
		{
			$monetaEnabled = $wizard->GetVar("MONETA_ENABLED", true);
			$monetaEmail = $wizard->GetVar("MONETA_EMAIL", true);
			$monetaScheme = $wizard->GetVar("MONETA_SCHEME", true);

			if (!check_email($wizard->GetVar('MONETA_EMAIL', false)))
			{
				$this->SetError(GetMessage("WIZ_COMPANY_SITE_EMAIL_WRONG"), "MONETA_EMAIL");
			}

			$RSCH = $wizard->GetVar('org_rsch');
			if (method_exists('\Citrus\Tszh\TszhTable', 'isRschCorrect') && !TszhTable::isRschCorrect($RSCH))
			{
				$wizard->SetVar('MONETA_ENABLED', 'N');
				$wizard->SetVar('MONETA_NO_OFFER', 'N');
			}

			/*if ($monetaEnabled == "Y")
			{
				if (!in_array($monetaScheme, array(1, 2)))
				{
					$this->SetError(GetMessage("WIZ_ERROR_REQ_MONETA_SCHEME"), "REQ_MONETA_SCHEME");
				}

				if (!check_email($monetaEmail))
				{
					$this->SetError(GetMessage("WIZ_ERROR_REQ_MONETA_EMAIL"), "REQ_MONETA_EMAIL");
				}
			}*/
		}
	}
}