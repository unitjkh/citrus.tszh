<?php
/**
 * @author  Maxim Cherepinin <m@vdgb-soft.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2018 otr@rarus.ru
 * @date 22.03.2018 12:34
 */

class SelectTemplateStep extends CSelectTemplateWizardStep
{
	function InitStep()
	{
		$wizard =& $this->GetWizard();

		$wizard->solutionName = "site_tszh";
		parent::InitStep();
	}

	function ShowStep()
	{
		$wizard =& $this->GetWizard();
		if (!CModule::IncludeModule('citrus.tszh'))
		{
			$this->SetPrevStep(false);

			$this->SetStepID("finish");
			$this->SetNextStep("finish");
			$this->SetNextCaption(GetMessage("TSZH_FINISH_WIZARD"));

			$this->SetError(GetMessage("TSZH_MODULE_INCLUDE_ERROR"));

			$wizard->SetFormActionScript('/bitrix/admin/');

		}
		else
		{
			$tpl = COption::GetOptionString("main", "wizard_template_id", "citrus_tszh_adaptive", $wizard->GetVar("siteID"));
			$wizard->SetVar('templateID', $tpl);
			parent::ShowStep();

			/** @var CWizardStep $this */
			if ($GLOBALS["APPLICATION"]->GetCurPage(false) != '/')
			{
				ob_start();
				?>
				<div class="inst-note-block inst-note-block-yellow">
					<div class="inst-note-block-icon"></div>
					<div class="inst-note-block-label" style="font-size: 14px;"><?=GetMessage("WIZ_REWRITE_WARNING")?></div>
					<br>
					<div class="inst-note-block-text"><br><?=GetMessage("WIZ_REWRITE_WARNING_TEXT")?></div>
				</div>
				<?
				$this->content = ob_get_contents() . $this->content;
				ob_end_clean();
			}
		}
	}
}