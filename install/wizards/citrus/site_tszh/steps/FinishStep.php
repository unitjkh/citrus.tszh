<?php
/**
 * @author  Maxim Cherepinin <m@vdgb-soft.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2018 otr@rarus.ru
 * @date 22.03.2018 12:24
 */
class FinishStep extends CFinishWizardStep
{
	function ShowStep()
	{
		parent::ShowStep();

		$wizard =& $this->GetWizard();

		if (WIZARD_SITE_DIR === '/')
		{
			$this->content .= GetMessage("CITRUS_WIZARD_SITEMAP_INFO");
		}

		$importError = $_SESSION[$wizard->solutionName]["importError"];
		if (strlen($importError))
		{
			$this->content .= GetMessage("CITRUS_WIZARD_IMPORT_ERROR", array("#ERROR#" => $importError));
		}
	}
}