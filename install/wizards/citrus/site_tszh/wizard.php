<? require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/install/wizard_sol/wizard.php");

require_once "steps/SelectSiteStep.php";
require_once "steps/SelectTemplateStep.php";
require_once "steps/SelectThemeStep.php";
require_once "steps/SiteSettingsStep.php";
require_once "steps/TszhSettingsStep.php";
require_once "steps/MonetaSettingsStep.php";
require_once "steps/AdditionalSettingsStep.php";
require_once "steps/DataInstallStep.php";
require_once "steps/FinishStep.php";

require_once "functions.php";