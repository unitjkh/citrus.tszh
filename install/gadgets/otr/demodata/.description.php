<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

use Bitrix\Main\Localization\Loc;

$arDescription = array(
	"NAME" => Loc::getMessage("OTR_GADGETS_DEMODATA_NAME"),
	"DESCRIPTION" => Loc::getMessage("OTR_GADGETS_DEMODATA_DESCRIPTION"),
	"ICON" => "",
	"TITLE_ICON_CLASS" => "otr-gadgets-demodata",
	"GROUP" => Array("ID" => "other"),
	"NOPARAMS" => "Y",
	"AI_ONLY" => true,
	"COLOURFUL" => true,
);