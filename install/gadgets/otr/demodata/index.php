<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

use Bitrix\Main\Localization\Loc;

$APPLICATION->SetAdditionalCSS('/bitrix/gadgets/otr/demodata/styles.min.css');
?>
<div class="otr-gadget-demodata">
	<div class="otr-gadget-demodata__header"><?= Loc::getMessage("OTR_GADGETS_DEMODATA_HEADER") ?></div>
	<div class="otr-gadget-demodata__text"><?= Loc::getMessage("OTR_GADGETS_DEMODATA_TEXT") ?></div>
	<a class="demodata-button" href="/bitrix/admin/tszh_demodata.php?lang=ru">
		<div class="demodata-button__text"><?= Loc::getMessage("OTR_GADGETS_DEMODATA_BUTTON_TEXT") ?></div>
	</a>
</div>

