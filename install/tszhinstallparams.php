<?php

trait TszhInstallParams
{
	private static $install_params = array();

	public function setInstallParam($param, $value)
	{
		self::$install_params[$param] = $value;

		return $this;
	}

	public function getInstallParam($param)
	{
		return isset(self::$install_params[$param]) ? self::$install_params[$param] : false;
	}

}