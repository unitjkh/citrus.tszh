<?php

use Bitrix\Main\Localization\Loc;

?>
<div class="otr-module-install">
	<form action="<? echo $APPLICATION->GetCurPage() ?>">
		<?=bitrix_sessid_post()?>
		<input type="hidden" name="lang" value="<? echo LANG ?>">
		<input type="hidden" name="id" value="citrus.tszh">
		<input type="hidden" name="uninstall" value="Y">
		<input type="hidden" name="step" value="2">
		<? echo CAdminMessage::ShowMessage(Loc::getMessage('MOD_UNINST_WARN')) ?>
		<h4><? echo Loc::getMessage('MOD_UNINST_SAVE') ?></h4>
		<p>
			<input type="checkbox" name="save_tables" id="save_tables" value="1" checked>
			<label for="save_tables"><? echo Loc::getMessage('MOD_UNINST_SAVE_TABLES') ?></label>
		</p>
		<p>
			<input type="checkbox" name="save_events" id="save_events" value="1" checked>
			<label for="save_events"><? echo Loc::getMessage('MOD_UNINST_SAVE_EVENTS') ?></label>
		</p>
		<input type="submit" name="inst" value="<? echo Loc::getMessage('MOD_UNINST_DEL') ?>">
	</form>
</div>

<style>
	.otr-module-install {
		background-color: #fff;
		padding: 20px;
		border-radius: 5px;
		border: 1px solid rgba(0, 0, 0, .2);
		font-size: 14px;
	}
</style>