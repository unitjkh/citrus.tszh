BX.ready(function() {
    function init(){
        $(".tabs").each(function () {
            var tabs = $(this);
            tabs.children("input,input + label").wrapAll("<div class='tabs__radio tabs__radio-decorate'></div>");
            var tabsRadio = $(this).find(".tabs__radio");
            var tab = tabsRadio.find("input + label:nth-of-type(n+2)").first();
            var labels = tabsRadio.find("input + label");
            var labelsWidth = 0, paddingDecorate = 20;
            labels.each(function () {
                var w = $(this).width();
                $(this)[0].minwidth = w;
                $(this)[0].paddingLeft = parseInt($(this).css("padding-left").replace("px",""));
                labelsWidth += w + ($(this)[0].paddingLeft + 1) * 2;
            });
            function resize(){
                var tabsWidth = tabs.width();
                if(tab.length > 0) {
                    if(labelsWidth > tabsWidth){
                        tabs.addClass("more1line");
                    }
                    else{
                        tabs.removeClass("more1line");
                    }
                }
                if(tabs.hasClass("more1line")){
                    labels.each(function () {
                        $(this).css("width", tabsWidth - ($(this)[0].paddingLeft + paddingDecorate)*2);
                    })
                }
                else{
                    var incWidth = (tabsWidth - labelsWidth) / labels.length;
                    var i = 0;
                    labels.each(function () {
                        i++;
                        $(this).css("width",$(this)[0].minwidth + incWidth);
                        if(i==labels.length && (tabs[0].getBoundingClientRect().right - $(this)[0].getBoundingClientRect().right > 1 )){
                            $(this).css("width",$(this).width() - labels.length);
                        }
                    })
                }
            }
            $(window).resize(function() {
                resize();
            });
            resize();
            function radioChange(){
                tabs.find("section").hide();
                var index = 1;
                tabsRadio.find("input").each(function () {
                    if($(this).attr("checked") !== undefined){
                        if($.isNumeric($(this).val())){
                            index = $(this).val();
                        }
                        return false;
                    }
                    index++;
                });
                var section = tabs.find('section[sectionid="' + index + '"]');
                if(section.length == 1){
                    section.show();
                }
                else{
                    tabs.find("section:nth-of-type(" + index + ")").show();
                }
            }
            radioChange();
            tabsRadio.find("input").change(function () {
                radioChange();
            });
        });
    }
    init();
    BX.addCustomEvent('onAjaxSuccess', function(){
        init();
    });
});
