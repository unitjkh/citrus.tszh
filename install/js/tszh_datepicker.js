/*** ������� ��������� start ***/
function Calendar(element, nameclass, year, month, day) {
    year = parseInt(year);
    month = parseInt(month);
    var Dlast = new Date(year,month+1,0).getDate(),
        D = new Date(year,month,Dlast),
        DNlast = D.getDay(),
        DNfirst = new Date(D.getFullYear(),D.getMonth(),1).getDay(),
        calendar = '<tr>',
        //m = document.querySelector('.'+nameclass+'>div>table option[value="' + D.getMonth() + '"]'),
        m = element.find('div>table option[value="' + D.getMonth() + '"]')[0],
        //g = document.querySelector('.'+nameclass+'>div>table input');
        g = element.find('div>table input')[0];
    if(day>Dlast)
        day = Dlast;
    if (DNfirst != 0) {
        for(var  i = 1; i < DNfirst; i++) calendar += '<td>';
    }else{
        for(var  i = 0; i < 6; i++) calendar += '<td>';
    }
    //alert(D.getFullYear() == new Date().getFullYear() && D.getMonth() == new Date().getMonth());
    for(var  i = 1; i <= Dlast; i++) {
        var classDaySelected = year == D.getFullYear() && month == D.getMonth() && day == i ? ' ' + nameclass +  '__day-selected' : '';
        if (i == new Date().getDate() && D.getFullYear() == new Date().getFullYear() && D.getMonth() == new Date().getMonth()) {
            calendar += '<td class="' + nameclass +  '__today ' + nameclass +  '__day'+ classDaySelected +'">' + i;
        }else{
            if (  // ������ ����������� ����������
            (i == 1 && D.getMonth() == 0 && ((D.getFullYear() > 1897 && D.getFullYear() < 1930) || D.getFullYear() > 1947)) || // ����� ���
            (i == 2 && D.getMonth() == 0 && D.getFullYear() > 1992) || // ����� ���
            ((i == 3 || i == 4 || i == 5 || i == 6 || i == 8) && D.getMonth() == 0 && D.getFullYear() > 2004) || // ����� ���
            (i == 7 && D.getMonth() == 0 && D.getFullYear() > 1990) || // ��������� ��������
            (i == 23 && D.getMonth() == 1 && D.getFullYear() > 2001) || // ���� ��������� ���������
            (i == 8 && D.getMonth() == 2 && D.getFullYear() > 1965) || // ������������� ������� ����
            (i == 1 && D.getMonth() == 4 && D.getFullYear() > 1917) || // �������� ����� � �����
            (i == 9 && D.getMonth() == 4 && D.getFullYear() > 1964) || // ���� ������
            (i == 12 && D.getMonth() == 5 && D.getFullYear() > 1990) || // ���� ������ (���������� � ��������������� ������������ ���������� ��������� ������������ ������������� ������ ����)
            (i == 7 && D.getMonth() == 10 && (D.getFullYear() > 1926 && D.getFullYear() < 2005)) || // ����������� ��������� 1917 ����
            (i == 8 && D.getMonth() == 10 && (D.getFullYear() > 1926 && D.getFullYear() < 1992)) || // ����������� ��������� 1917 ����
            (i == 4 && D.getMonth() == 10 && D.getFullYear() > 2004) // ���� ��������� ��������, ������� ������� ����������� ��������� 1917 ����
            ) {
                calendar += '<td class="' + nameclass +  '__holiday ' + nameclass +  '__day'+ classDaySelected +'">' + i;
            }else{
                calendar += '<td class="' + nameclass +  '__day'+ classDaySelected +'">' + i;
            }
        }
        if (new Date(D.getFullYear(),D.getMonth(),i).getDay() == 0 && i < Dlast) {
            calendar += '<tr>';
        }
    }
    element.find('div>table tbody')[0].innerHTML = calendar;
    g.value = D.getFullYear();

    m.selected = true;
    element.find('.' + nameclass + '__month').text(m.innerText);
    element.find('div>table option[value="' + new Date().getMonth() + '"]')[0].style.color = 'rgb(220, 0, 0)'; // � ���������� ������ ������� ������� �����
    element.find('input[name="date-value"]')[0].value = day + '.' + month + '.' + year;
    element.find("." + nameclass + "__day").click(function(){
        Calendar(element, nameclass, year, month, $(this).text());
    });
}

/*** ������� ��������� end ***/

function lpad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

BX.ready(function() {
    BX.tszh = {
        datepicker: {
            init : function () {
                var calendarClass = "tszh-datepicker";
                var calendarElement = $('.' + calendarClass);
                if (calendarElement.length) {
                    calendarElement.each(function () {
                        var element = $(this);
                        if(element.attr("init") != null)
                            return;
                        element.attr("init",true);
                        var valueDate = element.text().trim(), arDate = valueDate.split(' ');
                        var wordMonth = true;// wordMonth = true- ����� �������; wordMonth = false - ����� ������
                        if (arDate.length < 2) {
                            arDate = valueDate.split('.');
                            if (arDate.length > 1)
                                wordMonth = false;
                        }
                        var inputElement = element.children("input");
                        if (arDate.length > 1 && arDate.length < 4) {
                            var day = arDate.length == 3 ? arDate[0] : 1;
                            var months = JSON.parse(BX.message('JS_MONTHS_ARRAY'));
                            var daysOfWeek = JSON.parse(BX.message('JS_DAYS_WEEK'));
                            var monthsOption = '', idMonth = -1, typeMonth = -1,
                                monthCur = arDate[arDate.length - 2].toLowerCase();
                            months.forEach(function (item, i) {
                                monthsOption += '<option value="' + i + '">' + item[0].substr(0, 1).toUpperCase() + item[0].substr(1) + '</option>';
                                if (wordMonth && typeMonth === -1) {
                                    typeMonth = $.inArray(monthCur, item);
                                    if (typeMonth > -1)
                                        idMonth = i;
                                }
                            });
                            if (!wordMonth)
                                idMonth = arDate[arDate.length - 2] - 1;
                            if (idMonth > -1) {
                                var htmlParams = '';
                                var parentForm = element.closest("form");
                                if(parentForm.length == 1 && parentForm.attr("method") !== undefined && parentForm.attr("method").toLowerCase()=="get"){
                                    var ignoreParams = ["period1","period2","date-value"];
                                    var getParams = getUrlVars();
                                    console.log(getParams);
                                    $.each(getParams, function(key, value) {
                                        if($.inArray(key, ignoreParams) == -1 && $.inArray(key, parentForm[0].inputs) == -1){
                                                console.log(key,value);
                                                if(parentForm[0].inputs === undefined){
                                                    parentForm[0].inputs = [];
                                                }
                                                parentForm[0].inputs.push(key);
                                                htmlParams +='<input type="hidden" name="' + key + '" + value="' + value + '" />';
                                        }
                                    });
                                }
                                inputElement[0].value = (arDate.length == 3 ? +arDate[0] + '.' : '' ) + (idMonth + 1) + '.' + arDate[arDate.length - 1];
                                element.html(element.html() + htmlParams + '<div><input name="date-value" type="hidden"/><table>\
                                                 <thead>\
                                                 <tr><td colspan="7"><div class="select1 ' + calendarClass + '__select"><select>' + monthsOption + '</select></div><input type="number" value="" min="0" max="9999" size="4"></td></tr>\
                                                 <tr><td class="' + calendarClass + '__month-left"><span></span></td><td colspan="5" class="' + calendarClass + '__month"></td><td class="' + calendarClass + '__month-right"><span></span></td></tr>\
                                                    <tr><td>' + daysOfWeek[0] + '</td><td>' + daysOfWeek[1] + '</td><td>' + daysOfWeek[2] + '</td><td>' + daysOfWeek[3] + '</td><td>' + daysOfWeek[4] + '</td><td>' + daysOfWeek[5] + '</td><td>' + daysOfWeek[6] + '</td></tr>\
                                                </thead>\
                                                <tbody></tbody>\
                                            </table><div class="' + calendarClass + '__input"><input type="button" name="cancel" value="' + BX.message('JS_CANCEL') + '"/><input type="button" tabindex="0" name="select" value="' + BX.message('JS_CHOOSE') + '"/></div></div>');
                                Calendar(element, calendarClass, arDate[arDate.length - 1], idMonth, day);

                                element.find("div>table")[0].onchange = function () {
                                    var arDateValue = element.find('input[name="date-value"]')[0].value.split('.');
                                    Calendar(element, calendarClass, $(this).find('input')[0].value, $(this).find('select')[0].options[$(this).find('select')[0].selectedIndex].value, arDateValue[0]);
                                }
                                element.find("." + calendarClass + "__month-left").click(function () {
                                    var arDateValue = element.find('input[name="date-value"]')[0].value.split('.');
                                    if (--arDateValue[1] < 0) {
                                        arDateValue[1] = 11;
                                        arDateValue[2]--;
                                    }
                                    Calendar(element, calendarClass, arDateValue[2], arDateValue[1], arDateValue[0]);
                                });
                                element.find("." + calendarClass + "__month-right").click(function () {
                                    var arDateValue = element.find('input[name="date-value"]')[0].value.split('.');
                                    if (++arDateValue[1] > 11) {
                                        arDateValue[1] = 0;
                                        arDateValue[2]++;
                                    }
                                    Calendar(element, calendarClass, arDateValue[2], arDateValue[1], arDateValue[0]);
                                });
                                var divEl = element.children('div');

                                element.find("." + calendarClass + '__input input[name="select"]').click(function () {
                                    var elemValue = element.find("." + calendarClass + '__value');
                                    var arDateValue = element.find('input[name="date-value"]')[0].value.split('.');
                                    elemValue.text((arDate.length > 2 ? (!wordMonth ? lpad(parseInt(arDateValue[0]), 2) : arDateValue[0]) + (wordMonth ? ' ' : '.') : '') + (wordMonth ? months[arDateValue[1]][typeMonth] + ' ' : lpad(parseInt(arDateValue[1]) + 1, 2) + '.') + arDateValue[2]);
                                    idMonth = arDateValue[1];
                                    arDate[arDate.length - 1] = arDateValue[2];
                                    day = arDate.length > 2 ? arDateValue[0] : 1;
                                    element.children("input")[0].value = (arDate.length > 2 ? arDateValue[0] + '.' : '') + (parseInt(arDateValue[arDateValue.length - 2]) + 1) + "." + arDateValue[arDateValue.length - 1];
                                    element.removeClass(calendarClass + '__show-item');
                                    if (inputElement.attr("submit") == "true") {
                                        element.parent().parent()[0].submit();
                                    }
                                });

                                element.find("." + calendarClass + '__input input[name="cancel"]').click(function () {
                                    element.removeClass(calendarClass + '__show-item');
                                });

                                element.find("." + calendarClass + '__value').click(function () {
                                    if (!element.hasClass(calendarClass + '__show-item')) {
                                        //������������ �������������� ������� ��������� ������������ ������������� ��������
                                        if (element.offset().left + divEl.width() > element.parent().offset().left + element.parent().outerWidth()) {
                                            divEl.css('left', element.parent().offset().left + element.parent().outerWidth() - divEl.width())
                                        }

                                        if (element.offset().left + divEl.width() > element.parent().offset().left + element.parent().width())
                                            Calendar(element, calendarClass, arDate[arDate.length - 1], idMonth, day);
                                        element.addClass(calendarClass + '__show-item');
                                    }
                                    else {
                                        element.removeClass(calendarClass + '__show-item');
                                    }
                                });

                                $(document).mouseup(function (e) { // ������� ����� �� ���-���������
                                    if (!element.is(e.target) // ���� ���� ��� �� �� ������ �����
                                        && element.has(e.target).length === 0) { // � �� �� ��� �������� ���������
                                        //divEl.hide(); // �������� ���
                                        element.removeClass(calendarClass + '__show-item');
                                    }
                                });
                            }
                        }
                    })
                }
            }
        }
    }
    BX.tszh.datepicker.init();
    BX.addCustomEvent('onAjaxSuccess', function(){
        BX.tszh.datepicker.init();
    });
});

// ��������� GET ���������� �� URL �������� � ���������� �� ��� ������������� ������.
function getUrlVars()
{
    var vars = {}, hash;
    if(window.location.href.indexOf('?') > -1){
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for(var i = 0; i < hashes.length; i++)
        {
            hash = hashes[i].split('=');
            //vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
    }
    return vars;
}