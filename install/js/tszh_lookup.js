var script, arSrc, arTmp;

function TszhLookupCheckArray(ar, length)
{
	var result = false;

	if (ar.length == length)
	{
		result = true;
		for (var i in ar)
		{
			if (ar[i].length <= 0)
			{
				result = false;
				break;
			}
		}
	}

	return result;
}

if ((script = document.getElementById("tszh_lookup_js")) && script.src.length > 0 
	&& (arSrc = script.src.split('?')) && TszhLookupCheckArray(arSrc, 2)
	&& (arTmp = arSrc[1].split('&')) && TszhLookupCheckArray(arTmp, 2))
{
	var id, html;
	for (var i in arTmp)
	{
		var arParam = arTmp[i].split('=');
		if (TszhLookupCheckArray(arParam, 2))
		{
			switch (arParam[0])
			{
				case 'id':
					id = arParam[1];
					break;

				case 'html':
					html = arParam[1];
					break;
			}
		}
	}

	var elem;
	if (id && html && (elem = window.parent.document.getElementById(id)))
	{
		elem.innerHTML = decodeURIComponent(html);
	}
}