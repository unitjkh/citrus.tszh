<?php
/**
 * @author  Maxim Cherepinin <m@vdgb-soft.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2017 otr@rarus.ru
 * @date 15.09.2017 11:19
 */

$MESS['TSZH_MONETA_USAGE_TABLE_ERROR_ENTITY_TYPE_NOT_SET'] = 'Не установлен тип сущности в настройках компонента';
$MESS['TSZH_MONETA_USAGE_TABLE_ERROR_ENTITY_ID_NOT_SET'] = 'Не указан идентификатор сущности';
$MESS['TSZH_MONETA_USAGE_TABLE_ERROR_INCORRECT_MONETA_EMAIL'] = 'E-mail для взаимодействия с сервисом Монета.ру не указан';
$MESS["TSZH_MONETA_USAGE_TABLE_ERROR_INCORRECT_RSCH"] = "Подключение оплаты через Монета.ру невозможно для указанного расчетного счета";