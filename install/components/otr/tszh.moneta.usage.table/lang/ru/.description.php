<?php
/**
 * @author  Maxim Cherepinin <m@vdgb-soft.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2017 otr@rarus.ru
 * @date 15.09.2017 9:46
 */

$MESS['TSZH_MONETA_USAGE_TABLE_COMPONENT'] = 'Подключение платежной системы Монета.ру';
$MESS['TSZH_MONETA_USAGE_TABLE_COMPONENT_DESCRIPTION'] = 'Таблица с настройками подключения к платежной системе Монета.ру, используется в мастере и административной части';
$MESS['SITE_TSZH_PATH_NAME'] = 'Сайт ЖКХ';
$MESS['SITE_TSZH_PATH_NAME_CHILD_SERVICE'] = 'Служебные';