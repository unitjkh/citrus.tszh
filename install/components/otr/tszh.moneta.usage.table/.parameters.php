<?php
/**
 * @author  Maxim Cherepinin <m@vdgb-soft.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2017 otr@rarus.ru
 * @date 15.09.2017 9:42
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

if (!Loader::includeModule('citrus.tszh'))
{
	ShowError(Loc::getMessage('TSZH_MODULE_NOT_INSTALLED'));
}

$arComponentParameters = array(
	'GROUPS' => array(
	),
	'PARAMETERS' => array(
		'ENTITY_TYPE' => array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('MONETA_USAGE_TABLE_PARAMS_ENTITY_TYPE'),
			'TYPE' => 'LIST',
			'VALUES' => array(
				'tszh' => Loc::getMessage('MONETA_USAGE_TABLE_PARAMS_ENTITY_TYPE_VALUES_TSZH'),
				'house' => Loc::getMessage('MONETA_USAGE_TABLE_PARAMS_ENTITY_TYPE_VALUES_HOUSE'),
			),
			'REFRESH' => 'N',
			'MULTIPLE' => 'N',
			'DEFAULT' => 'tszh'
		),
		'ENTITY_ID' => array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('MONETA_USAGE_TABLE_PARAMS_ENTITY_ID'),
			'TYPE' => 'STRING',
			'REFRESH' => 'N',
			'MULTIPLE' => 'N',
			'DEFAULT' => ''
		),
		'HAS_ERRORS' => array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('MONETA_USAGE_TABLE_PARAMS_HAS_ERRORS'),
			'TYPE' => 'CHECKBOX',
			'REFRESH' => 'N',
			'DEFAULT' => 'N'
		),
		'DISABLE_OFFER' => array(
			'PARENT' => 'BASE',
			'NAME' => Loc::getMessage('MONETA_USAGE_TABLE_PARAMS_DISABLE_OFFER'),
			'TYPE' => 'CHECKBOX',
			'REFRESH' => 'N',
			'DEFAULT' => 'N'
		),
		// 'AJAX_MODE' => array(),
	),
);