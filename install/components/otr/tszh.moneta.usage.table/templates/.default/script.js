'use strict';

function initBindEvents()
{
    // ������� �� ������ �����������
    BX.bindDelegate(
        BX('moneta-usage-form'),
        'click',
        {
            tagName: 'button',
            class_name: 'moneta-usage-block__activate-button'
        },
        function (e) {
            e.preventDefault();
            // ���������� �����
            activateMotena(e);
        }
    );

    // ������� �� ������ �������������� ������
    BX.bind(BX('addititional-request'), 'click', function () {
        checkScheme(BX(this));
    });

    // ������� �� ������ "���������"
    BX.bind(BX('statement-request'), 'click', function () {
        checkStatement(BX(this));
    });

    // ������� �� ������ �����������
    /*BX.bind(BX('recepient'), 'click', function () {
        checkScheme(BX(this));
    });*/
}

function activateMotena(e) {

    // ��������� ��� ������� ������
    const type = e.target.id;

    if (type === 'recepient' && !arResult.MONETA_OFFER_ADDITITIONAL_DATA)
    {
        checkScheme(BX(this), true);
    }
    else
    {
        const moneta_block = BX.findParent(e.target, {class: 'moneta-usage-block'});
        const bg = BX.create(
            'div',
            {
                attrs: {
                    className: 'moneta-usage-loader'
                },
            }
        );
        BX.prepend(bg, moneta_block);
        BX.showWait(moneta_block);

        // showLoader(moneta_block);

        BX.ajax.post(
            // window.location.href,
            componentPath + '/ajax.php',
            {
                sessid: BX.message('bitrix_sessid'),
                ID: arParams.ENTITY_ID,
                MONETA_OFFER: (type === 'recepient' ? 'Y' : 'N'),
                MONETA_NO_OFFER: (type === 'payer' ? 'Y' : 'N')
            },
            function (data) {
                BX.remove(bg);
                BX.closeWait(moneta_block);
                hideLoader(moneta_block);
                const parent = BX.findParent(BX("moneta-usage-form"), {"tag": "td"});
                BX.remove(BX("moneta-usage-form"));
                // ���������� ������ � ����
                parent.innerHTML = parent.innerHTML + data;
                // ����� ���������� �������, �.�. ������� ����� ������������ ����� ajax
                initBindEvents();
            },
            function (error) {
                console.log('error');
                console.log(error);
            }
        );
    }
}

function showLoader(node, msg) {
}

function hideLoader(node) {
}

function checkScheme(t, emulateClick = false) {
    // ������� ID �����������
    // var id = <?= (int)CUtil::JSEscape($ID) ?>;
    // ���� �� ���, ����� ����������� �� �� �� ������ ����� ��������� � �����������
    if (arParams.ENTITY_ID <= 0) {
        // ���������� ��������� � ���������������
        const popup = BX.PopupWindowManager.create("popup-message", t, {
            content: BX.message('MONETA_OFFER_ADDITITIONAL_INFO_POPUP_CONTENT'),
            darkMode: false,
            autoHide: true,
            closeByEsc: true,
            overlay: true,
            closeIcon: true,
            angle: true,
            titleBar: BX.message('MONETA_OFFER_ADDITITIONAL_INFO_POPUP_TITLEBAR'),
            offsetLeft: 7,
            width: 300,
            height: 90,
        });

        popup.show();
    }
    else {
        const dialog = new BX.CDialog({
            title: BX.message('MONETA_OFFER_ADDITITIONAL_INFO_DIALOG_TITLE'),
            content_url: componentTemplatePath + '/tools/tszh_moneta_offer.php',
            content_post: 'TSZH_ID=' + arParams.ENTITY_ID,
            draggable: true,
            resizable: false,
            overlay: true,
            /*'buttons' : [
             BX.CDialog.prototype.btnSave,
             BX.CDialog.prototype.btnClose,
             ],*/
            height: 600,
            width: 900,
        });

        dialog.Show();

        BX.addCustomEvent(dialog, 'onWindowClose',function(){
            // ���������� this �������� ������ ����
            if (this.__form.filled.value == 'Y')
            {
                // �������������� ������ ����� ������ ��� ����� �� ������
                arResult.MONETA_OFFER_ADDITITIONAL_DATA = true;
                // ��������� ������� �� ������ "����������"
                if (emulateClick)
                {
                    BX.fireEvent(BX('recepient'), 'click');
                }
            }
        });

    }
}

function checkStatement() {
    const dialog = new BX.CDialog({
        title: BX.message('MONETA_OFFER_ADDITITIONAL_INFO_PARTNER_AGREEMENT_DIALOG_TITLE'),
        content_url: componentTemplatePath + '/tools/tszh_moneta_offer_partner_agreement.php',
        content_post: 'TSZH_ID=' + arParams.ENTITY_ID,
        draggable: true,
        resizable: false,
        overlay: true,
        /*'buttons' : [
         BX.CDialog.prototype.btnSave,
         BX.CDialog.prototype.btnClose,
         ],*/
        height: 300,
        width: 600,
    });

    dialog.Show();
}

BX.ready(function () {

    // ���������� ����������� �������
    initBindEvents();

    // BX.bind(BX('addititional-request'), 'click', BX.proxy(checkScheme, this));
    // BX.bind(BX('recepient'), 'click', BX.delegate(checkScheme, this));

    /*BX.bind(BX('moneta-activate-comission-recepient'), 'click', function () {
        activateMotenaRecepient();
    });
    BX.bind(BX('moneta-activate-comission-payer'), 'click', function () {
        activateMotena();
    });*/

});