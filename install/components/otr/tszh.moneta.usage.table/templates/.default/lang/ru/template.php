<?php
/**
 * @author  Maxim Cherepinin <m@vdgb-soft.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2017 otr@rarus.ru
 * @date 15.09.2017 16:08
 */
global $MESS;

$MESS['TSZH_MONETA_USAGE_TABLE_TEMPLATE_COMISSION_RECEPIENT'] = 'Комиссия с управляющей компании';
$MESS['TSZH_MONETA_USAGE_TABLE_TEMPLATE_COMISSION_PAYER'] = 'Комиссия с жильца';
$MESS['TSZH_MONETA_USAGE_TABLE_TEMPLATE_BUTTON_ACTIVATE'] = 'Подключить';
$MESS['TSZH_MONETA_USAGE_TABLE_TEMPLATE_BUTTON_ACTIVATED'] = 'Подключено';
$MESS['TSZH_MONETA_USAGE_TABLE_TEMPLATE_EXAMPLE'] = 'Пример';
$MESS['TSZH_MONETA_USAGE_TABLE_TEMPLATE_SUM'] = 'Сумма';
$MESS['TSZH_MONETA_USAGE_TABLE_TEMPLATE_TOTAL'] = 'Итого';
$MESS['TSZH_MONETA_USAGE_TABLE_TEMPLATE_COM'] = 'Комиссия';
$MESS['TSZH_MONETA_USAGE_TABLE_TEMPLATE_RUB'] = 'руб.';
$MESS['TSZH_MONETA_USAGE_TABLE_TEMPLATE_COMISSION_RECEPIENT_INFO'] = 'При оплате с комиссией с управляющей организацией жилец платит ровно ту сумму, которую он указал при оплате (по умолчанию это сумма к оплате за текущий период), а управляющая организация сумму с вычетом процента платежной системы.';
$MESS['TSZH_MONETA_USAGE_TABLE_TEMPLATE_COMISSION_PAYER_INFO'] = 'При оплате с комиссией с управляющей жильца владелец лицевого счета указывает сумму к оплате. Эта сумма автоматически увеличивается на 1,8%, для того чтобы после вычета процента платежной системы в управляющую организацию поступила ровно та сумма, которую жилец должен оплатить.';
$MESS['TSZH_MONETA_USAGE_TABLE_TEMPLATE_FILL_REQUEST'] = 'Заполнить <a id="addititional-request">заявку</a> на подключение';
$MESS['TSZH_MONETA_USAGE_TABLE_TEMPLATE_FILLED_REQUEST'] = 'Заявка заполнена. <a id="addititional-request">Изменить данные.</a>';
$MESS['TSZH_MONETA_USAGE_TABLE_TEMPLATE_OFFER_STATUS_NOT_USE'] = 'Не подключено';
$MESS['TSZH_MONETA_USAGE_TABLE_TEMPLATE_OFFER_STATUS_ON_VERIFICATION'] = 'Данные заполнены.<br/>Данные проверяются платежной системой  в течении 3 дней.';
$MESS['TSZH_MONETA_USAGE_TABLE_TEMPLATE_OFFER_STATUS_WITHOUT_DOCS'] = 'Данные проверены, ожидается <a id="statement-request">заявление</a>. После того как данные проверены оплата на сайте начинает работать. Но в течении <span class="text-gray">30 дней</span> необходимо подписать и отправить заявление в платежную систему по адресу: НКО «МОНЕТА» (ООО): 424000, Российская Федерация, Республика Марий Эл, г. Йошкар-Ола, ул. Гоголя, д. 2, строение «А».';
$MESS['TSZH_MONETA_USAGE_TABLE_TEMPLATE_OFFER_STATUS_ACTIVE'] = 'Заявление принято';
$MESS['TSZH_MONETA_USAGE_TABLE_TEMPLATE_STATUS_USE'] = 'Подключено';

$MESS["MONETA_OFFER_ADDITITIONAL_INFO_POPUP_CONTENT"] = "Для подлкючения к сервису оплаты Монета.ру нужно сохранить объект управления.";
$MESS["MONETA_OFFER_ADDITITIONAL_INFO_POPUP_TITLEBAR"] = "Внимание";
$MESS["MONETA_OFFER_ADDITITIONAL_INFO_DIALOG_TITLE"] = "Дополнительные данные для подключения системы Монета.ру";
$MESS["MONETA_OFFER_ADDITITIONAL_INFO_PARTNER_AGREEMENT_DIALOG_TITLE"] = "Заявление";