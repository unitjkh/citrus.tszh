<?
use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Type\Date;
use Citrus\Tszh\MonetaAdditionalInfoHelper;
use Citrus\Tszh\MonetaAdditionalInfoTable;

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszh/include.php");

// ��������� �������� ����
Loc::loadMessages(__FILE__);

$app = Application::getInstance();
$request = $app->getContext()->getRequest();

// ������� ����� ������� �������� ������������ �� ������
$modulePermissions = $APPLICATION->GetGroupRight("citrus.tszh");

// ���� ��� ���� - �������� � ����� ����������� � ���������� �� ������
if ($POST_RIGHT == "D")
{
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

ClearVars();
$errorMessage = '';

// ������������� ������� ����������
$ID = (int)$request->get('TSZH_ID');

// ������ ��������� �����
$monetaFields = MonetaAdditionalInfoTable::getEditableFields();

// �������� ��������� ������
$is_success = $request->get('success') == 'Y';

if ($is_success)
{
	CAdminMessage::ShowMessage(array(
		'TYPE' => 'OK',
		'MESSAGE' => Loc::getMessage('MONETA_OFFER_SUCCESS_MESSAGE'),
		"HTML" => true)
	);
}

if ($request->getRequestMethod() == "POST"
    // && !empty($request->get('Update'))
    && $request->get('update') == 'Y'
    && $modulePermissions >= "W"
    && check_bitrix_sessid()
)
{
	CUtil::JSPostUnescape();

	// ���� ������������ �������� ������ �� ������� request, �� �� ����������� ��������� CUtil::JSPostUnescape() � ������ �� ������� ����� �� �������������
	// $postList = $request->getPostList()->toArray();
	$postList = $_POST;

	// ����� �� ������� ���������� ������ ������ ������
	$monetaOfferFieldValues = array();
	array_map(function ($field) use (&$monetaOfferFieldValues, &$postList)
	{
		if (isset($postList[$field['column_name']]))
		{
			if($field['type'] == 'date')
			{
				$monetaOfferFieldValues[$field['column_name']] = new Date($postList[$field['column_name']]);
			}
			elseif($field['type'] == 'datetime')
			{
				$monetaOfferFieldValues[$field['column_name']] = new DateTime($postList[$field['column_name']]);
			}
			else
			{
				$monetaOfferFieldValues[$field['column_name']] = $postList[$field['column_name']];
			}
		}
	}, $monetaFields);

	try
	{
		$issetTszhRow = MonetaAdditionalInfoTable::getByPrimary($monetaOfferFieldValues['TSZH_ID'])->getSelectedRowsCount();
		if ($issetTszhRow > 0)
		{
			$result = MonetaAdditionalInfoTable::update($monetaOfferFieldValues['TSZH_ID'], $monetaOfferFieldValues);
		}
		else
		{
			$result = MonetaAdditionalInfoTable::add($monetaOfferFieldValues);
		}

		if(!$result->isSuccess())
		{
			$arErrors = $result->getErrorMessages();
			$errorMessage = implode('<br/>', $arErrors);
		}
	}
	catch (Exception $e)
	{
		$arErrors[] = $e->getMessage();
		$arErrors[] = $e->getTraceAsString();
		$errorMessage = implode('<br/>', $arErrors);
	}

	// CAdminMessage::ShowMessage('123123'); // ������
	// CAdminMessage::ShowMessage(array('TYPE' => 'OK', 'MESSAGE' => '123123')); // ������������� ���������

	if (strlen($errorMessage) <= 0)
	{
		LocalRedirect($APPLICATION->GetCurPageParam('success=Y&TSZH_ID=' . $monetaOfferFieldValues['TSZH_ID']));
	}
	else
	{
		$bVarsFromForm = true;
	}
}

if ($ID > 0)
{
	$APPLICATION->SetTitle(GetMessage("AE_TITLE_EDIT"));
}
else
{
	$APPLICATION->SetTitle(GetMessage("AE_TITLE_ADD"));
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_popup_admin.php");
// require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_js.php");
// require_once($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/include/prolog_admin_after.php");

if (strlen($errorMessage) > 0)
{
	CAdminMessage::ShowMessage(Array("DETAILS" => $errorMessage, "TYPE" => "ERROR", "MESSAGE" => Loc::getMessage('MONETA_OFFER_ERROR_MESSAGE'), "HTML" => true));
}

$aMonetaTabs = array(
	array(
		'DIV' => 'moneta_offer_partner_agreement',
		'TAB' => Loc::getMessage('MONETA_OFFER_PARTNER_AGREEMENT_TAB'),
		'TITLE' => Loc::getMessage('MONETA_OFFER_PARTNER_AGREEMENT_TITLE'),
	),
);

// ��������� ������
$arMonetaOfferFields = MonetaAdditionalInfoTable::getList(array(
	'filter' => array(
			'TSZH_ID' => $ID
	),
	'select' => array(
		'PARTNER_AGREEMENT_FILE',
		'PARTNER_AGREEMENT_SENT_DATE',
		'PARTNER_AGREEMENT_SENT_METHOD',
	)
))->fetch();

$arValues = array();
if ($bVarsFromForm)
{
	$arValues = $monetaOfferFieldValues;
}
elseif (is_array($arMonetaOfferFields))
{
	$arValues = $arMonetaOfferFields;
	if($arMonetaOfferFields['PARTNER_AGREEMENT_SENT_DATE'] instanceof Date)
	{
		$arValues['PARTNER_AGREEMENT_SENT_DATE'] = $arMonetaOfferFields['PARTNER_AGREEMENT_SENT_DATE']->format('d.m.Y');
	}
}

// ��������� �����
if ((int)$arValues['PARTNER_AGREEMENT_FILE'] > 0)
{
	$arFile = CFile::GetFileArray($arValues['PARTNER_AGREEMENT_FILE']);
}

// ���������� �������, ���� ����
MonetaAdditionalInfoHelper::fillFieldValues($monetaOfferTabs, $arValues);

$aMonetaTabsControl = new CAdminViewTabControl("siteTabControl", $aMonetaTabs);
?>
	<script type="text/javascript">
        BX.WindowManager.Get().SetButtons([BX.CDialog.prototype.btnSave, BX.CDialog.prototype.btnCancel]);
	</script>

	<form action="<?=$APPLICATION->GetCurPage()?>" name="monetaOfferAgreement_form" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="update" value="Y"/>
		<input type="hidden" name="lang" value="<? echo LANG ?>"/>
		<input type="hidden" name="TSZH_ID" value="<? echo $ID ?>"/>
		<? echo bitrix_sessid_post(); ?>

		<?php
		$aMonetaTabsControl->Begin();
		$aMonetaTabsControl->BeginNextTab();
		?>

		<table cellpadding="0" cellspacing="0" border="0" width="100%" class="adm-detail-content-table edit-table">
			<tr>
				<td width="40%"><?=Loc::getMessage('MONETA_OFFER_PARTNER_AGREEMENT_FILE')?></td>
				<td>
					<a href="<?=$arFile['SRC']?>" target="_blank"><?=$arFile['FILE_NAME']?></a>
				</td>
			</tr>
			<tr>
				<td width="40%"><?=$monetaFields['PARTNER_AGREEMENT_SENT_DATE']['title']?></td>
				<td>
					<? echo CalendarDate('PARTNER_AGREEMENT_SENT_DATE', $arValues['PARTNER_AGREEMENT_SENT_DATE']); ?>
				</td>
			</tr>
			<tr>
				<td width="40%"><?=$monetaFields['PARTNER_AGREEMENT_SENT_METHOD']['title']?></td>
				<td>
					<input type="text" name="PARTNER_AGREEMENT_SENT_METHOD" size="50" placeholder="<?=Loc::getMessage('MONETA_OFFER_PARTNER_AGREEMENT_SENT_METHOD_PLACEHOLDER')?>" value="<?=$arValues['PARTNER_AGREEMENT_SENT_METHOD']?>"/>
				</td>
			</tr>
		</table>

		<?php
		$aMonetaTabsControl->End();
		?>
	</form>
<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_popup_admin.php");
// require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin_js.php");
// require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");