<?

use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Type\Date;
use Bitrix\Main\Type\DateTime;
use Citrus\Tszh\MonetaAdditionalInfoHelper;
use Citrus\Tszh\MonetaAdditionalInfoTable;

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszh/include.php");

// ��������� �������� ����
Loc::loadMessages(__FILE__);

$app = Application::getInstance();
$request = $app->getContext()->getRequest();

// ������� ����� ������� �������� ������������ �� ������
$modulePermissions = $APPLICATION->GetGroupRight("citrus.tszh");

// ���� ��� ���� - �������� � ����� ����������� � ���������� �� ������
if ($POST_RIGHT == "D")
{
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

ClearVars();
$errorMessage = '';

// ������������� ������� ����������
$ID = (int)$request->get('TSZH_ID');

// ������ ��������� �����
$monetaFields = MonetaAdditionalInfoTable::getEditableFields();

// �������� ��������� ������
$is_success = $request->get('success') == 'Y';

if ($is_success)
{
	CAdminMessage::ShowMessage(array('TYPE' => 'OK', 'MESSAGE' => Loc::getMessage('MONETA_OFFER_SUCCESS_MESSAGE'), "HTML" => true));
}

if ($request->getRequestMethod() == "POST"
    // && !empty($request->get('Update'))
    && $request->get('update') == 'Y'
    && $modulePermissions >= "W"
    && check_bitrix_sessid()
)
{
	CUtil::JSPostUnescape();

	// ���� ������������ �������� ������ �� ������� request, �� �� ����������� ��������� CUtil::JSPostUnescape() � ������ �� ������� ����� �� �������������
	// $postList = $request->getPostList()->toArray();
	$postList = $_POST;

	// ����� �� ������� ���������� ������ ������ ������
	$monetaOfferFieldValues = array();
	array_map(function ($field) use (&$monetaOfferFieldValues, &$postList) {
		if (isset($postList[$field['column_name']]))
		{
			if ($field['type'] == 'date')
			{
				$monetaOfferFieldValues[$field['column_name']] = new Date($postList[$field['column_name']]);
			}
			elseif ($field['type'] == 'datetime')
			{
				$monetaOfferFieldValues[$field['column_name']] = new DateTime($postList[$field['column_name']]);
			}
			else
			{
				$monetaOfferFieldValues[$field['column_name']] = $postList[$field['column_name']];
			}
		}
	}, $monetaFields);

	try
	{
		// $tszhAddInfo = MonetaAdditionalInfoTable::getByPrimary($monetaOfferFieldValues['TSZH_ID'])->getSelectedRowsCount();
		$tszhAddInfo = MonetaAdditionalInfoTable::getByPrimary($monetaOfferFieldValues['TSZH_ID'])->fetch();
		
		if (!is_array($tszhAddInfo) || (is_array($tszhAddInfo) && $tszhAddInfo['STATUS'] === ''))
		{
			$monetaOfferFieldValues['STATUS'] = 'N';
		}

		if (is_array($tszhAddInfo))
		{
			$result = MonetaAdditionalInfoTable::update($monetaOfferFieldValues['TSZH_ID'], $monetaOfferFieldValues);
		}
		else
		{
			$result = MonetaAdditionalInfoTable::add($monetaOfferFieldValues);
		}

		if (!$result->isSuccess())
		{
			$arErrors = $result->getErrorMessages();
			$errorMessage = implode('<br/>', $arErrors);
		}
		/*else
		{
			$result = MonetaAdditionalInfoTable::update($monetaOfferFieldValues['TSZH_ID'], array('STATUS' => 'V'));
		}*/
	}
	catch (Exception $e)
	{
		$arErrors[] = $e->getMessage();
		$arErrors[] = $e->getTraceAsString();
		$errorMessage = implode('<br/>', $arErrors);
	}

	// CAdminMessage::ShowMessage('123123'); // ������
	// CAdminMessage::ShowMessage(array('TYPE' => 'OK', 'MESSAGE' => '123123')); // ������������� ���������

	if (strlen($errorMessage) <= 0)
	{
		LocalRedirect($APPLICATION->GetCurPageParam('success=Y&TSZH_ID=' . $monetaOfferFieldValues['TSZH_ID']));
	}
	else
	{
		$bVarsFromForm = true;
	}
}

if ($ID > 0)
{
	$APPLICATION->SetTitle(GetMessage("AE_TITLE_EDIT"));
}
else
{
	$APPLICATION->SetTitle(GetMessage("AE_TITLE_ADD"));
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_popup_admin.php");
// require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_js.php");
// require_once($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/include/prolog_admin_after.php");

if (strlen($errorMessage) > 0)
{
	// ����. ���� ������� ����� �������� ������
	$errorMessage .= '<div id="close-error-msg"></div>';
	CAdminMessage::ShowMessage(Array(
		"DETAILS" => $errorMessage,
		"TYPE" => "ERROR",
		"MESSAGE" => Loc::getMessage('MONETA_OFFER_ERROR_MESSAGE'),
		"HTML" => true,
	));
}

$aMonetaTabs = array(
	array(
		'DIV' => 'moneta_offer_personal',
		'TAB' => Loc::getMessage('MONETA_OFFER_PERSONAL_TAB'),
		'TITLE' => Loc::getMessage('MONETA_OFFER_PERSONAL_TAB_TITLE'),
		// 'ONSELECT' => "document.forms['socserv_settings'].siteTabControl_active_tab.value='opt_common'",
	),
	array(
		'DIV' => 'moneta_offer_contacts',
		'TAB' => Loc::getMessage('MONETA_OFFER_CONTACTS_TAB'),
		'TITLE' => Loc::getMessage('MONETA_OFFER_CONTACTS_TAB_TITLE'),
	),
	array(
		'DIV' => 'moneta_offer_head',
		'TAB' => Loc::getMessage('MONETA_OFFER_HEAD_TAB'),
		'TITLE' => Loc::getMessage('MONETA_OFFER_HEAD_TAB_TITLE'),
	),
	array(
		'DIV' => 'moneta_offer_finance',
		'TAB' => Loc::getMessage('MONETA_OFFER_FINANCE_TAB'),
		'TITLE' => Loc::getMessage('MONETA_OFFER_FINANCE_TAB_TITLE'),
	),
	array(
		'DIV' => 'moneta_offer_beneficiary',
		'TAB' => Loc::getMessage('MONETA_OFFER_BENEFICIARY_TAB'),
		'TITLE' => Loc::getMessage('MONETA_OFFER_BENEFICIARY_TAB_TITLE'),
	),
);

$monetaOfferTabs = array(
	'organization' => array(
		$monetaFields['PLANNED_TURNOVERS'],
		$monetaFields['POSITION_DIRECTOR'],
		$monetaFields['POSITION_DIRECTOR_DETAILS'],
		$monetaFields['ACTING_DOCUMENT'],
		$monetaFields['ACTING_DOCUMENT_DETAILS'],
		$monetaFields['ATTORNEY_NUMBER'],
		$monetaFields['ATTORNEY_DATE_FROM'],
		$monetaFields['ATTORNEY_DATE_TO'],
		$monetaFields['AGREEMENT_SIGNER_FIO'],
		array('type' => 'section', 'title' => Loc::getMessage('MONETA_OFFER_ORGANIZATION_REGISTRATION_SECTION')),
		$monetaFields['REGISTRATION_DATE'],
		$monetaFields['REGISTRATION_AUTHORITY'],
		$monetaFields['FIO_ACCOUNTANT'],
		$monetaFields['FIO_CONTACT'],
	),
	'contacts' => array(
		$monetaFields['POST_ADDRESS'],
		$monetaFields['LEGAL_ADDRESS'],
		array_merge($monetaFields['PHONE_CONTACT'], array('size' => 20, 'maxlength' => 11, 'placeholder' => '84953337799')),
		array_merge($monetaFields['PHONE_ACCOUNTANT'], array('size' => 20, 'maxlength' => 11, 'placeholder' => '84953337799')),
		array_merge($monetaFields['PHONE_SUPPORT'], array('size' => 20, 'maxlength' => 11, 'placeholder' => '84953337799')),
		array_merge($monetaFields['FINANCE_EMAIL'], array('size' => 30, 'placeholder' => 'email@company.ru')),
		array_merge($monetaFields['TECHNICAL_EMAIL'], array('size' => 30, 'placeholder' => 'email@company.ru')),
	),
	'director' => array(
		$monetaFields['DIRECTOR_NAME'],
		$monetaFields['DIRECTOR_DATE_OF_BIRTH'],
		$monetaFields['DIRECTOR_PLACE_OF_BIRTH'],
		$monetaFields['DIRECTOR_LEGAL_ADDRESS'],
		$monetaFields['DIRECTOR_POST_ADDRESS'],
		array_merge($monetaFields['DIRECTOR_PHONE_CONTACT'], array('size' => 20, 'maxlength' => 11, 'placeholder' => '84953337799')),
		array('type' => 'section', 'title' => Loc::getMessage('MONETA_OFFER_DIRECTOR_IDENTITY_DOCUMENT_SECTION')),
		array_merge($monetaFields['DIRECTOR_IDENTITY_DOCUMENT_SERIES'], array('size' => 15)),
		array_merge($monetaFields['DIRECTOR_IDENTITY_DOCUMENT_NUMBER'], array('size' => 15)),
		$monetaFields['DIRECTOR_IDENTITY_DOCUMENT_ISSUE_DATE'],
		$monetaFields['DIRECTOR_IDENTITY_DOCUMENT_ISSUED_BUREAU'],
	),
	'finance' => array(
		$monetaFields['CAPITAL_TYPE'],
		$monetaFields['PAID_CAPITAL_SIZE'],
		$monetaFields['REGISTERED_CAPITAL_SIZE'],
		$monetaFields['BUDGET_ARREARS_ABSENCE'],
	),
	'beneficiary' => array(
		$monetaFields['BENEFICIARY_NAME'],
		$monetaFields['BENEFICIARY_DATE_OF_BIRTH'],
		$monetaFields['BENEFICIARY_PLACE_OF_BIRTH'],
		$monetaFields['BENEFICIARY_LEGAL_ADDRESS'],
		$monetaFields['BENEFICIARY_POST_ADDRESS'],
		array_merge($monetaFields['BENEFICIARY_PHONE_CONTACT'], array('size' => 20, 'maxlength' => 11, 'placeholder' => '84953337799')),
		array('type' => 'section', 'title' => Loc::getMessage('MONETA_OFFER_BENEFICIARY_IDENTITY_DOCUMENT_SECTION')),
		array_merge($monetaFields['BENEFICIARY_IDENTITY_DOCUMENT_SERIES'], array('size' => 15)),
		array_merge($monetaFields['BENEFICIARY_IDENTITY_DOCUMENT_NUMBER'], array('size' => 15)),
		$monetaFields['BENEFICIARY_IDENTITY_DOCUMENT_ISSUE_DATE'],
		$monetaFields['BENEFICIARY_IDENTITY_DOCUMENT_ISSUED_BUREAU'],
	),
);

// ��������� ������
$arMonetaOfferFields = MonetaAdditionalInfoTable::getByPrimary($ID)->fetch();

$arValues = array();
if ($bVarsFromForm)
{
	$arValues = $monetaOfferFieldValues;
}
elseif (is_array($arMonetaOfferFields))
{
	$arValues = $arMonetaOfferFields;
}

// ���������� �������, ���� ����
MonetaAdditionalInfoHelper::fillFieldValues($monetaOfferTabs, $arValues);

/*$mess = new CAdminMessage(array("DETAILS" => "qwedqwef23f23f23", "TYPE" => "OK", "MESSAGE" => "123123123", "HTML" => true));
$mess->ShowNote("!23123123");
$mess->ShowMessage(array("MESSAGE" => "123123123", "HTML" => true, "TYPE" => "PROGRESS"));*/

$aMonetaTabsControl = new CAdminViewTabControl("siteTabControl", $aMonetaTabs);
?>
	<style>
		#close-error-msg {
			position: absolute;
			right: 5px;
			top: 5px;
			width: 10px;
			height: 10px;
		}
		#close-error-msg:hover {
			cursor: pointer;
		}
		#close-error-msg:after,
		#close-error-msg:before {
			content: "";
			display: block;
			width: 1px;
			height: 10px;
			position: absolute;
			background-color: red;
			right: 5px;
		}
		#close-error-msg:before {
			transform: rotate(45deg);
		}
		#close-error-msg:after {
			transform: rotate(-45deg);
		}
	</style>

	<script type="text/javascript">
        BX.WindowManager.Get().SetButtons([BX.CDialog.prototype.btnSave, BX.CDialog.prototype.btnCancel]);

        BX.ready(function () {
            function checkFields() {
                monetaOffer_form = document.forms['monetaOffer_form'];
                BX('moneta_ACTING_DOCUMENT_DETAILS').style.display = monetaOffer_form.ACTING_DOCUMENT.value == 'OTHER' ? 'table-row' : 'none';
                BX('moneta_ATTORNEY_NUMBER').style.display = monetaOffer_form.ACTING_DOCUMENT.value == 'POWER_OF_ATTORNEY' ? 'table-row' : 'none';
                BX('moneta_ATTORNEY_DATE_FROM').style.display = monetaOffer_form.ACTING_DOCUMENT.value == 'POWER_OF_ATTORNEY' ? 'table-row' : 'none';
                BX('moneta_ATTORNEY_DATE_TO').style.display = monetaOffer_form.ACTING_DOCUMENT.value == 'POWER_OF_ATTORNEY' ? 'table-row' : 'none';
                BX('moneta_POSITION_DIRECTOR_DETAILS').style.display = monetaOffer_form.POSITION_DIRECTOR.value == 'OTHER' ? 'table-row' : 'none';
            }

            BX.bind(BX('field_ACTING_DOCUMENT'), 'change', function () {
                checkFields();
            });

            BX.bind(BX('field_POSITION_DIRECTOR'), 'change', function () {
                checkFields();
            });

            // �������� ���� � ��������
            BX.bind(BX('close-error-msg'), 'click', function () {
                const parent = BX.findParent(BX('close-error-msg'),
	                {
                        "tag" : "div",
                        "class" : "adm-info-message-red"
	                }
                );
                BX.remove(parent);
            });

            checkFields();
        });

		<?
		/* if ($is_success) : ?>

		function timer()
		{
			var time = BX('moneta_offer_dialog_close_timer').innerHTML;
			time--;

			if (time == 0)
			{
				top.BX.WindowManager.Get().AllowClose();
				top.BX.WindowManager.Get().Close();
				setTimeout(function(){}, 1000);
			}
			else
			{
				BX('moneta_offer_dialog_close_timer').innerHTML = time;
				setTimeout(timer, 1000);
			}
		}

		setTimeout(timer, 1000);

		<? endif; */
		?>
	</script>

<?
echo BeginNote();
echo Loc::getMessage('MONETA_OFFER_FIELD_REQUIRE_NOTE');
echo EndNote();
?>

	<form action="<?=$APPLICATION->GetCurPage()?>" name="monetaOffer_form" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="update" id="update" value="Y"/>
		<input type="hidden" name="lang" id="lang" value="<? echo LANG ?>"/>
		<input type="hidden" name="TSZH_ID" id="TSZH_ID" value="<? echo $ID ?>"/>
		<input type="hidden" name="filled" id="filled" value="<?= (is_array($arMonetaOfferFields) ? 'Y' : 'N') ?>"/>
		<? echo bitrix_sessid_post(); ?>

		<?
		$aMonetaTabsControl->Begin();

		$aMonetaTabsControl->BeginNextTab();

		echo MonetaAdditionalInfoHelper::drawMonetaAdditionalTable($monetaOfferTabs['organization']);

		$aMonetaTabsControl->BeginNextTab();

		echo MonetaAdditionalInfoHelper::drawMonetaAdditionalTable($monetaOfferTabs['contacts']);

		$aMonetaTabsControl->BeginNextTab();

		echo MonetaAdditionalInfoHelper::drawMonetaAdditionalTable($monetaOfferTabs['director']);

		$aMonetaTabsControl->BeginNextTab();

		echo MonetaAdditionalInfoHelper::drawMonetaAdditionalTable($monetaOfferTabs['finance']);

		$aMonetaTabsControl->BeginNextTab();

		echo BeginNote();
		echo Loc::getMessage('MONETA_OFFER_TAB_BENEFICIARY_NOTE');
		echo EndNote();

		echo MonetaAdditionalInfoHelper::drawMonetaAdditionalTable($monetaOfferTabs['beneficiary']);

		$aMonetaTabsControl->End();
		?>
	</form>

<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_popup_admin.php");
// require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin_js.php");
// require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");