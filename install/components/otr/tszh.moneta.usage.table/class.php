<?php
/**
 * @author  Maxim Cherepinin <m@vdgb-soft.ru>
 * @since 1.0
 * @copyright Copyright (c) 1994-2017 otr@rarus.ru
 * @date 14.09.2017 17:21
 */

use Bitrix\Main\Application;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Citrus\Tszh\HouseTable;
use Citrus\Tszh\MonetaAdditionalInfoTable;
use Citrus\Tszh\TszhTable;
use Citrus\Tszh\Types\monetaProfileType;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

class OtrTszhMonetaUsageTableEdit extends CBitrixComponent
{
	/**
	 * ���� ��������� � ����������� ������ ���������
	 * @var array
	 */
	protected static $entityTypes = array('tszh', 'house');

	/**
	 * @var Application
	 */
	protected $app;

	/**
	 * @var \Bitrix\Main\HttpRequest
	 */
	protected $request;

	/**
	 * @param $arParams
	 *
	 * @return array
	 */
	public function onPrepareComponentParams($arParams)
	{
		if (!in_array($arParams['ENTITY_TYPE'], $this::$entityTypes))
		{
			$this->arResult['ERRORS'][] = Loc::getMessage('TSZH_MONETA_USAGE_TABLE_ERROR_ENTITY_TYPE');
		}

		if (!$this->isParamSet($arParams['ENTITY_ID']))
		{
			$this->arResult['ERRORS'][] = Loc::getMessage('TSZH_MONETA_USAGE_TABLE_ERROR_ENTITY_ID_NOT_SET');
		}

		if (empty($this->arResult['ERRORS']))
		{
			$arParams['ENTITY_ID'] = intval($arParams['ENTITY_ID']);
		}

		return parent::onPrepareComponentParams($arParams);
	}

	/**
	 * @return mixed|void
	 */
	public function executeComponent()
	{
		$this->initJsCore()
		     ->initEntityInfo()
		     ->checkRequest();

		if ($this->arResult['IS_DEMO'])
		{
			echo tszhMonetaDemoNotice();
		}
		else
		{
			// parent::executeComponent();
			$this->includeComponentTemplate();
		}
	}

	/**
	 * ��������, ���������� �� ��������
	 *
	 * @param $param
	 *
	 * @return bool
	 */
	protected function isParamSet($param)
	{
		return (isset($param) && strlen($param) > 0);
	}

	/**
	 *
	 */
	protected function saveData()
	{

	}

	/**
	 *
	 */
	public function showError()
	{
		// echo '!!!!';
	}

	/**
	 *
	 */
	protected function checkPost()
	{

	}

	/**
	 * OtrTszhMonetaUsageTableEdit constructor.
	 *
	 * @param null $component
	 */
	public function __construct($component = null)
	{
		$this->app = Application::getInstance();
		$this->request = $this->app->getContext()->getRequest();

		parent::__construct($component);
	}

	/**
	 * ����������� ����������� ���������
	 * @return $this
	 */
	protected function initJsCore()
	{
		\CJSCore::Init(array('window', 'ajax'));

		return $this;
	}

	/**
	 * �������� ������� �� �����������
	 * @return $this
	 */
	protected function checkRequest()
	{
		if ($this->request->isPost()
		    && $this->request->isAjaxRequest()
		    && check_bitrix_sessid())
		{
			$bCanRegisterMonetaProfile = true;
			$post = $this->request->getPostList()->toArray();
			$id = (int)$post['ID'];
			$this->initEntityInfo();

			$arTszhUpdateFileds = array(
				'MONETA_NO_OFFER' => $post['MONETA_NO_OFFER'] == 'Y' ? 'Y' : 'N',
				'MONETA_OFFER' => $post['MONETA_OFFER'] == 'Y' ? 'Y' : 'N',
			);

			if (!TszhTable::isRschCorrect($this->arResult['ENTITY']['RSCH']))
			{
				$this->arResult['ERRORS'][] = Loc::getMessage("TSZH_MONETA_USAGE_TABLE_ERROR_INCORRECT_RSCH");

				$bCanRegisterMonetaProfile = false;
			}

			if (strlen($this->arResult['ENTITY']['MONETA_EMAIL']) == 0)
			{
				$this->arResult['ERRORS'][] = Loc::getMessage('TSZH_MONETA_USAGE_TABLE_ERROR_INCORRECT_MONETA_EMAIL');

				$bCanRegisterMonetaProfile = false;
			}

			if ($bCanRegisterMonetaProfile)
			{
				$profile = $arTszhUpdateFileds['MONETA_OFFER'] == 'Y' ? monetaProfileType::OFFER : monetaProfileType::NO_OFFER;

				$bProfileRegistered = false;

				if (Loader::includeModule('citrus.tszhpayment')
				    && method_exists('CTszhPaymentGateway', 'registerMonetaProfile'))
				{
					$bProfileRegistered = \CTszhPaymentGateway::getInstance()->registerMonetaProfile($id, $profile);
				}

				if ($bProfileRegistered !== false)
				{
					CTszh::Update($id, $arTszhUpdateFileds);
				}
			}

		}

		return $this;
	}

	/**
	 * ���� ���������� �� ��������� ��������
	 * @return $this
	 */
	protected function initEntityInfo()
	{
		$entity = $this->arParams['ENTITY_TYPE'];
		$entity_id = $this->arParams['ENTITY_ID'];

		Loader::includeModule('citrus.tszhpayment');

		switch ($entity)
		{
			case 'tszh':
				$arData = CTszh::GetList(
					array(),
					array('ID' => $entity_id),
					false,
					false,
					array('MONETA_ENABLED', 'MONETA_OFFER', 'MONETA_EMAIL', 'MONETA_NO_OFFER', 'RSCH')
				)->Fetch();
				try
				{
					$arMonetaData = MonetaAdditionalInfoTable::getByPrimary($entity_id)->fetch();
				}
				catch (\Bitrix\Main\ObjectPropertyException $e)
				{
					$this->arResult['ERRORS'][] = $e->getMessage();
				}
				catch (\Bitrix\Main\ArgumentException $e)
				{
					$this->arResult['ERRORS'][] = $e->getMessage();
				}
				catch (\Bitrix\Main\SystemException $e)
				{
					$this->arResult['ERRORS'][] = $e->getMessage();
				}
				break;
			case 'house':
				try
				{
					$arData = HouseTable::getByPrimary($entity_id);
				}
				catch (\Bitrix\Main\ObjectPropertyException $e)
				{
					$this->arResult['ERRORS'][] = $e->getMessage();
				}
				catch (\Bitrix\Main\ArgumentException $e)
				{
					$this->arResult['ERRORS'][] = $e->getMessage();
				}
				catch (\Bitrix\Main\SystemException $e)
				{
					$this->arResult['ERRORS'][] = $e->getMessage();
				}
				break;
			default:
				$this->arResult['ERRORS'][] = Loc::getMessage('TSZH_MONETA_USAGE_TABLE_ERROR_ENTITY_TYPE');
				break;
		}
		$this->arResult['MONETA_NO_OFFER'] = ($arData['MONETA_ENABLED'] == 'Y' && $arData['MONETA_NO_OFFER'] == 'Y');
		$this->arResult['MONETA_OFFER'] = ($arData['MONETA_ENABLED'] == 'Y' && $arData['MONETA_OFFER'] == 'Y');
		$this->arResult['MONETA_OFFER_ADDITITIONAL_DATA'] = is_array($arMonetaData);
		$this->arResult['MONETA_OFFER_DATA'] = $arMonetaData;
		$this->arResult['IS_DEMO'] = CTszhPaymentGateway::isDemo();

		if ($this->arResult['MONETA_OFFER_ADDITITIONAL_DATA'])
		{
			$arServerData = \CTszhPaymentGatewayMoneta::getInstance()->getAddInfo($entity_id);
			if (is_array($arServerData)
			    && isset($arServerData['STATUS'])
			    && $arServerData['STATUS'] != $this->arResult['MONETA_OFFER_DATA']['STATUS'])
			{
				$this->arResult['MONETA_OFFER_DATA']['STATUS'] = $arServerData['STATUS'];
			}
		}

		$this->arResult['ENTITY'] = $arData;

		return $this;
	}
}