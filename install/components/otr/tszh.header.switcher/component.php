<?

use Bitrix\Main\Config\Option;


if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();


if (!CModule::IncludeModule('citrus.tszh')) return;


?>

<section class="top-switcher">
<?

Option::set("citrus.tszh", "use_header_switcher_component", 'Y', SITE_ID);
CTszhFunctionalityController::CheckEdition();
Option::set("citrus.tszh", "use_header_switcher_component", 'N', SITE_ID);

?>
</section>

<?
$this->IncludeComponentTemplate();

?>