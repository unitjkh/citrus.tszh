<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


$arComponentDescription = array(
	"NAME" => GetMessage("SUPER_COMPONENT_NAME"),
	"DESCRIPTION" => GetMessage("SUPER_COMPONENT_DESCRIPTION"),
	"ICON" => "/images/menu_ext.gif",
	"PATH" => array(
		"ID" => "utility",
	),
);
?>