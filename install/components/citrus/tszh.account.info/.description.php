<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("TSZH_ACCOUNT_INFO_NAME"),
	"DESCRIPTION" => GetMessage("TSZH_ACCOUNT_INFO_NAME"),
	"ICON" => "/images/menu_ext.gif",
	"PATH" => array(
		"ID" => "citrus.tszh",
		"NAME" => GetMessage("COMPONENT_ROOT_SECTION"),
	),
);
?>