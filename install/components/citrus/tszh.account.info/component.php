<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (!CModule::IncludeModule('citrus.tszh'))
{
	ShowError(GetMessage("TSZH_MODULE_NOT_FOUND_ERROR"));
	return;
}

if (!CTszhFunctionalityController::CheckEdition())
	return;

if (!$USER->IsAuthorized()) {
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
    return;
}
$arResult['OWNER_LS'] = CTszh::IsTenant();
/*if (!CTszh::IsTenant()) {
    $APPLICATION->AuthForm(GetMessage("TSZH_NOT_A_MEMBER"));
    return;
}*/

if($arParams["SHOW_FIO"] == 'Y')
    $arResult['FIO'] = CTszhAccount::GetFullName($USER->GetID());
    $res = CTszhAccount::GetList(array(), array("USER_ID"=>$USER->GetID()), false, array("nTopCount"=>1), array("XML_ID","ADDRESS_FULL","REGION","CITY","SETTLEMENT","DISTRICT","STREET","HOUSE","FLAT", "FLAT_ABBR"));
    $arElement = $res->GetNext();
    if(is_array($arElement)) {
        if($arParams["SHOW_ADDRESS"] == 'Y')
            $arResult['ADDRESS'] = CTszhAccount::GetFullAddress($arElement);
        $GLOBALS["VARS"]['LS_ID'] = $arElement['XML_ID'];
    }
$this->IncludeComponentTemplate();
?>