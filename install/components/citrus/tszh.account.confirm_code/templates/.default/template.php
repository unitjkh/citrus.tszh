<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

IncludeTemplateLangFile(__FILE__);

if (array_key_exists("ERRORS", $arResult))
{
	echo ShowError($arResult["ERRORS"]);
}
if (array_key_exists("NOTE", $arResult))
{
	?>
	<div style="margin:0 auto; width:75%; text-align: center">
		<? echo ShowNote($arResult["NOTE"]); ?>
	</div>
<? } ?>
<div><!--style="margin:0 auto; width:75%"-->
	<?
	if (is_array($arResult["ACCOUNT"])):
	foreach ($arResult["ACCOUNT"] as $arRes)
	{
		?><p><?=str_replace("#ACCOUNT#", $arRes["XML_ID"], GetMessage("TAC_YOUR_ACCOUNT"))?></p><?
	} ?>
		<p><?=GetMessage("TAC_GO_TO")?><a href="<?=$arParams["PERSONAL_PATH"]?>"><?=GetMessage("TAC_PERSONAL_SECTION")?></a>.</p>
	<?
	else:
	if ($arResult['ACC'] == 0 && $_REQUEST['action'] != 'confirm')
	{
	?>
		<script type="text/javascript">
            $(document).ready(function () {
                $("#step0").css('display', 'none');
                $("#step2").css('display', 'block');
                $("#error").css('display', 'block');
                $("#stepDesc0").attr('class', '');
                $("legend").text('���� ������');
                $("#stepDesc2").attr('class', '');
                $("#stepDesc1").attr('class', 'current');
                $("#progress").attr('aria-valuenow', '50');
                $(".ui-progressbar-value").css('width', '50%');

            });
		</script>
		<p style="color: red">�������� ����� ��� ������</p>
		<form method="post" action="#">
			<table style="width: 100%">
				<tr>
					<td class="span"><span class="title">������� �����:</span></td>
					<td><input type="text" name="regcode" value="" class="code"></td>
				</tr>
				<tr>
					<td class="span"><span class="title">������� ������:</span></td>
					<td><input type="password" name="password" value="" class="code"></td>
				</tr>
			</table>
			<br><br>
			<input type="hidden" id="word" name="word" value="2" class="code">
			<p style="text-align: right">
				<a href="/personal/confirm-account/" id="step3Prev" class="form-variable__button form-variable__saved link-theme-default"
				   style="color:#fff !important"> <?=GetMessage("TAC_BUTTON_PREV")?></a>
				<button type="submit" id="step3Prev1"
				        class="form-variable__button form-variable__saved link-theme-default"><?=GetMessage("TAC_BUTTON_NEXT")?></button>
			</p>
			</button>
		</form>
	<?
	}
	else
	{
	if ($_REQUEST['action'] != 'confirm')
	{

	?>
		<p> �������������� ������� ����� </p>
		<form method="post" enctype="multipart/form-data" action="#center" id="regcodeform">
			<table class="step3data">
				<?
				foreach ($arResult['ACC'] as $arRes)
				{
					?>
					<tr>
						<td>
							<label>
								<input type="checkbox" id="arrow" class="checkbox" name="accounts[]" value="<?=$arRes['ID']?>">
								<span class="checkbox-custom"></span>
							</label>
						</td>
						<td>
							<?=$arRes["XML_ID"]?><br>


							<span class="acc_name"><?=$arRes["NAME"]?></span>

						</td>
						<td class="whide">
							<?=$arRes["ADDRESS_FULL"]?>
						</td>
					</tr>
					<?
				} ?>
			</table>
			<br><br><br>
			<input type="hidden" name="action" value="confirm"/>
			<?=bitrix_sessid_post()?>
			<input type="hidden" name="regcode" class="input" id="confirm-regcode" value="<?=$arResult['REG_CODE']?>"/>
			<input type="hidden" id="check" name="check" value="2" class="code"><br>
			<p style="text-align: right; padding-top: 15px;" id="buttons">
				<a href="/personal/" id="step3Prev" class="form-variable__button form-variable__saved link-theme-default"
				   style="color:#fff !important"> <?=GetMessage("TAC_BUTTON_CANCEL")?></a>

				<button class="form-variable__button form-variable__saved"
				        form="regcodeform" id="step3Prev1" type="submit"><?=GetMessage("TAC_BUTTON_NEXT")?></button>
			</p>
		</form>
		<?
	}
	} ?>
	<?
	endif;
	?>
	<? if ($arResult['ACC'] != 0 && $_REQUEST['action'] != 'confirm')
	{
		?>
		<script type="text/javascript">
            $(document).ready(function () {
                var input = $("input.checkbox");
                if (input.attr('checked') != true) {
                    $("#step3Prev1").attr('disabled', true);
                }

                function isSelectedAccounts() {
                    var input = $("input.checkbox");
                    var bChecked = false;
                    input.each(function (i, elem) {
                        if (elem.checked) {
                            bChecked = true;
                        }
                    });
                    return bChecked;
                }

                $.each(input.click(function () {
                    //console.log(input);
                    if (isSelectedAccounts()) {
                        $("#step3Prev1").attr('disabled', false).addClass('link-theme-default');
                    }
                    else {
                        $("#step3Prev1").attr('disabled', true).removeClass('link-theme-default');
                    }
                }));
            });
		</script>
	<? } ?>
</div>
