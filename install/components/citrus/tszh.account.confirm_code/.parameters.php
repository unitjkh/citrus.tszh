<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
	"PARAMETERS" => array(
		"REGCODE_PROPERTY" => Array(
			"NAME" => GetMessage("REGCODE_USER_PROPERTY"),
			"TYPE" => "STRING",
			"DEFAULT" => "UF_REGCODE",
		),
		"PERSONAL_PATH" => Array(
			"NAME" => GetMessage("PERSONAL_PATH"),
			"TYPE" => "STRING",
			"DEFAULT" => "#SITE_DIR#personal/",
		),
	),
);
?>
