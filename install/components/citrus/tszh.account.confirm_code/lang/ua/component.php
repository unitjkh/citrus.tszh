<?
$MESS["TSZH_ALREADY_CONFIRMED"] = "Ваша належність до особового рахунку вже підтверджена.";
$MESS["IBLOCK_MODULE_NOT_INSTALLED"] = "Модуль інформаційних блоків не встановлений.";
$MESS["TSZH_NO_TSZHS"] = "Не знайдено жодного ТСЖ.";
$MESS["TSZH_WRONG_CAPTCHA"] = "Символи, що вказані на малюнку, введені не вірно.";
$MESS["ERROR_TSZH_NOT_SELECTED"] = "Будь-ласка, оберіть ТСЖ";
$MESS["ERROR_REGCODE_EMPTY"] = "Необхідно ввести кодове слово";
$MESS["ERROR_WRONG_CODE"] = "Кодове слово введене не вірно.";
$MESS["ERROR_SAVING_ACCOUNT"] = "При збереженні особового рахунку виникла помилка";
$MESS["CONFIRM_ACCOUNT_SUCCESS"] = "Особовий рахунок підтверджений.";
$MESS["TSZH_ACCOUNT_ALREADY_CONFIRMED"] = "Особовий рахунок вже прив'язаний до користувача на сайті.";
$MESS["TSZH_MODULE_NOT_FOUND_ERROR"] = "Модуль 1С: Сайт ЖКГ не встановлений.";
?>