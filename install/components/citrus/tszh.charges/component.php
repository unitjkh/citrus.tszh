<?
/**
 * ������ ���ƻ
 * ��������� ����������� ������ (citrus.tszh.charges)
 * ������� ������ ����������� ����� �������� ������������.
 * @package tszh
*/

use Citrus\Tszh\Types\ReceiptType;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (!CModule::IncludeModule('citrus.tszh'))
{
	ShowError(GetMessage("TSZH_MODULE_NOT_FOUND_ERROR"));
	return;
}

if (!CTszhFunctionalityController::CheckEdition())
	return;

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 300;

global $USER;
// ���� ������������ �� �����������, ������� ����� �����������
if (!$USER->IsAuthorized()) {
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

if (!CTszh::IsTenant()) {
	//$this->AbortResultCache();
	$APPLICATION->AuthForm(GetMessage("TSZH_NOT_A_MEMBER"));
	return;
}

$arAccount = CTszhAccount::GetByUserID($USER->GetID());

$lastPeriodId = CTszhPeriod::GetLast($arAccount["TSZH_ID"]);

if ($lastPeriodId > 0)
{
	$rsPeriods = CTszhAccountPeriod::GetList(
		Array('ID' => 'ASC'),
		Array(
			'PERIOD_ID' => $lastPeriodId,
			'ACCOUNT_ID' => $arAccount["ID"],
			"PERIOD_TSZH_ID" => $arAccount["TSZH_ID"],
			"PERIOD_ACTIVE" => "Y",
			"!PERIOD_ONLY_DEBT" => "Y",
			"@TYPE" => array(
				ReceiptType::MAIN,
				ReceiptType::OVERHAUL
			)
		)
	);
	$receiptTypes = ReceiptType::getTitles();
	$receiptCount = $rsPeriods->SelectedRowsCount();
	/** @noinspection PhpAssignmentInConditionInspection */
	while ($arPeriod = $rsPeriods->GetNext())
	{
		$arResult = Array();
		$arResult['PERIOD_ID'] = $arPeriod["ID"];
		$arResult["ACCOUNT_PERIOD"] = $arPeriod;

		if ($arResult['PERIOD_ID'] > 0)
		{

			if (is_array($arResult['ACCOUNT_PERIOD']))
			{
				$arResult['DISPLAY_NAME'] = CTszhPeriod::Format($arResult['ACCOUNT_PERIOD']['PERIOD_DATE']) . ($receiptCount > 0 && $arPeriod["TYPE"] > 0 ? ' (' . $receiptTypes[$arPeriod["TYPE"]] . ')' : '');
			}

			$dbCharge = CTszhCharge::GetList(
				Array("SORT" => "ASC", "ID" => "ASC"),
				Array(
					'ACCOUNT_PERIOD_ID' => $arPeriod['ID']
				),
				false,
				false,
				array('*')
			);
			$total_charges = 0;
			$total_payed = 0;
			$arResult['CHARGES'] = Array();
			/** @noinspection PhpAssignmentInConditionInspection */
			while ($arCharge = $dbCharge->GetNext())
			{
				$arResult['CHARGES'][] = $arCharge;
				if ($arCharge["COMPONENT"] == "N")
				{
					$total_charges += $arCharge['SUMM'];
					$total_payed += $arCharge['SUMM_PAYED'];
				}
			}
			$arResult['TOTAL_PAYED'] = $total_payed;
			$arResult['TOTAL_CHARGES'] = $total_charges;
		}

		$arResult['ACCOUNT'] = CTszhAccount::GetByUserID($USER->GetID());

		// ����������� ������� ����������
		$this->IncludeComponentTemplate();
	}
}