<?
$MESS['TSZH_CHARGES_SERVICE'] = "Услуга";
$MESS['TSZH_CHARGES_CHARGED'] = "Начислено";
$MESS["TSZH_CHARGES_CORRECTION"] = "Перерасчеты";
$MESS['TSZH_CHARGES_TO_PAY'] = "К оплате";
$MESS['TSZH_CHARGES_YOU_HAVE_NO_CHARGES'] = "У вас нет начислений.";
$MESS['TSZH_CHARGES_DEBT'] = "Задолженность";
$MESS['TSZH_CHARGES_BALANCE'] = "Ваш баланс";

$MESS['TSZH_CHARGES_YANDEX_PAYMENT_DESCR'] = "за услуги ЖКХ по л/с № #ACCOUNT_NUMBER# за #PREV_MONTH#";
$MESS["CITRUS_TSZHPAYMENT_LINK"] = "<p>Вы можете <a href=\"#LINK#\">оплатить коммунальные услуги</a> онлайн.</p>";
?>