<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams ���������, ������, ���������. �� ����������� ����������� ���� ����������, �� ��������� ��� ������ ��  � ����� template.php. */
/** @var array $arResult ���������, ������/���������. ����������� ����������� ���� ������ ����������. */
/** @var CBitrixComponentTemplate $this ������� ������ (������, ����������� ������) */

foreach ($arResult["CHARGES"] as $key => &$charge)
{
    if ($charge["DEBT_END"] == 0) // ��� ������������� �� ������� ���������� ������, ��� ���� debtend ��� ���������� (<item>) �� �����������
        $arResult["CHARGES"][$key]["DEBT_END"] = $charge["SUMM2PAY"];
    if ($charge["CORRECTION"] == 0 && $charge["DEBT_END"] == 0)  // ���� �� ������ ��� ������� �������� �� ���� ��������, ��������� ��� �������
        unset($arResult["CHARGES"][$key]);
}
if (isset($charge))
    unset($charge);