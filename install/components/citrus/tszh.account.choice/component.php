<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

if (!CModule::IncludeModule('citrus.tszh'))
{
	ShowError(GetMessage("TSZH_MODULE_NOT_FOUND_ERROR"));

	return;
}

if (!CTszhFunctionalityController::CheckEdition())
{
	return;
}

if (!CTszh::IsTenant())
{
	return;
}

if (array_key_exists('accountID', $_REQUEST))
{

	$accountID = IntVal($_REQUEST['accountID']);

	if (CTszhAccount::SetCurrent($accountID))
	{
		LocalRedirect($GLOBALS["APPLICATION"]->GetCurPageParam('', Array('accountID')));
	}
	else
	{
		$arResult["ERROR_MSG"] = GetMessage("COMP_ERROR_CANT_CHANGE_ACCOUNT");
	}
}

global $USER;

$arResult["ACCOUNTS"] = Array();

$dbAccounts = CTszhAccount::GetList(
	$arOrder = array(),
	$arFilter = array("USER_ID" => CUser::GetID()),
	$arGroupBy = false,
	$arNavStartParams = false,
	$arSelectFields = array("*")
);

$arResult["CURRENT"] = null;

while ($arAccount = $dbAccounts->GetNext())
{
	$arAccount["TITLE"] = GetMessage("TSZH_ACCOUNT_TITLE", Array(
		"#NUM#" => $arAccount['XML_ID'],
		"#TEXT#" => CTszhAccount::GetFullAddress($arAccount, Array("REGION", "CITY", "TOWN", "SETTLEMENT", "DISTRICT")),
	));
	$arResult["ACCOUNTS"][] = $arAccount;

	if (is_null($arResult["CURRENT"]) || $arAccount["CURRENT"] == "Y")
	{
		$arResult["CURRENT"] = $arAccount;
	}

}

if (count($arResult["ACCOUNTS"]) > 0)
{
	$this->IncludeComponentTemplate();
}

?>