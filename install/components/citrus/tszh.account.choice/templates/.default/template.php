<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();


if (count($arResult["ACCOUNTS"]) > 0):

	if (array_key_exists("ERROR_MSG", $arResult))
		ShowError($arResult["ERROR_MSG"]);

	?><form method="GET" action="<?=POST_FORM_ACTION_URI?>"><?
	
	echo GetMessage("TEMPLATE_YOUR_ACCOUNT") . ': '; 
	
	if (count($arResult["ACCOUNTS"]) == 1)
	{
		echo "<strong>" . $arResult["CURRENT"]["TITLE"] . "</strong>";
	}
	else
	{
		?>
		<select name="accountID" onchange="this.form.submit()" class="citrus-tszh-account-choice">
			<?foreach ($arResult["ACCOUNTS"] as $arAccount): ?>
				<option<?=($arAccount['CURRENT'] == "Y" ? " selected='selected'" : '')?> value="<?=$arAccount['ID']?>"><?=$arAccount["TITLE"]?></option>
			<?endforeach;?>
		</select>
		<?
	}
	
	?></form><?

endif;

?>