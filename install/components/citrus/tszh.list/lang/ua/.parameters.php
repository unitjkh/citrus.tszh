<?
$MESS["IBLOCK_DETAIL_URL"] = "URL, що веде на сторінку з вмістом елемента розділу";
$MESS["TSZH_DESC_ASC"] = "За зростанням";
$MESS["TSZH_DESC_DESC"] = "За спаданням";
$MESS["TSZH_DESC_FID"] = "ID";
$MESS["TSZH_DESC_FNAME"] = "Назва";
$MESS["TSZH_DESC_FTSAMP"] = "Дата останньої зміни";
$MESS["TSZH_DESC_IBORD"] = "Поле для сортування ТСЖ";
$MESS["TSZH_DESC_IBBY"] = "Напрямок для сортування ТСЖ";
$MESS["TSZH_DESC_LIST_CONT"] = "Кількість ТСЖ на сторінці";
$MESS["TSZH_DESC_PAGER_TSZH"] = "ТСЖ";
?>