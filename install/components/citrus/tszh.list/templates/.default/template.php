<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?><?

if($arParams["DISPLAY_TOP_PAGER"])
	echo "<div>" . $arResult["NAV_STRING"] . "</div>\n";

?>
<ul class="tszh-list">
<?
	foreach($arResult["ITEMS"] as $arItem) {
		echo "<li><a href=\"{$arItem["HREF"]}\" title\"{$arItem["NAME"]}\">{$arItem["NAME"]}</a></li>\n";
	}
?>
</ul>
<?

if($arParams["DISPLAY_BOTTOM_PAGER"])
	echo "<div>" . $arResult["NAV_STRING"] . "</div>\n";

?>
