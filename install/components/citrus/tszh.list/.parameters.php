<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arSorts = Array(
	"ASC" => GetMessage("TSZH_DESC_ASC"),
	"DESC" => GetMessage("TSZH_DESC_DESC"),
);

$arSortFields = Array(
		"ID" => GetMessage("TSZH_DESC_FID"),
		"NAME" => GetMessage("TSZH_DESC_FNAME"),
		"TIMESTAMP_X" => GetMessage("TSZH_DESC_FTSAMP")
);

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS"  =>  array(
		"TSZH_COUNT"  =>  Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("TSZH_DESC_LIST_CONT"),
			"TYPE" => "STRING",
			"DEFAULT" => "20",
		),
		"SORT_BY"  =>  Array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("TSZH_DESC_IBORD"),
			"TYPE" => "LIST",
			"DEFAULT" => "ACTIVE_FROM",
			"VALUES" => $arSortFields,
			"ADDITIONAL_VALUES" => "Y",
		),
		"SORT_ORDER"  =>  Array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("TSZH_DESC_IBBY"),
			"TYPE" => "LIST",
			"DEFAULT" => "DESC",
			"VALUES" => $arSorts,
		),
		"CACHE_TIME"  =>  Array("DEFAULT"=>7200),
	),
);

if (CModule::IncludeModule("iblock"))
	CIBlockParameters::AddPagerSettings($arComponentParameters, GetMessage("TSZH_DESC_PAGER_TSZH"), true, true);
?>