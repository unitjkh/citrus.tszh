<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
} ?>

<script>
    $(document).ready(function () {
        $("lebel").click(function () {

            if ($("lebel").hasClass('select')) {
                $(this).removeClass('select').addClass('select2');
            }
            else {
                $(this).removeClass('select2').addClass('select');
            }

        });


    });
</script>

<div class="contaner_ls">
	<br/>
	<div class="work_area">

		<div class="left_info">
			<div style="padding-bottom: 7px;">
				<div class="test" style=" float:left; margin-top:5px;"><span><?=GetMessage("TEMPLATE_YOUR_ACCOUNT")?></span></div>
				<? if (count($arResult["ACCOUNTS"]) > 0):

					if (array_key_exists("ERROR_MSG", $arResult))
					{
						ShowError($arResult["ERROR_MSG"]);
					}

					?>
					<form method="GET" action="<?=POST_FORM_ACTION_URI?>" <?=(count($arResult["ACCOUNTS"]) == 1?"style=\"padding-top: 5px\"": "" )?>><?

					//echo GetMessage("TEMPLATE_YOUR_ACCOUNT") . ': ';

					if (count($arResult["ACCOUNTS"]) == 1)
					{?>
						<i class="<?=($arResult["ACCOUNTS"]["0"]['ACTIVE'] == "Y" ? 'xml' : 'xml2')?>" >
							<?=($arResult["ACCOUNTS"]["0"]['ACTIVE'] == "Y" ? $arResult["ACCOUNTS"]["0"]["TITLE"] : GetMessage("TSZH_ACCOUNT_UNACTIVE").$arResult["ACCOUNTS"]["0"]["TITLE"])?>
						</i>
					<?}
					else
					{
						?>
						<div class="rail-select">
							<lebel for="select" class="select">
								<select name="accountID" onchange="this.form.submit()" id="select" class="selector">
									<?
									foreach ($arResult["ACCOUNTS"] as $arAccount): ?>
										<option<?=($arAccount['CURRENT'] == "Y" ? " selected='selected'" : '')?>
												class="<?=($arAccount['ACTIVE'] == "Y" ? 'xml' : 'xml2')?>"
												value="<?=$arAccount['ID']?>"
											<?=($arAccount['ACTIVE'] == "Y" ? '' : 'disabled')?>>
											<?=($arAccount['ACTIVE'] == "Y" ? $arAccount["TITLE"] : GetMessage("TSZH_ACCOUNT_UNACTIVE").$arAccount["TITLE"])?>
										</option>
									<? endforeach; ?>
								</select>
							</lebel>
						</div>

						<?
					}

					?></form><?

				endif;

				?>


			</div>

			<div style="padding-bottom: 15px;">
				<span><?=GetMessage("TSZH_ACCOUNT_ADRESS")?></span><span
						id="xml_adress"><?=$arResult["CURRENT"]['REGION'] . ', ' . $arResult["CURRENT"]['ADDRESS_FULL']?></span>
			</div>
			<div>
				<a href="/personal/confirm-account/">
					<button class="form-variable__button form-variable__saved link-theme-default"><?=GetMessage("TSZH_ACCOUNT_ADD")?></button>
				</a>
			</div>
		</div>
		<div class="right_info" style="text-align: left">
			<div style="padding-bottom: 7px;">
				<span><?=GetMessage("TSZH_ACCOUNT_DEBT")?></span>
			</div>
			<div style="padding-bottom: 15px;">
				<span id="debt"><?=($arResult["DEBT"]["DEBT_END"] > "0" ? $arResult["DEBT"]["DEBT_END"] : '0')?></span><span><?=GetMessage("TSZH_ACCOUNT_CURRENCY")?></span>
			</div>
			<div style="text-align: right">
				<a href="/personal/payment/">
					<button class="form-variable__button form-variable__saved link-theme-default"><?=GetMessage("TSZH_ACCOUNT_PAY")?></button>
				</a>
			</div>
		</div>

	</div>
	<br/>
</div>