<?
$MESS["CTM_METER"] = "Лічильник";
$MESS["CTM_TARIFF_N"] = "Тариф #N#";
$MESS["CTM_PREV_VALUE"] = "Попереднє значення";
$MESS["CTM_CURRENT_VALUE"] = "Поточне значення";
$MESS["CTM_VERIFICATION_DATE"] = "Дата повірки";
$MESS["CTM_YOU_HAVE_NO_METERS"] = "У вас немає лічильників.";
$MESS["CTM_BTN_CAPTION"] = "Зберегти";
$MESS["CTM_PAGER_TITLE"] = "Показання";
$MESS["CTM_METER_URL_TEXT"] = "Введення показань лічильників";
$MESS["CTM_METER_HISTORY_URL_TEXT"] = "Історія показань лічильників";
$MESS["CTM_METERS_BLOCK_EDIT_MESSAGE_DEFAULT"] = "Введення показань лічильників тимчасово призупинено.";
$MESS["CTM_METER_VALUES_INPUT_DENIED_BY_PERIOD"] = "Введення показань лічильників заборонений (введення показань дозволено з #START_DATE# по #END_DATE# число місяця).";
$MESS["CTM_PREV_VALUE_TITLE"] = "Попереднє значення";
$MESS["CTM_CURRENT_VALUE_TITLE"] = "поточне значення";
