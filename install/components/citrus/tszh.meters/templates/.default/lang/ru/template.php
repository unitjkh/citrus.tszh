<?
$MESS["CTM_METER"] = "Счетчик";
$MESS["CTM_TARIFF_N"] = "Тариф #N#";
$MESS["CTM_PREV_VALUE"] = "Предыдущее";
$MESS["CTM_CURRENT_VALUE"] = "Текущее";
$MESS["CTM_PREV_VALUE_TITLE"] = "Предыдущее значение";
$MESS["CTM_CURRENT_VALUE_TITLE"] = "Текущее значение";
$MESS["CTM_VERIFICATION_DATE"] = "Дата поверки";
$MESS["CTM_YOU_HAVE_NO_METERS"] = "У вас нет счетчиков.";
$MESS["CTM_METER_HISTORY"] = "История";
$MESS["CTM_BTN_CAPTION"] = "Сохранить";
$MESS["CTM_PAGER_TITLE"] = "Показания";
$MESS["CTM_METER_URL_TEXT"] = "Ввод показаний счетчиков";
$MESS["CTM_METER_HISTORY_URL_TEXT"] = "История показаний счетчиков";
$MESS["CTM_METERS_BLOCK_EDIT_MESSAGE_DEFAULT"] = "Ввод показаний счетчиков временно приостановлен.";
$MESS["CTM_METER_VALUES_INPUT_DENIED_BY_PERIOD"] = "Ввод показаний счетчиков запрещен (ввод показаний разрешен с #START_DATE# по #END_DATE# число месяца).";

$MESS["CTM_METER_NUM"] = "Заводской номер счетчика";
$MESS["CTM_SERVICE"] = "Услуга";
?>