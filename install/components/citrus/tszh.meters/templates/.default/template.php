<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}
// var_dump($arResult);
if ($arResult['allow_edit'])
{
if (!empty($arResult["WARNING"]))
{
	ShowError(implode("", $arResult["WARNING"]));
}
?>
<form method="post" action="<?=$APPLICATION->GetCurPage();?>" data-version="1">
	<?
	foreach ($arResult["HIDDEN_FIELDS"] as $name => $value)
	{
		?><input type="hidden" name="<?=$name?>" value="<?=$value?>">
		<?
	}
	?>
	<?=bitrix_sessid_post()?>
	<?
	}
	elseif (!CTszhMeter::CanPostMeterValues())
	{
		ShowNote(COption::GetOptionString('citrus.tszh', 'meters_block_message',
			GetMessage("CTM_METERS_BLOCK_EDIT_MESSAGE_DEFAULT")));
	}
	elseif (!$arResult['allow_edit_by_period'])
	{
		ShowNote(
			GetMessage(
				"CTM_METER_VALUES_INPUT_DENIED_BY_PERIOD",
				array(
					'#START_DATE#' => $arResult["ACCOUNT"]["TSZH_METER_VALUES_START_DATE"],
					'#END_DATE#' => $arResult["ACCOUNT"]["TSZH_METER_VALUES_END_DATE"],
				)
			)
		);
	}
	?>

	<? if ($arResult["CURRENT_PAGE"] != 'history') : ?>

	<table class="data-table meters-table">
		<thead>
		<tr>
			<?
			if (!empty($arResult["SORT"])) //���� ����� ������� ���������� ��������
			{
				foreach ($arResult["SORT"] as $item)
				{
					switch ($item)
					{
						case "VALUES_COUNT":
							for (
								$i = 1; $i <= $arResult['MAX_VALUES'];
								$i++
							)
							{
								?>
								<th colspan="2" class="cost-head-top"><?=
									str_replace("#N#", $i,
										GetMessage("CTM_TARIFF_N"))
									?></th>
								<?
							}
							break;
						case "METER_HISTORY":
							?>
							<th rowspan="2">
							<?=GetMessage("CTM_METER_HISTORY")?>
							</th><?
							break;
						case "VERIFICATION_DATE":
							?>
							<th rowspan="2">
							<?=GetMessage("CTM_VERIFICATION_DATE")?></th><?
							break;
						case "NUM":
							?>
							<th rowspan="2">
							<?=GetMessage("CTM_METER_NUM")?></th><?
							break;
						case "NAME":
							?>
							<th rowspan="2">
							<?=GetMessage("CTM_METER")?>
							</th><?
							break;
						case "SERVICE_NAME":
							?>
							<th rowspan="2">
							<?=GetMessage("CTM_SERVICE")?>
							</th><?
							break;
						default :
							?>
							<th rowspan="2">
								<?=$item?>
							</th>
							<?
							break;
					}
				}
			}
			else //�������������� ������� �� ���������
			{
				?>
				<th rowspan="2"><?=GetMessage("CTM_METER")?></th>
				<?
				for ($i = 1; $i <= $arResult['MAX_VALUES']; $i++)
				{
					?>
					<th colspan="2" class="cost-head-top"><?=
						str_replace("#N#", $i,
							GetMessage("CTM_TARIFF_N"))
						?></th>
					<?
				}
				?>
				<th rowspan="2"><?=GetMessage("CTM_VERIFICATION_DATE")?></th>
				<th rowspan="2"><?=GetMessage("CTM_METER_HISTORY")?></th>
			<? }
			?>
		</tr>
		<tr>
			<?
			if (empty($arResult["SORT"]) || in_array("VALUES_COUNT", $arResult["SORT"])) //���� ������� ���������� �� ����� ��� � ���� ������� ������� ���� �����
			{
				for ($i = 1; $i <= $arResult['MAX_VALUES']; $i++)
				{
					?>
					<th class="cost-head">
						<small style="font-weight: normal;"
						       title="<?=GetMessage("CTM_PREV_VALUE_TITLE")?>"><?=GetMessage("CTM_PREV_VALUE")?></small>
					</th>
					<th class="cost-head">
						<small style="font-weight: normal;"
						       title="<?=GetMessage("CTM_CURRENT_VALUE_TITLE")?>"><?=GetMessage("CTM_CURRENT_VALUE")?></small>
					</th>
					<?
				}
			}
			?>
		</tr>
		</thead>
		<tbody>
		<?
		if (empty($arResult['ITEMS']))
		{
			?>
			<tr>
			<td colspan="<?=(count($arResult['SORT']) + $arResult['MAX_VALUES'] * 2)?>"><em><?=GetMessage("CTM_YOU_HAVE_NO_METERS")?></em>
			</td></tr><?
		}
		else
		{
			foreach ($arResult['ITEMS'] as $id => $arItem)
			{
				?>
				<tr>
					<?
					if (!empty($arResult["SORT"]))
					{
						foreach ($arResult["SORT"] as $item)
						{
							switch ($item)
							{
								case "VALUES_COUNT":
									for (
										$i = 1;
										$i <= $arItem["VALUES_COUNT"];
										$i++
									)
									{
										?>
										<td class="cost">
											<?=FloatVal($arItem['PREV_VALUE']['VALUE' . $i])?>
										</td>
										<td class="cost">
											<? if ($arResult['allow_edit']): ?>
												<input type="text" name="indiccur<?=$i?>[<?=$arItem['ID']?>]"
												       value="<?=$arItem['VALUE']['VALUE' . $i]?>" style="width: 50px;"
												       class="styled">
											<? else: ?>
												<?=$arItem['VALUE']['VALUE' . $i]?>
											<? endif ?>
										</td>
										<?
									}

									if ($arResult["MAX_VALUES"] > $arItem["VALUES_COUNT"])
									{
										?>
										<td colspan="<?=(($arResult["MAX_VALUES"] - $arItem["VALUES_COUNT"]) * 2)?>">&nbsp;</td>
										<?
									}
									break;
								case "METER_HISTORY":
									?>
									<td class="center meter-history">
									<a href="<?=$arResult["HISTORY_URL"]?>&id=<?=$arItem["ID"]?>"><?=GetMessage("CTM_METER_HISTORY")?></a>
									</td><?
									break;
								case "VERIFICATION_DATE":
									?>
									<td class="center">
									<?=$arItem["VERIFICATION_DATE"]?></td><?
									break;
								case "NUM":
									?>
									<td class="center">
									<?=$arItem['NUM']?></td><?
									break;
								case "NAME":
									?>
									<td class="center">
									<?=$arItem['NAME']?>
									</td><?
									break;
								case "SERVICE_NAME":
									?>
									<td class="center">
									<?=strlen($arItem['SERVICE_NAME']) > 0 ? $arItem['SERVICE_NAME'] : $arItem['NAME']?>
									</td><?
									break;
								default:
									?>
									<td class="center">
									<?=$arItem[$item]?>
									</td><?
									break;

							}
						}
					}
					?>
				</tr>
				<?
				if (empty($arResult["SORT"]))
				{
					?>
					<tr>
						<td class="meter-name"><?=strlen($arItem['SERVICE_NAME']) > 0 ? $arItem['SERVICE_NAME'] : $arItem['NAME']?>
							:
						</td>
						<?
						for ($i = 1; $i <= $arItem["VALUES_COUNT"]; $i++)
						{
							?>
							<td class="cost">
								<?=FloatVal($arItem['PREV_VALUE']['VALUE' . $i])?>
							</td>
							<td class="cost">
								<? if ($arResult['allow_edit']): ?>
									<input type="text" name="indiccur<?=$i?>[<?=$arItem['ID']?>]"
									       value="<?=$arItem['VALUE']['VALUE' . $i]?>" style="width: 50px;"
									       class="styled">
								<? else: ?>
									<?=$arItem['VALUE']['VALUE' . $i]?>
								<? endif ?>
							</td>
							<?
						}

						if ($arResult["MAX_VALUES"] > $arItem["VALUES_COUNT"])
						{
							?>
							<td colspan="<?=(($arResult["MAX_VALUES"] - $arItem["VALUES_COUNT"]) * 2)?>">&nbsp;</td>
							<?
						}
						?>
						<td class="center"><?=$arItem["VERIFICATION_DATE"]?></td>
						<td class="center meter-history">
							<a href="<?=$arResult["HISTORY_URL"]?>&id=<?=$arItem["ID"]?>"><?=GetMessage("CTM_METER_HISTORY")?></a>
						</td>
					</tr>
					<?
				}
			}
		}
		?>
		</tbody>
		<?
		if ($arResult['allow_edit'])
		{
			$count_col = (count($arResult['SORT']) > 0) ? count($arResult['SORT']) : 3;
			?>
			<tfoot>
			<tr>
				<td colspan="<?=($count_col + $arResult['MAX_VALUES'] * 2)?>" style="text-align: right; padding-right: 30px;">
					<?
					if (empty($arResult["SORT"]) || in_array("VALUES_COUNT", $arResult["SORT"]))
					{
						?>
						<input type="submit" name="submit_btn" value="<?=GetMessage("CTM_BTN_CAPTION")?>"/>
					<? } ?>
				</td>
			</tr>
			</tfoot>
			<?
		}
		?>
	</table>

	<?
	if ($arResult['allow_edit'])
	{
	?></form><?
}

// template version
return 1.1;

else :
	?>

	<?
	$APPLICATION->IncludeComponent(
		"citrus:tszh.meter.history", "",
		Array(
			"COMPONENT_TEMPLATE" => ".default",
			"FILTER_NAME" => isset($arParams["FILTER_NAME"]) ? $arParams["FILTER_NAME"] : "",
			"MODIFIED_BY_OWNER" => isset($arParams["MODIFIED_BY_OWNER"]) ? $arParams["MODIFIED_BY_OWNER"] : "Y",
			"COUNT_METERS_HISTORY" => isset($arParams["COUNT_METERS_HISTORY"]) ? $arParams["COUNT_METERS_HISTORY"] : 6,
			"ID" => IntVal($_REQUEST["id"]),
		)
	);
	?>

<?
endif;
?>
