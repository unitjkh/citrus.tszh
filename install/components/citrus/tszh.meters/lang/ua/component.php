<?
$MESS["TSZH_NOT_A_MEMBER"] = "Ви не є власником особового рахунку.";
$MESS["TSZH_MODULE_NOT_FOUND_ERROR"] = "Модуль 1С: Сайт ЖКГ не встановлений.";
$MESS["CITRUS_TSZH_METER_INCORRECT_VALUE"] = "#METER_NAME#, тариф#A#- . Вказана неправильне значення <br />";
$MESS["CITRUS_TSZH_METER_MUST_BE_LARGER"] = "#METER_NAME#, тариф#A#- . Поточне значення не може бути менше попереднього <br />";
$MESS["CITRUS_TSZH_METER_VALUE_MUST_BE_POSITIVE"] = "#METER_NAME#, тариф#A#- . > Необхідно ввести значення більше нуля <br />";
$MESS["CITRUS_TSZH_METER_VALUE_MUST_BE_NOT_NEGATIVE"] = "#METER_NAME#, тариф#A#- . > Необхідно ввести невід'ємне значення <br />";
$MESS["TSZH_METER_VALUES_SAVED"] = "Показання лічильників збережені";
?>