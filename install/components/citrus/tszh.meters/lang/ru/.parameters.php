<?
$MESS["TSZH_DONT_CHECK_PREV_VALUE"] = "Разрешать вводить показания меньше предыдущих";
$MESS["CITRUS_TSZH_METERS_ALLOW_ZERO_VALUES"] = "Разрешать вводить нулевые показания";
$MESS["CITRUS_TSZH_METERS_METERS_HISTORY"] = "История показаний счетчиков";
$MESS["CITRUS_TSZH_NAV_PAGER"] = "Показания";
$MESS["CITRUS_TSZH_MODIFIED_BY_OWNER"] = "Отображать показания";
$MESS["CITRUS_TSZH_COUNT_METERS_HISTORY"] = "Количество показаний счетчиков на одной странице";
$MESS["CITRUS_ALL"] = "(все)";
$MESS["CITRUS_MODIFIED_BY_OWNER_Y"] = "введенные владельцем лицевого счета";
$MESS["CITRUS_MODIFIED_BY_OWNER_N"] = "заполненные другими (из 1С, администраторами)";
$MESS["CITRUS_TSZH_FILTER_NAME"] = "Имя переменной, содержащей фильтр";

$MESS["CITRUS_TSZH_TABLE_DATA"] = "Данные";
$MESS["CITRUS_TSZH_TABLE_COUNT_COLUMN"] = "Список полей таблицы";
$MESS["CITRUS_TSZH_TABLE_COLUMN"] = "Столбец";
$MESS["CITRUS_TSZH_TABLE_DATA_SERVICE_NAME"] = "Название услуги";
$MESS["CITRUS_TSZH_TABLE_DATA_NAME"] = "Название счетчика";
$MESS["CITRUS_TSZH_TABLE_DATA_NUM"] = "Заводской номер счетчика";
$MESS["CITRUS_TSZH_TABLE_DATA_VERIFICATION"] = "Дата проверки счетчика";
$MESS["CITRUS_TSZH_TABLE_DATA_VALUES_COUNT"] = "Тариф(ы)";
$MESS["CITRUS_TSZH_TABLE_DATA_HISTORY"] = "История показаний";
$MESS["CITRUS_TSZH_NO_DATA"] = "Не выводить";
$MESS["CITRUS_TSZH_NO_SORT"] = "Использовать таблицу по умолчанию";
$MESS["CITRUS_TSZH_MANUAL_SORT"] = "Выбрать список полей вручную";
$MESS["CITRUS_TSZH_TABLE_PARAM"] = "Параметры таблицы";
?>