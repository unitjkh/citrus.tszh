<?
$MESS["TSZH_MODULE_NOT_FOUND_ERROR"] = "Модуль 1С: Сайт ЖКХ не установлен.";
$MESS["TSZH_NOT_A_MEMBER"] = "Вы не являетесь владельцем лицевого счета.";
$MESS["CITRUS_TSZH_METER_INCORRECT_VALUE"] = "#METER_NAME#, тариф #N# — Указано некорректное значение.<br />";
$MESS["CITRUS_TSZH_METER_MUST_BE_LARGER"] = "#METER_NAME#, тариф #N# — Текущее значение не может быть меньше предыдущего.<br />";
$MESS["CITRUS_TSZH_METER_VALUE_MUST_BE_POSITIVE"] = "#METER_NAME#, тариф #N# — Необходимо ввести значение больше нуля.<br />";
$MESS["CITRUS_TSZH_METER_VALUE_MUST_BE_NOT_NEGATIVE"] = "#METER_NAME#, тариф #N# — Необходимо ввести неотрицательное значение.<br />";
$MESS["TSZH_METER_VALUES_SAVED"] = " — Показания сохранены";
$MESS["CITRUS_TSZH_METER_VALUE_CAPACITY_WARNING"] = "Пожалуйста проверьте правильность ввода этих показаний, если счетчик действительно обнулился, отправьте форму еще раз";
$MESS["CITRUS_TSZH_METERS_ERORR_CHANGED_CONFIRED_VALUES"] = "Текущие показания не совпадают с теми, для которых запрашивалось подтверждение!";
?>