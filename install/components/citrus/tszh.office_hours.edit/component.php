<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (!function_exists("htmlspecialcharsArray")):
	function htmlspecialcharsArray($ar)
	{
		if (is_array($ar))
		{
			foreach ($ar as $key => $value)
				$ar[$key] = htmlspecialcharsArray($value);

			return $ar;
		}
		else
			return htmlspecialcharsbx($ar);
	}
endif;

if (!is_array($arParams["~OFFICE_HOURS"]))
{
	if (empty($arParams["~OFFICE_HOURS"]))
		$arParams["OFFICE_HOURS"] = array();
	else
		$arParams["OFFICE_HOURS"] = @unserialize($arParams["~OFFICE_HOURS"]);
}

if (is_array($arParams["OFFICE_HOURS"]))
	$arParams["OFFICE_HOURS"] = htmlspecialcharsArray($arParams["OFFICE_HOURS"]);
else
	$arParams["OFFICE_HOURS"] = array();

$arResult = array(
	"ITEMS" => $arParams["OFFICE_HOURS"],
);

$this->IncludeComponentTemplate();
