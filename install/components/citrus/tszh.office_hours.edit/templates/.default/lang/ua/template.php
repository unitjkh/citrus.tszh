<?
$MESS["T_TITLE_DEL_DEPT_BUTTON"] = "Видалити підрозділ";
$MESS["T_CONFIRM_DEL_DEPT"] = "Видалити підрозділ?";

$MESS["T_DEPT_NAME"] = "Найменування підрозділу";
$MESS["T_DEPT_NAME_HINT"] = "наприклад: <i>бухгалтерія</i>";

$MESS["T_DAYS_OF_WEEK"] = "Дні тижня";
$MESS["T_DAY_HINT"] = "Дні тижня";
$MESS["T_WORK_TIME"] = "Час роботи";
$MESS["T_DELETE"] = "Видалити";

$MESS["T_TITLE_SET_DAY_BUTTON"] = "Задати дні тижня";
$MESS["T_TITLE_SET_HOUR_BUTTON"] = "Задати інтервал годин роботи";
$MESS["T_TITLE_DEL_HOUR_BUTTON"] = "Видалити інтервал годин роботи";
$MESS["T_CONFIRM_DEL_HOUR"] = "Видалити інтервал годин роботи?";
$MESS["T_TITLE_DEL_SCHEDULE_ITEM_BUTTON"] = "Видалити рядок";
$MESS["T_CONFIRM_DEL_SCHEDULE_ITEM"] = "Видалити рядок?";

$MESS["T_D_MON"] = "пн";
$MESS["T_D_TUE"] = "вт";
$MESS["T_D_WED"] = "ср";
$MESS["T_D_THU"] = "чт";
$MESS["T_D_FRI"] = "пт";
$MESS["T_D_SAT"] = "сб";
$MESS["T_D_SUN"] = "нд";

$MESS["T_HOLIDAY"] = "вихідний";
$MESS["T_HOLIDAY_PATTERN"] = "вихідн";

$MESS["T_TITLE_HOURS"] = "годинник";
$MESS["T_TITLE_MINUTES"] = "хвилини";
$MESS["T_HOUSE_RANGE_HINT"] = "наприклад: <i>9:00-18:00</i>";

$MESS["T_ERROR_INVALID_HOUR_INPUT"] = "Все що вводяться значення повинні бути числовими і більше нуля.";
$MESS["T_ERROR_INVALID_HOUR1_INPUT"] = "Годинники початку роботи повинні бути в діапазоні від 0 до 23 включно.";
$MESS["T_ERROR_INVALID_MINUTE1_INPUT"] = "Хвилини початку роботи повинні бути в діапазоні від 0 до 59 включно.";
$MESS["T_ERROR_INVALID_HOUR2_INPUT"] = "Годинники закінчення роботи повинні бути в діапазоні від 0 до 24 включно.";
$MESS["T_ERROR_INVALID_MINUTE2_INPUT"] = "Хвилини закінчення роботи повинні бути в діапазоні від 0 до 59 включно.";
$MESS["T_ERROR_INVALID_MINUTE2_24H_INPUT"] = "Час закінчення роботи не може бути більше 24:00.";
$MESS["T_ERROR_INVALID_HOUR_RANGE"] = "Час закінчення роботи має бути більше часу початку роботи.";

$MESS["T_CHOOSE"] = "Вибрати";
$MESS["T_CLOSE"] = "Закрити";

$MESS["T_ADD_HOUR_BUTTON_CAPTION"] = "Додати інтервал";
$MESS["T_ADD_HOUR_BUTTON_TITLE"] = "Додати інтервал годин роботи";

$MESS["T_ADD_SCHEDULE_ITEM_BUTTON_CAPTION"] = "Додати рядок";
$MESS["T_ADD_SCHEDULE_ITEM_BUTTON_TITLE"] = "Додати рядок для вказівки днів тижня";

$MESS["T_ADD_DEPT_BUTTON_CAPTION"] = "Додати підрозділ";
$MESS["T_ADD_DEPT_BUTTON_TITLE"] = "Додати підрозділ для вказівки розкладу його роботи";

$MESS["CITRUS_MDASH"] = "—";