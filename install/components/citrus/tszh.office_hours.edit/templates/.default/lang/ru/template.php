<?
$MESS["T_TITLE_DEL_DEPT_BUTTON"] = "Удалить подразделение";
$MESS["T_CONFIRM_DEL_DEPT"] = "Удалить подразделение?";

$MESS["T_DEPT_NAME"] = "Наименование подразделения";
$MESS["T_DEPT_NAME_HINT"] = "например: <i>бухгалтерия</i>";

$MESS["T_DAYS_OF_WEEK"] = "Дни недели";
$MESS["T_DAY_HINT"] = "Дни недели";
$MESS["T_WORK_TIME"] = "Время работы";
$MESS["T_DELETE"] = "Удалить";

$MESS["T_TITLE_SET_DAY_BUTTON"] = "Задать дни недели";
$MESS["T_TITLE_SET_HOUR_BUTTON"] = "Задать интервал часов работы";
$MESS["T_TITLE_DEL_HOUR_BUTTON"] = "Удалить интервал часов работы";
$MESS["T_CONFIRM_DEL_HOUR"] = "Удалить интервал часов работы?";
$MESS["T_TITLE_DEL_SCHEDULE_ITEM_BUTTON"] = "Удалить строку";
$MESS["T_CONFIRM_DEL_SCHEDULE_ITEM"] = "Удалить строку?";

$MESS["T_D_MON"] = "пн";
$MESS["T_D_TUE"] = "вт";
$MESS["T_D_WED"] = "ср";
$MESS["T_D_THU"] = "чт";
$MESS["T_D_FRI"] = "пт";
$MESS["T_D_SAT"] = "сб";
$MESS["T_D_SUN"] = "вс";

$MESS["T_HOLIDAY"] = "выходной";
$MESS["T_HOLIDAY_PATTERN"] = "выходн";

$MESS["T_TITLE_HOURS"] = "часы";
$MESS["T_TITLE_MINUTES"] = "минуты";
$MESS["T_HOUSE_RANGE_HINT"] = "например: <i>9:00&mdash;18:00</i>";

$MESS["T_ERROR_INVALID_HOUR_INPUT"] = "Все вводимые значения должны быть числовыми и больше нуля.";
$MESS["T_ERROR_INVALID_HOUR1_INPUT"] = "Часы начала работы должны быть в диапазоне от 0 до 23 включительно.";
$MESS["T_ERROR_INVALID_MINUTE1_INPUT"] = "Минуты начала работы должны быть в диапазоне от 0 до 59 включительно.";
$MESS["T_ERROR_INVALID_HOUR2_INPUT"] = "Часы окончания работы должны быть в диапазоне от 0 до 24 включительно.";
$MESS["T_ERROR_INVALID_MINUTE2_INPUT"] = "Минуты окончания работы должны быть в диапазоне от 0 до 59 включительно.";
$MESS["T_ERROR_INVALID_MINUTE2_24H_INPUT"] = "Время окончания работы не может быть больше 24:00.";
$MESS["T_ERROR_INVALID_HOUR_RANGE"] = "Время окончания работы должно быть больше времени начала работы.";

$MESS["T_CHOOSE"] = "Выбрать";
$MESS["T_CLOSE"] = "Закрыть";

$MESS["T_ADD_HOUR_BUTTON_CAPTION"] = "Добавить интервал";
$MESS["T_ADD_HOUR_BUTTON_TITLE"] = "Добавить интервал часов работы";

$MESS["T_ADD_SCHEDULE_ITEM_BUTTON_CAPTION"] = "Добавить строку";
$MESS["T_ADD_SCHEDULE_ITEM_BUTTON_TITLE"] = "Добавить строку для указания дней недели";

$MESS["T_ADD_DEPT_BUTTON_CAPTION"] = "Добавить подразделение";
$MESS["T_ADD_DEPT_BUTTON_TITLE"] = "Добавить подразделение для указания расписания его работы";

$MESS["CITRUS_MDASH"] = "—";
