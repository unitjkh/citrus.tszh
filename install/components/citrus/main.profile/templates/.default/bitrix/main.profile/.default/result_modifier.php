<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$arResult["CONFIRM_EMAIL_NOTIFICATION_SENT"] = false;
$arResult["IS_DUMMY_MAIL"] = CTszhSubscribe::isDummyMail($arResult["arUser"]["EMAIL"]);

if ($arResult["DATA_SAVED"] != "Y" || !is_object($USER))
	return;

$arChangedEmail = $USER->getParam("citrus.tszh.user.changedEmail");
if (is_array($arChangedEmail) && !empty($arChangedEmail))
{
	$arResult["CONFIRM_EMAIL_NOTIFICATION_SENT"] = $arChangedEmail["IS_NOTIFICATION_SENT"];
	$USER->setParam("citrus.tszh.user.changedEmail", false);
}