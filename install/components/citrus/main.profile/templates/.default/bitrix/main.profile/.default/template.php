<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
	die();
} ?>
<?
CJSCore::Init(array("jquery"));

$optionPhoneAdd = (COption::GetOptionString('citrus.tszh', 'input_phone_add', "Y") == "Y");
$optionPhoneRequired = (COption::GetOptionString('citrus.tszh', 'input_phone_require', "Y") == "Y");

$arShowFields = Array(
	Array(
		'name' => 'EMAIL',
		'required' => true,
		'title' => GetMessage("F_EMAIL") . ':',
		'type' => 'string',
	),
);

if ($optionPhoneAdd == 'Y')
{
	$arShowFields[] = Array(
		'name' => 'PERSONAL_PHONE',
		'required' => ($optionPhoneRequired == 'Y'),
		'title' => GetMessage("USER_PHONE"),
		'type' => 'phone',
	);
}

$arShowFields[] = Array(
	'name' => 'LAST_NAME',
	'title' => GetMessage("LAST_NAME"),
	'type' => 'string',
);

$arShowFields[] = Array(
	'name' => 'NAME',
	'title' => GetMessage("NAME"),
	'type' => 'string',
);

$arShowFields[] = Array(
	'name' => 'SECOND_NAME',
	'title' => GetMessage("SECOND_NAME"),
	'type' => 'string',
);

$arShowFields[] = Array(
	'name' => 'NEW_PASSWORD',
	'title' => GetMessage("F_PASSWORD") . ':',
	'type' => 'password',
);

$arShowFields[] = Array(
	'name' => 'NEW_PASSWORD_CONFIRM',
	'title' => GetMessage("F_CONFIRM_PASSWORD") . ':',
	'type' => 'password',
);
?>

<h2><?=GetMessage("F_TITLE")?></h2>
<?
if (strlen($arResult["strProfileError"]) > 0)
{
	echo ShowError($arResult["strProfileError"]);
}

if ($arResult['DATA_SAVED'] == 'Y')
{
	ShowNote(GetMessage('PROFILE_DATA_SAVED'));

	if ($arResult["CONFIRM_EMAIL_NOTIFICATION_SENT"])
	{
		ShowNote(GetMessage("PROFILE_CONFIRM_EMAIL_NOTIFICATION_SENT", array("#EMAIL#" => $arResult["arUser"]["EMAIL"])));
	}
}
?>
<form method="post" name="form1" action="<?=$arResult["FORM_TARGET"]?>?"
      enctype="multipart/form-data" <?=($optionPhoneAdd && $optionPhoneRequired) ? 'onsubmit="return checkEmptyPhone()"' : ''?>>
	<?=$arResult["BX_SESSION_CHECK"]?>
	<input type="hidden" name="lang" value="<?=LANG?>"/>
	<input type="hidden" name="ID" value=<?=$arResult["ID"]?>/>

	<table class="profile-table data-table">
		<tbody>
		<?
		if ($arResult["ID"] > 0)
		{
		$bHiddenLogin = true;
		$bHasRequiredFields = false;
		$strFooterNote = '';
		foreach ($arShowFields as $idx => $arField)
		{

			$bHasRequiredFields = $bHasRequiredFields || $arField['required'];
			$bHiddenLogin = ($arField['NAME'] == 'LOGIN' ? false : $bHiddenLogin);
			$currentValue = array_key_exists('value', $arField) ? $arField['value'] : $arResult["arUser"][$arField['name']];

			$strFieldNote = ($arField['required'] ? '<span class="starrequired">*</span>' : '');
			if ($arField["name"] == 'NEW_PASSWORD')
			{
				$strFieldNote .= '<sup class="starrequired" style="font-size: 11px; font-weight: normal;" title="' . $arResult['GROUP_POLICY']["PASSWORD_REQUIREMENTS"] . '">1</sup>';
				$strFooterNote .= '<p><sup class="starrequired" style="font-size: 11px; font-weight: normal;">1</sup> ? ' . $arResult['GROUP_POLICY']["PASSWORD_REQUIREMENTS"] . '</p>';
			}

			switch ($arField['type'])
			{
				case 'textarea':
					$html = "<textarea name=\"{$arField['name']}\" id=\"f_{$idx}\" class=\"input\" />{$currentValue}</textarea>";
					break;

				case 'password':
					$html = "<input type=\"password\" name=\"{$arField['name']}\" value=\"\" id=\"f_{$idx}\" class=\"input\" />";
					break;

				case 'phone':
					if ($arField['type'] == "phone")
					{
						$phoneId = 'f_' . $idx;
					}
					$html = '<input type="text" name="customer_phone" id="customer_phone" value="' . $currentValue . '" class="input"></br>';
					$html .= '<input type="hidden" id="phone_mask" checked=""><label id="descr" for="phone_mask"></label>';
					$html .= '<font style="display: none; color: red;" id="errorphone">' . GetMessage("PROFILE_PHONE_REQUIRED") . '</font>';
					$html .= '<input type="hidden" name="' . $arField['name'] . '" id="' . $phoneId . '" value="' . $currentValue . '">';
					break;

				default:
					$html = "<input type=\"text\" name=\"{$arField['name']}\" value=\"" . ($arField["name"] == "EMAIL" && $arResult["IS_DUMMY_MAIL"] ? '' : $currentValue) . "\" id=\"f_{$idx}\" class=\"input\" />";
					if ($arField["name"] == "EMAIL" && !$arResult["IS_DUMMY_MAIL"])
					{
						$html = "<table id=\"profile-email-table\"><tr><td>{$html}</td><td class=\"icon " . ($arResult["arUser"]["UF_TSZH_EML_CNFRM_RQ"] ? 'nonconfirmed' : 'confirmed') . '" title="' . (getMessage($arResult["arUser"]["UF_TSZH_EML_CNFRM_RQ"] ? "PROFILE_EMAIL_NONCONFIRMED" : "PROFILE_EMAIL_CONFIRMED")) . "\"></td></tr></table>";
					}
					break;
			}
			?>
			<tr>
				<td class="profile-table-field-title"><?=$strFieldNote?><label for="f_<?=$idx?>"><?=$arField['title']?></label></td>
				<td class="profile-table-field"><?=$html?></td>
			</tr>
			<?
		}


		if ($bHasRequiredFields || strlen($strFooterNote) > 0)
		{
			?>
			<tr>
				<td>&nbsp;</td>
				<td><?=$strFooterNote?><span class="starrequired">*</span><?=GetMessage('F_REQUIRED_FIELDS')?></td>
			</tr>
			<?
		}
		?>
		<tr>
			<td>&nbsp;</td>
			<td><?

				if ($bHiddenLogin)
				{
					?><input type="hidden" name="LOGIN" value="<?=$arResult['arUser']['LOGIN']?>" /><?
				}
				$attr = array('name' => 'save', 'value' => '1');
				if ($optionPhoneAdd)
				{
					if ($optionPhoneRequired)
					{
						$attr['onclick'] = 'checkEmptyPhone()';
					}
					else
					{
						$attr['onclick'] = 'getValuePhone()';
					}
				}
				TemplateShowButton(Array('title' => GetMessage("MAIN_SAVE"), 'type' => 'submit', 'attr' => $attr));
				?></td>
		</tr>
	</table>
<?
}
/*
	// ******************** /User properties ***************************************************?>
	<p><?echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?></p>*/ ?>
</form>


<?
if ($optionPhoneAdd)
{
	if ($optionPhoneRequired)
	{
		?>
		<script type="text/javascript">
            function checkEmptyPhone() {
                document.getElementById('<?= $phoneId ?>').value = document.getElementById('customer_phone').value;
                if (document.getElementById('<?= $phoneId ?>').value.length > 0)
                    return true;

                showEmptyPhone(document.getElementById('errorphone'));
                return false;
            }

            function showEmptyPhone(elem) {
                elem.style.display = 'block';
            }
		</script>
		<?
	}
	else
	{
		?>
		<script type="text/javascript">
            function getValuePhone() {
                document.getElementById('<?= $phoneId ?>').value = document.getElementById('customer_phone').value;
            }
		</script>

		<?
	}
}
?>

<script>
    var listCountries = $.masksSort($.masksLoad("<?= $this->GetFolder() ?>/json/phone-codes.json"), ['#'], /[0-9]|#/, "mask");
    var maskOpts = {
        inputmask: {
            definitions: {
                '#': {
                    validator: "[0-9]",
                    cardinality: 1
                }
            },
            showMaskOnHover: false,
            autoUnmask: true,
            clearMaskOnLostFocus: false
        },
        match: /[0-9]/,
        replace: '#',
        listKey: "mask"
    };

    var maskChangeWorld = function (maskObj, determined) {
        if (determined) {
            var hint = maskObj.name_ru;
            if (maskObj.desc_ru && maskObj.desc_ru != "") {
                hint += " (" + maskObj.desc_ru + ")";
            }
            $("#descr").html(hint);
        } else {
            $("#descr").html("");
        }
    }

    $('#phone_mask, input[name="mode"]').change(function () {
        $('#customer_phone').inputmasks($.extend(true, {}, maskOpts, {
            list: listCountries,
            onMaskChange: maskChangeWorld
        }));
    });

    $('#phone_mask').change();
</script>