<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var CBitrixComponent $component ������� ��������� ��������� */
/** @var CBitrixComponentTemplate $this ������� ������ (������, ����������� ������) */
/** @var array $arResult ������ ����������� ������ ���������� */
/** @var array $arParams ������ �������� ���������� ����������, ����� �������������� ��� ����� �������� ���������� ��� ������ ������� (��������, ����������� ��������� ����������� ��� ������). */
/** @var string $templateFile ���� � ������� ������������ ����� �����, �������� /bitrix/components/bitrix/iblock.list/templates/.default/template.php) */
/** @var string $templateName ��� ������� ���������� (��������: .d�fault) */
/** @var string $templateFolder ���� � ����� � �������� �� DOCUMENT_ROOT (�������� /bitrix/components/bitrix/iblock.list/templates/.default) */
/** @var array $templateData ������ ��� ������, �������� ��������, ����� ������� ����� �������� ������ �� template.php � ���� component_epilog.php, ������ ��� ������ �������� � ���, �.�. ���� component_epilog.php ����������� �� ������ ���� */
/** @var string $parentTemplateFolder ����� ������������� �������. ��� ����������� �������������� ����������� ��� �������� (��������) ������ ������������ ��� ����������. �� ����� ��������� ��� ������������ ������� ���� ������������ ����� ������� */
/** @var string $componentPath ���� � ����� � ����������� �� DOCUMENT_ROOT (����. /bitrix/components/bitrix/iblock.list) */

ShowMessage($arParams["~AUTH_RESULT"]);

// ����� ��������� ��������� ������ �� ����� ���������� ����� ����� ������ ���
if (is_array($arParams["~AUTH_RESULT"]) && $arParams["~AUTH_RESULT"]["TYPE"] == "OK")
	return;
?>
<form method="post" action="<?=$arResult["AUTH_FORM"]?>" id="auth-form" class="change-password">
	<?if (strlen($arResult["BACKURL"]) > 0): ?>
	<input type="hidden" name="backurl" value="<?=$arResult["BACKURL"]?>" />
	<? endif ?>
	<input type="hidden" name="AUTH_FORM" value="Y" />
	<input type="hidden" name="TYPE" value="CHANGE_PWD" />
	<?
	$wasAutofocus = false;

	if (strlen(trim($arResult["LAST_LOGIN"])))
	{
		?>
		<input type="hidden" name="USER_LOGIN" value="<?=$arResult["LAST_LOGIN"]?>">
		<p><?=GetMessage("AUTH_CHANGE_PASSWORD_FOR_USER", array("#LOGIN#" => $arResult["LAST_LOGIN"]))?></p>
		<?
	}
	else
	{
		?>
		<fieldset>
			<label for="user-login"><?=GetMessage("AUTH_LOGIN")?><span class="starrequired">*</span></label>
			<input type="text" name="USER_LOGIN" maxlength="50" value="<?=$arResult["LAST_LOGIN"]?>" class="styled" id="user-login"<?=($wasAutofocus ? '' : ' autofocus')?>>
		</fieldset>
		<?
		$wasAutofocus = true;
	}

	if (strlen(trim($arResult["USER_CHECKWORD"])))
	{
		?>
		<input type="hidden" name="USER_CHECKWORD" value="<?=$arResult["USER_CHECKWORD"]?>">
		<?
	}
	else
	{
		?>
		<fieldset>
			<label for="user-checkword"><?=GetMessage("AUTH_CHECKWORD")?><span class="starrequired">*</span></label>
			<input type="text" name="USER_CHECKWORD" maxlength="50" value="<?=$arResult["USER_CHECKWORD"]?>" class="styled" id="user-checkword"<?=($wasAutofocus ? '' : ' autofocus')?>>
		</fieldset>
		<?
		$wasAutofocus = true;
	}
	?>
	<fieldset>
		<label for="user-password"><?=GetMessage("AUTH_NEW_PASSWORD_REQ")?><span class="starrequired">*</span></label>
		<input type="password" name="USER_PASSWORD" maxlength="50" value="<?=$arResult["USER_PASSWORD"]?>" class="styled" id="user-password"<?=($wasAutofocus ? '' : ' autofocus')?>>
	</fieldset>
	<fieldset>
		<label for="user-confirm-password"><?=GetMessage("AUTH_NEW_PASSWORD_CONFIRM")?><span class="starrequired">*</span></label>
		<input type="password" name="USER_CONFIRM_PASSWORD" maxlength="50" value="<?=$arResult["USER_CONFIRM_PASSWORD"]?>" class="styled" id="user-confirm-password">
	</fieldset>
	<? if ($arResult["USE_CAPTCHA"]): ?>
		<fieldset>
			<label for="captcha_sid">&nbsp;</label>
			<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>"/>
			<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" id="CAPTCHA"/>
		</fieldset>
		<fieldset>
			<label for="captcha_word"><?=GetMessage("AUTH_CAPTCHA")?></label>
			<input type="text" name="captcha_word" maxlength="50" id="captcha_word" class="styled"/>
		</fieldset>
	<? endif ?>

	<fieldset>
		<label></label>
		<?
		if (function_exists("TemplateShowButton"))
			TemplateShowButton(array(
				"type" => "submit",
				"title" => GetMessage("AUTH_CHANGE"),
				"attr" => array(
					"name" => "change_pwd",
					"value" => GetMessage("AUTH_CHANGE"),
				),
			));
		else
		{
			?>
			<button type="submit" class="submit" name="change_pwd" value="<?=GetMessage("AUTH_CHANGE")?>"><?=GetMessage("AUTH_CHANGE")?></button>
			<?
		}
		?>
	</fieldset>

	<p class="links">
		<?echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"];?><br />
		<span class="starrequired">*</span><?=GetMessage("AUTH_REQ")?><br />
		<a href="<?=$arResult["AUTH_AUTH_URL"]?>"><b><?=GetMessage("AUTH_AUTH")?></b></a>
	</p>

</form>
<script type="text/javascript">
document.getElementById('auth-form').USER_LOGIN.focus();
</script>