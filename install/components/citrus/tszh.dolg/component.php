<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

if (!CModule::IncludeModule('citrus.tszh'))
{
	ShowError(GetMessage("TSZH_MODULE_NOT_FOUND_ERROR"));

	return;
}

if (!CTszhFunctionalityController::CheckEdition())
{
	return;
}

$arParams["ORDER"] = Array();
for ($i = 0; $i <= 3; $i++)
{
	if (array_key_exists("SORT_BY" . $i, $arParams) && array_key_exists("SORT_ORDER" . $i, $arParams))
	{
		$arParams["ORDER"][$arParams["SORT_BY" . $i]] = $arParams["SORT_ORDER" . $i];
	}
}
if (count($arParams["ORDER"]) <= 0)
{
	$arParams["ORDER"]["DEBT_END"] = "DESC";
}

$arResult["TSZH_LINKS"] = Array();
$arResult["TSZH"] = false;
if (array_key_exists("TSZH_ID", $arParams) && IntVal($arParams["TSZH_ID"]) > 0)
{
	$arResult["TSZH"] = CTszh::GetByID($arParams["TSZH_ID"]);
}
else
{
	$rsTszhs = CTszh::GetList(
		Array("NAME" => "ASC", "ID" => "ASC"),
		Array("SITE_ID" => SITE_ID)
	);
	$arCurSiteTszhs = array();
	while ($arTszh = $rsTszhs->getNext())
	{
		$arCurSiteTszhs[$arTszh["ID"]] = $arTszh;
	}

	if (!empty($arCurSiteTszhs))
	{
		$tszhID = false;
		if (is_set($_GET, "tszh_id"))
		{
			$tszhID = intval($_GET["tszh_id"]);
		}

		if ($tszhID > 0)
		{
			$arResult["TSZH"] = $arCurSiteTszhs[$tszhID];
		}
		elseif (count($arCurSiteTszhs) == 1)
		{
			$arResult["TSZH"] = array_shift($arCurSiteTszhs);
		}
		else
		{
			foreach ($arCurSiteTszhs as $tszhID => $arTszh)
			{
				$arResult["TSZH_LINKS"][$tszhID] = $arTszh["NAME"];
			}

			$arResult["TSZH"] = array_shift($arCurSiteTszhs);
		}
	}
}

if (!is_array($arResult["TSZH"]))
{
	ShowError(GetMessage("COMP_ERROR_TSZH_NOT_FOUND"));

	return;
}

if (!function_exists("checkDebtEndWithoutCharges")):
	function checkDebtEndWithoutCharges($arAccount, $arCharges, $minDebt)
	{
		$result = false;

		$debt = floatval($arAccount["DEBT_END"]) - floatval($arCharges[$arAccount["ACCOUNT_ID"]]);
		if ($debt >= floatval($minDebt))
		{
			$result = $debt;
		}

		return $result;
	}
endif;

if (!function_exists("cmpDebtEndWithoutCharges")):
	function cmpDebtEndWithoutCharges($a, $b)
	{
		global $arOrder;

		$arFields = array_merge($arOrder, array("DEBT_END_WITHOUT_CHARGES" => "desc"));
		unset($arFields["DEBT_BEG"]);
		unset($arFields["DEBT_END"]);
		foreach ($arFields as $field => $sort)
		{
			$A = $a[$field];
			$B = $b[$field];
			if (in_array($field, array("HOUSE", "FLAT", "DEBT_END_WITHOUT_CHARGES")))
			{
				$A = floatval($A);
				$B = floatval($B);
			}

			if ($A == $B)
			{
				if ($a[$field] != $b[$field])
				{
					return $a[$field] < $b[$field] ? -1 : 1;
				}

				continue;
			}

			$res = $A < $B ? -1 : 1;
			if ($field == "DEBT_END_WITHOUT_CHARGES")
			{
				$res = -$res;
			}

			return $res;
		}

		return $a["XML_ID"] < $b["XML_ID"] ? -1 : 1;
	}
endif;

$arParams["COLS_COUNT"] = IntVal($arParams["COLS_COUNT"]) > 0 ? IntVal($arParams["COLS_COUNT"]) : 4;
$arParams["MAX_COUNT"] = IntVal($arParams["MAX_COUNT"]) > 0 ? IntVal($arParams["MAX_COUNT"]) : 15;
$arParams["MIN_DEBT"] = FloatVal($arParams["MIN_DEBT"]) > 0 ? FloatVal($arParams["MIN_DEBT"]) : 1000.0;

$arParams['GROUP_BY_SUMMARY'] = strtoupper($arParams['GROUP_BY_SUMMARY']);
$arParams['GROUP_BY_SUMMARY'] = in_array($arParams['GROUP_BY_SUMMARY'], Array(
	'COUNT',
	'SUM',
	'AVG',
	'MIN',
	'MAX'
)) ? $arParams['GROUP_BY_SUMMARY'] : false;

$arParams["DISPLAY_TOP_PAGER"] = $arParams["DISPLAY_TOP_PAGER"] == "Y";
$arParams["DISPLAY_BOTTOM_PAGER"] = $arParams["DISPLAY_BOTTOM_PAGER"] != "N";
$arParams["PAGER_TITLE"] = trim($arParams["PAGER_TITLE"]);
$arParams["PAGER_SHOW_ALWAYS"] = $arParams["PAGER_SHOW_ALWAYS"] != "N";
$arParams["PAGER_TEMPLATE"] = trim($arParams["PAGER_TEMPLATE"]);
$arParams["PAGER_DESC_NUMBERING"] = $arParams["PAGER_DESC_NUMBERING"] == "Y";
$arParams["PAGER_DESC_NUMBERING_CACHE_TIME"] = intval($arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]);
$arParams["PAGER_SHOW_ALL"] = $arParams["PAGER_SHOW_ALL"] !== "N";

if (strlen($arParams["FILTER_NAME"]) <= 0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
{
	$arrFilter = array();
}
else
{
	$arrFilter = $GLOBALS[$arParams["FILTER_NAME"]];
	if (!is_array($arrFilter))
	{
		$arrFilter = array();
	}
}

if ($arParams["DISPLAY_TOP_PAGER"] || $arParams["DISPLAY_BOTTOM_PAGER"])
{
	$arNavParams = array(
		"nPageSize" => $arParams["MAX_COUNT"],
		"bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
		"bShowAll" => $arParams["PAGER_SHOW_ALL"],
	);
	$arNavigation = CDBResult::GetNavParams($arNavParams);
	if ($arNavigation["PAGEN"] == 0 && $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"] > 0)
	{
		$arParams["CACHE_TIME"] = $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"];
	}
}
else
{
	$arNavParams = array(
		"nTopCount" => $arParams["MAX_COUNT"],
		"bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
	);
	$arNavigation = false;
}

$arAvailableFields = Array(
	"XML_ID" => GetMessage("FL_XML_ID"),
	"TIMESTAMP_X" => GetMessage("FL_TIMESTAMP_X"),
	"PERIOD" => GetMessage("FL_PERIOD_DATE"),
	"USER_LAST_NAME" => GetMessage("FL_USER_LAST_NAME"),
	"USER_NAME" => GetMessage("FL_USER_NAME"),
	"USER_SECOND_NAME" => GetMessage("FL_USER_SECOND_NAME"),
	"DEBT_BEG" => GetMessage("FL_DEBT_BEG"),
	"DEBT_END" => GetMessage("FL_DEBT_END"),
	"DEBT_END_WITHOUT_CHARGES" => GetMessage("FL_DEBT_END_WITHOUT_CHARGES"),
	"DISTRICT" => GetMessage("FL_DISTRICT"),
	"CITY" => GetMessage("FL_CITY"),
	"SETTLEMENT" => GetMessage("FL_SETTLEMENT"),
	"STREET" => GetMessage("FL_STREET"),
	"HOUSE" => GetMessage("FL_HOUSE"),
	"FLAT" => GetMessage("FL_FLAT"),
	"USER_FULL_NAME" => GetMessage("FL_USER_FULL_NAME"),
	"USER_FULL_ADDRESS" => GetMessage("FL_USER_FULL_ADDRESS"),
);

// access
if (is_array($arParams['ACCESS_GROUPS']) && count($arParams['ACCESS_GROUPS']) > 0)
{
	$arHasAccess = array_intersect($USER->GetUserGroupArray(), $arParams['ACCESS_GROUPS']);
	$bHasAccess = is_array($arHasAccess) && count($arHasAccess) > 0;
	if (!($USER->IsAdmin() || $bHasAccess))
	{
		foreach ($arAvailableFields as $strField => $strTitle)
		{
			if (in_array($strField, $arParams['ACCESS_FIELDS']))
			{
				unset($arAvailableFields[$strField]);
			}
		}
	}
}

$arResult["FIELDS"] = Array();
for ($i = 0; $i < $arParams["COLS_COUNT"]; $i++)
{
	$fieldCode = $arParams["COL_$i"];
	if (!array_key_exists($fieldCode, $arAvailableFields))
	{
		continue;
	}
	$arResult["FIELDS"][] = Array(
		"CODE" => $fieldCode,
		"TITLE" => strlen($arAvailableFields[$fieldCode]) > 0 ? $arAvailableFields[$fieldCode] : $arParams["COL_$i"],
	);
}

$arFilter = Array(
	"ACTIVE" => "Y",
	"TSZH_ID" => $arResult["TSZH"]["ID"],
);

$arPeriod = CTszhPeriod::GetList(Array("DATE" => "DESC", "ID" => "DESC"), $arFilter)->Fetch();
$iLastPeriod = is_array($arPeriod) ? $arPeriod["ID"] : false;

$arResult["ACCOUNTS"] = Array();
$arExcludeAccounts = array();
if ($iLastPeriod)
{
	$summField = false;
	foreach ($arResult["FIELDS"] as $field)
	{
		if (substr($field["CODE"], 0, 5) == 'DEBT_')
		{
			$summField = $field["CODE"];
		}
	}
	if (false === $summField)
	{
		$summField = "DEBT_END";
	}

	if ($summField == "DEBT_END_WITHOUT_CHARGES" || $arParams["GROUP_BY_SUMMARY_FIELD"] == "DEBT_END_WITHOUT_CHARGES")
	{
		$rsCharges = CTszhCharge::GetList(
			Array(),
			Array(
				"PERIOD_ID" => $iLastPeriod,
				"COMPONENT" => "N",
				"DEBT_ONLY" => "N",
			),
			array("ACCOUNT_ID", "SUM" => "SUMM"),
			false
		);
		$arCharges = array();
		while ($arCharge = $rsCharges->GetNext())
		{
			$arCharges[$arCharge["ACCOUNT_ID"]] = $arCharge["SUMM"];
		}
	}

	global $arOrder;
	$arOrder = $arParams["ORDER"];
	if ($summField != "DEBT_END_WITHOUT_CHARGES")
	{
		$arOrder[$summField] = "DESC";
	}

	$arAddressFields = array("REGION", "DISTRICT", "CITY", "SETTLEMENT", "STREET", "HOUSE", "FLAT");
	if ($arParams["GROUP_BY"])
	{
		$arGroupFields = array();
		switch ($arParams["GROUP_BY"])
		{
			case 'STREET':
				$arGroupFields = array_diff($arAddressFields, Array('HOUSE', 'FLAT'));
				break;
			case 'HOUSE':
				$arGroupFields = array_diff($arAddressFields, Array('FLAT'));
				break;
			case 'FLAT':
				$arGroupFields = array_merge($arAddressFields, $arGroupOrder);
				break;
			default:
				$arGroupFields = array($arParams['GROUP_BY']);
				break;
		}

		$arGroupOrder = array();
		foreach ($arGroupFields as $field)
		{
			$arGroupOrder[$field] = 'asc';
		}
		$arOrder = array_merge($arGroupOrder, $arOrder);
		$groupByPrev = false;
		$arResult["GROUPS"] = Array();
	}

	$arPersonalFields = array(
		"ACCOUNT_NAME",
		"USER_LAST_NAME",
		"USER_NAME",
		"USER_SECOND_NAME",
		"USER_FULL_NAME",
	);

	$arFilter = array('PERIOD_ID' => $iLastPeriod);
	if ($summField != "DEBT_END_WITHOUT_CHARGES")
	{
		$arFilter[">{$summField}"] = $arParams["MIN_DEBT"];
	}

	if (array_key_exists('HOUSE_ID', $arParams) && is_array($arParams['HOUSE_ID']) && count($arParams['HOUSE_ID']) > 0)
	{
		$arFilter['@HOUSE_ID'] = $arParams['HOUSE_ID'];
	}

	if (isset($arParams['RECEIPT_TYPE_ENABLE']) && $arParams['RECEIPT_TYPE_ENABLE'] == 'Y')
	{
		$arFilter['@TYPE'] = $arParams['RECEIPT_TYPE'];
	}

	// �������� ������� ����� � �������������� �� ����� �������
	$rsAccounts = CTszhAccountPeriod::GetTotalDebt(array_merge($arFilter, $arrFilter), $arOrder);
	$rsAccounts->NavStart($arParams["MAX_COUNT"]);

	if ($summField == "DEBT_END_WITHOUT_CHARGES")
	{
		$arAccounts = array();
		while ($arAccount = $rsAccounts->GetNext())
		{
			$debt = checkDebtEndWithoutCharges($arAccount, $arCharges, $arParams["MIN_DEBT"]);
			if ($debt === false)
			{
				continue;
			}
			$arAccount["DEBT_END_WITHOUT_CHARGES"] = $debt;

			$arAccounts[] = $arAccount;
		}
		unset($rsAccounts);
		usort($arAccounts, "cmpDebtEndWithoutCharges");

		$rsAccounts = new CDBResult;
		$rsAccounts->InitFromArray($arAccounts);
		unset($arAccounts);
		$rsAccounts->navStart($arParams["MAX_COUNT"]);
	}

	while ($arAccount = $rsAccounts->GetNext())
	{
		//		echo '<pre>'; print_r($arAccount); echo '</pre>';
		if ($arParams["GROUP_BY_SUMMARY_FIELD"] == "DEBT_END_WITHOUT_CHARGES" && !is_set($arAccount, "DEBT_END_WITHOUT_CHARGES"))
		{
			$debt = checkDebtEndWithoutCharges($arAccount, $arCharges, $arParams["MIN_DEBT"]);
			$arAccount["DEBT_END_WITHOUT_CHARGES"] = $debt;
		}

		$arAccount["USER_FULL_ADDRESS"] = CTszhAccount::GetFullAddress($arAccount, Array("REGION"));
		$arAccount["USER_FULL_NAME"] = $arAccount["ACCOUNT_NAME"];
		$arAccount["PERIOD"] = CTszhPeriod::Format($arAccount["PERIOD_DATE"]);

		$arResult["ACCOUNTS"][] = $arAccount;
		$arExcludeAccounts[$arAccount["ACCOUNT_ID"]] = "";
		if ($arParams["GROUP_BY"])
		{
			switch ($arParams['GROUP_BY'])
			{
				case 'STREET':
					$arExcludeFields = array('HOUSE', 'FLAT');
					$groupValue = CTszhAccount::GetFullAddress($arAccount, $arExcludeFields);
					$arGroupFields = array_diff($arAddressFields, $arExcludeFields);
					break;
				case 'HOUSE':
					$arExcludeFields = array('FLAT');
					$groupValue = CTszhAccount::GetFullAddress($arAccount, $arExcludeFields);
					$arGroupFields = array_diff($arAddressFields, $arExcludeFields);
					break;
				case 'FLAT':
					$groupValue = CTszhAccount::GetFullAddress($arAccount);
					$arGroupFields = $arAddressFields;
					break;
				default:
					$groupValue = $arAccount[$arParams["GROUP_BY"]];
					$arGroupFields = array($arParams["GROUP_BY"]);
					break;
			}

			if (!array_key_exists($groupValue, $arResult['GROUPS']))
			{
				$summary = $summary_title = false;
				if ($arParams['GROUP_BY_SUMMARY'])
				{
					$arGroupFilter = array();
					foreach ($arGroupFields as $field)
					{
						$arGroupFilter[$field] = $arAccount[$field];
					}

					if ($arParams["GROUP_BY_SUMMARY_FIELD"] == "DEBT_END_WITHOUT_CHARGES")
					{
						$rsSummary = CTszhAccountPeriod::GetList(
							Array(),
							array_merge(array("PERIOD_ID" => $iLastPeriod), $arGroupFilter, $arrFilter),
							false,
							false,
							Array("*")
						);
						$count = $sum = 0;
						$min = $max = false;
						while ($ar = $rsSummary->Fetch())
						{
							$debt = checkDebtEndWithoutCharges($ar, $arCharges, $arParams["MIN_DEBT"]);
							if ($debt === false)
							{
								continue;
							}

							$count++;
							$sum += $debt;
							$min = $min === false ? $debt : min($min, $debt);
							$max = $max === false ? $debt : max($max, $debt);
						}
						$summary = 0;
						switch ($arParams["GROUP_BY_SUMMARY"])
						{
							case "COUNT":
								$summary = $count;
								break;
							case "SUM":
								$summary = $sum;
								break;
							case "AVG":
								if ($count > 0)
								{
									$summary = $sum / $count;
								}
								break;
							case "MIN":
								if ($min !== false)
								{
									$summary = $min;
								}
								break;
							case "MAX":
								if ($max !== false)
								{
									$summary = $max;
								}
								break;
						}

						if (is_numeric($summary) && floor($summary) != $summary)
						{
							$summary = number_format($summary, 2, ',', ' ');
						}

						$summary_title = GetMessage("FL_" . $arParams["GROUP_BY_SUMMARY_FIELD"]) . ', ' . GetMessage("COMP_GROUP_SUMMARY_F_" . $arParams['GROUP_BY_SUMMARY']);
					}
					else
					{
						$rsSummary = CTszhAccountPeriod::GetList(
						// sort
							Array(),
							// filter
							array_merge(array(
								"PERIOD_ID" => $iLastPeriod,
								">ACCOUNT_ID" => "0",
								">DEBT_END" => $arParams["MIN_DEBT"],
							), $arGroupFilter, $arrFilter, $arFilter),
							// group
							Array($arParams['GROUP_BY_SUMMARY'] => $arParams["GROUP_BY_SUMMARY_FIELD"]),
							false,
							Array($arParams["GROUP_BY"], 'DEBT_END')
						);
						if ($ar = $rsSummary->Fetch())
						{
							$summary = $ar[$arParams["GROUP_BY_SUMMARY_FIELD"]];
							if (is_numeric($summary) && floor($summary) != $summary)
							{
								$summary = number_format($summary, 2, ',', ' ');
							}
							$summary_title = GetMessage("FL_" . $arParams["GROUP_BY_SUMMARY_FIELD"]) . ', ' . GetMessage("COMP_GROUP_SUMMARY_F_" . $arParams['GROUP_BY_SUMMARY']);
						}
					}
				}

				$arResult["GROUPS"][$groupValue] = Array(
					'TSZH_ID' => $arAccount["PERIOD_TSZH_ID"],
					'TSZH_NAME' => $arResult["TSZH_LINKS"][$arAccount["PERIOD_TSZH_ID"]],
					'TITLE' => $groupValue,
					'SUMMARY' => $summary,
					'SUMMARY_TITLE' => $summary_title,
					'ITEMS' => Array(),
				);
			}

			$arResult['GROUPS'][$groupValue]['ITEMS'][] = $arAccount;
		}
	}

	$arResult["NAV_STRING"] = $rsAccounts->GetPageNavStringEx($navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);
}

// �������� �� ������� ����������������� ���� ���������� �� ������ ���������
$arResult["ACCOUNTS_EXCLUDED"][] = array();
$arExistingUserType = CUserTypeEntity::GetList(Array(), Array("ENTITY_ID" => 'TSZH_ACCOUNT', "FIELD_NAME" => 'UF_DEBTORS_EXCLUDED'))->Fetch();
if (is_array($arExistingUserType))
{
	$arFilterAcc['UF_DEBTORS_EXCLUDED'] = "1";
	$arFilterAcc['@ID'] = array_keys($arExcludeAccounts);
	$rsAccounts = \CTszhAccount::GetList(
		array(),
		$arFilterAcc
	);
	while ($arAccountEx = $rsAccounts->GetNext())
	{
		$arResult["ACCOUNTS_EXCLUDED"][] = $arAccountEx["ID"];
	}
}

$this->IncludeComponentTemplate();