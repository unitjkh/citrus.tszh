<?
$MESS["CITRUS_MYMS_PARAM_INIT_MAP_TYPE"] = "Стартовий тип карти";
$MESS["CITRUS_MYMS_PARAM_INIT_MAP_TYPE_MAP"] = "схема";
$MESS["CITRUS_MYMS_PARAM_INIT_MAP_TYPE_SATELLITE"] = "супутник";
$MESS["CITRUS_MYMS_PARAM_INIT_MAP_TYPE_HYBRID"] = "гібрид";
$MESS["CITRUS_MYMS_PARAM_INIT_MAP_TYPE_PUBLIC"] = "народна карта";
$MESS["CITRUS_MYMS_PARAM_INIT_MAP_TYPE_PUBLIC_HYBRID"] = "народний гібрид";
$MESS["CITRUS_MYMS_PARAM_OPTIONS"] = "Налаштування";
$MESS["CITRUS_MYMS_PARAM_OPTIONS_ENABLE_SCROLL_ZOOM"] = "зміна масштабу колесом миші";
$MESS["CITRUS_MYMS_PARAM_OPTIONS_ENABLE_DBLCLICK_ZOOM"] = "зміна масштабу подвійним клацанням миші";
$MESS["CITRUS_MYMS_PARAM_OPTIONS_ENABLE_RIGHT_MAGNIFIER"] = "зміна масштабу правою кнопкою миші";
$MESS["CITRUS_MYMS_PARAM_OPTIONS_ENABLE_DRAGGING"] = "перетягування карти";
$MESS["CITRUS_MYMS_PARAM_OPTIONS_ENABLE_HOTKEYS"] = "гарячі клавіші";
$MESS["CITRUS_MYMS_PARAM_OPTIONS_ENABLE_RULER"] = "інструмент \"лінійка\"";
$MESS["CITRUS_MYMS_PARAM_CONTROLS"] = "Елементи управління";
$MESS["CITRUS_MYMS_PARAM_CONTROLS_TOOLBAR"] = "Панель інструментів";
$MESS["CITRUS_MYMS_PARAM_CONTROLS_ZOOM"] = "Повзунок масштабу";
$MESS["CITRUS_MYMS_PARAM_CONTROLS_SMALLZOOM"] = "Кнопки масштабу";
$MESS["CITRUS_MYMS_PARAM_CONTROLS_MINIMAP"] = "Міні-карта";
$MESS["CITRUS_MYMS_PARAM_CONTROLS_TYPECONTROL"] = "Тип карти";
$MESS["CITRUS_MYMS_PARAM_CONTROLS_SCALELINE"] = "Шкала масштабу";
$MESS["CITRUS_MYMS_PARAM_CONTROLS_SEARCH"] = "Пошук по мапі";
$MESS["CITRUS_MYMS_PARAM_MAP_HEIGHT"] = "Висота карти";
$MESS["CITRUS_MYMS_PARAM_MAP_WIDTH"] = "Ширина карти";
$MESS["CITRUS_MYMS_PARAM_MAP_ID"] = "Ідентифікатор карти";
$MESS["CITRUS_MYMS_PARAM_ADDRESS"] = "Адреса для відображення";
$MESS["CITRUS_MYMS_PARAM_ADDRESS_DEFAULT"] = "";
$MESS["CITRUS_MYMS_PARAM_NAME"] = "Ім'я для відображення";
$MESS["CITRUS_MYMS_PARAM_BODY"] = "Текст для відображення";
?>