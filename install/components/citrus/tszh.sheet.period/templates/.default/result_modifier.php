<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams ���������, ������, ���������. �� ����������� ����������� ���� ����������, �� ��������� ��� ������ ��  � ����� template.php. */
/** @var array $arResult ���������, ������/���������. ����������� ����������� ���� ������ ����������. */
/** @var CBitrixComponentTemplate $this ������� ������ (������, ����������� ������) */

if (!empty($arResult['PERIODS'])) {
    foreach ($arResult['PERIODS'] as &$period) {
        if ($period["ONLY_DEBT"] != "Y" && is_array($period["CHARGES"]) && !empty($period["CHARGES"])) {
            foreach ($period["CHARGES"] as $key => &$charge) {
                if ($charge["DEBT_END"] == 0) // ��� ������������� �� ������� ���������� ������, ��� ���� debtend ��� ���������� (<item>) �� �����������
                    $period["CHARGES"][$key]["DEBT_END"] = $charge["SUMM2PAY"];
                if ($charge["CORRECTION"] == 0 && $charge["DEBT_END"] == 0) // ���� �� ������ ��� ������� �������� �� ���� ��������, ��������� ��� �������
                    unset($period["CHARGES"][$key]);
            }
            if (isset($charge))
                unset($charge);
        }
    }
}
if (isset($period))
    unset($period);