<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
	"GROUPS" => array(
		"BLOCK_CONTACT" => array(
			"NAME"=>GetMessage("BLOCK_CONTACT")
		),
        "BLOCK_PAS" => array(
            "NAME"=>GetMessage("BLOCK_PAS")
        )
	),
	"PARAMETERS" => array(
		"SHOW_CHANGE_CONTACT" => Array(
            "PARENT"=>"BLOCK_CONTACT",
			"NAME" => GetMessage("SHOW_BLOCK"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y",
		),
        "REQUIRE_EMAIL" => Array(
            "PARENT"=>"BLOCK_CONTACT",
            "NAME" => GetMessage("REQUIRE_EMAIL"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
        ),
        "REQUIRE_TELEPHONE" => Array(
            "PARENT"=>"BLOCK_CONTACT",
            "NAME" => GetMessage("REQUIRE_TELEPHONE"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "Y",
        ),
        "SHOW_CHANGE_PAS" => Array(
            "PARENT"=>"BLOCK_PAS",
            "NAME" => GetMessage("SHOW_BLOCK"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "Y",
        )
	),
);
?>
