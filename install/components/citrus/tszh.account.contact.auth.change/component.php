<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (!CModule::IncludeModule('citrus.tszh'))
{
	ShowError(GetMessage("TSZH_MODULE_NOT_FOUND_ERROR"));
	return;
}
/*
if (!CTszhFunctionalityController::CheckEdition())
	return;
*/
if (!$USER->IsAuthorized()) {
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
    return;
}
/*if (!CTszh::IsTenant()) {
    $APPLICATION->AuthForm(GetMessage("TSZH_NOT_A_MEMBER"));
    return;
}*/

function checkPhoneNumber($phoneNumber, $require = true){
    if(!$require && $phoneNumber == "")
        return true;
    $phoneNumber = preg_replace('/\s|\+|-|\(|\)/','', $phoneNumber); // ������ �������, � ������ �� ������ �����
    if(is_numeric($phoneNumber))
    {
        if(strlen($phoneNumber) < 5) // ���� ����� ������ ������� ��������, ������ false
        {
            return false;
        }
        else
        {
            return $phoneNumber;
        }
    }
    else
    {
        return false;
    }
}

$cookieName = 'message'.$APPLICATION->GetCurPage(false);
if(!empty($_POST)){
    $update = true;
    $message = '';
    if(isset($_POST['telephone']) && isset($_POST['email'])){
        $phoneNumber = trim($_POST['telephone']);
        if(!checkPhoneNumber($phoneNumber, $arParams["REQUIRE_TELEPHONE"]=="Y"))
        {
            $message = GetMessage("TACC_TELEPHONE_ERROR");
        }
        else{
            $email = trim($_POST['email']);
            if($email != "" && !check_email($email)){
                $message = GetMessage("TACC_EMAIL_ERROR");
            }
            else{
                COption::SetOptionString("main","new_user_email_required",$arParams["REQUIRE_EMAIL"]);
                $cUser = new CUser;
                $cUser->Update($USER->GetID(), Array(
                    "EMAIL" => $email,
                    "PERSONAL_PHONE" => $phoneNumber
                ));
                if($cUser->LAST_ERROR != "")
                    $message = $cUser->LAST_ERROR;
                else
                    $message = GetMessage('TACC_CONTACT_SUCCESS');
            }
        }
    }
    else if(isset($_POST['oldpassword']) && isset($_POST['newpassword1']) && isset($_POST['newpassword2'])){
        $login = CUser::GetLogin();
        $rsUser = CUser::GetByLogin( $login );
        if ($arUser = $rsUser->Fetch()) {
            if (strlen($arUser["PASSWORD"]) > 32) {
                $salt = substr($arUser["PASSWORD"], 0, strlen($arUser["PASSWORD"]) - 32);
                $db_password = substr($arUser["PASSWORD"], -32);
            } else {
                $salt = "";
                $db_password = $arUser["PASSWORD"];
            }

            $user_password = md5($salt . $_REQUEST['oldpassword']);

            if ($user_password == $db_password) {
                $login_password_correct = true;
            }

            if (!$login_password_correct)
                $message = GetMessage('TACC_PAS_ERROR');
            else {
                $obUser = new CUser;
                if(!$obUser->Update($USER->GetID(), array("PASSWORD"=>$_REQUEST['newpassword1'],"CONFIRM_PASSWORD"=>$_REQUEST['newpassword2']), true))
                    $message = $obUser->LAST_ERROR;
                else
                    $message = GetMessage('TACC_PAS_SUCCESS');
            }
        }

    }
    if($update){
        if($message != '')
            $APPLICATION->set_cookie($cookieName, $message, time()+60);
        //����� ��� ���������� �������� �� ��������� ������ post ������
        LocalRedirect($APPLICATION->GetCurPage(false));
        exit;
    }
}

$message = $APPLICATION->get_cookie($cookieName);
if($message!=''){
    ShowMessage($message);
    $APPLICATION->set_cookie($cookieName, '', time()-60);
}



if($arParams["SHOW_CHANGE_CONTACT"] == 'Y' || $arParams["SHOW_CHANGE_PAS"] == 'Y'){
    $result = CUser::GetList($by, $order, array('ID'=>$USER->GetID()),
        array(
            'NAV_PARAMS'=>array('nPageSize'=>'1'),
            'FIELDS'=>array('PERSONAL_PHONE','EMAIL', 'LOGIN')
        )
    );
    $arElement = $result->GetNext();
    if(is_array($arElement)) {
        if($arParams["SHOW_CHANGE_CONTACT"] == 'Y')
            $arResult['CONTACT'] = array('TELEPHONE' => $arElement['PERSONAL_PHONE'],'EMAIL' => $arElement['EMAIL']);
        if($arParams["SHOW_CHANGE_PAS"] == 'Y')
            $arResult['LOGIN'] = $arElement['LOGIN'];
    }
}

$this->IncludeComponentTemplate();
?>