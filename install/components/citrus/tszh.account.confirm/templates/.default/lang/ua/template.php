<?
$MESS["TAC_YOUR_ACCOUNT"] = "Ваш особовий рахунок: <strong>#ACCOUNT#</strong>";
$MESS["TAC_GO_TO"] = "Перейти в";
$MESS["TAC_PERSONAL_SECTION"] = "Особистий кабінет";
$MESS["TAC_SELECT_TSZH"] = "Оберіть Ваше ТСЖ";
$MESS["TAC_NOT_SELECTED"] = "(не обрано)";
$MESS["TAC_REG_WORD"] = "Кодове слово, видане ТСЖ:";
$MESS["TAC_CAPTCH_PROMPT"] = "Введіть символи, вказані на малюнку:";
$MESS["TAC_REQUIRED_FIELDS"] = "- поля, обов'язкові для заповнення";
$MESS["TAC_BUTTON_CAPTION"] = "Підтвердити";
?>