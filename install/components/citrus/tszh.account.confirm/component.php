<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}

if (!CModule::IncludeModule('citrus.tszh'))
{
	ShowError(GetMessage("TSZH_MODULE_NOT_FOUND_ERROR"));

	return;
}

if (!CTszhFunctionalityController::CheckEdition())
{
	return;
}

if (strlen($arParams["REGCODE_PROPERTY"]) <= 0)
{
	$arParams["REGCODE_PROPERTY"] = "UF_REGCODE";
}
$arParams['PERSONAL_PATH'] = trim($arParams['PERSONAL_PATH']);
if (strlen($arParams["PERSONAL_PATH"]) <= 0)
{
	$arParams['PERSONAL_PATH'] = "#SITE_DIR#personal/";
}
$arParams['PERSONAL_PATH'] = str_replace("#SITE_DIR#", SITE_DIR, $arParams['PERSONAL_PATH']);

$arResult = Array();
$arErrors = Array();

if (!$USER->IsAuthorized())
{
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

	return;
}

$arResult["TSZH_LIST"] = Array();
$rsTszh = CTszh::GetList(
	Array("NAME" => "DESC"),
	Array(),
	false, //mixed arGroupBy = false
	false, //mixed arNavStartParams = false
	Array("ID", "NAME") //array arSelectFields = Array());
);
while ($arTszh = $rsTszh->GetNext())
{
	$arResult["TSZH_LIST"][$arTszh["ID"]] = $arTszh;
}

if (count($arResult["TSZH_LIST"]) <= 0)
{
	ShowError(GetMessage("TSZH_NO_TSZHS"));

	return;
}
if (class_exists('CCitrusJkhOrg'))
{
	$arTszh = CCitrusJkhOrg::getCurrent();
	$tszhCode = $arTszh['CODE'];

	$dbGroup = CGroup::GetList($by = "", $order = "", Array("STRING_ID" => "jkh_users_" . $tszhCode));
	if ($arGroup = $dbGroup->Fetch())
	{
		$groupID = $arGroup['ID'];
	}
	else
	{
		$groupID = false;
	}
}
else
{
	$groupID = false;
}
$arResult['REG_CODE'] = $_REQUEST['regcode'];
if(strlen($arResult['REG_CODE'])>0){
	$arraccs = CTszhAccount::GetList(array(), array('UF_REGCODE' => $_REQUEST['regcode'],))->Fetch();
	$arResult["ACC"] = $arraccs;}
else{
	$arResult['ACC']=null;
}

if ($_SERVER["REQUEST_METHOD"] == 'POST' && $_POST['action'] == 'confirm' && check_bitrix_sessid())
{

	//$arResult["TSZH"] = $tszh;//IntVal($_REQUEST['tszh']);
	$arResult["REG_CODE"] = htmlspecialcharsbx(trim($_REQUEST['regcode']));

	if (strlen($arResult["REG_CODE"]) <= 0)
	{
		$arErrors[] = GetMessage("ERROR_REGCODE_EMPTY");
	}
	if (CTszh::IsTenant() && is_array($arResult['ACCOUNT']))
	{
		$arErrors[] = GetMessage("TSZH_ALREADY_CONFIRMED");
	}


	if (count($arErrors) <= 0)
	{
		$arFilter = Array(

			'=' . $arParams["REGCODE_PROPERTY"] => $arResult["REG_CODE"],
		);
		$rsAccount = CTszhAccount::GetList(Array(), $arFilter);
		if ($arAccount = $rsAccount->Fetch())
		{


			define("TSZH_CANCEL_REDIRECT", true);
			if (CTszhAccount::Update($arAccount["ID"], Array("USER_ID" => $GLOBALS["USER"]->GetID())))
			{
				$userID = $GLOBALS["USER"]->GetID();
				$arGroups = array_unique(array_merge(
					CUser::GetUserGroup($userID),
					Array(
						CTszh::GetTenantGroup(),
					),
					false !== $groupID ? Array($groupID) : Array()
				));
				CUser::SetUserGroup($userID, $arGroups);
				$GLOBALS['USER']->Logout();
				$GLOBALS['USER']->Authorize($userID);
				$arResult["NOTE"] = GetMessage("CONFIRM_ACCOUNT_SUCCESS");
				$arResult["ACCOUNT"] = $arAccount;
			}
			else
			{
				$arErrors[] = GetMessage("ERROR_SAVING_ACCOUNT");
			}


		}
		else
		{
			$arErrors[] = GetMessage("ERROR_WRONG_CODE");
		}
	}
}

$this->IncludeComponentTemplate();
?>