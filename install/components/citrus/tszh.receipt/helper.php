<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

class CCitrusTszhReceiptComponentHelper
{
	/**
	 * ���������� ����������������� �����
	 * @param float $number �����
	 * @param bool $bCanBeEmpty ���� == true, ������ �� ����� ���������� � ������ ������� �������� (0, false, ������ ������ � �.�.)
 	 * @param int $decimals ���������� ���� ����� �������. ���� ������� �������� ������ ���� � ����� ������� ��� �� �������� ���� ����� �������
	 * @return string
	 */
	public static function num($number, $bCanBeEmpty = false, $decimals = 2)
	{
		if (is_null($number))
			$result = '';
		elseif ($decimals < 0)
            $result = str_replace('.', ',', (float)$number);
        else
			$result = (float)$number == 0 && $bCanBeEmpty ? '' : number_format((float)$number, $decimals , ',', ' ');

		return $result;
	}

	/**
	 * ������������ ��� ��������� ��������� ������������� ������ � ������� � �������
	 * ����������� ��������/�� �������� ������ � ������������ ����������� ��������:
	 *    ���� ���� (���� �� ����) ���������� ����� $values["X_FIELDS"] ��������� ��������� ����������� � ����������� �������� �ջ (������������� � ��������� �� ����� �������)
	 *    ���� �������� ����� ��������, ��������� ����� �������������� ������� self::num()
	 *
	 * ���� � ��������� $field �������� ������:
	 *    ������ ����� ����� �� ������� $values ($values[$field]) � ���������� ���������� �������.
	 * ���� � ��������� $field ������� ������:
	 *    �������� ����� ������������ ��� ����� ������� � �������, ��� ����� ����� ��������� �� ���������� ������������ �������� �X�.
	 *    ���� ���� �� ���� �� ������ �������� �ջ, ����������� ����� �ջ, ����� ������������ �������� �� ��������� $value
	 *
	 * @param string|array $field ������ � ������ ��� ������ � ������� � ������� � ������� ($values)
	 * @param array $values ������ � �������, ������ ����� ����� �������
	 * @param bool $isNumber true ���� ���� ������ ��������� �������� ��������
	 * @param bool|mixed $value
 	 * @param int $decimals ���������� ���� ����� ������� (��� �����)
	 * @return bool|string
	 */
	public static function getArrayValue($field, $values, $isNumber, $value = false, $decimals = 2)
	{
		$checkXFields = is_array($field) ? $field : array($field);
		$hasXFields = count(array_intersect($checkXFields, $values["X_FIELDS"])) > 0;

		$value = is_array($field) ? $value : $values[$field];

		if ($hasXFields)
			return 'X';
		elseif ($isNumber)
			return self::num($value, false, $decimals);
		else
			return $value;
	}
}
