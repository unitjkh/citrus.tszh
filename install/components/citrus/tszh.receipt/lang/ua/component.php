<?
$MESS["TSZH_NOT_A_MEMBER"] = "Ви не є власником особового рахунку.";
$MESS["TSZH_NO_DATA"] = "Немає даних про нараховані послуги.";
$MESS["TSZH_NO_PERIOD"] = "Період не знайдений";
$MESS["TSZH_PRINT_ERROR_TEMPLATE_NOT_FOUND"] = "Шаблон для друку не знайдений.";
$MESS["TSZH_MODULE_NOT_FOUND_ERROR"] = "Модуль 1С: Сайт ЖКГ не встановлений.";
?>