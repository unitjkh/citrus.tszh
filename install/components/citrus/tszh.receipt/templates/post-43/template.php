<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**@var bool $printDebugInfo �������� ���������� �� ���������� ������ */
$printDebugInfo = false;

global $APPLICATION, $DB;
if ($_GET["print"] == "Y")
    $APPLICATION->RestartBuffer();

require_once(__DIR__ . '/functions.php');

$APPLICATION->SetPageProperty("SHOW_TOP_LEFT", "N");

if ($_GET["print"] != "Y") {
    $APPLICATION->AddHeadString('<link href="' . $this->GetFolder() . '/styles.css" rel="stylesheet" type="text/css" />');

    ?>
    <p>
        &mdash; <a href="<?= $APPLICATION->GetCurPageParam('print=Y', Array('print')) ?>"
                   target="_blank"><?= GetMessage("RCPT_OPEN_RECEIPT_IN_WINDOW") ?></a>
        <small><?= GetMessage("TPL_LANDSCAPE_NOTE") ?></small>
        <br>
        &mdash; <a href="../index.php"><?= GetMessage("TPL_BACK_TO_PERSONAL") ?></a>
    </p>
    <?
} else {
    if ($arResult["IS_FIRST"]):
        if ($arResult["MODE"] == "AGENT"):?>
            <style type="text/css">
                <?=$APPLICATION->GetFileContent($_SERVER["DOCUMENT_ROOT"] . $this->GetFolder() . "/styles.css")?>
            </style>
        <? else: ?>
            <!doctype html>
            <html>
            <head>
            <meta http-equiv="content-type" content="text/html;" charset="<?= SITE_CHARSET ?>"/>
            <link href="<?= $this->GetFolder() ?>/styles.css" rel="stylesheet" type="text/css"/>
        <?endif ?>

        <style type="text/css" media="print">
            @page {
                size: 35.7cm 24cm;
                margin: 1cm 1cm;

            }

            <?if ($arParams["INLINE"] != "Y"):?>
            #receipt div.page {
                page-break-after: always;
            }

            <?endif?>
        </style>

        <? if ($arResult["MODE"] == "ADMIN"):?>
        <style type="text/css" media="screen">
            div.rcpt {
                border-bottom: 1pt dashed #666;
                padding-bottom: 1em;
                margin-bottom: 1em;
            }
        </style>
    <?endif;

        if ($arResult["MODE"] == "AGENT"):?>
            <div id="receipt">
        <? else: ?>
            <title><?= GetMessage("RCPT_TITLE") ?></title>
            </head>
            <body id="receipt"<?= ($printDebugInfo ? '' : ' onload="window.print();"') ?>>
        <?endif;
    endif;
}

?>

    <div class="rcpt">
        <div class="overflowx">
            <table width="100%">
                <?
                if (!$arResult["HAS_SEPARATE_PENALTIES_RECEIPT"]) {
                    ?>
                    <tr>
                        <td colspan="5" class="rcpt-header">
                            <h2><?= GetMessage("TPL_DOCUMENT_TITLE") ?></h2>
                            <p><strong><?= GetMessage("TPL_DOCUMENT_SUBTITLE") ?></strong></p>
                        </td>
                    </tr>
                    <?
                }
                ?>
                <tr>
                    <td>
                        <?= GetMessage("TPL_SECTION1") ?>
                    </td>
                    <td>

                        <?= GetMessage("TPL_SECTION2") ?>

                    </td>

                </tr>

                <tr>
                    <td>
                        <? include(__DIR__ . '/1_info.php'); ?>
                    </td>

                    <td><? include(__DIR__ . '/6_installments.php'); ?></td>

                </tr>
            </table>
        </div>
        <div style=" padding: 0 4pt; overflow: hidden; margin-top: 1.5em">
                <? include(__DIR__ . '/3_charges.php'); ?>
        </div>
        <div>
        <div style=" padding: 0 4pt; overflow: hidden; margin-top: 1.5em;">
            <?= GetMessage("TPL_SECTION7") ?>
        </div>
        <div style=" padding: 0 4pt; overflow: hidden; margin-top: 1.5em">
            <? include(__DIR__ . '/2_contractors_info.php') ?>
        </div>
            </div>
        <div style=" padding: 0 4pt; overflow: hidden; margin-top: 1.5em">

            <div class="overflowx" style="width: 50%; float: left;">
                <? include(__DIR__ . '/4_meters.php'); ?>
            </div>
            <div class="overflowx" style="width: 50%; float: right;">
                <div><? include(__DIR__ . '/7_installs.php'); ?></div>
                <div style="margin-top: 1.5em"><? include(__DIR__ . '/5_corrections.php'); ?></div>
            </div>
        </div>


    </div>
<?

$summ2pay = $arResult["ACCOUNT_PERIOD"]["DEBT_END"];
if (COption::GetOptionString("citrus.tszh", "pay_to_executors_only", "N") == "Y") {
    if (CModule::IncludeModule("vdgb.portaljkh") && method_exists("CCitrusPortalTszh", "setPaymentBase"))
        CCitrusPortalTszh::setPaymentBase($arResult["TSZH"]);
    $summ2pay = CTszhAccountContractor::GetList(array(), array("ACCOUNT_PERIOD_ID" => $arResult["ACCOUNT_PERIOD"]["ID"], "!CONTRACTOR_EXECUTOR" => "N"), array("SUMM"))->Fetch();
    $summ2pay = is_array($summ2pay) ? $summ2pay['SUMM'] - $arResult["ACCOUNT_PERIOD"]["PREPAYMENT"] : 0;
}
if ($summ2pay > 0 && CModule::IncludeModule("citrus.tszhpayment") && ($paymentPath = CTszhPaySystem::getPaymentPath($arResult["TSZH"]["SITE_ID"])))
    echo '<div class="no-print">' . GetMessage("CITRUS_TSZHPAYMENT_LINK", Array("#LINK#" => $paymentPath . '?ptype=' . $arResult['RECEIPT_PAYMENT_TYPE'])) . '</div>';

if ($_GET["print"] == 'Y') {
    if ($arResult["IS_LAST"]):
        if ($arResult["MODE"] == "AGENT")
            echo '</div>';
        else {
            echo '</body></html>';
            exit();
        }

    endif;

    if (in_array($arResult["MODE"], array("ADMIN", "AGENT")))
        return;
    else {
        require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
        die();
    }
}