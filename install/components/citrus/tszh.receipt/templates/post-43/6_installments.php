<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

?>

<table class="rcpt-inner rcpt-inner-table" style="border: none; width: 95%;">


	<tr>
		<th><?= GetMessage("TPL_NORM_METER_ABBR") ?></th>
		<th><?= GetMessage("TPL_NORM_SERVICE_ABBR") ?></th>
		<th><?= GetMessage("TPL_NORM") ?></th>
		<th><?= GetMessage("TPL_METERS_CURRENT") ?></th>
	</tr>





	<? foreach ($arResult['METERS'] as $meter_val) { ?>
		<? //echo "<pre>"; var_dump($meter_val); echo "</pre>"; ?>
		<tr>
		<? if ($meter_val["VALUES_COUNT"] == 1) { ?>
			<td style="text-align: center"><?= $meter_val['NAME']?></td>
			<td style="text-align: center"><?= $meter_val['SERVICE_NAME']?></td>
			<td class="n"><?= $meter_val["VALUE_BEFORE"]["VALUE1"] ?></td>

			<td class="n"><?= $meter_val["VALUE"]["VALUE1"] ?></td>


		<? } elseif(($meter_val["VALUES_COUNT"] == 3)) { ?>

			<tr>
				<td style="text-align: center"><?= $meter_val['NAME']."(".GetMessage("TPL_PERSONAL_PIC").")" ?></td>
				<td style="text-align: center"><?= $meter_val['SERVICE_NAME']?></td>
				<td class="n"><?= $meter_val["VALUE_BEFORE"]["VALUE1"] ?></td>

				<td class="n"><?= $meter_val["VALUE"]["VALUE1"] ?></td>
			</tr>
			<tr>
				<td style="text-align: center"><?= $meter_val['NAME']."(".GetMessage("TPL_PERSONAL_DAY").")"  ?></td>
				<td style="text-align: center"><?= $meter_val['SERVICE_NAME']?></td>
				<td class="n"><?= $meter_val["VALUE_BEFORE"]["VALUE2"] ?></td>

				<td class="n"><?= $meter_val["VALUE"]["VALUE2"] ?></td>
			</tr>
			<tr>
				<td style="text-align: center"><?= $meter_val['NAME']."(".GetMessage("TPL_PERSONAL_NIGHT").")" ?></td>
				<td style="text-align: center"><?= $meter_val['SERVICE_NAME']?></td>
				<td class="n"><?= $meter_val["VALUE_BEFORE"]["VALUE3"]?></td>

				<td class="n"><?= $meter_val["VALUE"]["VALUE3"] ?></td>
			</tr>
			<?
		}
		else{?>
			<tr>
				<td style="text-align: center"><?= $meter_val['NAME']."(".GetMessage("TPL_PERSONAL_PIC").")" ?></td>
				<td style="text-align: center"><?= $meter_val['SERVICE_NAME']?></td>
				<td class="n"><?= $meter_val["VALUE_BEFORE"]["VALUE1"] ?></td>

				<td class="n"><?= $meter_val["VALUE"]["VALUE1"] ?></td>
			</tr>
			<tr>
				<td style="text-align: center"><?= $meter_val['NAME']."(".GetMessage("TPL_PERSONAL_DAY").")"  ?></td>
				<td style="text-align: center"><?= $meter_val['SERVICE_NAME']?></td>
				<td class="n"><?= $meter_val["VALUE_BEFORE"]["VALUE2"] ?></td>

				<td class="n"><?= $meter_val["VALUE"]["VALUE2"] ?></td>
			</tr>
		<?	}
	}?>
	</tr>
</table>
