<?
use Citrus\Tszh\Types\ComponentType;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

// �������� ����� ������, �� ������� ������� ������
$sourceFields = array(
	array(
		"name",
		"edizm",
		"ammount",
		//$arResult["HAS_AMOUNT_NOTES"] ? '*' : null,
		"hammount",
		//$arResult["HAS_AMOUNT_NOTES"] ? '*' : null,
		"tarif1..tariff3",
		//"sum-hsum",
		"hsum",
		"sum",
		"raise_multiplier",
		"raise_sum",
		"correction",
		"compensation",
		$arResult["HAS_PENALTIES"] ? 'peni' : null,
		"contractor",
		"sumtopay",
		"sumtopay-hsumtopay",
		"hsumtopay",
	),
	array(
		"norm-hnorm",
		"hnorm",
		"meter",
		"hmeter",
		"volumep",
		"volumea",
		//"volumeh",
	),
);
$colsCount = array();
foreach ($sourceFields as $idx => &$sf)
{
	$sf = array_values(array_diff($sf, array(null)));
	$colsCount[$idx] = count($sf);
}
unset($sf);

?>
<table class="rcpt-inner rcpt-inner-table" style="border: none; margin-top: 1.5em;">
	<tr class="rcpt-inner-empty">
		<td colspan="<?=(count($sourceFields[0]))?>" style="width: 100%;">
			<?=GetMessage("TPL_SECTION3")?>
		</td>
		<td rowspan="5" style="border-bottom: 1pt solid #fff; width: 2.5em;"></td>

	</tr>
	<tr>
		<th style="width: 15%"><?=GetMessage("TPL_SERVICES")?></th>
		<th ><?=GetMessage("TPL_UNITS")?></th>
		<th ><?=GetMessage("TPL_SERVICE_AMOUNT")?></th>
		<th ><?=GetMessage("TPL_SHARED_CONS")?>
		<th ><?=GetMessage("TPL_TARIFF")?></th>
		<th ><?=GetMessage("TPL_PERIOD_SUMM")?></th>
        <th ><?=GetMessage("TPL_RAISE_MULTIPLIER")?></th>
        <th ><?=GetMessage("TPL_RAISE_SUM")?></th>
		<th ><?=GetMessage("TPL_PERIOD_CORRECTIONS")?></th>
		<th ><?=GetMessage("TPL_PERIOD_COMPENSATIONS")?></th>
		<th ><?=GetMessage("TPL_PERIOD_TO_PAY")?></th>
		<th ><?=GetMessage("TPL_PREPAYMENT_DOLG")?></th>
		<th ><?=GetMessage("TPL_PERIOD_PENALTIES")?></th>
		<th ><?=GetMessage("TPL_PAYEE_NUM")?></th>
		<th ><?=GetMessage("TPL_PERIOD_TOTAL_TO_PAY")?></th>


	</tr>

	<tr class="rcpt-inner-table-num">
		<?
		for ($i = 1; $i <= $colsCount[0] - ($arResult["HAS_AMOUNT_NOTES"] ? 0 : 0); $i++)
		{
			?>
			<th><?=$i?></th>
			<?
		}

			?><th colspan="<?=$colsCount[1]?>" style="border:1pt solid #fff"></th>
	</tr>
	<?
	if ($printDebugInfo)
	{
		?>
		<tr class="debug">
			<?
			foreach ($sourceFields as $idx=>$group)
			{
				if ($idx !== 0)
				{
					?><td style="border: 0; background: none"></td><?
				}
				for ($i = 0; $i < count($group); $i++)
				{
					?>
					<td><?=$group[$i]?></td><?
				}
			}
			?>
		</tr>
	<?
	}

	/** @var int $tariffIndex ������� ������ ������ (������ ���������� � ������������ ������������� ������ �����������, ������� ������ ������� 1-� �����) */
	$tariffIndex = 0;
	foreach ($arResult['CHARGES'] as $idx => $arCharge)
	{
		/**
		 * ��� ����� � component=2 ������ �� ������� ��������� ��.
		 * ��� �������� ������ ����� ��������� ���������� ������� ����� ���� ����� �������, ������ � ������� ��������� ���� ��������� ���� �������� ������
		 *
		 * ��� ����� � component=1 ������� ����� ��������� ��������� ��������, ������� � �������� ��������� ��� ���� ���������.
		 * � �������� ������ ������ �� �������
		 */
		/** @var bool $isComponent �������� �� ����������� �� ��������� ������ ��� ������������ �� ������� */
		$isComponent = $arCharge["COMPONENT"] != ComponentType::NONE;
		/** @var int $tariffIndex ������� ������ ��������� (1 - �������, 2 - ������, 3 - �������) ��� ����������� �� ������� */
		$tariffIndex = $isComponent && $arCharge["COMPONENT"] == ComponentType::TARIFFS ? $tariffIndex + 1 : 0;

		// ��� ����� ����������, �� ������� �����������, ����� ����� ������ �����
		if (!$isComponent && !$arCharge["HAS_COMPONENTS"])
			$tariffIndex = 1;

		$meterValue = $hMeterValue = false;
		if ($tariffIndex > 0)
		{
			$decPlaces = -1;
			// ��������� ��������� �������������� ��������� �� �������� ������
			if ($arResult["HAS_CHARGES_METERS_BINDING"])
			{
				$meterValues = array();
				foreach ($arCharge["METER_IDS"] as $meterID)
				{
					$arMeter = $arResult["METERS"][$meterID];
					if (is_array($arMeter) && isset($arMeter["VALUE"]))
					{
						$meterValues[] = $arMeter["VALUE"]["VALUE" . $tariffIndex];
						$decPlaces = max($decPlaces, $arMeter["DEC_PLACES"]);
					}
				}
				$meterValue = empty($meterValues) ? false : CCitrusTszhReceiptComponentHelper::num(array_sum($meterValues), false, $decPlaces <= 0 ? 2 : $decPlaces);
			}
			else
			{
				$meterValues = array();
				foreach ($arResult["METERS"] as $meterID => $arMeter)
				{
					if (trim($arMeter["~SERVICE_NAME"]) == trim($arCharge["~SERVICE_NAME"]))
					{
						$meterValues[] = $arMeter["VALUE"]["VALUE" . $tariffIndex];
						$decPlaces = max($decPlaces, $arMeter["DEC_PLACES"]);
					}
				}
				$meterValue = empty($meterValues) ? false : CCitrusTszhReceiptComponentHelper::num(array_sum($meterValues), false, $decPlaces <= 0 ? 2 : $decPlaces);
			}

			$decPlaces = -1;
			// ��������� ��������� ����������� ��������� �� �������� ������
			$hMeterValues = array();
			foreach ($arCharge["HMETER_IDS"] as $hMeterID)
			{
				$hMeter = $arResult["HMETERS"][$hMeterID];
				if (is_array($hMeter) && isset($hMeter["VALUE"]))
				{
					$hMeterValues[] = $hMeter["VALUE"]["VALUE" . $tariffIndex];
					$decPlaces = max($decPlaces, $hMeter["DEC_PLACES"]);
				}
			}
			$hMeterValue = empty($hMeterValues) ? false : CCitrusTszhReceiptComponentHelper::num(array_sum($hMeterValues), false, $decPlaces <= 0 ? 2 : $decPlaces);
		}
		elseif ($arCharge["HAS_COMPONENTS"] === ComponentType::COMPLEX)
		{
			$decPlaces = -1;
			// ��������� ��������� �������������� ��������� �� �������� ������
			if ($arResult["HAS_CHARGES_METERS_BINDING"])
			{
				$meterValues = array();
				foreach ($arCharge["METER_IDS"] as $meterID)
				{
					$arMeter = $arResult["METERS"][$meterID];
					if (is_array($arMeter) && isset($arMeter["VALUE"]))
					{
						for ($i = 0; $i < $arMeter["VALUES_COUNT"]; $i++)
						{
							$meterValues[$i] += $arMeter["VALUE"]["VALUE" . ($i+1)];
						}
						$decPlaces = max($decPlaces, $arMeter["DEC_PLACES"]);
					}
				}
				$meterValue = empty($meterValues) ? false : implode('/', $meterValues);
			}
			else
			{
				$meterValues = array();
				foreach ($arResult["METERS"] as $meterID => $arMeter)
				{
					if (trim($arMeter["~SERVICE_NAME"]) == trim($arCharge["~SERVICE_NAME"]))
					{
						for ($i = 0; $i < $arMeter["VALUES_COUNT"]; $i++)
						{
							$meterValues[$i] += $arMeter["VALUE"]["VALUE" . ($i+1)];
						}
						$decPlaces = max($decPlaces, $arMeter["DEC_PLACES"]);
					}
				}
				$meterValue = empty($meterValues) ? false : implode('/', $meterValues);
			}

			$decPlaces = -1;
			// ��������� ��������� ����������� ��������� �� �������� ������
			$hMeterValues = array();
			foreach ($arCharge["HMETER_IDS"] as $hMeterID)
			{
				$hMeter = $arResult["HMETERS"][$hMeterID];
				if (is_array($hMeter) && isset($hMeter["VALUE"]))
				{
					for ($i = 0; $i < $arMeter["VALUES_COUNT"]; $i++)
					{
						$hMeterValues[$i] += $hMeter["VALUE"]["VALUE" . ($i+1)];
					}
					$decPlaces = max($decPlaces, $hMeter["DEC_PLACES"]);
				}
			}
			$hMeterValue = empty($hMeterValues) ? false : implode('/', $hMeterValues);
		}

		$arTariffs = array();
		for ($i = 0; $i < 3; $i++)
		{
			$fieldName = "SERVICE_TARIFF";
			if ($i)
				$fieldName .= $i + 1;
			if ($arCharge[$fieldName])
				$arTariffs[] = $arCharge[$fieldName];
		}

		if ($arResult["HAS_GROUPS"] && ($idx == 0 || $arResult["CHARGES"][$idx-1]["GROUP"] != $arCharge["GROUP"]))
		{
			?>
			<tr class="rcpt-group-title">
				<td colspan="15<?/*=($colsCount[0])*/?>">
					<?=$arCharge["GROUP"]?>
				</td>
				<td style="border: 1pt solid #fff"></td>

			</tr>
			<?
		}
		?>
        <?if ($arCharge["IS_INSURANCE"] != "Y"):?>
		<tr>
			<td><?=$arCharge['SERVICE_NAME']?><?=($arCharge["HAS_COMPONENTS"] && substr($arCharge['SERVICE_NAME'], -1, 1) !== ':' ? ':' : '')?></td>
			<td class="center"><?=CCitrusTszhReceiptComponentHelper::getArrayValue(array('UNITS'), $arCharge, false, $arCharge["SERVICE_UNITS"])?></td>
			<?php
			// ������� �������� �� ������������� �� 1�
			if (isset($arCharge["AMOUNT_VIEW"]) && strlen($arCharge["AMOUNT_VIEW"])) : ?>
				<td class="n"><?=$arCharge["AMOUNT_VIEW"]?></td>
			<?php else : ?>
				<td class="n"><?=CCitrusTszhReceiptComponentHelper::getArrayValue(array("AMOUNT"), $arCharge, true, $arCharge['AMOUNT'] - $arCharge['HAMOUNT'], 3)?></td>
			<?php endif; ?>

			<td class="n"><?=CCitrusTszhReceiptComponentHelper::getArrayValue('HAMOUNT', $arCharge, true, false, 3)?></td>

			<td class="n"><?=$arCharge["RATE"]?></td>
			<td class="n"><?=CCitrusTszhReceiptComponentHelper::getArrayValue((($arCharge["SUM_WITHOUT_RAISE"] != 0) ? "SUM_WITHOUT_RAISE" : "SUMM"), $arCharge, true)?></td>
            <td class="n"><?=CCitrusTszhReceiptComponentHelper::getArrayValue('RAISE_MULTIPLIER', $arCharge, true)?></td>
            <td class="n"><?=CCitrusTszhReceiptComponentHelper::getArrayValue('RAISE_SUM', $arCharge, true)?></td>
			<td class="n"><?=CCitrusTszhReceiptComponentHelper::getArrayValue('CORRECTION', $arCharge, true)?></td>
			<td class="n"><?=CCitrusTszhReceiptComponentHelper::getArrayValue('COMPENSATION', $arCharge, true)?></td>
			<td class="n"><?=CCitrusTszhReceiptComponentHelper::getArrayValue((($arCharge["SUM_WITHOUT_RAISE"] != 0) ? "SUM_WITHOUT_RAISE" : "SUMM"), $arCharge, true)?></td>
			<td class="n"><?=CCitrusTszhReceiptComponentHelper::getArrayValue('DEBT_BEG', $arCharge, true)?></td>
			<td class="n"><?=CCitrusTszhReceiptComponentHelper::getArrayValue('PENALTIES', $arCharge, true)?></td>
			<td class="n">
				<?


				foreach ($arResult['CONTRACTORS'] as $arContractor)
				{
					if ($arContractor['CONTRACTOR_XML_ID'] == $arCharge['CONTRACTOR_ID'])
					{
						?>
						<?=$arContractor['CONTRACTOR_ID']?>
						<?
					}

				}
				?>
			</td>
			<td class="n"><?=$arCharge["DEBT_END"]?></td>




		</tr>
        <?endif;?>
	<?

	}

	$firstCols = $arResult["HAS_AMOUNT_NOTES"] ? 7 : 5;
	?>

    <tr class="rcpt-inner-table-footer">
		<?//echo "<pre>"; var_dump($arResult); echo "</pre>";?>
            <td colspan="10<?/*=$firstCols*/?>"><?=GetMessage("TPL_PERIOD_TOTAL_TO_PAY")?></td>
            <td class="n"><?=CCitrusTszhReceiptComponentHelper::num($arResult["ACCOUNT_PERIOD"]["DEBT_END"]-$arResult["ACCOUNT_PERIOD"]["DEBT_BEG"])?></td>
            <td class="n"><?=CCitrusTszhReceiptComponentHelper::num($arResult["ACCOUNT_PERIOD"]["DEBT_BEG"])?></td>

			<td class="n"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["PENALTIES"])?></td>
			<td class="n"></td>
            <td class="n"><?=CCitrusTszhReceiptComponentHelper::num($arResult["ACCOUNT_PERIOD"]["SUMM_TO_PAY"])?></td>
            

    </tr>
</table>
