<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION, $DB;
if ($_GET["print"] == "Y")
	$APPLICATION->RestartBuffer();

if (!function_exists("__citrusReceiptNum"))
{
	function __citrusReceiptNum($number, $decimals=2, $isComponent=false) 
	{
		if ($number == 0) 
		{
			if ($isComponent)
				return '<div style="text-align: center;">X</div>';

			return '0';
		}
		return number_format($number, $decimals, ',', ' ');
	}
}

if (!function_exists("__citrusReceiptGetMeterAmount"))
{
	function __citrusReceiptGetMeterAmount($amount, $curValue, $prevValue, $capacity, $decPlaces)
	{
		if (strlen($amount) > 0)
		{
			$result = $amount;
		}
		else
		{
			$curValue = floatval($curValue);
			$prevValue = floatval($prevValue);
			if ($curValue >= $prevValue)
				$result = $curValue - $prevValue;
			else
			{
				$rate = intval($capacity) - intval($decPlaces);
				if ($rate > 0)
					$result = $curValue + pow(10, $rate) - $prevValue;
				else
					$result = 0;
			}
		}

		return $result;
	}
}

if ($_GET["print"] != "Y") 
{
	$APPLICATION->AddHeadString('<link href="' . $this->GetFolder() . '/styles.css" rel="stylesheet" type="text/css" />');
?>
<p><?=GetMessage("RCPT_YOU_CAN")?> <a href="<?=$APPLICATION->GetCurPageParam('print=Y', Array('print'))?>" target="_blank"><?=GetMessage("RCPT_OPEN_RECEIPT_IN_WINDOW")?></a> <?=GetMessage("RCPT_FOR_PRINTING")?></p>
<?

}
else 
{
	if ($arResult["IS_FIRST"]):
		if ($arResult["MODE"] == "AGENT"):?>
			<style type="text/css">
				<?=$APPLICATION->GetFileContent($_SERVER["DOCUMENT_ROOT"] . $this->GetFolder() . "/styles.css")?>
			</style>
		<?else:
			?><!DOCTYPE html>
			<html>
			<head>
				<meta http-equiv="content-type" content="text/html; charset=<?=SITE_CHARSET?>" />
				<link href="<?=$this->GetFolder()?>/styles.css" rel="stylesheet" type="text/css" />
		<?endif?>
		<style type="text/css" media="print">
			@page {
				size: 21cm 29.7cm;
				margin: 2.5cm 2cm;
			}
			<?if ($arParams["INLINE"] != "Y"):?>
				#receipt div.receipt {
					page-break-after: always;
					page-break-inside: avoid;
				}
			<?endif?>
		</style>
		<?if ($arResult["MODE"] == "ADMIN"):?>
			<style type="text/css" media="screen">
				#receipt div.receipt {
					border-bottom: 1pt dashed #000;
				}
			</style>
		<?endif;

		if ($arResult["MODE"] == "AGENT"):?>
			<div id="receipt">
		<?else:?>
				<title><?=GetMessage("RCPT_TITLE")?></title>
			</head>
			<body id="receipt" onload="window.print();">
		<?endif;
	endif;
}

?>
<div class="receipt">
<table class="no-border">
<tr>
	<td style="width: 41%;" class="tl">
		<?=GetMessage("RCPT_NOTICE")?><br />
		<strong><?=GetMessage("RCPT_FOR")?> <?=$arResult['ACCOUNT_PERIOD']['DISPLAY_NAME']?> <?=GetMessage("RCPT_YEAR_ABBR")?></strong>
	</td>
	<td class="bl">
		<strong><?=$arResult['ACCOUNT_PERIOD']['ACCOUNT_NAME']?></strong>
		<div align="right" style="font-weight: bold;"><?=GetMessage("RCPT_ACCOUNT_ABBR")?> <?=$arResult['ACCOUNT_PERIOD']['XML_ID']?></div>
		<div><?=CTszhAccount::GetFullAddress($arResult['ACCOUNT_PERIOD'])?></div>
	</td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td class="bt bl small">
		<?=$arResult['ORG_TITLE']?><br />
		<span style="white-space: nowrap;"><?=GetMessage("RCPT_INN")?> <?=$arResult['ORG_INN']?> / <?=GetMessage("RCPT_KPP")?>&nbsp;<?=$arResult['ORG_KPP']?></span>
		<br />
		<span style="white-space: nowrap;"><?=GetMessage("RCPT_RSCH")?> <?=$arResult['ORG_RSCH']?>, <?=GetMessage("RCPT_IN")?>&nbsp;<?=$arResult['ORG_BANK']?></span>,
		<span style="white-space: nowrap;"><?=GetMessage("RCPT_KSCH")?> <?=$arResult['ORG_KSCH']?></span>,
		<span style="white-space: nowrap;"><?=GetMessage("RCPT_BIK")?> <?=$arResult['ORG_BIK']?></span>
		<br />
		<div style="margin: 1.5pt 0 2pt 0;" align="right"><br /></div>
	</td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td class="bl">
		<strong style="display: block; float: left;"><?=GetMessage("RCPT_SUMM_TO_PAY"/*$arResult['SUM2PAY'] > 0 ? "RCPT_DEBT_END" : "RCPT_CREDIT_END"*/)?></strong>
		<span style="margin-left: 10pt; font-weight: bold;">
			<?=__citrusReceiptNum(abs($arResult['SUM2PAY']))?>
		</span>
	</td>
</tr>
<tr>
	<td style="vertical-align: middle;" class="tl">
		<?=GetMessage("RCPT_CASHIER")?><br>
		<?if (!empty($arResult["ACCOUNT_PERIOD"]["BARCODES"])):?>
			<table border="0">
				<tr>
					<?foreach ($arResult["ACCOUNT_PERIOD"]["BARCODES"] as $idx=>$barcode)
					{
						if ($link = CTszhBarCode::getImageLink($barcode))
						{
							$code128 = $barcode["TYPE"] == 'qr';
							$i = in_array($code128, $barcode);
							if($i ==true)
							{
								$needle['code128']["TYPE"] = $barcode["TYPE"];
								$needle['code128']["VALUE"] = $barcode["VALUE"];
							}
							else{
								$needle['code128'] = false;
							}
							//echo "<pre>";var_dump($barcode); echo "</pre>";

							if(count($arResult["ACCOUNT_PERIOD"]["BARCODES"]) == 1){
								?><td class="no-border rcpt-barcode-cell">&nbsp;</td><?}
							else{?>
								<td class="no-border rcpt-barcode-cell"><img style="<?=($code128 ? 'width: 3cm; height: 3cm; max-height: 4cm;' : '')?>" class="rcpt-barcode" src="<?=(isset($needle['code128']) && $needle['code128']["VALUE"]!=false && strlen($needle['code128']["VALUE"])>0? CTszhBarCode::getImageLink($needle['code128']): "")?>" alt="<?=htmlspecialcharsbx($needle)?>"></td>
							<?}
						}
					}?>
				</tr>
			</table>
		<?endif?>
	</td>
	<td style="padding-left: 30pt;" class="bl">
		<?if (!empty($arResult["ACCOUNT_PERIOD"]["BARCODES"])):?>
			<table border="0">
				<tr>
					<?foreach ($arResult["ACCOUNT_PERIOD"]["BARCODES"] as $idx=>$barcode)
					{
						if ($link = CTszhBarCode::getImageLink($barcode))
						{
							$code128 = $barcode["TYPE"] == 'code128';
							$i = in_array($code128, $barcode);
							if($i ==true)
							{
								$needle['code128']["TYPE"] = $barcode["TYPE"];
								$needle['code128']["VALUE"] = $barcode["VALUE"];
							}
							else{
								$needle['code128'] = false;
							}


							if(count($arResult["ACCOUNT_PERIOD"]["BARCODES"]) == 1){
								?><td class="no-border rcpt-barcode-cell"><img style="<?=($code128 ? 'width: 5.5cm' : 'width: 3cm; height: 3cm; max-height: 4cm;')?>" class="rcpt-barcode" src="<?=$link?>" alt="<?=htmlspecialcharsbx($barcode["VALUE"])?>"></td><?}
							else{?>
								<td class="no-border rcpt-barcode-cell"><img style="<?=($code128 ? 'width: 5.5cm' : '')?>" class="rcpt-barcode" src="<?=(isset($needle['code128']) && $needle['code128']["VALUE"]!=false && strlen($needle['code128']["VALUE"])>0? CTszhBarCode::getImageLink($needle['code128']): "")?>" alt="<?=htmlspecialcharsbx($needle)?>"></td>
							<?}
						}
					}?>
				</tr>
			</table>
		<?endif?>
	</td>
</tr>
<tr>
	<td class="bb tl"></td>
	<td class="bl bb"><?

	if ($arResult['METERS_LAST_UPDATE'])
	{
		$sDateFormat = CSite::GetDateFormat("FULL");
		$sNewDateFormat = 'DD/MM/YYYY';
		$sMetersUpdateDate = $DB->FormatDate($arResult['METERS_LAST_UPDATE'], $sDateFormat, $sNewDateFormat);
		$sMetersUpdateDate = str_replace('/', '&nbsp;&nbsp;/&nbsp;&nbsp;', $sMetersUpdateDate);
	}
	else
	{
		$sMetersUpdateDate = '&mdash;';
	}

	?>
		<?=GetMessage("RCPT_METER_VALUES_ON_DATE")?>&nbsp;&nbsp;&nbsp;<?=$sMetersUpdateDate?>&nbsp;&nbsp;
		<table class="border" style="font-size: 90%"><?

		foreach ($arResult['METERS'] as $arMeter):

		?>
		<tr>
			<td style="white-space: nowrap;"><?=strlen($arMeter['SERVICE_NAME']) > 0 ? $arMeter['SERVICE_NAME'] : $arMeter['NAME']?></td>
			<td style="width: 15%;white-space: nowrap;" class="num"><?=__citrusReceiptNum($arMeter['VALUE_BEFORE']['VALUE1'])?></td>
			<td style="width: 15%;white-space: nowrap;" class="num"><?=__citrusReceiptNum($arMeter['VALUE']['VALUE1'])?></td>
			<td style="width: 15%;white-space: nowrap;" class="num"><?=__citrusReceiptNum($arMeter['VALUE_BEFORE']['VALUE2'])?></td>
			<td style="width: 15%;white-space: nowrap;" class="num"><?=__citrusReceiptNum($arMeter['VALUE']['VALUE2'])?></td>
		</tr><?

		endforeach;

		?>

		</table>
	</td>
</tr>
<tr>
	<td style="padding-top: 20pt;" class="tl">
		<?=GetMessage("RCPT_CHECK")?><br />
		<strong><?=GetMessage("RCPT_FOR")?> <?=$arResult['ACCOUNT_PERIOD']['DISPLAY_NAME']?> <?=GetMessage("RCPT_YEAR_ABBR")?></strong>
	</td>
	<td class="bl" style="padding-top: 7pt;">
		<strong><?=$arResult['ACCOUNT_PERIOD']['ACCOUNT_NAME']?></strong>
		<div align="right" style="font-weight: bold;"><?=GetMessage("RCPT_ACCOUNT_ABBR")?> <?=$arResult['ACCOUNT_PERIOD']['XML_ID']?></div>
		<div><?=CTszhAccount::GetFullAddress($arResult['ACCOUNT_PERIOD'])?></div>
	</td>
</tr>
<tr>
	<td>&nbsp;</td>
	<td class="bt bl small" style="padding-top: 1pt; padding-bottom: 8pt;">
		<?=$arResult['ORG_TITLE']?><br />
		<span style="white-space: nowrap;"><?=GetMessage("RCPT_INN")?> <?=$arResult['ORG_INN']?> / <?=GetMessage("RCPT_KPP")?>&nbsp;<?=$arResult['ORG_KPP']?></span>
		<br />
		<span style="white-space: nowrap;"><?=GetMessage("RCPT_RSCH")?> <?=$arResult['ORG_RSCH']?>, <?=GetMessage("RCPT_IN")?>&nbsp;<?=$arResult['ORG_BANK']?></span>,
		<span style="white-space: nowrap;"><?=GetMessage("RCPT_KSCH")?> <?=$arResult['ORG_KSCH']?></span>,
		<span style="white-space: nowrap;"><?=GetMessage("RCPT_BIK")?> <?=$arResult['ORG_BIK']?></span>
		<br />
	</td>
</tr>
<tr>
	<td colspan="2" class="bt small tl" style="padding-top: 7pt;">
		<div style="margin-left: 2pt; line-height: 140%;">
			<?=str_replace("#PEOPLE#", $arResult['ACCOUNT_PERIOD']['PEOPLE'], GetMessage("RCPT_PEOPLE_LIVING"))?>
			<?=str_replace("#AREA#", __citrusReceiptNum($arResult['ACCOUNT_PERIOD']['AREA']), GetMessage("RCPT_AREA"))?>
			<?=str_replace("#AREA#", __citrusReceiptNum($arResult['ACCOUNT_PERIOD']['LIVING_AREA']), GetMessage("RCPT_LIVING_AREA"))?>
			<br />
			<?=str_replace("#DATE#", $sMetersUpdateDate, GetMessage("RCPT_METER_VALUES_ON"))?>
		</div>
		<table class="border counters" style="width: 86%; margin-top: 2pt; font-size: 90%;">
			<thead><tr>
				<td><?=GetMessage("RCPT_SERVICE")?></td>
				<td colspan="2" class="bt bl"><?=GetMessage("RCPT_METER_VALUE_1")?></td>
				<td class="bt"><?=GetMessage("RCPT_CONSUMPTION")?></td>
				<td colspan="2" class="bt"><?=GetMessage("RCPT_METER_VALUE_2")?></td>
				<td class="bt br"><?=GetMessage("RCPT_CONSUMPTION")?></td>
			</tr></thead>
			<tbody><?


			$lastKey = array_pop(array_keys($arResult['METERS']));

			foreach ($arResult['METERS'] as $key=>$arMeter) {

				$bef1 = $arMeter['VALUE_BEFORE']['VALUE1'];
				$cur1 = $arMeter['VALUE']['VALUE1'];
				$bef2 = $arMeter['VALUE_BEFORE']['VALUE2'];
				$cur2 = $arMeter['VALUE']['VALUE2'];

				$rash1 = __citrusReceiptGetMeterAmount($arMeter["VALUE"]["AMOUNT1"], $cur1, $bef1, $arMeter["CAPACITY"], $arMeter["DEC_PLACES"]);
				$rash2 = __citrusReceiptGetMeterAmount($arMeter["VALUE"]["AMOUNT2"], $cur2, $bef2, $arMeter["CAPACITY"], $arMeter["DEC_PLACES"]);
			?>
				<tr>
					<td style="white-space: nowrap;"><?=strlen($arMeter['SERVICE_NAME']) > 0 ? $arMeter['SERVICE_NAME'] : $arMeter['NAME']?></td>
					<td class="num bl <?=$key==$lastKey?' bb':''?>"><?=__citrusReceiptNum($bef1)?></td>
					<td class="num <?=$key==$lastKey?' bb':''?>"><?=__citrusReceiptNum($cur1)?></td>
					<td class="num <?=$key==$lastKey?' bb':''?>"><?=__citrusReceiptNum($rash1)?></td>
					<td class="num <?=$key==$lastKey?' bb':''?>"><?=__citrusReceiptNum($bef2)?></td>
					<td class="num <?=$key==$lastKey?' bb':''?>"><?=__citrusReceiptNum($cur2)?></td>
					<td class="num br <?=$key==$lastKey?' bb':''?>"><?=__citrusReceiptNum($rash2)?></td>
				</tr>
			<?
			}	
			?>
			</tbody>
		</table>
		<div style="font-weight: bold; text-align: right; margin: 6pt 0;">
			<?if ($arResult['ACCOUNT_PERIOD']['DEBT_BEG'] > 0):?>
			<?=GetMessage("RCPT_CREDIT_BEG")?> <?=__citrusReceiptNum($arResult['ACCOUNT_PERIOD']['DEBT_BEG'])?> <?=GetMessage("RCPT_CURRENCY")?>
			<?else:?>
			<?=GetMessage("RCPT_DEBT_BEG")?> <?=__citrusReceiptNum(-$arResult['ACCOUNT_PERIOD']['DEBT_BEG'])?> <?=GetMessage("RCPT_CURRENCY")?>
			<?endif;?>
		</div>

		<table class="border services">
			<thead>
				<tr>
					<td style="width: 37%;"><?=GetMessage("RCPT_CHARGED")?></td>
					<td><?=GetMessage("RCPT_UNITS")?></td>
					<td><?=GetMessage("RCPT_NORM")?></td>
					<td><?=GetMessage("RCPT_TARIFF")?></td>
					<td><?=GetMessage("RCPT_QUANTITY")?></td>
					<td><?=GetMessage("RCPT_CHARGED2")?></td>
					<td><?=GetMessage("RCPT_CORRECTION")?></td>
					<td><?=GetMessage("RCPT_PRIVIL")?></td>
					<td><?=GetMessage("RCPT_PENI")?></td>
					<td><?=GetMessage("RCPT_TO_PAY")?></td>
				</tr>
			</thead>
			<tbody>
				<?foreach ($arResult['CHARGES'] as $idx => $arCharge):

					$isComponent = $arCharge["COMPONENT"] != "N";

					if ($arResult["HAS_GROUPS"] && ($idx == 0 || $arResult["CHARGES"][$idx-1]["GROUP"] != $arCharge["GROUP"])):?>
						<tr class="rcpt-group-title">
							<td colspan="10">
								<?=$arCharge["GROUP"]?>
							</td>
						</tr>
					<?endif?>
					<tr>
						<td><?=$arCharge['SERVICE_NAME']?><?=($arCharge["HAS_COMPONENTS"] && substr($arCharge['SERVICE_NAME'], -1, 1) !== ':' ? ':' : '')?></td>
						<td class="center"><?=CCitrusTszhReceiptComponentHelper::getArrayValue(array("UNITS"), $arCharge, false, $arCharge["SERVICE_UNITS"])?></td>
						<td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue(array("NORM"), $arCharge, true, $arCharge["SERVICE_NORM"], 3)?></td>
						<td class="num"><?=$arCharge["RATE"]?></td>
						<?php
						// ������� �������� �� ������������� �� 1�
						if (isset($arCharge["AMOUNT_VIEW"]) && strlen($arCharge["AMOUNT_VIEW"])) : ?>
							<td class="num"><?=$arCharge["AMOUNT_VIEW"]?></td>
						<?php else : ?>
							<td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue("AMOUNT", $arCharge, true, false, 3)?></td>
						<?php endif; ?>
						<td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue("SUMM", $arCharge, true)?></td>
						<td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue("CORRECTION", $arCharge, true)?></td>
						<td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue("COMPENSATION", $arCharge, true)?></td>
						<td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue("PENALTIES", $arCharge, true)?></td>
						<td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue("SUMM2PAY", $arCharge, true)?></td>
					</tr>
				<?endforeach?>
			</tbody>
			<tfoot>
				<tr>
					<td style="text-align: right;"><strong><?=GetMessage("RCPT_TOTAL_CHARGED")?></strong></td>
					<td class="center">&nbsp;</td>
					<td class="num">&nbsp;</td>
					<td class="num">&nbsp;</td>
					<td class="num">&nbsp;</td>
					<td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["SUMM"])?></td>
					<td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["CORRECTION"])?></td>
					<td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["COMPENSATION"])?></td>
					<td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["PENALTIES"])?></td>
					<td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["SUMM2PAY"])?></td>
				</tr>
				<tr>
					<td colspan="5" style="text-align: right;"><strong><?=GetMessage("RCPT_TOTAL_PAYED")?></strong></td>
					<td colspan="5" class="num"><?=CCitrusTszhReceiptComponentHelper::num(/*$arResult["TOTALS"]["SUMM_PAYED"]*/$arResult["ACCOUNT_PERIOD"]["SUM_PAYED"])?></td>
				</tr>
				<tr>
					<td colspan="10" style="text-align: right;">
						<?=GetMessage("RCPT_DEBT_END")?>
						<?=__citrusReceiptNum($arResult['SUM2PAY'])?>
						<?=GetMessage("RCPT_CURRENCY")?>
					</td>
				</tr>
			</tfoot>
		</table>
	</td>
</tr>
</table>
<?

$summ2pay = $arResult["ACCOUNT_PERIOD"]["DEBT_END"];
if (COption::GetOptionString("citrus.tszh", "pay_to_executors_only", "N") == "Y") {
	if (CModule::IncludeModule("vdgb.portaljkh") && method_exists("CCitrusPortalTszh", "setPaymentBase"))
		CCitrusPortalTszh::setPaymentBase($arResult["TSZH"]);
	$summ2pay = CTszhAccountContractor::GetList(array(), array("ACCOUNT_PERIOD_ID" => $arResult["ACCOUNT_PERIOD"]["ID"], "!CONTRACTOR_EXECUTOR" => "N"), array("SUMM"))->Fetch();
	$summ2pay = is_array($summ2pay) ? $summ2pay['SUMM'] - $arResult["ACCOUNT_PERIOD"]["PREPAYMENT"] : 0;
}
if ($summ2pay > 0 && CModule::IncludeModule("citrus.tszhpayment") && ($paymentPath = CTszhPaySystem::getPaymentPath($arResult["TSZH"]["SITE_ID"])))
	echo '<div class="no-print">' . GetMessage("CITRUS_TSZHPAYMENT_LINK", Array("#LINK#" => $paymentPath . '?ptype=' . $arResult['RECEIPT_PAYMENT_TYPE'])) . '</div>';

?>
</div>
<?if ($_GET["print"] == "Y"):
	if ($arResult["IS_LAST"]):
		if ($arResult["MODE"] == "AGENT"):?>
			</div>
		<?else:?>
			</body>
			</html>
		<?
		exit();
		endif;
	endif;

	if (in_array($arResult["MODE"], array("ADMIN", "AGENT")))
	{
		return;
	}
	else
	{
		require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
		die();
	}
endif;
