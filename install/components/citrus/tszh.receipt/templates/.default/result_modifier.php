<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

// �������� � ������������ �������� �������� ��� � ������ ������������� ������������ �����
$arNames = Array();
foreach ($arResult['METERS'] as $key=>$arItem) {
	if (strlen($arItem["SERVICE_NAME"]) > 0) {
		if (array_key_exists($arItem["SERVICE_NAME"], $arNames)) {
			$arResult["METERS"][$key]["SERVICE_NAME"] = $arItem["SERVICE_NAME"] . " ({$arResult["METERS"][$key]["NAME"]})";
			$key2 = $arNames[$arItem["SERVICE_NAME"]];
			$arResult["METERS"][$key2]["SERVICE_NAME"] = $arItem["SERVICE_NAME"] . " ({$arResult["METERS"][$key2]["NAME"]})";
		} else {
			$arNames[$arItem["SERVICE_NAME"]] = $key;
		}		
	}
}

/**
 * ���� ��������� �� ���. ������ ��������� ��������� ����������� �� ��������� ����
 */
if ($arResult["IS_OVERHAUL"])
{
	$arResult["ORG_BANK"] = strlen($arResult["ACCOUNT_PERIOD"]["HOUSE_OVERHAUL_BANK"]) > 0 ? $arResult["ACCOUNT_PERIOD"]["HOUSE_OVERHAUL_BANK"] : $arResult["ORG_BANK"];
	$arResult["ORG_BIK"] = strlen($arResult["ACCOUNT_PERIOD"]["HOUSE_OVERHAUL_BIK"]) > 0 ? $arResult["ACCOUNT_PERIOD"]["HOUSE_OVERHAUL_BIK"] : $arResult["ORG_BIK"];
	$arResult["ORG_RSCH"] = strlen($arResult["ACCOUNT_PERIOD"]["HOUSE_OVERHAUL_RS"]) > 0 ? $arResult["ACCOUNT_PERIOD"]["HOUSE_OVERHAUL_RS"] : $arResult["ORG_RSCH"];
	$arResult["ORG_KSCH"] = strlen($arResult["ACCOUNT_PERIOD"]["HOUSE_OVERHAUL_KS"]) > 0 ? $arResult["ACCOUNT_PERIOD"]["HOUSE_OVERHAUL_KS"] : $arResult["ORG_KSCH"];
}
else
{
	$arResult["ORG_BANK"] = strlen($arResult["ACCOUNT_PERIOD"]["HOUSE_BANK"]) > 0 ? $arResult["ACCOUNT_PERIOD"]["HOUSE_BANK"] : $arResult["ORG_BANK"];
	$arResult["ORG_BIK"] = strlen($arResult["ACCOUNT_PERIOD"]["HOUSE_BIK"]) > 0 ? $arResult["ACCOUNT_PERIOD"]["HOUSE_BIK"] : $arResult["ORG_BIK"];
	$arResult["ORG_RSCH"] = strlen($arResult["ACCOUNT_PERIOD"]["HOUSE_RS"]) > 0 ? $arResult["ACCOUNT_PERIOD"]["HOUSE_RS"] : $arResult["ORG_RSCH"];
	$arResult["ORG_KSCH"] = strlen($arResult["ACCOUNT_PERIOD"]["HOUSE_KS"]) > 0 ? $arResult["ACCOUNT_PERIOD"]["HOUSE_KS"] : $arResult["ORG_KSCH"];
}

?>
