<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

// �������� � ������������ �������� �������� ��� � ������ ������������� ������������ �����
$arNames = Array();
foreach ($arResult['METERS'] as $key=>$arItem) {
	if (strlen($arItem["SERVICE_NAME"]) > 0) {
		if (array_key_exists($arItem["SERVICE_NAME"], $arNames)) {
			$arResult["METERS"][$key]["SERVICE_NAME"] = $arItem["SERVICE_NAME"] . " ({$arResult["METERS"][$key]["NAME"]})";
			$key2 = $arNames[$arItem["SERVICE_NAME"]];
			$arResult["METERS"][$key2]["SERVICE_NAME"] = $arItem["SERVICE_NAME"] . " ({$arResult["METERS"][$key2]["NAME"]})";
		} else {
			$arNames[$arItem["SERVICE_NAME"]] = $key;
		}
	}
}

// set default value for missing template parameters
$templateParams = CComponentUtil::GetTemplateProps($this->__component->getName(), $this->getName());
if (is_array($templateParams))
{
	foreach (array("METER_VALUES_START_DATE", "METER_VALUES_END_DATE", "PAY_BEFORE_DAY", "NOTE_TEXT") as $paramName)
	{
		if (!is_set($arParams, $paramName) && is_set($templateParams[$paramName], "DEFAULT"))
			$arParams[$paramName] = $templateParams[$paramName]["DEFAULT"];
	}
}

foreach ($arResult["METERS"] as $meterKey => $arMeter)
{
	if (is_set($arMeter, "VALUE") && is_set($arMeter["VALUE"], "TIMESTAMP_X"))
		$arResult["METERS"][$meterKey]["VALUE"]["TIMESTAMP_X_DATE"] = ConvertTimeStamp(MakeTimeStamp($arMeter["VALUE"]["TIMESTAMP_X"]));

	if (is_set($arMeter, "VALUE_BEFORE") && is_set($arMeter["VALUE_BEFORE"], "TIMESTAMP_X"))
		$arResult["METERS"][$meterKey]["VALUE_BEFORE"]["TIMESTAMP_X_DATE"] = ConvertTimeStamp(MakeTimeStamp($arMeter["VALUE_BEFORE"]["TIMESTAMP_X"]));
}

if (!function_exists("__citrusSetReceiptOrder")):
	function __citrusSetReceiptOrder(&$arItems, $arContractors, $itemField = "CONTRACTOR_ID", $contractorField = "CONTRACTOR_ID")
	{
		foreach ($arItems as $itemID => $arItem)
		{
			$receiptOrder = false;
			$contractorXmlID = false;
			if (strlen($arItem[$itemField]))
			{
				foreach ($arContractors as $arContractor)
				{
					if ($arContractor[$contractorField] == $arItem[$itemField])
					{
						$receiptOrder = strlen($arContractor["RECEIPT_ORDER"]) ? intval($arContractor["RECEIPT_ORDER"]) : false;
						$contractorXmlID = strlen($arContractor["CONTRACTOR_XML_ID"]) ? $arContractor["CONTRACTOR_XML_ID"] : false;
						break;
					}
				}
			}

			$arItems[$itemID]["RECEIPT_ORDER"] = $receiptOrder;
			$arItems[$itemID]["CONTRACTOR_XML_ID"] = $contractorXmlID;
		}
	}
endif;

if (!function_exists("__citrusSortByReceiptOrderFunction")):
	function __citrusSortByReceiptOrderFunction($a, $b)
	{
		if (strlen($a["RECEIPT_ORDER"]) && strlen($b["RECEIPT_ORDER"]))
			$key = "RECEIPT_ORDER";
		else
			$key = "CONTRACTOR_XML_ID";

		$a = intval($a[$key]);
		$b = intval($b[$key]);

		if ($a == $b)
			return 0;

		return ($a < $b) ? -1 : 1;
	}
endif;

if (!function_exists("__citrusSortByReceiptOrder")):
	function __citrusSortByReceiptOrder(&$arItems)
	{
		$arHaveOrder = array();
		$arHaveXmlID = array();
		$arHaveNothing = array();
		foreach ($arItems as $itemID => $arItem)
		{
			if (strlen($arItem["RECEIPT_ORDER"]))
				$arHaveOrder[$itemID] = $arItem;
			elseif (strlen($arItem["CONTRACTOR_XML_ID"]))
				$arHaveXmlID[$itemID] = $arItem;
			else
				$arHaveNothing[$itemID] = $arItem;
		}

		uasort($arHaveOrder, "__citrusSortByReceiptOrderFunction");
		uasort($arHaveXmlID, "__citrusSortByReceiptOrderFunction");
		$arItems = $arHaveOrder + $arHaveXmlID + $arHaveNothing;
	}
endif;

// set RECEIPT_ORDER 
__citrusSetReceiptOrder($arResult["CHARGES"], $arResult["CONTRACTORS"], "~SERVICE_NAME", "~CONTRACTOR_SERVICES");
__citrusSetReceiptOrder($arResult["CORRECTIONS"], $arResult["CONTRACTORS"]);
__citrusSetReceiptOrder($arResult["INSTALLMENTS"], $arResult["CONTRACTORS"]);

/*// sort by RECEIPT_ORDER 
__citrusSortByReceiptOrder($arResult["CHARGES"]);
__citrusSortByReceiptOrder($arResult["CONTRACTORS"]);
__citrusSortByReceiptOrder($arResult["CORRECTIONS"]);
__citrusSortByReceiptOrder($arResult["INSTALLMENTS"]);
*/

/**
 * � �������� ���������� ������� � ��� ���������� ������ ������ ����������-����������� ����� (��� ������� � 1�)
 */
if (is_array($arResult["EXECUTOR"]))
{
	$arResult["TSZH"]["ADDRESS"] = $arResult["EXECUTOR"]["ADDRESS"];
	$fieldsMap = array(
		"ORG_TITLE" => 'NAME',
		"ORG_INN" => 'INN',
		"ORG_KPP" => 'KPP',
		"ORG_RSCH" => 'RSCH',
		"ORG_BANK" => 'BANK',
		"ORG_KSCH" => 'KSCH',
		"ORG_BIK" => 'BIK',
	);
	foreach ($fieldsMap as $to => $from)
	{
		if (isset($arResult["EXECUTOR"][$from]) && strlen($arResult["EXECUTOR"][$from]))
		{
			$arResult[$to] = $arResult["EXECUTOR"][$from];
		}
	}
}

/**
 * ���� ��������� �� ���. ������ ��������� ��������� ����������� �� ��������� ����
 */
if ($arResult["IS_OVERHAUL"])
{
	$arResult["ORG_BANK"] = strlen($arResult["ACCOUNT_PERIOD"]["HOUSE_OVERHAUL_BANK"]) > 0 ? $arResult["ACCOUNT_PERIOD"]["HOUSE_OVERHAUL_BANK"] : $arResult["ORG_BANK"];
	$arResult["ORG_BIK"] = strlen($arResult["ACCOUNT_PERIOD"]["HOUSE_OVERHAUL_BIK"]) > 0 ? $arResult["ACCOUNT_PERIOD"]["HOUSE_OVERHAUL_BIK"] : $arResult["ORG_BIK"];
	$arResult["ORG_RSCH"] = strlen($arResult["ACCOUNT_PERIOD"]["HOUSE_OVERHAUL_RS"]) > 0 ? $arResult["ACCOUNT_PERIOD"]["HOUSE_OVERHAUL_RS"] : $arResult["ORG_RSCH"];
	$arResult["ORG_KSCH"] = strlen($arResult["ACCOUNT_PERIOD"]["HOUSE_OVERHAUL_KS"]) > 0 ? $arResult["ACCOUNT_PERIOD"]["HOUSE_OVERHAUL_KS"] : $arResult["ORG_KSCH"];
}
else
{
	$arResult["ORG_BANK"] = strlen($arResult["ACCOUNT_PERIOD"]["HOUSE_BANK"]) > 0 ? $arResult["ACCOUNT_PERIOD"]["HOUSE_BANK"] : $arResult["ORG_BANK"];
	$arResult["ORG_BIK"] = strlen($arResult["ACCOUNT_PERIOD"]["HOUSE_BIK"]) > 0 ? $arResult["ACCOUNT_PERIOD"]["HOUSE_BIK"] : $arResult["ORG_BIK"];
	$arResult["ORG_RSCH"] = strlen($arResult["ACCOUNT_PERIOD"]["HOUSE_RS"]) > 0 ? $arResult["ACCOUNT_PERIOD"]["HOUSE_RS"] : $arResult["ORG_RSCH"];
	$arResult["ORG_KSCH"] = strlen($arResult["ACCOUNT_PERIOD"]["HOUSE_KS"]) > 0 ? $arResult["ACCOUNT_PERIOD"]["HOUSE_KS"] : $arResult["ORG_KSCH"];
}