<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
{
	die();
}
global $APPLICATION, $DB;
if ($_GET["print"] == "Y")
{
	$APPLICATION->RestartBuffer();
}

$arHideODN = array();
if (is_array($arParams['HIDE_ODN']) && count($arParams['HIDE_ODN']) > 0)
{
	$arHideODN = array_flip($arParams['HIDE_ODN']);
}

{
	if (!function_exists("__citrusReceiptNum")):
		function __citrusReceiptNum($number, $decimals = 2, $canBeEmpty = false, $isComponent = false)
		{
			if ($number == 0)
			{
				if ($isComponent)
				{
					return '<div style="text-align: center;">X</div>';
				}

				if ($canBeEmpty)
				{
					return "&nbsp;";
				}
				else
				{
					return "0";
				}
			}

			return number_format($number, $decimals, ',', ' ');
		}
	endif;
}

if (!function_exists("__citrusGetPlaceholderCellsHtml")):
	function __citrusGetPlaceholderCellsHtml($cellCount, $arContent)
	{
		$arCells = array();
		for ($i = 0; $i < $cellCount; $i++)
		{
			$arCells[$i] = "&nbsp;";
		}

		foreach ($arContent as $arCellContent)
		{
			$cellNumber = $arCellContent["align"] == "left" ? 0 : $cellCount - 1;
			if ($arCellContent["split"])
			{
				for ($i = 0; $i < strlen($arCellContent["content"]); $i++)
				{
					if ($arCellContent["align"] == "left")
					{
						$cellNumber = $i;
						$cellContent = substr($arCellContent["content"], $i, 1);
					}
					else
					{
						$cellNumber = $cellCount - $i - 1;
						$cellContent = substr($arCellContent["content"], -($i + 1), 1);
					}
					$arCells[$cellNumber] = $cellContent;
					if ($arCellContent["bold"])
					{
						$arCells[$cellNumber] = "<span class=\"bold\">{$arCells[$cellNumber]}</span>";
					}
				}
			}
			else
			{
				$arCells[$cellNumber] = $arCellContent["content"];
				if ($arCellContent["bold"])
				{
					$arCells[$cellNumber] = "<span class=\"bold\">{$arCells[$cellNumber]}</span>";
				}
			}
		} ?>

		<table class="placeholder-table">
			<tr>
				<? foreach ($arCells as $cell): ?>
					<td><?=$cell?></td>
				<? endforeach ?>
			</tr>
		</table>
		<?
	}
endif;

if (!function_exists("__citrusGetMeterValues")):
	function __citrusGetMeterValues($arMeter, $lastExistingValuesFlag = false)
	{
		$arResult = array();
		for ($i = 1; $i <= $arMeter["VALUES_COUNT"]; $i++)
		{
			$value = false;

			if ($lastExistingValuesFlag)
			{
				if (is_array($arMeter["VALUE"]) && !empty($arMeter["VALUE"]))
				{
					$value = $arMeter["VALUE"]["VALUE" . $i];
				}
				elseif (is_array($arMeter["VALUE_BEFORE"]) && !empty($arMeter["VALUE_BEFORE"]))
				{
					$value = $arMeter["VALUE_BEFORE"]["VALUE" . $i];
				}
			}
			else
			{
				$value = $arMeter["VALUE"]["VALUE" . $i];
			}

			if ($value !== false)
			{
				$arResult[] = __citrusReceiptNum($value, $arMeter["DEC_PLACES"], true);
			}
		}

		return $arResult;
	}
endif;

if ($_GET["print"] != "Y")
{
	$APPLICATION->AddHeadString('<link href="' . $this->GetFolder() . '/styles.css" rel="stylesheet" type="text/css" />');
	?>
	<p><?=GetMessage("RCPT_YOU_CAN")?> <a href="<?=$APPLICATION->GetCurPageParam('print=Y', Array('print'))?>"
	                                      target="_blank"><?=GetMessage("RCPT_OPEN_RECEIPT_IN_WINDOW")?></a> <?=GetMessage("RCPT_FOR_PRINTING")?></p>
	<?

}
else
{
	if ($arResult["IS_FIRST"]):
		if ($arResult["MODE"] == "AGENT"):?>
			<style type="text/css">
				<?=$APPLICATION->GetFileContent($_SERVER["DOCUMENT_ROOT"] . $this->GetFolder() . "/styles.css")?>
			</style>
			<?
		else:
			?><!DOCTYPE html>
			<html>
			<head>
			<meta http-equiv="content-type" content="text/html;" charset="<?=SITE_CHARSET?>"/>
			<link href="<?=$this->GetFolder()?>/styles.css" rel="stylesheet" type="text/css"/>
		<? endif ?>
		<style type="text/css" media="print">
			@page {
				size: 21cm 29.7cm;
				margin: 0.5cm 0.5cm;
			}
			<?if ($arParams["INLINE"] != "Y"):?>
			#receipt div.receipt {
				page-break-after: always;
				/*page-break-inside: avoid;*/
			}
			div.page-break {
				page-break-after: always;
			}
			<?endif?>
		</style>
		<? if ($arResult["MODE"] == "ADMIN"): ?>
		<style type="text/css" media="screen">
			#receipt div.receipt {
				border-bottom: 1pt dashed #000;
			}
		</style>
	<?endif;

		if ($arResult["MODE"] !== "AGENT"):?>
			<title><?=GetMessage("RCPT_TITLE")?></title>
			</head>
			<body id="receipt" onload="window.print();">
			<?
		else:?>
			<div id="receipt">
		<?endif;
	endif;
}
//echo '<pre>' . /*htmlspecialchars(*/print_r($arResult, true)/*)*/ . '</pre>';
?>
	<div class="receipt">
		<?
		// ����� ��� �������� ��������� �� ���������� ��� ��������� � ����
		if (!$arResult["IS_FINES_RECEIPT"] && !empty($arResult["METERS"]))
		{
			?>
			<div class="top-section">
				<?
				if (!$arResult["HAS_SEPARATE_PENALTIES_RECEIPT"])
				{
					?>
					<div class="main-header center"><?=GetMessage("RCPT_MAIN_HEADER")?></div><?
				}
				?>
				<div class="address center bold"><?=CTszhAccount::GetFullAddress($arResult["ACCOUNT_PERIOD"])?></div>
				<div class="header center"><?=GetMessage("RCPT_HEADER")?></div>
				<div style="margin-top: 5pt; overflow: hidden;">
					<div style="float: left;">
						<? $arContent = array(
							array(
								"content" => $arResult["ACCOUNT_PERIOD"]["XML_ID"],
								"align" => "left",
								"split" => true,
							),
						);
						__citrusGetPlaceholderCellsHtml(max(15, strlen($arResult["ACCOUNT_PERIOD"]["XML_ID"])), $arContent); ?>
					</div>

					<? if (!empty($arResult["ACCOUNT_PERIOD"]["BARCODES"])): ?>
						<table border="0" style="float: right;">
							<tr>
								<? foreach ($arResult["ACCOUNT_PERIOD"]["BARCODES"] as $idx => $barcode)
								{
									/** @noinspection PhpAssignmentInConditionInspection */
									if ($link = CTszhBarCode::getImageLink($barcode))
									{
										$code128 = $barcode["TYPE"] == 'code128';
										?>
										<td class="no-border rcpt-barcode-cell"><img
												style="<?=($code128 ? 'width: 5.5cm' : 'width: 3cm; height: 3cm; max-height: 4cm;')?>"
												class="rcpt-barcode" src="<?=$link?>" alt="<?=htmlspecialcharsbx($barcode["VALUE"])?>"></td><?
									}
								} ?>
							</tr>
                        <?if (!empty($arResult["ACCOUNT_PERIOD"]["BARCODES_INSURANCE"])):?>
							<tr>
								<? foreach ($arResult["ACCOUNT_PERIOD"]["BARCODES_INSURANCE"] as $idx => $barcode)
								{
									/** @noinspection PhpAssignmentInConditionInspection */
									if ($link = CTszhBarCode::getImageLink($barcode))
									{
										$code128 = $barcode["TYPE"] == 'code128';
										?>
										<td class="no-border rcpt-barcode-cell"><img
												style="<?=($code128 ? 'width: 5.5cm' : 'width: 3cm; height: 3cm; max-height: 4cm;')?>"
												class="rcpt-barcode" src="<?=$link?>" alt="<?=htmlspecialcharsbx($barcode["VALUE"])?>"></td><?
									}
								} ?>
							</tr>
                        <?endif;?>
						</table>
					<? endif ?>
				</div>

				<div class="header-note">
					<?=GetMessage("RCPT_HEADER_NOTE", array(
						"#START_DATE#" => $arParams["METER_VALUES_START_DATE"],
						"#END_DATE#" => $arParams["METER_VALUES_END_DATE"],
					))?>
				</div>

				<table class="border" style="margin-top: 10pt;">
					<tr>
						<td class="center nowrap"><?=GetMessage("RCPT_METER_SERVICE_NAME")?></td>
						<td class="center nowrap"><?=GetMessage("RCPT_METER_NUMBER")?></td>
						<td class="center nowrap"><?=GetMessage("RCPT_METER_LAST_VALUES")?></td>
						<td class="center nowrap"><?=GetMessage("RCPT_METER_CUR_VALUES")?></td>
						<td class="center nowrap"><?=GetMessage("RCPT_METER_DATE")?></td>
					</tr>
					<? foreach ($arResult["METERS"] as $arMeter): ?>
						<tr>
							<td class="nowrap"><?=strlen($arMeter["SERVICE_NAME"]) > 0 ? $arMeter["SERVICE_NAME"] : $arMeter["NAME"]?></td>
							<td class="nowrap"><?=$arMeter["NUM"]?></td>
							<td class="num nowrap"><?=implode(" / ", __citrusGetMeterValues($arMeter, true))?></td>
							<td class="num nowrap">&nbsp;</td>
							<td class="center nowrap">&nbsp;</td>
						</tr>
					<? endforeach ?>
				</table>

				<div class="abonent-sign">
					<?=GetMessage("RCPT_DATA_ACCEPT")?> <span
							class="line"><?=str_repeat("&nbsp;", 40)?></span> <?=$arResult["ACCOUNT_PERIOD"]["ACCOUNT_NAME"]?>
				</div>
			</div>

			<div class="cut-line"><?=GetMessage("RCPT_CUT_LINE")?></div>
			<?
		}
		?>
		<? if (!empty($arResult["ACCOUNT_PERIOD"]["BARCODES"])): ?>
			<table border="0" style="float: right; margin-bottom: 5pt;">
				<tr>
					<? foreach ($arResult["ACCOUNT_PERIOD"]["BARCODES"] as $idx => $barcode)
					{
						/** @noinspection PhpAssignmentInConditionInspection */
						if ($link = CTszhBarCode::getImageLink($barcode))
						{
							$code128 = $barcode["TYPE"] == 'code128';
							?>
							<td class="no-border rcpt-barcode-cell"><img
									style="<?=($code128 ? 'width: 5.5cm' : 'width: 3cm; height: 3cm; max-height: 4cm;')?>" class="rcpt-barcode"
									src="<?=$link?>" alt="<?=htmlspecialcharsbx($barcode["VALUE"])?>"></td><?
						}
					} ?>
				</tr>
				<tr>
					<? foreach ($arResult["ACCOUNT_PERIOD"]["BARCODES_INSURANCE"] as $idx => $barcode)
					{
						/** @noinspection PhpAssignmentInConditionInspection */
						if ($link = CTszhBarCode::getImageLink($barcode))
						{
							$code128 = $barcode["TYPE"] == 'code128';
							?>
							<td class="no-border rcpt-barcode-cell"><img
									style="<?=($code128 ? 'width: 5.5cm' : 'width: 3cm; height: 3cm; max-height: 4cm;')?>" class="rcpt-barcode"
									src="<?=$link?>" alt="<?=htmlspecialcharsbx($barcode["VALUE"])?>"></td><?
						}
					} ?>
				</tr>
			</table>
		<? endif ?>

		<table class="no-border bb">
			<tr>
				<td style="width: 11%;" class="tl">
					<?=GetMessage("RCPT_NOTICE")?>
				</td>
				<td class="bl small">
					<table class="no-border" style="width: 100%;">
						<tr>
							<td class="tl"><?=GetMessage("RCPT_RECIPIENT")?>:</td>
							<td class="bold" style="vertical-align: middle;"><?=$arResult["ORG_TITLE"]?></td>
							<td>
								<table class="border calc-period">
									<tr>
										<td class="center nowrap"><?=GetMessage("RCPT_CALC_PERIOD")?><? $arPeriod = explode(" ", $arResult["ACCOUNT_PERIOD"]["DISPLAY_NAME"]); ?></td>
										<td class="center" style="width: 33.33%"><?=$arPeriod[0]?></td>
										<td class="center" style="width: 33.33%"><?=$arPeriod[1]?></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td class="tl"><?=GetMessage("RCPT_REQUISITES")?>:</td>
							<td colspan="2">
								<table class="no-border" style="width: 100%;">
									<tr>
										<td class="nowrap"><?=GetMessage("RCPT_INN")?>:</td>
										<td class="nowrap"><span class="bb"><?=$arResult["ORG_INN"]?></span></td>
										<td class="nowrap"><?=GetMessage("RCPT_KPP")?>:&nbsp;<span class="bb"><?=$arResult["ORG_KPP"]?></span></td>
									</tr>
									<tr>
										<td class="nowrap"><?=GetMessage("RCPT_RSCH")?>:</td>
										<td colspan="2" class="nowrap">
											<span class="bb"><?=$arResult["ORG_RSCH"]?>&nbsp;<?=GetMessage("RCPT_IN")?>
												&nbsp;<?=$arResult["ORG_BANK"]?></span>
										</td>
									</tr>
									<tr>
										<td class="nowrap"><?=GetMessage("RCPT_KSCH")?>:</td>
										<td class="nowrap"><span class="bb"><?=$arResult["ORG_KSCH"]?></span></td>
										<td class="nowrap"><?=GetMessage("RCPT_BIK")?>: <span class="bb"><?=$arResult['ORG_BIK']?></span></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td class="tl"><?=GetMessage("RCPT_ADDRESS")?>:</td>
							<td><?=$arResult["TSZH"]["ADDRESS"]?></td>
							<td class="nowrap">
								<? $arContent = array(
									array(
										"content" => ((($arResult["IS_OVERHAUL"]) || !sizeof($arResult["ACCOUNT_PERIOD"]["BARCODES_INSURANCE"])) ? GetMessage("RCPT_TO_PAY") : GetMessage("RCPT_TO_PAY_WITHOUT_INSURANCE")),
										"align" => "left",
										"split" => false,
										"bold" => true,
									),
									array(
										"content" => strval($arResult["ACCOUNT_PERIOD"]["SUMM_TO_PAY"]),
										"align" => "right",
										"split" => true,
										"bold" => true,
									),
								);
								__citrusGetPlaceholderCellsHtml(10, $arContent) ?>
							</td>
						</tr>
						<? if (!(($arResult["IS_OVERHAUL"]) || !sizeof($arResult["ACCOUNT_PERIOD"]["BARCODES_INSURANCE"]))): ?>
							<tr>
								<td class="tl"></td>
								<td></td>
								<td class="nowrap">
									<? $arContent = array(
										array(
											"content" => GetMessage("RCPT_TO_PAY_WITH_INSURANCE"),
											"align" => "left",
											"split" => false,
											"bold" => true,
										),
										array(
											"content" => strval(number_format($arResult["ACCOUNT_PERIOD"]["SUMM_TO_PAY"] + $arResult["INSURANCE"]["SUMM"], 2, '.', '')),
											"align" => "right",
											"split" => true,
											"bold" => true,
										),
									);
									__citrusGetPlaceholderCellsHtml(10, $arContent) ?>
								</td>
							</tr>
						<? endif; ?>
						<tr>
							<td class="tl"><?=GetMessage("RCPT_CONTACT_CENTER")?>:</td>
							<td>
								<?=$arResult["IS_OVERHAUL"] ? $arResult["TSZH"]["ADDITIONAL_INFO_OVERHAUL"] : $arResult["TSZH"]["ADDITIONAL_INFO_MAIN"]?>
							</td>
							<td class="nowrap" style="padding: 0 0 0 24%; vertical-align: bottom;"><?=GetMessage("RCPT_PAY_SUM")?>:</td>
						</tr>
						<tr>
							<td class="tl bold"><?=GetMessage("RCPT_ACCOUNT_NUMBER")?>:</td>
							<td>
								<? $arContent = array(
									array(
										"content" => $arResult["ACCOUNT_PERIOD"]["XML_ID"],
										"align" => "left",
										"split" => true,
									),
								);
								__citrusGetPlaceholderCellsHtml(max(15, strlen($arResult["ACCOUNT_PERIOD"]["XML_ID"])), $arContent); ?>
							</td>
							<td class="nowrap">
								<div class="b2" style="width: 59%; height: 3em; float: right"></div>
							</td>
						</tr>
						<tr>
							<td class="tl"><?=GetMessage("RCPT_FIO")?>:</td>
							<td colspan="2" class="bold"><?=$arResult["ACCOUNT_PERIOD"]["ACCOUNT_NAME"]?></td>
						</tr>
						<tr>
							<td class="tl"><?=GetMessage("RCPT_ADDRESS")?>:</td>
							<td><?=CTszhAccount::GetFullAddress($arResult["ACCOUNT_PERIOD"])?></td>
							<td>
								<table class="no-border" style="width: 100%;">
									<tr>
										<td style="width: 40%"></td>
										<td class="b2 center" style="width: 30%; padding: 3pt;"><?=GetMessage("RCPT_SIGN")?></td>
										<td class="b2 center" style="width: 30%; padding: 3pt;"><?=GetMessage("RCPT_DATE")?></td>
									</tr>
									<tr>
										<td></td>
										<td class="b2">&nbsp;</td>
										<td class="b2">&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td class="tl" style="padding-top: 5pt; border-top: 1pt dashed #000;">
					<?=GetMessage("RCPT_CHECK")?>
				</td>
				<td class="bl small" style="padding-top: 5pt; border-top: 1pt dashed #000;">
					<table class="no-border" style="width: 100%;">
						<tr>
							<td class="tl"><?=GetMessage("RCPT_RECIPIENT")?>:</td>
							<td class="bold" style="vertical-align: middle;"><?=$arResult["ORG_TITLE"]?></td>
							<td>
								<table class="border calc-period">
									<tr>
										<td class="center nowrap"><?=GetMessage("RCPT_CALC_PERIOD")?><? $arPeriod = explode(" ", $arResult["ACCOUNT_PERIOD"]["DISPLAY_NAME"]); ?></td>
										<td class="center" style="width: 33.33%"><?=$arPeriod[0]?></td>
										<td class="center" style="width: 33.33%"><?=$arPeriod[1]?></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td class="tl"><?=GetMessage("RCPT_REQUISITES")?>:</td>
							<td colspan="2">
								<table class="no-border" style="width: 100%;">
									<tr>
										<td class="nowrap"><?=GetMessage("RCPT_INN")?>:</td>
										<td class="nowrap"><span class="bb"><?=$arResult["ORG_INN"]?></span></td>
										<td class="nowrap"><?=GetMessage("RCPT_KPP")?>:&nbsp;<span class="bb"><?=$arResult["ORG_KPP"]?></span></td>
									</tr>
									<tr>
										<td class="nowrap"><?=GetMessage("RCPT_RSCH")?>:</td>
										<td colspan="2" class="nowrap">
											<span class="bb"><?=$arResult["ORG_RSCH"]?>&nbsp;<?=GetMessage("RCPT_IN")?>
												&nbsp;<?=$arResult["ORG_BANK"]?></span>
										</td>
									</tr>
									<tr>
										<td class="nowrap"><?=GetMessage("RCPT_KSCH")?>:</td>
										<td class="nowrap"><span class="bb"><?=$arResult["ORG_KSCH"]?></span></td>
										<td class="nowrap"><?=GetMessage("RCPT_BIK")?>: <span class="bb"><?=$arResult['ORG_BIK']?></span></td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td class="tl"><?=GetMessage("RCPT_PAYER")?>:</td>
							<td class="bold"><?=$arResult["ACCOUNT_PERIOD"]["ACCOUNT_NAME"]?></td>
							<td class="nowrap">
								<? $arContent = array(
									array(
										"content" => (($arResult["IS_OVERHAUL"] || !sizeof($arResult["ACCOUNT_PERIOD"]["BARCODES_INSURANCE"])) ? GetMessage("RCPT_TO_PAY") : GetMessage("RCPT_TO_PAY_WITHOUT_INSURANCE")),
										"align" => "left",
										"split" => false,
										"bold" => true,
									),
									array(
										"content" => strval($arResult["ACCOUNT_PERIOD"]["SUMM_TO_PAY"]),
										"align" => "right",
										"split" => true,
										"bold" => true,
									),
								);
								__citrusGetPlaceholderCellsHtml(10, $arContent) ?>
							</td>
						</tr>
						<? if (!(($arResult["IS_OVERHAUL"]) || (!sizeof($arResult["ACCOUNT_PERIOD"]["BARCODES_INSURANCE"])))): ?>
							<tr>
								<td class="tl"><?=GetMessage("RCPT_ADDRESS")?>:</td>
								<td><?=CTszhAccount::GetFullAddress($arResult["ACCOUNT_PERIOD"])?></td>
								<td class="nowrap">
									<? $arContent = array(
										array(
											"content" => GetMessage("RCPT_TO_PAY_WITH_INSURANCE"),
											"align" => "left",
											"split" => false,
											"bold" => true,
										),
										array(
											"content" => strval(number_format($arResult["ACCOUNT_PERIOD"]["SUMM_TO_PAY"] + $arResult["INSURANCE"]["SUMM"], 2, '.', '')),
											"align" => "right",
											"split" => true,
											"bold" => true,
										),
									);
									__citrusGetPlaceholderCellsHtml(10, $arContent) ?>
								</td>
							</tr>
						<? endif; ?>
					</table>

					<table class="no-border" style="width: 100%; margin-top: 5pt;">
						<tr>
							<td>
								<table class="border" style="width: 100%;">
									<tr>
										<td class="f90"><?=GetMessage("RCPT_FLAT_TYPE")?>:</td>
										<td class="f90"><?=$arResult["ACCOUNT_PERIOD"]["FLAT_TYPE"]?></td>
									</tr>
									<tr>
										<td class="f90"><?=GetMessage("RCPT_COMMON_LIVING_AREA")?>:</td>
										<td class="f90"><?=CCitrusTszhReceiptComponentHelper::num($arResult["ACCOUNT_PERIOD"]["AREA"], false, -1)?>
											/ <?=CCitrusTszhReceiptComponentHelper::num($arResult["ACCOUNT_PERIOD"]["LIVING_AREA"], false, -1)?></td>
									</tr>
									<tr>
										<td class="f90"><?=GetMessage("RCPT_REGISTERED_PEOPLE_PEOPLE")?>:</td>
										<td class="f90"><?=$arResult["ACCOUNT_PERIOD"]["REGISTERED_PEOPLE"]?>
											/ <?=$arResult["ACCOUNT_PERIOD"]["PEOPLE"]?></td>
									</tr>
									<tr>
										<td class="f90"><?=GetMessage("RCPT_EXEMPT_PEOPLE")?>:</td>
										<td class="f90"><?=($arResult["ACCOUNT_PERIOD"]["EXEMPT_PEOPLE"] > 0 ? $arResult["ACCOUNT_PERIOD"]["EXEMPT_PEOPLE"] : '-')?></td>
									</tr>
									<tr>
										<td class="f90"><?=GetMessage("RCPT_HOUSE_AREA")?>:</td>
										<td class="f90"><?=CCitrusTszhReceiptComponentHelper::num($arResult["ACCOUNT_PERIOD"]["HOUSE_AREA"], false, -1)?></td>
									</tr>
									<tr>
										<td class="f90"><?=GetMessage("RCPT_HOUSE_ROOMS_AREA")?>:</td>
										<td class="f90"><?=CCitrusTszhReceiptComponentHelper::num($arResult["ACCOUNT_PERIOD"]["HOUSE_ROOMS_AREA"], false, -1)?></td>
									</tr>
									<tr>
										<td class="f90"><?=GetMessage("RCPT_HOUSE_MOP_AREA")?>:</td>
										<td class="f90"><?=CCitrusTszhReceiptComponentHelper::num($arResult["ACCOUNT_PERIOD"]["HOUSE_COMMON_PLACES_AREA"], false, -1)?></td>
									</tr>
								</table>
							</td>
							<td>
								<table class="no-border" style="width: 100%;">
									<tr>
										<td class="num"><?=GetMessage("RCPT_ACCRUED_BY_PERIOD")?>:</td>
										<td class="num bordered"><?=__citrusReceiptNum($arResult["TOTALS"]["SUMM2PAY"])?></td>
									</tr>
									<tr>
										<td class="num"><?=GetMessage("RCPT_DOLG_AVANS")?>:</td>
										<td class="num bordered"><?=__citrusReceiptNum($arResult["ACCOUNT_PERIOD"]["DEBT_BEG"])?></td>
									</tr>
									<tr>
										<td class="num"><?=GetMessage("RCPT_PAYED_BY_PERIOD")?>:</td>
										<td class="num bordered"><?=__citrusReceiptNum(/*$arResult["TOTALS"]["SUMM_PAYED"] + $arResult["INSURANCE"]["SUMM_PAYED"]*/$arResult["ACCOUNT_PERIOD"]["SUM_PAYED"])?></td>
									</tr>
									<? if ($arResult["INSURANCE"]["SUMM_PAYED"] > 0 ): ?>
										<tr>
											<td class="num bold"><?=GetMessage("RCPT_SUMM_PAYED_INSURANCE")?>:</td>
											<td class="num bold bordered"><?=__citrusReceiptNum($arResult["INSURANCE"]["SUMM_PAYED"])?></td>
										</tr>
									<? endif; ?>
									<? if (floatval($arResult["ACCOUNT_PERIOD"]["CREDIT_PAYED"])): ?>
										<tr>
											<td class="num"><?=GetMessage("RCPT_CREDIT_PAYED")?>:</td>
											<td class="num bordered"><?=__citrusReceiptNum($arResult["ACCOUNT_PERIOD"]["CREDIT_PAYED"])?></td>
										</tr>
									<? endif ?>
									<?
									if (!$arResult["HAS_SEPARATE_PENALTIES_RECEIPT"])
									{
										?>
										<tr>
											<td class="num"><?=GetMessage("RCPT_PENALTIES_BY_PERIOD")?>:</td>
											<td class="num bordered"><?=__citrusReceiptNum($arResult["TOTALS"]["PENALTIES"])?></td>
										</tr>
										<?
									}
									?>
									<tr>
										<td class="num"><?=GetMessage("RCPT_LAST_PAYMENT_DATE")?>:</td>
										<td class="num bordered"><?=$arResult["ACCOUNT_PERIOD"]["LAST_PAYMENT"]?></td>
									</tr>
									<tr>
										<td class="num bold"><?=GetMessage(($arResult["IS_OVERHAUL"] || !sizeof($arResult["ACCOUNT_PERIOD"]["BARCODES_INSURANCE"])) ? "RCPT_SUMM_TO_PAY" : "RCPT_SUMM_TO_PAY_WITHOUT_INSURANCE")?>
											:
										</td>
										<td class="num bold bordered"><?=__citrusReceiptNum($arResult["ACCOUNT_PERIOD"]["SUMM_TO_PAY"])?></td>
									</tr>
									<? if (!(($arResult["IS_OVERHAUL"]) || !sizeof($arResult["ACCOUNT_PERIOD"]["BARCODES_INSURANCE"]))): ?>
										<tr>
											<td class="num bold"><?=GetMessage("RCPT_SUMM_TO_PAY_WITH_INSURANCE")?>:</td>
											<td class="num bold bordered"><?=__citrusReceiptNum($arResult["ACCOUNT_PERIOD"]["SUMM_TO_PAY"] + $arResult["INSURANCE"]["SUMM"])?></td>
										</tr>
									<? endif ?>
									<tr>
										<td class="num bold"><?=GetMessage("RCPT_PAY_BEFORE")?>:</td>
										<? $arDate = ParseDateTime($arResult["ACCOUNT_PERIOD"]["PERIOD_DATE"], "YYYY-MM-DD");
										$ts = strtotime("1 month", mktime(0, 0, 0, $arDate["MM"], $arParams["PAY_BEFORE_DAY"], $arDate["YYYY"])); ?>
										<td class="center nowrap bold bordered" style="border: none;"><?=ConvertTimeStamp($ts)?></td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<div class="page-break"></div>
		<?php
		// ���������� ������� ��� ����������� � �������
		$cols = 16;
		$cols_total = 6;
		if (isset($arHideODN['VOLUME']))
		{
			$cols--;
			$cols_total--;
		}
		if (isset($arHideODN['CHARGE']))
		{
			$cols--;
		}
		if (isset($arHideODN['TO_PAY']))
		{
			$cols--;
		}
		?>
		<table>
			<tr>
				<td colspan="2" class="small tl" style="padding-top: 7pt;">
					<table class="border services" style="width: 100%;">
						<thead>
						<tr>
							<td colspan="<?=$cols?>"><?=GetMessage("RCPT_SERVICES_TABLE_TITLE")?></td>
						</tr>
						<tr>
							<td rowspan="3"><?=GetMessage("RCPT_CONTRACTOR_NUMBER")?></td>
							<td rowspan="3"><?=GetMessage("RCPT_SERVICE_KINDS")?></td>
							<td rowspan="3"><?=GetMessage("RCPT_UNITS")?></td>
							<td rowspan="2" <?=!isset($arHideODN['VOLUME']) ? "colspan=\"2\"" : ""?>><?=GetMessage("RCPT_VOLUME")?></td>
							<td rowspan="3"><?=GetMessage("RCPT_TARIFF")?></td>
							<td rowspan="2" <?=!isset($arHideODN['CHARGE']) ? "colspan=\"2\"" : ""?>><?=GetMessage("RCPT_CHARGED")?></td>
							<td rowspan="3"><?=GetMessage("RCPT_CHARGED2")?></td>
							<td rowspan="3"><?=GetMessage("RCPT_RAISE_MULTIPLIER")?></td>
							<td rowspan="3"><?=GetMessage("RCPT_RAISE_SUM")?></td>
							<td rowspan="3"><?=GetMessage("RCPT_CORRECTION")?></td>
							<td rowspan="3"><?=GetMessage("RCPT_PRIVIL")?></td>
							<td <?=!isset($arHideODN['TO_PAY']) ? "colspan=\"3\"" : "colspan=\"2\""?>><?=GetMessage("RCPT_TO_PAY2")?></td>
						</tr>
						<tr>
							<td rowspan="2"><?=GetMessage("RCPT_TOTAL")?></td>
							<td <?=!isset($arHideODN['TO_PAY']) ? "colspan=\"2\"" : ""?>><?=GetMessage("RCPT_INCLUDING")?></td>
						</tr>
						<tr>
							<td><?=GetMessage("RCPT_PERS_CONSUMPTION_ABBR")?></td>
							<? if (!isset($arHideODN['VOLUME'])) : ?>
								<td><?=GetMessage("RCPT_COMMON_HOUSE_CONSUMPTION_ABBR")?></td>
							<? endif; ?>
							<td><?=GetMessage("RCPT_PERS_CONSUMPTION_ABBR")?></td>
							<? if (!isset($arHideODN['CHARGE'])) : ?>
								<td><?=GetMessage("RCPT_COMMON_HOUSE_CONSUMPTION_ABBR")?></td>
							<? endif; ?>
							<td><?=GetMessage("RCPT_PERS_CONSUMPTION_ABBR")?></td>
							<? if (!isset($arHideODN['TO_PAY'])) : ?>
								<td><?=GetMessage("RCPT_COMMON_HOUSE_CONSUMPTION_ABBR")?></td>
							<? endif; ?>
						</tr>
						<tr>
							<?
							for ($i = 1; $i <= $cols; $i++): ?>
								<td><?=$i?></td>
							<? endfor ?>
						</tr>
						</thead>

						<tbody>
						<? foreach ($arResult["CHARGES"] as $idx => $arCharge):
							$isComponent = $arCharge["COMPONENT"] != "N";

							if ($arResult["HAS_GROUPS"] && ($idx == 0 || $arResult["CHARGES"][$idx - 1]["GROUP"] != $arCharge["GROUP"])):?>
								<tr class="rcpt-group-title">
									<td colspan="<?=$cols?>">
										<?=$arCharge["GROUP"]?>
									</td>
								</tr>
							<? endif ?>
							<? if ($arCharge["IS_INSURANCE"] != "Y"):?>
							<tr>
								<td class="center"><?=$arCharge["RECEIPT_ORDER"]?></td>
								<td><?=$arCharge["SERVICE_NAME"]?><?=($arCharge["HAS_COMPONENTS"] && substr($arCharge['SERVICE_NAME'], -1, 1) !== ':' ? ':' : '')?></td>
								<td class="center"><?=CCitrusTszhReceiptComponentHelper::getArrayValue(array("UNITS"), $arCharge, false, $arCharge["SERVICE_UNITS"])?></td>
								<?php
								// ������� �������� �� ������������� �� 1�
								if (isset($arCharge["AMOUNT_VIEW"]) && strlen($arCharge["AMOUNT_VIEW"])) : ?>
									<td class="num"><?=$arCharge["AMOUNT_VIEW"]?></td>
								<?php else : ?>
									<td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue(array("AMOUNT"), $arCharge, true, $arCharge['AMOUNT'] - $arCharge['HAMOUNT'], 3)?></td>
								<?php endif; ?>
								<? if (!isset($arHideODN['VOLUME'])) : ?>
									<td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue("HAMOUNT", $arCharge, true, false, 3)?></td>
								<? endif; ?>
								<td class="num"><?=$arCharge["RATE"]?></td>
								<td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue(array(
										"SUMM",
										"HSUMM",
									), $arCharge, true, (($arCharge["SUM_WITHOUT_RAISE"] != 0) ? $arCharge["SUM_WITHOUT_RAISE"] : $arCharge["SUMM"]) - $arCharge["HSUMM"])?></td>
								<td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue("HSUMM", $arCharge, true)?></td>
								<? if (!isset($arHideODN['CHARGE'])) : ?>
									<td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue((($arCharge["SUM_WITHOUT_RAISE"] != 0) ? "SUM_WITHOUT_RAISE" : "SUMM"), $arCharge, true)?></td>
								<? endif; ?>
								<td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue("RAISE_MULTIPLIER", $arCharge, true)?></td>
								<td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue("RAISE_SUM", $arCharge, true)?></td>
								<td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue("CORRECTION", $arCharge, true)?></td>
								<td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue("COMPENSATION", $arCharge, true)?></td>
								<td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue((($arCharge["CSUM_WITHOUT_RAISE"] != 0) ? "CSUM_WITHOUT_RAISE" : "SUMM2PAY"), $arCharge, true)?></td>
								<td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue(array(
										"SUMM2PAY",
										"HSUMM2PAY",
									), $arCharge, true, $arCharge["SUMM2PAY"] - $arCharge["HSUMM2PAY"])?></td>
								<? if (!isset($arHideODN['TO_PAY'])) : ?>
									<td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue("HSUMM2PAY", $arCharge, true)?></td>
								<? endif; ?>
							</tr>
						<? endif; ?>
						<? endforeach ?>
						</tbody>
						<tfoot>
						<tr>
							<? if (!(($arResult["IS_OVERHAUL"]) || !sizeof($arResult["ACCOUNT_PERIOD"]["BARCODES_INSURANCE"]))): ?>
                                <td colspan="<?=$cols_total?>"><?=GetMessage("RCPT_TOTAL_CHARGED_WITHOUT_INSURANCE")?></td>
                                <td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["_SUMM"])?></td>
                                    <? if (!isset($arHideODN['CHARGE'])) : ?>
                                    <td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["HSUMM"])?></td>
                                    <? endif; ?>
                                <td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["CSUM_WITHOUT_RAISE"])?></td>
                                <td></td>
                                <td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["RAISE_SUM"])?></td>
                                <td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["CORRECTION"])?></td>
                                <td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["COMPENSATION"])?></td>
                                <td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["SUMM2PAY"])?></td>
                                <td style="border: none;">&nbsp;</td>
                                    <? if (!isset($arHideODN['VOLUME'])) : ?>
                                        <td style="border: none;">&nbsp;</td>
                                    <? endif; ?>
						        </tr>
                                <tr style="font-weight: normal !important;">
                                    <td></td>
                                    <td><?=$arResult["INSURANCE"]["SERVICE_NAME"].GetMessage("TPL_FOR").CTszhPeriod::Format(date('Y-m-d',strtotime($arResult["ACCOUNT_PERIOD"]["PERIOD_DATE"]." +2 month")));?></td>
                                    <td></td>
                                    <td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["INSURANCE"]["RATE"]);?></td>
                                    <td>X</td>
                                    <td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["INSURANCE"]["SUMM"]);?></td>
                                    <td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["INSURANCE"]["SUMM"]);?></td>
                                    <td class="num"><?=CCitrusTszhReceiptComponentHelper::num(0)?></td>
                                    <td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["INSURANCE"]["SUMM"]);?></td>
                                    <td></td>
                                    <td class="num"><?=CCitrusTszhReceiptComponentHelper::num(0)?></td>
                                    <td class="num"><?=CCitrusTszhReceiptComponentHelper::num(0)?></td>
                                    <td class="num"><?=CCitrusTszhReceiptComponentHelper::num(0)?></td>
                                    <td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["INSURANCE"]["SUMM"]);?></td>
                                </tr>
                                <tr>
                                    <td colspan="<?=$cols_total?>"><?=GetMessage("RCPT_TOTAL_CHARGED_WITH_INSURANCE")?></td>
                                    <td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["_SUMM"] + $arResult["INSURANCE"]["SUMM"])?></td>
                                    <? if (!isset($arHideODN['CHARGE'])) : ?>
                                        <td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["HSUMM"])?></td>
                                    <? endif; ?>
                                    <td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["CSUM_WITHOUT_RAISE"] + $arResult["INSURANCE"]["SUMM"])?></td>
                                    <td></td>
                                    <td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["RAISE_SUM"])?></td>
                                    <td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["CORRECTION"])?></td>
                                    <td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["COMPENSATION"])?></td>
                                    <td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["SUMM2PAY"] + $arResult["INSURANCE"]["SUMM"])?></td>
                                    <td style="border: none;">&nbsp;</td>
                                    <? if (!isset($arHideODN['VOLUME'])) : ?>
                                        <td style="border: none;">&nbsp;</td>
                                    <? endif; ?>
							<? else: ?>
								<td colspan="<?=$cols_total?>"><?=GetMessage("RCPT_TOTAL_CHARGED")?></td>
								<td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["_SUMM"])?></td>
								<? if (!isset($arHideODN['CHARGE'])) : ?>
									<td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["HSUMM"])?></td>
								<? endif; ?>
								<td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["CSUM_WITHOUT_RAISE"])?></td>
								<td></td>
								<td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["RAISE_SUM"])?></td>
								<td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["CORRECTION"])?></td>
								<td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["COMPENSATION"])?></td>
								<td class="num"><?=CCitrusTszhReceiptComponentHelper::num($arResult["TOTALS"]["SUMM2PAY"])?></td>
								<td style="border: none;">&nbsp;</td>
								<? if (!isset($arHideODN['VOLUME'])) : ?>
									<td style="border: none;">&nbsp;</td>
								<? endif; ?>
							<? endif; ?>
						</tr>
						</tfoot>
					</table>

					<div class="cut-line"></div>

					<div style="overflow: hidden;">
						<table class="border counters" style="width: 45%; float: left;">
							<thead>
							<tr>
								<td colspan="4"><?=GetMessage("RCPT_CORRECTIONS_INFO")?></td>
							</tr>
							<tr>
								<td><?=GetMessage("RCPT_CONTRACTOR_NUMBER")?></td>
								<td><?=GetMessage("RCPT_SERVICE_KINDS")?></td>
								<td><?=GetMessage("RCPT_CORRECTION_REASON")?></td>
								<td><?=GetMessage("RCPT_SUM_RUB")?></td>
							</tr>
							</thead>
							<tbody>
							<? foreach ($arResult["CORRECTIONS"] as $arCorrection): ?>
								<tr>
									<td class="center"><?=$arCorrection["RECEIPT_ORDER"]?></td>
									<td><?=$arCorrection["SERVICE"]?></td>
									<td><?=$arCorrection["GROUNDS"]?></td>
									<td class="num"><?=__citrusReceiptNum($arCorrection["SUMM"])?></td>
								</tr>
							<? endforeach ?>
							</tbody>
						</table>
						
						<?
						if (!$arResult["IS_FINES_RECEIPT"])
						{
							?>
							<table class="border counters" style="width: 52%; float: right;">
								<thead>
								<tr>
									<td colspan="7"><?=GetMessage("RCPT_INSTALLMENT_INFO")?></td>
								</tr>
								<tr>
									<td rowspan="2"><?=GetMessage("RCPT_CONTRACTOR_NUMBER")?></td>
									<td rowspan="2"><?=GetMessage("RCPT_SERVICE_KINDS")?></td>
									<td rowspan="2"><?=GetMessage("RCPT_PAYMENT_BY_PERIOD")?></td>
									<td rowspan="2"><?=GetMessage("RCPT_PAYMENT_BY_PREV_PERIOD")?></td>
									<td colspan="2"><?=GetMessage("RCPT_INSTALLMENT_PERCENTS")?></td>
									<td rowspan="2"><?=GetMessage("RCPT_ITOGO")?></td>
								</tr>
								<tr>
									<td>%</td>
									<td><?=GetMessage("RCPT_CURRENCY")?></td>
								</tr>
								</thead>
								<tbody>
								<? foreach ($arResult["INSTALLMENTS"] as $arInstallment): ?>
									<tr>
										<td class="center"><?=$arInstallment["RECEIPT_ORDER"]?></td>
										<td><?=$arInstallment["SERVICE"]?></td>
										<td class="num"><?=__citrusReceiptNum($arInstallment["SUMM_PAYED"])?></td>
										<td class="num"><?=__citrusReceiptNum($arInstallment["SUMM_PREV_PAYED"])?></td>
										<td class="num"><?=__citrusReceiptNum($arInstallment["PERCENT"])?></td>
										<td class="num"><?=__citrusReceiptNum($arInstallment["SUMM_RATED"])?></td>
										<td class="num"><?=__citrusReceiptNum($arInstallment["SUMM2PAY"])?></td>
									</tr>
								<? endforeach ?>
								</tbody>
							</table>
							<?
						}
						?>
					</div>

					<table class="border counters" style="width: 100%; margin-top: 8pt;">
						<thead>
						<tr>
							<td colspan="9"><?=GetMessage("RCPT_CONTRACTORS_INFO")?></td>
						</tr>
						<tr>
							<td rowspan="2"><?=GetMessage("RCPT_CONTRACTOR_NUMBER")?></td>
							<td rowspan="2"><?=GetMessage("RCPT_CONTRACTOR_NAME")?></td>
							<td rowspan="2"><?=GetMessage("RCPT_CONTRACTOR_CONTACTS")?></td>
							<td rowspan="2"><?=GetMessage("RCPT_CONTRACTOR_ACCOUNT")?></td>
							<td colspan="4"><?=GetMessage("RCPT_CHARGES_INFO")?></td>
							<td rowspan="2"><?=GetMessage("RCPT_TO_PAY_RUB")?></td>
						</tr>
						<tr>
							<td><?=GetMessage("RCPT_ON_PERIOD_BEGIN")?></td>
							<td><?=GetMessage("RCPT_PAID_BY_PERIOD")?></td>
							<td><?=GetMessage("RCPT_PENALTIES_PENI")?></td>
							<td><?=GetMessage("RCPT_ACCRUED_BY_PERIOD_RUB")?></td>
						</tr>
						</thead>
						<tbody>
						<? foreach ($arResult["CONTRACTORS"] as $arContractor):
							// ����������-����������� �� ��������� � ���� ����� ���������
							if ($arContractor["CONTRACTOR_EXECUTOR"] != "N")
							{
								continue;
							}
							$arContacts = array();
							if (strlen(trim($arContractor["CONTRACTOR_ADDRESS"])))
							{
								$arContacts[] = trim($arContractor["CONTRACTOR_ADDRESS"]);
							}
							if (strlen(trim($arContractor["CONTRACTOR_PHONE"])))
							{
								$arContacts[] = trim($arContractor["CONTRACTOR_PHONE"]);
							} ?>
							<tr>
								<td class="center"><?=$arContractor["RECEIPT_ORDER"]?></td>
								<td><?=$arContractor["CONTRACTOR_NAME"]?></td>
								<td><?=count($arContacts) ? implode("; ", $arContacts) : ''?></td>
								<td>&nbsp;</td>
								<td class="num"><?=__citrusReceiptNum($arContractor["DEBT_BEG"])?></td>
								<td class="num"><?=__citrusReceiptNum($arContractor["SUMM_PAYED"])?></td>
								<td class="num"><?=__citrusReceiptNum($arContractor["PENALTIES"])?></td>
								<td class="num"><?=__citrusReceiptNum($arContractor["SUMM_CHARGED"])?></td>
								<td class="num"><?=__citrusReceiptNum($arContractor["SUMM"])?></td>
							</tr>
						<? endforeach ?>
						</tbody>
					</table>
					<?
					if (!$arResult["IS_FINES_RECEIPT"])
					{
						?>
						<table class="border counters" style="width: 100%; margin-top: 8pt;">
							<thead>
							<tr>
								<td colspan="8"><?=GetMessage("RCPT_REF_INFO")?></td>
							</tr>
							<tr>
								<td rowspan="3"><?=GetMessage("RCPT_CONTRACTOR_NUMBER")?></td>
								<td rowspan="3"><?=GetMessage("RCPT_SERVICE")?></td>
								<td rowspan="3"><?=GetMessage("RCPT_UNITS")?></td>
								<td rowspan="3"><?=GetMessage("RCPT_METER_CUR_VALUES")?></td>
								<td colspan="4"><?=GetMessage("RCPT_CONSUMPTION")?></td>
							</tr>
							<tr>
								<td colspan="<?=isset($arHideODN['COMMON_HOUSE']) ? "4" : "3"?>"><?=GetMessage("RCPT_IN_HOUSE_ROOMS")?></td>
								<? if (!isset($arHideODN['COMMON_HOUSE'])) : ?>
									<td rowspan="2"><?=GetMessage("RCPT_COMMON_HOUSE_CONSUMPTION")?></td>
								<? endif; ?>
							</tr>
							<tr>
								<td><?=GetMessage("RCPT_PO_IPU")?></td>
								<td><?=GetMessage("RCPT_BY_NORM")?></td>
								<td <?=isset($arHideODN['COMMON_HOUSE']) ? "colspan=\"2\"" : ""?>><?=GetMessage("RCPT_DIFFERENCE")?></td>
							</tr>
							</thead>
							<tbody>
							<? foreach ($arResult["CHARGES"] as $arCharge)
							{
								$arMeterValues = array();

								if ($arResult["HAS_CHARGES_METERS_BINDING"])
								{
									foreach ($arCharge["METER_IDS"] as $meterID)
									{
										$value = __citrusGetMeterValues($arResult["METERS"][$meterID]);
										if (!empty($value))
										{
											$arMeterValues[] = implode(" / ", $value);
										}
									}
								}
								else
								{
									foreach ($arResult["METERS"] as $arMeter)
									{
										if (trim($arMeter["~SERVICE_NAME"]) == trim($arCharge["~SERVICE_NAME"]))
										{
											$value = __citrusGetMeterValues($arMeter);
											if (!empty($value))
											{
												$arMeterValues[] = implode(" / ", $value);
											}
											break;
										}
									}
								}

								foreach ($arCharge["HMETER_IDS"] as $meterID)
								{
									$value = __citrusGetMeterValues($arResult["HMETERS"][$meterID]);
									if (!empty($value))
									{
										$arMeterValues[] = implode(" / ", $value);
									}
								}

								/*
								*	��� ��������� �� �� � ������� ��������� � ������� ����������� �� ���� 
								*	����� �������� �� ������ ������ �� ����������, �� � ������, � ������� 
								*	���� �� ���� �� ��������� volume ��� amount_norm �� ����� 0.
								*/
								if ((!empty($arMeterValues)) || (CCitrusTszhReceiptComponentHelper::getArrayValue("AMOUNT_NORM", $arCharge, true, false, 3) > 0) || (CCitrusTszhReceiptComponentHelper::getArrayValue("VOLUMEP", $arCharge, true, false, 3) > 0)):?>
									<tr>
										<td class="center"><?=$arCharge["RECEIPT_ORDER"]?></td>
										<td class="nowrap"><?=$arCharge["SERVICE_NAME"]?></td>
										<td class="center"><?=$arCharge["SERVICE_UNITS"]?></td>
										<td class="num"><?=implode("<br>", $arMeterValues)?></td>
										<td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue(array(
												"VOLUMEP",
												"AMOUNT_NORM",
											), $arCharge, true, $arCharge["VOLUMEP"] - $arCharge["AMOUNT_NORM"], 3)//������� "�� ���" ������ ���� ����� ��� volumep - amount_norm?></td>
										<td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue("AMOUNT_NORM", $arCharge, true, false, 3)?></td>
										<td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue(array(
												"VOLUMEP",
												"VOLUMEA",
											), $arCharge, true, $arCharge["VOLUMEP"] - $arCharge["VOLUMEA"], 3)?></td>
										<? if (!isset($arHideODN['COMMON_HOUSE'])) : ?>
											<td class="num"><?=CCitrusTszhReceiptComponentHelper::getArrayValue("VOLUMEH", $arCharge, true, false, 3)?></td>
										<? endif; ?>
									</tr>
								<?endif;
							} ?>
							</tbody>
						</table>
						<?
					}
					$host = \CBXPunycode::ToUnicode($_SERVER["SERVER_NAME"], $errors);
					?>
					<div style="font-style: italic;">
							<?//=str_replace("#URL#", ($APPLICATION->IsHttps() ? "https" : "http") . "://" . $host . "/", $arParams["NOTE_TEXT"])?>
						<?=GetMessage("RCPT_NOTE_TEXT") . ":"?>
						<?= $arResult["IS_OVERHAUL"] ? $arResult["TSZH"]["ANNOTATION_OVERHAUL"] : $arResult["TSZH"]["ANNOTATION_MAIN"]?>
					</div>
				</td>
			</tr>
		</table>
		<?

		$summ2pay = $arResult["ACCOUNT_PERIOD"]["DEBT_END"];
		if (COption::GetOptionString("citrus.tszh", "pay_to_executors_only", "N") == "Y")
		{
			if (CModule::IncludeModule("vdgb.portaljkh") && method_exists("CCitrusPortalTszh", "setPaymentBase"))
			{
				CCitrusPortalTszh::setPaymentBase($arResult["TSZH"]);
			}
			$summ2pay = CTszhAccountContractor::GetList(array(), array(
				"ACCOUNT_PERIOD_ID" => $arResult["ACCOUNT_PERIOD"]["ID"],
				"!CONTRACTOR_EXECUTOR" => "N",
			), array("SUMM"))->Fetch();
			$summ2pay = is_array($summ2pay) ? $summ2pay['SUMM'] - $arResult["ACCOUNT_PERIOD"]["PREPAYMENT"] : 0;
		}
		if ($summ2pay > 0 && CModule::IncludeModule("citrus.tszhpayment") && ($paymentPath = CTszhPaySystem::getPaymentPath($arResult["TSZH"]["SITE_ID"])))
		{
			echo '<div class="no-print">' . GetMessage("CITRUS_TSZHPAYMENT_LINK", Array("#LINK#" => $paymentPath . '?ptype=' . $arResult['RECEIPT_PAYMENT_TYPE'])) . '</div>';
		}

		?>
	</div>
<? if ($_GET["print"] == "Y"):
	if ($arResult["IS_LAST"]):
		if ($arResult["MODE"] == "AGENT"):?>
			</div>
			<?
		else:?>
			</body>
			</html>
			<?
			exit();
		endif;
	endif;

	if (in_array($arResult["MODE"], array("ADMIN", "AGENT")))
	{
		return;
	}
	else
	{
		require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
		die();
	}
endif;