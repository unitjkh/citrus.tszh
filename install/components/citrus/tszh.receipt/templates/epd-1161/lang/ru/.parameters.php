<?
$MESS["P_METER_VALUES_START_DATE"] = "Начало периода (число месяца) снятия показаний счётчиков";
$MESS["P_METER_VALUES_END_DATE"] = "Окончание периода (число месяца) снятия показаний счётчиков";
$MESS["P_PAY_BEFORE_DAY"] = "До какого числа следующего месяца оплатить";
$MESS["P_NOTE_TEXT"] = "Текст примечания";
$MESS["HIDE_ODN"] = "Скрыть колонки с ОДН";
$MESS["HIDE_ODN_VOLUME"] = "Объем";
$MESS["HIDE_ODN_CHARGE"] = "Размер платы";
$MESS["HIDE_ODN_TO_PAY"] = "Итого к оплате";
$MESS["HIDE_ODN_COMMON_HOUSE"] = "Суммарный объем";
$MESS["P_NOTE_TEXT_DEF_VALUE"] = "Примечание: Распечатать квитанцию Вы можете также с сайта #URL#.";
?>