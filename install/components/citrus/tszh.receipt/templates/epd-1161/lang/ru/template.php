<?
$MESS["RCPT_YOU_CAN"] = "Вы можете";
$MESS["RCPT_OPEN_RECEIPT_IN_WINDOW"] = "открыть квитанцию в отдельном окне";
$MESS["RCPT_FOR_PRINTING"] = "для печати.";
$MESS["RCPT_TITLE"] = "Квитанция";
$MESS["RCPT_NOTICE"] = "ИЗВЕЩЕНИЕ";
$MESS["RCPT_FOR"] = "за";
$MESS["RCPT_YEAR_ABBR"] = "г.";
$MESS["RCPT_ACCOUNT_ABBR"] = "л/с";
$MESS["RCPT_IN"] = "в";
$MESS["RCPT_INN"] = "ИНН";
$MESS["RCPT_KPP"] = "КПП";
$MESS["RCPT_RSCH"] = "р/с";
$MESS["RCPT_KSCH"] = "к/с";
$MESS["RCPT_BIK"] = "БИК";
$MESS["RCPT_SUMM_TO_PAY"] = "Итого к оплате";
$MESS["RCPT_SUMM_TO_PAY_WITHOUT_INSURANCE"] = "Итого к оплате без учёта добровольного страхования";
$MESS["RCPT_SUMM_TO_PAY_WITH_INSURANCE"] = "Итого к оплате с учетом добровольного страхования";
$MESS["RCPT_CREDIT_END"] = "Остаток на конец месяца:";
$MESS["RCPT_DEBT_END"] = "Задолженность на конец месяца:";
$MESS["RCPT_METER_VALUES_ON_DATE"] = "Показания счетчиков на дату";
$MESS["RCPT_CHECK"] = "КВИТАНЦИЯ";
$MESS["RCPT_PEOPLE_LIVING"] = "Проживает: #PEOPLE# чел.;";
$MESS["RCPT_AREA"] = "Общ. площадь: #AREA#;";
$MESS["RCPT_LIVING_AREA"] = "Жил. площадь: #AREA#.";
$MESS["RCPT_METER_VALUES_ON"] = "Показания счетчиков на #DATE#";
$MESS["RCPT_CURRENCY"] = "руб.";
$MESS["RCPT_CREDIT_BEG"] = "Задолженность на начало месяца";
$MESS["RCPT_DEBT_BEG"] = "Остаток на начало месяца";
$MESS["RCPT_NORM"] = "Норма";
$MESS["RCPT_TARIFF"] = "Тариф";
$MESS["RCPT_QUANTITY"] = "Кол-во";
$MESS["RCPT_PENI"] = "Пени";
$MESS["RCPT_TO_PAY"] = "Сумма к оплате";
$MESS["RCPT_TO_PAY_WITHOUT_INSURANCE"] = "Сумма к оплате без учета добр. страхования:";
$MESS["RCPT_TO_PAY_WITH_INSURANCE"] = "Сумма к оплате с учетом добров. страхования:";
$MESS["RCPT_UNITS"] = "Ед.";
$MESS["RCPT_TOTAL_PAYED"] = "Итого оплачено:";
$MESS["CITRUS_TSZHPAYMENT_LINK"] = "<p>Вы можете <a href=\"#LINK#\">оплатить услуги</a> онлайн.</p>";

$MESS["RCPT_MAIN_HEADER"] = "Форма Единого платежного документа для внесения платы за содержание<br />и ремонт жилого помещения и предоставление коммунальных услуг";
$MESS["RCPT_HEADER"] = "БЛАНК ДЛЯ ПЕРЕДАЧИ ПОКАЗАНИЙ ПРИБОРОВ УЧЕТА";
$MESS["RCPT_NUM_SIGN"] = "№";
$MESS["RCPT_HEADER_NOTE"] = "Показания приборов учета снимаются в период с #START_DATE# по #END_DATE# число расчетного месяца и вписываются в таблицу ниже.<br />Заполненный отрывной бланк опустите в ящик управляющей организации.";

$MESS["RCPT_METER_SERVICE_NAME"] = "Наименование";
$MESS["RCPT_METER_NUMBER"] = "Номер прибора";
$MESS["RCPT_METER_LAST_VALUES"] = "Последние показания";
$MESS["RCPT_METER_CUR_VALUES"] = "Текущие показания";
$MESS["RCPT_METER_DATE"] = "Дата снятия показаний";
$MESS["RCPT_DATA_ACCEPT"] = "Данные подтверждаю";
$MESS["RCPT_CUT_LINE"] = "Линия отреза";

$MESS["RCPT_RECIPIENT"] = "Получатель<br />платежа";
$MESS["RCPT_REQUISITES"] = "Реквизиты";

$MESS["RCPT_CALC_PERIOD"] = "Расчетный период";
$MESS["RCPT_ADDRESS"] = "Адрес";

$MESS["RCPT_CONTACT_CENTER"] = "Контактный<br />центр";
$MESS["RCPT_PAY_SUM"] = "Сумма оплаты";
$MESS["RCPT_ACCOUNT_NUMBER"] = "Лицевой счет";
$MESS["RCPT_FIO"] = "Ф.И.О.";

$MESS["RCPT_SIGN"] = "Подпись";
$MESS["RCPT_DATE"] = "Дата";

$MESS["RCPT_PAYER"] = "Плательщик";
$MESS["TPL_FOR"] = " за ";

$MESS["RCPT_FLAT_TYPE"] = "Тип квартиры";
$MESS["RCPT_COMMON_LIVING_AREA"] = "Общая / жилая площадь";
$MESS["RCPT_REGISTERED_PEOPLE_PEOPLE"] = "Зарегистрировано / Проживает";
$MESS["RCPT_EXEMPT_PEOPLE"] = "Льготников";
$MESS["RCPT_HOUSE_AREA"] = "Общая площадь здания";
$MESS["RCPT_HOUSE_ROOMS_AREA"] = "Площадь помещений (жилых и нежилых)";
$MESS["RCPT_HOUSE_MOP_AREA"] = "Площадь мест общего пользования";

$MESS["RCPT_ACCRUED_BY_PERIOD"] = "Начислено за период";
$MESS["RCPT_DOLG_AVANS"] = "Долг/аванс на начало периода (+/-)";
$MESS["RCPT_PAYED_BY_PERIOD"] = "Поступило за период";
$MESS["RCPT_SUMM_PAYED_INSURANCE"] = "В т.ч. добровольное страхование";
$MESS["RCPT_CREDIT_PAYED"] = "в т.ч. предоплата рассрочки";
$MESS["RCPT_PENALTIES_BY_PERIOD"] = "Начислено пени";
$MESS["RCPT_LAST_PAYMENT_DATE"] = "Дата последней оплаты";
$MESS["RCPT_PAY_BEFORE"] = "Оплатить счет до";

$MESS["RCPT_SERVICES_TABLE_TITLE"] = "РАСШИФРОВКА СЧЕТА ДЛЯ ВНЕСЕНИЯ ПЛАТЫ ПО ВИДАМ ОКАЗАННЫХ УСЛУГ";
$MESS["RCPT_PERS_CONSUMPTION_ABBR"] = "индив.<br />потреб.";
$MESS["RCPT_COMMON_HOUSE_CONSUMPTION_ABBR"] = "обще-<br />дом.<br />нужды";
$MESS["RCPT_TOTAL"] = "Всего";
$MESS["RCPT_INCLUDING"] = "в том числе";

$MESS["RCPT_CONTRACTOR_NUMBER"] = "№<br />постав<br />щика";
$MESS["RCPT_SERVICE_KINDS"] = "Виды услуг";
$MESS["RCPT_VOLUME"] = "Объем";
$MESS["RCPT_CHARGED"] = "Размер платы за<br />оказ. услуги";
$MESS["RCPT_CHARGED2"] = "Всего<br />начисл.";
$MESS["RCPT_RAISE_MULTIPLIER"] = "Повыш. коэффи-<br/>циент";
$MESS["RCPT_RAISE_SUM"] = "Сумма<br/> повыше-<br/>ния";
$MESS["RCPT_CORRECTION"] = "Пере-<br />рас-<br />четы";
$MESS["RCPT_PRIVIL"] = "Льго-<br />ты,<br />суб-<br />сидии";
$MESS["RCPT_TO_PAY2"] = "Итого к оплате за<br />расч. период руб.";

$MESS["RCPT_OTHER_SERVICES"] = "КОММУНАЛЬНЫЕ УСЛУГИ";
$MESS["RCPT_TOTAL_CHARGED"] = "Итого к оплате за расчетный период";
$MESS["RCPT_TOTAL_CHARGED_WITHOUT_INSURANCE"] = "Итого к оплате за расчетный период без учета добровольного страхования";
$MESS["RCPT_TOTAL_CHARGED_WITH_INSURANCE"] = "Итого к оплате за расчетный период с учетом добровольного страхования";

$MESS["RCPT_CONTRACTORS_INFO"] = "Сведения о поставщиках";
$MESS["RCPT_CONTRACTOR_NAME"] = "Наименование поставщика";
$MESS["RCPT_CONTRACTOR_CONTACTS"] = "Адрес и телефон контактного центра";
$MESS["RCPT_CONTRACTOR_ACCOUNT"] = "Лицевой счет у поставщика";
$MESS["RCPT_CHARGES_INFO"] = "Сведения по начислениям";
$MESS["RCPT_ON_PERIOD_BEGIN"] = "На начало периода";
$MESS["RCPT_PAID_BY_PERIOD"] = "Оплачено за период";
$MESS["RCPT_PENALTIES_PENI"] = "Штрафы, пени, руб.";
$MESS["RCPT_ACCRUED_BY_PERIOD_RUB"] = "Начислено за период, руб.";
$MESS["RCPT_TO_PAY_RUB"] = "Итого к оплате, руб.";

$MESS["RCPT_CORRECTIONS_INFO"] = "Сведения о перерасчетах";
$MESS["RCPT_CORRECTION_REASON"] = "Основание перерасчета";
$MESS["RCPT_SUM_RUB"] = "Сумма, руб.";

$MESS["RCPT_INSTALLMENT_INFO"] = "Расчет суммы оплаты с учетом рассрочки платежа";
$MESS["RCPT_PAYMENT_BY_PERIOD"] = "Оплата за расч. период";
$MESS["RCPT_PAYMENT_BY_PREV_PERIOD"] = "Оплата за пред. период";
$MESS["RCPT_INSTALLMENT_PERCENTS"] = "Проценты за рассрочку";
$MESS["RCPT_ITOGO"] = "Итого";

$MESS["RCPT_REF_INFO"] = "Справочная информация";
$MESS["RCPT_SERVICE"] = "Вид коммунальной услуги";
$MESS["RCPT_CONSUMPTION"] = "Суммарный объем коммунальных услуг";
$MESS["RCPT_IN_HOUSE_ROOMS"] = "в помещениях дома";
$MESS["RCPT_COMMON_HOUSE_CONSUMPTION"] = "на общедомовые нужды";
$MESS["RCPT_PO_IPU"] = "по ИПУ";
$MESS["RCPT_BY_NORM"] = "по нормативу";
$MESS["RCPT_DIFFERENCE"] = "расхождение";

$MESS["RCPT_NOTE_TEXT"]="Примечание";