<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arTemplateParameters = array(
	"METER_VALUES_START_DATE" => array(
		"NAME" => GetMessage("P_METER_VALUES_START_DATE"),
		"TYPE" => "INT",
		"DEFAULT" => 15,
	),	
	"METER_VALUES_END_DATE" => array(
		"NAME" => GetMessage("P_METER_VALUES_END_DATE"),
		"TYPE" => "INT",
		"DEFAULT" => 25,
	),	
	"PAY_BEFORE_DAY" => array(
		"NAME" => GetMessage("P_PAY_BEFORE_DAY"),
		"TYPE" => "INT",
		"DEFAULT" => 20,
	),	
	"NOTE_TEXT" => array(
		"NAME" => GetMessage("P_NOTE_TEXT"),
		"TYPE" => "STRING",
		"DEFAULT" => GetMessage("P_NOTE_TEXT_DEF_VALUE"),
		"COLS" => "50",
		"ROWS" => "10",
	),	
);
?>
