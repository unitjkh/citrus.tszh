<?
$MESS["RCPT_YOU_CAN"] = "Вы можете";
$MESS["RCPT_OPEN_RECEIPT_IN_WINDOW"] = "открыть квитанцию в отдельном окне";
$MESS["RCPT_FOR_PRINTING"] = "для печати.";
$MESS["RCPT_TITLE"] = "Квитанция";
$MESS["RCPT_NOTICE"] = "ИЗВЕЩЕНИЕ";
$MESS["RCPT_FOR"] = "за";
$MESS["RCPT_YEAR_ABBR"] = "г.";
$MESS["RCPT_ACCOUNT_ABBR"] = "л/с";
$MESS["RCPT_IN"] = "в";
$MESS["RCPT_INN"] = "ИНН";
$MESS["RCPT_KPP"] = "КПП";
$MESS["RCPT_RSCH"] = "р/с";
$MESS["RCPT_KSCH"] = "к/с";
$MESS["RCPT_BIK"] = "БИК";
$MESS["RCPT_SUMM_TO_PAY"] = "Итого к оплате";
$MESS["RCPT_CREDIT_END"] = "Остаток на конец месяца:";
$MESS["RCPT_DEBT_END"] = "Задолженность на конец месяца:";
$MESS["RCPT_METER_VALUES_ON_DATE"] = "Показания счетчиков на дату";
$MESS["RCPT_CHECK"] = "КВИТАНЦИЯ";
$MESS["RCPT_PEOPLE_LIVING"] = "Проживает: #PEOPLE# чел.;";
$MESS["RCPT_AREA"] = "Общ. площадь: #AREA#;";
$MESS["RCPT_LIVING_AREA"] = "Жил. площадь: #AREA#.";
$MESS["RCPT_METER_VALUES_ON"] = "Показания счетчиков на #DATE#";
$MESS["RCPT_CURRENCY"] = "руб.";
$MESS["RCPT_CREDIT_BEG"] = "Задолженность на начало месяца";
$MESS["RCPT_DEBT_BEG"] = "Остаток на начало месяца";
$MESS["RCPT_NORM"] = "Норма";
$MESS["RCPT_TARIFF"] = "Тариф";
$MESS["RCPT_RAISE_MULTIPLIER"] = "Повыш. коэффи-<br/>циент";
$MESS["RCPT_RAISE_SUM"] = "Сумма<br/> повыше-<br/>ния";
$MESS["RCPT_QUANTITY"] = "Кол-во";
$MESS["RCPT_PENI"] = "Пени";
$MESS["RCPT_TO_PAY"] = "Сумма к оплате";
$MESS["RCPT_UNITS"] = "Ед. изм.";
$MESS["RCPT_TOTAL_PAYED"] = "Итого оплачено:";
$MESS["CITRUS_TSZHPAYMENT_LINK"] = "<p>Вы можете <a href=\"#LINK#\">оплатить коммунальные услуги</a> онлайн.</p>";

$MESS["RCPT_MAIN_HEADER"] = "Форма Единого платежного документа для внесения платы за жилое помещение,<br />предоставление коммунальных и иных услуг";
$MESS["RCPT_HEADER"] = "БЛАНК ДЛЯ ПЕРЕДАЧИ ПОКАЗАНИЙ ПРИБОРОВ УЧЕТА";
$MESS["RCPT_NUM_SIGN"] = "№";
$MESS["RCPT_HEADER_NOTE"] = "Показания приборов учета снимаются абонентом и вписываются в таблицу ниже<br />Заполненный отрывной бланк опускают в ящик для приема показаний в пунктах очного обслуживания управляющей организации/ЕИРЦ";

$MESS["RCPT_METER_SERVICE_NAME"] = "Тип";
$MESS["RCPT_METER_NUMBER"] = "Номер прибора учета";
$MESS["RCPT_METER_LAST_VALUES"] = "Предыдущие показания";
$MESS["RCPT_METER_CUR_VALUES"] = "Текущие показания";
$MESS["RCPT_METER_DATE"] = "Дата передачи<br/> предыдущих показаний";
$MESS["RCPT_METER_CHECK_DATE"] = "Дата поверки<br/> прибора учёта";
$MESS["RCPT_CHECK_DATE"] = "Дата снятия показаний";
$MESS["RCPT_DATA_ACCEPT"] = "Данные подтверждаю";
$MESS["RCPT_CUT_LINE"] = "Линия отреза";

$MESS["RCPT_JKH_SERVICES"] = "ЖИЛИЩНО-КОМУНАЛЬНЫЕ И ИНЫЕ УСЛУГИ #MONTH_YEAR#г.";
$MESS["RCPT_RECIPIENT"] = "Получатель платежа";
$MESS["RCPT_REQUISITES"] = "Реквизиты";

$MESS["RCPT_CALC_PERIOD"] = "Расчетный период";
$MESS["RCPT_ADDRESS"] = "Адрес";
$MESS["RCPT_ORG_ADDRESS"] = "Юридический адрес";
$MESS["RCPT_ACCOUNT_INFO"] = "Информация по Вашему лицевому счету";
$MESS["RCPT_ACCOUNT_INFO_ADD"] = "<b>Тип Квартиры</b> #FLAT_TYPE#. <b>Общая площадь</b> #AREA# <b>кв.м, зарегистрировано </b> #REGISTERED_PEOPLE# <b>чел.,<br/>Проживает #PEOPLE# <b>чел. Общая площадь дома</b> #HOUSE_AREA# <b>кв.м. Жилые помещения</b> #HOUSE_ROOMS_AREA# <b>кв.м. Места общего пользования</b> #HOUSE_COMMON_PLACES_AREA# <b>кв.м.</b>";

$MESS["RCPT_CONTACT_CENTER"] = "Контактный<br />центр";
$MESS["RCPT_PAY_SUM"] = "Сумма оплаты";
$MESS["RCPT_ACCOUNT_NUMBER"] = "Лицевой счет";
$MESS["RCPT_FIO"] = "Ф.И.О.";

$MESS["RCPT_SIGN"] = "Подпись";
$MESS["RCPT_DATE"] = "Дата";
$MESS["RCPT_SUMM_TOTAL_TO_PAY"] = "Итого к оплате за все услуги счета";
$MESS["RCPT_SUMM_TOTAL_TO_PAY_WITHOUT_INSURANCE"] = "Итого к оплате за все услуги счета </br> без учета добровольного страхования";
$MESS["RCPT_SUMM_TOTAL_TO_PAY_WITH_INSURANCE"] = "Итого к оплате за все услуги счета </br> c учетом добровольного страхования";


$MESS["RCPT_PAYER"] = "Плательщик";

$MESS["RCPT_FLAT_TYPE"] = "Тип квартиры";
$MESS["RCPT_COMMON_LIVING_AREA"] = "Общая / жилая площадь";
$MESS["RCPT_REGISTERED_PEOPLE_PEOPLE"] = "Зарегистрировано / Проживает";
$MESS["RCPT_EXEMPT_PEOPLE"] = "Льготников";
$MESS["RCPT_HOUSE_AREA"] = "Общая площадь здания";
$MESS["RCPT_HOUSE_ROOMS_AREA"] = "Площадь помещений (жилых и нежилых)";
$MESS["RCPT_HOUSE_MOP_AREA"] = "Площадь мест общего пользования";

$MESS["RCPT_ACCRUED_BY_PERIOD"] = "Начислено за период";
$MESS["RCPT_DOLG_AVANS"] = "Долг/аванс на начало периода (+/-)";
$MESS["RCPT_PAYED_BY_PERIOD"] = "Поступило за период";
$MESS["RCPT_CREDIT_PAYED"] = "в т.ч. предоплата рассрочки";
$MESS["RCPT_PENALTIES_BY_PERIOD"] = "Начислено пени";
$MESS["RCPT_LAST_PAYMENT_DATE"] = "Последняя оплата";
$MESS["RCPT_PAY_BEFORE"] = "Просим оплатить до";

$MESS["RCPT_SERVICES_TABLE_TITLE"] = "РАСШИФРОВКА СЧЕТА ДЛЯ ВНЕСЕНИЯ ПЛАТЫ ПО ВИДАМ ОКАЗАННЫХ УСЛУГ";
$MESS["RCPT_SERVICES_TABLE_TITLE2"] = "РАСЧЁТ РАЗМЕРА ПЛАТЫ ЗА ЖИЛОЕ ПОМЕЩЕНИЕ КОММУНАЛЬНЫЕ И ИНЫЕ УСЛУГИ <br /> ЗА #MONTH_YEAR# ГОДА ";
$MESS["RCPT_PERS_CONSUMPTION_ABBR"] = "индив.<br />потреб.";
$MESS["RCPT_COMMON_HOUSE_CONSUMPTION_ABBR"] = "обще-<br />дом.<br />нужды";
$MESS["RCPT_TOTAL"] = "Всего";
$MESS["RCPT_INCLUDING"] = "в том числе";

$MESS["RCPT_CONTRACTOR_NUMBER"] = "№<br />постав<br />щика";
$MESS["RCPT_DEBT_BEG_OR_OVERPAYED"] = "Задолжен-/<br />ность/<br />Переплата<br />(-)";
$MESS["RCPT_SERVICE_KINDS"] = "Виды услуг";
$MESS["RCPT_IND_ODN"] = "инд./ОДН";
$MESS["RCPT_IND"] = "инд.";
$MESS["RCPT_ODN"] = "ОДН";
$MESS["RCPT_SERVICES_VOLUME"] = "Объемы<br />услуг";

$MESS["RCPT_VOLUME"] = "Объем";
$MESS["RCPT_CHARGED"] = "Размер платы за<br />оказ. услуги";
$MESS["RCPT_CHARGED2"] = "Начисл. <br /> по тарифу";
$MESS["RCPT_CORRECTION"] = "Пере-<br />расчеты";
$MESS["RCPT_PAYED_CUR_MONTH"] = "Оплачено<br />за #MONTH#";
$MESS["RCPT_PRIVIL"] = "Льго-<br />ты,<br />суб-<br />сидии";
$MESS["RCPT_TO_PAY2"] = "Итого к оплате за<br />расч. период руб.";

$MESS["RCPT_OTHER_SERVICES"] = "НАЧИСЛЕНИЯ ЗА КОММУНАЛЬНЫЕ УСЛУГИ";
$MESS["RCPT_TOTAL_CHARGED"] = "Итого к оплате за расчетный период";
$MESS["RCPT_TOTAL_CHARGED_WITH_INSURANCE"] = "Итого к оплате за расчетный период с учётом добровольного страхования";
$MESS["RCPT_TOTAL_CHARGED_WITHOUT_INSURANCE"] = "Итого к оплате за расчетный период без учёта добровольного страхования";

$MESS["RCPT_CONTRACTORS_INFO"] = "Сведения о поставщиках";
$MESS["RCPT_CONTRACTOR_NAME"] = "Наименование поставщика";
$MESS["RCPT_CONTRACTOR_CONTACTS"] = "Адрес и телефон контактного центра";
$MESS["RCPT_CONTRACTOR_ACCOUNT"] = "Лицевой счет у поставщика";
$MESS["RCPT_CHARGES_INFO"] = "Сведения по начислениям";
$MESS["RCPT_ON_PERIOD_BEGIN"] = "На начало периода";
$MESS["RCPT_PAID_BY_PERIOD"] = "Оплачено за период";
$MESS["RCPT_PENALTIES_PENI"] = "Штрафы, пени, руб.";
$MESS["RCPT_ACCRUED_BY_PERIOD_RUB"] = "Начислено за период, руб.";
$MESS["RCPT_TO_PAY_RUB"] = "Итого к оплате, руб.";

$MESS["RCPT_CORRECTIONS_INFO"] = "ИНФОРМАЦИЯ О ПЕРЕРАСЧЕТАХ";
$MESS["RCPT_CORRECTION_REASON"] = "Основание <br /> перерасчёта";
$MESS["RCPT_SUM_RUB"] = "Сумма, руб.";
$MESS["TPL_FOR"] = " за ";

$MESS["RCPT_INSTALLMENT_INFO"] = "РАСЧЕТ СУММЫ ОПЛАТЫ С УЧЕТОМ РАССРОЧКИ ПЛАТЕЖА";
$MESS["RCPT_PAYMENT"] = "Сумма платы с учетом рассрочки";
$MESS["RCPT_PAYMENT_BY_PERIOD"] = "Оплата за <br />расч. период";
$MESS["RCPT_PAYMENT_BY_PREV_PERIOD"] = "Оплата за <br />пред. период";
$MESS["RCPT_INSTALLMENT_PERCENTS"] = "Проценты за рассрочку";
$MESS["RCPT_ITOGO"] = "Итого к оплате <br />с учетом процентов";
$MESS["RCPT_INSTALLMENTS_ITOGO"] = "Итого к оплате с учетом рассрочки платежа";

$MESS["RCPT_REF_INFO"] = "СПРАВОЧНАЯ ИНФОРМАЦИЯ";
$MESS["RCPT_IPU_ODPU"] = "ИПУ/<br/>ОДПУ";
$MESS["RCPT_SERVICE"] = "Вид коммунальной услуги";
$MESS["RCPT_CONSUMPTION"] = "Суммарный объем коммунальных услуг";

$MESS["RCPT_IN_HOUSE_ROOMS"] = "в поме-<br/>щениях дома";
$MESS["RCPT_PU_VALUES"] = "Показания приборов учета</br> коммунальных услуг";
$MESS["RCPT_COMMON_HOUSE_CONSUMPTION"] = "на общедомовые нужды";
$MESS["RCPT_VOLUME_CONSUMPTION"] = "Объем<br /> потребле-<br />ния";
$MESS["RCPT_PO_IPU"] = "по ИПУ";
$MESS["RCPT_BY_NORM"] = "Норматив потребления коммунальных услуг";
$MESS["RCPT_DIFFERENCE"] = "расхождение";
$MESS["RCPT_PO_ODN"] = "на ОДН";
$MESS["RCPT_NOTE_TEXT"] = "Примечание";