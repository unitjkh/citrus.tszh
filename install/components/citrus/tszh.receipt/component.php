<?
/**
 * ������ ���ƻ
 * ��������� ��������� �� ������ ����� (citrus.tszh.receipt)
 * @package tszh
 */
use Citrus\Tszh\Types\ComponentType;
use Citrus\Tszh\Types\ReceiptType;

/** @var CBitrixComponent $this */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (!CModule::IncludeModule("citrus.tszh"))
{
	ShowError(GetMessage("TSZH_MODULE_NOT_FOUND_ERROR"));
	return;
}

if (!CTszhFunctionalityController::CheckEdition())
	return;

$arParams["DISABLE_TIME_ZONE"] = $arParams["DISABLE_TIME_ZONE"] == "Y" ? "Y" : "N";

$arParams["INLINE"] = $arParams["INLINE"] == "Y" ? "Y" : "N";

$arParams["CUSTOM_TEMPLATE_PATH"] = trim($arParams["CUSTOM_TEMPLATE_PATH"]);

// "USER" - ��������� � ������ ��������; "ADMIN" - �������� ������ �� �������; "AGENT" - �������� ��������� ������� ��� ������ �������� �� �������
$mode = "USER";
if (isset($arParams["CUSTOM_TEMPLATE_PATH"]) && strlen($arParams["CUSTOM_TEMPLATE_PATH"]) > 0
	&& isset($arParams["ACCOUNT_PERIOD_FILTER"]) && is_array($arParams["ACCOUNT_PERIOD_FILTER"]))
{
	$mode = "AGENT";
	$agentHtml = '';
	if (isset($arParams["ACCOUNT_PERIOD_SORT"]) && is_array($arParams["ACCOUNT_PERIOD_SORT"]))
		$mode = "ADMIN";
}

global $USER;

switch ($mode)
{
	case "ADMIN":
		// ������� ����� ������� �������� ������������ �� ������
		$POST_RIGHT = $APPLICATION->GetGroupRight("citrus.tszh");

		// ���� ��� ���� - ��������� �� ������
		if ($POST_RIGHT < "R")
		{
			$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
			return;
		}

		if (!tszhCheckMinEdition("standard"))
		{
			return;
		}

		if (!CTszhReceiptTemplateProcessor::CheckTemplate($arParams["CUSTOM_TEMPLATE_PATH"]))
		{
			ShowError(GetMessage("TSZH_PRINT_ERROR_TEMPLATE_NOT_FOUND"));
			return;
		}
		break;

	case "USER":
		// ���� ������������ �� �����������, ������� ����� �����������
		if (!$USER->IsAuthorized())
		{
			$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
		}

		// ���� ������������ �� ����������� � ������ ���������� ��ƻ
		// ������� ��������� � �������� ������ ����������
		if (!CTszh::IsTenant() && !$USER->IsAdmin())
		{
			//$this->AbortResultCache();
			$APPLICATION->AuthForm(GetMessage("TSZH_NOT_A_MEMBER"));
		}
		break;
}

if ($arParams["DISABLE_TIME_ZONE"] == "Y")
	CTszh::DisableTimeZone();

$arErrors = array();
$arAccountPeriods = array();

if (in_array($mode, array("ADMIN", "AGENT")))
{
	$_GET["print"] = "Y";
}
else
{
	$arAccount = CTszhAccount::GetByUserID($USER->GetID());

	if (array_key_exists("period", $_GET) && IntVal($_GET["period"]) > 0)
	{
		$obLastPeriod = CTszhAccountPeriod::GetList(
			Array("PERIOD_DATE" => "DESC", "PERIOD_ID" => "DESC"),
			Array(
				'ACCOUNT_ID' => $arAccount["ID"],
				"PERIOD_ID" => IntVal($_GET["period"]),
				"PERIOD_ACTIVE" => "Y",
				"!PERIOD_ONLY_DEBT" => "Y",
			),
			false,
			false,
			Array('*', 'UF_*')
		);
	}
	else
	{
		// ������ ID ���������� �������
		$lastPeriod = CTszhAccountPeriod::GetList(
			Array("PERIOD_DATE" => "DESC", "PERIOD_ID" => "DESC"),
			Array(
				'ACCOUNT_ID' => $arAccount["ID"],
				"PERIOD_TSZH_ID" => $arAccount["TSZH_ID"],
				"PERIOD_ACTIVE" => "Y",
				"!PERIOD_ONLY_DEBT" => "Y",
			),
			false,
			array('nTopCount' => 1),
			Array('ID', 'PERIOD_ID')
		)->Fetch();

		if (is_array($lastPeriod))
		{
			$obLastPeriod = CTszhAccountPeriod::GetList(
				Array("PERIOD_DATE" => "DESC", "PERIOD_ID" => "DESC"),
				Array(
					'ACCOUNT_ID' => $arAccount["ID"],
					"PERIOD_ID" => $lastPeriod["PERIOD_ID"],
					"PERIOD_TSZH_ID" => $arAccount["TSZH_ID"],
					"PERIOD_ACTIVE" => "Y",
					"!PERIOD_ONLY_DEBT" => "Y",
				),
				false,
				false,
				Array('*', 'UF_*')
			);
		}
	}

	if (isset($obLastPeriod) && is_object($obLastPeriod))
	{
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($receipt = $obLastPeriod->GetNext())
		{
			$arAccountPeriods[] = $receipt;
		}
		unset($obLastPeriod);
	}
	else
		$arErrors[] = GetMessage("TSZH_NO_PERIOD");
}

if (empty($arErrors))
{
	if (in_array($mode, array("ADMIN", "AGENT")))
	{
		$rsAccountPeriods = CTszhAccountPeriod::GetList(
			$mode == "ADMIN" ? $arParams["ACCOUNT_PERIOD_SORT"] : array("ACCOUNT_ID" => "ASC", "TYPE" => "ASC"),
			$arParams["ACCOUNT_PERIOD_FILTER"],
			false,
			false,
			Array("*", "UF_*")
		);
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($arAccountPeriod = $rsAccountPeriods->GetNext())
		{
			$arAccountPeriods[] = $arAccountPeriod;
		}
		unset($rsAccountPeriods);
	}

	// ���������� ������
	$arIDs = array(
		"SITES" => array(),
		"TSZHS" => array(),
		"ACCOUNT_PERIODS" => array(),
		"ACCOUNTS" => array(),
		"EXECUTORS" => array(),
		"USERS" => array(),
	);
	$arData = array(
		"SITES" => array(),
		"TSZHS" => array(),
		"CONTRACTORS" => array(),
		"EXECUTOR_IDS" => array(),
		"EXECUTORS" => array(),
		"CORRECTIONS" => array(),
		"INSTALLMENTS" => array(),
		"CHARGES" => array(),
		"DEBT_CHARGES" => array(),
		"METERS" => array(),
		"HMETERS" => array(),
		"USERS" => array(),
	);
	foreach ($arAccountPeriods as $arAccountPeriod)
	{
		$arIDs["ACCOUNT_PERIODS"][] = $arAccountPeriod["ID"];
		$arIDs["TSZHS"][] = $arAccountPeriod["PERIOD_TSZH_ID"];
		$arIDs["ACCOUNTS"][] = $arAccountPeriod["ACCOUNT_ID"];
		$arIDs["USERS"][] = $arAccountPeriod["USER_ID"];
	}
	foreach ($arIDs as $key => $arValue)
	{
		$arIDs[$key] = array_unique($arValue);
	}

	$rsSites = CSite::GetList($by="sort", $order="asc");
	/** @noinspection PhpAssignmentInConditionInspection */
	while ($arSite = $rsSites->Fetch())
	{
		$arData["SITES"][$arSite["ID"]] = $arSite;
	}

	if (!empty($arIDs["TSZHS"]))
	{
		$rsTszhs = CTszh::GetList(
			Array(),
			Array("ID" => $arIDs["TSZHS"]),
			false,
			false,
			Array("*", "UF_*")
		);
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($arTszh = $rsTszhs->GetNext())
		{
			$arData["TSZHS"][$arTszh["ID"]] = $arTszh;
			$arIDs["SITES"][] = $arTszh["SITE_ID"];
		}
		unset($rsTszhs);
	}
	$arIDs["SITES"] = array_unique($arIDs["SITES"]);
	if (!empty($arIDs["ACCOUNT_PERIODS"]))
	{
		// ���������� �������
		$rsContractors = CTszhAccountContractor::GetList(
			Array("ID" => "ASC"),
			Array("ACCOUNT_PERIOD_ID" => $arIDs["ACCOUNT_PERIODS"]),
			false,
			false,
			Array("*")
		);
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($arContractor = $rsContractors->GetNext())
		{
			$arData['CONTRACTORS'][$arContractor['ACCOUNT_PERIOD_ID']][$arContractor['ID']] = $arContractor;

			if ($arContractor['CONTRACTOR_EXECUTOR'] != 'N')
			{
				$arIDs["EXECUTORS"][] = $arContractor['CONTRACTOR_ID'];
				$arData['EXECUTOR_IDS'][$arContractor['ACCOUNT_PERIOD_ID']] = $arContractor['CONTRACTOR_ID'];
			}
		}
		unset($rsContractors);

		$rsCorrections = CTszhAccountCorrections::GetList(
			Array("ID" => "ASC"),
			Array("ACCOUNT_PERIOD_ID" => $arIDs["ACCOUNT_PERIODS"]),
			false,
			false,
			Array("*")
		);
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($arCorrection = $rsCorrections->GetNext())
		{
			$arData["CORRECTIONS"][$arCorrection["ACCOUNT_PERIOD_ID"]][$arCorrection["ID"]] = $arCorrection;
		}
		unset($rsCorrections);

		$rsInstallments = CTszhAccountInstallments::GetList(
			Array("ID" => "ASC"),
			Array("ACCOUNT_PERIOD_ID" => $arIDs["ACCOUNT_PERIODS"]),
			false,
			false,
			Array("*")
		);
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($arInstallment = $rsInstallments->GetNext())
		{
			$arData["INSTALLMENTS"][$arInstallment["ACCOUNT_PERIOD_ID"]][$arInstallment["ID"]] = $arInstallment;
		}
		unset($rsInstallments);

		$rsCharges = CTszhCharge::GetList(
			Array("GROUP" => "ASC", "SORT" => "ASC", "ID" => "ASC"),
			Array(
				"ACCOUNT_PERIOD_ID" => $arIDs["ACCOUNT_PERIODS"],
			),
			false,
			false,
			Array("*", "UF_*")
		);
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($arCharge = $rsCharges->GetNext())
		{
			/**
			 * ������ XML �� �������� ����������� ������������ ������� > ������ �������� ���������
			 * ��� ������ ��� ������� � ��������� ����������� �������� �� -, ������� �� �������
			 */
			$arCharge['SERVICE_NAME'] = preg_replace('#^\-([^\s])#', ' > $1', $arCharge['SERVICE_NAME']);
			// ������ � ������ �������� ��������� ������ ������� -- ������ ��� �������
			if (substr($arCharge['SERVICE_NAME'], 0, 2) == '- ')
				$arCharge['SERVICE_NAME'] = ' ' . $arCharge['SERVICE_NAME'];

			if ($arCharge["X_FIELDS"])
				$arCharge["X_FIELDS"] = explode(',', $arCharge["X_FIELDS"]);
			else
				$arCharge["X_FIELDS"] = array();
			if ($arParams["WITH_DOLG"] == "N")
				$arCharge["SUMM2PAY"] = ($arCharge["SUMM"] ? $arCharge["SUMM"] : $arCharge["AMOUNT"] * $arCharge["SERVICE_TARIFF"]) + $arCharge["CORRECTON"];

			if ($arCharge["DEBT_ONLY"] == "Y")
			{
				$arCharge["SUMM2PAY"] = $arCharge["DEBT_BEG"] - $arCharge["SUMM_PAYED"]; // ���������� � ������� ����� � �������������� ���� � ������ �������� ������
				$arData['DEBT_CHARGES'][$arCharge["ACCOUNT_PERIOD_ID"]][] = $arCharge;
			}
			else
				$arData['CHARGES'][$arCharge["ACCOUNT_PERIOD_ID"]][] = $arCharge;
		}
		unset($rsCharges);
	}
	$arIDs["EXECUTORS"] = array_unique($arIDs["EXECUTORS"]);
	if (!empty($arIDs["EXECUTORS"]))
	{
		$rsExecutors = CTszhContractor::GetList(
			Array("ID" => "ASC"),
			Array("ID" => $arIDs["EXECUTORS"]),
			false,
			false,
			Array("*")
		);
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($arExecutor = $rsExecutors->GetNext())
		{
			$arData["EXECUTORS"][$arExecutor["ID"]] = $arExecutor;
		}
		unset($rsExecutors);
	}
	if (!empty($arIDs["ACCOUNTS"]))
	{
		$rsMeters = CTszhMeter::GetList(
			Array("SORT" => "ASC", "ID" => "ASC"),
			Array(
				"ACCOUNT_ID" => $arIDs["ACCOUNTS"],
				"ACTIVE" => "Y",
				"HOUSE_METER" => "N",
			)
		);
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($arMeter = $rsMeters->GetNext())
		{
			foreach ($arMeter["ACCOUNT_ID"] as $accountID)
				$arData["METERS"][$accountID][$arMeter["ID"]] = $arMeter;
		}
		unset($rsMeters);

		$rsHMeters = CTszhMeter::GetList(
			Array("SORT" => "ASC", "ID" => "ASC"),
			Array(
				"HOUSE_METER" => "Y",
				"ACTIVE" => "Y",
			)
		);
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($arMeter = $rsHMeters->GetNext())
		{
			$arData["HMETERS"][$arMeter["ID"]] = $arMeter;
		}
		unset($rsHMeters);
	}
	$arIdFilter = implode("|", $arIDs["USERS"]);
	if (!empty($arIDs["USERS"])&&(strlen($arIdFilter)>0))
	{
		$rsUsers = CUser::GetList($by="id", $order="asc", array("ID" => $arIdFilter));
		/** @noinspection PhpAssignmentInConditionInspection */
		while ($arUser = $rsUsers->GetNext())
		{
			$arData["USERS"][$arUser["ID"]] = $arUser;
		}
		unset($rsUsers);
	}

	$arOptions = Array(
		"org_title" => "NAME",
		"org_inn" => "INN",
		"org_kpp" => "KPP",
		"org_rsch" => "RSCH",
		"org_bank" => "BANK",
		"org_ksch" => "KSCH",
		"org_bik" => "BIK",
	);
	$arOptionValues = array();
	foreach ($arOptions as $sOption => $sField)
	{
		$arOptionValues[$sOption] = COption::GetOptionString("citrus.tszh", $sOption);
	}

	// ����������� ���������
	require_once(__DIR__ . '/helper.php');
	foreach ($arAccountPeriods as $key => $arAccountPeriod)
	{
		global $not_show_personal_data;
		if ($not_show_personal_data)
			$arAccountPeriod["ACCOUNT_NAME"] = "";

		$arResult = array(
			"MODE" => $mode,
			"ACCOUNT_PERIOD" => $arAccountPeriod,
			"USER" => $arData['USERS'][$arAccountPeriod['USER_ID']],
			"CONTRACTORS" => isset($arData['CONTRACTORS'][$arAccountPeriod['ID']]) ? $arData['CONTRACTORS'][$arAccountPeriod['ID']] : array(),
			"EXECUTOR_ID" => isset($arData['EXECUTOR_IDS'][$arAccountPeriod['ID']]) ? $arData['EXECUTOR_IDS'][$arAccountPeriod['ID']] : false,
			"EXECUTOR" => isset($arData['EXECUTOR_IDS'][$arAccountPeriod['ID']]) ? $arData['EXECUTORS'][$arData['EXECUTOR_IDS'][$arAccountPeriod['ID']]] : false,
			"CORRECTIONS" => isset($arData['CORRECTIONS'][$arAccountPeriod['ID']]) ? $arData['CORRECTIONS'][$arAccountPeriod['ID']] : array(),
			"INSTALLMENTS" => isset($arData["INSTALLMENTS"][$arAccountPeriod['ID']]) ? $arData["INSTALLMENTS"][$arAccountPeriod['ID']] : array(),
			"CHARGES" => isset($arData['CHARGES'][$arAccountPeriod['ID']]) ? $arData['CHARGES'][$arAccountPeriod['ID']] : array(),
			"HAS_CHARGES_METERS_BINDING" => false,
			"DEBT_CHARGES" => isset($arData['DEBT_CHARGES'][$arAccountPeriod['ID']]) ? $arData['DEBT_CHARGES'][$arAccountPeriod['ID']] : array(),
			"SUM2PAY" => $arAccountPeriod["DEBT_END"],
			"TSZH" => $arData["TSZHS"][$arAccountPeriod["PERIOD_TSZH_ID"]],
			"METERS" => isset($arData["METERS"][$arAccountPeriod["ACCOUNT_ID"]]) ? $arData["METERS"][$arAccountPeriod["ACCOUNT_ID"]] : array(),
			"HMETERS" => array(),
            "IS_INSURANCE" => ($arAccountPeriod["SUMM_PAYED_INSURANCE"] > 0) ? "Y" : "N",

			"IS_FIRST" => $key == 0,
			"IS_LAST" => $key == count($arAccountPeriods)-1,
			"TOTALS" => array(),
			// ������������� �� ����������?
			"HAS_GROUPS" => false,
			// ���� �� ������ � ������ ������������ ����� (������� �������)
			"HAS_AMOUNT_NOTES" => false,
		);

		/** @var array $prevNonComponentCharge �������� ������ �� ������ ������ ���������� � ������������ */
		$prevNonComponentCharge = null;

		foreach ($arResult["CHARGES"] as $idx => &$arCharge)
		{
			if (!empty($arCharge["METER_IDS"]))
				$arResult["HAS_CHARGES_METERS_BINDING"] = true;

			foreach ($arCharge["HMETER_IDS"] as $hMeterID)
				$arResult["HMETERS"][$hMeterID] = $arData["HMETERS"][$hMeterID];

			// ��������� HAS_COMPONENTS ��� ����������, ������� �����������
			$arCharge["HAS_COMPONENTS"] = false;
			if (isset($prevNonComponentCharge) && $arCharge["COMPONENT"] != ComponentType::NONE)
				$prevNonComponentCharge["HAS_COMPONENTS"] = $arCharge["COMPONENT"];
			if ($arCharge["COMPONENT"] == ComponentType::NONE)
				$prevNonComponentCharge = &$arCharge;
            if ($arCharge["IS_INSURANCE"] != "N")
            {
                $arResult["INSURANCE"]["AMOUNT"] = $arCharge["AMOUNT"];
                $arResult["INSURANCE"]["SUMM"] = $arCharge["SUMM"];
                $arResult["INSURANCE"]["RATE"] = $arCharge["RATE"];
                $arResult["INSURANCE"]["SERVICE_NAME"] = $arCharge["SERVICE_NAME"];
                $arResult["INSURANCE"]["SUMM_PAYED"] = $arCharge["SUMM_PAYED"];
            }
		}
		if (isset($prevNonComponentCharge))
			unset($prevNonComponentCharge);

		// ������ ���� �� ���� � ������ � ����������
		foreach ($arResult["CHARGES"] as $idx => $charge)
		{
//			$arResult["CHARGES"][$idx]["SUMM2PAY"] = floatval($charge["SUMM2PAY"]) - floatval($charge["PENALTIES"]);
			$arResult["CHARGES"][$idx]["SUMM2PAY"] = floatval($charge["SUMM2PAY"]);
		}
		foreach ($arResult["DEBT_CHARGES"] as $idx => $charge)
		{
//			$arResult["DEBT_CHARGES"][$idx]["SUMM2PAY"] = floatval($charge["SUMM2PAY"]) - floatval($charge["PENALTIES"]);
			$arResult["DEBT_CHARGES"][$idx]["SUMM2PAY"] = floatval($charge["SUMM2PAY"]);
		}

		// ������ ���� �� ���� � ������ � �����������
		foreach ($arResult["CONTRACTORS"] as $idx => $arContractor)
		{
//			$arResult["CONTRACTORS"][$idx]["SUMM"] = floatval($arContractor["SUMM"]) - floatval($arContractor["PENALTIES"]);
			$arResult["CONTRACTORS"][$idx]["SUMM"] = floatval($arContractor["SUMM"]);
		}

		// ������� ������ � ����������� ������� �����������
		// ================================================
		/** @var array $totalFields ���� ��� �������� ����. � ������� _ ���������� ����������� ����, �������� ������� ����������� ��� <����> - H<����>. ��������: _SUMM = SUMM-HSUMM */
		$totalFields = array("_SUMM", "HSUMM", "SUMM", "CORRECTION", "COMPENSATION", "PENALTIES", "SUMM2PAY", "SUMM_PAYED", "RAISE_SUM","SUM_WITHOUT_RAISE","CSUM_WITHOUT_RAISE", "DEBT_BEG");

		foreach ($totalFields as $field)
		{
			$arResult["TOTALS"][$field] = 0;
			$chargesAndDebt = array_merge($arResult['CHARGES'], $arResult['DEBT_CHARGES']);
			foreach ($chargesAndDebt as $charge)
			{
				if ($charge["IS_INSURANCE"] == "Y") continue;
			    if ($charge["~GROUP"] && $charge["~GROUP"] != 3)
					$arResult["HAS_GROUPS"] = true;
				if (isset($charge["AMOUNTN"]) && !empty($charge["AMOUNTN"]) || isset($charge["HAMOUNTN"]) && !empty($charge["HAMOUNTN"]))
					$arResult["HAS_AMOUNT_NOTES"] = true;

				// �� �������� � ����� ������ � ������������ ����������
				if ($charge["COMPONENT"] != "N")
					continue;

				// �������� � ����� ������ � �������������� ����� ������ ��� ���� �����
				if ($charge["DEBT_ONLY"] == "Y" && !in_array($field, array("SUMM_PAYED", "PENALTIES", "DEBT_BEG")))
					continue;

				if (substr($field, 0, 1) == '_')
				{
					$baseField = substr($field, 1);
					$value = CCitrusTszhReceiptComponentHelper::getArrayValue(array($baseField, "H" . $baseField), $charge, false, $charge[$baseField] - $charge["H" . $baseField]);
				}
				else
                {
                    $value = CCitrusTszhReceiptComponentHelper::getArrayValue($field, $charge, false);
                }
                if (($field == "CSUM_WITHOUT_RAISE") && !($value))
                {
                   $arResult["TOTALS"][$field] += CCitrusTszhReceiptComponentHelper::getArrayValue("SUMM", $charge, false);
                }
				$arResult["TOTALS"][$field] += (float) $value;
			}
		}

		// �������� ������ ������� ��������� ������� ������
		CTszhAccountHistory::CorrectFromHistory($arResult["ACCOUNT_PERIOD"], $arResult["ACCOUNT_PERIOD"]["ACCOUNT_ID"], ConvertTimeStamp(MakeTimeStamp($arResult["ACCOUNT_PERIOD"]["PERIOD_DATE"], "YYYY-MM-DD")));
		// ������, �������! ���� TYPE �� �������� ����� �������� ����������� � ��������� � ������ �� �����
		$arResult["ACCOUNT_PERIOD"]["TYPE"] = $arResult["ACCOUNT_PERIOD"]["~TYPE"];

		$arResult["ACCOUNT_PERIOD"]["DISPLAY_NAME"] = CTszh::ToUpperFirstChar(CTszhPeriod::Format($arResult["ACCOUNT_PERIOD"]["PERIOD_DATE"]));

		if($arParams["REPLACE_HOUSING"] == "Y")
			$arResult["ACCOUNT_PERIOD"]["HOUSE"] = str_replace("/", GetMessage("TSZH_HOUSING_REPLACE"), $arResult["ACCOUNT_PERIOD"]["HOUSE"]);

		$arTszh = $arResult["TSZH"];
		foreach ($arOptions as $sOption => $sField)
		{
			$arResult[ToUpper($sOption)] = array_key_exists($sField, $arTszh) ? $arTszh[$sField] : $arOptionValues[$sOption];
		}

		$arSite = $arData["SITES"][$arTszh["SITE_ID"]];

		// ���� ����� ��������� ���������
		$metersValuesLastUpdate = 0;
		$siteDateFormat = $arSite["FORMAT_DATETIME"];

		$arPeriodTimestamp = ParseDateTime($arResult["ACCOUNT_PERIOD"]["PERIOD_DATE"], "YYYY-MM-DD");
		$strMonth = $arPeriodTimestamp["YYYY"] . "-" . $arPeriodTimestamp["MM"] . "-01";
		$timeMonth = MakeTimeStamp($strMonth, "YYYY-MM-DD");
		$timeNextMonth = strtotime("+1 month", $timeMonth);
		$month = ConvertTimeStamp($timeMonth, "FULL", $arTszh["SITE_ID"]);
		$nextMonth = ConvertTimeStamp($timeNextMonth, "FULL", $arTszh["SITE_ID"]);

		/** @var CDBResult $finesReceipt ���� �� ��� �������� ������� ��������� ��������� � ���� ��� ���. �������� ��� �������� �������� ����� */
		$finesReceipt = CTszhAccountPeriod::GetList(array(), array("PERIOD_ID" => $arResult["ACCOUNT_PERIOD"]["PERIOD_ID"], "TYPE" => array(ReceiptType::FINES_MAIN, ReceiptType::FINES_OVERHAUL, ReceiptType::OVERHAUL)), false, array('nTopCount' => 1), array('ID'));
		$arResult["HAS_SEPARATE_PENALTIES_RECEIPT"] = $finesReceipt->SelectedRowsCount() > 0;

		$arMeterIDs = array_merge(array_keys($arResult["METERS"]), array_keys($arResult["HMETERS"]));
		// ���� ���� �������� ��������� � �����������, ��������� ����� ��������� � ��������� ������ ����, ��� ����� �������� � ����������� ���� ���������
		// ����� �� ������ ��������� ��� ������ ������, ����� ������� ��� ����������� ������ ��� ����� ������ ������, ��������� �� �� ������� ���������� ����� ���������
		if ($arResult["HAS_SEPARATE_PENALTIES_RECEIPT"])
		{
			/** @var array $metersInCharges ������ � ID ���������, ������� ����� �������� � ����������� ���� ��������� � � ��������� ������ �������� ������ ��� */
			$metersInCharges = array();
			$allServices = array_merge($arResult["CHARGES"], $arResult["DEBT_CHARGES"]);

			foreach ($allServices as $idx => &$arCharge)
			{
				if (!empty($arCharge["METER_IDS"]))
					$metersInCharges = array_merge($metersInCharges, $arCharge["METER_IDS"]);

				if (!empty($arCharge["HMETER_IDS"]))
					$metersInCharges = array_merge($metersInCharges, $arCharge["HMETER_IDS"]);
			}
			$metersInCharges = array_unique($metersInCharges);

			$arMeterIDs = array_intersect($arMeterIDs, $metersInCharges);
			$metersInCharges = array_fill_keys($metersInCharges, 1);
			$arResult["METERS"] = array_intersect_key($arResult["METERS"], $metersInCharges);
		}

		if (!empty($arMeterIDs))
		{
			$rsMeterValue = CTszhMeterValue::GetList(
				Array("TIMESTAMP_X" => "DESC", "ID" => "DESC"),
				Array(
					"@METER_ID" => $arMeterIDs,
					">=TIMESTAMP_X" => $month,
					"<=TIMESTAMP_X" => $nextMonth,
				)
			);
			/** @noinspection PhpAssignmentInConditionInspection */
			while ($arMeterValue = $rsMeterValue->GetNext())
			{
				$arMeterValue["TIMESTAMP_X_TS"] = MakeTimeStamp($arMeterValue["TIMESTAMP_X"], $siteDateFormat);
				if ($arMeterValue["METER_HOUSE_METER"] == "Y")
					$pMeter =& $arResult["HMETERS"][$arMeterValue["METER_ID"]];
				else
					$pMeter =& $arResult["METERS"][$arMeterValue["METER_ID"]];
				if ($pMeter["VALUE"]["TIMESTAMP_X_TS"] < $arMeterValue["TIMESTAMP_X_TS"]
					|| $pMeter["VALUE"]["TIMESTAMP_X_TS"] == $arMeterValue["TIMESTAMP_X_TS"] && $pMeter["VALUE"]["ID"] < $arMeterValue["ID"])
				{
					$pMeter["VALUE"] = $arMeterValue;

					// ���������� ���� ���������� ����� ���������
					if ($arMeterValue["METER_HOUSE_METER"] == "N" && $metersValuesLastUpdate < $arMeterValue["TIMESTAMP_X_TS"])
						$metersValuesLastUpdate = $arMeterValue["TIMESTAMP_X_TS"];
				}
			}

			$rsMaxBeforeDates = CTszhMeterValue::GetList(
				Array(),
				Array(
					"@METER_ID" => $arMeterIDs,
					"<TIMESTAMP_X" => $month,
				),
				array("METER_ID", "MAX" => "TIMESTAMP_X")
			);
			$arMetersMaxBeforeDates = array();
			$arMaxBeforeDates = array();
			/** @noinspection PhpAssignmentInConditionInspection */
			while ($arMaxBeforeDate = $rsMaxBeforeDates->GetNext())
			{
				$ts = MakeTimeStamp($arMaxBeforeDate["TIMESTAMP_X"], "YYYY-MM-DD HH:MI:SS");
				$arMetersMaxBeforeDates[$arMaxBeforeDate["METER_ID"]] = $ts;
				$arMaxBeforeDates[] = ConvertTimeStamp($ts, "FULL", $arTszh["SITE_ID"]);
			}

			$rsMeterValueBefore = CTszhMeterValue::GetList(
				Array("TIMESTAMP_X" => "DESC", "ID" => "DESC"),
				Array(
					"@METER_ID" => $arMeterIDs,
					"TIMESTAMP_X" => $arMaxBeforeDates,
				)
			);
			/** @noinspection PhpAssignmentInConditionInspection */
			while ($arMeterValueBefore = $rsMeterValueBefore->GetNext())
			{
				$arMeterValueBefore["TIMESTAMP_X_TS"] = MakeTimeStamp($arMeterValueBefore["TIMESTAMP_X"], $siteDateFormat);
				if ($arMeterValueBefore["METER_HOUSE_METER"] == "Y")
					$pMeter =& $arResult["HMETERS"][$arMeterValueBefore["METER_ID"]];
				else
					$pMeter =& $arResult["METERS"][$arMeterValueBefore["METER_ID"]];
				if ($pMeter["VALUE_BEFORE"]["TIMESTAMP_X_TS"] < $arMeterValueBefore["TIMESTAMP_X_TS"]
					|| $pMeter["VALUE_BEFORE"]["TIMESTAMP_X_TS"] == $arMeterValueBefore["TIMESTAMP_X_TS"] && $pMeter["VALUE_BEFORE"]["ID"] < $arMeterValueBefore["ID"])
				{
					$pMeter["VALUE_BEFORE"] = $arMeterValueBefore;
				}
			}
		}
		$arResult["METERS_LAST_UPDATE"] = $metersValuesLastUpdate > 0 ? ConvertTimeStamp($metersValuesLastUpdate, "FULL") : false;

		switch ($mode)
		{
			case "ADMIN":
				$templatePath = $arParams["CUSTOM_TEMPLATE_PATH"];
				$tszhTemplatePath = $arParams["CUSTOM_TEMPLATE_PATH"] == CTszhReceiptTemplateProcessor::AUTO_DETECT ? $arResult["TSZH"]["RECEIPT_TEMPLATE"] : "";
				break;

			case "AGENT":
				$templatePath = $arParams["CUSTOM_TEMPLATE_PATH"];
				$tszhTemplatePath = $arResult["TSZH"]["RECEIPT_TEMPLATE"];
				// ��� ������ ���������� ��������� �� ��� (���. ������ � �.�.) ��������� ������� ��� ��������
				// $arResult["MODE"] = 'ADMIN';
				break;

			case "USER":
				$templatePath = "";
				$tszhTemplatePath = $arResult["TSZH"]["RECEIPT_TEMPLATE"];
				// ��� ������ ���������� ��������� �� ��� (���. ������ � �.�.) ��������� ������� ��� ��������
				$arResult["MODE"] = 'ADMIN';
				break;
		}

		if ($arResult["MODE"] == "ADMIN")
		{
			if ($key == 0 && $_GET["print"] == 'Y')
				$APPLICATION->RestartBuffer();
			ob_start();
		}

		/**
		 * $arResult["IS_FINES_RECEIPT"] - �������� �� ���������� �� ����
		 * � ���������� � ���� �� 354 ������������� ���������� 4-� ������ � ������� 3, 4, 5, 6, 7, 10, 12 � 13 ������ ������ �-���
		 */
		$arResult["IS_FINES_RECEIPT"] = in_array($arResult["ACCOUNT_PERIOD"]["TYPE"], array(ReceiptType::FINES_MAIN, ReceiptType::FINES_OVERHAUL));
		if ($arResult["IS_FINES_RECEIPT"])
		{
			foreach ($arResult["CHARGES"] as &$charge)
			{
				$charge["X_FIELDS"] = array_merge($charge["X_FIELDS"], array(
					'AMOUNT',
					"HAMOUNT",
					"TARIF",
					"HSUMM",
					"COMPENSATION",
					"HSUMM2PAY",
				));
				$charge["RATE"] = 'X';
			}
			if (isset($charge))
				unset($charge);

			$arResult["TOTALS"]["_SUMM"] = $arResult["TOTALS"]["HSUMM"] = $arResult["TOTALS"]["COMPENSATION"] = '';
		}

		/**
		 * $arResult["IS_OVERHAUL"] - �������� �� ���������� �� ���. �������
		 * � ���������� � ���� �� 354 ������������� ���������� 4-� ������ � ������� 3, 4, 5, 6, 7, 10, 12 � 13 ������ ������ �-���
		 */
		$arResult["IS_OVERHAUL"] = in_array($arResult["ACCOUNT_PERIOD"]["TYPE"], array(ReceiptType::OVERHAUL, ReceiptType::FINES_OVERHAUL));
		$arResult["RECEIPT_PAYMENT_TYPE"] = ($arResult["IS_FINES_RECEIPT"] ? 'FINES_' : '') . ($arResult["IS_OVERHAUL"] ? 'OVERHAUL' : 'MAIN');

		$arInfo = CTszhReceiptTemplateProcessor::getTemplatePathInfo($arSite, $templatePath, $tszhTemplatePath);
		if (strlen($arInfo["TEMPLATE_NAME"]) > 0)
			$this->__templateName = $arInfo["TEMPLATE_NAME"];

		if (method_exists($this, "initComponentTemplate") && method_exists($this, "showComponentTemplate") && method_exists($this, "includeComponentEpilog")
			&& method_exists($this, "abortResultCache") && method_exists($this, "__showError"))
		{
			if ($this->initComponentTemplate("", $arInfo["SITE_TEMPLATE_ID"], $arInfo["TEMPLATE_PATH"]))
			{
				$this->showComponentTemplate();
				if ($this->__component_epilog)
					$this->includeComponentEpilog($this->__component_epilog);
			}
			else
			{
				$this->abortResultCache();
				$this->__showError("Cannot find '" . $this->getTemplateName() . "' template with page ''");
			}
		}
		else
			$this->IncludeComponentTemplate("", $arInfo["TEMPLATE_PATH"]);

		if ($mode == "ADMIN")
		{
			$html = ob_get_contents();
			ob_end_clean();
			echo $html;

			if ($_GET["print"] == 'Y' && $arResult["IS_LAST"])
			{
				require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
				die();
			}
		}
		elseif ($arResult["MODE"] == "ADMIN")
		{
			ob_end_flush();
			// ����� �������� �� ������ ���������, ����� �������� �� �����
			if ($_GET["print"] == 'Y' && $arResult["IS_LAST"] && $mode!="AGENT")
			{
				require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php");
				die();
			}
		}
	}
}

if ($mode == "AGENT")
{
	$agentHtml = ob_get_contents();
	ob_end_clean();
}

if (!empty($arErrors))
	ShowError(implode("<br />", $arErrors));

if ($arParams["DISABLE_TIME_ZONE"] == "Y")
	CTszh::EnableTimeZone();

if (isset($arInfo))
	return array($arInfo["TEMPLATE_PATH"], $agentHtml);