<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS"  =>  array(
		"WITH_DOLG" => array(
			"NAME" => GetMessage("SUMM_WITH_DOLG"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => 'Y',
		),
        "REPLACE_HOUSING" => array(
            "NAME" => GetMessage("REPLACE_HOUSING"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => 'N',
        ),
        "CACHE_TIME"  =>  Array("DEFAULT"=>3600),
	),
);
?>
