<?
$MESS["COMP_NAV_PAGER"] = "Периоды";
$MESS["COMP_MAX_COUNT"] = "Количество периодов на одной странице";
$MESS["RECEIPT_URL"] = "Путь к квитанции за период";
$MESS["CITRUS_TSZH_SHEET_SHOW_SERVICE_CORRECTIONS"] = "Отображать столбец перерасчетов в списке услуг";
$MESS["CITRUS_TSZH_SHEET_SHOW_SERVICE_COMPENSATION"] = "Отображать столбец льгот в списке услуг";
?>