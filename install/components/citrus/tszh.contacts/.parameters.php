<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (!\Bitrix\Main\Loader::includeModule("citrus.tszh"))
	return;

$rsTszhs = CTszh::GetList(
	array("NAME" => "ASC"),
	array(),
	false,
	false,
	array("ID", "CODE", "NAME")
);
$arTszhs = array("__TSZH_ALL__" => GetMessage("P_ALL_TSZHS"));
while ($arTszh = $rsTszhs->GetNext())
{
	$key = $arTszh["ID"];
	if (strlen($arTszh["CODE"]) > 0)
		$key = $arTszh["CODE"];
	$arTszhs[$key] = "[{$key}] {$arTszh["NAME"]}";
}

$site = ($_REQUEST["site"] <> "" ? $_REQUEST["site"] : ($_REQUEST["src_site"] <> ""? $_REQUEST["src_site"] : false));
$arFilter = Array("TYPE_ID" => "TSZH_FEEDBACK_FORM", "ACTIVE" => "Y");
if ($site !== false)
	$arFilter["LID"] = $site;

$arEvent = Array();
$dbType = CEventMessage::GetList($by="ID", $order="DESC", $arFilter);
while($arType = $dbType->GetNext())
	$arEvent[$arType["ID"]] = "[".$arType["ID"]."] ".$arType["SUBJECT"];

$arComponentParameters = array(
	"GROUPS" => array(
		"MAP" => array(
			"NAME" => GetMessage("P_G_MAP"),
			"SORT" => 210,
		),
		"FEEDBACK_FORM" => array(
			"NAME" => GetMessage("P_G_FEEDBACK_FORM"),
			"SORT" => 220,
		),
	),
	"PARAMETERS"  =>  array(
		"TSZH_ID"  =>  Array(
			"PARENT" => "DATA_SOURCE",
			"NAME" => GetMessage("P_TSZH_ID"),
			"TYPE" => "LIST",
			"VALUES" => $arTszhs,
			"MULTIPLE" => "Y",
			"DEFAULT" => "__TSZH_ALL__",
		),
		"MAX_COUNT" => array(
			"PARENT" => "ADDITIONAL_SETTINGS",
			"NAME" => GetMessage("P_MAX_COUNT"),
			"TYPE" => "INT",
			"DEFAULT" => "3",
		),
		"CONTACTS_URL"  =>  Array(
			"PARENT" => "ADDITIONAL_SETTINGS",
			"NAME" => GetMessage("P_CONTACTS_URL"),
			"TYPE" => "STRING",
		),
		"SHOW_MAP"  =>  Array(
			"PARENT" => "MAP",
			"NAME" => GetMessage("P_SHOW_MAP"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y",
			"REFRESH" => "Y",
		),
		"SHOW_FEEDBACK_FORM"  =>  Array(
			"PARENT" => "FEEDBACK_FORM",
			"NAME" => GetMessage("P_SHOW_FEEDBACK_FORM"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => "Y",
			"REFRESH" => "Y",
		),
		"CACHE_TIME"  =>  Array("DEFAULT" => 36000000),
	),
);

if ($arCurrentValues["SHOW_MAP"] == "Y" || !is_set($arCurrentValues, "SHOW_MAP"))
{
	$arComponentParameters["PARAMETERS"] = array_merge(
		$arComponentParameters["PARAMETERS"],
		Array(
			"MAP_INIT_MAP_TYPE" => array(
				"PARENT" => "MAP",
				"NAME" => GetMessage("P_MAP_INIT_MAP_TYPE"),
				"TYPE" => "LIST",
				"VALUES" => array(
					"MAP" => GetMessage("P_MAP_INIT_MAP_TYPE_MAP"),
					"SATELLITE" => GetMessage("P_MAP_INIT_MAP_TYPE_SATELLITE"),
					"HYBRID" => GetMessage("P_MAP_INIT_MAP_TYPE_HYBRID"),
					"PUBLIC" => GetMessage("P_MAP_INIT_MAP_TYPE_PUBLIC"),
					"PUBLIC_HYBRID" => GetMessage("P_MAP_INIT_MAP_TYPE_PUBLIC_HYBRID"),
				),
				"DEFAULT" => "MAP",
				"ADDITIONAL_VALUES" => "N",
			),
			/*"MAP_NAME" => array(
				"PARENT" => "MAP",
				"NAME" => GetMessage("P_MAP_NAME"),
				"TYPE" => "STRING",
				"DEFAULT" => "{$arItem["NAME"]}",
			),*/
			/*"MAP_BODY" => array(
				"PARENT" => "MAP",
				"NAME" => GetMessage("P_MAP_BODY"),
				"TYPE" => "STRING",
				"DEFAULT" => "",
			),*/
			/*"MAP_ADDRESS" => array(
				"PARENT" => "MAP",
				"NAME" => GetMessage("P_MAP_ADDRESS"),
				"TYPE" => "STRING",
				"DEFAULT" => "{$arItem["ADDRESS"]}",
			),*/
			"MAP_MAP_WIDTH" => array(
				"PARENT" => "MAP",
				"NAME" => GetMessage("P_MAP_MAP_WIDTH"),
				"TYPE" => "INT",
				"DEFAULT" => "370",
			),
			"MAP_MAP_HEIGHT" => array(
				"PARENT" => "MAP",
				"NAME" => GetMessage("P_MAP_MAP_HEIGHT"),
				"TYPE" => "INT",
				"DEFAULT" => "300",
			),
			"MAP_CONTROLS" => array(
				"PARENT" => "MAP",
				"NAME" => GetMessage("P_MAP_CONTROLS"),
				"TYPE" => "LIST",
				"MULTIPLE" => "Y",
				"VALUES" => array(
					"ZOOM" => GetMessage("P_MAP_CONTROLS_ZOOM"),
					"SMALLZOOM" => GetMessage("P_MAP_CONTROLS_SMALLZOOM"),
					"MINIMAP" => GetMessage("P_MAP_CONTROLS_MINIMAP"),
					"TYPECONTROL" => GetMessage("P_MAP_CONTROLS_TYPECONTROL"),
					"SCALELINE" => GetMessage("P_MAP_CONTROLS_SCALELINE"),
					"SEARCH" => GetMessage("P_MAP_CONTROLS_SEARCH"),
				),
				"DEFAULT" => array("ZOOM", "TYPECONTROL", "SCALELINE"),
			),
			"MAP_OPTIONS" => array(
				"PARENT" => "MAP",
				"NAME" => GetMessage("P_MAP_OPTIONS"),
				"TYPE" => "LIST",
				"MULTIPLE" => "Y",
				"VALUES" => array(
					"ENABLE_SCROLL_ZOOM" => GetMessage("P_MAP_OPTIONS_ENABLE_SCROLL_ZOOM"),
					"ENABLE_DBLCLICK_ZOOM" => GetMessage("P_MAP_OPTIONS_ENABLE_DBLCLICK_ZOOM"),
					"ENABLE_RIGHT_MAGNIFIER" => GetMessage("P_MAP_OPTIONS_ENABLE_RIGHT_MAGNIFIER"),
					"ENABLE_DRAGGING" => GetMessage("P_MAP_OPTIONS_ENABLE_DRAGGING"),
				),
				"DEFAULT" => array("ENABLE_SCROLL_ZOOM", "ENABLE_DBLCLICK_ZOOM", "ENABLE_DRAGGING"),
			),
			/*"MAP_MAP_ID" => array(
				"PARENT" => "MAP",
				"NAME" => GetMessage("P_MAP_MAP_ID"),
				"TYPE" => "STRING",
				"DEFAULT" => "CONTACTS_PAGE_MAP",
			),*/
		)
	);
}

if ($arCurrentValues["SHOW_FEEDBACK_FORM"] == "Y" || !is_set($arCurrentValues, "SHOW_FEEDBACK_FORM"))
{
	$arComponentParameters["PARAMETERS"] = array_merge(
		$arComponentParameters["PARAMETERS"],
		Array(
			"FEEDBACK_FORM_USE_CAPTCHA" => array(
				"PARENT" => "FEEDBACK_FORM",
				"NAME" => GetMessage("P_FEEDBACK_FORM_USE_CAPTCHA"),
				"TYPE" => "CHECKBOX",
				"DEFAULT" => "Y",
			),
			"FEEDBACK_FORM_OK_TEXT" => Array(
				"PARENT" => "FEEDBACK_FORM",
				"NAME" => GetMessage("P_FEEDBACK_FORM_OK_TEXT"),
				"TYPE" => "STRING",
				"DEFAULT" => GetMessage("P_FEEDBACK_FORM_OK_TEXT_DEFAULT"),
			),
			"FEEDBACK_FORM_EMAIL_TO" => Array(
				"PARENT" => "FEEDBACK_FORM",
				"NAME" => GetMessage("P_FEEDBACK_FORM_EMAIL_TO"),
				"TYPE" => "STRING",
				"DEFAULT" => htmlspecialcharsbx(COption::GetOptionString("main", "email_from")),
			),
			"FEEDBACK_FORM_REQUIRED_FIELDS" => Array(
				"PARENT" => "FEEDBACK_FORM",
				"NAME" => GetMessage("P_FEEDBACK_FORM_REQUIRED_FIELDS"),
				"TYPE" => "LIST",
				"MULTIPLE" => "Y",
				"VALUES" => Array("NONE" => GetMessage("P_FEEDBACK_FORM_F_ALL_REQ"), "NAME" => GetMessage("P_FEEDBACK_FORM_F_NAME"), "EMAIL" => "E-mail", "MESSAGE" => GetMessage("P_FEEDBACK_FORM_F_MESSAGE")),
				"DEFAULT" => array("NAME", "EMAIL", "MESSAGE"),
				"COLS" => 25,
			),
			"FEEDBACK_FORM_EVENT_MESSAGE_ID" => Array(
				"PARENT" => "FEEDBACK_FORM",
				"NAME" => GetMessage("P_FEEDBACK_FORM_EVENT_MESSAGE_ID"),
				"TYPE" => "LIST",
				"VALUES" => $arEvent,
				"DEFAULT" => array(),
				"MULTIPLE" => "Y",
				"COLS" => 25,
			),
		)
	);
}
