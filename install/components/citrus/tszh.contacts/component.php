<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (!\Bitrix\Main\Loader::includeModule("citrus.tszh"))
{
	ShowError(GetMessage("TSZH_MODULE_NOT_FOUND_ERROR"));
	return;
}

if (!CTszhFunctionalityController::CheckEdition())
	return;

// set default value for missing parameters, simple param check
$componentParams = CComponentUtil::GetComponentProps($this->getName());
if (is_array($componentParams))
{
	foreach ($componentParams["PARAMETERS"] as $paramName => $paramArray)
	{
		if (!is_set($arParams, $paramName) && is_set($paramArray, "DEFAULT"))
			$arParams[$paramName] = $paramArray["DEFAULT"];

		$paramArray["TYPE"] = ToUpper(is_set($paramArray, "TYPE") ? $paramArray["TYPE"] : "STRING");
		switch ($paramArray["TYPE"]) 
		{
			case "INT":
				$arParams[$paramName] = IntVal($arParams[$paramName]);
				break;

			case "LIST":
				if (!is_array($arParams[$paramName]))
				{
					if (!array_key_exists($arParams[$paramName], $paramArray["VALUES"]))
					{
						$arParams[$paramName] = $paramArray["DEFAULT"];
					}
				}
				break;

			case "CHECKBOX":
				$arParams[$paramName] = ($arParams[$paramName] == (is_set($paramArray, "VALUE") ? $paramArray["VALUE"] : "Y"));
				break;

			default:
				// string etc.
				break;
		}
	}
}

if (!is_array($arParams["TSZH_ID"]))
	$arParams["TSZH_ID"] = array($arParams["TSZH_ID"]);
foreach ($arParams["TSZH_ID"] as $key => $value)
{
	if ($value == "__TSZH_ALL__")
	{
		$arParams["TSZH_ID"] = array();
		break;
	}
	if (strlen($value) <= 0)
		unset($arParams["TSZH_ID"][$key]);
}
/*if (empty($arParams["TSZH_ID"]))
{
	ShowError(GetMessage("C_ERROR_REQ_TSZH_ID"));
	return;
}*/

if ($arParams["MAX_COUNT"] < 0)
	$arParams["MAX_COUNT"] = 0;

if ($this->StartResultCache())
{
	$arResult = array(
		"ITEMS" => Array(),
	);

	$arCodeItems = array();
	if (!empty($arParams["TSZH_ID"]))
	{
		$rsItems = CTszh::GetList(
			array(),
			array(
				"SITE_ID" => SITE_ID,
				"CODE" => $arParams["TSZH_ID"]
			),
			false,
			false,
			array("*", "UF_*")
		);
		while ($arItem = $rsItems->GetNext())
		{
			$arCodeItems["ID{$arItem["ID"]}"] = $arItem;
		}
	}

	$arFilter = array("SITE_ID" => SITE_ID);
	if (!empty($arParams["TSZH_ID"]))
		$arFilter = array("ID" => $arParams["TSZH_ID"]);
	$rsItems = CTszh::GetList(
		array(),
		$arFilter,
		false,
		false,
		array("*", "UF_*")
	);
	$arItems = array();
	while ($arItem = $rsItems->GetNext())
	{
		$arItems["ID{$arItem["ID"]}"] = $arItem;
	}

	$arItems = array_merge($arCodeItems, $arItems);

	$arNames = array();
	foreach ($arItems as $key => $arItem)
		$arNames[$arItem["ID"]] = $arItem["NAME"];
	asort($arNames);
	if (empty($arParams["TSZH_ID"]) && $arParams["MAX_COUNT"] > 0)
		$arNames = array_slice($arNames, 0, $arParams["MAX_COUNT"], true);

	foreach ($arNames as $id => $name)
		$arResult["ITEMS"][$id] = $arItems["ID{$id}"];

	$this->SetResultCacheKeys(array(
		"ITEMS",
		"TEMPLATE_HTML",
	));
	$this->IncludeComponentTemplate();
}
