<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("iblock"))
	return;

//prepare params
$arParams['IBLOCK_ID'] = intval($arParams['IBLOCK_ID']);
if($arParams['IBLOCK_ID']<=0)
	return;

$arParams['TOP_COUNT'] = intval($arParams['TOP_COUNT']);

$arParams["SECTION_ID"] = intval($arParams["SECTION_ID"]);
$arParams["DETAIL_URL"] = trim($arParams["DETAIL_URL"]);

if(strlen($arParams["FILTER_NAME"])<=0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
{
	$arrFilter = array();
}
else
{
	$arrFilter = $GLOBALS[$arParams["FILTER_NAME"]];
	if(!is_array($arrFilter))
		$arrFilter = array();
}

if(isset($arParams["IBLOCK_TYPE"]) && $arParams["IBLOCK_TYPE"]!='')
	$arFilter['IBLOCK_TYPE'] = $arParams["IBLOCK_TYPE"];

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 3600;

//SELECT
$arSelect = Array(
	"ID",
	"NAME",
	"IBLOCK_SECTION_ID",
	"PREVIEW_TEXT_TYPE",
	"PREVIEW_TEXT",
	"DETAIL_TEXT_TYPE",
	"DETAIL_TEXT",
	"DATE_CREATE",
	"TIMESTAMP_X",
	"PROPERTY_answer_date"
);
//WHERE
$arFilter = Array(
	'IBLOCK_ID' => $arParams["IBLOCK_ID"],
	'ACTIVE' => 'Y',
	'IBLOCK_ACTIVE' => 'Y',
);
if ($arParams["SECTION_ID"] > 0)
	$arFilter['SECTION_ID'] = $arParams["SECTION_ID"];

$arFilter = array_merge($arFilter, $arrFilter);

//ORDER BY
$arOrder = Array(
	'SORT' => 'ASC',
	'ID' => 'DESC',
);

$arAddCacheParams = array(
	"MODE" => $_REQUEST['bitrix_show_mode']?$_REQUEST['bitrix_show_mode']:'view',
	"SESS_MODE" => $_SESSION['SESS_PUBLIC_SHOW_MODE']?$_SESSION['SESS_PUBLIC_SHOW_MODE']:'view',
);

//**work body**//
if($this->StartResultCache(false, array($USER->GetGroups(), serialize($arFilter), $arAddCacheParams)))
{
	$arResult['ITEMS'] = Array();
	$arCount = $arParams['TOP_COUNT'] > 0 ? array('nTopCount' => $arParams['TOP_COUNT']) : false;
	$arItems = CIBlockElement::GetList($arOrder, $arFilter, false, $arCount, $arSelect);
	while ($arItem = $arItems->Fetch())
	{
		$arButtons = CIBlock::GetPanelButtons(
			$arParams["IBLOCK_ID"],
			$arItem["ID"],
			0,
			array("SECTION_BUTTONS"=>false, "SESSID"=>false)
		);
		$arItem["EDIT_LINK"] = $arButtons["edit"]["edit_element"]["ACTION_URL"];
		$arItem["DELETE_LINK"] = $arButtons["edit"]["delete_element"]["ACTION_URL"];

		$arResult['ITEMS'][] = $arItem;
	}

	if(count($arResult['ITEMS'])<=0)
	{
		$this->AbortResultCache();
		@define("ERROR_404", "Y");
		return;
	}

	//include template
	$this->IncludeComponentTemplate();

}