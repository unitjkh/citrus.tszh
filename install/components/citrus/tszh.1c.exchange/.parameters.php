<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
	"PARAMETERS" => array(
		"FILE_SIZE_LIMIT" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("1C_EXCHANGE_FILE_SIZE_LIMIT"),
			"TYPE" => "STRING",
			"DEFAULT" => '',
		),
		"SKIP_PERMISSION_CHECK" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("1C_EXCHANGE_SKIP_PERMISSION_CHECK"),
			"TYPE" => "CHECKBOX",
			"DEFAULT" => '',
		),
	),
);
?>
