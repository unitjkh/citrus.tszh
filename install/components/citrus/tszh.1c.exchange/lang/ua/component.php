<?
$MESS["TSZH_1C_ERROR_SESSION_ID_CHANGE"] = "Увімкнена зміна ідентифікатора сесій. У файлі підключення компоненту обміну, до підключення прологу визначіть константу BX_SESSION_ID_CHANGE: define('BX_SESSION_ID_CHANGE', false);";
$MESS["TSZH_1C_ERROR_AUTHORIZE"] = "Помилка авторизації. Не переданий ідентифікатор сесії або сесія скінчилася";
$MESS["TSZH_1C_ERROR_LOGIN"] = "Помилка авторизації. Будь ласка перевірте логін і пароль";
$MESS["TSZH_1C_ERROR_MODULE"] = "Модуль \"ТСЖ\" не встановлений.";
$MESS["TSZH_1C_ERROR_IBLOCK_MODULE"] = "Модуль \"Інформаційні блоки\" не встановлений.";
$MESS["TSZH_1C_ERROR_TICKETS_MODULE"] = "Модуль \"ТСЖ: Аварійно-диспечерська служба\" не встановлений";
$MESS["TSZH_1C_ERROR_HTTP_READ"] = "Помилка читання HTTP даних.";
$MESS["TSZH_1C_ERROR_UNKNOWN_COMMAND"] = "Невідома команда.";
$MESS["TSZH_1C_NO_ORDERS_IN_IMPORT"] = "В CML не знайдені замовлення.";
$MESS["TSZH_1C_ERROR_DIRECTORY"] = "Помилковий параметр - тимчасовий каталог.";
$MESS["TSZH_1C_ERROR_FILE_WRITE"] = "Помилка запису в файл #FILE_NAME#.";
$MESS["TSZH_1C_ERROR_FILE_OPEN"] = "Помилка відкриття файлу #FILE_NAME# для запису.";
$MESS["TSZH_1C_ERROR_INIT"] = "Помилка ініціалізації тимчасового каталогу.";
$MESS["TSZH_1C_ZIP_ERROR"] = "Помилка розпакування архіву.";
$MESS["TSZH_1C_EMPTY_CML"] = "Файл для імпорта пустий.";
$MESS["TSZH_1C_UNZIP_ERROR"] = "Розпакування на сайті не можливе. Відправте незапакований файл";
$MESS["TSZH_1C_ERROR_ORG_NOT_FOUND"] = "Організація з вказаним ІПН не знайдена на сайті.";
$MESS["TSZH_1C_METER_EXPORT_ERROR"] = "Помилка вивантаження показів лічильників.";
$MESS["TSZH_1C_ACCEXISTENCE_EXPORT_ERROR"] = "Помилка вивантаження параметрів доступу власників особових рахунків. ";
$MESS["TSZH_1C_QUESTIONS_EXPORT_IBLOCK_ERROR"] = "Не знайдений інфоблок Питань-відповідей";
$MESS["TSZH_1C_TICKET_EXPORT_INIT_ERROR"] = "Помилка ініціалізації експорту звернень";
$MESS["TSZH_1C_IMPORT_FILE_NOT_FOUND"] = "Помилка імпорту, вказаний файл імпорту не знайдений";
$MESS["TI_STEP1_DONE"] = "Тимчасові таблиці видалені";
$MESS["TI_STEP2_ERROR"] = "Помилка створення тимчасових таблиць";
$MESS["TI_STEP2_DONE"] = "Тимчасові таблиці створені";
$MESS["TI_STEP3_ERROR"] = "Помилка читання файла у тимчасові таблиці";
$MESS["TI_STEP3_PROGRESS"] = "Завантаження даних в тимчасові таблиці: #PROGRESS#%";
$MESS["TI_STEP4_ERROR"] = "Помилка індексації тимчасових таблиць";
$MESS["TI_STEP4_DONE"] = "Тимчасові таблиці проіндексовані";
$MESS["TI_STEP5_ERROR"] = "Помилка завантаження перекладу";
$MESS["TI_STEP5_DONE"] = "Дані періода та організації завантажені";
$MESS["TI_STEP6_DONE"] = "Особові рахунки завантажені";
$MESS["TI_STEP6_PROGRESS"] = "Завантажено особових рахунків:#CNT# з #TOTAL#";
$MESS["TI_STEP7_DONE"] = "Видалення/деактивація особових рахунків та лічильників завершена";
$MESS["TI_STEP7_PROGRESS"] = "Видалено/деактивовано особових рахунків та лічильників:#CNT#";
$MESS["TI_STEP8_DONE"] = "Завантаження завершене";
$MESS["TI_STEP8_PROGRESS"] = "Демонстраційні особові рахунки видалені";
$MESS["TI_ERROR_ALREADY_FINISHED"] = "Завантаження було завершене";
$MESS["TSZH_1C_ERROR_PAYMENT_MODULE"] = "Модуль «1С: Сайт ЖКГ. Оплата» не встановлено.";
$MESS["BI_STEP5_DONE"] = "Довідник характеристик будинку завантажений";
$MESS["BI_STEP6_DONE"] = "будинки завантажені";
$MESS["BI_STEP6_PROGRESS"] = "Завантажено будинків : #CNT# з#TOTAL#";
$MESS["BI_STEP7_DONE"] = "Завантаження завершено";
$MESS["BI_SHOWN_FIRST_ERRORS"] = "показані перші";
$MESS["BI_SHOWN_ERRORS1"] = "помилка";
$MESS["BI_SHOWN_ERRORS2"] = "помилки";
$MESS["BI_SHOWN_ERRORS5"] = "помилок";
$MESS["BI_HOUSE_NUMBER"] = "будинок №";
$MESS["BI_WRONG_HOUSE_PROPERTY_CODE"] = "Невірно заданий код властивості '#NAME# '";
$MESS["BI_HOUSE_PROPERTY_UPDATE_ERROR"] = "Помилка при зміні властивості #CODE#  інфоблоків будинків : #ERROR_TEXT#";
$MESS["BI_HOUSE_PROPERTY_ADD_ERROR"] = "Помилка при додаванні властивості #CODE# в інфоблок будинків : #ERROR_TEXT#";
$MESS["BI_WRONG_XML_ID"] = "Невірно вказано ідентифікатор будинку (атрибут ID)";
$MESS["BI_WRONG_HOUSE"] = "Невірно вказано номер будинку (атрибут house)";
$MESS["BI_WRONG_STREET"] = "Невірно вказана вулиця будинку  (атрибут street)";
$MESS["BI_HOUSE_UPDATE_ERROR"] = "Помилка при зміні будинку з ID = '#XML_ID#' :#ERROR_TEXT#";
$MESS["BI_HOUSE_ADD_ERROR"] = "Помилка при додаванні вдома з ID = '#XML_ID#' :#ERROR_TEXT#";
$MESS["BI_STREET_ADD_ERROR"] = "Помилка при додаванні розділу (вулиці ) #STREET# в інфоблок будинків : #ERROR_TEXT#";
$MESS["BDI_STEP5_DONE"] = "Підготовчі дії виконані";
$MESS["BDI_STEP6_DONE"] = "документи завантажені";
$MESS["BDI_STEP6_PROGRESS"] = "Завантажено документів: #CNT# з #TOTAL#";
$MESS["BDI_STEP7_DONE"] = "Завантаження завершено";
$MESS["BDI_WRONG_BUILDING"] = "Невірно вказано ідентифікатор будинку ( атрибут building)";
$MESS["BDI_HOUSE_NOT_FOUND"] = "Чи не знайдено будинок документа";
$MESS["BDI_WRONG_TITLE"] = "Невірно вказано назву документа ( атрибут title)";
$MESS["BDI_WRONG_FILENAME"] = "Невірно вказано ім'я файлу документа ( атрибут filename)";
$MESS["BDI_DOCUMENT_UPDATE_ERROR"] = "Помилка при зміні документа \"# XML_ID#' : #ERROR_TEXT#";
$MESS["BDI_DOCUMENT_ADD_ERROR"] = "Помилка при додаванні документа \"# XML_ID#' :#ERROR_TEXT#";
?>