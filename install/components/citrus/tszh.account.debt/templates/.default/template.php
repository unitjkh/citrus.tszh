<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="account-info">
    <div class="account-info__main">
        <div class="account-info__text">
            <p><? echo GetMessage('TAD_ACCOUNT', array('#URL#'=>$arParams["PATH_PERSONAL_INFO"],'#ACCOUNT#'=>$arResult["ACCOUNT"])).' ('.$arResult["ADDRESS"];?>)</p>
            <p>
                <?if(isset($arResult["DATE_PAYED"]))
                    echo GetMessage('TAD_LAST_PAYMENT', array('#DATE#'=>$arResult["DATE_PAYED"],'#SUM#'=>$arResult["SUMM_PAYED"]));
                ?>
            </p>
        </div>
    </div>
    <? if(!is_null($arResult['SUMM'])):?>
        <div class="account-info__pay">
            <div class="account-info__pay-title"><? echo GetMessage('TAD_SUMM_PAYMENT').$arResult["SUMM"]?></div>
            <a class="account-info__pay-button" href="<?=$arParams["PATH_PERSONAL_PAY"].'?summ=' . round($arResult['SUMM'])?>"><?=GetMessage('TAD_PAY')?></a>
        </div>
    <?endif;?>
</div>