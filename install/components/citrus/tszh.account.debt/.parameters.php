<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
	"PARAMETERS" => array(
		"PATH_PERSONAL_PAY" => Array(
			"NAME" => GetMessage("PATH_PERSONAL_PAY"),
			"TYPE" => "STRING",
			"DEFAULT" => "#SITE_DIR#cabinet/payment",
		),
        "PATH_PERSONAL_INFO" => Array(
            "NAME" => GetMessage("PATH_PERSONAL_INFO"),
            "TYPE" => "STRING",
            "DEFAULT" => "#SITE_DIR#cabinet/info",
        ),
	),
);
?>
