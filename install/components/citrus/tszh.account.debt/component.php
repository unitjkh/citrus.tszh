<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (!CModule::IncludeModule('citrus.tszh'))
{
	ShowError(GetMessage("TSZH_MODULE_NOT_FOUND_ERROR"));
	return;
}

if (!CModule::IncludeModule('citrus.tszhpayment'))
{
	return;
}
//$obUser = new CUser;
//$obUser->Update(20, array("PASSWORD"=>'123456',"CONFIRM_PASSWORD"=>'123456'), true);

if (!CTszhFunctionalityController::CheckEdition())
	return;

if (!$USER->IsAuthorized()) {
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
    return;
}

if (!CTszh::IsTenant()) {
    //$APPLICATION->AuthForm(GetMessage("TSZH_NOT_A_MEMBER"));
    return;
}

if(defined('PATH_PERSONAL'))
    define('PATH_PERSONAL','personal');
foreach(array(array('PATH_PERSONAL_PAY',"#SITE_DIR#".PATH_PERSONAL."/payment"),array('PATH_PERSONAL_INFO',"#SITE_DIR#".PATH_PERSONAL."/info")) as $item)
{
    $arParams[$item[0]] = trim($arParams[$item[0]]);
    $arParams[$item[0]] = str_replace("#SITE_DIR#", SITE_DIR, strlen($arParams[$item[0]]) <= 0 ? $item[1]: $arParams[$item[0]]);
}

$arResult = Array();
$arElement = CTszhAccount::GetList(array(), array("USER_ID"=>$USER->GetID()), false, array("nTopCount"=>1), array("XML_ID","REGION","CITY","SETTLEMENT","DISTRICT","STREET","HOUSE","FLAT", "FLAT_ABBR"))->GetNext();
if(is_array($arElement)) {
    $arResult["ADDRESS"] = CTszhAccount::GetFullAddress($arElement, Array("REGION", "CITY", "SETTLEMENT", "DISTRICT"));
    $arResult['ACCOUNT'] = $arElement['XML_ID'];
}
$arElement = CTszhPayment::GetList(array("DATE_PAYED"=>"DESC"), array("USER_ID"=>$USER->GetID(),"PAYED"=>"Y"), false, array("nTopCount"=>1), array("DATE_PAYED","SUMM_PAYED"))->GetNext();
if(is_array($arElement)){
    $arElement["DATE_PAYED"] = new DateTime($arElement["DATE_PAYED"]);
    $arResult["DATE_PAYED"] = $arElement["DATE_PAYED"]->format("d.m.Y");
    $arResult['SUMM_PAYED'] = CTszhPublicHelper::FormatCurrency($arElement['SUMM_PAYED']);
}

$arElement = CTszhPayment::GetList(array("DATE_INSERT"=>"DESC"), array("USER_ID"=>$USER->GetID(),"PAYED"=>"N"), false, array("nTopCount"=>1), array("SUMM"))->GetNext();
if(is_array($arElement)){
    if(!is_null($arElement['SUMM']))
        $arResult['SUMM'] = CTszhPublicHelper::FormatCurrency($arElement['SUMM']);
}
$this->IncludeComponentTemplate();
?>