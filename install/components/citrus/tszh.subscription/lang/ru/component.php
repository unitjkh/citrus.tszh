<?
$MESS["TSZH_MODULE_NOT_FOUND_ERROR"] = "Модуль «1С:Сайт ЖКХ» не установлен.";
$MESS["C_ERROR_TSZH_EDITION"] = "В текущей редакции модуля «1С:Сайт ЖКХ» функционал рассылок не доступен.";
$MESS["TSZH_NOT_A_MEMBER"] = "Вы не являетесь владельцем лицевого счёта.";

$MESS["C_ERROR_SET_SUBSCRIPTION"] = "Ошибка при сохранении подписки: #ERROR#";
$MESS["C_CHANGES_SAVED"] = "Изменения сохранены.";

$MESS["C_NOTIFY_DUMMY_MAIL"] = "Для получения рассылок укажите e-mail в <a href=\"#URL#\">профиле</a>.";
