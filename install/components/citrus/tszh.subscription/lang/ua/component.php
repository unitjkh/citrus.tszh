<?
$MESS["TSZH_MODULE_NOT_FOUND_ERROR"] = "Модуль «1С:Сайт ЖКГ» не встановлено.";
$MESS["C_ERROR_TSZH_EDITION"] = "У поточній редакції модуля «1С:Сайт ЖКГ» функціонал розсилок не доступний.";
$MESS["TSZH_NOT_A_MEMBER"] = "Ви не є власником особового рахунку.";

$MESS["C_ERROR_SET_SUBSCRIPTION"] = "Помилка при збереженні підписки: #ERROR#";
$MESS["C_CHANGES_SAVED"] = "Зміни збережені.";

$MESS["C_NOTIFY_DUMMY_MAIL"] = "Для отримання розсилок вкажіть, будь ласка, у Вашому <a href=\"#URL#\">профілі</a> коректний e-mail.";