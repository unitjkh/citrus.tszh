<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<div class="tszh_subscription">
<?if (strlen($arResult["NOTE"])):?>
	<div class="errortext"><?=$arResult["NOTE"]?></div>
<?endif;

$msgCode = $this->__component->__name;
if (is_set($_SESSION[$msgCode]) && strlen($_SESSION[$msgCode]))
{
	?><div class="message-ok"><?ShowMessage(array("TYPE"=>"OK", "MESSAGE"=>$_SESSION[$msgCode]))?></div><?
	unset($_SESSION[$msgCode]);
}

if (empty($arResult["ITEMS"])):?>
	<?ShowMessage(array("TYPE"=>"OK", "MESSAGE"=>GetMessage("T_NONE_SUBSCRIBES")))?>
<?else:?>
	<form method="POST" action="<?=POST_FORM_ACTION_URI?>">
   		<?=bitrix_sessid_post()?>
   		<input type="hidden" name="citrus_tszh_subscription_component" value="Y">

		<table class="data-table">
			<tr>
				<th><?=GetMessage("T_SUBSCRIBE")?></th>
				<th><?=GetMessage("T_SUBSCRIPTION_STATE")?></th>
			</tr>

			<?foreach ($arResult["ITEMS"] as $code => $arSubscribe):?>
				<tr>
					<td>
						<div class="subscribe-name"><label for="tszh-subscribe-<?=$code?>-checkbox"><?=$arSubscribe["NAME"]?></label></div>
						<div class="subscribe-descr"><?=$arSubscribe["DESCR"]?></div>
					</td>

					<td class="checkbox-td">
						<input type="checkbox" name="<?=$code?>" value="Y" id="tszh-subscribe-<?=$code?>-checkbox" <?=($arSubscribe["USER_SUBSCRIBED"] ? "checked" : "")?>>
					</td>
				</tr>
			<?endforeach?>

			<tr>
				<td colspan="2" class="button-td"><input type="submit" name="save" value="<?=GetMessage("T_SAVE")?>"></td>
			</tr>

		</table>
	</form>
<?endif?>
</div>