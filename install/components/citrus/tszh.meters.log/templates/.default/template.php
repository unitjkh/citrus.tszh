<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (empty($arResult['METERS']))
{
	ShowNote(GetMessage("CITRUS_TSZH_METER_VALUES_NOT_FOUND"));
	return;
}

if ($arParams["DISPLAY_TOP_PAGER"])
	echo "<div>{$arResult['NAV_STRING']}</div>\n";

?>
<? foreach ($arResult["ROWS"] as $date => $arItem) : ?>
	<h3><?= $date ?></h3>
	<table class="data-table">
	<tr>
		<th <?= (($arResult["MAX_VALUES_COUNT"] > 1) ? "rowspan=\"2\"" : "") ?>>
			��������
		</th>
		<th colspan="<?= $arResult["MAX_VALUES_COUNT"] ?>">
			���������
		</th>
	</tr>
	<? if ($arResult["MAX_VALUES_COUNT"] > 1) : ?>
		<tr>
			<? for ($i = 1; $i <= $arResult["MAX_VALUES_COUNT"]; $i++) : ?>
				<th>
					����� <?= $i ?>
				</th>
			<? endfor; ?>
		</tr>
	<? endif; ?>
	<? foreach ($arItem as $arMeter) : ?>
		<tr>
			<td><?= $arMeter["SERVICE_NAME"] . " (" . $arMeter["NAME"] . ")" ?></td>
			<? for ($i = 1; $i <= $arResult["MAX_VALUES_COUNT"]; $i++) : ?>
				<td class="cost"><?= $arMeter["VALUE" . $i] ?></td>
			<? endfor; ?>
		</tr>
	<? endforeach; ?>
	</table>
<? endforeach; ?>

<?
if ($arParams["DISPLAY_BOTTOM_PAGER"])
	echo "<div>{$arResult['NAV_STRING']}</div>\n";
?>