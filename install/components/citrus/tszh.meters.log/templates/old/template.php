<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if (empty($arResult['METERS']))
{
	ShowNote(GetMessage("CITRUS_TSZH_METER_VALUES_NOT_FOUND"));
	return;
}

if($arParams["DISPLAY_TOP_PAGER"])
	echo "<div>{$arResult['NAV_STRING']}</div>\n";

?>
<table class="data-table">
<thead>
	<th><?=GetMessage("CITRUS_TSZH_DATE")?></th>
	<?
	foreach ($arResult['METERS'] as $arMeter)
	{
		for ($i = 1; $i <= $arMeter['VALUES_COUNT']; $i++)
		{
			?>
			<th><?=$arMeter['NAME']?><?=($arMeter["VALUES_COUNT"] > 1 ? GetMessage("CITRUS_TSZH_METER_TARIF", Array("#N#" => $i)) : '')?></th>
			<?
		}
	}
	?>
</thead>
<tbody>
<?
foreach ($arResult["ROWS"] as $date=>$arItem)
{
	?>
	<tr>
		<td><?=$date?></td>
		<?
		foreach ($arResult['METERS'] as $arMeter)
		{
			for ($i = 1; $i <= $arMeter['VALUES_COUNT']; $i++)
			{
				$value = is_set($arItem, $arMeter['ID']) && is_set($arItem[$arMeter['ID']], "VALUE$i") ? $arItem[$arMeter["ID"]]["VALUE$i"] : '';
				?>
				<td class="cost"><?=$value?></td>
				<?
			}
		}
		?>
	</tr>
	<?
}

?>
</tbody>
</table>
<?

if($arParams["DISPLAY_BOTTOM_PAGER"])
	echo "<div>{$arResult['NAV_STRING']}</div>\n";

?>