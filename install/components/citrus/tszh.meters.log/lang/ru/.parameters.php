<?
$MESS["CITRUS_TSZH_ITEMS_LIMIT"] = "Максимальное количество записей на одной странице";
$MESS["CITRUS_TSZH_NAV_PAGER"] = "Показания";
$MESS["CITRUS_TSZH_MODIFIED_BY_OWNER"] = "Отображать показания";
$MESS["CITRUS_ALL"] = "(все)";
$MESS["CITRUS_MODIFIED_BY_OWNER_Y"] = "введенные владельцем лицевого счета";
$MESS["CITRUS_MODIFIED_BY_OWNER_N"] = "заполненные другими (из 1С, администраторами)";
$MESS["CITRUS_TSZH_FILTER_NAME"] = "Имя переменной, содержащей фильтр";
?>