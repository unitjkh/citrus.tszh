<?
$MESS["CITRUS_TSZH_ITEMS_LIMIT"] = "Максимальна кількість записів на одній сторінці";
$MESS["CITRUS_TSZH_NAV_PAGER"] = "Показання";
$MESS["CITRUS_TSZH_MODIFIED_BY_OWNER"] = "Відображати показання ";
$MESS["CITRUS_ALL"] = "( усі)";
$MESS["CITRUS_MODIFIED_BY_OWNER_Y"] = "введені власником особового рахунку";
$MESS["CITRUS_MODIFIED_BY_OWNER_N"] = "заповнені іншими ( з 1С , адміністраторами )";
$MESS["CITRUS_TSZH_FILTER_NAME"] = "Ім'я змінної, що містить фільтр";
?>