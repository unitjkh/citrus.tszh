<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!CModule::IncludeModule('citrus.tszh'))
{
	ShowError(GetMessage("CITRUS_TSZH_MODULE_NOT_FOUND_ERROR"));
	return;
}

if (!CTszhFunctionalityController::CheckEdition())
	return;

// ���� ������������ �� ����������� � ������ ���������� ��ƻ
// ������� ��������� � �������� ������ ����������
if (!$USER->IsAuthorized() || !CTszh::IsTenant())
{
	$this->AbortResultCache();
	$APPLICATION->AuthForm(GetMessage("CITRUS_TSZH_NOT_A_MEMBER"));
	return;
}

CPageOption::SetOptionString("main", "nav_page_in_session", "N");

// set default value for missing parameters, simple param check
$componentParams = CComponentUtil::GetComponentProps($this->getName());
if (is_array($componentParams))
{
	foreach ($componentParams["PARAMETERS"] as $paramName => $paramArray)
	{
		if (!is_set($arParams, $paramName) && is_set($paramArray, "DEFAULT"))
			$arParams[$paramName] = $paramArray["DEFAULT"];

		$paramArray["TYPE"] = ToUpper(is_set($paramArray, "TYPE") ? $paramArray["TYPE"] : "STRING");
		switch ($paramArray["TYPE"]) {
			case 'INT':
				$arParams[$paramName] = IntVal($arParams[$paramName]);
				break;

			case 'LIST':
				if (!array_key_exists($arParams[$paramName], $paramArray['VALUES']))
					$arParams[$paramName] = $paramArray["DEFAULT"];
				break;

			case 'CHECKBOX':
				$arParams[$paramName] = ($arParams[$paramName] == (is_set($paramArray, 'VALUE') ? $paramArray['VALUE'] : 'Y'));
				break;

			default:
				// string etc.
				break;
		}
	}
}

if(strlen($arParams["FILTER_NAME"])<=0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
	$arrFilter = array();
else
{
	$arrFilter = $GLOBALS[$arParams["FILTER_NAME"]];
	if(!is_array($arrFilter))
		$arrFilter = array();
}


if($arParams["DISPLAY_TOP_PAGER"] || $arParams["DISPLAY_BOTTOM_PAGER"])
{
	$arNavParams = array(
		"nPageSize" => $arParams["ITEMS_LIMIT"],
		"bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
		"bShowAll" => $arParams["PAGER_SHOW_ALL"],
	);
	$arNavigation = CDBResult::GetNavParams($arNavParams);
	if($arNavigation["PAGEN"]==0 && $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]>0)
		$arParams["CACHE_TIME"] = $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"];
}
else
{
	$arNavParams = array(
		"nTopCount" => $arParams["ITEMS_LIMIT"],
		"bDescPageNumbering" => $arParams["PAGER_DESC_NUMBERING"],
	);
	$arNavigation = false;
}

// ������� ���� �������� ������������
$arResult['ACCOUNT'] = CTszhAccount::GetByUserID($USER->GetID());
if (!is_array($arResult['ACCOUNT']))
{
	$this->AbortResultCache();
	$APPLICATION->AuthForm(GetMessage("CITRUS_TSZH_NOT_A_MEMBER"));
	return;
}


$arSort= array("TIMESTAMP_X" => "DESC", "ID" => "DESC");
$arFilter = array_merge(array("ACCOUNT_ID" => $arResult["ACCOUNT"]["ID"], "METER_ACTIVE" => "Y", "!METER_HOUSE_METER" => "Y"), $arrFilter);
if ($arParams["MODIFIED_BY_OWNER"])
	$arFilter["MODIFIED_BY_OWNER"] = $arParams["MODIFIED_BY_OWNER"];
$arSelect = Array("ID", "METER_ID", "VALUE1", "VALUE2", "VALUE3", "VALUES_COUNT", "TIMESTAMP_X", "NAME", "SERVICE_NAME", "MODIFIED_BY_OWNER", "MODIFIED_BY");

$rsDates = CTszhMeterValue::GetList($arSort, $arFilter, Array("TIMESTAMP_X"), $arNavParams, $arSelect);
$arDates = Array();
while ($ar = $rsDates->GetNext())
	$arDates[] = $ar['TIMESTAMP_X'];

$arFilter["TIMESTAMP_X"] = $arDates;
$rsValues = CTszhMeterValue::GetList($arSort, $arFilter, false, false, $arSelect);
$arResult['ROWS'] = $arNames = Array();
while ($arValue = $rsValues->GetNext())
{
	$date = $arValue["TIMESTAMP_X"];
	if (CModule::IncludeModule('iblock'))
	{
		if (substr($date, -strlen(' 00:00:00')) == ' 00:00:00')
		{
			$date = substr($date, 0, strlen($date) - strlen(' 00:00:00'));
			$timeStamp = MakeTimeStamp($date);
			if (strpos($date, '01.') === 0)
				$date = GetMessage("CITRUS_BEGINNING") . ToLower(CIBlockFormatProperties::DateFormat($DB->DateFormatToPHP('F Y'), $timeStamp));
			elseif (strpos($date, '31.') === 0 || strpos($date, '30.') === 0 || strpos($date, '29.') === 0 || strpos($date, '28.') === 0)
				$date = GetMessage("CITRUS_ENDING") . ToLower(CIBlockFormatProperties::DateFormat($DB->DateFormatToPHP('F Y'), $timeStamp));
			else
				$date = ToLower(CIBlockFormatProperties::DateFormat($DB->DateFormatToPHP('d f Y'), $timeStamp));
		}
		else
		{
			$timeStamp = MakeTimeStamp($date);
			$date = ToLower(CIBlockFormatProperties::DateFormat($DB->DateFormatToPHP('d F Y H:i'), $timeStamp));
		}
	}
	if (!(array_key_exists($date, $arResult['ROWS']) && array_key_exists($arValue["METER_ID"], $arResult['ROWS'][$date])))
	{
		$arResult['ROWS'][$date][$arValue["METER_ID"]] = $arValue;

		$name = empty($arValue["SERVICE_NAME"]) ? $arValue["NAME"] : $arValue["SERVICE_NAME"];
		if (!empty($arValue["SERVICE_NAME"]))
		{
			if (array_key_exists($name, $arNames) && $arNames[$name] != $arValue["METER_ID"])
			{
				$preMeterID = $arNames[$name];
				$arPrevMeter = $arResult["METERS"][$preMeterID];
				$arResult["METERS"][$preMeterID]["NAME"] = $arPrevMeter["NAME"] . " (" . $arPrevMeter["METER_NAME"] . ")";
				$name .= " (" . $arValue['NAME'] . ")";
			}
			else
				$arNames[$name] = $arValue['METER_ID'];
		}
		$arResult["METERS"][$arValue['METER_ID']] = Array(
			"ID" => $arValue["METER_ID"],
			"NAME" => $name,
			"METER_NAME" => $arValue['NAME'],
			"VALUES_COUNT" => $arValue["VALUES_COUNT"],
		);
	}
}

$arResult["NAV_STRING"] = $rsDates->GetPageNavStringEx($navComponentObject, $arParams["PAGER_TITLE"], $arParams["PAGER_TEMPLATE"], $arParams["PAGER_SHOW_ALWAYS"]);
$this->IncludeComponentTemplate();

?>