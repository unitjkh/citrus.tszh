<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("citrus.tszh"))
	return;

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(

		"ITEMS_LIMIT" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("CITRUS_TSZH_ITEMS_LIMIT"),
			"TYPE" => "STRING",
			"DEFAULT" => "10",
		),
		"MODIFIED_BY_OWNER" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("CITRUS_TSZH_MODIFIED_BY_OWNER"),
			"TYPE" => "LIST",
			"DEFAULT" => "",
			"VALUES" => Array(
				'' => GetMessage("CITRUS_ALL"),
				'Y' => GetMessage("CITRUS_MODIFIED_BY_OWNER_Y"),
				'N' => GetMessage("CITRUS_MODIFIED_BY_OWNER_N"),
			),
		),
		"FILTER_NAME" => array(
			"NAME" => GetMessage("CITRUS_TSZH_FILTER_NAME"),
			"TYPE" => "STRING",
			"DEFAULT" => "",
			"PARENT" => "DATA",
		),
	),
);

if (CModule::IncludeModule("iblock"))
	CIBlockParameters::AddPagerSettings($arComponentParameters, GetMessage("CITRUS_TSZH_NAV_PAGER"), true, true);

?>