<?
$MESS["CITRUS_TSZH_ID_TITLE"] = "Идентификатор счетчика";
$MESS["CITRUS_TSZH_MODIFIED_BY_OWNER"] = "Отображать показания";
$MESS["CITRUS_ALL"] = "(все)";
$MESS["CITRUS_MODIFIED_BY_OWNER_Y"] = "введенные владельцем лицевого счета";
$MESS["CITRUS_MODIFIED_BY_OWNER_N"] = "заполненные другими (из 1С, администраторами)";
$MESS["CITRUS_TSZH_FILTER_NAME"] = "Имя переменной, содержащей фильтр";
$MESS["CITRUS_TSZH_COUNT_METERS_HISTORY"] = "Количество показаний счетчиков на одной странице";
?>