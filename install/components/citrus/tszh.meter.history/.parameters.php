<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

if(!CModule::IncludeModule("citrus.tszh"))
	return;

$arComponentParameters = array(
	"GROUPS" => array(
	),
	"PARAMETERS" => array(
		"MODIFIED_BY_OWNER" => Array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("CITRUS_TSZH_MODIFIED_BY_OWNER"),
			"TYPE" => "LIST",
			"DEFAULT" => "",
			"VALUES" => Array(
				'' => GetMessage("CITRUS_ALL"),
				'Y' => GetMessage("CITRUS_MODIFIED_BY_OWNER_Y"),
				'N' => GetMessage("CITRUS_MODIFIED_BY_OWNER_N"),
			),
		),
		"COUNT_METERS_HISTORY" => Array(
			"PARENT" => "METERS_HISTORY",
			"NAME" => GetMessage("CITRUS_TSZH_COUNT_METERS_HISTORY"),
			"TYPE" => "LIST",
			"DEFAULT" => "6",
			"VALUES" => Array(
				"3" => 3,
				"6" => 6,
				"12" => 12,
			),
		),
		"FILTER_NAME" => array(
			"NAME" => GetMessage("CITRUS_TSZH_FILTER_NAME"),
			"TYPE" => "STRING",
			"DEFAULT" => "",
			"PARENT" => "DATA",
		),
		"ID" => array(
			"NAME" => GetMessage("CITRUS_TSZH_ID_TITLE"),
			"TYPE" => "STRING",
			"DEFAULT" => "",
			"PARENT" => "DATA",
		),
	),
);

?>