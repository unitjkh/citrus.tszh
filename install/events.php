<?php

// ������ ����� � �������� �������� �� ���������
// ���� FIELDX_NAME � FIELDX_VALUE ��������� ��� ������������� � ������� �������� ������, ������� 15.0.7
$arEventTypes = array(
	"QUESTIONS_FORM" => array(
		array(
			"SUBJECT" => GetMessage("QUESTIONS_FORM_SUBJECT1"),
			"MESSAGE" => GetMessage("QUESTIONS_FORM_MESSAGE1"),
		),
		array(
			"EMAIL_TO" => "#MAIL#",
			"SUBJECT" => GetMessage("QUESTIONS_FORM_SUBJECT2"),
			"MESSAGE" => GetMessage("QUESTIONS_FORM_MESSAGE2"),
		),
	),
	"QUESTIONS_ANSWER" => array(
		array(
			"EMAIL_TO" => "#MAIL#",
			"SUBJECT" => GetMessage("QUESTIONS_ANSWER_SUBJECT"),
			"MESSAGE" => GetMessage("QUESTIONS_ANSWER_MESSAGE"),
		),
	),
	"TSZH_CONFIRM_EMAIL" => array(
		array(
			"EMAIL_TO" => "#USER_EMAIL#",
			"SUBJECT" => GetMessage("TSZH_CONFIRM_EMAIL_SUBJECT"),
			"MESSAGE" => GetMessage("TSZH_CONFIRM_EMAIL_MESSAGE"),
			"BODY_TYPE" => "text",
		),
	),
	"TSZH_FEEDBACK_FORM" => array(
		array(
			"REPLY_TO" => "#AUTHOR_EMAIL#",
			"SUBJECT" => GetMessage("TSZH_FEEDBACK_FORM_SUBJECT"),
			"MESSAGE" => GetMessage("TSZH_FEEDBACK_FORM_MESSAGE"),
			"BODY_TYPE" => "text",
			"ADDITIONAL_FIELD" => array(
				array("NAME" => "Sender", "VALUE" => "#HEADER_SENDER#"),
			),
			"FIELD1_NAME" => "Sender",
			"FIELD1_VALUE" => "#HEADER_SENDER#",
		),
	),
	"TSZH_BEFORE_TSZH_DEMO_EXPIRED_NOTICE" => array(
		array(
			"SITE_TEMPLATE_ID" => "citrus_tszh_subscribe",
			"EMAIL_FROM" => "#EMAIL_FROM#",
			"EMAIL_TO" => "#EMAIL_TO#",
			"SUBJECT" => GetMessage("TSZH_BEFORE_TSZH_DEMO_EXPIRED_NOTICE_SUBJECT"),
			"MESSAGE" => GetMessage("TSZH_BEFORE_TSZH_DEMO_EXPIRED_NOTICE_MESSAGE"),
			"BODY_TYPE" => "html",
			"ADDITIONAL_FIELD" => array(
				array("NAME" => "Sender", "VALUE" => "#HEADER_SENDER#"),
				array("NAME" => "List-Unsubscribe", "VALUE" => "<#UNSUBSCRIBE_URL#>"),
			),
			"FIELD1_NAME" => "Sender",
			"FIELD1_VALUE" => "#HEADER_SENDER#",
			"FIELD2_NAME" => "List-Unsubscribe",
			"FIELD2_VALUE" => "<#UNSUBSCRIBE_URL#>",
		),
	),
	"TSZH_AFTER_TSZH_DEMO_EXPIRED_NOTICE" => array(
		array(
			"SITE_TEMPLATE_ID" => "citrus_tszh_subscribe",
			"EMAIL_FROM" => "#EMAIL_FROM#",
			"EMAIL_TO" => "#EMAIL_TO#",
			"SUBJECT" => GetMessage("TSZH_AFTER_TSZH_DEMO_EXPIRED_NOTICE_SUBJECT"),
			"MESSAGE" => GetMessage("TSZH_AFTER_TSZH_DEMO_EXPIRED_NOTICE_MESSAGE"),
			"BODY_TYPE" => "html",
			"ADDITIONAL_FIELD" => array(
				array("NAME" => "Sender", "VALUE" => "#HEADER_SENDER#"),
				array("NAME" => "List-Unsubscribe", "VALUE" => "<#UNSUBSCRIBE_URL#>"),
			),
			"FIELD1_NAME" => "Sender",
			"FIELD1_VALUE" => "#HEADER_SENDER#",
			"FIELD2_NAME" => "List-Unsubscribe",
			"FIELD2_VALUE" => "<#UNSUBSCRIBE_URL#>",
		),
	),
	"TSZH_ADD_PAY_SYSTEM_NOTICE" => array(
		array(
			"SITE_TEMPLATE_ID" => "citrus_tszh_subscribe",
			"EMAIL_FROM" => "#EMAIL_FROM#",
			"EMAIL_TO" => "#EMAIL_TO#",
			"SUBJECT" => GetMessage("TSZH_ADD_PAY_SYSTEM_NOTICE_SUBJECT"),
			"MESSAGE" => GetMessage("TSZH_ADD_PAY_SYSTEM_NOTICE_MESSAGE"),
			"BODY_TYPE" => "html",
			"ADDITIONAL_FIELD" => array(
				array("NAME" => "Sender", "VALUE" => "#HEADER_SENDER#"),
				array("NAME" => "List-Unsubscribe", "VALUE" => "<#UNSUBSCRIBE_URL#>"),
			),
			"FIELD1_NAME" => "Sender",
			"FIELD1_VALUE" => "#HEADER_SENDER#",
			"FIELD2_NAME" => "List-Unsubscribe",
			"FIELD2_VALUE" => "<#UNSUBSCRIBE_URL#>",
		),
	),
	"TSZH_IMPORT_ACCOUNTS_NOTICE" => array(
		array(
			"SITE_TEMPLATE_ID" => "citrus_tszh_subscribe",
			"EMAIL_FROM" => "#EMAIL_FROM#",
			"EMAIL_TO" => "#EMAIL_TO#",
			"SUBJECT" => GetMessage("TSZH_IMPORT_ACCOUNTS_NOTICE_SUBJECT"),
			"MESSAGE" => GetMessage("TSZH_IMPORT_ACCOUNTS_NOTICE_MESSAGE"),
			"BODY_TYPE" => "html",
			"ADDITIONAL_FIELD" => array(
				array("NAME" => "Sender", "VALUE" => "#HEADER_SENDER#"),
				array("NAME" => "List-Unsubscribe", "VALUE" => "<#UNSUBSCRIBE_URL#>"),
			),
			"FIELD1_NAME" => "Sender",
			"FIELD1_VALUE" => "#HEADER_SENDER#",
			"FIELD2_NAME" => "List-Unsubscribe",
			"FIELD2_VALUE" => "<#UNSUBSCRIBE_URL#>",
		),
	),
	"TSZH_IMPORT_CHARGES_NOTICE" => array(
		array(
			"SITE_TEMPLATE_ID" => "citrus_tszh_subscribe",
			"EMAIL_FROM" => "#EMAIL_FROM#",
			"EMAIL_TO" => "#EMAIL_TO#",
			"SUBJECT" => GetMessage("TSZH_IMPORT_CHARGES_NOTICE_SUBJECT"),
			"MESSAGE" => GetMessage("TSZH_IMPORT_CHARGES_NOTICE_MESSAGE"),
			"BODY_TYPE" => "html",
			"ADDITIONAL_FIELD" => array(
				array("NAME" => "Sender", "VALUE" => "#HEADER_SENDER#"),
				array("NAME" => "List-Unsubscribe", "VALUE" => "<#UNSUBSCRIBE_URL#>"),
			),
			"FIELD1_NAME" => "Sender",
			"FIELD1_VALUE" => "#HEADER_SENDER#",
			"FIELD2_NAME" => "List-Unsubscribe",
			"FIELD2_VALUE" => "<#UNSUBSCRIBE_URL#>",
		),
	),
	"TSZH_ADD_NEWS_NOTICE" => array(
		array(
			"SITE_TEMPLATE_ID" => "citrus_tszh_subscribe",
			"EMAIL_FROM" => "#EMAIL_FROM#",
			"EMAIL_TO" => "#EMAIL_TO#",
			"SUBJECT" => GetMessage("TSZH_ADD_NEWS_NOTICE_SUBJECT"),
			"MESSAGE" => GetMessage("TSZH_ADD_NEWS_NOTICE_MESSAGE"),
			"BODY_TYPE" => "html",
			"ADDITIONAL_FIELD" => array(
				array("NAME" => "Sender", "VALUE" => "#HEADER_SENDER#"),
				array("NAME" => "List-Unsubscribe", "VALUE" => "<#UNSUBSCRIBE_URL#>"),
			),
			"FIELD1_NAME" => "Sender",
			"FIELD1_VALUE" => "#HEADER_SENDER#",
			"FIELD2_NAME" => "List-Unsubscribe",
			"FIELD2_VALUE" => "<#UNSUBSCRIBE_URL#>",
		),
	),
	"TSZH_NEWS_ITEM" => array(
		array(
			"SITE_TEMPLATE_ID" => "citrus_tszh_subscribe",
			"EMAIL_FROM" => "#EMAIL_FROM#",
			"EMAIL_TO" => "#EMAIL_TO#",
			"SUBJECT" => GetMessage("TSZH_NEWS_ITEM_SUBJECT"),
			"MESSAGE" => GetMessage("TSZH_NEWS_ITEM_MESSAGE"),
			"BODY_TYPE" => "html",
			"ADDITIONAL_FIELD" => array(
				array("NAME" => "Sender", "VALUE" => "#HEADER_SENDER#"),
				array("NAME" => "List-Unsubscribe", "VALUE" => "<#UNSUBSCRIBE_URL#>"),
			),
			"FIELD1_NAME" => "Sender",
			"FIELD1_VALUE" => "#HEADER_SENDER#",
			"FIELD2_NAME" => "List-Unsubscribe",
			"FIELD2_VALUE" => "<#UNSUBSCRIBE_URL#>",
		),
	),
	"TSZH_DEBTOR_NOTICE" => array(
		array(
			"SITE_TEMPLATE_ID" => "citrus_tszh_subscribe",
			"EMAIL_FROM" => "#EMAIL_FROM#",
			"EMAIL_TO" => "#EMAIL_TO#",
			"SUBJECT" => GetMessage("TSZH_DEBTOR_NOTICE_SUBJECT"),
			"MESSAGE" => GetMessage("TSZH_DEBTOR_NOTICE_MESSAGE"),
			"BODY_TYPE" => "html",
			"ADDITIONAL_FIELD" => array(
				array("NAME" => "Sender", "VALUE" => "#HEADER_SENDER#"),
				array("NAME" => "List-Unsubscribe", "VALUE" => "<#UNSUBSCRIBE_URL#>"),
				"FIELD1_NAME" => "Sender",
				"FIELD1_VALUE" => "#HEADER_SENDER#",
				"FIELD2_NAME" => "List-Unsubscribe",
				"FIELD2_VALUE" => "<#UNSUBSCRIBE_URL#>",
			),
		),
	),
	"TSZH_METERS_VALUES_NEED_NOTICE" => array(
		array(
			"SITE_TEMPLATE_ID" => "citrus_tszh_subscribe",
			"EMAIL_FROM" => "#EMAIL_FROM#",
			"EMAIL_TO" => "#EMAIL_TO#",
			"SUBJECT" => GetMessage("TSZH_METERS_VALUES_NEED_NOTICE_SUBJECT"),
			"MESSAGE" => GetMessage("TSZH_METERS_VALUES_NEED_NOTICE_MESSAGE"),
			"BODY_TYPE" => "html",
			"ADDITIONAL_FIELD" => array(
				array("NAME" => "Sender", "VALUE" => "#HEADER_SENDER#"),
				array("NAME" => "List-Unsubscribe", "VALUE" => "<#UNSUBSCRIBE_URL#>"),
			),
			"FIELD1_NAME" => "Sender",
			"FIELD1_VALUE" => "#HEADER_SENDER#",
			"FIELD2_NAME" => "List-Unsubscribe",
			"FIELD2_VALUE" => "<#UNSUBSCRIBE_URL#>",
		),
	),
	"TSZH_METER_VERIFICATION_NOTICE" => array(
		array(
			"SITE_TEMPLATE_ID" => "citrus_tszh_subscribe",
			"EMAIL_FROM" => "#EMAIL_FROM#",
			"EMAIL_TO" => "#EMAIL_TO#",
			"SUBJECT" => GetMessage("TSZH_METER_VERIFICATION_NOTICE_SUBJECT"),
			"MESSAGE" => GetMessage("TSZH_METER_VERIFICATION_NOTICE_MESSAGE"),
			"BODY_TYPE" => "html",
			"ADDITIONAL_FIELD" => array(
				array("NAME" => "Sender", "VALUE" => "#HEADER_SENDER#"),
				array("NAME" => "List-Unsubscribe", "VALUE" => "<#UNSUBSCRIBE_URL#>"),
			),
			"FIELD1_NAME" => "Sender",
			"FIELD1_VALUE" => "#HEADER_SENDER#",
			"FIELD2_NAME" => "List-Unsubscribe",
			"FIELD2_VALUE" => "<#UNSUBSCRIBE_URL#>",
		),
	),
	"TSZH_RECEIPT" => array(
		array(
			"EMAIL_FROM" => "#EMAIL_FROM#",
			"EMAIL_TO" => "#EMAIL_TO#",
			"SUBJECT" => GetMessage("TSZH_RECEIPT_SUBJECT"),
			"MESSAGE" => GetMessage("TSZH_RECEIPT_MESSAGE"),
			"BODY_TYPE" => "html",
			"ADDITIONAL_FIELD" => array(
				array("NAME" => "Sender", "VALUE" => "#HEADER_SENDER#"),
				array("NAME" => "List-Unsubscribe", "VALUE" => "<#UNSUBSCRIBE_URL#>"),
			),
			"FIELD1_NAME" => "Sender",
			"FIELD1_VALUE" => "#HEADER_SENDER#",
			"FIELD2_NAME" => "List-Unsubscribe",
			"FIELD2_VALUE" => "<#UNSUBSCRIBE_URL#>",
		),
	),
);