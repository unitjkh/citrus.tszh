<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

use \Citrus\Tszh\Types\AccountType;

ob_start();
if (!CTszhFunctionalityController::CheckEdition())
{
	$c = ob_get_contents();
	ob_end_clean();
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
	echo $c;
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
	return;
}
$demoNotice = ob_get_contents();
ob_end_clean();

$modulePermissions = $APPLICATION->GetGroupRight("citrus.tszh");
if ($modulePermissions <= "R")
{
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

require_once($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/citrus.tszh/include.php");
IncludeModuleLangFile(__FILE__);

$strRedirect = BX_ROOT . "/admin/tszh_account_edit.php?lang=" . LANG;
$strRedirectList = BX_ROOT . "/admin/tszh_account_list.php?lang=" . LANG;

ClearVars();
$errorMessage = "";
$bVarsFromForm = false;
$message = null;

$ID = IntVal($_REQUEST["ID"]);

$aTabs = array(
	array("DIV" => "edit1", "TAB" => GetMessage("AE_TAB1"), "ICON" => "citrus.tszh", "TITLE" => GetMessage("AE_TAB1_TITLE")),
	array("DIV" => "edit2", "TAB" => GetMessage("AE_TAB2"), "ICON" => "citrus.tszh", "TITLE" => GetMessage("AE_TAB2_TITLE"))
);

$UF_ENTITY = "TSZH_ACCOUNT";

$tabControl = new CAdminForm("accountEdit", $aTabs);

if ($_SERVER["REQUEST_METHOD"] == "POST" && !empty($_REQUEST['Update']) && $modulePermissions >= "W" && check_bitrix_sessid())
{
	if ($modulePermissions < "W")
	{
		$errorMessage .= GetMessage("AE_ERROR_NO_PERMISSIONS") . "<br>";
	}

	if ($ID > 0 && !($arOldAccount = CTszhAccount::GetByID($ID)))
	{
		$errorMessage .= str_replace('#ID#', $ID, GetMessage("AE_ERROR_ACCOUNT_NOT_FOUND")) . "<br>";
	}

	if (strlen($errorMessage) <= 0)
	{
		$AREA = doubleval($AREA);
		$LIVING_AREA = doubleval($LIVING_AREA);
		$PEOPLE = intval($PEOPLE);

		$arFields = Array(
			"XML_ID" => $XML_ID,
			"TSZH_ID" => $TSZH_ID,
			"TYPE" => $_REQUEST["TYPE"],
			"NAME" => $NAME,
			"HOUSE_ID" => $_REQUEST["HOUSE_ID"],
			"FLAT" => $_REQUEST['FLAT'],
			"FLAT_ABBR" => strlen($_REQUEST['FLAT_ABBR']) > 0 ? $_REQUEST['FLAT_ABBR'] : false,
			"FLAT_TYPE" => $_REQUEST['FLAT_TYPE'],
			"AREA" => $_REQUEST['AREA'],
			"LIVING_AREA" => $_REQUEST['LIVING_AREA'],
			"PEOPLE" => $_REQUEST['PEOPLE'],
			"REGISTERED_PEOPLE" => intval($_REQUEST['REGISTERED_PEOPLE']) > 0 ? $_REQUEST['REGISTERED_PEOPLE'] : false,
			"EXEMPT_PEOPLE" => intval($_REQUEST['EXEMPT_PEOPLE']) > 0 ? $_REQUEST['EXEMPT_PEOPLE'] : false,
			"USER_ID" => IntVal($_REQUEST['USER_ID']) > 0 ? IntVal($_REQUEST['USER_ID']) : false,
			"ACTIVE" => $ACTIVE == "Y" ? "Y" : "N",
		);

		$USER_FIELD_MANAGER->EditFormAddFields($UF_ENTITY, $arFields);
		if ($ID > 0)
		{
			$bResult = CTszhAccount::Update($ID, $arFields);
		}
		else
		{
			$ID = CTszhAccount::Add($arFields);
			$bResult = $ID > 0;
		}
		if (!$bResult)
		{
			/** @noinspection PhpAssignmentInConditionInspection */
			if ($ex = $APPLICATION->GetException())
			{
				$errorMessage .= $ex->GetString() . ".<br />";
			}
			else
			{
				$errorMessage .= GetMessage("AE_ERROR_SAVE") . "<br />";
			}
		}
	}

	if (strlen($errorMessage) <= 0)
	{
		if (empty($_REQUEST['apply']))
		{
			LocalRedirect($strRedirectList . GetFilterParams("filter_", false));
		}
		else
		{
			LocalRedirect($strRedirect . "&ID=" . $ID . "&" . $tabControl->ActiveTabParam());
		}
	}
	else
	{
		$bVarsFromForm = true;
	}
}

//require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/citrus.tszh/prolog.php");

if ($ID > 0)
{
	$APPLICATION->SetTitle(GetMessage("AE_TITLE_EDIT"));
}
else
{
	$APPLICATION->SetTitle(GetMessage("AE_TITLE_ADD"));
}

require_once($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/include/prolog_admin_after.php");

echo $demoNotice;

$dbAccount = CTszhAccount::GetList(
	Array(),
	Array("ID" => $ID),
	false,
	Array("nTopCount" => 1),
	Array("*")
);

if (!$arAccount = $dbAccount->ExtractFields("str_"))
{
	if ($modulePermissions < "W")
	{
		$errorMessage .= GetMessage("AE_ERROR_NO_ADD_PERMISSIONS") . "<br />";
	}
	$ID = 0;
}

if ($bVarsFromForm)
{
	$DB->InitTableVarsForEdit("b_tszh_accounts", "", "str_");
}
else
{
	$createFromUser = IntVal($_GET["for_user"]) > 0 ? IntVal($_GET["for_user"]) : 0;
	/** @noinspection PhpUndefinedVariableInspection */
	if ($createFromUser && IntVal($str_ID) <= 0)
	{
		$arUser = CUser::GetList($by = "id", $order = "desc", Array("ID" => $createFromUser), Array("SELECT" => Array("*", "UF_*")))->Fetch();
		if ($arUser)
		{
			$str_NAME = CUser::FormatName("#LAST_NAME# #NAME# #SECOND_NAME#", $arUser, false, false);
			/*$str_CITY = $arUser['PERSONAL_CITY'];
			$str_REGION = $arUser['PERSONAL_STATE'];
			$arStreet = explode(',', $arUser['PERSONAL_STREET']);
			$str_STREET = array_shift($arStreet);
			$str_HOUSE = trim(implode(',', $arStreet));
			if (array_key_exists("UF_HOUSING", $arUser))
				$str_HOUSE = $arUser['UF_HOUSING'];*/
			if (array_key_exists("UF_FLAT", $arUser))
			{
				$str_FLAT = $arUser['UF_FLAT'];
			}
		}
	}
}


if (strlen($errorMessage) > 0)
{
	CAdminMessage::ShowMessage(Array("DETAILS" => $errorMessage, "TYPE" => "ERROR", "MESSAGE" => GetMessage("AE_ERROR_HAPPENED"), "HTML" => true));
}


/**
 *   CAdminForm()
 **/

$aMenu = array(
	array(
		"TEXT" => GetMessage("AE_M_ACCOUNT_LIST"),
		"LINK" => "/bitrix/admin/tszh_account_list.php?lang=" . LANG . GetFilterParams("filter_"),
		"ICON" => "btn_list",
		"TITLE" => GetMessage("AE_M_ACCOUNT_LIST_TITLE"),
	)
);

if ($ID > 0 && $modulePermissions >= "W")
{
	$aMenu[] = array("SEPARATOR" => "Y");

	$aMenu[] = array(
		"TEXT" => GetMessage("AE_M_ADD_ACCOUNT"),
		"LINK" => "/bitrix/admin/tszh_account_edit.php?lang=" . LANG . GetFilterParams("filter_"),
		"ICON" => "btn_new",
		"TITLE" => GetMessage("AE_M_ADD_ACCOUNT_TITLE"),
	);

	if ($modulePermissions >= "W")
	{
		$aMenu[] = array(
			"TEXT" => GetMessage("AE_M_DELETE_ACCOUNT"),
			"LINK" => "javascript:if(confirm('" . GetMessage("AE_M_DELETE_ACCOUNT_CONFIRM") . "')) window.location='/bitrix/admin/tszh_account_list.php?ID=" . $ID . "&action=delete&lang=" . LANG . "&" . bitrix_sessid_get() . "#tb';",
			"WARNING" => "Y",
			"ICON" => "btn_delete"
		);
	}

}
if (!empty($aMenu))
{
	$aMenu[] = array("SEPARATOR" => "Y");
}
$link = DeleteParam(array("mode"));
$link = $APPLICATION->GetCurPage() . "?mode=settings" . ($link <> "" ? "&" . $link : "");

if ($str_USER_ID > 0 && $USER->IsAdmin())
{
	$aMenu[] = array(
		"TEXT" => GetMessage("AE_ACTION_AUTH"),
		"TITLE" => GetMessage("AE_ACTION_AUTH_TITLE"),
		"LINK" => "user_admin.php?lang=" . LANGUAGE_ID . "&ID=" . $str_USER_ID . "&action=authorize&" . bitrix_sessid_get(),
		"ICON" => "",
	);
}

$context = new CAdminContextMenu($aMenu);
$context->Show();

if (method_exists($USER_FIELD_MANAGER, 'showscript'))
{
	$tabControl->BeginPrologContent();
	echo $USER_FIELD_MANAGER->ShowScript();
	$tabControl->EndPrologContent();
}


// ============ FORM PROLOG ==================
$tabControl->BeginEpilogContent();
echo GetFilterHiddens("filter_");
?>
	<input type="hidden" name="Update" value="Y"/>
	<input type="hidden" name="lang" value="<? echo LANG ?>"/>
	<input type="hidden" name="ID" value="<? echo $ID ?>"/>
<?
echo bitrix_sessid_post();
$tabControl->EndEpilogContent();
// ===========================================

$tabControl->Begin(array(
	"FORM_ACTION" => $APPLICATION->GetCurPage(),
	"FORM_ATTRIBUTES" => "method=\"POST\" enctype=\"multipart/form-data\"",
));
$tabControl->BeginNextFormTab();

if ($str_ID > 0)
{
	$tabControl->AddViewField("ID", "ID", $str_ID);
}

$tabControl->BeginCustomField("USER_ID", GetMessage("AEF_USER_ID"));
?>
	<tr>
		<td width="40%"><?= $tabControl->GetCustomLabelHTML() ?>:</td>
		<td width="60%">
			<?= FindUserID("USER_ID", $ID > 0 ? $str_USER_ID : IntVal($_GET["for_user"]), "", "accountEdit_form"); ?>
		</td>
	</tr>
<?
$tabControl->EndCustomField("USER_ID", '<input type="hidden" name="USER_ID" value="' . $str_USER_ID . '">');
$tabControl->AddCheckBoxField("ACTIVE", GetMessage("ACTIVE"), false, "Y", ($str_ACTIVE == "Y"));

// �������� ���� ��� �������
$arTszhFilter = $arTszhRight = array();
if (CTszhFunctionalityController::isPortal() && !$USER->IsAdmin())
{
	global $USER;
	$arGroups = $USER->GetUserGroupArray();

	$rsPerms = \Vdgb\PortalTszh\PermsTable::getList(
		array(
			"filter" => array("@GROUP_ID" => $arGroups)
		)
	);

	while ($arPerms = $rsPerms->fetch())
	{
		if ($arPerms["PERMS"] >= "W")
		{
			$arTszhRight[$arPerms["TSZH_ID"]] = $arPerms["PERMS"];
		}
	}

	$arTszhFilter["@ID"] = array_keys($arTszhRight);
}

$rsTszh = CTszh::GetList(Array("NAME" => "ASC"), $arTszhFilter);
$arTszhList = Array();
/** @noinspection PhpAssignmentInConditionInspection */
while ($arTszh = $rsTszh->Fetch())
{
	$arTszhList[$arTszh['ID']] = $arTszh["NAME"];
}
$tabControl->AddDropDownField("TSZH_ID", GetMessage("AEF_TSZH_ID"), true, $arTszhList, $str_TSZH_ID);

$tabControl->AddEditField("XML_ID", GetMessage("AEF_XML_ID"), true, array("size" => 30, "maxlength" => 50), $str_XML_ID);
$tabControl->AddDropDownField("TYPE", GetMessage("CITRUS_TSZH_ACCOUNT_TYPE"), true, AccountType::getTitles(), $arAccount["TYPE"]);
$tabControl->AddEditField("NAME", GetMessage("AEF_NAME"), false, array("size" => 50, "maxlength" => 50), $str_NAME);

$tabControl->BeginCustomField("HOUSE_ID", GetMessage("AEF_HOUSE"), false);
?>
	<tr>
		<td width="40%"><?= GetMessage("AEF_HOUSE") ?>:</td>
		<td width="60%"><?= tszhLookup('HOUSE_ID', $arAccount['HOUSE_ID'], Array("type" => 'house', "formName" => 'accountEdit_form')); ?></td>
	</tr>
<?
$tabControl->EndCustomField("HOUSE_ID", '<input type="hidden" name="ACCOUNT_ID" value="' . $arAccountPeriod['ACCOUNT_ID'] . '">');

$tabControl->AddEditField("FLAT", GetMessage("AEF_FLAT"), false, array("size" => 5, "maxlength" => 50), $str_FLAT);
$tabControl->AddEditField("FLAT_ABBR", GetMessage("AEF_FLAT_ABBR"), false, array("size" => 10, "maxlength" => 50), $str_FLAT_ABBR);
$tabControl->AddEditField("FLAT_TYPE", GetMessage("AEF_FLAT_TYPE"), false, array("size" => 20, "maxlength" => 50), $str_FLAT_TYPE);

$tabControl->BeginCustomField("AREA", GetMessage("AEF_AREA"));
?>
	<tr>
		<td width="40%"><?= $tabControl->GetCustomLabelHTML() ?>:</td>
		<td width="60%">
			<input type="text" name="AREA" value="<?= $str_AREA ?>" size="5"/>

			&nbsp;&nbsp;
			<?= GetMessage("AEF_L_AREA") ?>: <input type="text" name="LIVING_AREA" value="<?= $str_LIVING_AREA ?>"
			                                        size="5"/>
		</td>
	</tr>
<?
$tabControl->EndCustomField("AREA",
	'<input type="hidden" name="AREA" value="' . $str_AREA . '">' .
	'<input type="hidden" name="LIVING_AREA" value="' . $str_LIVING_AREA . '">'
);

$tabControl->BeginCustomField("PEOPLE", GetMessage("AEF_PEOPLE"));
?>
	<tr>
		<td width="40%"><?= $tabControl->GetCustomLabelHTML() ?>:</td>
		<td width="60%">
			<input type="text" name="PEOPLE" value="<?= $str_PEOPLE ?>" size="5"/>

			&nbsp;&nbsp;
			<?= GetMessage("AEF_REGISTERED_PEOPLE") ?>: <input type="text" name="REGISTERED_PEOPLE"
			                                                   value="<?= $str_REGISTERED_PEOPLE ?>" size="5"/>
		</td>
	</tr>
<?
$tabControl->EndCustomField("PEOPLE");
//$tabControl->AddEditField("PEOPLE", GetMessage("AEF_PEOPLE"), false, array("size"=>5, "maxlength"=>50), $str_PEOPLE);

$tabControl->AddEditField("EXEMPT_PEOPLE", GetMessage("AEF_EXEMPT_PEOPLE"), false, array("size" => 5), $str_EXEMPT_PEOPLE);

// ������ "������� ������"
$tabControl->AddSection("BUILDING_AREAS", GetMessage("AEF_BUILDING_AREAS"));
$tabControl->AddEditField("HOUSE_AREA", GetMessage("AEF_HOUSE_AREA"), false, array("size" => 8), $str_HOUSE_AREA);
$tabControl->AddEditField("HOUSE_ROOMS_AREA", GetMessage("AEF_HOUSE_ROOMS_AREA"), false, array("size" => 8), $str_HOUSE_ROOMS_AREA);
$tabControl->AddEditField("HOUSE_COMMON_PLACES_AREA", GetMessage("AEF_HOUSE_COMMON_PLACES_AREA"), false, array("size" => 8), $str_HOUSE_COMMON_PLACES_AREA);

// ������� ������ � ����������������� ������, ���� �� ��� ���� ���� ��� � ������������ ���� ����� �� ���������� �����
$tabControl->AddSection("USER_FIELDS", GetMessage("AEF_USER_FIELDS"));
if (
	(count($USER_FIELD_MANAGER->GetUserFields($UF_ENTITY)) > 0) ||
	($USER_FIELD_MANAGER->GetRights($UF_ENTITY) >= "W")
)
{
	$tabControl->AddSection('USER_FIELDS', GetMessage("AEF_USER_FIELDS"));
	$tabControl->ShowUserFields($UF_ENTITY, $str_ID, $bVarsFromForm);
}

$tabControl->BeginNextFormTab();
$tabControl->BeginCustomField("ACCOUNT_HISTORY", GetMessage("AEF_HISTORY_RECORDS"));
//echo '<tr class="heading"><td colspan="2">' . GetMessage("AEF_HISTORY_RECORDS") . '</td></tr>';
echo '<tr><td colspan="2">';
define('B_ADMIN_SUBELEMENTS', 1);
define('B_ADMIN_SUBELEMENTS_LIST', false);
$strSubElementAjaxPath = '/bitrix/admin/tszh_accounts_history_admin.php?ACCOUNT_ID=' . $ID . '&lang=' . LANGUAGE_ID;
$accountID = $ID;
require($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/citrus.tszh/tools/tszh_account_history.php');
echo '</td></tr>';
$tabControl->EndCustomField("ACCOUNT_HISTORY");


$tabControl->Buttons(Array(
	"disabled" => ($modulePermissions < "W"),
	"back_url" => "/bitrix/admin/tszh_account_list.php?lang=" . LANG . GetFilterParams("filter_")
));


$tabControl->Show();

$tabControl->ShowWarnings($tabControl->GetName(), $message);

if (!defined('BX_PUBLIC_MODE') || BX_PUBLIC_MODE != 1):?>
	<? echo BeginNote(); ?>
	<span class="required">*</span> <? echo GetMessage("REQUIRED_FIELDS") ?>
	<? echo EndNote(); ?>
<? endif; ?>
<? require_once($DOCUMENT_ROOT . BX_ROOT . "/modules/main/include/epilog_admin.php"); ?>