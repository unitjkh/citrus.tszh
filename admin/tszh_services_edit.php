<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

ob_start();
if (!CTszhFunctionalityController::CheckEdition())
{
	$c = ob_get_contents();
	ob_end_clean();
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
	echo $c;
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
	return;
}
$demoNotice = ob_get_contents();
ob_end_clean();

$modulePermissions = $APPLICATION->GetGroupRight("citrus.tszh");
if ($modulePermissions<="R")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

require_once($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/citrus.tszh/include.php");
IncludeModuleLangFile(__FILE__);

$strRedirect = BX_ROOT."/admin/tszh_services_edit.php?lang=".LANG;
$strRedirectList = BX_ROOT."/admin/tszh_services.php?lang=".LANG;

ClearVars();
$errorMessage = "";
$bVarsFromForm = false;
$message = null;

$ID = IntVal($ID);

// �������� ���� ��� �������
$arTszhFilter = $arTszhRight = $arListFilter = array();
if (CTszhFunctionalityController::isPortal() && !$USER->IsAdmin())
{
	global $USER;
	$arGroups = $USER->GetUserGroupArray();

	$rsPerms = \Vdgb\PortalTszh\PermsTable::getList(
		array(
			"filter" => array("@GROUP_ID" => $arGroups)
		)
	);

	while($arPerms = $rsPerms->fetch())
	{
		if($arPerms["PERMS"] >= "R")
		{
			$arTszhRight[$arPerms["TSZH_ID"]] = $arPerms["PERMS"];
		}
	}

	$arTszhFilter["@ID"] = $arListFilter["@TSZH_ID"] = array_keys($arTszhRight);
}

$aTabs = array(
		array("DIV" => "edit1", "TAB" => GetMessage("TSE_TAB1"), "ICON" => "citrus.tszh", "TITLE" => GetMessage("TSE_TAB1_TITLE"))
	);

$UF_ENTITY = "TSZH_SERVICE";

$tabControl = new CAdminForm("serviceEdit", $aTabs);

if ($REQUEST_METHOD=="POST" && strlen($Update)>0 && $modulePermissions>="W" && check_bitrix_sessid())
{
	if ($modulePermissions < "W")
		$errorMessage .= GetMessage("TSE_ERROR_NO_PERMISSIONS") . ".<br>";

	if ($ID > 0)
	{
		if (!($arOldService = CTszhService::GetByID($ID)))
			$errorMessage .= str_replace("#ID#", $ID, GetMessage("TSE_ERROR_NOT_FOUND")) . ".<br />";
	}	
	
	if (strlen($errorMessage) <= 0)
	{
		$arFields = Array(
			"ACTIVE" => $ACTIVE == "Y" ? "Y" : "N",
			"NAME" => htmlspecialcharsbx($NAME),
			"TSZH_ID" => $TSZH_ID,
			"XML_ID" => htmlspecialcharsbx($XML_ID),
			"UNITS" => htmlspecialcharsbx($UNITS),
			"NORM" => $NORM,
			"TARIFF" => $TARIFF,
			"TARIFF2" => false,
			"TARIFF3" => false,
		);

		$USER_FIELD_MANAGER->EditFormAddFields($UF_ENTITY, $arFields);

		if (FloatVal($TARIFF2) > 0)
			$arFields["TARIFF2"] = FloatVal($TARIFF2); 
		if (FloatVal($TARIFF3) > 0)
			$arFields["TARIFF3"] = FloatVal($TARIFF3); 
		if ($ID > 0) {
			$bResult = CTszhService::Update($ID, $arFields);
		} else {
			$ID = CTszhService::Add($arFields);
			$bResult = $ID > 0;
		}

// ��.		
		
		if (!$bResult)
		{
			if ($ex = $APPLICATION->GetException())
				$errorMessage .= GetMessage("TSE_ERROR_SAVE") . ":<br />" . $ex->GetString().".<br />";
			else
				$errorMessage .= GetMessage("TSE_ERROR_SAVE") . ".<br />";
		}
	}

	if (strlen($errorMessage) <= 0)
	{
		if (strlen($apply) <= 0)
			LocalRedirect($strRedirectList . GetFilterParams("filter_", false));
		else
			LocalRedirect($strRedirect."&ID=".$ID."&".$tabControl->ActiveTabParam());
	}
	else
	{
		$bVarsFromForm = true;
	}
}

//require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/citrus.tszh/prolog.php");

if ($ID > 0)
	$APPLICATION->SetTitle(str_replace("#ID#", $ID, GetMessage("TSE_PAGE_TITLE_EDIT")));
else
	$APPLICATION->SetTitle(GetMessage("TSE_PAGE_TITLE_ADD"));

require_once ($DOCUMENT_ROOT.BX_ROOT."/modules/main/include/prolog_admin_after.php");

echo $demoNotice;

if ($ID > 0):
	$dbService = CTszhService::GetList(
		Array(),
		Array("ID" => $ID),
		false,
		Array("nTopCount" => 1),
		Array("*")
	);
	
	if (!$arService = $dbService->ExtractFields("str_"))
	{	
		if ($modulePermissions < "W")
			$errorMessage .= GetMessage("TSE_ERROR_NO_PERMISSIONS") . ".<br />";
		$ID = 0;
	}	

endif;

if ($bVarsFromForm) {
	$DB->InitTableVarsForEdit("b_tszh_services", "", "str_");
}


if(strlen($errorMessage)>0)
	echo CAdminMessage::ShowMessage(Array("DETAILS"=>$errorMessage, "TYPE"=>"ERROR", "MESSAGE"=> GetMessage("TSE_ERRORS_HAPPENED"), "HTML"=>true));

/**
 *   CAdminForm()
**/

		$aMenu = array(
		array(
				"TEXT" => GetMessage("TSE_M_SERVICES_LIST"),
				"LINK" => "/bitrix/admin/tszh_services.php?lang=".LANG.GetFilterParams("filter_"),
				"ICON"	=> "btn_list",
				"TITLE" => GetMessage("TSE_M_SERVICES_LIST_TITLE"),
			)
	);

if ($ID > 0 && $modulePermissions >= "W")
{
	$aMenu[] = array("SEPARATOR" => "Y");

	$aMenu[] = array(
			"TEXT" => GetMessage("TSE_M_ADD_SERVICE"),
			"LINK" => "/bitrix/admin/tszh_services_edit.php?lang=".LANG.GetFilterParams("filter_"),
			"ICON"	=> "btn_new",
			"TITLE" => GetMessage("TSE_M_ADD_SERVICE_TITLE"),
		);

	if ($modulePermissions >= "W")
	{
		$aMenu[] = array(
				"TEXT" => GetMessage("TSE_M_DELETE"),
				"LINK" => "javascript:if(confirm('" . GetMessage("TSE_M_DELETE_CONFIRM") . "')) window.location='/bitrix/admin/tszh_services_edit.php?ID=".$ID."&action=delete&lang=".LANG."&".bitrix_sessid_get()."#tb';",
				"WARNING" => "Y",
				"ICON"	=> "btn_delete"
			);
	}

}
if(!empty($aMenu))
	$aMenu[] = array("SEPARATOR"=>"Y");
$link = DeleteParam(array("mode"));
$link = $GLOBALS["APPLICATION"]->GetCurPage()."?mode=settings".($link <> ""? "&".$link:"");
$aMenu[] = array(
	"TEXT"=> GetMessage("TSE_M_SETTINGS"),
	"TITLE"=> GetMessage("TSE_M_SETTINGS_TITLE"),
	"LINK"=>"javascript:".$tabControl->GetName().".ShowSettings('".htmlspecialcharsbx(CUtil::addslashes($link))."')",
	"ICON"=>"btn_settings",
);

$context = new CAdminContextMenu($aMenu);
$context->Show();

// ============ FORM PROLOG ==================
$tabControl->BeginEpilogContent();
echo GetFilterHiddens("filter_");
?>
<input type="hidden" name="Update" value="Y" />
<input type="hidden" name="lang" value="<?echo LANG ?>" />
<input type="hidden" name="ID" value="<?echo $ID ?>" />
<?
echo bitrix_sessid_post();
$tabControl->EndEpilogContent();
// ===========================================

$tabControl->Begin(array(
	"FORM_ACTION" => $APPLICATION->GetCurPage(),
	"FORM_ATTRIBUTES" => "method=\"POST\" enctype=\"multipart/form-data\"",
));


$tabControl->BeginNextFormTab();

	if ($str_ID > 0) {
		$tabControl->AddViewField("ID", "ID", $str_ID);
	}

	$tabControl->AddCheckBoxField("ACTIVE", GetMessage("TSE_F_ACTIVE"), false, "Y", ($str_ACTIVE=="Y"));

	$rsTszh = CTszh::GetList(Array("NAME" => "ASC"), $arTszhFilter);
	$arTszhList = Array();
	while ($arTszh = $rsTszh->Fetch()) {
		$arTszhList[$arTszh['ID']] = $arTszh["NAME"];
	}
	$tabControl->AddDropDownField("TSZH_ID", GetMessage("TSE_TSZH_ID"), true, $arTszhList, $str_TSZH_ID);

	$tabControl->AddEditField("NAME", GetMessage("TSE_F_NAME"), true, array("size"=>30, "maxlength"=>50), $str_NAME);
	$tabControl->AddEditField("XML_ID", GetMessage("TSE_F_XML_ID"), false, array("size"=>30, "maxlength"=>50), $str_XML_ID);

	$tabControl->AddEditField("UNITS", GetMessage("TSE_F_UNITS"), false, array("size"=>5, "maxlength"=>11), $str_UNITS);
	$tabControl->AddEditField("NORM", GetMessage("TSE_F_NORM"), true, array("size"=>5, "maxlength"=>11), $str_NORM);
	$tabControl->AddEditField("TARIFF", GetMessage("TSE_F_TARIFF"), true, array("size"=>5, "maxlength"=>11), $str_TARIFF);
	$tabControl->AddEditField("TARIFF2", GetMessage("TSE_F_TARIFF2"), false, array("size"=>5, "maxlength"=>11), $str_TARIFF2);
	$tabControl->AddEditField("TARIFF3", GetMessage("TSE_F_TARIFF3"), false, array("size"=>5, "maxlength"=>11), $str_TARIFF3);

	// ������� ������ � ����������������� ������, ���� �� ��� ���� ���� ��� � ������������ ���� ����� �� ���������� �����
	if(
		(count($USER_FIELD_MANAGER->GetUserFields($UF_ENTITY)) > 0) ||
		($USER_FIELD_MANAGER->GetRights($UF_ENTITY) >= "W")
	)
	{
		$tabControl->AddSection('USER_FIELDS', GetMessage("TSE_USER_FIELDS"));
		$tabControl->ShowUserFields($UF_ENTITY, $str_ID, $bVarsFromForm);
	}
	
	$tabControl->Buttons(Array(
		"disabled" => ($modulePermissions < "W"),
		"back_url" => "/bitrix/admin/tszh_services.php?lang=".LANG.GetFilterParams("filter_")
	));


	$tabControl->Show();

	$tabControl->ShowWarnings($tabControl->GetName(), $message);

if(!defined('BX_PUBLIC_MODE') || BX_PUBLIC_MODE != 1):?>
<?echo BeginNote();?>
<span class="required">*</span> <?echo GetMessage("REQUIRED_FIELDS")?>
<?echo EndNote(); ?>
<?endif;?>
<?require_once ($DOCUMENT_ROOT.BX_ROOT."/modules/main/include/epilog_admin.php");?>