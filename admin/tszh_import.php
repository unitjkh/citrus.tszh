<?
/**
 * ������ ���ƻ
 * �������� ������� ������� ������, ����������� ����� � ��������� ��������� �� 1�
 * @package tszh
 */

use Bitrix\Main\Localization\Loc;
use Citrus\Tszh\Exchange\v4\Formats\Accounts;

define("NO_KEEP_STATISTIC", true);

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php"); // ������ ����� ������

ob_start();
if (!CTszhFunctionalityController::CheckEdition())
{
	$c = ob_get_contents();
	ob_end_clean();
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
	echo $c;
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");

	return;
}
$demoNotice = ob_get_contents();
ob_end_clean();

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszh/include.php"); // ������������� ������
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszh/prolog.php"); // ������ ������

//define("TSZH_IMPORT_DEBUG", true);

IncludeModuleLangFile(__FILE__);

// ������� ����� ������� �������� ������������ �� ������
$POST_RIGHT = $APPLICATION->GetGroupRight("citrus.tszh");

// ���� ��� ���� - �������� � ����� ����������� � ���������� �� ������
if ($POST_RIGHT < "U")
{
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

if (!CModule::IncludeModule("iblock"))
{
	CAdminMessage::ShowMessage(GetMessage("TI_ERROR_IBLOCK_NOT_INSTALLED"));
}

$maxTime = intval(ini_get('max_execution_time'));
if (!isset($INTERVAL))
{
	$INTERVAL = min($maxTime > 10 ? $maxTime - 5 : 10, 60);
}
else
{
	$INTERVAL = intval($INTERVAL);
}
if ($INTERVAL <= 0)
{
	@set_time_limit(0);
}

$start_time = time();

$arErrors = array();
$arMessages = array();

$mayCreateTszhFlag = false;

// �� ��������� ������ ����� 4 ������ ������
$VERSION = 4;

if ($_REQUEST["Import"] == "Y" && (array_key_exists("NS", $_REQUEST) || array_key_exists("bFirst", $_REQUEST)))
{
	require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_js.php");

	CUtil::JSPostUnescape();

	//Initialize NS variable which will save step data
	if (array_key_exists("NS", $_REQUEST))
	{
		$NS = unserialize($_REQUEST["NS"]);
	}
	else
	{
		$NS = array(
			"STEP" => 0,
			"VERSION" => isset($_REQUEST['VERSION']) ? intval($_REQUEST['VERSION']) : 4,
			"ONLY_DEBT" => $_REQUEST['updateMode'] == "D",
			"UPDATE_MODE" => $_REQUEST['updateMode'] == "Y" || $_REQUEST['updateMode'] == "D",
			"CREATE_USERS" => $_REQUEST['createUsers'] == "Y",
			"URL_DATA_FILE" => $_REQUEST["URL_DATA_FILE"],
			"ACTION" => $_REQUEST['updateMode'] != "D" ? $_REQUEST["outFileAction"] : 'N',
			"DEPERSONALIZE" => $_REQUEST['depersonalize'] == "Y",
		);

		CUserOptions::SetOption('citrus.tszh.import', 'UpdateMode', $NS["UPDATE_MODE"]);
		CUserOptions::SetOption('citrus.tszh.import', 'Action', $NS["ACTION"]);
		CUserOptions::SetOption('citrus.tszh.import', 'Depersonalize', $NS["DEPERSONALIZE"]);
	}

	$VERSION = $NS["VERSION"];

	//We have to strongly check all about file names at server side
	$ABS_FILE_NAME = false;
	$WORK_DIR_NAME = false;
	if (isset($NS["URL_DATA_FILE"]) && (strlen($NS["URL_DATA_FILE"]) > 0))
	{
		$filename = trim(str_replace("\\", "/", trim($NS["URL_DATA_FILE"])), "/");
		$FILE_NAME = rel2abs($_SERVER["DOCUMENT_ROOT"], "/" . $filename);
		if ((strlen($FILE_NAME) > 1) && ($FILE_NAME === "/" . $filename) && ($APPLICATION->GetFileAccessPermission($FILE_NAME) >= "W"))
		{
			$ABS_FILE_NAME = $_SERVER["DOCUMENT_ROOT"] . $FILE_NAME;
			$WORK_DIR_NAME = substr($ABS_FILE_NAME, 0, strrpos($ABS_FILE_NAME, "/") + 1);
		}
	}

	if ($VERSION == 4)
	{
		$debug = \COption::GetOptionInt('citrus.tszh', '1c_exchange.Debug', "Y") != "N";
		$obImport = new Accounts($NS, $_REQUEST['INTERVAL'], true);
		try
		{
			$progress = array();
			$finished = false;
			if ($NS["STEP"] < 1)
			{
				$NS["STEP"]++;
				$progress[] = Loc::getMessage("TI_STEP1_DONE");
			}
			elseif ($NS["STEP"] < 2)
			{
				if (file_exists($ABS_FILE_NAME) && is_file($ABS_FILE_NAME) && ($fp = fopen($ABS_FILE_NAME, "rb")))
				{
					if ($obImport->readXml($fp))
					{
						$NS["STEP"]++;
					}
					fclose($fp);
				}
				else
				{
					CAdminMessage::ShowMessage(GetMessage("TI_FILE_ERROR"));
					throw new \Exception(GetMessage("TI_FILE_ERROR"));
				}
				$file_size = file_exists($ABS_FILE_NAME) && is_file($ABS_FILE_NAME) ? filesize($ABS_FILE_NAME) : 0;
			}
			elseif ($NS["STEP"] < 3)
			{
				try
				{
					$obImport->ProcessPeriod();
				}
				catch (Exception $e)
				{
					$mess = $e->getMessage();
					if (strpos($mess, "CITRUS_TSZH_ERROR_WRONG_VERSION") > 0)
					{
						$arErrors[] = GetMessage("CITRUS_TSZH_ERROR_WRONG_VERSION_TEXT");
					}
					else
					{
						$arErrors[] = $e->getCode();
					}
				}
				$obImport->ImportServices();
				$NS['ACCOUNTS_IMPORT_STARTED'] = time();
				$NS["STEP"]++;
			}
			elseif ($NS["STEP"] < 4)
			{
				if ($obImport->importHouses())
				{
					$NS["STEP"]++;
				}
			}
			elseif ($NS["STEP"] < 5)
			{
				$NS["STEP"]++;
			}
			elseif ($NS["STEP"] < 6)
			{
				$result = $obImport->ImportAccounts();
				$counter = 0;
				foreach ($result as $key => $value)
				{
					$NS["DONE"][$key] += $value;
					$counter += $value;
				}
				if (!$counter)
				{
					$NS["STEP"]++;
				}
			}
			elseif ($NS["STEP"] < 7)
			{
				if ($NS['ADDTITIONAL_PERIODS_EXISTS'])
				{
					$result = $obImport->ImportAdditionalPeriods();
					if ($result)
					{
						$NS["STEP"]++;
					}

				}
				else
				{
					$NS["STEP"]++;
				}
			}
			elseif ($NS["STEP"] < 8)
			{
				$result = $obImport->Cleanup($NS["ACTION"], $start_time, $_REQUEST['INTERVAL']);

				$counter = 0;
				foreach ($result as $key => $value)
				{
					$NS["DONE"][$key] += $value;
					$counter += $value;
				}
				if (!$counter)
				{
					$NS["STEP"]++;
				}
			}
			elseif ($NS["STEP"] < 9)
			{
				if (!$debug)
				{
					@unlink($ABS_FILE_NAME);
				}
				if (!$debug && \CTszh::hasDemoAccounts($NS["TSZH"], $arDemoAccounts))
				{
					$res = false;
					foreach ($arDemoAccounts as $accountID => $arAccount)
					{
						$res = \CTszhAccount::delete($accountID);
					}
					if ($res)
					{
						$progress[] = Loc::getMessage("TI_STEP8_PROGRESS");
					}
				}
				$NS["STEP"]++;
				$obImport->clearSession();
				$strErrors = \CTszhImport::GetErrors();
				if (strlen($strErrors) > 0)
				{
					echo "warning\n" . $strErrors;

					return;
				}
				$finished = true;
			}
			else
			{
				if ($debug)
				{
					$NS["STEP"] = 0;
				}
				else
				{
					throw new \Exception(Loc::getMessage("TI_ERROR_ALREADY_FINISHED"));
				}
			}
		}
		catch (\Exception $e)
		{
			$NS = array();
			throw $e;
		}
	}
	else
	{
		$obImport = new CTszhImport($NS, $INTERVAL);

		if (!check_bitrix_sessid())
		{
			$arErrors[] = GetMessage("TI_SESSION_EXPIRED");
		}
		elseif ($ABS_FILE_NAME)
		{
			if ($NS["STEP"] < 1)
			{
				$NS["STEP"] = 2;
			}
			elseif ($NS["STEP"] < 3)
			{
				$original = $ABS_FILE_NAME;
				$utf = iconv(SITE_CHARSET, 'utf-8', $original);
				if (file_exists($ABS_FILE_NAME) && is_file($ABS_FILE_NAME) && ($fp = fopen($ABS_FILE_NAME, "rb")))
				{
					if ($obImport->readXml($fp))
					{
						$NS["STEP"]++;
					}
					else
					{
						$NS["position"] = $obImport->getXml()->file_position;
					}
					fclose($fp);
				}
				elseif (file_exists($utf) && is_file($utf) && ($fp = fopen($utf, "rb")))
				{
					if ($obImport->readXml($fp))
					{
						$NS["STEP"]++;
					}
					else
					{
						$NS["position"] = $obImport->getXml()->file_position;
					}
					fclose($fp);
				}
				else
				{
					$arErrors[] = GetMessage("TI_FILE_ERROR");
				}
			}
			elseif ($NS["STEP"] < 4)
			{
				$NS["STEP"]++;
			}
			elseif ($NS["STEP"] < 5)
			{
				$result = $obImport->ProcessPeriod();
				if ($result === true)
				{
					$obImport->ImportServices();
					$NS['ACCOUNTS_IMPORT_STARTED'] = time();
					$NS["STEP"]++;
				}
				elseif ($result === 1)
				{
					$mayCreateTszhFlag = true;
				}
				else
				{
					if ($ex = $APPLICATION->GetException())
					{
						$arErrors[] = $ex->GetString();
					}
					else
					{
						$arErrors[] = GetMessage("TI_ERROR_PROCESS_PERIOD");
					}
				}
			}
			elseif ($NS["STEP"] < 6)
			{
				$result = $obImport->ImportAccounts($start_time);

				$counter = 0;
				foreach ($result as $key => $value)
				{
					$NS["DONE"][$key] += $value;
					$counter += $value;
				}

				if (!$counter)
				{
					$NS["STEP"]++;
				}
			}
			elseif ($NS["STEP"] < 7)
			{
				$result = $obImport->Cleanup($NS["ACTION"], $start_time, $INTERVAL);

				$counter = 0;
				foreach ($result as $key => $value)
				{
					$NS["DONE"][$key] += $value;
					$counter += $value;
				}

				if (!$counter)
				{
					$NS["STEP"]++;
				}
			}
			elseif ($NS["STEP"] < 8)
			{
				if (CTszh::hasDemoAccounts($NS["TSZH"], $arDemoAccounts))
				{
					$res = false;
					foreach ($arDemoAccounts as $accountID => $arAccount)
					{
						$res = CTszhAccount::delete($accountID);
					}
				}
				$NS["STEP"]++;
			}
		}
		else
		{
			$arErrors[] = GetMessage("TI_FILE_ERROR");
		}
	}

	$strErrors = CTszhImport::GetErrors();

	if (strlen($strErrors) > 0)
	{
		$arErrors[] = $strErrors;
		CTszhImport::ResetErrors();
	}

	foreach ($arErrors as $strError)
	{
		CAdminMessage::ShowMessage($strError);
	}

	foreach ($arMessages as $strMessage)
	{
		CAdminMessage::ShowMessage(array("MESSAGE" => $strMessage, "TYPE" => "OK"));
	}

	if (count($arErrors) == 0):?>
		<? if ($NS["STEP"] < 8): ?>
			<ul>
				<li>
					<? echo GetMessage("TI_TABLES_DROPPED"); ?>
				</li>
				<li>
					<?
					if ($NS["STEP"] < 1)
					{
						echo GetMessage("TI_TABLES_CREATION");
					}
					elseif ($NS["STEP"] < 2)
					{
						echo "<b>" . GetMessage("TI_TABLES_CREATION") . "</b>";
					}
					else
					{
						echo GetMessage("TI_TABLES_CREATED");
					}
					?>
				</li>
				<li><?
					if ($NS["STEP"] < 2)
					{
						echo GetMessage("TI_FILE_READING");
					}
					elseif ($NS["STEP"] < 3)
					{
						if (file_exists($ABS_FILE_NAME))
						{
							$file_size = filesize($ABS_FILE_NAME);
						}
						else
						{
							$file_size = 0;
						}
						echo "<b>" . GetMessage("TI_FILE_PROGRESS", array("#PERCENT#" => $file_size > 0 ? round($obImport->getXml()->GetFilePosition() / $file_size * 100, 2) : 0)) . "</b>" .
							'<br><div style="width:200px;border:1px solid black"><div style="background-color:green;width:' . ($file_size > 0 ? round($obImport->getXml()->GetFilePosition() / $file_size * 200) : 0) . 'px;">&nbsp;</div></div>';
					}
					else
					{
						echo GetMessage("TI_FILE_READ");
					}
					?></li>
				<li><?
					if ($NS["STEP"] < 3)
					{
						echo GetMessage("TI_INDEX_CREATION");
					}
					elseif ($NS["STEP"] < 4)
					{
						echo "<b>" . GetMessage("TI_INDEX_CREATION") . "</b>";
					}
					else
					{
						echo GetMessage("TI_INDEX_CREATED");
					}
					?></li>
				<li><?
					if ($NS["STEP"] < 4)
					{
						echo GetMessage("TI_METADATA");
					}
					elseif ($NS["STEP"] < 5)
					{
						echo "<b>" . GetMessage("TI_METADATA") . "</b>";
					}
					else
					{
						echo GetMessage("TI_METADATA_DONE");
					}
					?></li>
				<li><?
					if ($NS["STEP"] < 5)
					{
						echo GetMessage("TI_ELEMENTS");
					}
					elseif ($NS["STEP"] < 6)
					{
						echo "<b>" . GetMessage("TI_ELEMENTS_PROGRESS", array(
								"#DONE#" => intval($NS["DONE"]["ADD"] + $NS["DONE"]["UPD"] + $NS["DONE"]["ERR"]),
								"#TOTAL#" => intval($NS["DONE"]["ALL"])
							)) . "</b>" .
							'<br><div style="width:200px; border:1px solid black; margin-top: 4px;"><div style="background-color:green;width:' . (intval($NS["DONE"]["ALL"]) > 0 ? round((intval($NS["DONE"]["ADD"] + $NS["DONE"]["UPD"] + $NS["DONE"]["ERR"])) / intval($NS["DONE"]["ALL"]) * 200) : 0) . 'px;">&nbsp;</div></div>';
						if (array_key_exists('DEL', $NS['DONE']))
						{
							echo '<br><b>' . GetMessage("TI_ACCOUNT_DELETE_PROGRESS", Array('#DONE#' => intval($NS["DONE"]["DEL"]))) . '</b>';
						}
						if (array_key_exists('DEL_METERS', $NS['DONE']))
						{
							echo '<br><b>' . GetMessage("TI_METER_DELETE_PROGRESS", Array('#DONE#' => intval($NS["DONE"]["DEL_METERS"]))) . '</b>';
						}
					}
					else
					{
						echo GetMessage("TI_ELEMENTS_DONE");
					}
					?></li>
				<? if ($NS["ACTION"] == "A" || $NS["ACTION"] == "D"): ?>
					<li><?
						if ($NS["ACTION"] == "A")
						{
							if ($NS["STEP"] < 6)
							{
								echo GetMessage("TI_DEACTIVATION");
							}
							elseif ($NS["STEP"] < 7)
							{
								echo "<b>" . GetMessage("TI_DEACTIVATION_PROGRESS", array("#DONE#" => intval($NS["DONE"]["DEA"]) + intval($NS["DONE"]["METER_DEA"]))) . "</b>";
							}
							else
							{
								echo GetMessage("TI_DEACTIVATION_DONE");
							}
						}
						else
						{
							if ($NS["STEP"] < 6)
							{
								echo GetMessage("TI_DELETE");
							}
							elseif ($NS["STEP"] < 7)
							{
								echo "<b>" . GetMessage("TI_DELETE_PROGRESS", array("#DONE#" => intval($NS["DONE"]["DEL"]) + intval($NS["DONE"]["METER_DEL"]))) . "</b>";
							}
							else
							{
								echo GetMessage("TI_DELETE_DONE");
							}
						}
						?></li>
				<? endif ?>
			</ul>

			<? if (defined('TSZH_IMPORT_DEBUG') && function_exists('memory_get_peak_usage')): ?>
				<p>
					Current memory
					usage: <?= number_format(memory_get_peak_usage(true) / 1024 / 1024, 2, ',', '') ?>
					�����<br/><?

					$NS['DONE']['MEM_PEAK'] = max(memory_get_peak_usage(true), $NS['DONE']['MEM_PEAK']);

					?>
					Max memory usage: <?= number_format($NS["DONE"]["MEM_PEAK"] / 1024 / 1024, 2, ',', '') ?>
					�����<br/>
				</p>
			<? endif; ?>

			<? if ($NS["STEP"] > 0): ?>
				<input type="hidden" id="NS" name="NS" value="<?= htmlspecialcharsbx(serialize($NS)) ?>">
			<? endif ?>

		<? else: ?>
			<p>
				<b>
					<? echo GetMessage("TI_DONE") ?>
				</b>
			</p>

			<?

			$arCounters = Array(
				GetMessage("TI_COUNTER_ACCOUNTS") => Array(
					"ADD",
					"UPD",
					"DEL",
					"DEA",
					"ERR",
				),
				GetMessage("TI_COUNTER_USERS") => Array(
					"USR_ADD",
					"USR_UPD",
					"USR_DEL",
					"USR_DEA",
					"USR_ERR",
				),
				GetMessage("TI_COUNTER_CHARGES") => Array(
					"CHARGE_ADD",
					"CHARGE_ERR",
				),
				GetMessage("TI_COUNTER_METERS") => Array(
					"METER_ADD",
					"METER_UPD",
					"METER_ERR",
					"METER_DEL",
					"METER_DEA",
				),
				GetMessage("TI_COUNTER_METER_VALUES") => Array(
					"METER_VALUE_ADD",
					"METER_VALUE_ERR",
				),
			);

			foreach ($arCounters as $groupName => $arGroup)
			{

				$strGroup = '';
				foreach ($arGroup as $code)
				{
					if (IntVal($NS["DONE"][$code]) > 0)
					{
						if (strstr($code, 'ERR') !== false)
						{
							$strGroup .= '<span style="color: red;">';
						}
						$strGroup .= str_replace("#COUNT#", IntVal($NS["DONE"][$code]), GetMessage("TI_C_" . $code)) . '<br />';
						if (strstr($code, 'ERR') !== false)
						{
							$strGroup .= '</span>';
						}
					}
				}
				if (strlen($strGroup) > 0)
				{
					echo "<p style=\"margin-bottom: 0;\"><strong>{$groupName}</strong>:</p><p style=\"margin-left: 2em; margin-top: 0;\">";
					echo $strGroup;
					echo "</p></div>\n\n";
				}
			}

			$strErrors = CTszhImport::GetErrors();

			if (strlen($strErrors) > 0)
			{
				$message = new CAdminMessage($strErrors);
				echo $message->Show();
			}
			?>

			<? if (defined('TSZH_IMPORT_DEBUG') && function_exists('memory_get_peak_usage')): ?>
				<p>
					Current memory
					usage: <?= number_format(memory_get_peak_usage(true) / 1024 / 1024, 2, ',', '') ?>
					�����<br/><?

					$NS['DONE']['MEM_PEAK'] = max(memory_get_peak_usage(true), $NS['DONE']['MEM_PEAK']);

					?>
					Max memory usage: <?= number_format($NS["DONE"]["MEM_PEAK"] / 1024 / 1024, 2, ',', '') ?>
					�����<br/>
				</p>
			<? endif; ?>

			<? /*<a href="<?echo htmlspecialcharsbx($urlElementAdminPage)?>"><?echo GetMessage("TI_ELEMENTS_LIST")?></a></p>*/ ?>
		<? endif; ?>
	<?endif;
	require($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/include/epilog_admin_js.php");
}

$APPLICATION->SetTitle(GetMessage("TI_TITLE"));
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

echo $demoNotice;

CUtil::InitJSCore(Array("ajax", "window"));

$rsSites = CSite::GetList($by = "ID", $order = "asc");
$arSites = array();
/** @noinspection PhpAssignmentInConditionInspection */
while ($arSite = $rsSites->GetNext())
{
	$arSites[$arSite["ID"]] = $arSite["NAME"];
}
?>
	<div id="tszh_import_result_div"></div>
<?
$aTabs = array(
	array(
		"DIV" => "edit1",
		"TAB" => GetMessage("TI_TAB"),
		"ICON" => "main_user_edit",
		"TITLE" => GetMessage("TI_TAB_TITLE"),
	),
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);
?>
	<!--suppress JSUnfilteredForInLoop -->
	<script language="JavaScript" type="text/javascript">
		<!--
		var running = false;
		var oldNS = '';
		var arSites = <?=CUtil::PhpToJsObject($arSites)?>;
		function UpdateModeChanged() {
			var bUpdateMode = getCheckedValue(document.form1.updateMode).length > 0;
			document.getElementById('outFileAction_N').disabled = !bUpdateMode;
			document.getElementById('outFileAction_A').disabled = !bUpdateMode;
			document.getElementById('outFileAction_D').disabled = !bUpdateMode;
		}
		function getCheckedValue(radioObj) {
			if (!radioObj)
				return "";
			var radioLength = radioObj.length;
			if (radioLength == undefined)
				if (radioObj.checked)
					return radioObj.value;
				else
					return "";
			for (var i = 0; i < radioLength; i++) {
				if (radioObj[i].checked) {
					return radioObj[i].value;
				}
			}
			return "";
		}

		function createTszhButtonYesAction() {
			var name = BX.util.trim(BX('create-tszh-name').value);
			var siteID = BX.util.trim(BX('create-tszh-site-id').value);

			var messReqField = '<?=CUtil::JSEscape(GetMessage("TI_CREATE_TSZH_REQ_FIELD"))?>';

			var arErrors = [];
			if (name.length <= 0)
				arErrors.push(messReqField.replace(/#FIELD#/, '<?=CUtil::JSEscape(GetMessage("TI_CREATE_TSZH_NAME"))?>'));
			if (siteID.length <= 0)
				arErrors.push(messReqField.replace(/#FIELD#/, '<?=CUtil::JSEscape(GetMessage("TI_CREATE_TSZH_SITE"))?>'));

			if (arErrors.length) {
				for (var key in arErrors)
					BX.WindowManager.Get().ShowError(arErrors[key]);
				return;
			}

			BX.WindowManager.Get().AllowClose();
			BX.WindowManager.Get().Close();

			var newNS = oldNS.replace(/s:11:"CREATE_TSZH";s:1:"Q"/, 's:11:"CREATE_TSZH";s:1:"Y"');
			newNS = newNS.replace(/s:9:"TSZH_NAME";s:\d+:"[^"]*"/, 's:9:"TSZH_NAME";s:' + name.length + ':"' + name + '"');
			newNS = newNS.replace(/s:7:"SITE_ID";s:\d+:"[^"]*"/, 's:7:"SITE_ID";s:' + siteID.length + ':"' + siteID + '"');

			BX('NS').value = newNS;
			DoNext(false);
		}
		function createTszhButtonNoAction() {
			EndImport();

			BX.WindowManager.Get().AllowClose();
			BX.WindowManager.Get().Close();
		}
		function createTszhDialog(oParams) {
			var messText = '<?=CUtil::JSEscape(GetMessage("TI_CREATE_TSZH_TEXT"))?>'.replace(/#INN#/, oParams.inn);
			var content = messText
				+ '<table class="edit-table" style="width: 100%;">'
				+ '<tr>'
				+ '<td class="field-label">'
				+ '<?=CUtil::JSEscape(GetMessage("TI_CREATE_TSZH_NAME"))?>:'
				+ '</td>'
				+ '<td class="field-value">'
				+ '<input type="text" id="create-tszh-name" size="50" maxlength="100" value="' + oParams.name + '" />'
				+ '</td>'
				+ '</tr>'
				+ '<tr>'
				+ '<td class="field-label">'
				+ '<?=CUtil::JSEscape(GetMessage("TI_CREATE_TSZH_SITE"))?>:'
				+ '</td>'
				+ '<td class="field-value">'
				+ '<select id="create-tszh-site-id">';

			for (var key in oParams.arSites) {
				content += '<option value="' + key + '">[' + key + '] ' + oParams.arSites[key] + '</option>'
			}

			content += '</select>'
				+ '</td>'
				+ '</tr>'
				+ '</table>';

			var dialog = new BX.CDialog({
				'width': 700,
				'height': 300,
				'title': '<?=CUtil::JSEscape(GetMessage("TI_TITLE"))?>',
				'content': content,
				'resizable': false,
				'buttons': [
					new BX.CWindowButton({
						'title': '<?=CUtil::JSEscape(GetMessage("TI_CREATE_TSZH_SITE_BUTTON_YES"))?>',
						'name': 'create-tszh-button-yes',
						'id': 'create-tszh-button-yes',
						'action': createTszhButtonYesAction
					}),
					new BX.CWindowButton({
						'title': '<?=CUtil::JSEscape(GetMessage("TI_CREATE_TSZH_SITE_BUTTON_NO"))?>',
						'name': 'create-tszh-button-no',
						'id': 'create-tszh-button-no',
						'action': createTszhButtonNoAction
					})
				]
			});
			dialog.DenyClose();
			dialog.Show();
		}

		function DoNext(bFirst) {
			var queryData = {
				Import: 'Y',
				sessid: '<?=bitrix_sessid()?>'
			};

			if (bFirst) {
				queryData['bFirst'] = 1;
				queryData['URL_DATA_FILE'] = document.getElementById('URL_DATA_FILE').value;
				//queryData['tszhID'] = document.getElementById('tszhID').value;
				if (document.getElementById('createUsers').checked) {
					queryData['createUsers'] = document.getElementById('createUsers').value;
				}

				queryData['updateMode'] = getCheckedValue(document.form1.updateMode);

				queryData['VERSION'] = document.getElementById('VERSION').value;

				if (document.getElementById('depersonalize').checked) {
					queryData['depersonalize'] = document.getElementById('depersonalize').value;
				}

				/*if (document.getElementById('dontUpdateUsers').checked)
				 queryData['dontUpdateUsers'] = document.getElementById('dontUpdateUsers').value;*/

				queryData['INTERVAL'] = document.getElementById('INTERVAL').value;
				var bUpdateMode = getCheckedValue(document.form1.updateMode).length > 0;
				if (!bUpdateMode) {
					if (document.getElementById('outFileAction_N').checked)
						queryData['outFileAction'] = document.getElementById('outFileAction_N').value;
					if (document.getElementById('outFileAction_A').checked)
						queryData['outFileAction'] = document.getElementById('outFileAction_A').value;
					if (document.getElementById('outFileAction_D').checked)
						queryData['outFileAction'] = document.getElementById('outFileAction_D').value;
				}
			}
			else {
				queryData['INTERVAL'] = document.getElementById('INTERVAL').value;
				queryData['NS'] = document.getElementById('NS').value;
			}

			var ajaxConfig = {
				method: 'POST', // request method: GET|POST
				dataType: 'html', // type of data loading: html|json|script
				timeout: queryData['INTERVAL'] + 5, // request timeout. 0 for browser-default
				cache: false, // whether NOT to add random addition to URL
				url: 'tszh_import.php?lang=<?=LANG?>',
				data: queryData,
				onsuccess: function (result) {
					CloseWaitWindow();
					document.getElementById('tszh_import_result_div').innerHTML = result;
					if (result.length <= 0) {
						running = false;
						document.getElementById('start_button').disabled = false;
						alert("<?=GetMessage("TI_ERROR_AJAX_EMPTY_RESULT")?>");
					}
					//Check if stop button was pressed
					if (running) {
						//Check if next step is needed
						if (document.getElementById('NS') && (document.getElementById('NS').value != oldNS)) {
							//Should prevent from step repeat when killed by timeout
							oldNS = document.getElementById('NS').value;
							if (/s:11:"CREATE_TSZH";s:1:"Q"/.test(oldNS)) {
								var inn = /s:3:"INN";s:\d+:"([^"]*)"/.exec(oldNS);
								inn = inn ? inn[1] : '';

								var tszhName = /s:9:"TSZH_NAME";s:\d+:"([^"]*)"/.exec(oldNS);
								tszhName = tszhName ? tszhName[1] : '';

								var oCreateTszhParams = {
									'inn': inn,
									'name': tszhName,
									'arSites': arSites
								};
								createTszhDialog(oCreateTszhParams);
							}
							else
								DoNext(false);
						}
						else {
							oldNS = '';
							EndImport();
						}
					}
				}
			};

			ShowWaitWindow();
			if (!BX.ajax(ajaxConfig)) {
				alert("<?=GetMessage("TI_ERROR_AJAX_GET_ERROR")?>");
			}
		}
		function StartImport() {
			running = true;
			document.getElementById('start_button').disabled = true;
			DoNext(true);
		}
		function EndImport() {
			running = false;
			document.getElementById('start_button').disabled = false;
		}
		//-->
	</script>
	<form method="POST" action="<? echo $APPLICATION->GetCurPage() ?>?lang=<? echo htmlspecialcharsbx(LANG) ?>"
	      name="form1" id="form1">
		<?

		$arrVersion = array(
			"REFERENCE" =>
				array(GetMessage('TI_VERSION_3'), GetMessage('TI_VERSION_4')),
			"REFERENCE_ID" =>
				array(3, 4),
		);

		$tabControl->Begin();
		$tabControl->BeginNextTab();
		?>
		<tr valign="top">
			<td>
				<span class="required">*</span><? echo GetMessage("TI_VERSION") ?>:
			</td>
			<td>
				<? echo SelectBoxFromArray('VERSION', $arrVersion, $VERSION, "", "", false, "form1") ?>
			</td>
		</tr>
		<tr valign="top">
			<td width="40%"><span class="required">*</span><? echo GetMessage("TI_URL_DATA_FILE") ?>:</td>
			<td width="60%">
				<input type="text" id="URL_DATA_FILE" name="URL_DATA_FILE" size="30"
				       value="<?= htmlspecialcharsbx($URL_DATA_FILE) ?>">
				<input type="button" value="<? echo GetMessage("TI_OPEN") ?>" OnClick="BtnClick()">
				<?
				CAdminFileDialog::ShowScript
				(
					Array(
						"event" => "BtnClick",
						"arResultDest" => array("FORM_NAME" => "form1", "FORM_ELEMENT_NAME" => "URL_DATA_FILE"),
						"arPath" => array("SITE" => SITE_ID, "PATH" => "/upload"),
						"select" => 'F',// F - file only, D - folder only
						"operation" => 'O',
						"showUploadTab" => true,
						"showAddToMenuTab" => false,
						"fileFilter" => 'xml',
						"allowAllFiles" => true,
						"SaveConfig" => true,
					)
				);
				?>
			</td>
		</tr>
		<tr valign="top">
			<td><?= GetMessage("TI_UPDATE_MODE") ?>:</td>
			<td>

				<label><input type="radio" name="updateMode" value="N" onchange="UpdateModeChanged();"
				              onclick="UpdateModeChanged();"<?

					if (!CUserOptions::GetOption('citrus.tszh.import', 'UpdateMode', false))
					{
						echo ' checked="checked"';
					}

					?> /><? echo GetMessage("TI_UPDATE_MODE_NO") ?></label><br/>

				<label title="<?= GetMessage("TI_NOTE_1") ?>"><input type="radio" name="updateMode" value="Y"
				                                                     onchange="UpdateModeChanged();"
				                                                     onclick="UpdateModeChanged();"<?

					if (CUserOptions::GetOption('citrus.tszh.import', 'UpdateMode', false))
					{
						echo ' checked="checked"';
					}

					?> /><? echo GetMessage("TI_UPDATE_MODE_TITLE") ?><span
						class="required"><sup>1</sup></span></label><br/>

				<label title="<?= GetMessage("TI_NOTE_2") ?>"><input type="radio" name="updateMode" value="D"
				                                                     onchange="UpdateModeChanged();"
				                                                     onclick="UpdateModeChanged();"/><? echo GetMessage("TI_UPDATE_MODE_ONLY_DEBT") ?>
					<span class="required"><sup>2</sup></span></label><br/>
			</td>
		</tr>
		<tr valign="top">
			<td><?= GetMessage("TI_USERS") ?>:</td>
			<td>
				<label style="margin-left: 7px"><input type="checkbox" name="createUsers" id="createUsers" value="Y"<?

					if (COption::GetOptionString("main", "new_user_registration", "N") == "N")
					{
						echo ' checked="checked"';
					}

					?> /><? echo GetMessage("TI_CREATE_USERS") ?></label><br/>
			</td>
		</tr>
		<tr valign="top">
			<td><? echo GetMessage("TI_ACTION") ?>:</td>
			<td><?

				$action = CUserOptions::GetOption('citrus.tszh.import', 'Action', "N");

				?>
				<input type="radio" name="outFileAction" value="N"
				       id="outFileAction_N"<?= ($action == 'N' ? ' checked="checked"' : '') ?> /><label
					for="outFileAction_N"><? echo GetMessage("TI_ACTION_NONE") ?></label><br>
				<input type="radio" name="outFileAction" value="A"
				       id="outFileAction_A"<?= ($action == 'A' ? ' checked="checked"' : '') ?> /><label
					for="outFileAction_A"><? echo GetMessage("TI_ACTION_DEACTIVATE") ?></label><br>
				<?/*
				<!--input type="radio" name="outFileAction" value="D"
				       id="outFileAction_D"<?= ($action == 'D' ? ' checked="checked"' : '') ?> /><label
					for="outFileAction_D"><? echo GetMessage("TI_ACTION_DELETE") ?></label><br-->
			*/?>
			</td>
		</tr>
		<tr valign="top">
			<td><label for="depersonalize"><? echo GetMessage("TI_DEPERSONALIZE") ?>:</label></td>
			<td><?

				$depersonalize = COption::GetOptionString('citrus.tszh.import', 'Depersonalize', "N");

				?>
				<input type="checkbox" name="depersonalize" id="depersonalize"
				       value="Y" <?= $depersonalize == 'Y' ? 'checked="checked"' : '' ?> />
			</td>
		</tr>
		<tr valign="top">
			<td><? echo GetMessage("TI_INTERVAL") ?>:</td>
			<td>
				<input type="text" id="INTERVAL" name="INTERVAL" size="5" value="<? echo intval($INTERVAL) ?>">
			</td>
		</tr>
		<? $tabControl->Buttons(); ?>
		<input type="button" id="start_button" value="<? echo GetMessage("TI_START_IMPORT") ?>"
		       onclick="StartImport();"
		       class="adm-btn-save"/>
		<input type="button" id="stop_button" value="<? echo GetMessage("TI_STOP_IMPORT") ?>" onclick="EndImport();"/>
		<? $tabControl->End(); ?>
	</form>
<?
$arGroupPolicy = CUser::GetGroupPolicy(Array(CTszh::GetTenantGroup()));
if (strlen($arGroupPolicy['PASSWORD_REQUIREMENTS']) > 0)
{
	echo BeginNote();
	echo $arGroupPolicy['PASSWORD_REQUIREMENTS'];
	echo EndNote();
}
?>
<? echo BeginNote(); ?>
	<span class="required"><sup>1</sup></span> <?= GetMessage('TI_NOTE_1') ?><br/>
	<span class="required"><sup>2</sup></span> <?= GetMessage('TI_NOTE_2') ?><br/>
<? echo EndNote(); ?>
	<script type="text/javascript">
		<!--
		UpdateModeChanged();
		//-->
	</script>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php"); ?>