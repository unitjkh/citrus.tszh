<?
// ��������� ��� ����������� �����:
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php"); // ������ ����� ������

ob_start();
if (!CTszhFunctionalityController::CheckEdition())
{
	$c = ob_get_contents();
	ob_end_clean();
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
	echo $c;
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
	return;
}
$demoNotice = ob_get_contents();
ob_end_clean();

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszh/prolog.php"); // ������ ������

// ��������� �������� ����
IncludeModuleLangFile(__FILE__);

// ������� ����� ������� �������� ������������ �� ������
$POST_RIGHT = $APPLICATION->GetGroupRight("citrus.tszh");

// ���� ��� ���� - �������� � ����� ����������� � ���������� �� ������
if ($POST_RIGHT < "V")
{
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

$UF_ENTITY = "TSZH";

$sTableID = "tbl_tszh"; // ID �������
$oSort    = new CAdminSorting($sTableID, "ID", "DESC"); // ������ ����������
$lAdmin   = new CAdminList($sTableID, $oSort); // �������� ������ ������

// ******************************************************************** //
//                           ������                                     //
// ******************************************************************** //

// *********************** CheckFilter ******************************** //
// �������� �������� ������� ��� �������� ������� � ��������� �������
function CheckFilter()
{
	global $FilterArr, $lAdmin;
	foreach ($FilterArr as $f) global $$f;

	return count($lAdmin->arFilterErrors) == 0; // ���� ������ ����, ������ false;
}

// *********************** /CheckFilter ******************************* //
// ������ �������� �������
$FilterArr = Array(
	"find",
	"find_type",
	"find_name",
	"find_site_id",
	"find_code",
);

$USER_FIELD_MANAGER->AdminListAddFilterFields($UF_ENTITY, $FilterArr);

// �������������� ������
$lAdmin->InitFilter($FilterArr);

$arFilter = Array();

// ���� ��� �������� ������� ���������, ���������� ���
if (CheckFilter())
{
	if (strlen($find) > 0 && strlen($find_type) > 0)
	{
		switch ($find_type)
		{
			case 'ID':
				$arFilter['ID'] = $find;
				break;
			case 'NAME':
				$arFilter['%NAME'] = $find;
				break;
			case 'CODE':
				$arFilter['%CODE'] = $find;
				break;
			default:
		}
	}
	if (strlen($find_name) > 0)
	{
		$arFilter['%NAME'] = $find_name;
	}
	if (strlen($find_site_id) > 0 && $find_site_id != "NOT_REF")
	{
		$arFilter["SITE_ID"] = Trim($find_site_id);
	}
	if (strlen($find_code) > 0)
	{
		$arFilter["%CODE"] = $find_code;
	}
	if (strlen($find_meter_values_start_date) > 0)
	{
		$arFilter["METER_VALUES_START_DATE"] = $find_meter_values_start_date;
	}
	if (strlen($find_meter_values_end_date) > 0)
	{
		$arFilter["METER_VALUES_END_DATE"] = $find_meter_values_end_date;
	}

	$USER_FIELD_MANAGER->AdminListAddFilter($UF_ENTITY, $arFilter);

	// get rid of empty filter fields
	foreach ($arFilter as $key => $value)
	{
		if (is_array($value))
		{
			foreach ($value as $key2 => $value2)
			{
				if (strlen(trim($value2)) == 0)
				{
					unset($value[$key2]);
				}
			}
			if (count($value) <= 0)
			{
				unset($arFilter[$key]);
			}
		} else
		{
			if (strlen(trim($value)) == 0)
			{
				unset($arFilter[$key]);
			}
		}
	}
}
// ******************************************************************** //
//                ��������� �������� ��� ���������� ������              //
// ******************************************************************** //

// ���������� ����������������� ���������
if ($lAdmin->EditAction() && $POST_RIGHT >= "W")
{
	// ������� �� ������ ���������� ���������
	foreach ($FIELDS as $ID => $arFields)
	{
		if (!$lAdmin->IsUpdated($ID))
		{
			continue;
		}

		// �������� ��������� ������� ��������
		$DB->StartTransaction();
		$ID = IntVal($ID);
		if ($arData = CTszh::GetByID($ID))
		{
			/*foreach($arFields as $key=>$value)
				$arData[$key]=$value;*/
			$USER_FIELD_MANAGER->AdminListPrepareFields($UF_ENTITY, $arFields);
			if (!CTszh::Update($ID, $arFields))
			{
				$lAdmin->AddGroupError(GetMessage("TSZH_ERROR_UPDATE"), $ID);
				$DB->Rollback();
			}
		} else
		{
			$lAdmin->AddGroupError(GetMessage("TSZH_ERROR_UPDATE_NOT_FOUND"), $ID);
			$DB->Rollback();
		}
		$DB->Commit();
	}
}

// ��������� ��������� � ��������� ��������
if (($arID = $lAdmin->GroupAction()) && $POST_RIGHT >= "W")
{
	// ���� ������� "��� ���� ���������"
	if ($_REQUEST['action_target'] == 'selected')
	{
		$arSort   = Array();
		$arSortBy = explode('|', $by);
		foreach ($arSortBy as $sBy)
		{
			$arSort[$sBy] = $order;
		}

		$rsData = CTszh::GetList($arSort, $arFilter);
		while ($arRes = $rsData->Fetch())
			$arID[] = $arRes['ID'];
	}

	// ������� �� ������ ���������
	foreach ($arID as $ID)
	{
		if (strlen($ID) <= 0)
		{
			continue;
		}
		$ID = IntVal($ID);

		// ��� ������� �������� �������� ��������� ��������
		switch ($_REQUEST['action'])
		{
			// ��������
			case "delete":
				@set_time_limit(0);
				$DB->StartTransaction();
				if (!CTszh::Delete($ID))
				{
					$DB->Rollback();
					$lAdmin->AddGroupError(GetMessage("TSZH_ERROR_DELETE"), $ID);
				}
				$DB->Commit();
				break;
		}
	}
}

// ******************************************************************** //
//                ������� ��������� ������                              //
// ******************************************************************** //


$arSort   = Array();
$arSortBy = explode('|', $by);
foreach ($arSortBy as $sBy)
{
	$arSort[$sBy] = $order;
}

// �������� ���� �� ������ ���������� ��� �������
$arTszhFilter = $arTszhRight = array();
if (CModule::IncludeModule("vdgb.portaltszh") && TSZH_PORTAL === true && !$USER->IsAdmin())
{
	global $USER;
	$arGroups = $USER->GetUserGroupArray();

	$rsPerms = \Vdgb\PortalTszh\PermsTable::getList(
		array(
			"filter" => array("@GROUP_ID" => $arGroups)
		)
	);

	while ($arPerms = $rsPerms->fetch())
	{
		if ($arPerms["PERMS"] >= "R")
		{
			$arTszhRight[$arPerms["TSZH_ID"]] = $arPerms["PERMS"];
		}
	}

	$arTszhFilter["@ID"] = array_keys($arTszhRight);
}

$rsData = CTszh::GetList(
	$arSort,
	array_merge($arFilter, $arTszhFilter),
	false,
	false,
	Array('*', 'UF_*')
);

// ����������� ������ � ��������� ������ CAdminResult
$rsData = new CAdminResult($rsData, $sTableID);

// ���������� CDBResult �������������� ������������ ���������.
$rsData->NavStart();

// �������� ����� ������������� ������� � �������� ������ $lAdmin
$lAdmin->NavText($rsData->GetNavPrint(GetMessage("TSZH_NAV")));

// ******************************************************************** //
//                ���������� ������ � ������                            //
// ******************************************************************** //

$arAdminListHeaders = array(
	array(
		"id" => "ID",
		"content" => "ID",
		"sort" => "ID",
		"align" => "right",
		"default" => true,
	),
	array(
		"id" => "NAME",
		"content" => GetMessage("TSZH_F_NAME"),
		"sort" => "NAME",
		"align" => "left",
		"default" => true,
	),
	array(
		"id" => "SITE_ID",
		"content" => GetMessage("TSZH_F_SITE_ID"),
		"sort" => "SITE_ID",
		"align" => "left",
		"default" => true,
	),
	array(
		"id" => "ACCOUNTS",
		"content" => GetMessage("TSZH_F_ACCOUNTS"),
		"align" => "left",
		"default" => true,
	),
	array(
		"id" => "SERVICES",
		"content" => GetMessage("TSZH_F_SERVICES"),
		"align" => "left",
		"default" => true,
	),
	array(
		"id" => "PERIODS",
		"content" => GetMessage("TSZH_F_PERIODS"),
		"align" => "left",
		"default" => true,
	),
	array(
		"id" => "CODE",
		"content" => GetMessage("TSZH_F_CODE"),
		"sort" => "CODE",
		"align" => "left",
		"default" => false,
	),
	array(
		"id" => "TIMESTAMP_X",
		"content" => GetMessage("TSZH_F_TIMESTAMP"),
		"sort" => "TIMESTAMP",
		"align" => "left",
		"default" => true,
	),

	array(
		"id" => "INN",
		"content" => GetMessage("TSZH_F_INN"),
		"sort" => "INN",
		"align" => "left",
		"default" => false,
	),
	array(
		"id" => "KPP",
		"content" => GetMessage("TSZH_F_KPP"),
		"sort" => "KPP",
		"align" => "left",
		"default" => false,
	),
	array(
		"id" => "RSCH",
		"content" => GetMessage("TSZH_F_RSCH"),
		"sort" => "RSCH",
		"align" => "left",
		"default" => false,
	),
	array(
		"id" => "BANK",
		"content" => GetMessage("TSZH_F_BANK"),
		"sort" => "BANK",
		"align" => "left",
		"default" => false,
	),
	array(
		"id" => "KSCH",
		"content" => GetMessage("TSZH_F_KSCH"),
		"sort" => "KSCH",
		"align" => "left",
		"default" => false,
	),
	array(
		"id" => "BIK",
		"content" => GetMessage("TSZH_F_BIK"),
		"sort" => "BIK",
		"align" => "left",
		"default" => false,
	),
	array(
		"id" => "METER_VALUES_START_DATE",
		"content" => GetMessage("TSZH_F_METER_VALUES_START_DATE"),
		"sort" => "METER_VALUES_START_DATE",
		"align" => "left",
		"default" => false,
	),
	array(
		"id" => "METER_VALUES_END_DATE",
		"content" => GetMessage("TSZH_F_METER_VALUES_END_DATE"),
		"sort" => "METER_VALUES_END_DATE",
		"align" => "left",
		"default" => false,
	),
);
$USER_FIELD_MANAGER->AdminListAddHeaders($UF_ENTITY, $arAdminListHeaders);
$lAdmin->AddHeaders($arAdminListHeaders);

$arCounters = Array(
	"A" => Array(),
	"S" => Array(),
	"P" => Array(),
);

$rs = CTszhAccount::GetList(Array(), Array(), Array("TSZH_ID"));
while ($ar = $rs->Fetch())
{
	$arCounters['A'][$ar["TSZH_ID"]] = $ar["CNT"];
}
$rs = CTszhService::GetList(Array(), Array(), Array("TSZH_ID"));
while ($ar = $rs->Fetch())
{
	$arCounters['S'][$ar["TSZH_ID"]] = $ar["CNT"];
}
$rs = CTszhPeriod::GetList(Array(), Array(), Array("TSZH_ID"));
while ($ar = $rs->Fetch())
{
	$arCounters['P'][$ar["TSZH_ID"]] = $ar["CNT"];
}

while ($arRes = $rsData->NavNext(true, "f_")):

	$row =& $lAdmin->AddRow($f_ID, $arRes);

	$row->AddViewField('SITE_ID', $arRes["SITE_ID"]);

	$row->AddInputField("NAME", array("size" => 50));
	$row->AddInputField("CODE", array("size" => 20));
	$row->AddViewField("TIMESTAMP_X", $arRes["TIMESTAMP_X"]);

	$row->AddInputField("INN", array("size" => 12));
	$row->AddInputField("KPP", array("size" => 9));
	$row->AddInputField("RSCH", array("size" => 14));
	$row->AddInputField("BANK", array("size" => 20));
	$row->AddInputField("KSCH", array("size" => 14));
	$row->AddInputField("BIK", array("size" => 9));

	$row->AddInputField("METER_VALUES_START_DATE", array("size" => 2));
	$row->AddInputField("METER_VALUES_END_DATE", array("size" => 2));

	$row->AddViewField("ACCOUNTS", '<a href="tszh_account_list.php?lang=' . LANG . '&amp;find_tszh=' . $f_ID . '" title="' . GetMessage("TSZH_ACCOUNTS_LIST") . ' ' . $f_NAME . '">' . ($arCounters['A'][$f_ID] > 0 ? $arCounters['A'][$f_ID] : 0) . '</a>');
	$row->AddViewField("SERVICES", '<a href="tszh_services.php?lang=' . LANG . '&amp;find_tszh=' . $f_ID . '" title="' . GetMessage("TSZH_SERVICES_LIST") . ' ' . $f_NAME . '">' . ($arCounters['S'][$f_ID] > 0 ? $arCounters['S'][$f_ID] : 0) . '</a>');
	$row->AddViewField("PERIODS", '<a href="tszh_periods.php?lang=' . LANG . '&amp;find_tszh=' . $f_ID . '" title="' . GetMessage("TSZH_PERIODS_LIST") . ' ' . $f_NAME . '">' . ($arCounters['P'][$f_ID] > 0 ? $arCounters['P'][$f_ID] : 0) . '</a>');

	$USER_FIELD_MANAGER->AddUserFields($UF_ENTITY, $arRes, $row);


	// ���������� ����������� ����
	$arActions = Array();

	if ($POST_RIGHT >= "W")
	{
		// �������������� ��������
		$arActions[] = array(
			"ICON" => "edit",
			"TEXT" => GetMessage("TSZH_M_EDIT"),
			"DEFAULT" => "Y",
			"ACTION" => $lAdmin->ActionRedirect("tszh_edit.php?ID=" . $f_ID . '&lang=' . LANG)
		);

		// ������� �����
		$arActions[] = array(
			"ICON" => "list",
			"TEXT" => GetMessage("TSZH_M_ACCOUNTS"),
			"ACTION" => $lAdmin->ActionRedirect("tszh_account_list.php?lang=" . LANG . "&amp;find_tszh=" . $f_ID),
		);

		// �������
		$arActions[] = array(
			"ICON" => "list",
			"TEXT" => GetMessage("TSZH_M_PERIODS"),
			"ACTION" => $lAdmin->ActionRedirect("tszh_periods.php?lang=" . LANG . "&amp;find_tszh=" . $f_ID),
		);

		// �������� ��������
		$arActions[] = array(
			"ICON" => "delete",
			"TEXT" => GetMessage("TSZH_M_DELETE"),
			"ACTION" => "if(confirm('" . GetMessage("TSZH_M_DELETE_CONFIRM") . "?')) " . $lAdmin->ActionDoGroup($f_ID, "delete")
		);
	}

	// ������� �����������
	//$arActions[] = array("SEPARATOR"=>true);

	// ���� ��������� ������� - �����������, �������� �����.
	if (is_set($arActions[count($arActions) - 1], "SEPARATOR"))
	{
		unset($arActions[count($arActions) - 1]);
	}

	// �������� ����������� ���� � ������
	$row->AddActions($arActions);

endwhile;

// ������ �������
$lAdmin->AddFooter(
	array(
		array("title" => GetMessage("TSZH_CNT_TOTAL") . ":", "value" => $rsData->SelectedRowsCount()), // ���-�� ���������
		array("counter" => true, "title" => GetMessage("TSZH_CNT_SELECTED") . ":", "value" => "0"), // ������� ��������� ���������
	)
);

// ��������� ��������
$lAdmin->AddGroupActionTable(Array(
	"delete" => GetMessage("MAIN_ADMIN_LIST_DELETE"), // ������� ��������� ��������
	/*  "activate"=>GetMessage("MAIN_ADMIN_LIST_ACTIVATE"), // ������������ ��������� ��������
	  "deactivate"=>GetMessage("MAIN_ADMIN_LIST_DEACTIVATE"), // �������������� ��������� ��������*/
));

// ******************************************************************** //
//                ���������������� ����                                 //
// ******************************************************************** //

// ���������� ���� �� ������ ������ - ����������
$aContext = array(
	array(
		"TEXT" => GetMessage("TSZH_C_ADD"),
		"LINK" => "tszh_edit.php?lang=" . LANG,
		"TITLE" => GetMessage("TSZH_C_ADD_ACCOUNT_TITLE"),
		"ICON" => "btn_new",
	),
);

if (TSZH_PORTAL === true && !$USER->IsAdmin())
{
	unset($aContext);
}

// � ��������� ��� � ������
$lAdmin->AddAdminContextMenu($aContext);

// ******************************************************************** //
//                �����                                                 //
// ******************************************************************** //

// �������������� �����
$lAdmin->CheckListMode();

// ��������� ��������� ��������
$APPLICATION->SetTitle(GetMessage("TSZH_PAGE_TITLE"));

// �� ������� ��������� ���������� ������ � �����
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

echo $demoNotice;

CTszhFunctionalityController::checkSubscribe(true);

// ******************************************************************** //
//                ����� �������                                         //
// ******************************************************************** //

$arFindFields = Array(
	GetMessage("TSZH_F_NAME"),
	GetMessage("TSZH_F_SITE_ID"),
	GetMessage("TSZH_F_CODE"),
	GetMessage("TSZH_F_METER_VALUES_START_DATE"),
	GetMessage("TSZH_F_METER_VALUES_END_DATE"),
);
$USER_FIELD_MANAGER->AddFindFields($UF_ENTITY, $arFindFields);
// �������� ������ �������
$oFilter = new CAdminFilter(
	$sTableID . "_filter",
	$arFindFields
);
?>
	<form name="find_form" method="get" action="<? echo $APPLICATION->GetCurPage(); ?>">
		<? $oFilter->Begin(); ?>
		<tr>
			<td><b><?= GetMessage("TSZH_FIND") ?>:</b></td>
			<td>
				<input type="text" size="25" name="find" value="<? echo htmlspecialcharsbx($find) ?>" title="">
				<?
				$arr = array(
					"reference" => array(
						"ID",
						GetMessage("TSZH_F_CODE"),
						GetMessage("TSZH_F_NAME"),
					),
					"reference_id" => array(
						"ID",
						'CODE',
						'NAME',
					)
				);
				echo SelectBoxFromArray("find_type", $arr, $find_type, "", "");
				?>
			</td>
		</tr>
		<tr>
			<td><?= GetMessage("TSZH_F_NAME") ?>:</td>
			<td>
				<input type="text" name="find_name" size="47" value="<? echo htmlspecialcharsbx($find_name) ?>">
			</td>
		</tr>
		<tr>
			<td><? echo GetMessage("TSZH_F_SITE_ID"); ?>:</td>
			<td>
				<select name="find_site_id">
					<option value=""><?= htmlspecialcharsex(GetMessage("TSZH_F_ALL")) ?></option>
					<?
					$dbSitesList = CLang::GetList($b1 = "sort", $o1 = "asc");
					while ($arSitesList = $dbSitesList->Fetch())
					{
						?>
					<option
						value="<?= $arSitesList["LID"] ?>"<? if ($arSitesList["LID"] == $find_site_id) echo " selected"; ?>>
						[<?= htmlspecialcharsex($arSitesList["LID"]) ?>]
						&nbsp;<?= htmlspecialcharsex($arSitesList["NAME"]) ?></option><?
					}
					?>
				</select>
			</td>
		</tr>
		<tr>
			<td><?= GetMessage("TSZH_F_CODE") ?>:</td>
			<td>
				<input type="text" name="find_code" size="47" value="<? echo htmlspecialcharsbx($find_code) ?>">
			</td>
		</tr>
		<tr>
			<td><?= GetMessage("TSZH_F_METER_VALUES_START_DATE") ?>:</td>
			<td>
				<input type="text" name="find_meter_values_start_date" size="5"
				       value="<? echo htmlspecialcharsbx($find_meter_values_start_date) ?>">
			</td>
		</tr>
		<tr>
			<td><?= GetMessage("TSZH_F_METER_VALUES_END_DATE") ?>:</td>
			<td>
				<input type="text" name="find_meter_values_end_date" size="5"
				       value="<? echo htmlspecialcharsbx($find_meter_values_end_date) ?>">
			</td>
		</tr>
		<?

		$USER_FIELD_MANAGER->AdminListShowFilter($UF_ENTITY);

		$oFilter->Buttons(array("table_id" => $sTableID, "url" => $APPLICATION->GetCurPage(), "form" => "find_form"));
		$oFilter->End();

		?>

	</form>

<?

// ������� ������� ������ ���������
$lAdmin->DisplayList();
?>

<?
// ���������� ��������
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>