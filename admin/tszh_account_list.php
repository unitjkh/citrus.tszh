<?php
/**
 * Bitrix vars
 * @global CUser $USER
 * @global CMain $APPLICATION
 * @global CDatabase $DB
 * @global CUserTypeManager $USER_FIELD_MANAGER
 * @global string $by
 * @global string $order
 */

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszh/include.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszh/prolog.php");

IncludeModuleLangFile(__FILE__);

ob_start();
if (!CTszhFunctionalityController::CheckEdition())
{
	$c = ob_get_contents();
	ob_end_clean();
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
	echo $c;
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");

	return;
}
$demoNotice = ob_get_contents();
ob_end_clean();

// ������� ����� ������� �������� ������������ �� ������
$moduleRights = $APPLICATION->GetGroupRight("citrus.tszh");
// ���� ��� ���� - �������� � ����� ����������� � ���������� �� ������
if ($moduleRights <= "D")
{
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

$entity_id = "TSZH_ACCOUNT";

$sTableID = "tbl_tszh_accounts";
$oSort = new CAdminSorting($sTableID, "ID", "desc");
// $lAdmin = new CAdminList($sTableID, $oSort);
$lAdmin = new CAdminUiList($sTableID, $oSort);

// �������� ���� ��� �������
$arTszhFilter = $arTszhRight = $arListFilter = array();
if (CTszhFunctionalityController::isPortal() && !$USER->IsAdmin())
{
	global $USER;
	$arGroups = $USER->GetUserGroupArray();

	$rsPerms = \Vdgb\PortalTszh\PermsTable::getList(
		array(
			"filter" => array("@GROUP_ID" => $arGroups)
		)
	);

	while ($arPerms = $rsPerms->fetch())
	{
		if ($arPerms["PERMS"] >= "R")
		{
			$arTszhRight[$arPerms["TSZH_ID"]] = $arPerms["PERMS"];
		}
	}

	$arTszhFilter["@ID"] = $arListFilter["@TSZH_ID"] = array_keys($arTszhRight);
}

// ������ �����������
$arTszhList = Array();
$rsTszh = CTszh::GetList(Array("NAME" => "ASC"), $arTszhFilter, false, false, Array("ID", "NAME"));
while ($arTszh = $rsTszh->Fetch())
{
	$arTszhList[$arTszh['ID']] = $arTszh['NAME'];
}

// ��������� ������� ��� ������ � �������
$arHeaders = array(
	array("id" => "ID", "align" => "right", "default" => true, "type" => "int"),
	array("id" => "TSZH_ID", "align" => "left", "default" => false, "type" => "list", "items" => $arTszhList),
	array("id" => "XML_ID", "align" => "left", "default" => true, "type" => "string"),
	array("id" => "EXTERNAL_ID", "align" => "left", "default" => false, "type" => "string"),
	array("id" => "TYPE", "align" => "left", "default" => false, "type" => "list", "items" => \Citrus\Tszh\Types\AccountType::getTitles()),
	array("id" => "NAME", "align" => "left", "default" => true, "type" => "string"),
	array("id" => "USER_ID", "align" => "left", "default" => false, "type" => "user"),
	array("id" => "CITY", "align" => "left", "default" => false, "type" => "string"),
	array("id" => "REGION", "align" => "left", "default" => false, "type" => "string"),
	array("id" => "SETTLEMENT", "align" => "left", "default" => false, "type" => "string"),
	array("id" => "STREET", "align" => "left", "default" => false, "type" => "string"),
	array("id" => "HOUSE", "align" => "left", "default" => false, "type" => "string"),
	array("id" => "FLAT", "align" => "left", "default" => false, "type" => "string"),
	array("id" => "FLAT_ABBR", "align" => "left", "default" => false, "type" => "string"),
	array("id" => "FLAT_TYPE", "align" => "left", "default" => false, "type" => "string"),
	array("id" => "AREA", "align" => "right", "default" => false, "type" => "int"),
	array("id" => "LIVING_AREA", "align" => "right", "default" => false, "type" => "int"),
	array("id" => "HOUSE_AREA", "align" => "right", "default" => false, "type" => "int"),
	array("id" => "HOUSE_ROOMS_AREA", "align" => "right", "default" => false, "type" => "int"),
	array("id" => "HOUSE_COMMON_PLACES_AREA", "align" => "right", "default" => false, "type" => "int"),
	array("id" => "PEOPLE", "align" => "right", "default" => false, "type" => "int"),
	array("id" => "REGISTERED_PEOPLE", "align" => "right", "default" => false, "type" => "int"),
	array("id" => "EXEMPT_PEOPLE", "align" => "right", "default" => false, "type" => "int"),
	array("id" => "ADDRESS_FULL", "align" => "left", "default" => true, "type" => "string"),
);
$fieldTitles = IncludeModuleLangFile($_SERVER['DOCUMENT_ROOT'] . BX_ROOT . '/modules/citrus.tszh/entities/account.php', false, true);
foreach ($arHeaders as &$header)
{
	if (!array_key_exists("content", $header))
	{
		$header["content"] = $fieldTitles["CITRUS_TSZH_ACCOUNT_" . $header["id"]];
	}
	if (!array_key_exists("sort", $header))
	{
		$header["sort"] = ToLower($header["id"]);
	}
}
// ******************************************************************** //
//                           ������                                     //
// ******************************************************************** //

// *********************** CheckFilter ******************************** //
// �������� �������� ������� ��� �������� ������� � ��������� �������
/*function CheckFilter()
{
	global $filterFields, $lAdmin;
	foreach ($filterFields as $value)
	{
		global $$value;
	}

	return true;
}*/

// *********************** /CheckFilter ******************************* //
// ������ �������� �������
$arFilterFields = TszhAdminFilterParams($arHeaders);
// �������������� ������
$lAdmin->InitFilter($arFilterFields);

// ����� ������ ��� �������
$filterFields = TszhAdminFilterParamsV2($arHeaders, array('ID', 'XML_ID'));
// $USER_FIELD_MANAGER->AdminListAddFilterFields($entity_id, $filterFields);
$USER_FIELD_MANAGER->AdminListAddFilterFieldsV2($entity_id, $filterFields);

// ���������� ������� (������ 2)
$arFilter = array();
$lAdmin->AddFilter($filterFields, $arFilter);
$USER_FIELD_MANAGER->AdminListAddFilterV2($entity_id, $arFilter, $sTableID, $filterFields);

// ���� ��� �������� ������� ���������, ���������� ���
/*$arFilter = array();
if (CheckFilter() && !is_set($_REQUEST, 'del_filter'))
{
	// �������� ������ ���������� ��� ������� �� ������ �������� �������
	TszhAdminMakeFilter($arFilter, $arHeaders);
	// ���������� � ����� ���������������� �����
	$USER_FIELD_MANAGER->AdminListAddFilter($entity_id, $arFilter);
}*/

// ******************************************************************** //
//                ��������� �������� ��������                           //
// ******************************************************************** //
if (isset($_REQUEST["edit_form_del"]) && IntVal($_REQUEST["edit_form_del"]) == 1 && check_bitrix_sessid())
{
	if (isset($_REQUEST["ID"]) && IntVal($_REQUEST["ID"]) > 0)
	{
		@set_time_limit(0);

		$arItem = CTszhAccount::GetByID($_REQUEST["ID"]);
		if (is_array($arItem))
		{
			if (CTszhAccount::Delete($arItem['ID']))
			{
				$messageOK = GetMessage("TSZH_DELETE_OK");
			}
			else
			{
				$lAdmin->AddGroupError(GetMessage("TSZH_DELETE_ERROR"), $ID);
			}
		}
		else
		{
			$lAdmin->AddGroupError(GetMessage("TSZH_DELETE_ERROR") . ". " . GetMessage("TSZH_DELETE_ITEM_NOT_FOUND", Array("#ID#" => $ID)), $ID);
		}
	}
}


// ******************************************************************** //
//                ��������� �������� ��� ���������� ������              //
// ******************************************************************** //
// ���������� ����������������� ���������
if ($lAdmin->EditAction() && $moduleRights >= "W" && check_bitrix_sessid())
{
	// ������� �� ������ ���������� ���������
	foreach ($FIELDS as $ID => $arFields)
	{
		if (!$lAdmin->IsUpdated($ID))
		{
			continue;
		}

		// �������� ��������� ������� ��������
		$ID = IntVal($ID);

		$arItem = CTszhAccount::GetByID($ID);
		if (is_array($arItem))
		{
			$USER_FIELD_MANAGER->AdminListPrepareFields($entity_id, $arFields);
			if (!CTszhAccount::Update($ID, $arFields))
			{
				$strError = '';
				if ($ex = $APPLICATION->GetException())
				{
					$strError = $ex->GetMessage();
				}
				$lAdmin->AddGroupError(GetMessage("TSZH_GROUP_SAVE_ERROR") . " " . $strError, $ID);
			}
		}
		else
		{
			$lAdmin->AddGroupError(GetMessage("TSZH_GROUP_SAVE_ERROR") . " " . GetMessage("TSZH_GROUP_SAVE_ERROR_ITEM_NOT_FOUND"), $ID);
		}
	}
}

// ��������� ��������� � ��������� ��������
if (($arID = $lAdmin->GroupAction()) && $moduleRights >= "W" && check_bitrix_sessid())
{
	// ���� ������� "��� ���� ���������"
	if ($_REQUEST['action_target'] == 'selected')
	{
		$rsItems = CTszhAccount::GetList(Array(), $arFilter, false, false, Array("ID"));
		$arID = Array();
		while ($arItem = $rsItems->Fetch())
		{
			$arID[] = $arItem['ID'];
		}
	}

	@set_time_limit(0);

	// ������� �� ������ ���������
	foreach ($arID as $ID)
	{
		if (strlen($ID) <= 0)
		{
			continue;
		}

		$ID = IntVal($ID);

		// ��� ������� �������� �������� ��������� ��������
		switch ($_REQUEST['action'])
		{
			// ��������
			case "delete":
				$arItem = CTszhAccount::GetByID($ID);
				if (is_array($arItem))
				{
					if (CTszhAccount::Delete($ID))
					{
						$messageOK = GetMessage("qroup_del_ok");
					}
					else
					{
						$lAdmin->AddGroupError(GetMessage("TSZH_DELETE_ERROR"), $ID);
					}
				}
				else
				{
					$lAdmin->AddGroupError(GetMessage("TSZH_DELETE_ERROR") . " " . GetMessage("TSZH_DELETE_ITEM_NOT_FOUND"), $ID);
				}
				break;
		}
	}
}

// ******************************************************************** //
//                ������� ��������� ������                              //
// ******************************************************************** //

$USER_FIELD_MANAGER->AdminListAddHeaders($entity_id, $arHeaders);
$lAdmin->AddHeaders($arHeaders);

// visible columns
$arVisibleColumns = $_visibleColumns = $lAdmin->GetVisibleHeaderColumns();

// additional fields required for formatting
if (in_array('NAME', $arVisibleColumns))
{
	$arVisibleColumns[] = "USER_ID";
}
if (in_array('TSZH_ID', $arVisibleColumns))
{
	$arVisibleColumns[] = 'TSZH_NAME';
}
if (in_array('USER_ID', $arVisibleColumns))
{
	$arVisibleColumns[] = 'USER_FULL_NAME';
	$arVisibleColumns[] = 'USER_LOGIN';
}
$arVisibleColumns = array_unique(array_merge($arVisibleColumns, Array('ID')));

// proper page navigation using limit in SQL
/*$arNavParams = CAdminResult::GetNavParams(CAdminResult::GetNavSize($sTableID));
$arNav = Array(
	'nPageSize' => $arNavParams['SIZEN'],
	'iNumPage' => $arNavParams['PAGEN'],
	'bShowAll' => $arNavParams['SHOW_ALL'],
);*/

$arNav = array(
	'nPageSize' => CAdminUiResult::GetNavSize($sTableID),
);

$arSort = Array();
$arSortBy = explode('|', $by);
foreach ($arSortBy as $sBy)
{
	$arSort[$sBy] = $order;
}

// ����������� � ������� ����� �� ��������� ��� ��������� �����
foreach ($filterFields as $field)
{
	if ((!isset($field['type']) || 'string' === $field['type']) && isset($arFilter[$field['id']]))
	{
		$arFilter['%' . $field['id']] = $arFilter[$field['id']];
		unset($arFilter[$field['id']]);
	}
}

// ������� ������ �������
$rsData = CTszhAccount::GetList(
	$arSort,
	array_merge($arFilter, $arListFilter),
	false,
	$_REQUEST["mode"] == "excel" ? false : $arNav,
	$arVisibleColumns
);

// ����������� ������ � ��������� ������ CAdminResult
// $rsData = new CAdminResult($rsData, $sTableID);
$rsData = new CAdminUiResult($rsData, $sTableID);

// ���������� CDBResult �������������� ������������ ���������.
$rsData->NavStart();

// �������� ����� ������������� ������� � �������� ������ $lAdmin
// $lAdmin->NavText($rsData->GetNavPrint(GetMessage("CITRUS_TSZH_ACCOUNTS")));
$lAdmin->SetNavigationParams($rsData);

// ******************************************************************** //
//                ���������� ������ � ������                            //
// ******************************************************************** //

// ������ ������������ ��������� ��� ���������������� �����
// $template_path = $_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/interface/navigation.php";
// ����� ������������� �������
// $lAdmin->NavText($rsData->GetNavPrint(GetMessage("CITRUS_TSZH_ACCOUNTS"), true, "", $template_path, array('action', 'sessid')));

$accountTypes = \Citrus\Tszh\Types\AccountType::getTitles();
while ($arRes = $rsData->NavNext(true, "f_"))
{
	// ������� ������. ��������� - ��������� ������ CAdminListRow
	$row =& $lAdmin->AddRow($f_ID, $arRes);

	$row->AddInputField('XML_ID');

	$row->AddViewField('ID', '<a href="tszh_account_edit.php?ID=' . $arRes['ID'] . '&lang=' . LANGUAGE_ID . '" title="' . GetMessage("CITRUS_TSZH_EDIT_ACCOUNT") . '">' . $arRes['ID'] . '</a>');

	// ����� �������� ����������� �������� ��� ��������� � �������������� ������
	foreach ($arHeaders as $arH)
	{
		if ($arH["type"] == 'checkbox')
		{
			$row->AddViewField($arH["id"], $arRes[$arH["id"]] == 'Y' ? GetMessage("TSZH_YES") : GetMessage("TSZH_NO"));
		}
	}

	if (in_array('TSZH_ID', $arVisibleColumns))
	{
		$strId = '[<a href="/bitrix/admin/tszh_edit.php?lang=' . LANG . '&ID=' . $arRes["TSZH_ID"] . '">' . $arRes["TSZH_ID"] . '</a>] ' . (array_key_exists($arRes["TSZH_ID"], $arTszhList) ? $arTszhList[$arRes["TSZH_ID"]] : '');
		$row->AddViewField('TSZH_ID', $strId);
	}

	if (in_array('USER_ID', $arVisibleColumns))
	{
		if ($arRes['USER_ID'] > 0)
		{
			$sFullName = trim('(' . $arRes['USER_LOGIN'] . ') ' . $arRes["USER_FULL_NAME"]);
			$row->AddViewField('USER_ID', '[<a href="user_edit.php?ID=' . $arRes['USER_ID'] . '&lang=' . LANGUAGE_ID . '" title="' . GetMessage("CITRUS_TSZH_VIEW_USER_PROFILE") . '">' . $arRes['USER_ID'] . '</a>] ' . $sFullName);
		}
		else
		{
			$row->AddViewField('USER_ID', '');
		}
	}

	if (in_array('NAME', $arVisibleColumns))
	{
		if ($arRes['USER_ID'] > 0)
		{
			$sFullName = trim('(' . $arRes['USER_LOGIN'] . ') ' . $arRes["USER_FULL_NAME"]);
			$row->AddViewField('NAME', '[<a href="user_edit.php?ID=' . $arRes['USER_ID'] . '&lang=' . LANGUAGE_ID . '" title="' . GetMessage("CITRUS_TSZH_VIEW_USER_PROFILE") . '">' . $arRes['USER_ID'] . '</a>] ' . $arRes["NAME"]);
		}
	}

	if (in_array('TYPE', $arVisibleColumns))
	{
		if ($arRes["TYPE"] > 0 && array_key_exists($arRes["TYPE"], $accountTypes))
		{
			$row->AddViewField('TYPE', $accountTypes[$arRes["TYPE"]]);
		}
		else
		{
			$row->AddViewField('TYPE', '');
		}
	}

	$USER_FIELD_MANAGER->AddUserFields($entity_id, $arRes, $row);

	// ���������� ����������� ����
	$arActions = Array();

	// �������� ��������
	if ($moduleRights >= "W")
	{
		// �������������� ��������
		$arActions[] = array(
			"ICON" => "edit",
			"DEFAULT" => true,
			"TEXT" => GetMessage("TSZH_GROUP_EDIT_TITLE"),
			"ACTION" => $lAdmin->ActionRedirect("tszh_account_edit.php?ID=" . $f_ID . '&lang=' . LANG)
		);
		$arActions[] = array(
			"ICON" => "view",
			"DEFAULT" => false,
			"TEXT" => GetMessage("CITRUS_TSZH_ACCOUNTS_PERIODS"),
			"ACTION" => $lAdmin->ActionRedirect("tszh_account_period_list.php?LANG=" . LANG . "&find_account=" . $f_ID)
		);
		$arActions[] = array(
			"ICON" => "view",
			"DEFAULT" => false,
			"TEXT" => GetMessage("CITRUS_TSZH_ACCOUNTS_METERS"),
			"ACTION" => $lAdmin->ActionRedirect("tszh_meters.php?LANG=" . LANG . "&set_filter=Y&find_account_id=" . $f_ID)
		);
		$arActions[] = array(
			"ICON" => "delete",
			"TEXT" => GetMessage("TSZH_GROUP_DELETE_TITLE"),
			"ACTION" => "if(confirm('" . GetMessage('TSZH_GROUP_DELETE_CONFIRM') . "')) " . $lAdmin->ActionDoGroup($f_ID, "delete")
		);
	}

	// ������� �����������
	$arActions[] = array("SEPARATOR" => true);

	if ($USER->IsAdmin() && ($arRes['USER_ID'] > 0))
	{
		$arActions[] = array(
			"DEFAULT" => false,
			"TEXT" => GetMessage("CITRUS_TSZH_ACCOUNTS_AUTH"),
			"LINK" => "user_admin.php?lang=" . LANGUAGE_ID . "&ID=" . $arRes['USER_ID'] . "&action=authorize&" . bitrix_sessid_get()
		);
	}

	// ���� ��������� ������� - �����������, �������� �����.
	if (is_set($arActions[count($arActions) - 1], "SEPARATOR"))
	{
		unset($arActions[count($arActions) - 1]);
	}

	// �������� ����������� ���� � ������
	$row->AddActions($arActions);
}

// ������ �������
/*$lAdmin->AddFooter(
	array(
		array("title" => GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value" => $rsData->SelectedRowsCount()), // ���-�� ���������
		array("counter" => true, "title" => GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value" => "0"), // ������� ��������� ���������
	)
);*/

// ��������� ��������
$lAdmin->AddGroupActionTable(Array(
	// "delete" => GetMessage("MAIN_ADMIN_LIST_DELETE"), // ������� ��������� ��������
	"edit" => true,
	"delete" => true,
	"for_all" => true,
));

// ******************************************************************** //
//                ���������������� ����                                 //
// ******************************************************************** //

// ���������� ���� �� ������ ������ - ���������� ��������
$aContext = array(
	array(
		"TEXT" => GetMessage("CITRUS_TSZH_ADD_ACCOUNT"),
		"LINK" => "tszh_account_edit.php?lang=" . LANG,
		"TITLE" => GetMessage("CITRUS_TSZH_ADD_ACCOUNT_TITLE"),
		"ICON" => "btn_new",
	),
);

// � ��������� ��� � ������
$lAdmin->AddAdminContextMenu($aContext);

// ******************************************************************** //
//                �����                                                 //
// ******************************************************************** //
if (($arID = $lAdmin->GroupAction()) && $moduleRights >= "W" && isset($messageOK) && $_REQUEST['action'] == "delete")
{
	$message = new CAdminMessage(array("MESSAGE" => $messageOK, "TYPE" => "OK"), false);
	echo $message->Show();
}

// �������������� �����
$lAdmin->CheckListMode();

$APPLICATION->SetTitle(GetMessage("CITRUS_TSZH_ACCOUNTS"));

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

echo $demoNotice;

if (isset($message) && isset($_REQUEST["edit_form_del"]))
{
	$message = new CAdminMessage(array("MESSAGE" => $messageOK, "TYPE" => "OK"), false);
	echo $message->Show();
}

// ******************************************************************** //
//                ����� �������                                         //
// ******************************************************************** //

// ����� �������
$lAdmin->DisplayFilter($filterFields);

// ������� ������� ������ ���������
$lAdmin->DisplayList();

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>