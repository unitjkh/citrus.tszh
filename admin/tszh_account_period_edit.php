<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

use \Citrus\Tszh\Types\ReceiptType;

ob_start();
if (!CTszhFunctionalityController::CheckEdition())
{
	$c = ob_get_contents();
	ob_end_clean();
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
	echo $c;
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");

	return;
}
$demoNotice = ob_get_contents();
ob_end_clean();


$modulePermissions = $APPLICATION->GetGroupRight("citrus.tszh");
if ($modulePermissions <= "R")
{
	$APPLICATION->AuthForm(GetMessage("TSZH_ACCESS_DENIED"));
}

require_once($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/citrus.tszh/include.php");
IncludeModuleLangFile(__FILE__);

$APPLICATION->AddHeadScript('//ajax.googleapis.com/ajax/libs/angularjs/1.0.6/angular.min.js');

$strRedirect = BX_ROOT . "/admin/tszh_account_period_edit.php?lang=" . LANG;
$strRedirectList = BX_ROOT . "/admin/tszh_account_period_list.php?lang=" . LANG;

ClearVars();
$errorMessage = "";
$bVarsFromForm = false;
$message = null;

$ID = IntVal($ID);

$aTabs = array(
	array("DIV" => "edit1", "TAB" => GetMessage("T_TAB1"), "ICON" => "citrus.tszh", "TITLE" => GetMessage("T_TAB1_TITLE")),
	array("DIV" => "edit2", "TAB" => GetMessage("T_TAB2"), "ICON" => "citrus.tszh", "TITLE" => GetMessage("T_TAB2_TITLE")),
);

$UF_ENTITY = "TSZH_ACCOUNT_PERIOD";
$UF_CHARGE_ENTITY = "TSZH_CHARGE";

$tabControl = new CAdminForm("accountPeriodEdit", $aTabs);

if ($REQUEST_METHOD == "POST" && strlen($Update) > 0 && $modulePermissions >= "W" && check_bitrix_sessid())
{
	if ($modulePermissions < "W")
	{
		$errorMessage .= GetMessage("TSZH_NO_PERMISSIONS_CHANGE") . "<br />";
	}

	if ($ID > 0)
	{
		if (!($arAccountPeriod = CTszhAccountPeriod::GetByID($ID)))
		{
			$errorMessage .= str_replace("#ID#", $ID, GetMessage("APE_ERROR_ACCOUNTPERIOD_NOT_FOUND")) . "<br />";
		}
	}

	if (strlen($errorMessage) <= 0)
	{
		$arFields = Array(
			"BARCODES" => array(),
            "BARCODES_INSURANCE" => array(),
			"DEBT_BEG" => $_REQUEST['DEBT_BEG'],
			"DEBT_END" => $_REQUEST['DEBT_END'],
			"DEBT_PREV" => $_REQUEST['DEBT_PREV'],
			"PREPAYMENT" => $_REQUEST['PREPAYMENT'],
			"CREDIT_PAYED" => $_REQUEST['CREDIT_PAYED'],
			"SUMM_TO_PAY" => $_REQUEST['SUMM_TO_PAY'],
			"SUM_PAYED" => $_REQUEST['SUM_PAYED'],
			"LAST_PAYMENT" => $_REQUEST['LAST_PAYMENT'],
		);

		if (isset($_REQUEST["TYPE"]))
		{
			$arFields["TYPE"] = $_REQUEST["TYPE"];
		}

		$barcodeTypes = array_keys(CTszhBarCode::getTypes());
		foreach ($_REQUEST["barcode_value"] as $key => $val)
		{
			$arFields["BARCODES"][$key] = array(
				"TYPE" => $barcodeTypes[$_REQUEST["barcode_type"][$key]],
				"VALUE" => $val,
			);
		}
        foreach ($_REQUEST["barcodeInsurance_value"] as $key => $val)
        {
            $arFields["BARCODES_INSURANCE"][$key] = array(
                "TYPE" => $barcodeTypes[$_REQUEST["barcodeInsurance_type"][$key]],
                "VALUE" => $val,
            );
        }

		if (is_set($_REQUEST, "DATE_SENT_RESET") && $_REQUEST["DATE_SENT_RESET"] == "Y")
		{
			$arFields["DATE_SENT"] = false;
		}

		if ($ID <= 0)
		{
			$arFields['ACCOUNT_ID'] = $_REQUEST['ACCOUNT_ID'];
			$arFields['PERIOD_ID'] = $_REQUEST['PERIOD_ID'];
		}

		$USER_FIELD_MANAGER->EditFormAddFields($UF_ENTITY, $arFields);
		if ($ID > 0)
		{
			$bResult = CTszhAccountPeriod::Update($ID, $arFields);
		}
		else
		{
			$ID = CTszhAccountPeriod::Add($arFields);
			$bResult = $ID > 0;
		}
		if (!$bResult)
		{
			$ex = $APPLICATION->GetException();
			if ($ex && strlen($str = $ex->GetString()) > 0)
			{
				$errorMessage .= $str . ".<br>";
			}
			else
			{
				$errorMessage .= GetMessage("APE_ERROR_PERIOD_SAVE") . "<br />";
			}
		}
		else
		{
			if (!is_array($arAccountPeriod))
			{
				$arAccountPeriod = CTszhAccountPeriod::GetByID($ID);
			}

			$dbCharges = CTszhCharge::GetList(Array("SORT" => "ASC", "ID" => "ASC"), Array(
				"ACCOUNT_PERIOD_ID" => $arAccountPeriod["ID"],
			));
			$arCharges = Array();
			while ($arCharge = $dbCharges->GetNext())
			{
				$arCharges[$arCharge['ID']] = $arCharge;
			}

			$arUpdateCharges = Array();
			$arAddCharges = Array();
			$arDeleteCharges = Array();
			foreach ($_REQUEST['CHARGES'] as $key => $val)
			{
				if (is_numeric($key) && array_key_exists($key, $arCharges))
				{
					if ($val["DEL"] == '1')
					{
						$arDeleteCharges[] = $key;
					}
					else
					{
						$arUpdateCharge = Array(
							"SERVICE_ID" => $val["SERVICE_ID"],
							"COMPONENT" => $val["COMPONENT"] == "Y" ? "Y" : "N",
							"DEBT_ONLY" => $val["DEBT_ONLY"] == "Y" ? "Y" : "N",
							"DEBT_BEG" => $val['DEBT_BEG'],
							"DEBT_END" => $val['DEBT_END'],
							"AMOUNT" => $val['AMOUNT'],
							"SUMM" => $val['SUMM'],
							"SUMM_PAYED" => $val['SUMM_PAYED'],
							"SUMM2PAY" => $val['SUMM2PAY'],
							"SORT" => $val['SORT'],
							"CORRECTION" => $val['CORRECTION'],
							"COMPENSATION" => $val['COMPENSATION'],

							"HAMOUNT" => $val['HAMOUNT'],
							"HSUMM" => $val['HSUMM'],
						);
						foreach (array_keys($val) as $valKey)
						{
							if (strlen($valKey) > 3 && ToLower(substr($valKey, 0, 3)) == 'uf_')
							{
								$arUpdateCharge[$valKey] = $val[$valKey];
							}
						}
						$arUpdateCharges[$key] = $arUpdateCharge;
					}
				}
				elseif (!is_numeric($key) && ($val["SERVICE_ID"] > 0 || $val["AMOUNT"] > 0 || $val["SUMM"] > 0 || $val["SUMM_PAYED"] > 0 || $val["SUMM2PAY"] > 0))
				{
					$arAddCharge = Array(
						"ACCOUNT_PERIOD_ID" => $arAccountPeriod["ID"],

						"SERVICE_ID" => $val["SERVICE_ID"],
						"COMPONENT" => $val["COMPONENT"] == "Y" ? "Y" : "N",
						"DEBT_ONLY" => $val["DEBT_ONLY"] == "N" ? "N" : "Y",
						"AMOUNT" => $val['AMOUNT'],
						"DEBT_BEG" => $val['DEBT_BEG'],
						"DEBT_END" => $val['DEBT_END'],
						"SUMM" => $val['SUMM'],
						"SUMM_PAYED" => $val['SUMM_PAYED'],
						"SUMM2PAY" => $val['SUMM2PAY'],
						"SORT" => $val['SORT'],
						"CORRECTION" => $val['CORRECTION'],
						"COMPENSATION" => $val['COMPENSATION'],

						"HAMOUNT" => $val['HAMOUNT'],
						"HSUMM" => $val['HSUMM'],
					);
					foreach (array_keys($val) as $valKey)
					{
						if (strlen($valKey) > 3 && ToLower(substr($valKey, 0, 3)) == 'uf_')
						{
							$arAddCharge[$valKey] = $val[$valKey];
						}
					}
					$arAddCharges[] = $arAddCharge;
				}
			}

			foreach ($arUpdateCharges as $iChargeID => $arUpdateCharge)
			{
				if (!CTszhCharge::Update($iChargeID, $arUpdateCharge))
				{
					if ($ex = $APPLICATION->GetException())
					{
						$errorMessage .= GetMessage("APE_ERROR_SAVING_CHARGE") . ":<br />" . $ex->GetString() . ".<br>";
					}
					else
					{
						$errorMessage .= GetMessage("APE_ERROR_SAVING_CHARGE") . ".<br />";
					}
				}
			}
			foreach ($arAddCharges as $arAddCharge)
			{
				if (!CTszhCharge::Add($arAddCharge))
				{
					if ($ex = $APPLICATION->GetException())
					{
						$errorMessage .= GetMessage("APE_ERROR_SAVING_CHARGE") . ":<br>" . $ex->GetString() . ".<br>";
					}
					else
					{
						$errorMessage .= GetMessage("APE_ERROR_SAVING_CHARGE") . ".<br>";
					}
				}
			}
			foreach ($arDeleteCharges as $iDeleteCharge)
			{
				if (!CTszhCharge::Delete($iDeleteCharge))
				{
					$errorMessage .= GetMessage("APE_ERROR_DELETING_CHARGE") . ".<br />";
				}
			}

		}
	}

	if (strlen($errorMessage) <= 0)
	{
		if (strlen($apply) <= 0)
		{
			LocalRedirect($strRedirectList . GetFilterParams("filter_", false));
		}
		else
		{
			LocalRedirect($strRedirect . "&ID=" . $ID . "&" . $tabControl->ActiveTabParam());
		}
	}
	else
	{
		$bVarsFromForm = true;
	}
}

//require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/citrus.tszh/prolog.php");

if ($ID > 0)
{
	$APPLICATION->SetTitle(str_replace('#ID#', $ID, GetMessage("TSZH_ACCOUNT_PERIOD_EDIT")));
}
else
{
	$APPLICATION->SetTitle(GetMessage("TSZH_ACCOUNT_PERIOD_ADD"));
}

require_once($DOCUMENT_ROOT . BX_ROOT . "/modules/main/include/prolog_admin_after.php");

echo $demoNotice;

if (intval($ID) > 0):

	$dbAccountPeriod = CTszhAccountPeriod::GetList(
		Array(),
		Array("ID" => $ID),
		false,
		Array("nTopCount" => 1),
		Array("*")
	);

	if (!$arAccountPeriod = $dbAccountPeriod->ExtractFields("str_"))
	{
		if ($modulePermissions < "W")
		{
			$errorMessage .= GetMessage("TSZH_NO_PERMISSIONS_CHANGE") . "<br />";
		}
		$ID = 0;
	}
	else
	{
		$dbCharges = CTszhCharge::GetList(
			Array("SORT" => "ASC", "ID" => "ASC"),
			Array(
				"ACCOUNT_PERIOD_ID" => $arAccountPeriod["ID"],
			),
			false,
			false,
			array('*', 'UF_*')
		);
		$arCharges = Array();
		while ($arCharge = $dbCharges->GetNext())
		{
			$arCharges[] = $arCharge;
		}
	}
endif;

$dbServices = CTszhService::GetList(Array("NAME" => "ASC"), Array("ACTIVE" => "Y"));
$arServicesRef = Array(
	"REFERENCE" => Array(""),
	"REFERENCE_ID" => Array(0),
);
$arTariffs = Array();
while ($arService = $dbServices->GetNext())
{
	$arTariffs[$arService['ID']] = $arService["TARIFF"];
	$arServicesRef["REFERENCE_ID"][] = $arService['ID'];
	$arServicesRef["REFERENCE"][] = "[{$arService['ID']}] {$arService['NAME']}";
}

if ($bVarsFromForm)
{
	$DB->InitTableVarsForEdit("b_tszh_accounts_period", "", "str_");
}


if (strlen($errorMessage) > 0)
{
	CAdminMessage::ShowMessage(Array("DETAILS" => $errorMessage, "TYPE" => "ERROR", "MESSAGE" => GetMessage("TSZH_ERROR_LIST"), "HTML" => true));
}


/**
 *   CAdminForm()
 **/

$aMenu = array(
	array(
		"TEXT" => GetMessage("T_MENU_LIST"),
		"LINK" => "/bitrix/admin/tszh_account_period_list.php?lang=" . LANG . GetFilterParams("filter_"),
		"ICON" => "btn_list",
		"TITLE" => GetMessage("T_MENU_LIST_TITLE"),
	),
);

if ($ID > 0 && $modulePermissions >= "W")
{
	$aMenu[] = array("SEPARATOR" => "Y");

	$aMenu[] = array(
		"TEXT" => GetMessage("T_MENU_ADD"),
		"LINK" => "/bitrix/admin/tszh_account_period_edit.php?lang=" . LANG . GetFilterParams("filter_"),
		"ICON" => "btn_new",
		"TITLE" => GetMessage("T_MENU_ADD_TITLE"),
	);

	if ($modulePermissions >= "W")
	{
		$aMenu[] = array(
			"TEXT" => GetMessage("T_MENU_DELETE"),
			"LINK" => "javascript:if(confirm('" . GetMessage("T_MENU_DELETE_CONFIRM") . "')) window.location='/bitrix/admin/tszh_account_period_list.php?ID=" . $ID . "&action=delete&lang=" . LANG . "&" . bitrix_sessid_get() . "#tb';",
			"WARNING" => "Y",
			"ICON" => "btn_delete",
			"TITLE" => GetMessage("T_MENU_DELETE_TITLE"),
		);

		if (tszhCheckMinEdition("standard"))
		{
			$aMenu[] = array(
				"TEXT" => GetMessage("T_MENU_SEND"),
				"LINK" => "javascript:if(confirm('" . GetMessage("T_MENU_SEND_CONFIRM") . "')) window.location='/bitrix/admin/tszh_account_period_list.php?ID=" . $ID . "&action=send&lang=" . LANG . "&" . bitrix_sessid_get() . "#tb';",
				"WARNING" => "Y",
				//"ICON"	=> "",
				"TITLE" => GetMessage("T_MENU_SEND_TITLE"),
			);
		}
	}

}
if (!empty($aMenu))
{
	$aMenu[] = array("SEPARATOR" => "Y");
}
$link = DeleteParam(array("mode"));
$link = $GLOBALS["APPLICATION"]->GetCurPage() . "?mode=settings" . ($link <> "" ? "&" . $link : "");
$aMenu[] = array(
	"TEXT" => GetMessage("T_MENU_SETTINGS"),
	"TITLE" => GetMessage("T_MENU_SETTINGS_TITLE"),
	"LINK" => "javascript:" . $tabControl->GetName() . ".ShowSettings('" . htmlspecialcharsbx(CUtil::addslashes($link)) . "')",
	"ICON" => "btn_settings",
);

$context = new CAdminContextMenu($aMenu);
$context->Show();

if (method_exists($USER_FIELD_MANAGER, 'showscript'))
{
	$tabControl->BeginPrologContent();
	echo $USER_FIELD_MANAGER->ShowScript();
	$tabControl->EndPrologContent();
}

// ============ FORM PROLOG ==================
$tabControl->BeginEpilogContent();
echo GetFilterHiddens("filter_");
?>
	<input type="hidden" name="Update" value="Y"/>
	<input type="hidden" name="lang" value="<? echo LANG ?>"/>
	<input type="hidden" name="ID" value="<? echo $ID ?>"/>
<?
echo bitrix_sessid_post();
$tabControl->EndEpilogContent();
// ===========================================

$tabControl->Begin(array(
	"FORM_ACTION" => $APPLICATION->GetCurPage(),
	"FORM_ATTRIBUTES" => "method=\"POST\" enctype=\"multipart/form-data\" ng-app",
));
$tabControl->BeginNextFormTab();

if ($ID > 0)
{
	$strFullName = IntVal($arAccountPeriod['USER_ID']) > 0 ? CTszhAccount::GetFullName($arAccountPeriod['USER_ID']) : $arAccountPeriod["ACCOUNT_NAME"];

	$tabControl->AddViewField("ACCOUNT_ID", GetMessage('TF_ACCOUNT_ID'), '[<a href="/bitrix/admin/tszh_account_edit.php?lang=' . LANG . '&amp;ID=' . $arAccountPeriod["ACCOUNT_ID"] . '" title="' . GetMessage("APE_M_EDIT_ACCOUNT") . '">' . $arAccountPeriod['ACCOUNT_ID'] . '</a>] ' . $strFullName);
	$tabControl->AddViewField("PERIOD_ID", GetMessage('TF_PERIOD_ID'), '[<a href="/bitrix/admin/tszh_periods_edit.php?lang=' . LANG . '&amp;ID=' . $arAccountPeriod["PERIOD_ID"] . '" title="' . GetMessage("APE_M_EDIT_PERIOD") . '">' . $arAccountPeriod['PERIOD_ID'] . '</a>] ' . CTszhPeriod::Format($arAccountPeriod["PERIOD_DATE"]));
}
else
{
	$tabControl->BeginCustomField("ACCOUNT_ID", GetMessage("TF_ACCOUNT_ID"), true);
	?>
	<tr>
		<td width="40%"><span class="required">*</span><?=GetMessage("TF_ACCOUNT_ID")?>:</td>
		<td width="60%"><?=tszhLookup('ACCOUNT_ID', $arAccountPeriod['ACCOUNT_ID'], Array(
				"type" => 'account',
				"formName" => 'accountPeriodEdit_form',
			));?></td>
	</tr>
	<?
	$tabControl->EndCustomField("ACCOUNT_ID", '<input type="hidden" name="ACCOUNT_ID" value="' . $arAccountPeriod['ACCOUNT_ID'] . '">');

	$tabControl->BeginCustomField("PERIOD_ID", GetMessage("TF_PERIOD_ID"), true);
	?>
	<tr>
		<td width="40%"><span class="required">*</span><?=GetMessage("TF_PERIOD_ID")?>:</td>
		<td width="60%"><?=tszhLookup('PERIOD_ID', $arAccountPeriod['PERIOD_ID'], Array(
				"type" => 'period',
				"formName" => 'accountPeriodEdit_form',
			));?></td>
	</tr>
	<?
	$tabControl->EndCustomField("PERIOD_ID", '<input type="hidden" name="PERIOD_ID" value="' . $arAccountPeriod['PERIOD_ID'] . '">');

}

$tabControl->AddDropDownField("TYPE", GetMessage("CITRUS_TSZH_ACCOUNT_PERIOD_TYPE"), true, ReceiptType::getTitles(), $arAccountPeriod["TYPE"]);

$tabControl->BeginCustomField("BARCODE", GetMessage("TF_BARCODE"), false);
$barcodeTypes = array();
foreach (CTszhBarCode::getTypes() as $barcodeType => $barcodeName)
{
	$barcodeTypes[] = array(
		'type' => $barcodeType,
		'name' => $barcodeName,
	);
}

?>
	<tr>
		<td width="40%"><?=GetMessage("TF_BARCODE")?>:</td>
		<td width="60%">

			<table cellspacing="0" cellpadding="0" border="0" class="internal" ng-controller="barcodeCtrl">
				<tbody>
				<tr class="heading" ng-show="barcodes.length">
					<td><?=GetMessage("CITRUS_TSZH_ACC_PERIOD_BARCODE")?></td>
					<td><?=GetMessage("CITRUS_TSZH_ACC_PERIOD_BARCODE_TYPE")?></td>
					<td><?=GetMessage("CITRUS_TSZH_ACC_PERIOD_BARCODE_DEL")?></td>
				</tr>
				<tr ng-repeat="barcode in barcodes">
					<td><input type="text" ng-model="barcode.VALUE" name="barcode_value[]" size="50"></td>
					<td><select ng-model="barcode.TYPE" name="barcode_type[]"
					            ng-options="barcodeType.type as barcodeType.name for barcodeType in barcodeTypes"></select></td>
					<td>[<a href="javascript:void(0)" ng-click="removeBarcode($index)">x</a>]</td>
				</tr>
				</tbody>
				<tfoot>
				<tr>
					<td colspan="3">
						<a href="javascript:void(0)" ng-click="addBarcode()"
						   style="border-bottom: 1px dashed #2675d7; text-decoration: none"><?=GetMessage("CITRUS_TSZH_ACC_PERIOD_BARCODE_ADD")?></a>
					</td>
				</tr>
				</tfoot>
			</table>
			<script>
                function barcodeCtrl($scope) {
                    $scope.barcodes = <?=CUtil::PhpToJSObject($arAccountPeriod["BARCODES"])?>;
                    $scope.barcodeText = '';
                    $scope.barcodeTypes = <?=CUtil::PhpToJSObject($barcodeTypes)?>;

                    $scope.addBarcode = function () {
                        $scope.barcodes.push({value: $scope.barcodeText, type: 'code128'});
                        $scope.barcodeText = '';
                    };
                    $scope.removeBarcode = function (index) {
                        $scope.barcodes.splice(index, 1);
                    };
                }
			</script>
		</td>
	</tr>
<?
$barcodesHidden = array();
if (isset($arAccountPeriod["BARCODES"]) && is_array($arAccountPeriod["BARCODES"]))
{
	foreach ($arAccountPeriod["BARCODES"] as $barcode)
	{
		$barcodesHidden[] = '<input type="hidden" name="barcode_value[]" value="' . $barcode["VALUE"] . '">';
		$barcodesHidden[] = '<input type="hidden" name="barcode_type[]" value="' . $barcode["TYPE"] . '">';
	}
}
$tabControl->EndCustomField("BARCODE", implode("\n", $barcodesHidden));

$tabControl->BeginCustomField("BARCODE_INSURANCE", GetMessage("TF_BARCODE_INSURANCE"), false);
$barcodeInsuranceTypes = array();
foreach (CTszhBarCode::getTypesInsurance() as $barcodeInsuranceType => $barcodeInsuranceName)
{
    $barcodeInsuranceTypes[] = array(
        'type' => $barcodeInsuranceType,
        'name' => $barcodeInsuranceName,
    );
}

?>
    <tr>
        <td width="40%"><?=GetMessage("TF_BARCODE_INSURANCE")?>:</td>
        <td width="60%">

            <table cellspacing="0" cellpadding="0" border="0" class="internal" ng-controller="barcodeInsuranceCtrl">
                <tbody>
                <tr class="heading" ng-show="barcodesInsurance.length">
                    <td><?=GetMessage("CITRUS_TSZH_ACC_PERIOD_BARCODE")?></td>
                    <td><?=GetMessage("CITRUS_TSZH_ACC_PERIOD_BARCODE_TYPE")?></td>
                    <td><?=GetMessage("CITRUS_TSZH_ACC_PERIOD_BARCODE_DEL")?></td>
                </tr>
                <tr ng-repeat="barcodeInsurance in barcodesInsurance">
                    <td><input type="text" ng-model="barcodeInsurance.VALUE" name="barcodeInsurance_value[]" size="50"></td>
                    <td><select ng-model="barcodeInsurance.TYPE" name="barcodeInsurance_type[]"
                                ng-options="barcodeInsuranceType.type as barcodeInsuranceType.name for barcodeInsuranceType in barcodeInsuranceTypes"></select></td>
                    <td>[<a href="javascript:void(0)" ng-click="removeBarcodeInsurance($index)">x</a>]</td>
                </tr>
                </tbody>
                <tfoot>
                <tr>
                    <td colspan="3">
                        <a href="javascript:void(0)" ng-click="addBarcodeInsurance()"
                           style="border-bottom: 1px dashed #2675d7; text-decoration: none"><?=GetMessage("CITRUS_TSZH_ACC_PERIOD_BARCODE_ADD")?></a>
                    </td>
                </tr>
                </tfoot>
            </table>
            <script>
                function barcodeInsuranceCtrl($scope) {
                    $scope.barcodesInsurance = <?=CUtil::PhpToJSObject($arAccountPeriod["BARCODES_INSURANCE"])?>;
                    $scope.barcodeInsuranceText = '';
                    $scope.barcodeInsuranceTypes = <?=CUtil::PhpToJSObject($barcodeInsuranceTypes)?>;

                    $scope.addBarcodeInsurance = function () {
                        $scope.barcodesInsurance.push({value: $scope.barcodeInsuranceText, type: 'code128'});
                        $scope.barcodeInsuranceText = '';
                    };
                    $scope.removeBarcodeInsurance = function (index) {
                        $scope.barcodesInsurance.splice(index, 1);
                    };
                }
            </script>
        </td>
    </tr>
<?
$barcodesInsuranceHidden = array();
if (isset($arAccountPeriod["BARCODES_INSURANCE"]) && is_array($arAccountPeriod["BARCODES_INSURANCE"]))
{
	foreach ($arAccountPeriod["BARCODES_INSURANCE"] as $barcodeInsurance)
	{
		$barcodesInsuranceHidden[] = '<input type="hidden" name="barcodeInsurance_value[]" value="' . $barcodeInsurance["VALUE"] . '">';
		$barcodesInsuranceHidden[] = '<input type="hidden" name="barcodeInsurance_type[]" value="' . $barcodeInsurance["TYPE"] . '">';
	}
}
$tabControl->EndCustomField("BARCODE_INSURANCE", implode("\n", $barcodesHidden));

$tabControl->AddEditField("DEBT_BEG", GetMessage("TF_DEBT_BEG"), true, array("size" => 10), $str_DEBT_BEG);
$tabControl->AddEditField("DEBT_END", GetMessage("TF_DEBT_END"), true, array("size" => 10), $str_DEBT_END);
$tabControl->AddEditField("DEBT_PREV", GetMessage("TF_DEBT_PREV"), false, array("size" => 10), $str_DEBT_PREV);
$tabControl->AddEditField("PREPAYMENT", GetMessage("TF_PREPAYMENT"), false, array("size" => 10), $str_PREPAYMENT);
$tabControl->AddEditField("CREDIT_PAYED", GetMessage("TF_CREDIT_PAYED"), false, array("size" => 10), $str_CREDIT_PAYED);
$tabControl->AddEditField("SUM_PAYED", GetMessage("TF_SUM_PAYED"), false, array("size" => 10), $str_SUM_PAYED);
$tabControl->AddEditField("SUMM_TO_PAY", GetMessage("TF_SUMM_TO_PAY"), false, array("size" => 10), $str_SUMM_TO_PAY);
$tabControl->AddCalendarField("LAST_PAYMENT", GetMessage("TF_LAST_PAYMENT"), $str_LAST_PAYMENT, false);

$tabControl->AddViewField("DATE_SENT", GetMessage("TF_DATE_SENT"), $str_DATE_SENT ? $str_DATE_SENT . ' <label><input type="checkbox" name="DATE_SENT_RESET" value="Y"> ' . GetMessage("TF_DATE_SENT_RESET") . '</label>' : GetMessage("TF_DATE_SENT_NEVER"));

$tabControl->BeginCustomField("CORRECTIONS", GetMessage('TF_CORRECTIONS'));
if ($ID > 0)
{
	$rsCorrections = CTszhAccountCorrections::GetList(Array("ID" => "ASC"), Array("ACCOUNT_PERIOD_ID" => $ID), false, false, Array('*'));
	if ($rsCorrections->SelectedRowsCount() > 0)
	{
		?>
		<tr id="tr_CORRECTIONS">
			<td valign="top"><?=$tabControl->GetCustomLabelHTML()?></td>
			<td>
				<table cellspacing="0" cellpadding="0" border="0" class="internal">
					<tr class="heading">
						<td><?=GetMessage('TF_CORRECTIONS_SERVICE')?></td>
						<td><?=GetMessage('TF_CORRECTIONS_GROUNDS')?></td>
						<td><?=GetMessage('TF_CORRECTIONS_SUMM')?></td>
					</tr>
					<?
					while ($arCorrection = $rsCorrections->GetNext())
					{
						?>
						<tr>
							<td><?=$arCorrection["SERVICE"]?></td>
							<td><?=$arCorrection["GROUNDS"]?></td>
							<td><?=$arCorrection["SUMM"]?>&nbsp;<?=GetMessage("TF_CURRENCY_SYM")?></td>
						</tr>
						<?
					}
					?>
				</table>
				<br>
			</td>
		</tr>
		<?
	}
}
$tabControl->EndCustomField("CORRECTIONS", '');

if ($ID > 0)
{
	//$tabControl->AddSection("SECTION_INSTALLMENTS", GetMessage("TF_INSTALLMENTS"), false);
	$tabControl->BeginCustomField("INSTALLMENTS", GetMessage("TF_INSTALLMENTS"));
	$rsInstallments = CTszhAccountInstallments::GetList(Array("ID" => "ASC"), Array("ACCOUNT_PERIOD_ID" => $ID), false, false, Array('*'));
	if ($rsInstallments->SelectedRowsCount() > 0)
	{
		?>
		<tr id="tr_INSTALLMENTS">
			<td valign="top"><?=$tabControl->GetCustomLabelHTML()?></td>
			<td <?//colspan="2"
			?>>
				<table cellspacing="0" cellpadding="0" border="0" class="internal">
					<tr class="heading">
						<td rowspan="2"><?=GetMessage("TF_INSTALLMENT_SERVICES")?></td>
						<td rowspan="2"><?=GetMessage("TF_INSTALLMENT_PAYED")?></td>
						<td rowspan="2"><?=GetMessage("TF_INSTALLMENT_PREV_PAYED")?></td>
						<td colspan="2"><?=GetMessage("TF_INSTALLMENT_PERCENTS")?></td>
						<td rowspan="2"><?=GetMessage("TF_INSTALLMENT_SUMM2PAY")?></td>
					</tr>
					<tr class="heading">
						<td><?=GetMessage("TF_INSTALLMENT_PERCENT")?></td>
						<td><?=GetMessage("TF_INSTALLMENT_SUMM_RATED")?></td>
					</tr>
					<?
					while ($arInstallment = $rsInstallments->GetNext())
					{
						?>
						<tr>
							<td><?=$arInstallment["SERVICE"]?></td>
							<td style="text-align: right;"><?=$arInstallment["SUMM_PAYED"]?></td>
							<td style="text-align: right;"><?=$arInstallment["SUMM_PREV_PAYED"]?></td>
							<td style="text-align: right;"><?=$arInstallment["PERCENT"]?></td>
							<td style="text-align: right;"><?=$arInstallment["SUMM_RATED"]?></td>
							<td style="text-align: right;"><?=$arInstallment["SUMM2PAY"]?></td>
						</tr>
						<?
					}
					?>
				</table>
				<br>
			</td>
		</tr>
		<?
	}
	$tabControl->EndCustomField("INSTALLMENTS", "");
}

if ($ID > 0)
{
	$tabControl->AddSection("SECTION_CONTRACTORS", GetMessage("TF_CONTRACTORS"), false);
	$tabControl->BeginCustomField("CONTRACTORS", GetMessage('TF_CONTRACTORS'));
	$rsContractors = CTszhAccountContractor::GetList(Array("ID" => "ASC"), Array("ACCOUNT_PERIOD_ID" => $ID), false, false, Array('*'));
	if ($rsContractors->SelectedRowsCount() > 0)
	{
		?>
		<tr id="tr_CONTRACTORS">
			<td colspan="2">
				<table cellspacing="0" cellpadding="0" border="0" class="internal">
					<tr class="heading">
						<td><?=GetMessage('TF_CONTRACTOR_NAME')?></td>
						<td><?=GetMessage('TF_CONTRACTOR_DEBT_BEG')?></td>
						<td><?=GetMessage('TF_CONTRACTOR_PAYED')?></td>
						<td><?=GetMessage('TF_CONTRACTOR_PENALTIES')?></td>
						<td><?=GetMessage('TF_CONTRACTOR_SUMM_CHARGED')?></td>
						<td><?=GetMessage('TF_CONTRACTOR_SUMM')?></td>
						<td><?=GetMessage('TF_CONTRACTOR_BILLING')?></td>
					</tr>
					<?
					while ($arContractor = $rsContractors->GetNext())
					{
						?>
						<tr>
							<td><a href="tszh_contractors_edit.php?LANG=<?=LANG?>&amp;ID=<?=$arContractor['CONTRACTOR_ID']?>"
							       target="_blank"><?=$arContractor["CONTRACTOR_NAME"]?></a></td>
							<td style="text-align: right;"><?=$arContractor["DEBT_BEG"]?></td>
							<td style="text-align: right;"><?=$arContractor["SUMM_PAYED"]?></td>
							<td style="text-align: right;"><?=$arContractor["PENALTIES"]?></td>
							<td style="text-align: right;"><?=$arContractor["SUMM_CHARGED"]?></td>
							<td style="text-align: right;"><?=$arContractor["SUMM"]?></td>
							<td><?=$arContractor["CONTRACTOR_BILLING"]?></td>
						</tr>
						<?
					}
					?>
				</table>
				<br>
			</td>
		</tr>
		<?
	}
	$tabControl->EndCustomField("CONTRACTORS", "");
}

if (
	(count($USER_FIELD_MANAGER->GetUserFields($UF_ENTITY)) > 0) ||
	($USER_FIELD_MANAGER->GetRights($UF_ENTITY) >= "W")
)
{
	$tabControl->AddSection('USER_FIELDS', GetMessage("AEF_USER_FIELDS"));
	$tabControl->ShowUserFields($UF_ENTITY, $str_ID, $bVarsFromForm);
}

$tabControl->BeginNextFormTab();
//$tabControl->AddSection("CHARGES_SECTION", GetMessage("CHARGES"));

$tabControl->BeginCustomField("CHARGES", GetMessage('TF_CHARGES'));
?>
	<tr id="tr_CHARGES">
		<td colspan="2">
			<style type="text/css">
				#chargesTable select {
					max-width: 200px;
				}
			</style>

			<?
			$arChargesUserFields = $USER_FIELD_MANAGER->GetUserFields($UF_CHARGE_ENTITY, 0, LANGUAGE_ID); ?>
			<table cellspacing="0" cellpadding="0" border="0" align="center" class="internal" id="chargesTable">
				<tr class="heading">
					<td><?=GetMessage('TFC_ID')?></td>
					<td><?=GetMessage('TFC_SORT')?></td>
					<td title="<?=GetMessage('TF_COMPONENT_TITLE')?>"><?=GetMessage('TF_COMPONENT')?></td>
					<td title="<?=GetMessage('TF_DEBT_ONLY_TITLE')?>"><?=GetMessage('TF_DEBT_ONLY')?></td>
					<td><?=GetMessage('TFC_SERVICE_ID')?></td>
                    <td><?=GetMessage('TFC_DEBT')?></td>
					<td title="<?=GetMessage('TFC_AMOUNT_TITLE')?>"><?=GetMessage('TFC_AMOUNT')?></td>
                    <td><?=GetMessage('TFC_RAISE_MULTIPLIER')?></td>
                    <td><?=GetMessage('TFC_RAISE_SUM')?></td>
                    <td><?=GetMessage('TFC_SUM_WITHOUT_RAISE')?></td>
                    <td><?=GetMessage('TFC_CSUM_WITHOUT_RAISE')?></td>
                    <td><?=GetMessage('TFC_TARIFF')?></td>
					<td title="<?=GetMessage('TFC_SUMM_TITLE')?>"><?=GetMessage('TFC_SUMM')?></td>
					<td><?=GetMessage('TFC_CORRECTION')?></td>
					<td><?=GetMessage('TFC_COMPENSATION')?></td>
					<td><?=GetMessage('TFC_SUMM_PAYED')?></td>
					<td><?=GetMessage('TFC_SUMM2PAY')?></td>
					<td><?=GetMessage('TFC_DELETE')?></td>
					<? foreach ($arChargesUserFields as $arUserField): ?>
						<td><?=$arUserField['LIST_COLUMN_LABEL']?></td>
					<? endforeach ?>
				</tr>
				<?

				if (isset($arCharges) && is_array($arCharges))
				{
					foreach ($arCharges as $arCharge)
					{

						$varPrefix = "CHARGES[{$arCharge["ID"]}]";
						$arChargeUserFields = $USER_FIELD_MANAGER->GetUserFields($UF_CHARGE_ENTITY, $arCharge["ID"], LANGUAGE_ID);
						?>
						<tr>
							<td><?=$arCharge['ID']?></td>
							<td>
								<input name="<?=$varPrefix . '[SORT]'?>" type="text" value="<?=$arCharge['SORT']?>" size="3"
								       id="sort_<?=$arCharge['ID']?>">&nbsp;
							</td>
							<td>
								<label title="<?=GetMessage("TF_COMPONENT_TITLE")?>"><input name="<?=$varPrefix . '[COMPONENT]'?>" type="checkbox"
								                                                            value="Y"<?=($arCharge["COMPONENT"] == 'Y' ? ' checked="checked"' : '')?> ></label>
							</td>
							<td>
								<label title="<?=GetMessage("TF_DEBT_ONLY_TITLE")?>"><input name="<?=$varPrefix . '[DEBT_ONLY]'?>" type="checkbox"
								                                                            value="Y"<?=($arCharge["DEBT_ONLY"] == 'Y' ? ' checked="checked"' : '')?> ></label>
							</td>
							<td>
								<?
								echo SelectBoxFromArray($varPrefix . '[SERVICE_ID]', $arServicesRef, $arCharge['SERVICE_ID'], "", 'onchange="changeTariff(this, ' . $arCharge["ID"] . ');"');
								?>
							</td>
							<td style="white-space: nowrap;">
								<input name="<?=$varPrefix . '[DEBT_BEG]'?>" type="text" value="<?=$arCharge['DEBT_BEG']?>" size="4"
								       id="debtbeg_<?=$arCharge['ID']?>">
								/
								<input name="<?=$varPrefix . '[DEBT_END]'?>" type="text" value="<?=$arCharge['DEBT_END']?>" size="4"
								       id="debtend_<?=$arCharge['ID']?>">
							</td>

							<td style="white-space: nowrap;">
								<input name="<?=$varPrefix . '[AMOUNT]'?>" type="text" value="<?=$arCharge['AMOUNT']?>" size="4"
								       onchange="amountChanged(<?=$arCharge['ID']?>)" id="amount_<?=$arCharge['ID']?>">
								/
								<input name="<?=$varPrefix . '[HAMOUNT]'?>" type="text" value="<?=$arCharge['HAMOUNT']?>" size="4"
								       id="hamount_<?=$arCharge['ID']?>">
							</td>
							<td>
								<input name="<?=$varPrefix . '[RAISE_MULTIPLIER]'?>" type="text" value="<?=$arCharge['RAISE_MULTIPLIER']?>"
								       onchange="amountChanged(<?=$arCharge['ID']?>)" size="6"
								       id="raise_multiplier_<?=$arCharge['ID']?>"> <?=GetMessage("TF_CURRENCY_SYM")?>&nbsp;
							</td>
							<td>
								<input name="<?=$varPrefix . '[RAISE_SUM]'?>" type="text" value="<?=$arCharge['RAISE_SUM']?>"
								       onchange="amountChanged(<?=$arCharge['ID']?>)" size="6"
								       id="raise_sum_<?=$arCharge['ID']?>"> <?=GetMessage("TF_CURRENCY_SYM")?>&nbsp;
							</td>
							<td>
								<input name="<?=$varPrefix . '[SUM_WITHOUT_RAISE]'?>" type="text" value="<?=$arCharge['SUM_WITHOUT_RAISE']?>"
								       onchange="amountChanged(<?=$arCharge['ID']?>)" size="6"
								       id="sum_without_raise_<?=$arCharge['ID']?>"> <?=GetMessage("TF_CURRENCY_SYM")?>&nbsp;
							</td>
							<td>
								<input name="<?=$varPrefix . '[CSUM_WITHOUT_RAISE]'?>" type="text" value="<?=$arCharge['CSUM_WITHOUT_RAISE']?>"
								       onchange="amountChanged(<?=$arCharge['ID']?>)" size="6"
								       id="csum_without_raise_<?=$arCharge['ID']?>"> <?=GetMessage("TF_CURRENCY_SYM")?>&nbsp;
							</td>
							<td style="text-align: right;"><span id="tariff_<?=$arCharge['ID']?>"><?=$arCharge["SERVICE_TARIFF"]?></span></td>
							<td style="white-space: nowrap;">
								<input name="<?=$varPrefix . '[SUMM]'?>" type="text" value="<?=$arCharge['SUMM']?>" size="6"
								       id="summ_<?=$arCharge['ID']?>">
								/
								<input name="<?=$varPrefix . '[HSUMM]'?>" type="text" value="<?=$arCharge['HSUMM']?>" size="6"
								       id="summ_<?=$arCharge['ID']?>"> <?=GetMessage("TF_CURRENCY_SYM")?>
							</td>
							<td>
								<input name="<?=$varPrefix . '[CORRECTION]'?>" type="text" value="<?=$arCharge['CORRECTION']?>"
								       onchange="amountChanged(<?=$arCharge['ID']?>)" size="6"
								       id="correction_<?=$arCharge['ID']?>"> <?=GetMessage("TF_CURRENCY_SYM")?>&nbsp;
							</td>
							<td>
								<input name="<?=$varPrefix . '[COMPENSATION]'?>" type="text" value="<?=$arCharge['COMPENSATION']?>" size="6"
								       id="compensation_<?=$arCharge['ID']?>"> <?=GetMessage("TF_CURRENCY_SYM")?>&nbsp;
							</td>
							<td>
								<input name="<?=$varPrefix . '[SUMM_PAYED]'?>" type="text" value="<?=$arCharge['SUMM_PAYED']?>" size="6"
								       id="summ_payed_<?=$arCharge['ID']?>"> <?=GetMessage("TF_CURRENCY_SYM")?>&nbsp;
							</td>
							<td>
								<input name="<?=$varPrefix . '[SUMM2PAY]'?>" type="text" value="<?=$arCharge['SUMM2PAY']?>" size="6"
								       id="summ2pay_<?=$arCharge['ID']?>"> <?=GetMessage("TF_CURRENCY_SYM")?>&nbsp;
							</td>
							<td style="text-align: center;">
								<label><input name="<?=$varPrefix . '[DEL]'?>" type="checkbox" value="1"></label>
							</td>
							<? foreach ($arChargeUserFields as $key => $arUserField): ?>
								<td>
									<input name="<?=$varPrefix . '[' . $key . ']'?>" type="text" value="<?=$arUserField['VALUE']?>"
									       size="<?=$arUserField['SETTINGS']['SIZE']?>">
								</td>
							<?endforeach ?>
						</tr>
						<?
					}
				}

				$varPrefix = "CHARGES[n#IDX#]";
				$arChargesNewRow = Array(
					"&nbsp;",
					"<input name=\"{$varPrefix}[SORT]\" type=\"text\" value=\"\" size=\"3\" id=\"sort_n#IDX#\" />&nbsp;",
					"<label><input name=\"{$varPrefix}[COMPONENT]\" type=\"checkbox\" value=\"Y\" title=\"" . GetMessage("TF_COMPONENT_TITLE") . "\"></label>",
					"<label><input name=\"{$varPrefix}[DEBT_ONLY]\" type=\"checkbox\" value=\"Y\" title=\"" . GetMessage("TF_DEBT_ONLY_TITLE") . "\" ></label>",
					SelectBoxFromArray($varPrefix . '[SERVICE_ID]', $arServicesRef, "", "", 'onchange="changeTariff(this, \'n#IDX#\');"'),
					"
		<input name=\"{$varPrefix}[DEBT_BEG]\" type=\"text\" value=\"\" size=\"4\" id=\"debtbeg_n#IDX#\" />
		/
		<input name=\"{$varPrefix}[DEBT_END]\" type=\"text\" value=\"\" size=\"4\" id=\"debtend_n#IDX#\" />
	",
					"
		<input name=\"{$varPrefix}[AMOUNT]\" type=\"text\" value=\"\" size=\"4\" onchange=\"amountChanged('n#IDX#')\" id=\"amount_n#IDX#\" />
		/
		<input name=\"{$varPrefix}[HAMOUNT]\" type=\"text\" value=\"\" size=\"4\" id=\"hamount_n#IDX#\" />
	",
        "
        <input name=\"{$varPrefix}[RAISE_MULTIPLIER]\" type=\"text\" value=\"\" size=\"6\" id=\"raise_multiplier_n#IDX#\" /> " . "&nbsp;",
        "
         <input name=\"{$varPrefix}[RAISE_SUM]\" type=\"text\" value=\"\" size=\"6\" id=\"raise_sum_n#IDX#\" /> " . GetMessage("TF_CURRENCY_SYM") . "&nbsp;",
        "
         <input name=\"{$varPrefix}[SUM_WITHOUT_RAISE]\" type=\"text\" value=\"\" size=\"6\" id=\"sum_without_raise_n#IDX#\" /> " . GetMessage("TF_CURRENCY_SYM") . "&nbsp;",
        "
         <input name=\"{$varPrefix}[CSUM_WITHOUT_RAISE]\" type=\"text\" value=\"\" size=\"6\" id=\"csum_without_raise_n#IDX#\" /> " . GetMessage("TF_CURRENCY_SYM") . "&nbsp;",
					"<div id=\"tariff_n#IDX#\" style=\"text-align: right;\">&nbsp;</div>",
					"
		<input name=\"{$varPrefix}[SUMM]\" type=\"text\" value=\"\" size=\"6\" id=\"summ_n#IDX#\" />
		/
		<input name=\"{$varPrefix}[SUMM]\" type=\"text\" value=\"\" size=\"6\" id=\"summ_n#IDX#\" /> " . GetMessage("TF_CURRENCY_SYM") . "&nbsp;
	",
					"<input name=\"{$varPrefix}[CORRECTION]\" type=\"text\" value=\"\" size=\"6\" id=\"correction_n#IDX#\" onchange=\"amountChanged('n#IDX#')\" /> " . GetMessage("TF_CURRENCY_SYM") . "&nbsp;",
					"<input name=\"{$varPrefix}[COMPENSATION]\" type=\"text\" value=\"\" size=\"6\" id=\"compensation_n#IDX#\" /> " . GetMessage("TF_CURRENCY_SYM") . "&nbsp;",
					"<input name=\"{$varPrefix}[SUMM_PAYED]\" type=\"text\" value=\"\" size=\"6\" id=\"summ_payed_n#IDX#\" /> " . GetMessage("TF_CURRENCY_SYM") . "&nbsp;",
					"<input name=\"{$varPrefix}[SUMM2PAY]\" type=\"text\" value=\"\" size=\"6\" id=\"summ2pay_n#IDX#\" /> " . GetMessage("TF_CURRENCY_SYM") . "&nbsp;",
					"&nbsp;",
				);

				foreach ($arChargesUserFields as $key => $arUserField)
				{
					$arChargesNewRow[] = "<input name=\"{$varPrefix}[{$key}]\" type=\"text\" value=\"\" size=\"{$arUserField['SETTINGS']['SIZE']}\">";
				}

				for ($i = 0; $i < 2; $i++)
				{
					echo "\t<tr>\n";
					foreach ($arChargesNewRow as $htmlRow)
					{
						echo "\t\t<td>" . str_replace('#IDX#', $i, $htmlRow) . "</td>\n";
					}
					echo "\t</tr>\n";
				}

				?>
				<tr>
					<td colspan="<?=count($arChargesUserFields) + 18?>" style="text-align: center;">
						<button onclick="appendRow(); return false;"><?=GetMessage("T_ADD_ROW")?></button>
					</td>
				</tr>
			</table>
			<script type="text/javascript">


                var arTariffs = <?=CUtil::PhpToJSObject($arTariffs)?>;

                function changeTariff(select, code) {
                    var inputTariff = document.getElementById('tariff_' + code);
                    if (inputTariff) {
                        inputTariff.innerHTML = parseInt(select.value) > 0 ? arTariffs[select.value] : '';
                        amountChanged(code);
                    }
                }

                function amountChanged(code) {

                    var inputAmount = document.getElementById('amount_' + code);
                    var inputSumm = document.getElementById('summ_' + code);
                    var inputSummPayed = document.getElementById('summ_payed_' + code);
                    var inputSumm2Pay = document.getElementById('summ2pay_' + code);
                    var inputTariff = document.getElementById('tariff_' + code);
                    var inputCorrection = document.getElementById('correction_' + code);

                    if (inputAmount && inputSumm && inputSummPayed && inputSumm2Pay && inputTariff) {

                        var iAmount = parseFloat(inputAmount.value);
                        var iSumm = parseFloat(inputSumm.value);
                        var iSummPayed = parseFloat(inputSummPayed.value);
                        var iSumm2Pay = parseFloat(inputSumm2Pay.value);
                        var iTariff = parseFloat(inputTariff.innerHTML);
                        var iCorrection = inputCorrection ? parseFloat(inputCorrection.value) : 0;

                        if (isNaN(iSummPayed)) {
                            inputSummPayed.value = 0;
                            iSummPayed = 0;
                        }

                        if (isNaN(iAmount) || isNaN(iTariff)) {
                            return;
                        }

                        inputSumm.value = (iAmount * iTariff).toFixed(2);
                        inputSumm2Pay.value = iAmount * iTariff - iSummPayed > 0 ? (iAmount * iTariff - iSummPayed + iCorrection).toFixed(2) : 0;
                    }

                }

                var chargesNewRow = <?=CUtil::PhpToJSObject($arChargesNewRow)?>;
                var chargesNewRowStart = <?=$i?>;

                function appendRow() {
                    var idx = chargesNewRowStart++;
                    var tbl = document.getElementById('chargesTable'); // table reference
                    var row = tbl.insertRow(tbl.rows.length - 1);
                    for (var i = 0; i < tbl.rows[0].cells.length; i++) {
                        var html = chargesNewRow[i];
                        row.insertCell(i).innerHTML = html.replace('#IDX#', idx);
                    }
                }

			</script>
			<br>
		</td>
	</tr>
<?
$tabControl->EndCustomField("CHARGES", '');

$tabControl->Buttons(Array(
	"disabled" => ($modulePermissions < "W"),
	"back_url" => "/bitrix/admin/tszh_account_period_list.php?lang=" . LANG . GetFilterParams("filter_"),
));

$tabControl->Show();

if (method_exists($USER_FIELD_MANAGER, 'showscript'))
{
	$tabControl->BeginPrologContent();
	echo $USER_FIELD_MANAGER->ShowScript();
	$tabControl->EndPrologContent();
}

$tabControl->ShowWarnings($tabControl->GetName(), $message);

if (!defined('BX_PUBLIC_MODE') || BX_PUBLIC_MODE != 1):?>
	<? echo BeginNote(); ?>
	<span class="required">*</span> <? echo GetMessage("REQUIRED_FIELDS") ?>
	<? echo EndNote(); ?>
<? endif; ?>
<? require_once($DOCUMENT_ROOT . BX_ROOT . "/modules/main/include/epilog_admin.php"); ?>