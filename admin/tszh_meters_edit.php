<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

ob_start();
if (!CTszhFunctionalityController::CheckEdition())
{
	$c = ob_get_contents();
	ob_end_clean();
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
	echo $c;
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");

	return;
}
$demoNotice = ob_get_contents();
ob_end_clean();

$modulePermissions = $APPLICATION->GetGroupRight("citrus.tszh");
if ($modulePermissions <= "R")
{
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

require_once($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/citrus.tszh/include.php");
IncludeModuleLangFile(__FILE__);

if (!function_exists("appendAccountRow")):
	function appendAccountRow($idx, $accountID = "")
	{
		?>
		<tr>
			<td>
				<?=tszhLookup("ACCOUNT_ID[{$idx}][ID]", $accountID, Array("type" => 'account', "formName" => 'meterEdit_form'));?>
			</td>
			<td style="text-align: center;">
				<label><input name="ACCOUNT_ID[<?=$idx?>][DEL]" type="checkbox" value="1"/></label>
			</td>
		</tr>
	<?
	}
endif;

if ($_REQUEST["appendAccountRow"] == "Y" && isset($_REQUEST["idx"]))
{
	require_once($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/include/prolog_admin_js.php");
	appendAccountRow($_REQUEST["idx"]);
	require($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/include/epilog_admin_js.php");
}

$strRedirect = BX_ROOT . "/admin/tszh_meters_edit.php?lang=" . LANG;
$strRedirectList = BX_ROOT . "/admin/tszh_meters.php?lang=" . LANG;

ClearVars();
$errorMessage = "";
$bVarsFromForm = false;
$message = null;

$ID = IntVal($ID);

$aTabs = array(
	array("DIV" => "edit1", "TAB" => GetMessage("TME_TAB1"), "ICON" => "citrus.tszh", "TITLE" => GetMessage("TME_TAB1_TITLE")),
);

$UF_ENTITY = "TSZH_METER";

$tabControl = new CAdminForm("meterEdit", $aTabs);

if ($REQUEST_METHOD == "POST" && strlen($Update) > 0 && $modulePermissions >= "W" && check_bitrix_sessid())
{
	if ($modulePermissions < "W")
	{
		$errorMessage .= GetMessage("TME_ERROR_NO_PERMISSIONS_TO_EDIT") . "<br>";
	}

	if ($ID > 0)
	{
		if (!($arOldMeter = CTszhMeter::GetByID($ID)))
		{
			$errorMessage .= str_replace("#ID#", $ID, GetMessage("TME_ERROR_NOT_FOUND")) . "<br />";
		}
	}

	if (strlen($errorMessage) <= 0)
	{
		$arMetersAccounts = array();
		foreach ($_POST["ACCOUNT_ID"] as $key => $arAccountID)
		{
			if (!$arAccountID["DEL"] && intval($arAccountID["ID"]) > 0)
			{
				$arMetersAccounts[] = intval($arAccountID["ID"]);
			}
		}
		$arMetersAccounts = array_unique($arMetersAccounts);

		$arFields = Array(
			"NAME" => htmlspecialcharsbx($NAME),
			"NUM" => strlen($NUM) ? htmlspecialcharsbx($NUM) : false,
			"SORT" => htmlspecialcharsbx($SORT),
			"ACTIVE" => $ACTIVE == "Y" ? "Y" : "N",
			"XML_ID" => htmlspecialcharsbx($XML_ID),
			"SERVICE_ID" => false,
			"SERVICE_NAME" => htmlspecialcharsbx($SERVICE_NAME),
			"VALUES_COUNT" => intval($VALUES_COUNT),
			"DEC_PLACES" => intval($DEC_PLACES),
			"CAPACITY" => intval($CAPACITY),
			"VERIFICATION_DATE" => strlen($VERIFICATION_DATE) ? htmlspecialcharsbx($VERIFICATION_DATE) : false,
			"HOUSE_METER" => $HOUSE_METER == "Y" ? "Y" : "N",
		);
		$USER_FIELD_MANAGER->EditFormAddFields($UF_ENTITY, $arFields);
		if (IntVal($SERVICE_ID) > 0)
		{
			$arFields['SERVICE_ID'] = intval($SERVICE_ID);
		}
		if ($ID > 0)
		{
			$bResult = CTszhMeter::Update($ID, $arFields);
			if ($bResult)
			{
				CTszhMeter::unbindAccount($ID);
				if ($bResult && !empty($arMetersAccounts))
				{
					CTszhMeter::bindAccount($ID, $arMetersAccounts);
				}
			}
		}
		else
		{
			$ID = CTszhMeter::Add($arFields);
			$bResult = $ID > 0;
			if ($bResult && !empty($arMetersAccounts))
			{
				CTszhMeter::bindAccount($ID, $arMetersAccounts);
			}
		}

		if ($bResult)
		{
			$dbMeterValue = CTszhMeterValue::GetList(Array('TIMESTAMP_X' => 'DESC'), Array(
				'METER_ID' => $ID,
			));
			$arMeterValue = $dbMeterValue->GetNext();

			$bChanged = false;
			for ($i = 1; $i <= $arFields['VALUES_COUNT']; $i++)
			{
				if ($arMeterValue["VALUE$i"] != ${"VALUE$i"}
				    || $arMeterValue["AMOUNT$i"] != ${"AMOUNT$i"})
				{
					$bChanged = true;
					break;
				}
			}
			if ($bChanged)
			{
				$arNewMeterValue = Array("METER_ID" => $ID);
				for ($i = 1; $i <= $arFields['VALUES_COUNT']; $i++)
				{
					$arNewMeterValue["VALUE$i"] = ${"VALUE$i"};
					$arNewMeterValue["AMOUNT$i"] = ${"AMOUNT$i"};
				}
				CTszhMeterValue::Add($arNewMeterValue);
			}
		}

		if (!$bResult)
		{
			if ($ex = $APPLICATION->GetException())
			{
				$errorMessage .= GetMessage("TME_ERROR_SAVE") . ":<br />" . $ex->GetString() . ".<br />";
			}
			else
			{
				$errorMessage .= GetMessage("TME_ERROR_SAVE") . ".<br />";
			}
		}
	}

	if (strlen($errorMessage) <= 0)
	{
		if (strlen($apply) <= 0)
		{
			LocalRedirect($strRedirectList . GetFilterParams("filter_", false));
		}
		else
		{
			LocalRedirect($strRedirect . "&ID=" . $ID . "&" . $tabControl->ActiveTabParam());
		}
	}
	else
	{
		$bVarsFromForm = true;
	}
}

//require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/citrus.tszh/prolog.php");

if ($ID > 0)
{
	$APPLICATION->SetTitle(str_replace("#ID#", $ID, GetMessage("TME_PAGE_TITLE_EDIT")));
}
else
{
	$APPLICATION->SetTitle(GetMessage("TME_PAGE_TITLE_ADD"));
}

require_once($DOCUMENT_ROOT . BX_ROOT . "/modules/main/include/prolog_admin_after.php");

echo $demoNotice;

if ($ID > 0 /*&& !$bVarsFromForm*/):
	$dbMeter = CTszhMeter::GetList(
		Array(),
		Array("ID" => $ID),
		false,
		Array("nTopCount" => 1),
		Array("*")
	);
	if (!$arMeter = $dbMeter->ExtractFields("str_"))
	{
		if ($modulePermissions < "W")
		{
			$errorMessage .= GetMessage("TME_ERROR_NO_PERMISSIONS_TO_EDIT") . "<br />";
		}
		$ID = 0;
	}

	if (is_array($arMeter))
	{
		$dbMeterValue = CTszhMeterValue::GetList(Array('TIMESTAMP_X' => 'DESC'), Array(
			'METER_ID' => $arMeter['ID'],
		));
		$arMeterValue = $dbMeterValue->GetNext();
	}
	else
	{
		$arMeterValue = Array();
	}
else:
	$DB->InitTableVarsForEdit("b_tszh_meters", "", "str_");
endif;


if (strlen($errorMessage) > 0)
{
	echo CAdminMessage::ShowMessage(Array(
		"DETAILS" => $errorMessage,
		"TYPE" => "ERROR",
		"MESSAGE" => GetMessage("TME_ERRORS_HAPPENED"),
		"HTML" => true,
	));
}


/**
 *   CAdminForm()
 **/
$aMenu = array(
	array(
		"TEXT" => GetMessage("TME_M_METERS_LIST"),
		"LINK" => "/bitrix/admin/tszh_meters.php?lang=" . LANG . GetFilterParams("filter_"),
		"ICON" => "btn_list",
		"TITLE" => GetMessage("TME_M_METERS_LIST_TITLE"),
	),
);

if ($ID > 0 && $modulePermissions >= "W")
{
	$aMenu[] = array("SEPARATOR" => "Y");

	$aMenu[] = array(
		"TEXT" => GetMessage("TME_M_ADD_METER"),
		"LINK" => "/bitrix/admin/tszh_meters_edit.php?lang=" . LANG . GetFilterParams("filter_"),
		"ICON" => "btn_new",
		"TITLE" => GetMessage("TME_M_ADD_METER_TITLE"),
	);

	if ($modulePermissions >= "W")
	{
		$aMenu[] = array(
			"TEXT" => GetMessage("TME_M_DELETE"),
			"LINK" => "javascript:if(confirm('" . GetMessage("TME_M_DELETE_CONFIRM") . "')) window.location='/bitrix/admin/tszh_meters_edit.php?ID=" . $ID . "&action=delete&lang=" . LANG . "&" . bitrix_sessid_get() . "#tb';",
			"WARNING" => "Y",
			"ICON" => "btn_delete",
		);
	}

}
if (!empty($aMenu))
{
	$aMenu[] = array("SEPARATOR" => "Y");
}
$link = DeleteParam(array("mode"));
$link = $GLOBALS["APPLICATION"]->GetCurPage() . "?mode=settings" . ($link <> "" ? "&" . $link : "");
$aMenu[] = array(
	"TEXT" => GetMessage("TME_M_SETTINGS"),
	"TITLE" => GetMessage("TME_M_SETTINGS_TITLE"),
	"LINK" => "javascript:" . $tabControl->GetName() . ".ShowSettings('" . htmlspecialcharsbx(CUtil::addslashes($link)) . "')",
	"ICON" => "btn_settings",
);

$context = new CAdminContextMenu($aMenu);
$context->Show();


// ============== JS init =========================
CJsCore::Init(Array("jquery"));

// ============ FORM PROLOG ==================
$tabControl->BeginEpilogContent();
echo GetFilterHiddens("filter_");
?>
	<input type="hidden" name="Update" value="Y"/>
	<input type="hidden" name="lang" value="<? echo LANG ?>"/>
	<input type="hidden" name="ID" value="<? echo $ID ?>"/>
<?
echo bitrix_sessid_post();
$tabControl->EndEpilogContent();
// ===========================================

$tabControl->Begin(array(
	"FORM_ACTION" => $APPLICATION->GetCurPage(),
	"FORM_ATTRIBUTES" => "method=\"POST\" enctype=\"multipart/form-data\"",
));
$tabControl->BeginNextFormTab();

if ($str_ID > 0)
{
	$tabControl->AddViewField("ID", "ID", $str_ID);
}

$tabControl->BeginCustomField("SERVICE_ID", GetMessage("TME_F_SERVICE_ID"), false);
?>
	<tr>
		<td width="40%"><?=$tabControl->GetCustomLabelHTML()?></td>
		<td width="60%"><?=tszhLookup('SERVICE_ID', $str_SERVICE_ID, Array("type" => 'service', "formName" => 'meterEdit_form'));?></td>
	</tr>
<?
$tabControl->EndCustomField("SERVICE_ID", '<input type="hidden" name="SERVICE_ID" value="' . $str_SERVICE_ID . '">');

$tabControl->AddEditField("SERVICE_NAME", GetMessage("TME_F_SERVICE_NAME"), false, array("size" => 30, "maxlength" => 50), $str_SERVICE_NAME);

$tabControl->AddCheckBoxField("ACTIVE", GetMessage("TME_F_ACTIVE"), false, "Y", ($str_ACTIVE == "Y"));
$tabControl->AddEditField("NAME", GetMessage("TME_F_NAME"), true, array("size" => 30, "maxlength" => 50), $str_NAME);
$tabControl->AddEditField("NUM", GetMessage("TME_F_NUM"), false, array("size" => 30, "maxlength" => 100), $str_NUM);
$tabControl->AddEditField("SORT", GetMessage("TME_F_SORT"), true, array("size" => 3, "maxlength" => 10), $str_SORT);
$tabControl->AddEditField("XML_ID", GetMessage("TME_F_XML_ID"), false, array("size" => 30, "maxlength" => 50), $str_XML_ID);
$tabControl->AddCheckBoxField("HOUSE_METER", GetMessage("TME_F_HOUSE_METER"), false, "Y", ($str_HOUSE_METER == "Y"));
$tabControl->AddEditField("DEC_PLACES", GetMessage("TME_F_DEC_PLACES"), true, array("size" => 2, "maxlength" => 1), $str_DEC_PLACES);
$tabControl->AddEditField("CAPACITY", GetMessage("TME_F_CAPACITY"), false, array("size" => 2, "maxlength" => 2), $str_CAPACITY);

$tabControl->BeginCustomField("VALUES_COUNT", GetMessage("TME_F_VALUES_COUNT"), true);
$arSelect = array(1 => 1, 2 => 2, 3 => 3);
$html = '<select name="VALUES_COUNT" onchange="tszhChangeValuesCount()" id="VALUES_COUNT">';
foreach ($arSelect as $key => $val)
{
	$html .= '<option value="' . htmlspecialcharsbx($key) . '"' . ($str_VALUES_COUNT == $key ? ' selected' : '') . '>' . htmlspecialcharsex($val) . '</option>';
}
$html .= '</select>';
?>
	<tr>
		<td width="40%"><?=$tabControl->GetCustomLabelHTML()?></td>
		<td width="60%"><?=$html?>
			<script type="text/javascript">
                function tszhChangeValuesCount() {
                    var formValuesCount = document.getElementById('VALUES_COUNT');
                    if (!formValuesCount)
                        return;
                    valuesCount = formValuesCount.value;
                    if (valuesCount < 1 || valuesCount > 3)
                        return;
                    for (var i = 1; i <= 3; i++) {
                        var tr2Show = document.getElementById('tr_VALUE' + i);
                        if (tr2Show)
                            tr2Show.style.display = (i <= valuesCount ? '' : 'none');
                    }
                }

                window.setTimeout('tszhChangeValuesCount()', 100);
			</script>
		</td>
	</tr>
<?
$tabControl->EndCustomField("VALUES_COUNT", '<input type="hidden" name="VALUES_COUNT" value="' . $str_VALUES_COUNT . '">');

$tabControl->AddCalendarField("VERIFICATION_DATE", GetMessage("TME_F_VERIFICATION_DATE"), $str_VERIFICATION_DATE, false);

$tabControl->AddSection("ACCOUNTS_SECTION", GetMessage("TME_F_ACCOUNTS"));
$tabControl->BeginCustomField("ACCOUNTS", GetMessage("TME_F_ACCOUNTS"));
?>
	<tr id="tr_ACCOUNTS">
		<td id="td_ACCOUNTS" colspan="2">
			<style type="text/css">
				#accountsTable {
					margin: 0 auto !important;
				}
			</style>
			<table cellspacing="0" cellpadding="0" border="0" align="center" class="internal" id="accountsTable">
				<tr class="heading">
					<td><?=GetMessage("TME_A_ACCOUNT")?></td>
					<td><?=GetMessage("TME_A_DELETE")?></td>
				</tr>
				<?
				if (!empty($arMeter["ACCOUNT_ID"]))
				{
					foreach ($arMeter["ACCOUNT_ID"] as $key => $accountID)
					{
						appendAccountRow($key, $accountID);
					}
				}

				for ($i = 0; $i < 2; $i++)
				{
					appendAccountRow("n{$i}");
				}
				?>
				<tr>
					<td colspan="2" style="text-align: center;">
						<button onclick="appendRow(); return false;"><?=GetMessage("TME_ADD_ROW")?></button>
					</td>
				</tr>
			</table>
			<script type="text/javascript">
                var accountsNewRowStart = <?=$i?>;

                function appendRow() {
                    var idx = accountsNewRowStart++;
                    var td = BX('td_ACCOUNTS');
                    BX.showWait(td);
                    BX.ajax.post('<?=POST_FORM_ACTION_URI?>', {'appendAccountRow': 'Y', 'idx': 'n' + idx}, function (data) {
                        data = data.replace(/^\s*<tr>/, '');
                        data = data.replace(/<\/tr>\s*$/, '');
                        var tbl = BX('accountsTable');
                        var row = tbl.insertRow(tbl.rows.length - 1);
                        row.innerHTML = data;
                        BX.closeWait(td);
                    });
                }
			</script>
			<br>
		</td>
	</tr>
<?
$tabControl->EndCustomField("ACCOUNTS", '');

if ($str_VALUES_COUNT > 0)
{
	$tabControl->AddSection("CURRENT_METER_VALUES", GetMessage("TME_F_CURRENT_METER_VALUES_AND_AMOUNTS"));
	$tabControl->AddViewField("VALUE_DATE", GetMessage("TME_F_VALUE_DATE"), $arMeterValue["TIMESTAMP_X"] . '<br>[<a href="/bitrix/admin/tszh_meters_history.php?lang=' . LANG . '&amp;find_meter_id=' . $str_ID . '">' . GetMessage("TME_METER_HISTORY") . '</a>]');
	/*for ($i = 1; $i <= 3; $i++) {
		$tabControl->AddEditField("VALUE$i", GetMessage("TME_VALUE_N", array("#N#" => $i)), true, array("size"=>5, "maxlength"=>11), $arMeterValue["VALUE$i"]);
	}*/

	$tabControl->BeginCustomField("CURRENT_METER_VALUES_AND_AMOUNTS", GetMessage("TME_F_CURRENT_METER_VALUES_AND_AMOUNTS"));
	for ($i = 1; $i <= 3; $i++):?>
		<tr id="tr_VALUE<?=$i?>">
			<td width="40%"><span class="adm-required-field"><?=GetMessage("TME_VALUE_N", array("#N#" => $i))?></span></td>
			<td width="60%">
				<input type="text" name="VALUE<?=$i?>" value="<?=$arMeterValue["VALUE$i"]?>" maxlength="11" size="5"/>

				&nbsp;&nbsp;
				<?=GetMessage("TME_AMOUNT")?> <input type="text" name="AMOUNT<?=$i?>" value="<?=$arMeterValue["AMOUNT$i"]?>" maxlength="11" size="5"/>
			</td>
		</tr>
	<?endfor;
	$hiddenFieldsStr = "";
	for ($i = 1; $i <= 3; $i++)
	{
		$hiddenFieldsStr .= "<input type=\"hidden\" name=\"VALUE{$i}\" value=\"{$arMeterValue["VALUE$i"]}\"><input type=\"hidden\" name=\"AMOUNT{$i}\" value=\"{$arMeterValue["AMOUNT$i"]}\">";
	}
	$tabControl->EndCustomField("CURRENT_METER_VALUES_AND_AMOUNTS", $hiddenFieldsStr);
}

// ������� ������ � ����������������� ������, ���� �� ��� ���� ���� ��� � ������������ ���� ����� �� ���������� �����
if ((count($USER_FIELD_MANAGER->GetUserFields($UF_ENTITY)) > 0)
    || ($USER_FIELD_MANAGER->GetRights($UF_ENTITY) >= "W"))
{
	$tabControl->AddSection('USER_FIELDS', GetMessage("TME_USER_FIELDS"));
	$tabControl->ShowUserFields($UF_ENTITY, $str_ID, $bVarsFromForm);
}

$tabControl->Buttons(Array(
	"disabled" => ($modulePermissions < "W"),
	"back_url" => "/bitrix/admin/tszh_meters.php?lang=" . LANG . GetFilterParams("filter_"),
));

$tabControl->Show();
$tabControl->ShowWarnings($tabControl->GetName(), $message);

if (!defined('BX_PUBLIC_MODE') || BX_PUBLIC_MODE != 1):?>
	<? echo BeginNote(); ?>
	<span class="required">*</span> <? echo GetMessage("REQUIRED_FIELDS") ?>
	<? echo EndNote(); ?>
<? endif;/*?>

<script type="text/javascript">
$(function() {
	function __checkHouseMeter($this)
	{
		if ($this.is(':checked'))
			$('#tr_ACCOUNT_ID').hide();
		else
			$('#tr_ACCOUNT_ID').show();
	}
	
	$('#tr_HOUSE_METER input[type=checkbox]').click(function() {
		__checkHouseMeter($(this));
	});
	__checkHouseMeter($('#tr_HOUSE_METER input[type=checkbox]'));
	
});
</script>*/ ?>

<? require_once($DOCUMENT_ROOT . BX_ROOT . "/modules/main/include/epilog_admin.php"); ?>