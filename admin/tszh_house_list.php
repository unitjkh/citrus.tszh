<?
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszh/include.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszh/prolog.php");

use Citrus\Tszh\HouseTable;
use Vdgb\Tszhepasport\EpasportTable;

IncludeModuleLangFile(__FILE__);

ob_start();
if (!CTszhFunctionalityController::CheckEdition())
{
	$c = ob_get_contents();
	ob_end_clean();
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
	echo $c;
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");

	return;
}
$demoNotice = ob_get_contents();
ob_end_clean();


// ������� ����� ������� �������� ������������ �� ������
$POST_RIGHT = $APPLICATION->GetGroupRight("citrus.tszh");
// ���� ��� ���� - �������� � ����� ����������� � ���������� �� ������
if ($POST_RIGHT <= "D")
{
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

$UF_ENTITY = "TSZH_HOUSE";

$sTableID = "tbl_tszh_house";
$oSort = new CAdminSorting($sTableID, "ID", "desc");
$lAdmin = new CAdminList($sTableID, $oSort);

// �������� ���� ��� �������
$arTszhFilter = $arTszhRight = $arListFilter = array();
if (CTszhFunctionalityController::isPortal() && !$USER->IsAdmin())
{
	global $USER;
	$arGroups = $USER->GetUserGroupArray();

	$rsPerms = \Vdgb\PortalTszh\PermsTable::getList(
		array(
			"filter" => array("@GROUP_ID" => $arGroups)
		)
	);

	while ($arPerms = $rsPerms->fetch())
	{
		if ($arPerms["PERMS"] >= "R")
		{
			$arTszhRight[$arPerms["TSZH_ID"]] = $arPerms["PERMS"];
		}
	}

	$arTszhFilter["@ID"] = $arListFilter["@TSZH_ID"] = array_keys($arTszhRight);
}

// ������ ������
$rsSites = CSite::GetList($sort = 'sort', $ord = 'desc', Array());
$arSites = Array('' => '');
while ($arSite = $rsSites->Fetch())
{
	$arSites[$arSite['ID']] = '[' . $arSite['ID'] . '] ' . $arSite['NAME'];
}

// ������ �������� ����������
$arTszhList = Array();
$rsTszh = CTszh::GetList(Array("NAME" => "ASC"), $arTszhFilter, false, false, Array("ID", "NAME"));
while ($arTszh = $rsTszh->Fetch())
{
	$arTszhList[$arTszh['ID']] = $arTszh['NAME'];
}

$arHeaders = array();
$entityFields = HouseTable::getMap();
if (CModule::IncludeModule('vdgb.tszhepasport'))
{
	$entityFields = array_merge($entityFields, EpasportTable::getMap()); /*���������� ������� � ������ ������*/
	array_pop($entityFields); /*������� �� ������ ����� �������� ����, ����������� reference-����� (� ����� ������ ��������� � �������)*/
}
$typesMap = array(
	'integer' => 'int',
	'float' => 'int',
	"string" => 'string',
);
$shownByDefault = array(
	"ID",
	"TSZH_ID",
	"FULL_ADDRESS",
	"AREA",
);
foreach ($entityFields as $fieldName => $fieldSettings)
{
	$isNumeric = in_array($fieldSettings['data_type'], array('integer', 'float'));

	$fieldHeading = array(
		"id" => $fieldName,
		"content" => $fieldSettings['title'],
		"align" => $isNumeric ? "right" : "left",
		"default" => in_array($fieldName, $shownByDefault),
		"type" => $typesMap[$fieldSettings['data_type']],
	);
	if (!isset($fieldSettings['expression']))
	{
		$fieldHeading['sort'] = $fieldName;
	}
	if ($fieldName !== "ID" && !isset($fieldSettings['expression']))
	{
		$fieldHeading['edit'] = true;
	}

	if ($fieldName == 'TSZH_ID')
	{
		$fieldHeading['type'] = 'list';
		$fieldHeading['items'] = $arTszhList;
		$fieldHeading['align'] = 'left';
	}

	if ($fieldName == 'SITE_ID')
	{
		$fieldHeading['type']  = 'list';
		$fieldHeading['items'] = $arSites;
		$fieldHeading['align'] = 'left';
	}

	$arHeaders[] = $fieldHeading;
}

// ******************************************************************** //
//                           ������                                     //
// ******************************************************************** //

// *********************** CheckFilter ******************************** //
// �������� �������� ������� ��� �������� ������� � ��������� �������
function CheckFilter()
{
	global $FilterArr, $lAdmin;
	foreach ($FilterArr as $value)
	{
		global $$value;
	}

	return true;
}

// *********************** /CheckFilter ******************************* //

// ������ �������� �������
$FilterArr = TszhAdminFilterParams($arHeaders);

// �������������� ������
$lAdmin->InitFilter($FilterArr);

$USER_FIELD_MANAGER->AdminListAddFilterFields($UF_ENTITY, $FilterArr);

// ���� ��� �������� ������� ���������, ���������� ���
$arFilter = array();
if (CheckFilter() && !is_set($_REQUEST, 'del_filter'))
{
	// �������� ������ ���������� ��� ������� �� ������ �������� �������
	TszhAdminMakeFilter($arFilter, $arHeaders);
	$USER_FIELD_MANAGER->AdminListAddFilter($UF_ENTITY, $arFilter);
}

// ******************************************************************** //
//                ��������� �������� ��������                           //
// ******************************************************************** //

if (isset($_REQUEST["edit_form_del"]) && IntVal($_REQUEST["edit_form_del"]) == 1 && check_bitrix_sessid())
{
	if (isset($_REQUEST["ID"]) && IntVal($_REQUEST["ID"]) > 0)
	{
		@set_time_limit(0);

		$arItem = HouseTable::getById($_REQUEST["ID"])->fetch();
		if (is_array($arItem))
		{
			if (HouseTable::Delete($arItem['ID']))
			{
				$messageOK = GetMessage("TSZH_DELETE_OK");
			}
			else
			{
				$lAdmin->AddGroupError(GetMessage("TSZH_DELETE_ERROR"), $ID);
			}
		}
		else
		{
			$lAdmin->AddGroupError(GetMessage("TSZH_DELETE_ERROR") . ". " . GetMessage("TSZH_DELETE_ITEM_NOT_FOUND", Array("#ID#" => $ID)), $ID);
		}
	}
}


// ******************************************************************** //
//                ��������� �������� ��� ���������� ������              //
// ******************************************************************** //

// ���������� ����������������� ���������
if ($lAdmin->EditAction() && $POST_RIGHT >= "W" && check_bitrix_sessid())
{

	// ������� �� ������ ���������� ���������
	foreach ($FIELDS as $ID => $arFields)
	{
		if (!$lAdmin->IsUpdated($ID))
		{
			continue;
		}

		// �������� ��������� ������� ��������
		$ID = IntVal($ID);

		$houseEntityFields = HouseTable::getFieldNames();
		$arHouseFields = array(); // �������� �� ������ ������ ����� ������ ��, ������� ��������� � �������� ��������� � �����
		array_map(function ($fieldName) use (&$arHouseFields, $arFields)
		{
			if (isset($arFields[$fieldName]))
			{
				$arHouseFields[$fieldName] = $arFields[$fieldName];
			}
		}, $houseEntityFields);

		if (CModule::IncludeModule('vdgb.tszhepasport'))
		{
			$epasportEntityFields = EpasportTable::getFieldNames();
			$arEpasportFields = array(); // �������� �� ������ ������ ����� ������ ��, ������� ��������� � ������� � ������������ ����������
			array_map(function ($fieldName) use (&$arEpasportFields, $arFields)
			{
				if (isset($arFields[$fieldName]))
				{
					$arEpasportFields[$fieldName] = $arFields[$fieldName];
				}
			}, $epasportEntityFields);
		}

		$arItem = HouseTable::getById($ID)->fetch();
		if (is_array($arItem))
		{
			$USER_FIELD_MANAGER->AdminListPrepareFields($UF_ENTITY, $arHouseFields);
			if (!HouseTable::Update($ID, $arHouseFields))
			{
				$strError = '';
				if ($ex = $APPLICATION->GetException())
				{
					$strError = $ex->GetMessage();
				}
				$lAdmin->AddGroupError(GetMessage("TSZH_GROUP_SAVE_ERROR") . " " . $strError, $ID);
			}
			if (CModule::IncludeModule('vdgb.tszhepasport'))
			{
				$arEpasportFields['DATE_CREATE'] = \Bitrix\Main\Type\DateTime::createFromUserTime($arEpasportFields['DATE_CREATE']);
				$arEpasportFields["DATE_UPDATE"] = \Bitrix\Main\Type\DateTime::createFromTimestamp(time());
				$arEPasport = EpasportTable::getList(array("filter" => array("=EP_HOUSE_ID" => $ID), "select" => Array("EP_ID")))->fetch();
				if(!$arEPasport)
				{
					$arEpasportFields["EP_HOUSE_ID"] = $ID;
					$ep_result = EpasportTable::add($arEpasportFields);
				}
				else
				{
					$ep_result = EpasportTable::update($arEPasport['EP_ID'], $arEpasportFields);
				}
			}
		}
		else
		{
			$lAdmin->AddGroupError(GetMessage("TSZH_GROUP_SAVE_ERROR") . " " . GetMessage("TSZH_GROUP_SAVE_ERROR_ITEM_NOT_FOUND"), $ID);
		}
	}
}

// ��������� ��������� � ��������� ��������
if (($arID = $lAdmin->GroupAction()) && $POST_RIGHT >= "W" && check_bitrix_sessid())
{
	// ���� ������� "��� ���� ���������"
	if ($_REQUEST['action_target'] == 'selected')
	{
		$rsItems = HouseTable::getList(array("filter" => $arFilter, "select" => Array("ID")));
		$arID = Array();
		while ($arItem = $rsItems->Fetch())
		{
			$arID[] = $arItem['ID'];
		}
	}

	@set_time_limit(0);

	// ������� �� ������ ���������
	foreach ($arID as $ID)
	{
		if (strlen($ID) <= 0)
		{
			continue;
		}

		$ID = IntVal($ID);

		// ��� ������� �������� �������� ��������� ��������
		switch ($_REQUEST['action'])
		{
			// ��������
			case "delete":
				$arItem = HouseTable::getByID($ID)->fetch();
				if (is_array($arItem))
				{
					if (HouseTable::delete(IntVal($ID)))
					{
						$messageOK = GetMessage("qroup_del_ok");
					}
					else
					{
						$lAdmin->AddGroupError(GetMessage("TSZH_DELETE_ERROR"), $ID);
					}
				}
				else
				{
					$lAdmin->AddGroupError(GetMessage("TSZH_DELETE_ERROR") . " " . GetMessage("TSZH_DELETE_ITEM_NOT_FOUND"), $ID);
				}
				if (CModule::IncludeModule('vdgb.tszhepasport'))
				{
					$arItem = EpasportTable::getList(array("filter" => array("=EP_HOUSE_ID" => $ID), "select" => Array("EP_ID")))->Fetch();
					if (is_array($arItem))
					{
						EpasportTable::delete(IntVal($arItem['EP_ID']));
					}
				}
				break;
		}
	}
}

// ******************************************************************** //
//                ������� ��������� ������                              //
// ******************************************************************** //

// ������� ������ �������
if (CModule::IncludeModule('vdgb.tszhepasport'))
{
	$arSelect = Array("*", "FULL_ADDRESS", '' => 'Vdgb\Tszhepasport\EpasportTable:HOUSE.*');
}
else
{
	$arSelect = Array("*", "FULL_ADDRESS",);
}
$rsItems = HouseTable::getList(
	array(
		"order" => array(ToUpper($by) => $order),
		"filter" => array_merge($arFilter, $arListFilter),
		"select" => $arSelect
	)
);

// ����������� ������ � ��������� ������ CAdminResult
$rsData = new CAdminResult($rsItems, $sTableID);

// ���������� CDBResult �������������� ������������ ���������.
$rsData->NavStart();

// �������� ����� ������������� ������� � �������� ������ $lAdmin
$lAdmin->NavText($rsData->GetNavPrint(GetMessage("TSZH_HOUSES")));

// ******************************************************************** //
//                ���������� ������ � ������                            //
// ******************************************************************** //

$USER_FIELD_MANAGER->AdminListAddHeaders($UF_ENTITY, $arHeaders);
$lAdmin->AddHeaders($arHeaders);

while ($rowData = $rsData->NavNext(true, "f_"))
{
	// ������� ������. ��������� - ��������� ������ CAdminListRow
	$row =& $lAdmin->AddRow($rowData["ID"], $rowData);

	// ����� �������� ����������� �������� ��� ��������� � �������������� ������
	foreach ($arHeaders as $field)
	{
		$fieldName = $field['id'];

		if ($fieldName == 'ID')
		{
			$row->AddViewField("ID", '<a href="tszh_house_edit.php?ID=' . $rowData["ID"] . '&lang=' . LANG . '">' . $rowData["ID"] . '</a>');
			continue;
		}
		elseif ($fieldName == 'TSZH_ID')
		{
			$row->AddSelectField($fieldName, $field['items']);
			$row->AddViewField($fieldName, '[<a href="tszh_edit.php?ID=' . $rowData[$fieldName] . '&lang=' . LANGUAGE_ID . '" title="' . GetMessage("AL_VIEW_TSZH") . '">' . $rowData[$fieldName] . '</a>] ' . $field['items'][$rowData[$fieldName]]);
			continue;
		}
		elseif ($fieldName == 'SITE_ID')
		{
			$row->AddSelectField($fieldName, $field['items']);
			$row->AddViewField($fieldName, $field['items'][$rowData[$fieldName]]);
			continue;
		}
		elseif ($fieldName == 'FULL_ADDRESS')
		{
			$row->AddViewField($fieldName, CTszhAccount::GetFullAddress($rowData, Array("FLAT")));
		}

		if (!isset($entityFields[$fieldName]['expression']))
		{
			$row->AddInputField($fieldName, array("size" => 20));
		}
	}
	$USER_FIELD_MANAGER->AddUserFields($UF_ENTITY, $rowData, $row);

	// ���������� ����������� ����
	$arActions = Array();

	// �������������� ��������
	$arActions[] = array(
		"ICON" => "edit",
		"DEFAULT" => true,
		"TEXT" => GetMessage("TSZH_GROUP_EDIT_TITLE"),
		"ACTION" => $lAdmin->ActionRedirect("tszh_house_edit.php?ID=" . $f_ID . '&lang=' . LANG)
	);

	// �������� ��������
	if ($POST_RIGHT >= "W")
	{
		$arActions[] = array(
			"ICON" => "delete",
			"TEXT" => GetMessage("TSZH_GROUP_DELETE_TITLE"),
			"ACTION" => "if(confirm('" . GetMessage('TSZH_GROUP_DELETE_CONFIRM') . "')) " . $lAdmin->ActionDoGroup($f_ID, "delete")
		);
	}

	// ������� �����������
	$arActions[] = array("SEPARATOR" => true);

	// ���� ��������� ������� - �����������, �������� �����.
	if (is_set($arActions[count($arActions) - 1], "SEPARATOR"))
	{
		unset($arActions[count($arActions) - 1]);
	}

	// �������� ����������� ���� � ������
	$row->AddActions($arActions);

}

// ������ �������
$lAdmin->AddFooter(
	array(
		array("title" => GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value" => $rsData->SelectedRowsCount()), // ���-�� ���������
		array("counter" => true, "title" => GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value" => "0"), // ������� ��������� ���������
	)
);

// ��������� ��������
$lAdmin->AddGroupActionTable(Array(
	"delete" => GetMessage("MAIN_ADMIN_LIST_DELETE"), // ������� ��������� ��������
	//	"activate"=>GetMessage("MAIN_ADMIN_LIST_ACTIVATE"), // ������������ ��������� ��������
	//	"deactivate"=>GetMessage("MAIN_ADMIN_LIST_DEACTIVATE"), // �������������� ��������� ��������
));

// ******************************************************************** //
//                ���������������� ����                                 //
// ******************************************************************** //

// ���������� ���� �� ������ ������ - ���������� ������
$aContext = array(
	array(
		"TEXT" => GetMessage("TSZH_ADD_ITEM"),
		"LINK" => "tszh_house_edit.php?lang=" . LANG,
		"TITLE" => GetMessage("TSZH_ADD_ITEM_TITLE"),
		"ICON" => "btn_new",
	),
);

// � ��������� ��� � ������
$lAdmin->AddAdminContextMenu($aContext);

// ******************************************************************** //
//                �����                                                 //
// ******************************************************************** //

if (($arID = $lAdmin->GroupAction()) && $POST_RIGHT >= "W" && isset($messageOK) && $_REQUEST['action'] == "delete")
{
	$message = new CAdminMessage(array("MESSAGE" => $messageOK, "TYPE" => "OK"), false);
	echo $message->Show();
}

// �������������� �����
$lAdmin->CheckListMode();

$APPLICATION->SetTitle(GetMessage("TSZH_HOUSES"));

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

echo $demoNotice;

if (isset($message) && isset($_REQUEST["edit_form_del"]))
{
	$message = new CAdminMessage(array("MESSAGE" => $messageOK, "TYPE" => "OK"), false);
	echo $message->Show();
}

// ******************************************************************** //
//                ����� �������                                         //
// ******************************************************************** //

$arFilterItems = Array();
foreach ($arHeaders as $arField)
{
	$arFilterItems[] = $arField['content'];
}

$USER_FIELD_MANAGER->AddFindFields($UF_ENTITY, $arFilterItems);
$oFilter = new CAdminFilter(
	$sTableID . "_filter",
	$arFilterItems
);

?>
	<form name="form_filter" method="get" action="<? echo $APPLICATION->GetCurPage(); ?>">
		<?
		$oFilter->Begin();

		TszhShowShowAdminFilter($arHeaders);

		$USER_FIELD_MANAGER->AdminListShowFilter($UF_ENTITY);

		$oFilter->Buttons(array("table_id" => $sTableID, "url" => $APPLICATION->GetCurPage(), "form" => "form_filter"));
		$oFilter->End();
		?>
	</form>
<?

// ������� ������� ������ ���������
$lAdmin->DisplayList();

//require($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog_admin_js.php");

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>