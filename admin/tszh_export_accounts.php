<?
/**
 * ������ ���ƻ
 * �������� �������� ���������� ������� ���������� ������� ������ � ����� � 1�
 * @package tszh
*/

// ��������� ��� ����������� �����:
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php"); // ������ ����� ������

ob_start();
if (!CTszhFunctionalityController::CheckEdition())
{
	$c = ob_get_contents();
	ob_end_clean();
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
	echo $c;
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
	return;
}
$demoNotice = ob_get_contents();
ob_end_clean();

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/citrus.tszh/include.php"); // ������������� ������
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/citrus.tszh/prolog.php"); // ������ ������

// ��������� �������� ����
IncludeModuleLangFile(__FILE__);

set_time_limit(0);

$step = IntVal($_REQUEST['step']);
if ($step < 1 || $step > 2 || !check_bitrix_sessid()) {
	$step = 1;
}

// ������� ����� ������� �������� ������������ �� ������
$POST_RIGHT = $APPLICATION->GetGroupRight("citrus.tszh");

// ���� ��� ���� - �������� � ����� ����������� � ���������� �� ������
if ($POST_RIGHT < "E")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$strError = false;

// �������� ���� ��� �������
$arTszhFilter = $arTszhRight = array();
if(CModule::IncludeModule("vdgb.portaltszh") && TSZH_PORTAL === true && !$USER->IsAdmin())
{
	global $USER;
	$arGroups = $USER->GetUserGroupArray();

	$rsPerms = \Vdgb\PortalTszh\PermsTable::getList(
		array(
			"filter" => array("@GROUP_ID" => $arGroups)
		)
	);

	while($arPerms = $rsPerms->fetch())
	{
		if($arPerms["PERMS"] >= "R")
		{
			$arTszhRight[$arPerms["TSZH_ID"]] = $arPerms["PERMS"];
		}
	}

	$arTszhFilter["@ID"] = array_keys($arTszhRight);
}

// �������� ����� ��������
if ($_REQUEST['delete_btn'] && check_bitrix_sessid())
{
	if (strlen($_SESSION['citrus.tszh.export.accounts']['FILENAME']) > 0 && file_exists($_SERVER['DOCUMENT_ROOT'] . $_SESSION['citrus.tszh.export.accounts']['FILENAME']))
	{
		unlink($_SERVER['DOCUMENT_ROOT'] . $_SESSION['citrus.tszh.export.accounts']['FILENAME']);
		unset($_SESSION['citrus.tszh.export.accounts']);
	}
}


// ��������� � ���������� ������

if ($step > 1 && check_bitrix_sessid()) {

	if ($step == 2) {

		$lastID = isset($_REQUEST['lastID']) && IntVal($_REQUEST['lastID']) > 0 ? IntVal($_REQUEST['lastID']) : 0;
		if ($lastID <= 0)
		{
			$_SESSION['citrus.tszh.export.accounts'] = Array(
				'FILENAME' => "/upload/export_accounts_" . date('Y-m-d_Hms') . ".xml",
				'TSZH_ID' => IntVal($_REQUEST["tszhID"]),
				'STEP_TIME' => isset($_REQUEST['step_time']) && IntVal($_REQUEST['step_time']) > 0 ? IntVal($_REQUEST['step_time']) : 25,
				'TSZH' => CTszh::GetByID($_REQUEST["tszhID"]),
			);
		}

		$filename = $_SESSION['citrus.tszh.export.accounts']['FILENAME'];
		$tszhID = $_SESSION['citrus.tszh.export.accounts']['TSZH_ID'];
		$stepTime = $_SESSION['citrus.tszh.export.accounts']['STEP_TIME'];
		$arTszh = $_SESSION['citrus.tszh.export.accounts']['TSZH'];

		if (is_array($arTszh))
		{
			$orgName = $arTszh["NAME"];
			$orgINN = $arTszh["INN"];
		}
		else
		{
			$strError .= GetMessage("TSZH_EXPORT_NO_TSZH_SELECTED");
		}

		if (strlen($strError) <= 0)
		{
			global $total, $cnt;
			
			$obExport = new CTszhExport();
			
			$arValueFilter = Array();
				
			$lastID = $obExport->DoExportAccess($orgINN, $_SERVER['DOCUMENT_ROOT'] . $filename, Array("TSZH_ID" => $tszhID), $stepTime, $lastID);
			$leftCnt = $total-$cnt;  
			if ($lastID === false)
			{
	   			if ($ex = $APPLICATION->GetException())
	   			{
					$strError = $ex->GetString() . '<br />';
				}
				else
				{
					$strError = GetMessage("TEA_ERROR_EXPORT") . "<br />";
				}
				$APPLICATION->ResetException();
			}
			elseif ($leftCnt > 0)
			{
				$href = $APPLICATION->GetCurPageParam("lastID=$lastID&step=$step&" . bitrix_sessid_get(), Array('tszhID', "lastID", 'step', 'step_time', 'sessid'));
				?>
				<!doctype html>
				<html>
				<meta http-equiv="REFRESH" content="0;url=<?=$href?>">
				</html>
				<body>
					<?
					echo GetMessage("TSZH_EXPORT_PROGRESS", array('#CNT#' => $cnt, '#TOTAL#' => $total-$cnt));
					?>
				</body>
				<?
				return;
			}
		}

		if (strlen($strError) <= 0)
		{
			// ��������� ���� ��������� ��������� (����� �������� ����� ����� �������)
			//COption::SetOptionString("citrus.tszh", "meters_block_edit", "Y");
		}

		if (strlen($strError) > 0)
			$step = 1;
	}
}

$APPLICATION->SetTitle(GetMessage("TEA_PAGE_TITLE"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php"); // ������ ����� ������

echo $demoNotice;

//==========================================================================================
//==========================================================================================


CAdminMessage::ShowMessage($strError);

?>
<form method="POST" action="<?echo $sDocPath?>?lang=<?echo LANG ?>" enctype="multipart/form-data" name="dataload" id="dataload">
<?=bitrix_sessid_post()?>
<?

// ����� ��������
//==========================================================================================
$aTabs = array(
	array("DIV" => "edit1", "TAB" => GetMessage("TEA_TAB1"), "ICON" => "iblock", "TITLE" => GetMessage("TEA_TAB1_TITLE")),
	array("DIV" => "edit2", "TAB" => GetMessage("TEA_TAB2"), "ICON" => "iblock", "TITLE" => GetMessage("TEA_TAB2_TITLE")),
);

$tabControl = new CAdminTabControl("tabControl", $aTabs, false);
$tabControl->Begin();

$tabControl->BeginNextTab();

if ($step == 1) {?>
	<tr>
		<td></td>
		<td>
			<?echo BeginNote();?>
			<?echo GetMessage("TSZH_EXPORT_TEXT");?>
			<?echo EndNote();?>
		</td>
	</tr>
	<tr>
		<td><?echo GetMessage("TSZH_EXPORT_TSZH");?>:</td>
		<td>
			<select name="tszhID" id="tszhID">
				<?
				$dbTszh = CTszh::GetList(Array("NAME" => "ASC"), $arTszhFilter);
				while ($arTszh = $dbTszh->Fetch())
				{
					?><option value="<?= $arTszh["ID"] ?>"<?if ($arTszh["ID"] == CUserOptions::GetOption('citrus.tszh.import', 'TszhID', false)) echo " selected";?>>[<?= htmlspecialcharsex($arTszh["ID"]) ?>]&nbsp;<?= htmlspecialcharsex($arTszh["NAME"]) ?></option><?
				}
				?>
			</select>
		</td>
	</tr>
	<tr>
		<td><?echo GetMessage("TSZH_EXPORT_STEP_TIME");?>:</td>
		<td>
			<input name="step_time" value="25" type="text" />
		</td>
	</tr>
<?
}
$tabControl->EndTab();

$tabControl->BeginNextTab();
if ($step == 2):?>
	<tr>
		<td>
			<?=GetMessage("TEA_EXPORT_DONE")?>.<br />
			<a href="<?=htmlspecialcharsbx($filename)?>" target="_blank" download="<?=htmlspecialcharsbx(basename($filename))?>"><?=GetMessage("TEA_DOWNLOAD_XML")?></a>
		</td>
	</tr>
<?endif;
$tabControl->EndTab();

$tabControl->Buttons();?>
<input type="hidden" name="step" value="<?=$step+1?>" />
<?if ($step < 2):?>
	<input type="submit" name="submit_btn" value="<?=GetMessage("TEA_NEXT_BTN")?>" class="adm-btn-save" />
<?else:?>
	<input type="submit" name="delete_btn" value="<?=GetMessage("TEA_DELETE_FILE_BTN")?>" />
<?endif;
$tabControl->End();?>
</form>

<script language="JavaScript">
<!--
<?if ($step < 2):?>
tabControl.SelectTab("edit1");
tabControl.DisableTab("edit2");
<?elseif ($step >= 2):?>
tabControl.SelectTab("edit2");
tabControl.DisableTab("edit1");
<?endif;?>
//-->
</script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>