<?
// ��������� ��� ����������� �����:
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php"); // ������ ����� ������

ob_start();
if (!CTszhFunctionalityController::CheckEdition())
{
	$c = ob_get_contents();
	ob_end_clean();
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
	echo $c;
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");

	return;
}
$demoNotice = ob_get_contents();
ob_end_clean();

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszh/include.php"); // ������������� ������
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszh/prolog.php"); // ������ ������

// ��������� �������� ����
IncludeModuleLangFile(__FILE__);

// ������� ����� ������� �������� ������������ �� ������
$POST_RIGHT = $APPLICATION->GetGroupRight("citrus.tszh");

// ���� ��� ���� - �������� � ����� ����������� � ���������� �� ������
if ($POST_RIGHT == "D")
{
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

$UF_ENTITY = "TSZH_ACCOUNT_PERIOD";
$sTableID = "tbl_account_periods"; // ID �������
$oSort = new CAdminSorting($sTableID, "ID", "DESC"); // ������ ����������
$lAdmin = new CAdminList($sTableID, $oSort); // �������� ������ ������
//echo '<pre>'; var_dump($oSort); echo '</pre>';

// �������� ���� ��� �������
$arTszhFilter = $arTszhRight = $arListFilter = array();
if (CTszhFunctionalityController::isPortal() && !$USER->IsAdmin())
{
	global $USER;
	$arGroups = $USER->GetUserGroupArray();

	$rsPerms = \Vdgb\PortalTszh\PermsTable::getList(
		array(
			"filter" => array("@GROUP_ID" => $arGroups)
		)
	);

	while ($arPerms = $rsPerms->fetch())
	{
		if ($arPerms["PERMS"] >= "R")
		{
			$arTszhRight[$arPerms["TSZH_ID"]] = $arPerms["PERMS"];
		}
	}

	$arTszhFilter["@ID"] = $arListFilter["@PERIOD_TSZH_ID"] = array_keys($arTszhRight);
}

// ******************************************************************** //
//                           ������                                     //
// ******************************************************************** //

// *********************** CheckFilter ******************************** //
// �������� �������� ������� ��� �������� ������� � ��������� �������
function CheckFilter()
{
	global $FilterArr, $lAdmin;
	foreach ($FilterArr as $f)
	{
		global $$f;
	}

	if (strlen(trim($find_area1)) > 0 && strlen(trim($find_area2)) > 0)
	{
		if (FloatVal($find_area1) > $find_area2)
		{
			$lAdmin->AddFilterError(GetMessage("APL_ERROR_F_AREA"));
		}
	}
	if (strlen(trim($find_larea1)) > 0 && strlen(trim($find_larea2)) > 0)
	{
		if (FloatVal($find_larea1) > $find_larea2)
		{
			$lAdmin->AddFilterError(GetMessage("APL_ERROR_F_LAREA"));
		}
	}
	if (strlen(trim($find_people1)) > 0 && strlen(trim($find_people2)) > 0)
	{
		if (FloatVal($find_people1) > $find_people2)
		{
			$lAdmin->AddFilterError(GetMessage("APL_ERROR_F_PEOPLE"));
		}
	}

	if (strlen(trim($find_flat1)) > 0 && strlen(trim($find_flat2)) > 0)
	{
		if (FloatVal($find_flat1) > $find_flat2)
		{
			$lAdmin->AddFilterError(GetMessage("APL_ERROR_F_FLAT"));
		}
	}

	if (strlen(trim($find_house1)) > 0 && strlen(trim($find_house2)) > 0)
	{
		if (FloatVal($find_house1) > $find_house2)
		{
			$lAdmin->AddFilterError(GetMessage("APL_ERROR_F_HOUSE"));
		}
	}

	return count($lAdmin->arFilterErrors) == 0; // ���� ������ ����, ������ false;
}

// *********************** /CheckFilter ******************************* //

// ������ �������� �������
$FilterArr = Array(
	"find",
	"find_type",
	"find_id",
	'find_account',
	'find_account_number',
	'find_period',
	'find_receipt_type',

	"find_name",
	"find_address",
	"find_area1",
	"find_area2",
	"find_larea1",
	"find_larea2",
	"find_people1",
	"find_people2",

	"find_city",
	"find_district",
	"find_region",
	"find_settlement",
	"find_street",
	"find_house1",
	"find_house2",
	"find_flat1",
	"find_flat2",

	"find_debt_beg1",
	"find_debt_beg2",
	"find_debt_end1",
	"find_debt_end2",
);
$USER_FIELD_MANAGER->AdminListAddFilterFields($UF_ENTITY, $FilterArr);

// �������������� ������
$lAdmin->InitFilter($FilterArr);

$arFilter = Array();

// ���� ��� �������� ������� ���������, ���������� ���
if (CheckFilter())
{
	if (strlen($_REQUEST['find']) > 0 && strlen($_REQUEST['find_type']) > 0)
	{
		switch ($_REQUEST['find_type'])
		{
			case 'ID':
				$arFilter['ID'] = $_REQUEST['find'];
				break;
			case 'ACCOUNT_XML_ID':
				$arFilter['XML_ID'] = $_REQUEST['find'];
				break;
			default:
		}
	}
	if (strlen($_REQUEST['find_id']) > 0)
	{
		$arFilter['ID'] = $_REQUEST['find_id'];
	}
	if (strlen($_REQUEST['find_account']) > 0)
	{
		$arFilter['ACCOUNT_ID'] = IntVal($_REQUEST['find_account']);
	}
	if (strlen($_REQUEST['find_account_number']) > 0)
	{
		$arFilter['XML_ID'] = $_REQUEST['find_account_number'];
	}
	if (IntVal($_REQUEST['find_period']) > 0)
	{
		$arFilter['PERIOD_ID'] = $_REQUEST['find_period'];
	}

	if (isset($_REQUEST['find_receipt_type']))
	{
		$arFilter['TYPE'] = $_REQUEST['find_receipt_type'];
	}

	if (strlen($find_name) > 0)
	{
		$arFilter["%USER_FULL_NAME"] = $find_name;
	}
	if (strlen($find_address) > 0)
	{
		$arFilter["%ADDRESS_FULL"] = $find_address;
	}

	if (strlen($find_area1) > 0)
	{
		$arFilter[">=AREA"] = $find_area1;
	}
	if (strlen($find_area2) > 0)
	{
		$arFilter["<=AREA"] = $find_area2;
	}
	if (strlen($find_larea1) > 0)
	{
		$arFilter[">=LIVING_AREA"] = $find_larea1;
	}
	if (strlen($find_larea2) > 0)
	{
		$arFilter["<=LIVING_AREA"] = $find_larea2;
	}
	if (strlen($find_people1) > 0)
	{
		$arFilter[">=PEOPLE"] = $find_people1;
	}
	if (strlen($find_people2) > 0)
	{
		$arFilter["<=PEOPLE"] = $find_people2;
	}

	if (strlen($find_city) > 0)
	{
		$arFilter["%CITY"] = $find_city;
	}
	if (strlen($find_district) > 0)
	{
		$arFilter["%DISTRICT"] = $find_district;
	}
	if (strlen($find_region) > 0)
	{
		$arFilter["%REGION"] = $find_region;
	}
	if (strlen($find_settlement) > 0)
	{
		$arFilter["%SETTLEMENT"] = $find_settlement;
	}
	if (strlen($find_street) > 0)
	{
		$arFilter["%STREET"] = $find_street;
	}

	if (strlen($find_house1) > 0)
	{
		$arFilter[">=HOUSE"] = $find_house1;
	}
	if (strlen($find_house2) > 0)
	{
		$arFilter["<=HOUSE"] = $find_house2;
	}
	if (strlen($find_flat1) > 0)
	{
		$arFilter[">=FLAT_INT"] = $find_flat1;
	}
	if (strlen($find_flat2) > 0)
	{
		$arFilter["<=FLAT_INT"] = $find_flat2;
	}

	if (strlen($find_debt_beg1) > 0)
	{
		$arFilter[">=DEBT_BEG"] = $find_debt_beg1;
	}
	if (strlen($find_debt_beg2) > 0)
	{
		$arFilter["<=DEBT_BEG"] = $find_debt_beg2;
	}
	if (strlen($find_debt_end1) > 0)
	{
		$arFilter[">=DEBT_END"] = $find_debt_end1;
	}
	if (strlen($find_debt_end2) > 0)
	{
		$arFilter["<=DEBT_END"] = $find_debt_end2;
	}

	$USER_FIELD_MANAGER->AdminListAddFilter($UF_ENTITY, $arFilter);
}

// ******************************************************************** //
//                ��������� �������� ��� ���������� ������              //
// ******************************************************************** //

// ���������� ����������������� ���������
if ($lAdmin->EditAction() && $POST_RIGHT >= "W")
{
	// ������� �� ������ ���������� ���������
	foreach ($FIELDS as $ID => $arFields)
	{
		if (!$lAdmin->IsUpdated($ID))
		{
			continue;
		}

		// �������� ��������� ������� ��������
		$DB->StartTransaction();
		$ID = IntVal($ID);
		if ($arData = CTszhAccountPeriod::GetByID($ID))
		{
			$USER_FIELD_MANAGER->AdminListPrepareFields($UF_ENTITY, $arFields);
			if (!CTszhAccountPeriod::Update($ID, $arFields))
			{
				$lAdmin->AddGroupError(GetMessage("APL_ERROR_UPDATE"), $ID);
				$DB->Rollback();
			}
		}
		else
		{
			$lAdmin->AddGroupError(GetMessage("APL_ERROR_UPDATE_NOT_FOUND"), $ID);
			$DB->Rollback();
		}
		$DB->Commit();
	}
}

$isTszhMinEditionStandard = tszhCheckMinEdition('standard');

$strPrintFilter = false;
// ��������� ��������� � ��������� ��������
if (($arID = $lAdmin->GroupAction()) && $POST_RIGHT == "W")
{
	if ($_REQUEST['action'] == 'print' && $isTszhMinEditionStandard)
	{
		$printTemplate = trim($_REQUEST['template']);

		if ($_REQUEST['action_target'] == 'selected')
		{
			$arPrintFilter = $arFilter;
		}
		else
		{
			$arPrintFilter["ID"] = $arID;
		}

		$arSort = Array(
			"ACCOUNT_ID" => "ASC",
			"TYPE" => "ASC",
		);
		$arSortBy = explode('|', $by);
		foreach ($arSortBy as $sBy)
		{
			$arSort[$sBy] = $order;
		}

		$hash = md5(microtime(1) . 'rnd_hash');
		$_SESSION["TSZH_PRINT"][$hash]['filter'] = $arPrintFilter;
		$_SESSION["TSZH_PRINT"][$hash]['sort'] = $arSort;
		$_SESSION["TSZH_PRINT"][$hash]['template'] = CTszhReceiptTemplateProcessor::CheckTemplate($printTemplate) ? $printTemplate : '/bitrix/components/citrus/tszh.receipt/templates/.default';
		$_SESSION["TSZH_PRINT"][$hash]['timestamp'] = time();

		?>
		<script type="text/javascript">
			var w = (opener ? opener.window : parent.window);
			if ('undefined' !== typeof(w.CloseWaitWindow))
				w.CloseWaitWindow();
			w.open('<?=('/bitrix/admin/tszh_print_receipt.php?lang=' . LANG . '&hash=' . urlencode($hash))?>');
		</script>
		<?
		//		return;

	}
	elseif ($_REQUEST['action'] == 'send' && $isTszhMinEditionStandard)
	{
		// ������ �������� ��������� (��������� ���������)
		if ($_REQUEST['action_target'] == 'selected')
		{
			$arSendFilter = $arFilter;
		}
		else
		{
			$arSendFilter["ID"] = $arID;
		}

		CTszhSubscribe::sendReceipts($arSendFilter);
	}
	elseif ($_REQUEST['action'] == 'send_not_sent' && $isTszhMinEditionStandard)
	{
		// ������ �������� ��������� (��������� �� ������������)
		if ($_REQUEST['action_target'] == 'selected')
		{
			$arSendFilter = $arFilter;
		}
		else
		{
			$arSendFilter["ID"] = $arID;
		}

		CTszhSubscribe::sendReceipts($arSendFilter, true);
	}
	else
	{
		// ���� ������� "��� ���� ���������"
		if ($_REQUEST['action_target'] == 'selected')
		{
			$arSort = Array();
			$arSortBy = explode('|', $by);
			foreach ($arSortBy as $sBy)
			{
				$arSort[$sBy] = $order;
			}

			$rsData = CTszhAccountPeriod::GetList($arSort, $arFilter);
			while ($arRes = $rsData->Fetch())
			{
				$arID[] = $arRes['ID'];
			}
		}

		// ������� �� ������ ���������
		foreach ($arID as $ID)
		{
			if (strlen($ID) <= 0)
			{
				continue;
			}
			$ID = IntVal($ID);

			// ��� ������� �������� �������� ��������� ��������
			switch ($_REQUEST['action'])
			{
				// ��������
				case "delete":
					@set_time_limit(0);
					$DB->StartTransaction();
					if (!CTszhAccountPeriod::Delete($ID))
					{
						$DB->Rollback();
						$lAdmin->AddGroupError(GetMessage("APL_ERROR_DELETE"), $ID);
					}
					$DB->Commit();
					break;
			}
		}
	}
}

// ******************************************************************** //
//                ������� ��������� ������                              //
// ******************************************************************** //


$arSort = Array();
$arSortBy = explode('|', $by);
foreach ($arSortBy as $sBy)
{
	$arSort[$sBy] = $order;
}


$rsPeriods = CTszhPeriod::GetList();
$arPeriodRef = Array();
$arPeriodRef["REFERENCE"][] = '(' . GetMessage("APL_ALL") . ')';
$arPeriodRef["REFERENCE_ID"][] = '0';
while ($arPeriod = $rsPeriods->GetNext())
{
	$arPeriodRef["REFERENCE"][] = CTszhPeriod::Format($arPeriod['DATE']);
	$arPeriodRef["REFERENCE_ID"][] = $arPeriod['ID'];
}

$rsData = CTszhAccountPeriod::GetList(
	$arSort,
	array_merge($arFilter, $arListFilter),
	false,
	false,
	Array('*')
);

// ����������� ������ � ��������� ������ CAdminResult
$rsData = new CAdminResult($rsData, $sTableID);

// ���������� CDBResult �������������� ������������ ���������.
$rsData->NavStart();

// �������� ����� ������������� ������� � �������� ������ $lAdmin
$lAdmin->NavText($rsData->GetNavPrint(GetMessage("APL_NAV_TITLE")));

// ******************************************************************** //
//                ���������� ������ � ������                            //
// ******************************************************************** //

$arAdminListHeaders = array(
	array(
		"id" => "ID",
		"content" => "ID",
		"sort" => "ID",
		"align" => "left",
		"default" => true,
	),
	array(
		"id" => "PERIOD_DATE",
		"content" => GetMessage("APL_PERIOD"),
		"sort" => "PERIOD_DATE",
		"align" => "left",
		"default" => true,
	),
	array(
		"id" => "TYPE",
		"content" => GetMessage("APL_F_TYPE"),
		"sort" => "TYPE",
		"align" => "left",
		"default" => true,
	),
	array(
		"id" => "ACCOUNT_ID",
		"content" => GetMessage("APL_ACCOUNT"),
		"sort" => "ACCOUNT_ID",
		"align" => "left",
		"default" => true,
	),
	array(
		"id" => "DEBT_BEG",
		"content" => GetMessage("APL_DEBT_BEG"),
		"sort" => "DEBT_BEG",
		"align" => "right",
		"default" => true,
	),
	array(
		"id" => "DEBT_END",
		"content" => GetMessage("APL_DEBT_END"),
		"sort" => "DEBT_END",
		"align" => "right",
		"default" => true,
	),
	array(
		"id" => "SUM_PAYED",
		"content" => GetMessage("APL_SUM_PAYED"),
		"sort" => "DEBT_END",
		"align" => "right",
		"default" => false,
	),
	array(
		"id" => "SUMM_TO_PAY",
		"content" => GetMessage("APL_SUMM_TO_PAY"),
		"sort" => "DEBT_END",
		"align" => "right",
		"default" => false,
	),
	array(
		"id" => "BARCODE",
		"content" => GetMessage("APL_BARCODE"),
		"sort" => "BARCODE",
		"align" => "left",
		"default" => false,
	),

	array(
		"id" => "ADDRESS",
		"content" => GetMessage("APL_ADDRESS"),
		"sort" => "STREET|HOUSE|FLAT",
		"align" => "left",
		"default" => true,
	),
	array(
		"id" => "AREA",
		"content" => GetMessage("APL_AREA"),
		"sort" => "AREA",
		"align" => "left",
		"default" => false,
	),
	array(
		"id" => "LIVING_AREA",
		"content" => GetMessage("APL_LAREA"),
		"sort" => "LIVING_AREA",
		"align" => "left",
		"default" => false,
	),
	array(
		"id" => "PEOPLE",
		"content" => GetMessage("APL_PEOPLE"),
		"sort" => "PEOPLE",
		"align" => "left",
		"default" => false,
	),
	array(
		"id" => "DATE_SENT",
		"content" => GetMessage("APL_DATE_SENT"),
		"sort" => "DATE_SENT",
		"align" => "left",
		"default" => false,
	),
);
$USER_FIELD_MANAGER->AdminListAddHeaders($UF_ENTITY, $arAdminListHeaders);
$lAdmin->AddHeaders($arAdminListHeaders);

while ($arRes = $rsData->NavNext(true, "f_")):

	// ������� ������. ��������� - ��������� ������ CAdminListRow
	$row =& $lAdmin->AddRow($f_ID, $arRes);

	// ����� �������� ����������� �������� ��� ��������� � �������������� ������
	$row->AddViewField('ID', '<a href="tszh_account_period_edit.php?ID=' . $arRes['ID'] . '&lang=' . LANGUAGE_ID . '" title="' . GetMessage("APL_EDIT_RECEIPT") . '">' . $arRes['ID'] . '</a>');

	$sFullName = $arRes["USER_ID"] ? trim(
		'(' . $arRes['USER_LOGIN'] . ') ' .
		$arRes['USER_NAME'] . ' ' .
		$arRes['USER_SECOND_NAME'] . ' ' .
		$arRes['USER_LAST_NAME']
	) : $arRes["ACCOUNT_NAME"];
	if ($arRes['ACCOUNT_ID'] > 0)
	{
		$row->AddViewField('ACCOUNT_ID', '[<a href="tszh_account_edit.php?ID=' . $arRes['ACCOUNT_ID'] . '&lang=' . LANGUAGE_ID . '" title="' . GetMessage("APL_EDIT_ACCOUNT") . '">' . $arRes['ACCOUNT_ID'] . '</a>] ' . $sFullName);
	}
	else
	{
		$row->AddViewField('ACCOUNT_ID', '�');
	}

	$sAddress = CTszhAccount::GetFullAddress($arRes);
	$row->AddViewField('ADDRESS', $sAddress);

	$row->AddViewField('DEBT_BEG', number_format($arRes['DEBT_BEG'], 2, ',', ' ') . GetMessage("APL_CURRENCY"));
	$row->AddInputField("DEBT_BEG");
	$row->AddViewField('DEBT_END', number_format($arRes['DEBT_END'], 2, ',', ' ') . GetMessage("APL_CURRENCY"));
	$row->AddInputField("DEBT_END");
	$row->AddViewField('SUM_PAYED', number_format($arRes['SUM_PAYED'], 2, ',', ' ') . GetMessage("APL_CURRENCY"));
	$row->AddInputField("SUM_PAYED");
	$row->AddViewField('SUMM_TO_PAY', number_format($arRes['SUMM_TO_PAY'], 2, ',', ' ') . GetMessage("APL_CURRENCY"));
	$row->AddInputField("SUMM_TO_PAY");

	$row->AddViewField('PERIOD_DATE', CTszhPeriod::Format($arRes['PERIOD_DATE']));

	$row->AddSelectField('TYPE', \Citrus\Tszh\Types\ReceiptType::getTitles());

	$USER_FIELD_MANAGER->AddUserFields($UF_ENTITY, $arRes, $row);

	// ���������� ����������� ����
	$arActions = Array();

	if ($POST_RIGHT >= "W")
	{
		// ��������� ��������
		$arActions[] = array(
			"ICON" => "edit",
			"TEXT" => GetMessage("APL_EDIT"),
			"DEFAULT" => "Y",
			"ACTION" => $lAdmin->ActionRedirect("tszh_account_period_edit.php?ID=" . $f_ID . '&LANG=' . LANG)
		);

		// �������� ��������
		$arActions[] = array(
			"ICON" => "delete",
			"TEXT" => GetMessage("APL_DELETE"),
			"ACTION" => "if(confirm('" . GetMessage("APL_DELETE_CONFIRM") . "')) " . $lAdmin->ActionDoGroup($f_ID, "delete")
		);

		if ($isTszhMinEditionStandard)
		{
			// ����������� ���������
			$arActions[] = array(
				"ICON" => "view",
				"TEXT" => GetMessage("CITRUS_TSZH_CONTEXT_PRINT"),
				"ACTION" => $lAdmin->ActionDoGroup($f_ID, "print", "template=" . CTszhReceiptTemplateProcessor::AUTO_DETECT),
			);
			// ������� ���������
			$arActions[] = array(
				"ICON" => "move",
				"TEXT" => GetMessage("APL_SEND"),
				"ACTION" => "if(confirm('" . GetMessage("APL_SEND_CONFIRM") . "')) " . $lAdmin->ActionDoGroup($f_ID, "send"),
			);
		}
	}

	// ������� �����������
	//$arActions[] = array("SEPARATOR"=>true);

	// ���� ��������� ������� - �����������, �������� �����.
	if (is_set($arActions[count($arActions) - 1], "SEPARATOR"))
	{
		unset($arActions[count($arActions) - 1]);
	}

	// �������� ����������� ���� � ������
	$row->AddActions($arActions);

endwhile;

// ������ �������
$lAdmin->AddFooter(
	array(
		array("title" => GetMessage("APL_CNT_TOTAL") . ":", "value" => $rsData->SelectedRowsCount()), // ���-�� ���������
		array("counter" => true, "title" => GetMessage("APL_CNT_SELECTED") . ":", "value" => "0"), // ������� ��������� ���������
	)
);

// ��������� ��������
$arGroupActions = Array(
	"delete" => GetMessage("MAIN_ADMIN_LIST_DELETE"), // ������� ��������� ��������
);
$arGroupActionsParams = Array();

if (tszhCheckMinEdition('standard'))
{
	$arPrintTemplates = CTszhReceiptTemplateProcessor::getTemplatesForSelect("AUTO_DETECT");
	if (count($arPrintTemplates) > 0)
	{
		$printTemplateSelectHTML = '&nbsp;&nbsp;<select name="template" id="print_template_select" style="display: none;">';
		foreach ($arPrintTemplates as $key => $value)
		{
			//$value = htmlspecialcharsbx($value);
			$key = htmlspecialcharsbx($key);
			$printTemplateSelectHTML .= "<option value=\"{$key}\">{$value}</option>";
		}
		$printTemplateSelectHTML .= "</select>\n";

		$arGroupActions["print"] = GetMessage("APL_GA_PRINT");
		$arGroupActions["print_html"] = Array(
			'type' => 'html',
			'value' => $printTemplateSelectHTML,
		);
		$arGroupActionsParams["select_onchange"] = "document.getElementById('print_template_select').style.display = (this[this.selectedIndex].value == 'print' ? 'inline' : 'none')";
	}

	$arGroupActions["send"] = GetMessage("APL_GA_SEND");
	$arGroupActions["send_not_sent"] = GetMessage("APL_GA_SEND_NOT_SENT");

	$arQueue = $DB->Query("SELECT COUNT(*) AS CNT FROM b_event WHERE EVENT_NAME = 'TSZH_RECEIPT' AND SUCCESS_EXEC = 'N'")->Fetch();
	if ($arQueue["CNT"] > 0)
	{
		$arGroupActions["send_queue"] = Array(
			'type' => 'html',
			'value' => '<style type="text/css">.adm-table-counter.adm-table-counter-visible {margin-top: 20px;}</style><span class="adm-table-counter" style="display: inline-block; position: absolute; right: 0; margin-top: 0px;">' . GetMessage("APL_GA_IN_SEND_QUEUE") . ': ' . $arQueue["CNT"] . '</span>',
		);
	}
}
$lAdmin->AddGroupActionTable($arGroupActions, $arGroupActionsParams);

// ******************************************************************** //
//                ���������������� ����                                 //
// ******************************************************************** //

$aContext = array(
	array(
		"TEXT" => GetMessage("APL_PERIOD_ADD"),
		"LINK" => "tszh_account_period_edit.php?lang=" . LANG,
		"TITLE" => GetMessage("APL_PERIOD_ADD_TITLE"),
		"ICON" => "btn_new",
	),
);

// � ��������� ��� � ������
$lAdmin->AddAdminContextMenu($aContext);

// ******************************************************************** //
//                �����                                                 //
// ******************************************************************** //

// �������������� �����
$lAdmin->CheckListMode();

// ��������� ��������� ��������
$APPLICATION->SetTitle(GetMessage("APL_PAGE_TITLE"));

// �� ������� ��������� ���������� ������ � �����
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

echo $demoNotice;

// ******************************************************************** //
//                ����� �������                                         //
// ******************************************************************** //

// �������� ������ �������
$arFindFields = array(
	"ID",
	GetMessage("APL_F_ACCOUNT"),
	GetMessage("APL_F_ACCOUNT_NUMBER"),
	GetMessage("APL_F_PERIOD"),
	GetMessage("APL_F_TYPE"),
	GetMessage("APL_F_FIO"),
	GetMessage("APL_F_ADDRESS"),
	GetMessage("APL_F_AREA"),
	GetMessage("APL_F_LAREA"),
	GetMessage("APL_F_PEOPLE"),
	GetMessage("APL_F_CITY"),
	GetMessage("APL_F_DISTRICT"),
	GetMessage("APL_F_REGION"),
	GetMessage("APL_F_SETTLEMENT"),
	GetMessage("APL_F_STREET"),
	GetMessage("APL_F_HOUSE"),
	GetMessage("APL_F_FLAT"),
	GetMessage("APL_F_DEBT_BEG"),
	GetMessage("APL_F_DEBT_END"),
);
$USER_FIELD_MANAGER->AddFindFields($UF_ENTITY, $arFindFields);
$oFilter = new CAdminFilter($sTableID . "_filter", $arFindFields);
?>
	<form name="find_form" method="get" action="<? echo $APPLICATION->GetCurPage(); ?>">
		<? $oFilter->Begin(); ?>
		<tr>
			<td><b><?=GetMessage("APL_F_FIND")?>:</b></td>
			<td>
				<input type="text" size="25" name="find" value="<? echo htmlspecialcharsbx($_REQUEST['find']) ?>" title=""/>
				<?
				$arr = array(
					"reference" => array(
						"ID",
						GetMessage("APL_F_ACCOUNT_NUMBER"),
					),
					"reference_id" => array(
						"ID",
						'ACCOUNT_XML_ID',
					)
				);
				echo SelectBoxFromArray("find_type", $arr, $_REQUEST['find_type'], "", "");
				?>
			</td>
		</tr>
		<tr>
			<td><?="ID"?>:</td>
			<td>
				<input type="text" name="find_id" size="7" value="<? echo htmlspecialcharsbx($_REQUEST['find_id']) ?>">
			</td>
		</tr>
		<tr>
			<td><?=GetMessage("APL_F_ACCOUNT")?>:</td>
			<td><?=tszhLookup('find_account', $_REQUEST['find_account'], Array("type" => 'account', "formName" => 'find_form'));?></td>
		</tr>
		<tr>
			<td><?=GetMessage("APL_F_ACCOUNT_NUMBER")?>:</td>
			<td>
				<input type="text" name="find_account_number" size="7" value="<? echo htmlspecialcharsbx($_REQUEST['find_account_number']) ?>">
			</td>
		</tr>
		<tr>
			<td><?=GetMessage("APL_F_PERIOD")?>:</td>
			<td><?=tszhLookup('find_period', $_REQUEST['find_period'], Array("type" => 'period', "formName" => 'find_form'));?></td>
		</tr>
		<tr>
			<td><?=GetMessage("APL_F_TYPE")?>:</td>
			<td><?=Citrus\Tszh\Types\ReceiptType::getSelect($find_receipt_type, 'find_receipt_type')?></td>
		</tr>
		<tr>
			<td><?=GetMessage("APL_F_FIO")?>:</td>
			<td>
				<input type="text" name="find_name" size="47" value="<? echo htmlspecialcharsbx($find_name) ?>">
			</td>
		</tr>
		<tr>
			<td><?=GetMessage("APL_F_ADDRESS")?>:</td>
			<td>
				<input type="text" name="find_address" size="47" value="<? echo htmlspecialcharsbx($find_address) ?>">
			</td>
		</tr>
		<tr>
			<td><?=GetMessage("APL_F_AREA")?>:</td>
			<td>
				<?=GetMessage("APL_F_FROM")?>
				<input type="text" name="find_area1" size="10" value="<? echo htmlspecialcharsbx($find_area1) ?>"/>
				<?=GetMessage("APL_F_TO")?>
				<input type="text" name="find_area2" size="10" value="<? echo htmlspecialcharsbx($find_area2) ?>"/>
			</td>
		</tr>
		<tr>
			<td><?=GetMessage("APL_F_LAREA")?>:</td>
			<td>
				<?=GetMessage("APL_F_FROM")?>
				<input type="text" name="find_larea1" size="10" value="<? echo htmlspecialcharsbx($find_larea1) ?>"/>
				<?=GetMessage("APL_F_TO")?>
				<input type="text" name="find_larea2" size="10" value="<? echo htmlspecialcharsbx($find_larea2) ?>"/>
			</td>
		</tr>
		<tr>
			<td><?=GetMessage("APL_F_PEOPLE")?>:</td>
			<td>
				<?=GetMessage("APL_F_FROM")?>
				<input type="text" name="find_people1" size="10" value="<? echo htmlspecialcharsbx($find_people1) ?>"/>
				<?=GetMessage("APL_F_TO")?>
				<input type="text" name="find_people2" size="10" value="<? echo htmlspecialcharsbx($find_people2) ?>"/>
			</td>
		</tr>
		<tr>
			<td><?=GetMessage("APL_F_CITY")?>:</td>
			<td>
				<input type="text" name="find_city" size="47" value="<? echo htmlspecialcharsbx($find_city) ?>">
			</td>
		</tr>
		<tr>
			<td><?=GetMessage("APL_F_DISTRICT")?>:</td>
			<td>
				<input type="text" name="find_district" size="47" value="<? echo htmlspecialcharsbx($find_district) ?>">
			</td>
		</tr>
		<tr>
			<td><?=GetMessage("APL_F_REGION")?>:</td>
			<td>
				<input type="text" name="find_region" size="47" value="<? echo htmlspecialcharsbx($find_region) ?>">
			</td>
		</tr>
		<tr>
			<td><?=GetMessage("APL_F_SETTLEMENT")?>:</td>
			<td>
				<input type="text" name="find_settlement" size="47" value="<? echo htmlspecialcharsbx($find_settlement) ?>">
			</td>
		</tr>
		<tr>
			<td><?=GetMessage("APL_F_STREET")?>:</td>
			<td>
				<input type="text" name="find_street" size="47" value="<? echo htmlspecialcharsbx($find_street) ?>">
			</td>
		</tr>
		<tr>
			<td><?=GetMessage("APL_F_HOUSE")?>:</td>
			<td>
				<?=GetMessage("APL_F_FROM")?>
				<input type="text" name="find_house1" size="10" value="<? echo htmlspecialcharsbx($find_house1) ?>"/>
				<?=GetMessage("APL_F_TO")?>
				<input type="text" name="find_house2" size="10" value="<? echo htmlspecialcharsbx($find_house2) ?>"/>
			</td>
		</tr>
		<tr>
			<td><?=GetMessage("APL_F_FLAT")?>:</td>
			<td>
				<?=GetMessage("APL_F_FROM")?>
				<input type="text" name="find_flat1" size="10" value="<? echo htmlspecialcharsbx($find_flat1) ?>"/>
				<?=GetMessage("APL_F_TO")?>
				<input type="text" name="find_flat2" size="10" value="<? echo htmlspecialcharsbx($find_flat2) ?>"/>
			</td>
		</tr>
		<tr>
			<td><?=GetMessage("APL_F_DEBT_BEG")?>:</td>
			<td>
				<?=GetMessage("APL_F_FROM")?>
				<input type="text" name="find_debt_beg1" size="10" value="<? echo htmlspecialcharsbx($find_debt_beg1) ?>"/>
				<?=GetMessage("APL_F_TO")?>
				<input type="text" name="find_debt_beg2" size="10" value="<? echo htmlspecialcharsbx($find_debt_beg2) ?>"/>
			</td>
		</tr>
		<tr>
			<td><?=GetMessage("APL_F_DEBT_END")?>:</td>
			<td>
				<?=GetMessage("APL_F_FROM")?>
				<input type="text" name="find_debt_end1" size="10" value="<? echo htmlspecialcharsbx($find_debt_end1) ?>"/>
				<?=GetMessage("APL_F_TO")?>
				<input type="text" name="find_debt_end2" size="10" value="<? echo htmlspecialcharsbx($find_debt_end2) ?>"/>
			</td>
		</tr>
		<?

		$USER_FIELD_MANAGER->AdminListShowFilter($UF_ENTITY);

		$oFilter->Buttons(array("table_id" => $sTableID, "url" => $APPLICATION->GetCurPage(), "form" => "find_form"));
		$oFilter->End();
		?>
	</form>

<?
// ������� ������� ������ ���������
$lAdmin->DisplayList();
?>

<?
// ���������� ��������
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>