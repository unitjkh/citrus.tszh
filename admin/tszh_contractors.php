<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/citrus.tszh/include.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/citrus.tszh/prolog.php");

IncludeModuleLangFile(__FILE__);

ob_start();
if (!CTszhFunctionalityController::CheckEdition())
{
	$c = ob_get_contents();
	ob_end_clean();
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
	echo $c;
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
	return;
}
$demoNotice = ob_get_contents();
ob_end_clean();


// ������� ����� ������� �������� ������������ �� ������
$POST_RIGHT = $APPLICATION->GetGroupRight("citrus.tszh");
// ���� ��� ���� - �������� � ����� ����������� � ���������� �� ������
if ($POST_RIGHT <= "D")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$UF_ENTITY = "TSZH_CONTRACTOR";

$sTableID = "tbl_tszh_contractors";
$oSort = new CAdminSorting($sTableID, "ID", "desc");
$lAdmin = new CAdminList($sTableID, $oSort);

$arHeaders = array(
	array("id" => "ID",					"content" => GetMessage("TSZH_F_ID"),				"sort" => "id",				"align" => "right",		"default" => true,		"type" => "int"),
	array("id" => "XML_ID",				"content" => GetMessage("TSZH_F_XML_ID"),			"sort" => "xml_id",			"align" => "left",		"default" => false,		"type" => "string"),
	array("id" => "TSZH_ID",			"content" => GetMessage("TSZH_F_TSZH_ID"),			"sort" => "tszh_id",		"align" => "left",		"default" => true,		"type" => "int"),
	array("id" => "EXECUTOR",			"content" => GetMessage("TSZH_F_EXECUTOR"),			"sort" => "executor",		"align" => "center",	"default" => false,		"type" => "checkbox"),
	array("id" => "NAME",				"content" => GetMessage("TSZH_F_NAME"),				"sort" => "name",			"align" => "left",		"default" => true,		"type" => "string"),
	array("id" => "ADDRESS",			"content" => GetMessage("TSZH_F_ADDRESS"),			"sort" => "address",		"align" => "left",		"default" => false,		"type" => "string"),
	array("id" => "SERVICES",			"content" => GetMessage("TSZH_F_SERVICES"),			"sort" => "services",		"align" => "left",		"default" => false,		"type" => "string"),
	array("id" => "PHONE",				"content" => GetMessage("TSZH_F_PHONE"),			"sort" => "phone",			"align" => "left",		"default" => true,		"type" => "string"),
	array("id" => "BILLING",			"content" => GetMessage("TSZH_F_BILLING"),			"sort" => "billing",		"align" => "left",		"default" => true,		"type" => "string"),
);

// ******************************************************************** //
//                           ������                                     //
// ******************************************************************** //

// *********************** CheckFilter ******************************** //
// �������� �������� ������� ��� �������� ������� � ��������� �������
function CheckFilter()
{
	global $FilterArr, $lAdmin;
	foreach ($FilterArr as $value) {
		global $$value;
	} 

	return true;
}
// *********************** /CheckFilter ******************************* //

// ������ �������� �������
$FilterArr = TszhAdminFilterParams($arHeaders);

// �������������� ������
$lAdmin->InitFilter($FilterArr);

$USER_FIELD_MANAGER->AdminListAddFilterFields($UF_ENTITY, $FilterArr);

// ���� ��� �������� ������� ���������, ���������� ���
$arFilter = array();
if (CheckFilter() && !is_set($_REQUEST, 'del_filter'))
{
	// �������� ������ ���������� ��� ������� �� ������ �������� �������
	TszhAdminMakeFilter($arFilter, $arHeaders);
	$USER_FIELD_MANAGER->AdminListAddFilter($UF_ENTITY, $arFilter);
}

// ******************************************************************** //
//                ��������� �������� ��������                           //
// ******************************************************************** //

if (isset($_REQUEST["edit_form_del"]) && IntVal($_REQUEST["edit_form_del"]) == 1 && check_bitrix_sessid())
{
	if (isset($_REQUEST["ID"]) && IntVal($_REQUEST["ID"]) > 0)
	{
		@set_time_limit(0);
        
		$arItem = CTszhContractor::GetByID($_REQUEST["ID"]);
		if (is_array($arItem))
		{
			if(CTszhContractor::Delete($arItem['ID']))
			{
				$messageOK = GetMessage("TSZH_DELETE_OK");
			}
			else
			{
				$lAdmin->AddGroupError(GetMessage("TSZH_DELETE_ERROR"), $ID);
			}
		}
		else
		{
			$lAdmin->AddGroupError(GetMessage("TSZH_DELETE_ERROR").". ".GetMessage("TSZH_DELETE_ITEM_NOT_FOUND", Array("#ID#" => $ID)), $ID);
		}
	}
}


// ******************************************************************** //
//                ��������� �������� ��� ���������� ������              //
// ******************************************************************** //

// ���������� ����������������� ���������
if ($lAdmin->EditAction() && $POST_RIGHT>="W" && check_bitrix_sessid())
{

	// ������� �� ������ ���������� ���������
	foreach($FIELDS as $ID=>$arFields)
	{
		if(!$lAdmin->IsUpdated($ID))
			continue;
	
	    // �������� ��������� ������� ��������
		$ID = IntVal($ID);
	
		$arItem= CTszhContractor::GetByID($ID);
		if (is_array($arItem))
		{
			$USER_FIELD_MANAGER->AdminListPrepareFields($UF_ENTITY, $arFields);
			if (!CTszhContractor::Update($ID, $arFields))
			{
				$strError = ''; 
				if ($ex = $APPLICATION->GetException())
					$strError = $ex->GetMessage();
				$lAdmin->AddGroupError(GetMessage("TSZH_GROUP_SAVE_ERROR")." ".$strError, $ID);
			}
		}
		else
		{
			$lAdmin->AddGroupError(GetMessage("TSZH_GROUP_SAVE_ERROR")." ".GetMessage("TSZH_GROUP_SAVE_ERROR_ITEM_NOT_FOUND"), $ID);
		}
	}
}

// ��������� ��������� � ��������� ��������
if (($arID = $lAdmin->GroupAction()) && $POST_RIGHT>="W" && check_bitrix_sessid())
{
	// ���� ������� "��� ���� ���������"
	if($_REQUEST['action_target']=='selected')
	{
		$rsItems = CTszhContractor::GetList(Array(), $arFilter, false, false, Array("ID"));
		$arID = Array();
		while ($arItem = $rsItems->Fetch())
			$arID[] = $arItem['ID'];
	}

	@set_time_limit(0);
	
	// ������� �� ������ ���������
	foreach($arID as $ID)
	{
		if(strlen($ID)<=0)
			continue;
		
		$ID = IntVal($ID);

		// ��� ������� �������� �������� ��������� ��������
		switch($_REQUEST['action'])
		{
			// ��������
			case "delete":
				$arItem = CTszhContractor::GetByID($ID);
				if (is_array($arItem))
				{
					if (CTszhContractor::Delete(IntVal($ID)))
						$messageOK = GetMessage("qroup_del_ok");
					else
						$lAdmin->AddGroupError(GetMessage("TSZH_DELETE_ERROR"), $ID);
				}
				else
					$lAdmin->AddGroupError(GetMessage("TSZH_DELETE_ERROR")." ".GetMessage("TSZH_DELETE_ITEM_NOT_FOUND"), $ID);
				break;
		}
	}
}

// ******************************************************************** //
//                ������� ��������� ������                              //
// ******************************************************************** //

// �������� ���� ��� �������
$arTszhFilter = $arTszhRight = array();
if (CTszhFunctionalityController::isPortal() && !$USER->IsAdmin())
{
	global $USER;
	$arGroups = $USER->GetUserGroupArray();

	$rsPerms = \Vdgb\PortalTszh\PermsTable::getList(
		array(
			"filter" => array("@GROUP_ID" => $arGroups)
		)
	);

	while($arPerms = $rsPerms->fetch())
	{
		if($arPerms["PERMS"] >= "R")
		{
			$arTszhRight[$arPerms["TSZH_ID"]] = $arPerms["PERMS"];
		}
	}

	$arTszhFilter["@TSZH_ID"] = array_keys($arTszhRight);
}

// ������� ������ �������
$rsItems = CTszhContractor::GetList(array($by=>$order), array_merge($arFilter, $arTszhFilter), false, false, Array("*","UF_*"));

// ����������� ������ � ��������� ������ CAdminResult
$rsData = new CAdminResult($rsItems, $sTableID);

// ���������� CDBResult �������������� ������������ ���������.
$rsData->NavStart();

// �������� ����� ������������� ������� � �������� ������ $lAdmin
$lAdmin->NavText($rsData->GetNavPrint(GetMessage("TSZH_CONTRACTORS")));

// ******************************************************************** //
//                ���������� ������ � ������                            //
// ******************************************************************** //

$USER_FIELD_MANAGER->AdminListAddHeaders($UF_ENTITY, $arHeaders);
$lAdmin->AddHeaders($arHeaders);

while($arRes = $rsData->NavNext(true, "f_"))
{

	// ������� ������. ��������� - ��������� ������ CAdminListRow
	$row =& $lAdmin->AddRow($f_ID, $arRes);
	
	// ����� �������� ����������� �������� ��� ��������� � �������������� ������

	// �������� NAME ����� ��������������� ��� �����, � ������������ �������
	$row->AddViewField("NAME", '<a href="tszh_contractors_edit.php?ID='.$f_ID.'&lang='.LANG.'">'.$f_NAME.'</a>');
	$row->AddInputField("NAME", array("size"=>20));
	
	$row->AddInputField("PHONE", array("size"=>20));
	$row->AddInputField("BILLING", array("size"=>40));

	if ($f_TSZH_ID > 0) {
		$arTszh = CTszh::GetByID($f_TSZH_ID);
	  	$row->AddViewField('TSZH_ID', '[<a href="tszh_edit.php?ID=' . $f_TSZH_ID . '&lang=' . LANGUAGE_ID . '" title="' . GetMessage("AL_VIEW_TSZH") . '">' . $f_TSZH_ID . '</a>] ' . $arTszh["NAME"]);
	} else {
	  	$row->AddViewField('TSZH_ID', '�');
	}
	
	$row->AddViewField('EXECUTOR', $f_EXECUTOR != 'N' ? GetMessage("TSZH_YES") : GetMessage("TSZH_NO"));

	$USER_FIELD_MANAGER->AddUserFields($UF_ENTITY, $arRes, $row);

	// ���������� ����������� ����
	$arActions = Array();

	// �������������� ��������
	$arActions[] = array(
		"ICON"		=> "edit",
		"DEFAULT"	=> true,
		"TEXT"		=> GetMessage("TSZH_GROUP_EDIT_TITLE"),
		"ACTION"	=> $lAdmin->ActionRedirect("tszh_contractors_edit.php?ID=".$f_ID.'&lang='.LANG)
	);

	// �������� ��������
	if ($POST_RIGHT>="W")
		$arActions[] = array(
			"ICON"		=> "delete",
			"TEXT"		=> GetMessage("TSZH_GROUP_DELETE_TITLE"),
			"ACTION"	=> "if(confirm('".GetMessage('TSZH_GROUP_DELETE_CONFIRM')."')) ".$lAdmin->ActionDoGroup($f_ID, "delete")
		);

	// ������� �����������
	$arActions[] = array("SEPARATOR"=>true);

	// ���� ��������� ������� - �����������, �������� �����.
	if(is_set($arActions[count($arActions)-1], "SEPARATOR"))
		unset($arActions[count($arActions)-1]);

	// �������� ����������� ���� � ������
	$row->AddActions($arActions);

}

// ������ �������
$lAdmin->AddFooter(
	array(
		array("title"=>GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value"=>$rsData->SelectedRowsCount()), // ���-�� ���������
		array("counter"=>true, "title"=>GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value"=>"0"), // ������� ��������� ���������
	)
);

// ��������� ��������
$lAdmin->AddGroupActionTable(Array(
	"delete"=>GetMessage("MAIN_ADMIN_LIST_DELETE"), // ������� ��������� ��������
//	"activate"=>GetMessage("MAIN_ADMIN_LIST_ACTIVATE"), // ������������ ��������� ��������
//	"deactivate"=>GetMessage("MAIN_ADMIN_LIST_DEACTIVATE"), // �������������� ��������� ��������
));

// ******************************************************************** //
//                ���������������� ����                                 //
// ******************************************************************** //

// ���������� ���� �� ������ ������ - ���������� ������
$aContext = array(
	array(
		"TEXT"=>GetMessage("TSZH_ADD_ITEM"),
		"LINK"=>"tszh_contractors_edit.php?lang=".LANG,
		"TITLE"=>GetMessage("TSZH_ADD_ITEM_TITLE"),
		"ICON"=>"btn_new",
	),
);

// � ��������� ��� � ������
$lAdmin->AddAdminContextMenu($aContext);

// ******************************************************************** //
//                �����                                                 //
// ******************************************************************** //

if (($arID = $lAdmin->GroupAction()) && $POST_RIGHT>="W" && isset($messageOK) && $_REQUEST['action'] == "delete")
{
	$message = new CAdminMessage(array("MESSAGE" => $messageOK, "TYPE" => "OK"), false);
	echo $message->Show();
}

// �������������� �����
$lAdmin->CheckListMode();

$APPLICATION->SetTitle(GetMessage("TSZH_CONTRACTORS"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

echo $demoNotice;

if (isset($message) && isset($_REQUEST["edit_form_del"]))
{
	$message = new CAdminMessage(array("MESSAGE" => $messageOK, "TYPE" => "OK"), false);
	echo $message->Show();
}

// ******************************************************************** //
//                ����� �������                                         //
// ******************************************************************** //

$arFilterItems = Array();
foreach ($arHeaders as $arField)
{
	$arFilterItems[] = $arField['content'];
}

$USER_FIELD_MANAGER->AddFindFields($UF_ENTITY, $arFilterItems);
$oFilter = new CAdminFilter(
	$sTableID."_filter",
	$arFilterItems
);

?>
<form name="form_filter" method="get" action="<?echo $APPLICATION->GetCurPage();?>">
	<?
	$oFilter->Begin();

	TszhShowShowAdminFilter($arHeaders);

	$USER_FIELD_MANAGER->AdminListShowFilter($UF_ENTITY);
	
	$oFilter->Buttons(array("table_id" => $sTableID, "url" => $APPLICATION->GetCurPage(), "form" => "form_filter"));
	$oFilter->End();
	?>
</form>
<?

// ������� ������� ������ ���������
$lAdmin->DisplayList();

//require($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog_admin_js.php");

require ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
?>