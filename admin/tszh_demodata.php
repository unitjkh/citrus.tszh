<?

use Bitrix\Main\Application;
use Bitrix\Main\Localization\Loc;
use Citrus\Tszh\Demodata;

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php"); // ������ ����� ������

ob_start();
if (!CTszhFunctionalityController::CheckEdition())
{
	$c = ob_get_contents();
	ob_end_clean();
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");
	echo $c;
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");

	return;
}
$demoNotice = ob_get_contents();
ob_end_clean();

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszh/include.php"); // ������������� ������
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/citrus.tszh/prolog.php"); // ������ ������

// ������� ����� ������� �������� ������������ �� ������
$POST_RIGHT = $APPLICATION->GetGroupRight("citrus.tszh");

// ���� ��� ���� - �������� � ����� ����������� � ���������� �� ������
if ($POST_RIGHT < "U")
{
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));
}

$arErrors = array();
$arMessages = array();

$APPLICATION->SetTitle(GetMessage("TSZH_DEMODATA"));
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

echo $demoNotice;

$arTszhList = array();
$rsTszh = CTszh::GetList(array('ID' => 'ASC'), array(), false, false, array('ID', 'NAME'));
while ($arTszh = $rsTszh->Fetch())
{
	$arTszhList[$arTszh['ID']] = $arTszh['NAME'];
}

if (empty($arTszhList))
{
	\CAdminMessage::ShowMessage(Array(
		"DETAILS" => GetMessage("TSZH_ERROR_MESSAGE_DETAILS"),
		"TYPE" => 'ERROR',
		"MESSAGE" => GetMessage("TSZH_NOT_FOUND"),
		"HTML" => true,
	));
	require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
}

$request_id = isset($_REQUEST['id']) ? (int)$_REQUEST['id'] : LocalRedirect($APPLICATION->GetCurPageParam('id=' . array_shift(array_keys($arTszhList))));

$errorMessage = '';
$app = Application::getInstance();
$request = $app->getContext()->getRequest();
if ($request->getRequestMethod() == 'GET'
    && ($request->get('action') == 'download_demodata'
        || $request->get('action') == 'delete_demodata')
    && $request_id > 0
    && check_bitrix_sessid())
{
	$arErrors = array();
	$demo = new Demodata();
	switch ($request->get('action'))
	{
		case 'download_demodata':
			try
			{
				$demo->installForTszh($request_id);
				$demo->setStatus(array('download'));
			}
			catch (Exception $e)
			{
				$arErrors[] = $e->getMessage();
			}
			break;

		case 'delete_demodata':
			try
			{
				$demo->deleteDemoData($request_id);
				$demo->setStatus(array('delete'));
			}
			catch (Exception $e)
			{
				$arErrors[] = $e->getMessage();
			}
			break;
	}

	$strRedirect = $APPLICATION->GetCurPageParam('', array('action', 'sessid'), false);

	if (!empty($arErrors))
	{
		$errorMessage = implode('<br/>', $arErrors);
	}
	else
	{
		LocalRedirect($strRedirect);
	}
}

if (strlen($errorMessage) > 0)
{
	\CAdminMessage::ShowMessage($errorMessage);
}

$demo = new Demodata();
$demo->showStatusMessage();
try
{
	$arDemoData = $demo->getDemoData($request_id);
}
catch (\Exception $e)
{
	\CAdminMessage::ShowMessage($e->getMessage());
}
?>
	<div class="adm-detail-content-wrap" style="border-top: 1px solid #ced7d8">
		<div class="adm-detail-content" id="edit1">
			<div class="adm-detail-content-item-block">
				<table class="adm-detail-content-table edit-table">
					<tbody>
					<tr>
						<td width="200px">
							<span style="font-size: 18px"><?=GetMessage("TSZH_OBJECT")?></span>
						</td>
						<td>
							<select name="tszh_id" onchange="window.location='tszh_demodata.php?lang=<?=LANG?>&id='+this[this.selectedIndex].value;">
								<?php
								foreach ($arTszhList as $id => $value)
								{
									echo '<option' . ($id == $request_id ? ' selected ' : ' ') . 'value="' . $id . '">[' . $id . '] ' . $value . '</option>';
								}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<?php
							if (empty($arDemoData))
							{
								echo BeginNote();
								echo $demo->getDownloadNoteBlock();
								echo $demo->getDownloadButtonHtml();
								echo EndNote();
							}
							else
							{
								$demo->showDeleteNoteBlock($arDemoData);
							}
							?>
						</td>
					</tr>
					</tbody>
				</table>

			</div>
		</div>
		<div class="adm-detail-content-btns-wrap" id="tabControl_buttons_div" style="left: 0px;">
			<div class="adm-detail-content-btns">
			</div>
		</div>
	</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php"); ?>