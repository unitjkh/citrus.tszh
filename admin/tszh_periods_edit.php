<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");

ob_start();
if (!CTszhFunctionalityController::CheckEdition())
{
	$c = ob_get_contents();
	ob_end_clean();
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
	echo $c;
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
	return;
}
$demoNotice = ob_get_contents();
ob_end_clean();

$modulePermissions = $APPLICATION->GetGroupRight("citrus.tszh");
if ($modulePermissions<="R")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

require_once($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/citrus.tszh/include.php");
IncludeModuleLangFile(__FILE__);

$strRedirect = BX_ROOT."/admin/tszh_periods_edit.php?lang=".LANG;
$strRedirectList = BX_ROOT."/admin/tszh_periods.php?lang=".LANG;

ClearVars();
$errorMessage = "";
$bVarsFromForm = false;
$message = null;

$ID = IntVal($ID);

// �������� ���� ��� �������
$arTszhFilter = $arTszhRight = $arListFilter = array();
if (CTszhFunctionalityController::isPortal() && !$USER->IsAdmin())
{
	global $USER;
	$arGroups = $USER->GetUserGroupArray();

	$rsPerms = \Vdgb\PortalTszh\PermsTable::getList(
		array(
			"filter" => array("@GROUP_ID" => $arGroups)
		)
	);

	while($arPerms = $rsPerms->fetch())
	{
		if($arPerms["PERMS"] >= "R")
		{
			$arTszhRight[$arPerms["TSZH_ID"]] = $arPerms["PERMS"];
		}
	}

	$arTszhFilter["@ID"] = $arListFilter["@TSZH_ID"] = array_keys($arTszhRight);
}

$aTabs = array(
		array("DIV" => "edit1", "TAB" => GetMessage("TPE_TAB1"), "ICON" => "citrus.tszh", "TITLE" => GetMessage("TPE_TAB1_TITLE"))
	);

$tabControl = new CAdminForm("periodEdit", $aTabs);

if ($REQUEST_METHOD=="POST" && strlen($Update)>0 && $modulePermissions>="W" && check_bitrix_sessid())
{
	if ($modulePermissions < "W")
		$errorMessage .= GetMessage("TPE_NO_PERMISSIONS") . ".<br />";

	if ($ID > 0)
	{
		if (!($arOldPeriod = CTszhPeriod::GetByID($ID)))
			$errorMessage .= str_replace("#ID#", $ID, GetMessage("TPE_ERROR_NOT_FOUND")) . ".<br />";
	}
	if (!$DB->IsDate($DATE, CSite::GetDateFormat('SHORT'))) {
		$errorMessage .= GetMessage("TPE_ERROR_WRONG_DATE") . ".<br />";
	} else {
		$DATE = $DB->FormatDate($DATE, CSite::GetDateFormat('SHORT'), 'YYYY-MM-DD');
	}

	if (strlen($errorMessage) <= 0)
	{

		$DATE = htmlspecialcharsbx($DATE);

		$arFields = Array(
			"DATE" => $DATE,
			"TSZH_ID" => $TSZH_ID,
			"ACTIVE" => $ACTIVE == "Y" ? "Y" : "N",
			"ONLY_DEBT" => $ONLY_DEBT == "Y" ? "Y" : "N",
		);

		if ($ID > 0) {
			$bResult = CTszhPeriod::Update($ID, $arFields);
		} else {
			$ID = CTszhPeriod::Add($arFields);
			$bResult = $ID > 0;
		}
		if (!$bResult)
		{
			if ($ex = $APPLICATION->GetException())
				$errorMessage .= GetMessage("TPE_ERROR_SAVE") . ': ' . $ex->GetString().".<br />";
			else
				$errorMessage .= GetMessage("TPE_ERROR_SAVE") . ".<br />";
		}
	}

	if (strlen($errorMessage) <= 0)
	{
		if (strlen($apply) <= 0)
			LocalRedirect($strRedirectList . GetFilterParams("filter_", false));
		else
			LocalRedirect($strRedirect."&ID=".$ID."&".$tabControl->ActiveTabParam());
	}
	else
	{
		$bVarsFromForm = true;
	}
}

//require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/citrus.tszh/prolog.php");

if ($ID > 0)
	$APPLICATION->SetTitle(str_replace('#ID#', $ID, GetMessage("TPE_PAGE_TITLE_EDIT")));
else
	$APPLICATION->SetTitle(GetMessage("TPE_PAGE_TITLE_ADD"));

require_once ($DOCUMENT_ROOT.BX_ROOT."/modules/main/include/prolog_admin_after.php");

echo $demoNotice;

$dbPeriod = CTszhPeriod::GetList(
	Array(),
	Array("ID" => $ID),
	false,
	Array("nTopCount" => 1),
	Array("*")
);

if (!$arPeriod = $dbPeriod->ExtractFields("str_"))
{
	if ($modulePermissions < "W")
		$errorMessage .= GetMessage("TPE_NO_PERMISSIONS") . ".<br />";
	$ID = 0;
}

if ($bVarsFromForm) {
	$DB->InitTableVarsForEdit("b_tszh_periods", "", "str_");
}


if(strlen($errorMessage)>0)
	echo CAdminMessage::ShowMessage(Array("DETAILS"=>$errorMessage, "TYPE"=>"ERROR", "MESSAGE"=> GetMessage("TPE_ERRORS_HAPPENED"), "HTML"=>true));


/**
 *   CAdminForm()
**/

$aMenu = array(
		array(
				"TEXT" => GetMessage("TPE_M_PERIODS_LIST"),
				"LINK" => "/bitrix/admin/tszh_periods.php?lang=".LANG.GetFilterParams("filter_"),
				"ICON"	=> "btn_list",
				"TITLE" => GetMessage("TPE_M_PERIODS_LIST_TITLE"),
			)
	);

if ($ID > 0 && $modulePermissions >= "W")
{
	$aMenu[] = array("SEPARATOR" => "Y");

	$aMenu[] = array(
			"TEXT" => GetMessage("TPE_M_PERIODS_ADD"),
			"LINK" => "/bitrix/admin/tszh_periods_edit.php?lang=".LANG.GetFilterParams("filter_"),
			"ICON"	=> "btn_new",
			"TITLE" => GetMessage("TPE_M_PERIODS_ADD_TITLE"),
		);

	if ($modulePermissions >= "W")
	{
		$aMenu[] = array(
				"TEXT" => GetMessage("TPE_M_PERIODS_DELETE"),
				"LINK" => "javascript:if(confirm('" . GetMessage("TPE_M_PERIODS_DELETE_CONFIRM") . "')) window.location='/bitrix/admin/tszh_periods_edit.php?ID=".$ID."&action=delete&lang=".LANG."&".bitrix_sessid_get()."#tb';",
				"WARNING" => "Y",
				"ICON"	=> "btn_delete"
			);
	}

}
if(!empty($aMenu))
	$aMenu[] = array("SEPARATOR"=>"Y");
$link = DeleteParam(array("mode"));
$link = $GLOBALS["APPLICATION"]->GetCurPage()."?mode=settings".($link <> ""? "&".$link:"");
$aMenu[] = array(
	"TEXT"=> GetMessage("TPE_M_SETTINGS"),
	"TITLE"=> GetMessage("TPE_M_SETTINGS_TITLE"),
	"LINK"=>"javascript:".$tabControl->GetName().".ShowSettings('".htmlspecialcharsbx(CUtil::addslashes($link))."')",
	"ICON"=>"btn_settings",
);

$context = new CAdminContextMenu($aMenu);
$context->Show();


// ============ FORM PROLOG ==================
$tabControl->BeginEpilogContent();
echo GetFilterHiddens("filter_");
?>
<input type="hidden" name="Update" value="Y" />
<input type="hidden" name="lang" value="<?echo LANG ?>" />
<input type="hidden" name="ID" value="<?echo $ID ?>" />
<?
echo bitrix_sessid_post();
$tabControl->EndEpilogContent();
// ===========================================

$tabControl->Begin(array(
	"FORM_ACTION" => $APPLICATION->GetCurPage(),
	"FORM_ATTRIBUTES" => "method=\"POST\" enctype=\"multipart/form-data\"",
));
$tabControl->BeginNextFormTab();

	if ($str_ID > 0) {
		$tabControl->AddViewField("ID", "ID", $str_ID);
		$tabControl->AddViewField("TIMESTAMP_X", GetMessage("TPE_F_TIMESTAMP_X"), $DB->FormatDate($str_TIMESTAMP_X, 'YYYY-MM-DD HH:MI:SS', CSite::GetDateFormat('FULL')));
	}

	$tabControl->AddCheckBoxField("ACTIVE", GetMessage("TPE_F_ACTIVE"), false, "Y", ($str_ACTIVE!="N"));
	$tabControl->AddCheckBoxField("ONLY_DEBT", GetMessage("TPE_F_ONLY_DEBT"), false, "Y", ($str_ONLY_DEBT=="Y"));
	
	$html = BeginNote();
	$html .= GetMessage("TPE_ONLY_DEBT_DESCR");
	$html .= EndNote();
	$tabControl->AddViewField("ONLY_DEBT_DESCR", "", $html);
	
	
	$rsTszh = CTszh::GetList(Array("NAME" => "ASC"), $arTszhFilter);
	$arTszhList = Array();
	while ($arTszh = $rsTszh->Fetch()) {
		$arTszhList[$arTszh['ID']] = $arTszh["NAME"];
	}
	$tabControl->AddDropDownField("TSZH_ID", GetMessage("TPE_F_TSZH_ID"), true, $arTszhList, $str_TSZH_ID);

	$tabControl->AddCalendarField("DATE", GetMessage("TPE_F_DATE"), $DB->FormatDate($str_DATE, 'YYYY-MM-DD', CSitE::GetDateFormat('SHORT')), true);


	$tabControl->Buttons(Array(
		"disabled" => ($modulePermissions < "W"),
		"back_url" => "/bitrix/admin/tszh_periods.php?lang=".LANG.GetFilterParams("filter_")
	));


	$tabControl->Show();

	$tabControl->ShowWarnings($tabControl->GetName(), $message);

if(!defined('BX_PUBLIC_MODE') || BX_PUBLIC_MODE != 1):?>
<?echo BeginNote();?>
<span class="required">*</span> <?echo GetMessage("REQUIRED_FIELDS")?>
<?echo EndNote(); ?>
<?endif;?>
<?require_once ($DOCUMENT_ROOT.BX_ROOT."/modules/main/include/epilog_admin.php");?>