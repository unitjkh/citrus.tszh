<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/citrus.tszh/include.php");
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/citrus.tszh/prolog.php");

IncludeModuleLangFile(__FILE__);

ob_start();
if (!CTszhFunctionalityController::CheckEdition())
{
	$c = ob_get_contents();
	ob_end_clean();
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");
	echo $c;
	require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");
	return;
}
$demoNotice = ob_get_contents();
ob_end_clean();


// ������� ����� ������� �������� ������������ �� ������
$moduleRights = $APPLICATION->GetGroupRight("citrus.tszh");
// ���� ��� ���� - �������� � ����� ����������� � ���������� �� ������
if ($moduleRights <= "D")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$UF_ENTITY = "TSZH_METER";

$sTableID = "tbl_tszh_meters";
$oSort = new CAdminSorting($sTableID, "ID", "desc");
$lAdmin = new CAdminList($sTableID, $oSort);

// �������� ���� ��� �������
$arTszhFilter = $arTszhRight = $arListFilter = array();
if (CTszhFunctionalityController::isPortal() && !$USER->IsAdmin())
{
	global $USER;
	$arGroups = $USER->GetUserGroupArray();

	$rsPerms = \Vdgb\PortalTszh\PermsTable::getList(
		array(
			"filter" => array("@GROUP_ID" => $arGroups)
		)
	);

	while($arPerms = $rsPerms->fetch())
	{
		if($arPerms["PERMS"] >= "R")
		{
			$arTszhRight[$arPerms["TSZH_ID"]] = $arPerms["PERMS"];
		}
	}

	$arTszhFilter["@ID"] = $arListFilter["@TSZH_ID"] = array_keys($arTszhRight);
}

$arTszhList = Array();
$rsTszh = CTszh::GetList(Array("NAME" => "ASC"), $arTszhFilter, false, false, Array("ID", "NAME"));
while ($arTszh = $rsTszh->Fetch())
	$arTszhList[$arTszh['ID']] = $arTszh['NAME'];

$arHeaders = array(
	array("id" => "ID",					"content" => GetMessage("CITRUS_TSZH_F_ID"),				"sort" => "id",				"align" => "right",		"default" => true,		"type" => "int"),
	array("id" => "ACTIVE",				"content" => GetMessage("CITRUS_TSZH_F_ACTIVE"),			"sort" => "active",			"align" => "center",	"default" => true,		"type" => "checkbox"),
	array("id" => "SORT",				"content" => GetMessage("CITRUS_TSZH_F_SORT"),				"sort" => "sort",			"align" => "right",		"default" => true,		"type" => "int"),
	array("id" => "NAME",				"content" => GetMessage("CITRUS_TSZH_F_METER_NAME"),		"sort" => "name",			"align" => "left",		"default" => true,		"type" => "string"),
	array("id" => "NUM",				"content" => GetMessage("CITRUS_TSZH_F_NUM"),				"sort" => "num",			"align" => "left",		"default" => true,		"type" => "string"),
	array("id" => "XML_ID",				"content" => GetMessage("CITRUS_TSZH_F_XML_ID"),			"sort" => "xml_id",			"align" => "left",		"default" => false,		"type" => "string"),
	array("id" => "HOUSE_METER",		"content" => GetMessage("CITRUS_TSZH_F_HOUSE_METER"),		"sort" => "house_meter",	"align" => "center",	"default" => true,		"type" => "checkbox"),
	array("id" => "ACCOUNT_ID",			"content" => GetMessage("CITRUS_TSZH_F_ACCOUNT_ID"),		"sort" => "account_id",		"align" => "left",		"default" => true,		"type" => "account"),
	array("id" => "SERVICE_ID",			"content" => GetMessage("CITRUS_TSZH_F_SERVICE_ID"),		"sort" => "service_id",		"align" => "left",		"default" => true,		"type" => "service"),
	array("id" => "SERVICE_NAME",		"content" => GetMessage("CITRUS_TSZH_F_SERVICE_NAME"),		"sort" => "service_name",	"align" => "left",		"default" => false,		"type" => "string"),
	array("id" => "VALUES_COUNT",		"content" => GetMessage("CITRUS_TSZH_F_VALUES_COUNT"),		"sort" => "values_count",	"align" => "right",		"default" => true,		"type" => "int"),
	array("id" => "DEC_PLACES",			"content" => GetMessage("CITRUS_TSZH_F_DEC_PLACES"),		"sort" => "dec_places",		"align" => "right",		"default" => false,		"type" => "int"),
	array("id" => "CAPACITY",			"content" => GetMessage("CITRUS_TSZH_F_CAPACITY"),			"sort" => "capacity",		"align" => "right",		"default" => false,		"type" => "int"),

	array("id" => "TSZH_ID",			"content" => GetMessage("CITRUS_TSZH_F_TSZH_ID"),			"sort" => "tszh_id",		"align" => "left",		"default" => false,		"type" => "list",	"items" => $arTszhList),
	array("id" => "ACCOUNT_XML_ID",		"content" => GetMessage("CITRUS_TSZH_F_ACCOUNT_XML_ID"),	"sort" => "account_xml_id",	"align" => "left",		"default" => false,		"type" => "string"),
	array("id" => "USER_ID",			"content" => GetMessage("CITRUS_TSZH_F_USER_ID"),			"sort" => "user_id",		"align" => "left",		"default" => false,		"type" => "user"),
	array("id" => "VERIFICATION_DATE",			"content" => GetMessage("CITRUS_TSZH_F_VERIFICATION_DATE"),			"sort" => "verification_date",		"align" => "left",		"default" => false,		"type" => "string"),
);

// ******************************************************************** //
//                           ������                                     //
// ******************************************************************** //

// *********************** CheckFilter ******************************** //
// �������� �������� ������� ��� �������� ������� � ��������� �������
function CheckFilter()
{
	global $FilterArr, $lAdmin;
	foreach ($FilterArr as $value) {
		global $$value;
	}

	return true;
}
// *********************** /CheckFilter ******************************* //

// ������ �������� �������
$FilterArr = TszhAdminFilterParams($arHeaders);

// �������������� ������
$lAdmin->InitFilter($FilterArr);

$USER_FIELD_MANAGER->AdminListAddFilterFields($UF_ENTITY, $FilterArr);

// ���� ��� �������� ������� ���������, ���������� ���
$arFilter = array();
if (CheckFilter() && !is_set($_REQUEST, 'del_filter'))
{
	// �������� ������ ���������� ��� ������� �� ������ �������� �������
	TszhAdminMakeFilter($arFilter, $arHeaders);
	$USER_FIELD_MANAGER->AdminListAddFilter($UF_ENTITY, $arFilter);
}

// ******************************************************************** //
//                ��������� �������� ��������                           //
// ******************************************************************** //

if (isset($_REQUEST["edit_form_del"]) && IntVal($_REQUEST["edit_form_del"]) == 1 && check_bitrix_sessid())
{
	if (isset($_REQUEST["ID"]) && IntVal($_REQUEST["ID"]) > 0)
	{
		@set_time_limit(0);

		$arItem = CTszhMeter::GetByID($_REQUEST["ID"]);
		if (is_array($arItem))
		{
			if(CTszhMeter::Delete($arItem['ID']))
			{
				$messageOK = GetMessage("TSZH_DELETE_OK");
			}
			else
			{
				$lAdmin->AddGroupError(GetMessage("TSZH_DELETE_ERROR"), $ID);
			}
		}
		else
		{
			$lAdmin->AddGroupError(GetMessage("TSZH_DELETE_ERROR").". ".GetMessage("TSZH_DELETE_ITEM_NOT_FOUND", Array("#ID#" => $ID)), $ID);
		}
	}
}


// ******************************************************************** //
//                ��������� �������� ��� ���������� ������              //
// ******************************************************************** //

// ���������� ����������������� ���������
if ($lAdmin->EditAction() && $moduleRights>="W" && check_bitrix_sessid())
{

	// ������� �� ������ ���������� ���������
	foreach($FIELDS as $ID=>$arFields)
	{
		if(!$lAdmin->IsUpdated($ID))
			continue;

		// �������� ��������� ������� ��������
		$ID = IntVal($ID);

		$arItem= CTszhMeter::GetByID($ID);
		if (is_array($arItem))
		{
			$USER_FIELD_MANAGER->AdminListPrepareFields($UF_ENTITY, $arFields);
			if (!CTszhMeter::Update($ID, $arFields))
			{
				$strError = '';
				if ($ex = $APPLICATION->GetException())
					$strError = $ex->GetMessage();
				$lAdmin->AddGroupError(GetMessage("TSZH_GROUP_SAVE_ERROR")." ".$strError, $ID);
			}
		}
		else
		{
			$lAdmin->AddGroupError(GetMessage("TSZH_GROUP_SAVE_ERROR")." ".GetMessage("TSZH_GROUP_SAVE_ERROR_ITEM_NOT_FOUND"), $ID);
		}
	}
}

// ��������� ��������� � ��������� ��������
if (($arID = $lAdmin->GroupAction()) && $moduleRights>="W" && check_bitrix_sessid())
{
	// ���� ������� "��� ���� ���������"
	if($_REQUEST['action_target']=='selected')
	{
		$rsItems = CTszhMeter::GetList(Array(), $arFilter, false, false, Array("ID"));
		$arID = Array();
		while ($arItem = $rsItems->Fetch())
			$arID[] = $arItem['ID'];
	}

	@set_time_limit(0);

	// ������� �� ������ ���������
	foreach($arID as $ID)
	{
		if(strlen($ID)<=0)
			continue;

		$ID = IntVal($ID);

		// ��� ������� �������� �������� ��������� ��������
		switch($_REQUEST['action'])
		{
			// ��������
			case "delete":
				$arItem = CTszhMeter::GetByID($ID);
				if (is_array($arItem))
				{
					if (CTszhMeter::Delete($ID))
						$messageOK = GetMessage("qroup_del_ok");
					else
						$lAdmin->AddGroupError(GetMessage("TSZH_DELETE_ERROR"), $ID);
				}
				else
					$lAdmin->AddGroupError(GetMessage("TSZH_DELETE_ERROR")." ".GetMessage("TSZH_DELETE_ITEM_NOT_FOUND"), $ID);
				break;
			case "activate":
				@set_time_limit(0);
				$DB->StartTransaction();
				if (!CTszhMeter::Update($ID, Array("ACTIVE" => "Y"))) {
					$DB->Rollback();
					$lAdmin->AddGroupError(GetMessage("TSZH_GROUP_SAVE_ERROR"), $ID);
				}
				$DB->Commit();
				break;
			case "deactivate":
				@set_time_limit(0);
				$DB->StartTransaction();
				if (!CTszhMeter::Update($ID, Array("ACTIVE" => "N"))) {
					$DB->Rollback();
					$lAdmin->AddGroupError(GetMessage("TSZH_GROUP_SAVE_ERROR"), $ID);
				}
				$DB->Commit();
				break;

		}
	}
}

// ******************************************************************** //
//                ������� ��������� ������                              //
// ******************************************************************** //

$USER_FIELD_MANAGER->AdminListAddHeaders($UF_ENTITY, $arHeaders);
$lAdmin->AddHeaders($arHeaders);

// visible columns
$arVisibleColumns = $_visibleColumns = $lAdmin->GetVisibleHeaderColumns();

// additional fields required for formatting
if (in_array('ACCOUNT_ID', $arVisibleColumns))
	$arVisibleColumns[] = 'HOUSE_METER';
if (in_array('SERVICE_ID', $arVisibleColumns))
	$arVisibleColumns[] = 'SERVICE_NAME';
if (in_array('SERVICE_ID', $arVisibleColumns))
	$arVisibleColumns[] = 'SERVICE_NAME';
$arVisibleColumns = array_unique(array_merge($arVisibleColumns, Array('ID')));

// proper page navigation using limit in SQL
$arNavParams = CAdminResult::GetNavParams(CAdminResult::GetNavSize($sTableID));
$arNav = Array(
	'nPageSize' => $arNavParams['SIZEN'],
	'iNumPage' => $arNavParams['PAGEN'],
	'bShowAll' => $arNavParams['SHOW_ALL'],
);

$arSort = Array();
$arSortBy = explode('|', $by);
foreach ($arSortBy as $sBy) {
	$arSort[$sBy] = $order;
}
// ������� ������ �������
$rsData = CTszhMeter::GetList(
	$arSort,
	array_merge($arFilter, $arListFilter),
	false,
	$_REQUEST["mode"] == "excel" ? false : $arNav,
	$arVisibleColumns
);
$rsData = new CTszhMultiValuesAdminResult($rsData, $sTableID, CTszhMeter::$arGetListFields);

// ���������� CDBResult �������������� ������������ ���������.
$rsData->NavStart();

// �������� ����� ������������� ������� � �������� ������ $lAdmin
$lAdmin->NavText($rsData->GetNavPrint(GetMessage("TMH_PAGE_TITLE")));

// ******************************************************************** //
//                ���������� ������ � ������                            //
// ******************************************************************** //

// ������ ������������ ��������� ��� ���������������� �����
$template_path = $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/interface/navigation.php";
// ����� ������������� �������
$lAdmin->NavText($rsData->GetNavPrint(GetMessage("CITRUS_TSZH_METERS"), true, "", $template_path, array('action', 'sessid')));

while($arRes = $rsData->NavNext(true, "f_"))
{
	// ������� ������. ��������� - ��������� ������ CAdminListRow
	$row =& $lAdmin->AddRow($f_ID, $arRes);

	// ����� �������� ����������� �������� ��� ��������� � �������������� ������
	foreach ($arHeaders as $arH)
	{
		if ($arH["type"] == 'checkbox')
			$row->AddViewField($arH["id"], $arRes[$arH["id"]] == 'Y' ? GetMessage("TSZH_YES") : GetMessage("TSZH_NO"));
	}

	if (in_array("NAME", $arVisibleColumns) && !in_array("ID", $_visibleColumns))
		$row->AddViewField('NAME', '[<a href="/bitrix/admin/tszh_meters_edit.php?lang=' . LANG . '&amp;ID=' . $f_ID . '" title="' . GetMessage("CITRUS_TSZH_EDIT_METER") . '">' . $f_ID . '</a>] ' . $f_NAME);

	if (in_array('ACCOUNT_ID', $arVisibleColumns) && !empty($arRes["ACCOUNT_ID"]))
	{
		$arAccountIDView = $names = array();
		if (!is_array($arRes["ACCOUNT_ID"]))
			$arRes["ACCOUNT_ID"] = array($arRes["ACCOUNT_ID"]);
		$dbAccounts = CTszhAccount::GetList(array("ID" => "ASC"), array("ID" => $arRes["ACCOUNT_ID"]));
		while ($account = $dbAccounts->GetNext())
		{
			$name = CTszhAccount::GetFullName($account);
			$names[$account["ID"]] = strlen($name) ? $name : '(' . $account["XML_ID"] . ')';
		}

		foreach ($names as $accountID => $accountName)
			$arAccountIDView[] = '[<a href="/bitrix/admin/tszh_account_edit.php?lang=' . LANG . '&amp;ID=' . $accountID . '" title="' . GetMessage("TSZH_F_EDIT_ACCOUNT") . '">' . $accountID . '</a>] ' . $accountName;

		$row->AddViewField('ACCOUNT_ID', implode("<br>", $arAccountIDView));
	}
	else
		$row->AddViewField('ACCOUNT_ID', '');

	if (in_array('SERVICE_ID', $arVisibleColumns))
		if ($f_SERVICE_ID > 0)
			$row->AddViewField('SERVICE_ID', '[<a href="/bitrix/admin/tszh_services_edit.php?lang=' . LANG . '&amp;ID=' . $f_SERVICE_ID . '" title="' . GetMessage("TSZH_F_EDIT_ACCOUNT") . '">' . $f_SERVICE_ID . '</a>] ' . $f_SERVICE_NAME);
		else
			$row->AddViewField('SERVICE_ID', $f_SERVICE_NAME);

	if (in_array('ACCOUNT_XML_ID', $arVisibleColumns))
		$row->AddViewField('ACCOUNT_XML_ID', implode("<br>", $arRes["ACCOUNT_XML_ID"]));

	if (in_array('ACCOUNT_ADDRESS_FULL', $arVisibleColumns))
		$row->AddViewField('ACCOUNT_ADDRESS_FULL', implode("<br>", $arAddresses));

	if (in_array("USER_ID", $arVisibleColumns) && is_array($arRes["USER_ID"]))
	{
		$result = array();
		foreach ($arRes["USER_ID"] as $userId)
			$result[] = '[<a href="/bitrix/admin/user_edit.php?lang=' . LANG . '&amp;ID=' . $userId. '">' . $userId . '</a>] ' . CTszhAccount::GetFullName($userId);
		$row->AddViewField('USER_ID', implode(", ", $result));
	}

	if (in_array('TSZH_ID', $arVisibleColumns))
	{
		$arTszh = array_unique($arRes['TSZH_ID']);
		$arTszhIDView = array();
		foreach ($arTszh as $tszhID)
			$arTszhIDView[] = '[<a href="/bitrix/admin/tszh_edit.php?lang=' . LANG . '&amp;ID=' . $tszhID . '">' . $tszhID . '</a>] ' . (array_key_exists($tszhID, $arTszhList) ? $arTszhList[$tszhID] : '');

		$row->AddViewField('TSZH_ID', implode("<br>", $arTszhIDView));
	}

	$USER_FIELD_MANAGER->AddUserFields($UF_ENTITY, $arRes, $row);

	// ���������� ����������� ����
	$arActions = Array();

	// �������������� ��������
	$arActions[] = array(
		"ICON"		=> "edit",
		"DEFAULT"	=> true,
		"TEXT"		=> GetMessage("TSZH_GROUP_EDIT_TITLE"),
		"ACTION"	=> $lAdmin->ActionRedirect("tszh_meters_edit.php?ID=".$f_ID.'&lang='.LANG)
	);

    // �������� ���������
    $arActions[] = array(
        "ICON"		=> "view",
        "DEFAULT"	=> true,
        "TEXT"		=> GetMessage("CITRUS_TSZH_METER_VALUES"),
        "ACTION"	=> $lAdmin->ActionRedirect("tszh_meters_history.php?find_meter_id=".$f_ID.'&lang='.LANG)
    );

    // �������� ��������
	if ($moduleRights>="W")
		$arActions[] = array(
			"ICON"		=> "delete",
			"TEXT"		=> GetMessage("TSZH_GROUP_DELETE_TITLE"),
			"ACTION"	=> "if(confirm('".GetMessage('TSZH_GROUP_DELETE_CONFIRM')."')) ".$lAdmin->ActionDoGroup($f_ID, "delete")
		);

	// ������� �����������
	$arActions[] = array("SEPARATOR"=>true);

	// ���� ��������� ������� - �����������, �������� �����.
	if(is_set($arActions[count($arActions)-1], "SEPARATOR"))
		unset($arActions[count($arActions)-1]);

	// �������� ����������� ���� � ������
	$row->AddActions($arActions);
}

// ������ �������
$lAdmin->AddFooter(
	array(
		array("title"=>GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value"=>$rsData->SelectedRowsCount()), // ���-�� ���������
		array("counter"=>true, "title"=>GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value"=>"0"), // ������� ��������� ���������
	)
);

// ��������� ��������
$lAdmin->AddGroupActionTable(Array(
	"delete"=>GetMessage("MAIN_ADMIN_LIST_DELETE"), // ������� ��������� ��������
	"activate"=>GetMessage("MAIN_ADMIN_LIST_ACTIVATE"), // ������������ ��������� ��������
	"deactivate"=>GetMessage("MAIN_ADMIN_LIST_DEACTIVATE"), // �������������� ��������� ��������
));

// ******************************************************************** //
//                ���������������� ����                                 //
// ******************************************************************** //

// ���������� ���� �� ������ ������ - ���������� ��������
$aContext = array(
	array(
		"TEXT"=> GetMessage("CITRUS_TSZH_ADD_METER"),
		"LINK"=>"tszh_meters_edit.php?lang=".LANG,
		"TITLE"=> GetMessage("CITRUS_TSZH_ADD_METER_TITLE"),
		"ICON"=>"btn_new",
	),
);

// � ��������� ��� � ������
$lAdmin->AddAdminContextMenu($aContext);

// ******************************************************************** //
//                �����                                                 //
// ******************************************************************** //

if (($arID = $lAdmin->GroupAction()) && $moduleRights>="W" && isset($messageOK) && $_REQUEST['action'] == "delete")
{
	$message = new CAdminMessage(array("MESSAGE" => $messageOK, "TYPE" => "OK"), false);
	echo $message->Show();
}

// �������������� �����
$lAdmin->CheckListMode();

$APPLICATION->SetTitle(GetMessage("CITRUS_TSZH_METERS"));

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php");

echo $demoNotice;

if (isset($message) && isset($_REQUEST["edit_form_del"]))
{
	$message = new CAdminMessage(array("MESSAGE" => $messageOK, "TYPE" => "OK"), false);
	echo $message->Show();
}

// ******************************************************************** //
//                ����� �������                                         //
// ******************************************************************** //

$arFilterItems = Array();
foreach ($arHeaders as $arField)
{
	if ($arField['id'] != "ID")
		$arFilterItems[] = $arField['content'];
}

$USER_FIELD_MANAGER->AddFindFields($UF_ENTITY, $arFilterItems);
$oFilter = new CAdminFilter(
	$sTableID."_filter",
	$arFilterItems
);

?>
	<form name="form_filter" method="get" action="<?echo $APPLICATION->GetCurPage();?>">
		<?
		$oFilter->Begin();

		TszhShowShowAdminFilter($arHeaders);

		$USER_FIELD_MANAGER->AdminListShowFilter($UF_ENTITY);

		$oFilter->Buttons(array("table_id" => $sTableID, "url" => $APPLICATION->GetCurPage(), "form" => "form_filter"));
		$oFilter->End();
		?>
	</form>
<?

// ������� ������� ������ ���������
$lAdmin->DisplayList();


require ($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");

?>