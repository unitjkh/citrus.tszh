<?
IncludeModuleLangFile(__FILE__);

class CTszhPublicHelper
{
	/**
	 * ���������� ������������ �������� "1�:���� ���"
	 *
	 * @return string
	 */
	public static function getProductName()
	{
		if (!class_exists("citrus_tszh"))
		{
			$oModule = new citrus_tszh();
		}

		return getMessage("CITRUS_TSZH_MODULE_NAME");
	}

	/**
	 * �������������� ������ � ������ �����
	 *
	 * @param $cost
	 * @param bool $bHTML
	 *
	 * @return mixed
	 */
	public static function FormatCurrency($cost, $bHTML = true)
	{
		if (defined('CITRUS_TSZH_CURRENCY_FORMAT') && strlen(CITRUS_TSZH_CURRENCY_FORMAT) > 0)
		{
			return $bHTML ? sprintf(CITRUS_TSZH_CURRENCY_FORMAT, $cost) : strip_tags(sprintf(CITRUS_TSZH_CURRENCY_FORMAT, $cost));
		}

		$cost = round($cost, 2);

		$bNegative = $cost < 0;
		$rub = floor(abs($cost));
		$kop = round((abs($cost) - $rub) * 100);

		$kop = str_pad($kop, 2, '0', STR_PAD_LEFT);

		return str_replace(
			Array("#A#", "#B#"), Array(
			$bNegative ? '-' . $rub : $rub,
			$kop,
		), $bHTML ? GetMessage("TSZH_COST_FORMAT") : strip_tags(GetMessage("TSZH_COST_FORMAT"))
		);
	}

	/**
	 * ������������� �����
	 * �-�, pluralForm(1, array("�����", "�����", "������"));
	 *
	 * @param $n
	 * @param $ar
	 *
	 * @return mixed
	 */
	function pluralForm($n, $ar)
	{
		$n = abs($n) % 100;
		$n1 = $n % 10;
		if ($n > 10 && $n < 20)
		{
			return $ar[3];
		}
		if ($n1 > 1 && $n1 < 5)
		{
			return $ar[0];
		}
		if ($n1 == 1)
		{
			return $ar[1];
		}

		return $ar[2];
	}

	/**
	 * �������������� ��� ��� �������� email
	 *
	 * @param $userId
	 * @param $email
	 *
	 * @return string
	 */
	public static function calcConfirmEmailHash($userId, $email)
	{
		return md5(md5($userId) . "|" . $email);
	}

	/**
	 * ���������� ������ ����� ���������� ����� (�������� ����������)
	 *
	 * @param string $Id Id �����
	 *
	 * @return array
	 */
	public static function getSite($Id)
	{
		static $arSites = array();

		if (!isset($arSites[$Id]))
		{
			$arSites[$Id] = CSite::GetByID($Id)->fetch();
		}

		return $arSites[$Id];
	}

	/**
	 * ���������� URL ����� (�������� ����������)
	 *
	 * @param string $siteId Id �����
	 *
	 * @return string
	 */
	public static function getSiteUrl($siteId)
	{
		global $APPLICATION;

		static $arSiteUrls = array();

		if (!isset($arSiteUrls[$siteId]))
		{
			$arSite = self::getSite($siteId);
			$serverName = is_array($arSite) && !empty($arSite["SERVER_NAME"]) ? $arSite["SERVER_NAME"] : (COption::GetOptionString("main", "server_name", $_SERVER["HTTP_HOST"]));
			$siteDir = is_array($arSite) && !empty($arSite["DIR"]) ? $arSite["DIR"] : "/";
			$arSiteUrls[$siteId] = ($APPLICATION->IsHTTPS() ? "https://" : "http://") . $serverName . $siteDir;
		}

		return $arSiteUrls[$siteId];
	}

	/**
	 * ���������� � ������������ �� ������ ���������� URL ����������� � ������������ � �����
	 *
	 * @param array $arTszh ������ ����� ���
	 * @param string $orgUrl ���������� URL ���
	 * @param string $orgSiteName ���������� ������������ ����� ���
	 *
	 * @return
	 */
	public static function getOrgUrlAndSiteName(
		array $arTszh, &$orgUrl,
		&$orgSiteName
	) {
		$siteUrl = self::getSiteUrl($arTszh["SITE_ID"]);

		if (CModule::IncludeModule("vdgb.portaljkh"))
		{
			$orgUrl = $siteUrl . "org/{$arTszh["CODE"]}/";
			$orgSiteName = $arTszh["NAME"];
		}
		else
		{
			$arSite = self::getSite($arTszh["SITE_ID"]);
			$orgUrl = $siteUrl;
			$orgSiteName = $arSite["NAME"];
		}
	}

	/**
	 * ��� ���������� ��� ���������� � ������������ �� ������ ���������� URL ������� ������� ��������:
	 * URL ������� ��������, URL �������� ������, URL �������� ����� ��������� ���������, URL �������� ���������
	 *
	 * @param array $arTszh ������ ����� ���
	 * @param bool|string $personalUrl �� �����: ���� (bool) ������������� �������� URL ������� ��������. �� ������: ���� true, �� ���������� URL ������� ��������
	 * @param bool|string $paymentUrl �� �����: ���� (bool) ������������� �������� URL �������� ������ ������� ��������. �� ������: ���� true, �� ���������� URL �������� ������ ������� ��������
	 * @param bool|string $metersUrl �� �����: ���� (bool) ������������� �������� URL �������� ����� ��������� ��������� ������� ��������. �� ������: ���� true, �� ���������� URL �������� ����� ��������� ��������� ������� ��������
	 * @param bool|string $receiptUrl �� �����: ���� (bool) ������������� �������� URL �������� ��������� ������� ��������. �� ������: ���� true, �� ���������� URL �������� ��������� ������� ��������
	 *
	 * @return void
	 */
	public static function getPersonalUrls(
		array $arTszh, &$personalUrl = false,
		&$paymentUrl = false, &$metersUrl = false, &$receiptUrl = false
	) {
		if (!$personalUrl && !$paymentUrl && !$metersUrl && !$receiptUrl)
		{
			return;
		}

		$siteUrl = self::getSiteUrl($arTszh["SITE_ID"]);
		self::getOrgUrlAndSiteName($arTszh, $orgUrl, $orgSiteName);

		if ($personalUrl)
		{
			$personalPathOption = COption::GetOptionString(TSZH_MODULE_ID, "personal_path", "personal/", $arTszh["SITE_ID"]);
			if (CModule::IncludeModule("vdgb.portaljkh"))
			{
				if (method_exists("CCitrusPortalTszh", "setPaymentBase"))
				{
					CCitrusPortalTszh::setPaymentBase($arTszh);
				}
			}
			$personalUrl = $orgUrl . $personalPathOption;
		}

		if ($paymentUrl)
		{
			$paymentUrl = false;
			if (CModule::IncludeModule("citrus.tszhpayment"))
			{
				$paymentUrl = CTszhPaySystem::getPaymentPath($arTszh["SITE_ID"], false, true);
				if (strlen($paymentUrl))
				{
					$arSiteUrl = parse_url($siteUrl);
					$paymentUrl = $arSiteUrl["scheme"] . "://" . $arSiteUrl["host"] . $paymentUrl;
				}
			}
		}

		if ($metersUrl)
		{
			$metersPathOption = COption::GetOptionString(TSZH_MODULE_ID, "meters_path", "personal/meters/", $arTszh["SITE_ID"]);
			$metersUrl = $orgUrl . $metersPathOption;
		}

		if ($receiptUrl)
		{
			$receiptPathOption = COption::GetOptionString(TSZH_MODULE_ID, "receipt_path", "personal/receipt/?print=Y", $arTszh["SITE_ID"]);
			$receiptUrl = $orgUrl . $receiptPathOption;
		}
	}

	/**
	 * ��� ��������� ��� ���������� Id � ��� ��������� �� ��������
	 *
	 * @param array $arTszhs ������ ���, ��� ������� ���� ���������� ��������� (tszhId => arTszh)
	 *
	 * @return array|false ������ (tszhId => array("type" => iblockType, "id" => iblockId)), false � ������ ������
	 */
	public static function getTszhsNewsIBlocks(array $arTszhs = array())
	{
		if (!CModule::includeModule("iblock"))
		{
			return false;
		}

		if (empty($arTszhs))
		{
			// ������� ��� ���
			$rsTszhs = CTszh::getList(
				array("ID" => "ASC"), array(), false, false, array("ID", "SITE_ID")
			);
			while ($arTszh = $rsTszhs->getNext())
			{
				$arTszhs[$arTszh["ID"]] = $arTszh;
			}
			if (empty($arTszhs))
			{
				return array();
			}
		}

		$arResult = array();

		$isPortal = CModule::includeModule("vdgb.portaljkh");

		$arIBlockIds = array();
		foreach ($arTszhs as $tszhId => $arTszh)
		{
			$iblockId = false;
			$iblockType = false;

			if ($isPortal)
			{
				$iblockCode = "tszh_news_" . $arTszh["ID"];
				$iblockType = "tszhportal_" . $arTszh["ID"];
				$arIBlock = CIBlock::GetList(array(), array(
					"CODE" => $iblockCode,
					"TYPE" => $iblockType,
					"SITE_ID" => $arTszh["SITE_ID"],
				))->fetch();
				if (is_array($arIBlock) && !empty($arIBlock))
				{
					$iblockId = $arIBlock["ID"];
				}
			}
			else
			{
				$iblockId = intval(COption::GetOptionInt(TSZH_MODULE_ID, "news.iblock", 0, $arTszh["SITE_ID"]));
				if ($iblockId > 0)
				{
					$arIBlock = CIBlock::getById($iblockId)->fetch();
					if (is_array($arIBlock) && !empty($arIBlock))
					{
						$iblockId = intval($arIBlock["ID"]);
						$iblockType = $arIBlock["IBLOCK_TYPE_ID"];
					}
				}

				if ($iblockId <= 0)
				{
					$iblockCode = "news_" . $arTszh["SITE_ID"];
					$iblockType = "news";
					$arIBlock = CIBlock::GetList(array(), array(
						"XML_ID" => $iblockCode,
						"TYPE" => $iblockType,
						"SITE_ID" => $arTszh["SITE_ID"],
					))->fetch();
					if (is_array($arIBlock) && !empty($arIBlock))
					{
						$iblockId = $arIBlock["ID"];
					}
				}
			}

			$iblockId = intval($iblockId);
			if ($iblockId > 0)
			{
				$arResult[$tszhId] = array("id" => $iblockId, "type" => $iblockType);
			}
		}

		return $arResult;
	}

	/**
	 * ���������� ������ ��� ������ ����������
	 *
	 * @return array
	 */
	function MakeAnnouncementsFilter()
	{
		global $USER;
		$arFilter = Array();

		static $arFilterFields = Array(
			"district",
			"region",
			"city",
			"settlement",
			"street",
			"house",
		);

		if (is_object($USER) && $USER->IsAuthorized())
		{
			$arAccount = CTszhAccount::GetByUserID($USER->GetID());
			if (!is_array($arAccount))
			{
				// if user have no account, fill in account fields with empty strings
				$arAccount = Array();
				foreach ($arFilterFields as $field)
				{
					$arAccount[ToUpper($field)] = "";
				}
			}
			foreach ($arFilterFields as $field)
			{
				$arFilter[] = Array(
					"LOGIC" => "OR",
					"PROPERTY_{$field}" => false,
					"=PROPERTY_{$field}" => $arAccount[ToUpper($field)],
				);
			}
		}
		// if user is not authorized, make filter, that won't return any elements
		if (count($arFilter) <= 0)
		{
			$arFilter = Array("=ID" => "-1");
		}

		return $arFilter;
	}

	/**
	 * ShowPanel()
	 * ��������� ������ ������� ������� �� ���������������� ������
	 *
	 * @return void
	 */
	function ShowPanel()
	{
		if ($GLOBALS["USER"]->IsAdmin() && COption::GetOptionString("main", "wizard_solution", "", SITE_ID) == "site_tszh")
		{
			$GLOBALS["APPLICATION"]->AddPanelButton(array(
				"HREF" => "/bitrix/admin/wizard_install.php?lang=" . LANGUAGE_ID . "&wizardName=citrus:site_tszh&wizardSiteID=" . SITE_ID . "&" . bitrix_sessid_get(),
				"ID" => "site_tszh_wizard",
				"ICON" => "bx-panel-site-wizard-icon",
				"MAIN_SORT" => 2500,
				"TYPE" => "BIG",
				"SORT" => 10,
				"ALT" => GetMessage("TSZH_WIZ_BUTTON_DESCRIPTION"),
				"TEXT" => GetMessage("TSZH_WIZ_BUTTON_NAME"),
				"MENU" => array(),
			));
		}
	}

	/**
	 * CTszhPublicHelper::showFancyboxMessage()
	 * ������� � fancybox ��������� � �������� ����������
	 *
	 * @param string $header html ���������
	 * @param string $message html ���������
	 * @param string $type ��� ��������� (��������������): success - ��������� �� ������ �������� (������������ ������ ������), error - �� ������ (�������)
	 *
	 * @return void
	 */
	function showFancyboxMessage($header, $message, $type = false)
	{
		global $APPLICATION;

		CJSCore::Init(array("tszh_fancybox"));

		switch ($type)
		{
			case "success":
				$style = 'style="color: green;"';
				break;

			case "error":
				$style = 'style="color: red;"';
				break;

			default:
				$style = "";
		}

		$html = "
			<script>
				$(document).ready(function () {
					$.fancybox('<div style=\"max-width:600px\"><h2 align=\"center\">" . $header . "</h2><p {$style}>{$message}</p></div>', { 'title' : null });
				});
			</script>
		";
		$APPLICATION->AddHeadString($html, true);
	}

	/**
	 * CTszhPublicHelper::notifyConfirmAccount()
	 * ������� � fancybox ���������� � ������������� �������� � �������� �����
	 *
	 * @return void
	 */
	function notifyConfirmAccount()
    {
        if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')) {

            global $USER;

            $confirmUrl = $USER->getParam("citrus.tszh.notification.confirmAccount");
            if (strlen($confirmUrl) <= 0 || defined("ADMIN_SECTION")) {
                return;
            }

            self::showFancyboxMessage(getMessage("TSZH_NOTIFY_CONFIRM_ACC_MAIL_HEADER"), getMessage("TSZH_NOTIFY_CONFIRM_ACC_MAIL_BODY", array(
                "#URL#" => $confirmUrl,
            )));

            $USER->setParam("citrus.tszh.notification.confirmAccount", false);
        }
    }

	function notifyDummyEmailAndEmptyPhone()
    {
        if (!(isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')) {

            global $USER;

            // ���� ������ ���������
            if ($USER->getParam("citrus.tszh.notification.message") === false) {
                return;
            }

            if (defined("ADMIN_SECTION")) {
                return;
            }

            // ������������ �����������
            if (is_null($UserID = $USER->GetID())) {
                return;
            }

            //������� ������ ���������� �/�
            $cSettings = COption::GetOptionInt("citrus.tszh", "user_group_id", false);
            if ($cSettings > 0) {
                $arUserGroups = $USER->GetUserGroupArray();
                //�� �������� �/�, ������������� => ��������� �� �������
                if (!in_array($cSettings, $arUserGroups) || $USER->IsAdmin()) {
                    return;
                }
            }

            // �������� ������ ������������
            $arUser = CUser::GetById($UserID)->Fetch();

            //��������� ������
            $errors = '';

            // �������� �����
            if (
                CTszhSubscribe::isDummyMail($arUser["EMAIL"]) && (COption::GetOptionString("citrus.tszh", "notification_dummyMail", "N") == "Y")
            ) {
                $errors .= "<li>Email</li>";
            }

            // �������� ��������
            if (
                (COption::GetOptionString('citrus.tszh', 'input_phone_add', "N") == 'Y') && (COption::GetOptionString('citrus.tszh', 'input_phone_require', "N") == 'Y') && (COption::GetOptionString("citrus.tszh", "notification_emptyPhone", "N") == "Y") && (strlen($arUser["PERSONAL_PHONE"]) <= 0)
            ) {
                $errors .= '<li>' . getMessage("TSZH_NOTIFY_BODY_PHONE") . '</li>';
            }

            // ���� ������ ���, �� ��������� ����������
            if ($errors == '') {
                return;
            }

            // ��������� ������ �� �������
            $profileUrl = $USER->getParam('citrus.tszh.personal') . 'info/';

            //��������� ����� ������ � ������ ���������
            $message = '<p align="left">' . getMessage("TSZH_NOTIFY_BODY_TEXT") . ':</p><ul>' . $errors . '</ul></br>';
            $message .= '<p align="center"><a href="' . $profileUrl . '">' . getMessage("TSZH_NOTIFY_WRITE") . '</a></p>';

            //����� ���������
            self::showFancyboxMessage(getMessage("TSZH_NOTIFY_HEADER"), $message);

            //��������� ��������, ������ ��� �� �������
            $USER->setParam("citrus.tszh.notification.message", false);
        }
    }

	function OnAfterUserAuthorizeHandler($arUser)
	{
		global $USER;

		//�������� ����� ������� � ����������� �������
		if ($USER->IsAdmin())
		{
			CTszhFunctionalityController::CheckUpdatePeriod(true);
		}

		// �������� ��������� ����� ������������� � ���� ������ ��������
		// TODO �������� � ������ ��������� ��� ��� ����������� ���������� �������� (�������������) �� ������������ ����� � ��������� ������� ����� ����� ����������
		$personalURL = false;

		if (defined("ADMIN_SECTION"))
		{
			$arUser = $USER->GetByID($USER->GetID())->Fetch();
			$arSite = CSite::GetByID($arUser['LID'])->GetNext();
			$siteDir = $arSite["DIR"];
		}
		else
		{
			$siteDir = SITE_DIR;
		}

		if (CModule::IncludeModule("vdgb.operator") && class_exists("CTszhOperator") && CTszhOperator::IsOperator())
		{
			$personalURL = $siteDir . 'operator/';
		}

		if ($personalURL === false)
		{
			$rsUserGroups = $USER->GetUserGroupEx($USER->GetID());
			while ($arGroup = $rsUserGroups->Fetch())
			{
				if (in_array($arGroup['STRING_ID'], array(
					'TSZH_SUPPORT_ADMINISTRATORS',
					'TSZH_SUPPORT_CONTRACTORS',
				)))
				{
					$personalURL = $siteDir . "workplace/";
					break;
				}
			}
		}

		// �������� ������������ �� �������� � ��
		if (\COption::GetOptionString("citrus.tszh", "notification_confirmAccount", "") == "Y")
		{
			$cSettings = \COption::GetOptionInt("citrus.tszh", "user_group_id", false);
			if ($cSettings > 0)
			{
				$arUserGroups = $USER->GetUserGroupArray();
				if (in_array($cSettings, $arUserGroups) && !$USER->IsAdmin())
				{
					$arAccount = CTszhAccount::GetList(
						Array(), Array("USER_ID" => $USER->GetID()), false, Array(
						'nTopCount' => 1,
					), Array("ID", "TSZH_SITE", "TSZH_CODE")
					)->Fetch();
					if (!is_array($arAccount))
					{
						$personalURL = substr(SITE_DIR, 0, strlen(SITE_DIR) - 1) . COption::GetOptionString("citrus.tszh", "personal_path", "/personal/");
						$USER->setParam("citrus.tszh.notification.confirmAccount", "{$personalURL}confirm-account/");
					}
				}
			}
		}

		if ($personalURL === false && intval($USER->GetID()) > 0)
		{
			$arAccount = CTszhAccount::GetList(
				Array(), Array("USER_ID" => $USER->GetID()), false, Array(
				'nTopCount' => 1,
			), Array("ID", "TSZH_SITE", "TSZH_CODE")
			)->Fetch();
			if (is_array($arAccount))
			{
				$arSite = CSite::GetByID($arAccount["TSZH_SITE"])->Fetch();
				if (is_array($arSite))
				{
					if (CModule::IncludeModule("vdgb.portaljkh") && COption::GetOptionString('main', 'wizard_solution', false, $arAccount["TSZH_SITE"]) == 'citrus_portaljkh')
					{
						$personalURL = $arSite["DIR"] . 'org/' . $arAccount["TSZH_CODE"] . '/personal/';
					}
					else
					{
						if ($arAccount["TSZH_SITE"] != SITE_ID)
						{
							$arDomains = Array();
							if (is_string($arDomains))
							{
								$arDomains = explode("\n", $arDomains);
							}
							$personalURL = ((is_array($arDomains) && strlen(trim($arDomains[0])) > 0) ? 'http://' . $arDomains[0] . '' : '') . $arSite["DIR"];
						}
						else
						{
							$personalURL = SITE_DIR;
						}

						$personalURL .= "personal/";
					}
				}
			}
		}

		if ($personalURL !== false)
		{
			$USER->SetParam('citrus.tszh.personal', $personalURL);
			if (defined("ADMIN_SECTION") || defined("TSZH_CANCEL_REDIRECT"))
			{
				return;
			}

            if ((isset($_SERVER['HTTP_X_REQUESTED_WITH']) && !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'))
                return;
			LocalRedirect($personalURL);
		}

		$USER->setParam("citrus.tszh.notification.message", true);
	}

	// ������� ���������� ������� "OnAfterIndexAdd" � "OnBeforeIndexUpdate"
	function OnAfterIndexAddUpdate($ID, $arFields)
	{
		// �������� ��������
		// ����� �� �������� ��������� �������� � �������� ����������� ������ �������������� �������������

		$newsIBlockID = COption::GetOptionInt('citrus.tszh', "news.iblock", 0);

		if ($newsIBlockID > 0 && $arFields["MODULE_ID"] == "iblock" && CModule::IncludeModule("iblock") && $arFields["PARAM2"] == $newsIBlockID)
		{
			$rsProp = CIBlockElement::GetProperty($arFields["PARAM2"], $arFields["ITEM_ID"], Array(), Array(
				"CODE" => "only_for_authorized",
			));
			if ($arProp = $rsProp->Fetch())
			{

				static $arAllowedGroups = false;
				if (!is_array($arAllowedGroups))
				{
					$rsGroups = CGroup::GetList($by = "id", $order = "asc", Array(
						"ACTIVE" => "Y",
					));
					$arAllowedGroups = Array();
					while ($arGroup = $rsGroups->Fetch())
					{
						$arAllowedGroups[] = $arGroup['ID'];
					}
				}
				CSearch::ChangePermission($arFields["MODULE_ID"], $arAllowedGroups, $arFields["ITEM_ID"]);
			}
		}

		return $arFields;
	}

	// ������� ���������� ������� "OnAfterIBlockElementAdd"
	function OnAfterIBlockElementAddHandler(&$arFields)
	{
		global $APPLICATION;

		$questionsIBlockID = COption::GetOptionInt('citrus.tszh', "questions.iblock", 0);

		if ($questionsIBlockID > 0 && $arFields['IBLOCK_ID'] == $questionsIBlockID && $arFields['ID'] > 0)
		{

			$rsElement = CIBlockElement::GetList(
				Array(), Array(
				"IBLOCK_ID" => $arFields['IBLOCK_ID'],
				"ID" => $arFields['ID'],
			), $arGroupBy = false, $arNavStartParams = false, $arSelectFields = Array(
				"ID",
				"NAME",
				"IBLOCK_TYPE_ID",
				"IBLOCK_ID",
				"IBLOCK_SECTION_ID",
				"PROPERTY_author_address",
				"PROPERTY_author_phone",
				"PROPERTY_author_email",
				"PREVIEW_TEXT",
				"PREVIEW_TEXT_TYPE",
			)
			);
			if ($arElement = $rsElement->GetNext())
			{
				$arEvent = Array(
					"NAME" => $arElement['~NAME'],
					"ADDRESS" => $arElement['~PROPERTY_AUTHOR_ADDRESS_VALUE'],
					"PHONE" => $arElement['~PROPERTY_AUTHOR_PHONE_VALUE'],
					"MAIL" => $arElement['~PROPERTY_AUTHOR_EMAIL_VALUE'],
					"TEXT" => $arElement['PREVIEW_TEXT_TYPE'] != 'text' ? HTMLToTxt($arElement['~PREVIEW_TEXT']) : $arElement['~PREVIEW_TEXT'],
					"ELEMENT_ID" => IntVal($arElement['ID']),
					"IBLOCK_TYPE" => $arElement['IBLOCK_TYPE_ID'],
					"IBLOCK_ID" => IntVal($arElement["IBLOCK_ID"]),
					"SECTION_ID" => IntVal($arElement['IBLOCK_SECTION_ID']),
				);
				CEvent::Send("QUESTIONS_FORM", SITE_ID, $arEvent);
			}
		}
		// ������� �������
		elseif (CTszhSubscribe::checkSendNewsItem($arFields))
		{

		}
	}

	// ���������� ������� OnAfterIBlockElementUpdate
	function OnAfterIBlockElementUpdateHandler(&$arFields)
	{
		$questionsIBlockID = 0;
		// ��������� ������������ ���� � ����� "questions.iblock"
		$rsSites = CIBlock::GetSite($arFields['IBLOCK_ID']);
		while ($arSite = $rsSites->Fetch())
		{
			$questionsIBlockID = COption::GetOptionInt('citrus.tszh', "questions.iblock", 0, $arSite["SITE_ID"]);
			if ($questionsIBlockID > 0 && $arFields['IBLOCK_ID'] == $questionsIBlockID)
			{
				$siteID = $arSite["SITE_ID"];
				break;
			}
		}

		if ($questionsIBlockID > 0 && $arFields['IBLOCK_ID'] == $questionsIBlockID && $arFields['ID'] > 0)
		{
			$rsElement = CIBlockElement::GetByID($arFields['ID']);
			if ($obElement = $rsElement->GetNextElement())
			{
				$arElement = $obElement->GetFields();
				$arProperties = $obElement->GetProperties();

				$arEvent = Array(
					"NAME" => $arElement['NAME'],
					"ADDRESS" => $arProperties['author_address']['VALUE'],
					"PHONE" => $arProperties['author_phone']['VALUE'],
					"MAIL" => trim($arProperties['author_email']['VALUE']),
					"TEXT" => $arElement['PREVIEW_TEXT_TYPE'] != 'text' ? HTMLToTxt($arElement['PREVIEW_TEXT']) : $arElement['PREVIEW_TEXT'],
					"ANSWER" => trim($arElement['DETAIL_TEXT_TYPE'] != 'text' ? HTMLToTxt($arElement['DETAIL_TEXT']) : $arElement['DETAIL_TEXT']),
					"ANSWER_AUTHOR" => strlen($arProperties['answer_author']['VALUE']) > 0 ? $arProperties['answer_author']['VALUE'] : '�',
					"ELEMENT_ID" => IntVal($arElement['ID']),
					"IBLOCK_TYPE" => $arElement['IBLOCK_TYPE_ID'],
					"IBLOCK_ID" => IntVal($arElement["IBLOCK_ID"]),
					"SECTION_ID" => IntVal($arElement['IBLOCK_SECTION_ID']),
				);

				// ��������� �����������, � ������, ���� ��� �������� ����� � E-Mail ������ �������
				if (md5($arEvent['ANSWER']) != $arProperties['md5_answer_sent']['VALUE']
				    && strlen($arEvent['ANSWER']) > 0 && strlen($arEvent['MAIL']) > 0 && check_email($arEvent['MAIL'])
				)
				{
					CEvent::Send("QUESTIONS_ANSWER", $siteID, $arEvent);
					$updateProperties = array(
						'md5_answer_sent' => md5($arEvent['ANSWER']),
						"answer_date" => ConvertTimeStamp(false, "FULL"),
					);
					CIBlockElement::SetPropertyValuesEx($arElement['ID'], $arElement["IBLOCK_ID"], $updateProperties);
				}
			}
		}
		// ������� �������
		elseif (CTszhSubscribe::checkSendNewsItem($arFields))
		{

		}
	}

	/**
	 * � ��������� �����:
	 *   1) ������������ ������������� e-mail'� ������������
	 *   2) ������������ ������� ������������ �� ��������
	 *   3) ������� ��������� ����������.
	 *
	 * @return void
	 */
	function OnPrologHandler()
	{
		if (defined("ADMIN_SECTION"))
		{
			return;
		}

		if (isset($_GET["tszh_confirm_email"]) && $_GET["tszh_confirm_email"] == "y"
		    && isset($_GET["user_id"]) && ($userID = intval($_GET["user_id"])) > 0
		    && isset($_GET["hash"]) && strlen($_GET["hash"]) == 32
		)
		{
			// ������������� e-mail'� ������������
			try
			{
				$arUser = CUser::getByID($userID)->fetch();
				if (!is_array($arUser) || empty($arUser))
				{
					throw new Exception(getMessage("TSZH_ERROR_USER_NOT_FOUND", array(
						"#USER_ID#" => $userID,
					)));
				}

				if (self::calcConfirmEmailHash($userID, $arUser["EMAIL"]) != toLower($_GET["hash"]))
				{
					throw new Exception(getMessage("TSZH_CONFIRM_EMAIL_ERROR_INVALID_HASH"));
				}

				$user = new CUser;
				$arUpdateFields = Array(
					"UF_TSZH_EML_CNFRM_RQ" => false,
				);
				$res = $user->Update($userID, $arUpdateFields);
				if (!$res)
				{
					throw new Exception(getMessage("TSZH_CONFIRM_EMAIL_ERROR_UPDATE", array(
						"#ERROR#" => $user->LAST_ERROR,
					)));
				}

				$msg = getMessage("TSZH_CONFIRM_EMAIL_SUCCEEDED", array("#EMAIL#" => $arUser["EMAIL"]));
				$type = "success";
			}
			catch (Exception $e)
			{
				$msg = $e->getMessage();
				$type = "error";
			}

			self::showFancyboxMessage(getMessage("TSZH_CONFIRM_EMAIL_HEADER"), $msg, $type);

			return;
		}
		elseif (isset($_GET["tszh_unsubscribe"]) && $_GET["tszh_unsubscribe"] == "y"
		        && isset($_GET["user_id"]) && ($userID = intval($_GET["user_id"])) > 0
		        && isset($_GET["subscribe"]) && (strlen($subscribeCode = $_GET["subscribe"]))
		        && isset($_GET["hash"]) && strlen($_GET["hash"]) == 32
		        && CTszhFunctionalityController::checkSubscribe(false, $isSubscribeEdition) && $isSubscribeEdition
		)
		{
			// ������� �� ��������
			try
			{
				$arUser = CUser::getByID($userID)->fetch();
				if (!is_array($arUser) || empty($arUser))
				{
					throw new Exception(getMessage("TSZH_ERROR_USER_NOT_FOUND", array(
						"#USER_ID#" => $userID,
					)));
				}

				$className = CTszhSubscribe::getSubscribeClassName($subscribeCode);
				if (!CTszhSubscribe::checkSubscribeClass($className))
				{
					throw new Exception(getMessage("TSZH_UNSUBSCRIBE_ERROR_SUBSCRIBE_NOT_FOUND"));
				}

				if (CTszhSubscribe::calcUnsubscribeHash($userID, $subscribeCode) != toLower($_GET["hash"]))
				{
					throw new Exception(getMessage("TSZH_UNSUBSCRIBE_ERROR_INVALID_HASH"));
				}

				CTszhSubscribe::setSubscription($userID, array($subscribeCode => false));

				$msg = getMessage("TSZH_UNSUBSCRIBE_SUCCEEDED", array("#SUBSCRIBE#" => $className::getMessage("NAME")));
				$type = "success";
			}
			catch (Exception $e)
			{
				$msg = $e->getMessage();
				$type = "error";
			}

			self::showFancyboxMessage(getMessage("TSZH_UNSUBSCRIBE_HEADER"), $msg, $type);

			return;
		}

		self::notifyConfirmAccount();
		self::notifyDummyEmailAndEmptyPhone();
	}

	// 1. ��� ����� e-mail'� ������� ���������������� ���� UF_TSZH_EML_CNFRM_RQ
	// 2. �������� ������ e-mail �������� ������������
	function OnBeforeUserUpdateHandler(&$arFields)
	{
		global $USER;

		if (!is_object($USER))
		{
			return;
		}

		$USER->setParam("citrus.tszh.user.changedEmail", false);

		if (defined("ADMIN_SECTION") || !is_set($arFields, "EMAIL") || $arFields["ID"] != $USER->getID())
		{
			return;
		}

		if (!is_set($arFields, "UF_TSZH_EML_CNFRM_RQ") && !CTszhSubscribe::isDummyMail($arFields["EMAIL"]))
		{
			$arUser = CUser::getByID($arFields["ID"])->fetch();
			if (is_array($arUser) && !empty($arUser) && $arUser["EMAIL"] != $arFields["EMAIL"])
			{
				$arFields["UF_TSZH_EML_CNFRM_RQ"] = true;
			}
		}

		$arChangedEmail = array(
			"OLD_EMAIL" => $USER->getEmail(),
			"NEW_EMAIL" => false,
			"IS_EMAIL_CHANGED" => false,
			"IS_EMAIL_CORRECT" => false,
			"IS_NOTIFICATION_SENT" => false,
		);
		$USER->setParam("citrus.tszh.user.changedEmail", $arChangedEmail);
	}

	// ���� ������� ������������ ������ ���� e-mail, �� �������� �� ���� ������ � �������� ��� �������������
	function OnAfterUserUpdateHandler(&$arFields)
	{
		global $USER;

		if (!is_object($USER))
		{
			return;
		}

		$arChangedEmail = $USER->getParam("citrus.tszh.user.changedEmail");
		if (defined("ADMIN_SECTION") || !is_set($arFields, "EMAIL") || !$arFields["RESULT"] || $arFields["ID"] != $USER->getID() || !is_array($arChangedEmail))
		{
			$USER->setParam("citrus.tszh.user.changedEmail", false);

			return;
		}

		$arChangedEmail["NEW_EMAIL"] = $arFields["EMAIL"];
		$arChangedEmail["IS_EMAIL_CHANGED"] = $arChangedEmail["NEW_EMAIL"] != $arChangedEmail["OLD_EMAIL"];
		$arChangedEmail["IS_EMAIL_CORRECT"] = !CTszhSubscribe::isDummyMail($arChangedEmail["NEW_EMAIL"]);

		if ($arChangedEmail["IS_EMAIL_CHANGED"] && $arChangedEmail["IS_EMAIL_CORRECT"])
		{
			// �������� �� ����� e-mail ������ � �������� ��� �������������
			$arUser = CUser::getByID($arFields["ID"])->fetch();
			if (is_array($arUser) && !empty($arUser))
			{
				$arSendFields = array(
					"USER_ID" => $arUser["ID"],
					"USER_EMAIL" => $arUser["EMAIL"],
					"HASH" => self::calcConfirmEmailHash($arUser["ID"], $arUser["EMAIL"]),
				);
				$arChangedEmail["IS_NOTIFICATION_SENT"] = CEvent::SendImmediate("TSZH_CONFIRM_EMAIL", $arUser["LID"], $arSendFields);
			}
		}

		$USER->setParam("citrus.tszh.user.changedEmail", $arChangedEmail);
	}

	/**
	 * ��������� �������: ���������� ���� ���������������� �����.
	 * ������������ ��� ������� ������� ���ƻ
	 *
	 * @param $aGlobalMenu
	 * @param $aModuleMenu
	 */
	public static function OnBuildGlobalMenuHandler(&$aGlobalMenu, &$aModuleMenu)
	{
		foreach ($aModuleMenu as &$item)
		{
			if (array_key_exists('section', $item))
			{
				if (strpos($item["section"], "citrus.tszh") !== false
				    || strpos($item["section"], "vdgb.tszhvote") !== false
				    || strpos($item["section"], "vdgb.carscontrol") !== false
				    || strpos($item["section"], "vdgb.operator") !== false
				)
				{
					$item["text"] = str_replace(
						\Bitrix\Main\Localization\Loc::getMessage("CITRUS_TSZH_MODULE_MENU_SEARCH_TO_REPLACE"), strlen(COption::GetOptionString("citrus.tszh", "tszh_module_caption")) > 0 ? COption::GetOptionString("citrus.tszh", "tszh_module_caption") : \Bitrix\Main\Localization\Loc::getMessage("CITRUS_TSZH_MODULE_MENU_REPLACE_DEFAULT"), $item["text"]);
				}
			}
		}
	}

	/**
	 * ���������� ������ � ������ �������
	 *
	 * @param string $message ����� ������
	 * @param string $message ID ���� ������� (TSZH_EXCHANGE, TSZH_ACCOUNT_ADD, TSZH_ACCOUNT_UPDATE, TSZH_ACCOUNT_DELETE)
	 * @param string $severity ��������� (INFO, ERROR, WARNING, SECURITY, DEBUG)
	 * @param bool|string $itemId ������ �������
	 */
	public static function logMessage(
		$message, $audit_type, $severity = "INFO",
		$itemId = false
	) {
		global $DB;

		$logFields = Array(
			"SEVERITY" => $severity,
			"AUDIT_TYPE_ID" => $audit_type, // �������
			"MODULE_ID" => "citrus.tszh",
			"DESCRIPTION" => nl2br($message), // ���������
			"ITEM_ID" => $itemId ? $itemId : session_id(),
		);

		// ���� ��� ���� ������� ��������� � ������� ����� � ���� ������ �������� ����� ������
		$logExists = \CEventLog::GetList(array(), array(
			"REQUEST_URI" => preg_replace("/(&?sessid=[0-9a-z]+)/", "", $_SERVER["REQUEST_URI"]),
			"SEVERITY" => $logFields["SEVERITY"],
			"AUDIT_TYPE_ID" => $logFields["AUDIT_TYPE_ID"],
			"MODULE_ID" => $logFields["MODULE_ID"],
			"ITEM_ID" => $logFields["ITEM_ID"],
		))->Fetch();

		if ($logExists)
		{
			$updateFields = array("DESCRIPTION" => $logExists["DESCRIPTION"] . "<br>" . nl2br(trim($message)));
			$strUpdate = $DB->PrepareUpdate("b_event_log", $updateFields);
			$DB->Query("UPDATE b_event_log SET ${strUpdate} WHERE ID=${logExists["ID"]}");

			return $logExists["ID"];
		}
		else
		{
			return \CEventLog::Add($logFields);
		}
	}

	function OnEventLogGetAuditTypesHandler()
	{
		return array(
			'TSZH_EXCHANGE' => '[TSZH_EXCHANGE] ' . \Bitrix\Main\Localization\Loc::getMessage('CITRUS_TSZH_EXCHANGE_AUDIT_TYPE_TSZH_EXCHANGE'),
			'TSZH_ACCOUNT_ADD' => '[TSZH_ACCOUNT_ADD] ' . \Bitrix\Main\Localization\Loc::getMessage('CITRUS_TSZH_EXCHANGE_AUDIT_TYPE_TSZH_ACCOUNT_ADD'),
			'TSZH_ACCOUNT_UPDATE' => '[TSZH_ACCOUNT_ADD] ' . \Bitrix\Main\Localization\Loc::getMessage('CITRUS_TSZH_EXCHANGE_AUDIT_TYPE_TSZH_ACCOUNT_UPDATE'),
			'TSZH_ACCOUNT_DELETE' => '[TSZH_ACCOUNT_ADD] ' . \Bitrix\Main\Localization\Loc::getMessage('CITRUS_TSZH_EXCHANGE_AUDIT_TYPE_TSZH_ACCOUNT_DELETE'),
		);
	}

}
