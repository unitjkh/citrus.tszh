<?
$MESS["TSZH_COST_FORMAT"] = "#A# <span class=\"currency\">руб.</span> #B# <span class=\"currency\">коп.</span>";
$MESS["TSZH_WIZ_BUTTON_NAME"] = "Мастер#BR#настройки";
$MESS["TSZH_WIZ_BUTTON_DESCRIPTION"] = "Запустить мастер установки и настройки сайта ТСЖ";

$MESS["TSZH_NOTIFY_DUMMY_MAIL_HEADER"] = "В Вашем профиле не задан корректный e-mail";
$MESS["TSZH_NOTIFY_DUMMY_MAIL_BODY"] = "Пожалуйста, укажите в Вашем <a href=\"#URL#\">профиле</a> корректный e-mail.";

$MESS["TSZH_NOTIFY_CONFIRM_ACC_MAIL_HEADER"] = "Привязка к лицевому счету";
$MESS["TSZH_NOTIFY_CONFIRM_ACC_MAIL_BODY"] = "Для полноценной работы с личным кабинетом вам нужно подтвердить свое управление лицевым счетом, введя полученный пин-код на <a href=\"#URL#\">специальной странице</a> сайта.";

$MESS["TSZH_ERROR_USER_NOT_FOUND"] = "Пользователь с ID #USER_ID# не найден.";

$MESS["TSZH_CONFIRM_EMAIL_HEADER"] = "Подтверждение адреса e-mail";
$MESS["TSZH_CONFIRM_EMAIL_ERROR_INVALID_HASH"] = "Подтверждаемый e-mail отличается от установленного.";
$MESS["TSZH_CONFIRM_EMAIL_ERROR_UPDATE"] = "Ошибка при сохранении подтверждения: #ERROR#.";
$MESS["TSZH_CONFIRM_EMAIL_SUCCEEDED"] = "Ваш e-mail #EMAIL# успешно подтверждён.";

$MESS["TSZH_UNSUBSCRIBE_HEADER"] = "Отписка от рассылки";
$MESS["TSZH_UNSUBSCRIBE_ERROR_SUBSCRIBE_NOT_FOUND"] = "Отписываемая рассылка не найдена.";
$MESS["TSZH_UNSUBSCRIBE_ERROR_INVALID_HASH"] = "Отписываемая рассылка не соответствует указанной.";
$MESS["TSZH_UNSUBSCRIBE_SUCCEEDED"] = "Вы отписались от рассылки «#SUBSCRIBE#».";

$MESS["CITRUS_TSZH_MODULE_MENU_SEARCH_TO_REPLACE"] = "ТСЖ";
$MESS["CITRUS_TSZH_MODULE_MENU_REPLACE_DEFAULT"] = "ЖКХ";

$MESS["CITRUS_TSZH_EXCHANGE_AUDIT_TYPE_TSZH_EXCHANGE"] = "Обмен данными";
$MESS["CITRUS_TSZH_EXCHANGE_AUDIT_TYPE_TSZH_ACCOUNT_ADD"] = "Добавление лицевого счета";
$MESS["CITRUS_TSZH_EXCHANGE_AUDIT_TYPE_TSZH_ACCOUNT_UPDATE"] = "Изменение лицевого счета";
$MESS["CITRUS_TSZH_EXCHANGE_AUDIT_TYPE_TSZH_ACCOUNT_DELETE"] = "Удаление лицевого счета";


$MESS["TSZH_NOTIFY_BODY_PHONE"] = "Телефон";
$MESS["TSZH_NOTIFY_HEADER"] = "Данные профиля";
$MESS["TSZH_NOTIFY_BODY_TEXT"] = "Пожалуйста, заполните следующие поля";
$MESS["TSZH_NOTIFY_WRITE"] = "Заполнить";