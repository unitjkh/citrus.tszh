<?php
$MESS["ACCOUNTS_PERIOD_ENTITY_ID_FIELD"] = "ID";
$MESS["ACCOUNTS_PERIOD_ENTITY_ACCOUNT_ID_FIELD"] = "Номер лицевого счета";
$MESS["ACCOUNTS_PERIOD_ENTITY_TIMESTAMP_X_FIELD"] = "Время создания";
$MESS["ACCOUNTS_PERIOD_ENTITY_PERIOD_ID_FIELD"] = "Период";
$MESS["ACCOUNTS_PERIOD_ENTITY_TYPE_FIELD"] = "Тип квитанции";
$MESS["ACCOUNTS_PERIOD_ENTITY_BARCODE_FIELD"] = "Штрих-код";
$MESS["ACCOUNTS_PERIOD_ENTITY_DEBT_BEG_FIELD"] = "Задолженность на начало периода";
$MESS["ACCOUNTS_PERIOD_ENTITY_DEBT_END_FIELD"] = "Задолженность на конец периода";
$MESS["ACCOUNTS_PERIOD_ENTITY_DEBT_PREV_FIELD"] = "Задолженность за предыдущие периоды";
$MESS["ACCOUNTS_PERIOD_ENTITY_PREPAYMENT_FIELD"] = "Аванс на начало периода";
$MESS["ACCOUNTS_PERIOD_ENTITY_CREDIT_PAYED_FIELD"] = "Предоплата рассрочки (в числе внесенных оплат)	";
$MESS["ACCOUNTS_PERIOD_ENTITY_SUMM_TO_PAY_FIELD"] = "Итого к оплате";
$MESS["ACCOUNTS_PERIOD_ENTITY_SUM_PAYED_FIELD"] = "Оплачено";
$MESS["ACCOUNTS_PERIOD_ENTITY_LAST_PAYMENT_FIELD"] = "Дата последнего платежа";
$MESS["ACCOUNTS_PERIOD_ENTITY_DATE_SENT_FIELD"] = "Письмо с квитанцией отправлено";
?>