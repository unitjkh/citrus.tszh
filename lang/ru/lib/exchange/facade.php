<?
$MESS["CITRUS_TSZH_1C_ERROR_ORG_NOT_FOUND"] = "Организация с указанным ИНН #INN# не найдена на сайте.";
$MESS["CITRUS_TSZH_1C_ERROR_KPP"] = "КПП";
$MESS["CITRUS_TSZH_IMPORT_FINISH"] = "Загрузка успешно завершена";

// TODO убрать лишнее
$MESS["TSZH_1C_ERROR_FILENAME"] = "Указано не существующее имя файла";

$MESS["TSZH_1C_ERROR_SESSION_ID_CHANGE"] = "Включена смена идентификатора сессий. В файле подключения компонента обмена, до подключения пролога определите константу BX_SESSION_ID_CHANGE: define('BX_SESSION_ID_CHANGE', false);";
$MESS["TSZH_1C_ERROR_AUTHORIZE"] = "Ошибка авторизации. Не передан идентификатор сессии или сессия истекла";
$MESS["TSZH_1C_ERROR_LOGIN"] = "Ошибка авторизации. Пожалуйста проверьте логин и пароль";
$MESS["TSZH_1C_PERMISSION_DENIED"] = "Указанная учетная запись имеет недостаточно прав доступа для выполнения обмена";
$MESS["TSZH_1C_ERROR_MODULE"] = "Модуль «1С:Сайт ЖКХ» не установлен";
$MESS["TSZH_1C_ERROR_IBLOCK_MODULE"] = "Модуль «Информационные блоки» не установлен.";
$MESS["TSZH_1C_ERROR_TICKETS_MODULE"] = "Модуль «1С:Сайт ЖКХ. Аварийно-диспетчерская служба» не установлен.";
$MESS["TSZH_1C_ERROR_PAYMENT_MODULE"] = "Модуль «1С:Сайт ЖКХ. Оплата» не установлен.";
$MESS["TSZH_1C_ERROR_HTTP_READ"] = "Ошибка чтения HTTP данных.";
$MESS["TSZH_1C_ERROR_UNKNOWN_COMMAND"] = "Неизвестная команда.";
$MESS["TSZH_1C_NO_ORDERS_IN_IMPORT"] = "В CML не найдены заказы.";
$MESS["TSZH_1C_ERROR_DIRECTORY"] = "Ошибочный параметр - временный каталог.";
$MESS["TSZH_1C_ERROR_FILE_WRITE"] = "Ошибка записи в файл #FILE_NAME#.";
$MESS["TSZH_1C_ERROR_FILE_OPEN"] = "Ошибка открытия файла #FILE_NAME# для записи.";
$MESS["TSZH_1C_ERROR_INIT"] = "Ошибка инициализации временного каталога.";
$MESS["TSZH_1C_ZIP_ERROR"] = "Ошибка распаковки архива.";
$MESS["TSZH_1C_EMPTY_CML"] = "Файл для импорта пуст.";
$MESS["TSZH_1C_UNZIP_ERROR"] = "Распаковка на сайте невозможна. Отправьте незапакованный файл.";
$MESS["TSZH_1C_ERROR_ORG_NOT_FOUND"] = "Организация с указанным ИНН не найдена на сайте.";
$MESS["TSZH_1C_METER_EXPORT_ERROR"] = "Ошибка выгрузки показний счетчиков. ";
$MESS["TSZH_1C_ACCEXISTENCE_EXPORT_ERROR"] = "Ошибка выгрузки параметров доступа владельцев лицевых счетов. ";
$MESS["TSZH_1C_QUESTIONS_EXPORT_IBLOCK_ERROR"] = "Не найден инфоблок Вопросов-ответов";
$MESS["TSZH_1C_TICKET_EXPORT_INIT_ERROR"] = "Ошибка инициализации экспорта обращений";
$MESS["TSZH_1C_IMPORT_FILE_NOT_FOUND"] = "Ошибка импорта, указанный файл импорта не найден";
$MESS["TI_STEP1_DONE"] = "Временные таблицы удалены";
$MESS["TI_STEP2_ERROR"] = "Ошибка создания временных таблиц";
$MESS["TI_STEP2_DONE"] = "Временные таблицы созданы";
$MESS["TI_STEP3_ERROR"] = "Ошибка чтения файла во временные таблицы";
$MESS["TI_STEP3_PROGRESS"] = "Загрузка данных во временные таблицы: #PROGRESS#%";
$MESS["TI_STEP4_ERROR"] = "Ошибка индексации временных таблиц";
$MESS["TI_STEP4_DONE"] = "Временные таблицы проиндексированы";
$MESS["TI_STEP5_ERROR"] = "Ошибка загрузки периода";
$MESS["TI_STEP5_DONE"] = "Данные периода и организации загружены";
$MESS["TI_STEP6_DONE"] = "Лицевые счета загружены";
$MESS["TI_STEP6_PROGRESS"] = "Обработано лицевых счетов: #CNT# из #TOTAL#. Из них с ошибками: #ERR#";
$MESS["TI_STEP7_DONE"] = "Удаление/деактивация лицевых счетов и счетчиков завершена";
$MESS["TI_STEP7_PROGRESS"] = "Удалено/деактивировано лицевых счетов и счетчиков: #CNT#";
$MESS["TI_STEP8_DONE"] = "Загрузка завершена";
$MESS["TI_STEP8_PROGRESS"] = "Демонстрационные лицевые счета удалены";
$MESS["TI_ERROR_ALREADY_FINISHED"] = "Загрузка была завершена";
$MESS["BI_STEP5_DONE"] = "Справочник характеристик дома загружен";
$MESS["BI_STEP6_DONE"] = "Дома загружены";
$MESS["BI_STEP6_PROGRESS"] = "Загружено домов: #CNT# из #TOTAL#";
$MESS["BI_STEP7_DONE"] = "Загрузка завершена";
$MESS["BI_SHOWN_FIRST_ERRORS"] = "Показаны первые";
$MESS["BI_SHOWN_ERRORS1"] = "ошибка";
$MESS["BI_SHOWN_ERRORS2"] = "ошибки";
$MESS["BI_SHOWN_ERRORS5"] = "ошибок";
$MESS["BI_HOUSE_NUMBER"] = "дом №";
$MESS["BI_WRONG_HOUSE_PROPERTY_CODE"] = "Неверно задан код свойства '#NAME#'";
$MESS["BI_HOUSE_PROPERTY_UPDATE_ERROR"] = "Ошибка при изменении свойства #CODE# инфоблока домов: #ERROR_TEXT#";
$MESS["BI_HOUSE_PROPERTY_ADD_ERROR"] = "Ошибка при добавлении свойства #CODE# в инфоблок домов: #ERROR_TEXT#";
$MESS["BI_WRONG_XML_ID"] = "Неверно указан идентификатор дома (атрибут id)";
$MESS["BI_WRONG_HOUSE"] = "Неверно указан номер дома (атрибут house)";
$MESS["BI_WRONG_STREET"] = "Неверно указана улица дома (атрибут street)";
$MESS["BI_HOUSE_UPDATE_ERROR"] = "Ошибка при изменении дома с id = '#XML_ID#': #ERROR_TEXT#";
$MESS["BI_HOUSE_ADD_ERROR"] = "Ошибка при добавлении дома с id = '#XML_ID#': #ERROR_TEXT#";
$MESS["BI_STREET_ADD_ERROR"] = "Ошибка при добавлении раздела (улицы) #STREET# в инфоблок домов: #ERROR_TEXT#";
$MESS["BDI_STEP5_DONE"] = "Подготовительные действия выполнены";
$MESS["BDI_STEP6_DONE"] = "Документы загружены";
$MESS["BDI_STEP6_PROGRESS"] = "Загружено документов: #CNT# из #TOTAL#";
$MESS["BDI_STEP7_DONE"] = "Загрузка завершена";
$MESS["BDI_WRONG_BUILDING"] = "Неверно указан идентификатор дома (атрибут building)";
$MESS["BDI_HOUSE_NOT_FOUND"] = "Не найден дом документа";
$MESS["BDI_WRONG_TITLE"] = "Неверно указано название документа (атрибут title)";
$MESS["BDI_WRONG_FILENAME"] = "Неверно указано имя файлй документа (атрибут filename)";
$MESS["BDI_DOCUMENT_UPDATE_ERROR"] = "Ошибка при изменении документа '#XML_ID#': #ERROR_TEXT#";
$MESS["BDI_DOCUMENT_ADD_ERROR"] = "Ошибка при добавлении документа '#XML_ID#': #ERROR_TEXT#";

$MESS["CITRUS_TSZH_EXCHANGE_AUDIT_TYPE"] = "Обмен данными";
$MESS["CITRUS_TSZH_EXPORT_SESSION_FAILURE"] = "Не найдена сохранная сессия начала экспорта";
$MESS["CITRUS_TSZH_EXCHANGE_FILE_UPLOADED"] = "Загружен файл: #FILE_NAME#, #SIZE# байт";
$MESS["CITRUS_TSZH_EXCHANGE_FILE_EXPORTED"] = "Выгруженные данные сохранены в файл: ";
$MESS["CITRUS_TSZH_EXCHANGE_ERROR"] = "Произошла ошибка";
$MESS["CITRUS_TSZH_EXCHANGE_RESPONSE"] = "Отправлен ответ";
$MESS["CITRUS_TSZH_EXCHANGE_RESPONSE_FILE"] = "Ответ сохранен в файл: ";
$MESS["CITRUS_TSZH_EXCHANGE_FATAL_ERROR"] = "Произошла неизвестная ошибка при обработке #METHOD#-запроса.\nЛог ошибок: #LOG#";
$MESS["CITRUS_TSZH_EXCHANGE_WARNING"] = "Загрузка успешно завершена с предупреждениями:";
$MESS["CITRUS_TSZH_EXCHANGE_SUCCESS"] = "Загрузка успешно завершена";

$MESS["CITRUS_TSZH_EXCHANGE_WRONG_PARAM"] = "Не верно указан параметр #PARAM#";
$MESS["CITRUS_TSZH_EXCHANGE_WRONG_DATE_PARAM"] = "Не верно указан параметр #PARAM#: нужно указать дату или время вида '31.12.2015 23:00:00'";
