<?
$MESS["CITRUS_TSZH_ERROR_TSZH_CHARGE_NO_ACCOUNT_PERIOD_ID"] = "Не указано поле «Квитанция» (ACCOUNT_PERIOD_ID)";
$MESS["CITRUS_TSZH_ERROR_ACCOUNT_PERIOD_NOT_FOUND_COMPAT"] = "Указана не существующая квитанция (ACCOUNT_ID = #ACCOUNT_ID# и PERIOD_ID = #PERIOD_ID#). Пожалуйста обратитесь в службу технической поддержки решения 1С:Сайт ЖКХ.";
$MESS["CITRUS_TSZH_ERROR_ACCOUNT_PERIOD_DEPRECATED_USE"] = "Используется не поддерживаемый способ указания квитанции (через ACCOUNT_ID или PERIOD_ID). Пожалуйста обратитесь в службу технической поддержки решения 1С:Сайт ЖКХ.";