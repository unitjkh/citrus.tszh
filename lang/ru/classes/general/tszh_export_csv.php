<?
$MESS["TSZH_ERROR_EXPORT_NO_DATA"] = "Нет данных для экспорта.";
$MESS["TSZH_EXPORT_FIO"] = "ФИО";
$MESS["TSZH_EXPORT_ACCOUNT"] = "Лицевой счет";
$MESS["TSZH_EXPORT_ADDRESS"] = "Адрес";
$MESS["TSZH_EXPORT_TARIFF"] = "(тариф #N#)";
$MESS["TSZH_EXPORT_MNAME"] = "Название счетчика";
$MESS["TSZH_EXPORT_MCODE"] = "Код счетчика";
$MESS["TSZH_EXPORT_MVALUES"] = "Количество тарифов";
$MESS["TSZH_EXPORT_MDATE"] = "Дата";
$MESS["TSZH_EXPORT_MVAL1"] = "Показание(1)";
$MESS["TSZH_EXPORT_MVAL2"] = "Показание(2)";
$MESS["TSZH_EXPORT_MVAL3"] = "Показание(3)";
?>
