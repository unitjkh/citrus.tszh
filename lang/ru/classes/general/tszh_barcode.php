<?
$MESS["TSZH_ERROR_BARCODE_NO_ACCOUNT_PERIOD_ID"] = "Не указана квитанция для штрих-кода";
$MESS["TSZH_ERROR_BARCODE_WRONG_TYPE"] = "Тип штрих-кода указан не верно";
$MESS["CITRUS_TSZH_BARCODE_CODE128"] = "Code 128";
$MESS["CITRUS_TSZH_BARCODE_QR"] = "QR-код";
