<?
$MESS["TI_ERROR_NO_ORG_ELEMENT"] = "Ошибка в формате XML-файла. Элемент 'ORG' не найден.";
$MESS["TI_ERROR_MISSING_ORG_ATTRS"] = "Ошибка в формате XML-файла. У элемента 'ORG' отсутствуют атрибуты: #ATTRS#.";
$MESS["TI_ERROR_EMPTY_ORG_ATTRS"] = "Ошибка в формате XML-файла. У элемента 'ORG' не указаны значения атрибутов: #ATTRS#.";
$MESS["TI_ERROR_WRONG_ORG_FILEDATE"] = "Ошибка в формате даты атрибута 'filedate' элемента 'ORG'.";
$MESS["TI_ERROR_PERIOD_ADD_ERROR"] = "Ошибка при добавлении периода.";
$MESS["TSZH_IMPORT_USER_ADMIN_NOTES"] = "Пользователь импортирован из прикладного ПО

Пароль — #PASSWORD#
";
$MESS["TI_ERROR_ADD_ACCOUNT_PERIOD"] = "Ошибка при добавлении данных лицевого счета за период (л/счет - #ACCOUNT#)";
$MESS["TI_ERROR_UPDATE_USER"] = "Ошибка при обновлении пользователя ##ID#";
$MESS["TI_ERROR_ADD_UPDATE_USER"] = "Ошибка при добавлении или обновлении пользователя #LOGIN#";
$MESS["TI_ERROR_ADD_USER"] = "Ошибка при добавлении пользователя (лицевой счет - #ACCOUNT_ID#)";
$MESS["TI_ERROR_UPDATE_ACCOUNT"] = "Ошибка при обновлении лицевого счета";
$MESS["TI_ERROR_ADD_ACCOUNT"] = "Ошибка при добавлении лицевого счета #ACCOUNT_ID#";
$MESS["TI_ERROR_ADD_CHARGE"] = "Ошибка при создании начисления (лицевой счет #ACCOUNT_ID#)";
$MESS["TI_ERROR_ADD_CHARGE_NO_SERVICE"] = "Ошибка при создании начисления: услуга не найдена (лицевой счет #ACCOUNT_ID#)";
$MESS["TI_ERROR_CHARGE_BIND_HMETERS"] = "Ошибка при привязке общедомовых счётчиков к начислению (лицевой счет #ACCOUNT_ID#, услуга '#SERVICE_NAME#')";
$MESS["TI_ERROR_CHARGE_HMETER_BIND_ACCOUNT"] = "Ошибка при привязке общедомового счётчика начисления к лицевому счету (лицевой счет #ACCOUNT_ID#, услуга '#SERVICE_NAME#')";
$MESS["TI_ERROR_ADD_METER"] = "Ошибка при добавлении счетчика (лицевой счет #ACCOUNT_ID#)";
$MESS["TI_ERROR_UPDATE_METER"] = "Ошибка при обновлении счетчика (лицевой счет #ACCOUNT_ID#): #NAME#";
$MESS["TI_ERROR_ADD_HMETER"] = "Ошибка при добавлении общедомого счетчика";
$MESS["TI_ERROR_UPDATE_HMETER"] = "Ошибка при обновлении общедомого счетчика: #NAME#";
$MESS["TI_ERROR_UPDATE_METER_VALUE"] = "Ошибка при добавлении показания счетчика (код счетчика #METER_XML_ID#)";
$MESS["TI_WRONG_ACCOUNT_NUMBER"] = "Неверно указан номер лицевого счета (атрибут name_ls)";
$MESS["TI_WRONG_ACCOUNT_EXTERNAL_ID"] = "Не указан идентификатор (атрибут kod_ls)";
$MESS["TI_WRONG_ACCOUNT_LOGIN"] = "Неверно указан логин пользователя (атрибут #FIELD#): \"#LOGIN#\"";
$MESS["TI_WRONG_ACCOUNT_EMAIL"] = "Неверный E-Mail лицевого счета (#MAIL#)";
$MESS["TI_SHOWN_FIRST_ERRORS"] = "Показаны первые";
$MESS["TI_SHOWN_ERRORS1"] = "ошибка";
$MESS["TI_SHOWN_ERRORS2"] = "ошибки";
$MESS["TI_SHOWN_ERRORS5"] = "ошибок";
$MESS["TSZH_DOLG_REMAINS_SERVICE"] = "Перенос начальных остатков";
$MESS["TI_ERROR_HOUSE_METER"] = "Ошибка загрузки общедомовых приборов учета";
$MESS["TI_ERROR_UPDATE_CONTRACTOR"] = "Ошибка обновления поставщика услуг";
$MESS["TI_ERROR_ADD_CONTRACTOR"] = "Ошибка добавления поставщика услуг";
$MESS["TI_ERROR_ADD_ACCOUNT_CONTRACTOR"] = "Ошибка добавления поставщика по квитанции (лицевой счет #ACCOUNT_ID#)";
$MESS["TI_ERROR_ADD_CORRECTIONS"] = "Ошибка добавления корректировки";
$MESS["TI_ERROR_ADD_ACCOUNT_INSTALLMENT"] = "Ошибка добавления рассрочки платежа (лицевой счет #ACCOUNT_ID#)";
$MESS["TI_ERROR_CHARGE_HMETER_NOT_FOUND"] = "Указанный для начисления общедомовой счетчик не найден (hmeter)";
$MESS["TI_ERROR_WRONG_VERSION"] = "Загружаемый файл имеет новый формат, который не поддерживается в данной версии модуля.<br><br>Пожалуйста, установите все доступные обновления решения 1C: Сайт ЖКХ.<br>Сделать это можно на странице Настройки — Marketplace — Обновление решений.";
$MESS["TI_ERROR_WRONG_FILETYPE"] = "Ошибка в формате XML-файла. Атрибут filetype элемента 'ORG' имеет некорректное значение.";
$MESS["TI_ERROR_WRONG_INN"] = "Ошибка в формате XML-файла. Атрибут inn элемента 'ORG' имеет некорректное значение.";
$MESS["TI_ERROR_WRONG_VERSION_FILETYPE"] = "Ошибка в формате XML-файла в элементе ORG. Значение атрибута filetype не соответствует значению атрибута version.";
$MESS["TI_ERROR_TSZH_CREATE_REQ_SITE_ID"] = "Невозможно создать объект управления: не указан ID сайта.";
$MESS["TI_ERROR_TSZH_CREATE_REQ_NAME"] = "Невозможно создать объект управления: не указано наименование объекта управления.";
$MESS["TI_ERROR_TSZH_CREATE"] = "Ошибка при создании объекта управления.";
$MESS["TI_N_SIGN"] = "№";

$MESS["TI_ERROR_TSZH_WITH_INN_NOT_EXISTS"] = "Объекта управления с ИНН #INN# на сайте не существует.\nИмпорт возможен только после создания объекта управления с данным ИНН.";
$MESS["TI_WRONG_ACCOUNT_KOD_LS_NEW"] = "Указан некорректный атрибут kod_ls_new (лицевой счет: #ACCOUNT#)";
$MESS["TI_WRONG_CONTRACTOR_EXECUTOR"] = "Не верно указан атрибут executor у тега ORG->contractors->contractor.";
$MESS["TI_ERROR_ADD_BARCODE"] = "Ошибка добавления штрих-кода для лицевого счета #ACCOUNT_ID#";

$MESS["TI_ERROR_CHARGE_METER_NOT_FOUND"] = "Для услуги с кодом #SERVICE_KOD# (л/с №#ACCOUNT_ID#) указан некорректный код счетчика (#ATTR_NAME# = #ATTR_VALUE#)";
