<?
$MESS["Common-ERROR-PARAM-MISSING"] = "Не задано значение параметра «#PARAM#»";

$MESS["Debtors-NAME"] = "Уведомления о задолженности";
$MESS["Debtors-NOTE-ADMIN"] = "В указанный день месяца владельцам лицевых счетов, имеющим задолженность, высылаются письма с уведомлением о ней.";
$MESS["Debtors-DESCR-PUBLIC"] = "При наличии задолженности, превышающей определённый размер, высылается письмо с уведомлением о ней.";
$MESS["Debtors-SETTING-ACTIVE-NAME"] = "Активна";
$MESS["Debtors-SETTING-ACTIVE-NAME-WIZARD"] = "Рассылать уведомления должникам";
$MESS["Debtors-PARAM-DATE-NAME"] = "День месяца для рассылки";
$MESS["Debtors-PARAM-DATE-ERROR-RANGE"] = "Значение параметра «#PARAM#» должно быть в диапазоне от 1 до 31 включительно";
$MESS["Debtors-PARAM-MIN_SUM-NAME"] = "Минимальная сумма задолженности для отправки уведомления, руб.";
$MESS["Debtors-PARAM-MIN_SUM-ERROR-MIN"] = "Значение параметра «#PARAM#» должно быть не менее одной копейки (0.01)";
$MESS["Debtors-SETTING-LAST_EXEC-NAME"] = "Время последнего запуска";
$MESS["Debtors-SETTING-LAST_EXEC-NAME-WIZARD"] = "Время последнего запуска";
$MESS["Debtors-SETTING-NEXT_EXEC-NAME"] = "Время следующего запуска";
$MESS["Debtors-SETTING-NEXT_EXEC-NAME-WIZARD"] = "Время следующего запуска";