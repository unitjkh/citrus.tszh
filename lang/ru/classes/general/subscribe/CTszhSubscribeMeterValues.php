<?
$MESS["Common-ERROR-PARAM-MISSING"] = "Не задано значение параметра «#PARAM#»";

$MESS["MeterValues-NAME"] = "Напоминания о необходимости передачи показаний счётчиков";
$MESS["MeterValues-NOTE-ADMIN"] = "В указанный день месяца владельцам лицевых счетов отправляются уведомления с напоминанием о необходимости ввести показания счётчиков на сайте (если они не введены).";
$MESS["MeterValues-DESCR-PUBLIC"] = "При отсутствии показаний счётчиков высылается письмо с напоминанием о необходимости ввести их на сайте.";
$MESS["MeterValues-SETTING-ACTIVE-NAME"] = "Активна";
$MESS["MeterValues-SETTING-ACTIVE-NAME-WIZARD"] = "Рассылать уведомления о необходимости ввода показаний счётчиков";
$MESS["MeterValues-PARAM-DATE-NAME"] = "День месяца для рассылки";
$MESS["MeterValues-PARAM-DATE-ERROR-RANGE"] = "Значение параметра «#PARAM#» должно быть в диапазоне от 1 до 31 включительно";
$MESS["MeterValues-PARAM-LAST_PERIOD-NAME"] = "Последний период, по которому отправлены уведомления";
$MESS["MeterValues-SETTING-LAST_EXEC-NAME"] = "Время последнего запуска";
$MESS["MeterValues-SETTING-LAST_EXEC-NAME-WIZARD"] = "Время последнего запуска";
$MESS["MeterValues-SETTING-NEXT_EXEC-NAME"] = "Время следующего запуска";
$MESS["MeterValues-SETTING-NEXT_EXEC-NAME-WIZARD"] = "Время следующего запуска";