<?
$MESS["ORDER_DESCR_VALUE"] = "за услуги ЖКХ по л/с № #ACCOUNT_NUMBER# за #PREV_MONTH#";
$MESS["TYP_INFORMER_TITLE"] = "#TSZH_NAME#";
$MESS["TYP_ERROR_ACCOUNTPERIODID_AND_ACCOUNTID_NOT_PASSED"] = "Параметры accountPeriodID и accountID не заданы.";
$MESS["TYP_ERROR_ACCOUNT_PERIOD_NOT_FOUND"] = "Данные лицевого счета за период не найдены.";
$MESS["TYP_ERROR_ACCOUNT_NOT_FOUND"] = "Лицевой счет не найден.";
$MESS["TYP_ERROR_TSZH_NOT_FOUND"] = "Объект управления не найден.";
$MESS["TYP_ERROR_JS_EMPTY_FIELD"] = "Пожалуйста, заполните поле \"#FIELD#\".";
$MESS["TYP_ERROR_JS_MIN_SUM"] = "Минимальная сумма платежа: 1 руб.";
$MESS["TYP_ERROR_TSZH_MISSING_REQUIRED_FIELDS"] = "Не заполнены обязательные поля объекта управления. Пожалуйста обратитесь к администрации сайта.";
$MESS["TYP_FORM_HEADER"] = "Оплатить";
$MESS["TYP_FORM_ORDER_DESCR"] = "Назначение платежа";
$MESS["TYP_FORM_TO_PAY"] = "Сумма";
$MESS["TYP_FORM_BUTTON"] = "Оплатить";
$MESS["TYP_FORM_RUB_ABBR"] = "руб.";
$MESS["TYP_FORM_RECEIPT"] = "или <a href=\"#URL#\">распечатать квитанцию</a><br />для оплаты в банке";
$MESS["TYP_M_01"] = "январь";
$MESS["TYP_M_02"] = "февраль";
$MESS["TYP_M_03"] = "март";
$MESS["TYP_M_04"] = "апрель";
$MESS["TYP_M_05"] = "май";
$MESS["TYP_M_06"] = "июнь";
$MESS["TYP_M_07"] = "июль";
$MESS["TYP_M_08"] = "август";
$MESS["TYP_M_09"] = "сентябрь";
$MESS["TYP_M_10"] = "октябрь";
$MESS["TYP_M_11"] = "ноябрь";
$MESS["TYP_M_12"] = "декабрь";
?>