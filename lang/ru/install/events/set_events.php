<?
$MESS["QUESTIONS_FORM_TITLE"] = "Заполнена форма «Задать вопрос»";
$MESS["QUESTIONS_FORM_TEXT"] = "#NAME# - Автор вопроса
#ADDRESS# - Адрес
#PHONE# - Телефон
#MAIL# - E-Mail
#TEXT# - Текст вопроса

";
$MESS["QUESTIONS_ANSWER_TITLE"] = "Получен ответ на вопрос";
$MESS["QUESTIONS_ANSWER_TEXT"] = "#NAME# - Автор вопроса
#ADDRESS# - Адрес
#PHONE# - Телефон
#MAIL# - E-Mail
#TEXT# - Текст вопроса

#ANSWER# - Ответ
#ANSWER_AUTHOR# - Автор ответа
";
$MESS["QUESTIONS_FORM_SUBJECT1"] = "#SITE_NAME#: Заполнена форма «Задать вопрос»";
$MESS["QUESTIONS_FORM_MESSAGE1"] = "На сайте #SITE_NAME# получен вопрос через форму «Задать вопрос»
---------------------------------------------------------------------------

Номер вопроса: #ELEMENT_ID#
Автор: #NAME#
Адрес: #ADDRESS#
Телефон: #PHONE#
E-Mail: #MAIL#

Текст вопроса:
---------------------------------------------------------------------------
#TEXT#
---------------------------------------------------------------------------


Для просмотра и ответа воспользуйтесь ссылкой:
http://#SERVER_NAME#/bitrix/admin/iblock_element_edit.php?lang=ru&ID=#ELEMENT_ID#&type=#IBLOCK_TYPE#&IBLOCK_ID=#IBLOCK_ID#&filter_section=#SECTION_ID#

Письмо сгенерировано автоматически.
";
$MESS["QUESTIONS_FORM_SUBJECT2"] = "Ваш вопрос на сайте #SITE_NAME#";
$MESS["QUESTIONS_FORM_MESSAGE2"] = "Текст Вашего вопроса:
---------------------------------------------------------------------------
#TEXT#
---------------------------------------------------------------------------

Данные, которые Вы указали:

Номер вопроса: #ELEMENT_ID#
Автор: #NAME#
Адрес: #ADDRESS#
Телефон: #PHONE#
E-Mail: #MAIL#


Ответ будет отправлен на данный E-Mail.

Для просмотра текущего статуса вопроса воспользуйтесь ссылкой:
http://#SERVER_NAME#/questions-and-answers/?check=#ELEMENT_ID#


Письмо сгенерировано автоматически.
";
$MESS["QUESTIONS_ANSWER_SUBJECT"] = "Получен ответ на Ваш вопрос на сайте #SITE_NAME#";
$MESS["QUESTIONS_ANSWER_MESSAGE"] = "Текст Вашего вопроса:
---------------------------------------------------------------------------
#TEXT#
---------------------------------------------------------------------------


Ответ:
---------------------------------------------------------------------------
#ANSWER#
---------------------------------------------------------------------------

Автор ответа: #ANSWER_AUTHOR#


Письмо сгенерировано автоматически.";

$MESS["TSZH_BEFORE_TSZH_DEMO_EXPIRED_NOTICE_TITLE"] = "Уведомление о предстоящем истечении демо-периода продукта «1С:Сайт ЖКХ»";
$MESS["TSZH_BEFORE_TSZH_DEMO_EXPIRED_NOTICE_TEXT"] = "#HEADER_SENDER# - Значение в заголовке Sender (служебное поле)
#EMAIL_FROM# - E-mail отправителя письма
#EMAIL_TO# – E-Mail получателя
#EXPIRE_DATE# – Дата истечения пробного периода
#ORG_NAME# – Наименование организации
#~ORG_NAME# – Наименование организации, не преобразованное функцией htmlspecialchars()
#ORG_SITE_NAME# – Наименование сайта организации
#ORG_URL# – URL сайта организации
#VDGB_CONTACTS_HTML# – HTML таблицы контактов «Тиражные решения 1С-Рарус»
#UNSUBSCRIBE_URL# – URL отписки от рассылки
--------------------------------------------------------
";
$MESS["TSZH_BEFORE_TSZH_DEMO_EXPIRED_NOTICE_SUBJECT"] = "#~ORG_NAME#: срок доступа к демоверсии Сайта ЖКХ истекает #EXPIRE_DATE#";
$MESS["TSZH_BEFORE_TSZH_DEMO_EXPIRED_NOTICE_MESSAGE"] = '
			<span style="line-height:1.13;">#ORG_NAME#:<br>срок доступа к демоверсии<br>Сайта ЖКХ истекает #EXPIRE_DATE#.</span>
		</td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td width="20"> 
		</td>
		<td style="width:618px;">
			<table width="618" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0">
				<tr>
					<td style="width:618px;">
						<span style="font-size:18px; color:#333333; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif;">
							Добрый день.
						</span>
					</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Вы успешно активировали демосайт <a href="#ORG_URL#" target="_blank" style="color:#ff6c00;">#ORG_SITE_NAME#</a> для вашей организации #ORG_NAME#.
						</span>
					</td>
				</tr>
				<tr height="14">
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Ознакомьтесь подробнее с его возможностями, это очень легко и не требует специальных знаний. Можете разместить новость или заполнить информацию о вашей организации. 
							Если вы решите в итоге приобрести сайт, вам не нужно будет заполнять эти данные второй раз, мы поможем перенести всю информацию на ваш сайт.
						</span>
					</td>
				</tr>
				<tr height="17">
					<td>&nbsp;</td>
				</tr>
			</table>

			<table width="618" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0;" border="0">
				<tr>
					<td colspan="2" style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">Дополнительно хотелось бы напомнить, какие выгоды несет для вас готовый сайт ЖКХ:</td>
				</tr>
				<tr>
					<td colspan="2" height="7">&nbsp;</td>
				</tr>
				<tr>
					<td style="width:17px; height:18px; vertical-align:top; padding-left:10px;"><img src="/bitrix/templates/citrus_tszh_subscribe/images/9.png" height="18" width="17" alt=""></td>
					<td height="18" style="padding-left:10px; vertical-align:top;" >
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Увеличение лояльности потребителей коммунальных услуг - многие жильцы уже давно пользуются интернетом и хотели бы, чтобы у обслуживающей их дом управляющей компании или ТСЖ был свой сайт с личными кабинетами.
						</span>
					</td>
				</tr>
				<tr>
					<td colspan="2" height="15">&nbsp;</td>
				</tr>
				<tr>
					<td style="width:17px; height:18px; vertical-align:top; padding-left:10px;"><img src="/bitrix/templates/citrus_tszh_subscribe/images/9.png" height="18" width="17" alt=""></td>
					<td height="18" style="padding-left:10px; vertical-align:top;" >
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Вы сможете принимать платежи за коммунальные услуги напрямую без посредников прямо через интернет - по статистике у ТСЖ и управляющих компаний имеющих свой сайт собираемость платежей в среднем на 15.7% выше.
						</span>
					</td>
				</tr>
				<tr>
					<td colspan="2" height="15">&nbsp;</td>
				</tr>
				<tr>
					<td style="width:17px; height:18px; vertical-align:top; padding-left:10px;"><img src="/bitrix/templates/citrus_tszh_subscribe/images/9.png" height="18" width="17" alt=""></td>
					<td height="18" style="padding-left:10px; vertical-align:top;" >
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Вы сможете быстрее собирать показания приборов учёта (счётчиков) - так как жильцы будут заполнять их онлайн на сайте, а вы загружать в один клик.
						</span>
					</td>
				</tr>
				<tr>
					<td colspan="2" height="15">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">
						<p style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">Обратитесь в отдел продаж, и мы поможем вам определиться с выбором редакции сайта.</p>
					</td>
				</tr>
			</table>
		</td>

		<td width="20"> </td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td colspan="3" style="height:50px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="20"> </td>
		<td style="width:618px; padding-bottom:8px;">
			<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">С уважением,<br>Компания «Тиражные решения 1С-Рарус».</span>
			#VDGB_CONTACTS_HTML#
		</td>
		<td width="20"> </td>
	</tr>
</table>

				</td>
			</tr>
		</table>

<table width="660" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#ffffff;" border="0">
	<tr>
		<td style="width:658px; padding:9px 0 5px 0;">
			<span style="font-size:12px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.5;">
				<a href="#UNSUBSCRIBE_URL#" target="_blank" style="color:#999999; text-decoration:underline;">отписаться от рассылки</a>
';

$MESS["TSZH_AFTER_TSZH_DEMO_EXPIRED_NOTICE_TITLE"] = "Уведомление об истечении демо-периода продукта «1С:Сайт ЖКХ»";
$MESS["TSZH_AFTER_TSZH_DEMO_EXPIRED_NOTICE_TEXT"] = "#HEADER_SENDER# - Значение в заголовке Sender (служебное поле)
#EMAIL_FROM# - E-mail отправителя письма
#EMAIL_TO# – E-mail получателя
#ORG_NAME# – Наименование организации
#~ORG_NAME# – Наименование организации, не преобразованное функцией htmlspecialchars()
#ORG_SITE_NAME# – Наименование сайта организации
#ORG_URL# – URL сайта организации
#VDGB_CONTACTS_HTML# – HTML таблицы контактов «Тиражные решения 1С-Рарус»
#UNSUBSCRIBE_URL# – URL отписки от рассылки
--------------------------------------------------------
";
$MESS["TSZH_AFTER_TSZH_DEMO_EXPIRED_NOTICE_SUBJECT"] = "#~ORG_NAME#: срок доступа к демоверсии Сайта ЖКХ истёк";
$MESS["TSZH_AFTER_TSZH_DEMO_EXPIRED_NOTICE_MESSAGE"] = '
			<span style="line-height:1.13;">#ORG_NAME#:<br>срок доступа к демоверсии<br>Сайта ЖКХ истёк.</span>
		</td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td width="20"> 
		</td>
		<td style="width:618px;">
			<table width="618" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0">
				<tr>
					<td style="width:618px;">
						<span style="font-size:18px; color:#333333; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif;">
							Добрый день.
						</span>
					</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Вы успешно активировали демосайт <a href="#ORG_URL#" target="_blank" style="color:#ff6c00;">#ORG_SITE_NAME#</a> для вашей организации #ORG_NAME#,
							но срок доступа к демоверсии истёк.
						</span>
					</td>
				</tr>
				<tr height="14">
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Если вы так и не успели ознакомиться подробнее с его возможностями, обратитесь в отдел продаж, мы поможем вам активировать его заново.
							Пользоваться сайтом ЖКХ очень легко и не требует специальных знаний. Можете разместить новость или заполнить информацию о вашей организации. 
							Если вы решите в итоге приобрести сайт, вам не нужно будет заполнять эти данные второй раз, мы поможем перенести всю информацию на ваш сайт.
						</span>
					</td>
				</tr>
				<tr height="17">
					<td>&nbsp;</td>
				</tr>
			</table>

			<table width="618" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0;" border="0">
				<tr>
					<td colspan="2" style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">Дополнительно хотелось бы напомнить, какие выгоды несет для вас готовый сайт ЖКХ:</td>
				</tr>
				<tr>
					<td colspan="2" height="7">&nbsp;</td>
				</tr>
				<tr>
					<td style="width:17px; height:18px; vertical-align:top; padding-left:10px;"><img src="/bitrix/templates/citrus_tszh_subscribe/images/9.png" height="18" width="17" alt=""></td>
					<td height="18" style="padding-left:10px; vertical-align:top;" >
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Увеличение лояльности потребителей коммунальных услуг - многие жильцы уже давно пользуются интернетом и хотели бы, чтобы у обслуживающей их дом управляющей компании или ТСЖ был свой сайт с личными кабинетами.
						</span>
					</td>
				</tr>
				<tr>
					<td colspan="2" height="15">&nbsp;</td>
				</tr>
				<tr>
					<td style="width:17px; height:18px; vertical-align:top; padding-left:10px;"><img src="/bitrix/templates/citrus_tszh_subscribe/images/9.png" height="18" width="17" alt=""></td>
					<td height="18" style="padding-left:10px; vertical-align:top;" >
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Вы сможете принимать платежи за коммунальные услуги напрямую без посредников прямо через интернет - по статистике у ТСЖ и управляющих компаний имеющих свой сайт собираемость платежей в среднем на 15.7% выше.
						</span>
					</td>
				</tr>
				<tr>
					<td colspan="2" height="15">&nbsp;</td>
				</tr>
				<tr>
					<td style="width:17px; height:18px; vertical-align:top; padding-left:10px;"><img src="/bitrix/templates/citrus_tszh_subscribe/images/9.png" height="18" width="17" alt=""></td>
					<td height="18" style="padding-left:10px; vertical-align:top;" >
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Вы сможете быстрее собирать показания приборов учёта (счётчиков) - так как жильцы будут заполнять их онлайн на сайте, а вы загружать в один клик.
						</span>
					</td>
				</tr>
				<tr>
					<td colspan="2" height="15">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">
						<p style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">Обратитесь в отдел продаж, и мы поможем вам определиться с выбором редакции сайта.</p>
					</td>
				</tr>
			</table>
		</td>

		<td width="20"> </td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td colspan="3" style="height:50px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="20"> </td>
		<td style="width:618px; padding-bottom:8px;">
			<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">С уважением,<br>Компания «Тиражные решения 1С-Рарус».</span>
			#VDGB_CONTACTS_HTML#
		</td>
		<td width="20"> </td>
	</tr>
</table>

				</td>
			</tr>
		</table>

<table width="660" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#ffffff;" border="0">
	<tr>
		<td style="width:658px; padding:9px 0 5px 0;">
			<span style="font-size:12px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.5;">
				<a href="#UNSUBSCRIBE_URL#" target="_blank" style="color:#999999; text-decoration:underline;">отписаться от рассылки</a>
';

$MESS["TSZH_ADD_PAY_SYSTEM_NOTICE_TITLE"] = "Уведомление с предложением подключить систему приема платежей";
$MESS["TSZH_ADD_PAY_SYSTEM_NOTICE_TEXT"] = "#HEADER_SENDER# - Значение в заголовке Sender (служебное поле)
#EMAIL_FROM# - E-mail отправителя письма
#EMAIL_TO# – E-mail получателя
#ORG_NAME# – Наименование организации
#~ORG_NAME# – Наименование организации, не преобразованное функцией htmlspecialchars()
#ORG_SITE_NAME# – Наименование сайта организации
#ORG_URL# – URL сайта организации
#ADD_PAY_SYSTEM_URL# – URL добавления платёжной системы
#VDGB_CONTACTS_HTML# – HTML таблицы контактов «Тиражные решения 1С-Рарус»
#UNSUBSCRIBE_URL# – URL отписки от рассылки
--------------------------------------------------------
";
$MESS["TSZH_ADD_PAY_SYSTEM_NOTICE_SUBJECT"] = "#~ORG_NAME#: принимайте платежи на своём сайте без посредников";
$MESS["TSZH_ADD_PAY_SYSTEM_NOTICE_MESSAGE"] = '
			<span style="line-height:1.13;">#ORG_NAME#:<br>подключите систему приёма платежей.</span>
		</td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td width="20"> 
		</td>
		<td style="width:618px;">
			<table width="618" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0">
				<tr>
					<td style="width:618px;">
						<span style="font-size:18px; color:#333333; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif;">
							Добрый день.
						</span>
					</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Напоминаем, что ваша организация #ORG_NAME# до сих пор ещё не подключила приём платежей на вашем сайте 
							<a href="#ORG_URL#" target="_blank" style="color:#ff6c00;">#ORG_SITE_NAME#</a>. Вы можете принимать платежи сами без посредников.
						</span>
					</td>
				</tr>
				<tr height="14">
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Очень часто задолженность по коммунальным услугам накапливается только лишь по причине того, что жильцам некогда оплатить коммунальные услуги. 
							<a href="#ORG_URL#" target="_blank" style="color:#ff6c00;">Ваш сайт</a> поможет вам собирать коммунальные платежи быстрее. 
							Распространенность банковских карт и доступность интернета уже настолько велика, что многим удобно оплачивать услуги ЖКХ через интернет.
						</span>
					</td>
				</tr>
				<tr height="14">
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							<a href="#ADD_PAY_SYSTEM_URL#" target="_blank" style="color:#ff6c00;">Подключите сами</a> или обратитесь в нашу службу поддержки, 
							и мы поможем подключить приём платежей вам совершенно бесплатно.
						</span>
					</td>
				</tr>
				<tr height="17">
					<td>&nbsp;</td>
				</tr>
			</table>
		</td>

		<td width="20"> </td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td colspan="3" style="height:50px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="20"> </td>
		<td style="width:618px; padding-bottom:8px;">
			<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">С уважением,<br>Компания «Тиражные решения 1С-Рарус».</span>
			#VDGB_CONTACTS_HTML#
		</td>
		<td width="20"> </td>
	</tr>
</table>

				</td>
			</tr>
		</table>

<table width="660" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#ffffff;" border="0">
	<tr>
		<td style="width:658px; padding:9px 0 5px 0;">
			<span style="font-size:12px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.5;">
				<a href="#UNSUBSCRIBE_URL#" target="_blank" style="color:#999999; text-decoration:underline;">отписаться от рассылки</a>
';

$MESS["TSZH_IMPORT_ACCOUNTS_NOTICE_TITLE"] = "Уведомление с предложением загрузить лицевые счета";
$MESS["TSZH_IMPORT_ACCOUNTS_NOTICE_TEXT"] = "#HEADER_SENDER# - Значение в заголовке Sender (служебное поле)
#EMAIL_FROM# - E-mail отправителя письма
#EMAIL_TO# – E-mail получателя
#ORG_NAME# – Наименование организации
#~ORG_NAME# – Наименование организации, не преобразованное функцией htmlspecialchars()
#ORG_SITE_NAME# – Наименование сайта организации
#ORG_URL# – URL сайта организации
#VDGB_CONTACTS_HTML# – HTML таблицы контактов «Тиражные решения 1С-Рарус»
#UNSUBSCRIBE_URL# – URL отписки от рассылки
--------------------------------------------------------
";
$MESS["TSZH_IMPORT_ACCOUNTS_NOTICE_SUBJECT"] = "#~ORG_NAME#: выгрузите данные по лицевым счетам на сайт";
$MESS["TSZH_IMPORT_ACCOUNTS_NOTICE_MESSAGE"] = '
			<span style="line-height:1.13;">#ORG_NAME#:<br>выгрузите данные по лицевым счетам<br>на сайт.</span>
		</td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td width="20"> 
		</td>
		<td style="width:618px;">
			<table width="618" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0">
				<tr>
					<td style="width:618px;">
						<span style="font-size:18px; color:#333333; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif;">
							Добрый день.
						</span>
					</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
						    Напоминание руководству #ORG_NAME#.<br>
							Жильцы дома(ов), которые обслуживает ваша организация, знают о том, что у вас есть сайт 
							<a href="#ORG_URL#" target="_blank" style="color:#ff6c00;">#ORG_SITE_NAME#</a>. И они очень хотят воспользоваться функциями личного кабинета.
						</span>
					</td>
				</tr>
				<tr height="14">
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Как можно скорее, пожалуйста, загрузите информацию о лицевых счетах на <a href="#ORG_URL#" target="_blank" style="color:#ff6c00;">ваш сайт</a> 
							и раздайте логины и пароли жильцам.
						</span>
					</td>
				</tr>
				<tr height="17">
					<td>&nbsp;</td>
				</tr>
			</table>

			<table width="618" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0;" border="0">
				<tr>
					<td colspan="2" style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">Благодаря этому:</td>
				</tr>
				<tr>
					<td colspan="2" height="7">&nbsp;</td>
				</tr>
				<tr>
					<td style="width:17px; height:18px; vertical-align:top; padding-left:10px;"><img src="/bitrix/templates/citrus_tszh_subscribe/images/9.png" height="18" width="17" alt=""></td>
					<td height="18" style="padding-left:10px; vertical-align:top;" >
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Вы повысите лояльность к вашей организации.
						</span>
					</td>
				</tr>
				<tr>
					<td colspan="2" height="15">&nbsp;</td>
				</tr>
				<tr>
					<td style="width:17px; height:18px; vertical-align:top; padding-left:10px;"><img src="/bitrix/templates/citrus_tszh_subscribe/images/9.png" height="18" width="17" alt=""></td>
					<td height="18" style="padding-left:10px; vertical-align:top;" >
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Сможете принимать платежи за услуги ЖКХ напрямую, без посредников через ваш сайт.
						</span>
					</td>
				</tr>
				<tr>
					<td colspan="2" height="15">&nbsp;</td>
				</tr>
				<tr>
					<td style="width:17px; height:18px; vertical-align:top; padding-left:10px;"><img src="/bitrix/templates/citrus_tszh_subscribe/images/9.png" height="18" width="17" alt=""></td>
					<td height="18" style="padding-left:10px; vertical-align:top;" >
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Собирать показания счётчиков автоматически.
						</span>
					</td>
				</tr>
				<tr>
					<td colspan="2" height="15">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2" style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">Вам помогут следующие инструкции:</td>
				</tr>
				<tr>
					<td colspan="2" height="7">&nbsp;</td>
				</tr>
				<tr>
					<td style="width:17px; height:18px; vertical-align:top; padding-left:10px;"><img src="/bitrix/templates/citrus_tszh_subscribe/images/9.png" height="18" width="17" alt=""></td>
					<td height="18" style="padding-left:10px; vertical-align:top;" >
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							<a href="http://www.vdgb-soft.ru/faq/faq_tsj_3_0/%D0%B8%D0%BD%D1%82%D0%B5%D0%B3%D1%80%D0%B0%D1%86%D0%B8%D1%8F_%D1%81_%D1%81%D0%B0%D0%B9%D1%82%D0%BE%D0%BC/" target="_blank" style="color:#ff6c00;">Выгрузка из 1С ЖКХ</a>
						</span>
					</td>
				</tr>
				<tr>
					<td colspan="2" height="15">&nbsp;</td>
				</tr>
				<tr>
					<td style="width:17px; height:18px; vertical-align:top; padding-left:10px;"><img src="/bitrix/templates/citrus_tszh_subscribe/images/9.png" height="18" width="17" alt=""></td>
					<td height="18" style="padding-left:10px; vertical-align:top;" >
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							<a href="http://www.vdgb-soft.ru/faq/faq_site_gkh/" target="_blank" style="color:#ff6c00;">Инструкции по работе с сайтом</a>
						</span>
					</td>
				</tr>
			</table>
		</td>

		<td width="20"> </td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td colspan="3" style="height:50px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="20"> </td>
		<td style="width:618px; padding-bottom:8px;">
			<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">С уважением,<br>Компания «Тиражные решения 1С-Рарус».</span>
			#VDGB_CONTACTS_HTML#
		</td>
		<td width="20"> </td>
	</tr>
</table>

				</td>
			</tr>
		</table>

<table width="660" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#ffffff;" border="0">
	<tr>
		<td style="width:658px; padding:9px 0 5px 0;">
			<span style="font-size:12px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.5;">
				<a href="#UNSUBSCRIBE_URL#" target="_blank" style="color:#999999; text-decoration:underline;">отписаться от рассылки</a>
';

$MESS["TSZH_IMPORT_CHARGES_NOTICE_TITLE"] = "Уведомление с предложением загрузить начисления";
$MESS["TSZH_IMPORT_CHARGES_NOTICE_TEXT"] = "#HEADER_SENDER# - Значение в заголовке Sender (служебное поле)
#EMAIL_FROM# - E-mail отправителя письма
#EMAIL_TO# – E-mail получателя
#ORG_NAME# – Наименование организации
#~ORG_NAME# – Наименование организации, не преобразованное функцией htmlspecialchars()
#ORG_SITE_NAME# – Наименование сайта организации
#ORG_URL# – URL сайта организации
#VDGB_CONTACTS_HTML# – HTML таблицы контактов «Тиражные решения 1С-Рарус»
#UNSUBSCRIBE_URL# – URL отписки от рассылки
--------------------------------------------------------
";
$MESS["TSZH_IMPORT_CHARGES_NOTICE_SUBJECT"] = "#~ORG_NAME#: загрузите квитанции на сайт";
$MESS["TSZH_IMPORT_CHARGES_NOTICE_MESSAGE"] = '
			<span style="line-height:1.13;">#ORG_NAME#:<br>загрузите квитанции на сайт.</span>
		</td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td width="20"> 
		</td>
		<td style="width:618px;">
			<table width="618" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0">
				<tr>
					<td style="width:618px;">
						<span style="font-size:18px; color:#333333; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif;">
							Добрый день.
						</span>
					</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
						    Напоминание руководству #ORG_NAME#.<br>
							На вашем сайте <a href="#ORG_URL#" target="_blank" style="color:#ff6c00;">#ORG_SITE_NAME#</a> есть личные кабинеты для жильцов, 
							но вы давно не загружали на сайт начисления. Людям очень удобно получать эту информацию через интернет.
						</span>
					</td>
				</tr>
				<tr height="14">
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Сделайте выгрузку последних начислений на сайт.
						</span>
					</td>
				</tr>
				<tr height="17">
					<td>&nbsp;</td>
				</tr>
			</table>

			<table width="618" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0;" border="0">
				<tr>
					<td colspan="2" style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">В этом вам помогут наши инструкции:</td>
				</tr>
				<tr>
					<td colspan="2" height="7">&nbsp;</td>
				</tr>
				<tr>
					<td style="width:17px; height:18px; vertical-align:top; padding-left:10px;"><img src="/bitrix/templates/citrus_tszh_subscribe/images/9.png" height="18" width="17" alt=""></td>
					<td height="18" style="padding-left:10px; vertical-align:top;" >
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							<a href="http://www.vdgb-soft.ru/faq/faq_tsj_3_0/%D0%B8%D0%BD%D1%82%D0%B5%D0%B3%D1%80%D0%B0%D1%86%D0%B8%D1%8F_%D1%81_%D1%81%D0%B0%D0%B9%D1%82%D0%BE%D0%BC/" target="_blank" style="color:#ff6c00;">Выгрузка из 1С ЖКХ</a>
						</span>
					</td>
				</tr>
				<tr>
					<td colspan="2" height="15">&nbsp;</td>
				</tr>
				<tr>
					<td style="width:17px; height:18px; vertical-align:top; padding-left:10px;"><img src="/bitrix/templates/citrus_tszh_subscribe/images/9.png" height="18" width="17" alt=""></td>
					<td height="18" style="padding-left:10px; vertical-align:top;" >
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							<a href="http://www.vdgb-soft.ru/faq/faq_site_gkh/" target="_blank" style="color:#ff6c00;">Инструкции по работе с сайтом</a>
						</span>
					</td>
				</tr>
			</table>
		</td>

		<td width="20"> </td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td colspan="3" style="height:50px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="20"> </td>
		<td style="width:618px; padding-bottom:8px;">
			<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">С уважением,<br>Компания «Тиражные решения 1С-Рарус».</span>
			#VDGB_CONTACTS_HTML#
		</td>
		<td width="20"> </td>
	</tr>
</table>

				</td>
			</tr>
		</table>

<table width="660" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#ffffff;" border="0">
	<tr>
		<td style="width:658px; padding:9px 0 5px 0;">
			<span style="font-size:12px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.5;">
				<a href="#UNSUBSCRIBE_URL#" target="_blank" style="color:#999999; text-decoration:underline;">отписаться от рассылки</a>
';

$MESS["TSZH_ADD_NEWS_NOTICE_TITLE"] = "Уведомление с предложением опубликовать новость";
$MESS["TSZH_ADD_NEWS_NOTICE_TEXT"] = "#HEADER_SENDER# - Значение в заголовке Sender (служебное поле)
#EMAIL_FROM# - E-mail отправителя письма
#EMAIL_TO# – E-mail получателя
#ORG_NAME# – Наименование организации
#~ORG_NAME# – Наименование организации, не преобразованное функцией htmlspecialchars()
#ORG_SITE_NAME# – Наименование сайта организации
#ORG_URL# – URL сайта организации
#ADD_NEWS_ITEM_URL# – URL добавления новости
#VDGB_CONTACTS_HTML# – HTML таблицы контактов «Тиражные решения 1С-Рарус»
#UNSUBSCRIBE_URL# – URL отписки от рассылки
--------------------------------------------------------
";
$MESS["TSZH_ADD_NEWS_NOTICE_SUBJECT"] = "#~ORG_NAME#: вы давно не делились новостями с вашими потребителями услуг ЖКХ";
$MESS["TSZH_ADD_NEWS_NOTICE_MESSAGE"] = '
			<span style="line-height:1.13;">#ORG_NAME#:<br>опубликуйте новость на сайте.</span>
		</td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td width="20"> 
		</td>
		<td style="width:618px;">
			<table width="618" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0">
				<tr>
					<td style="width:618px;">
						<span style="font-size:18px; color:#333333; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif;">
							Добрый день.
						</span>
					</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Напоминание руководству #ORG_NAME#.<br>
							Вы давно не публиковали новостей на вашем сайте <a href="#ORG_URL#" target="_blank" style="color:#ff6c00;">#ORG_SITE_NAME#</a>.
						</span>
					</td>
				</tr>
				<tr height="14">
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Жильцы домов, которые(ый) обслуживает ваша организация, часто заходят на ваш сайт <a href="#ORG_URL#" target="_blank" style="color:#ff6c00;">#ORG_SITE_NAME#</a>.
							Чаще всего их интересуют различные новости и объявления, касающиеся их домов, особенно часто они заходят в дни, 
							когда случаются какие-то аварии или прочие чрезвычайные ситуации.
						</span>
					</td>
				</tr>
				<tr height="14">
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Вовремя публикуя новости о различных аварийных ситуациях и сроках их устранения, а также прочие важные новости, 
							вы значительно снизите нагрузку на вашу аварийно-диспетчерскую службу и люди будут меньше звонить с однотипными вопросами.<br>
							Начните публиковать новости о деятельности #ORG_NAME# и о зданиях, находящихся на вашем управлении, прямо сегодня.
						</span>
					</td>
				</tr>
				<tr height="17">
					<td>&nbsp;</td>
				</tr>
			</table>
		</td>

		<td width="20"> </td>
	</tr>
</table>

<table cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0">
	<tr>
		<td colspan="3" height="20">&nbsp;</td>
	</tr>
	<tr>
		<td style="width: 210px;"> </td>
		<td width="240" align="center">
			<table align="center" cellpadding="0" cellspacing="0" style="border-collapse:separate !important; border-radius:3px; margin:0; padding:0; background:#ff9900;" border="0">
				<tr>
					<td style="background-color:#ff9900; width:240px; height:36px; border-radius:3px;" align="center">
						<a href="#ADD_NEWS_ITEM_URL#" target="_blank" style="display:block; width:240px; height:36px; padding:0; margin:0; font-size:18px; line-height:1.7; color:#ffffff; text-decoration:none; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; border-radius:3px;">Опубликовать новость</a>
					</td>
				</tr>
			</table>               
		</td>
		<td style="width: 210px;"> </td>
	</tr>
	<tr>
		<td colspan="3" height="25">&nbsp;</td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td colspan="3" style="height:50px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="20"> </td>
		<td style="width:618px; padding-bottom:8px;">
			<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">С уважением,<br>Компания «Тиражные решения 1С-Рарус».</span>
			#VDGB_CONTACTS_HTML#
		</td>
		<td width="20"> </td>
	</tr>
</table>
				</td>
			</tr>
		</table>

<table width="660" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#ffffff;" border="0">
	<tr>
		<td style="width:658px; padding:9px 0 5px 0;">
			<span style="font-size:12px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.5;">
				<a href="#UNSUBSCRIBE_URL#" target="_blank" style="color:#999999; text-decoration:underline;">отписаться от рассылки</a>
';

$MESS["TSZH_NEWS_ITEM_TITLE"] = "Уведомление собственникам об опубликованной новости";
$MESS["TSZH_NEWS_ITEM_TEXT"] = "#HEADER_SENDER# – Значение в заголовке Sender (служебное поле)
#EMAIL_FROM# – E-mail отправителя письма
#EMAIL_TO# – E-Mail получателя
#ACCOUNT_NAME# – ФИО владельца лицевого счёта
#ORG_NAME# – Наименование организации
#~ORG_NAME# – Наименование организации, не преобразованное функцией htmlspecialchars()
#NEWS_ITEM_DATE_ACTIVE_FROM# – Дата публикации новости
#NEWS_ITEM_NAME# – Наименование новости
#NEWS_ITEM_PREVIEW_TEXT# – Текст анонса новости
#NEWS_ITEM_DETAIL_PAGE_URL# – URL детальной страницы новости
#UNSUBSCRIBE_URL# – URL отписки от рассылки
--------------------------------------------------------
";
$MESS["TSZH_NEWS_ITEM_SUBJECT"] = "На сайте #~ORG_NAME# опубликована новость";
$MESS["TSZH_NEWS_ITEM_MESSAGE"] = '
			<span style="line-height:1.13;">На сайте #ORG_NAME#<br>опубликована новость.</span>
		</td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td width="20"> 
		</td>
		<td style="width:618px;">
			<table width="618" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0">
				<tr>
					<td style="width:618px;">
						<span style="font-size:18px; color:#333333; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif;">
							Добрый день.
						</span>
					</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Сообщаем Вам, что на сайте #ORG_NAME# опубликована следующая новость.
						</span>
					</td>
				</tr>
				<tr height="14">
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							#NEWS_ITEM_DATE_ACTIVE_FROM#<br>
							<a href="#NEWS_ITEM_DETAIL_PAGE_URL#" target="_blank" style="color:#ff6c00;">#NEWS_ITEM_NAME#</a>
							<div style="margin-top:5px;">#NEWS_ITEM_PREVIEW_TEXT#</div>
						</span>
					</td>
				</tr>
				<tr height="17">
					<td>&nbsp;</td>
				</tr>
			</table>
		</td>

		<td width="20"> </td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td colspan="3" style="height:50px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="20"> </td>
		<td style="width:618px; padding-bottom:8px;">
			<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">С уважением,<br>#ORG_NAME#.</span>
		</td>
		<td width="20"> </td>
	</tr>
</table>
				</td>
			</tr>
		</table>

<table width="660" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#ffffff;" border="0">
	<tr>
		<td style="width:658px; padding:9px 0 5px 0;">
			<span style="font-size:12px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.5;">
				<a href="#UNSUBSCRIBE_URL#" target="_blank" style="color:#999999; text-decoration:underline;">отписаться от рассылки</a>
';

$MESS["TSZH_DEBTOR_NOTICE_TITLE"] = "Уведомление о задолженности";
$MESS["TSZH_DEBTOR_NOTICE_TEXT"] = "#HEADER_SENDER# - Значение в заголовке Sender (служебное поле)
#EMAIL_FROM# - E-mail отправителя письма
#EMAIL_TO# – E-Mail получателя
#ACCOUNT_NAME# – ФИО владельца лицевого счёта
#DEBT_END# – Задолженность на конец периода за вычетом начислений за период
#ORG_NAME# – Наименование организации
#~ORG_NAME# – Наименование организации, не преобразованное функцией htmlspecialchars()
#PERSONAL_URL# – URL личного кабинета
#PAYMENT_URL# – URL страницы оплаты в личном кабинете
#UNSUBSCRIBE_URL# – URL отписки от рассылки
--------------------------------------------------------
";
$MESS["TSZH_DEBTOR_NOTICE_SUBJECT"] = "Погасите задолженность по коммунальным услугам перед #~ORG_NAME#";
$MESS["TSZH_DEBTOR_NOTICE_MESSAGE"] = '
			<span style="line-height:1.13;">Погасите задолженность<br>по коммунальным услугам<br>перед #ORG_NAME#.</span>
		</td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td width="20"> 
		</td>
		<td style="width:618px;">
			<table width="618" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0">
				<tr>
					<td style="width:618px;">
						<span style="font-size:18px; color:#333333; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif;">
							Уважаемый(ая) #ACCOUNT_NAME#.
						</span>
					</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							У вас имеется задолженность по коммунальным услугам в размере <b>#DEBT_END#</b> перед #ORG_NAME#.
						</span>
					</td>
				</tr>
				<tr height="14">
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Вы можете подробно ознакомиться с квитанцией за коммунальные услуги в своём <a href="#PERSONAL_URL#" target="_blank" style="color:#ff6c00;">личном кабинете</a>, 
							а также увидеть историю расчетов за прошлые периоды.<br>
							Рекомендуем вам оперативно оплатить сформировавшуюся задолженность.
						</span>
					</td>
				</tr>
				<tr height="17">
					<td>&nbsp;</td>
				</tr>
			</table>
		</td>

		<td width="20"> </td>
	</tr>
</table>

<?if (strlen($arParams["PAYMENT_URL"])):?>
<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td width="20"> </td>
		<td style="width:618px;">
			<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">Вы можете оплатить прямо на сайте #ORG_NAME# в вашем личном кабинете.</span>
		</td>
		<td width="20"> </td>
	</tr>
</table>

<table cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0">
	<tr>
		<td colspan="3" height="20">&nbsp;</td>
	</tr>
	<tr>
		<td style="width: 255px;"> </td>
		<td width="150" align="center">
			<table align="center" cellpadding="0" cellspacing="0" style="border-collapse:separate !important; border-radius:3px; margin:0; padding:0; background:#ff9900;" border="0">
				<tr>
					<td style="background-color:#ff9900; width:150px; height:36px; border-radius:3px;" align="center">
						<a href="#PAYMENT_URL#" target="_blank" style="display:block; width:150px; height:36px; padding:0; margin:0; font-size:18px; line-height:1.7; color:#ffffff; text-decoration:none; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; border-radius:3px;">Оплатить</a>
					</td>
				</tr>
			</table>               
		</td>
		<td style="width: 255px;"> </td>
	</tr>
	<tr>
		<td colspan="3" height="25">&nbsp;</td>
	</tr>
</table>
<?endif?>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td colspan="3" style="height:50px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="20"> </td>
		<td style="width:618px; padding-bottom:8px;">
			<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">С уважением,<br>#ORG_NAME#.</span>
		</td>
		<td width="20"> </td>
	</tr>
</table>

				</td>
			</tr>
		</table>

<table width="660" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#ffffff;" border="0">
	<tr>
		<td style="width:658px; padding:9px 0 5px 0;">
			<span style="font-size:12px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.5;">
				<a href="#UNSUBSCRIBE_URL#" target="_blank" style="color:#999999; text-decoration:underline;">отписаться от рассылки</a>
';

$MESS["TSZH_METERS_VALUES_NEED_NOTICE_TITLE"] = "Напоминание о необходимости ввода показаний счетчиков";
$MESS["TSZH_METERS_VALUES_NEED_NOTICE_TEXT"] = "#HEADER_SENDER# - Значение в заголовке Sender (служебное поле)
#EMAIL_FROM# - E-mail отправителя письма
#EMAIL_TO# – E-Mail получателя
#PERIOD_NAME# – Период уведомления
#ORG_NAME# – Наименование организации
#~ORG_NAME# – Наименование организации, не преобразованное функцией htmlspecialchars()
#METERS_URL# – URL страницы ввода показаний счётчиков личного кабинета
#UNSUBSCRIBE_URL# – URL отписки от рассылки
--------------------------------------------------------
";
$MESS["TSZH_METERS_VALUES_NEED_NOTICE_SUBJECT"] = "#~ORG_NAME#: укажите показания счетчиков по вашей квартире";
$MESS["TSZH_METERS_VALUES_NEED_NOTICE_MESSAGE"] = '			
			<span style="line-height:1.13;">Укажите показания приборов учёта<br>на сайте #ORG_NAME#.</span>
		</td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td width="20"> 
		</td>
		<td style="width:618px;">
			<table width="618" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0">
				<tr>
					<td style="width:618px;">
						<span style="font-size:18px; color:#333333; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif;">
							Добрый день.
						</span>
					</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Напоминаем, что Вам необходимо указать актуальные показания счетчиков для правильного начисления квартплаты.
						</span>
					</td>
				</tr>
				<tr height="17">
					<td>&nbsp;</td>
				</tr>
			</table>
		</td>

		<td width="20"> </td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td width="20"> </td>
		<td style="width:618px;">
			<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">Вы можете ввести показания прямо на сайте #ORG_NAME# в вашем личном кабинете.</span>
		</td>
		<td width="20"> </td>
	</tr>
</table>

<table cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0">
	<tr>
		<td colspan="3" height="20">&nbsp;</td>
	</tr>
	<tr>
		<td style="width: 225px;"> </td>
		<td width="210" align="center">
			<table align="center" cellpadding="0" cellspacing="0" style="border-collapse:separate !important; border-radius:3px; margin:0; padding:0; background:#ff9900;" border="0">
				<tr>
					<td style="background-color:#ff9900; width:210px; height:36px; border-radius:3px;" align="center">
						<a href="#METERS_URL#" target="_blank" style="display:block; width:210px; height:36px; padding:0; margin:0; font-size:18px; line-height:1.7; color:#ffffff; text-decoration:none; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; border-radius:3px;">Ввести показания</a>
					</td>
				</tr>
			</table>               
		</td>
		<td style="width: 225px;"> </td>
	</tr>
	<tr>
		<td colspan="3" height="25">&nbsp;</td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td colspan="3" style="height:50px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="20"> </td>
		<td style="width:618px; padding-bottom:8px;">
			<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">С уважением,<br>#ORG_NAME#.</span>
		</td>
		<td width="20"> </td>
	</tr>
</table>

				</td>
			</tr>
		</table>

<table width="660" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#ffffff;" border="0">
	<tr>
		<td style="width:658px; padding:9px 0 5px 0;">
			<span style="font-size:12px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.5;">
				<a href="#UNSUBSCRIBE_URL#" target="_blank" style="color:#999999; text-decoration:underline;">отписаться от рассылки</a>
';

$MESS["TSZH_METER_VERIFICATION_NOTICE_TITLE"] = "Напоминание о дате поверки счетчиков";
$MESS["TSZH_METER_VERIFICATION_NOTICE_TEXT"] = "#HEADER_SENDER# - Значение в заголовке Sender (служебное поле)
#EMAIL_FROM# - E-mail отправителя письма
#EMAIL_TO# – E-mail получателя
#VERIFICATION_DATE# – Дата поверки
#METERS# – Сериализованный массив счётчиков, подлежащих поверке
#ORG_NAME# – Наименование организации
#~ORG_NAME# – Наименование организации, не преобразованное функцией htmlspecialchars()
#ORG_ADDRESS# – Адрес организации
#ORG_PHONE# – Телефон организации
#UNSUBSCRIBE_URL# – URL отписки от рассылки
--------------------------------------------------------
";
$MESS["TSZH_METER_VERIFICATION_NOTICE_SUBJECT"] = "#~ORG_NAME#: #VERIFICATION_DATE# — дата поверки счетчиков";
$MESS["TSZH_METER_VERIFICATION_NOTICE_MESSAGE"] = '
			<span style="line-height:1.13;">Приближается дата поверки<br>Ваших счётчиков.</span>
		</td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td width="20"> 
		</td>
		<td style="width:618px;">
			<table width="618" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0">
				<tr>
					<td colspan="2" style="width:618px;">
						<span style="font-size:18px; color:#333333; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif;">
							Добрый день.
						</span>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Заканчивается межповерочный срок у следующих счётчиков:
						</span>
					</td>
				</tr>
				<tr height="7">
					<td colspan="2">&nbsp;</td>
				</tr>
				<?$arMeters = unserialize($arParams["METERS"]);
				foreach ($arMeters as $meterId => $arMeter):?>
					<tr>
						<td style="width:17px; height:18px; vertical-align:top; padding-left:10px;"><img src="/bitrix/templates/citrus_tszh_subscribe/images/9.png" height="18" width="17" alt=""></td>
						<td height="18" style="padding-left:10px; vertical-align:top;" >
							<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
								<?=$arMeter["NAME"]?> <?=strlen($arMeter["SERVICE_NAME"]) ? "(" . $arMeter["SERVICE_NAME"] . ")" : ""?>
							</span>
						</td>
					</tr>
					<tr>
						<td colspan="2" height="15">&nbsp;</td>
					</tr>
				<?endforeach?>
				<tr>
					<td colspan="2">
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">Необходимо их поверить или заменить.<br>Обратитесь в #ORG_NAME#:</span>
						<?if (strlen($arParams["ORG_ADDRESS"]) || strlen($arParams["ORG_PHONE"])):?>
							<table border="0" style="margin-top:8px; font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
								<?if (strlen($arParams["ORG_ADDRESS"])):?>
									<tr>
										<td><b>Адрес:</b></td>
										<td>#ORG_ADDRESS#</td>
									</tr>
								<?endif;
								if (strlen($arParams["ORG_PHONE"])):?>
									<tr>
										<td><b>Телефон:</b></td>
										<td>#ORG_PHONE#</td>
									</tr>
								<?endif?>
							</table>
						<?endif?>
					</td>
				</tr>
				<tr>
					<td colspan="2" height="15">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">
						<p style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Поверка средств измерений — совокупность операций, выполняемых в целях подтверждения соответствия средств измерений метрологическим требованиям.
						</p>
					</td>
				</tr>
			</table>
		</td>

		<td width="20"> </td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td colspan="3" style="height:50px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="20"> </td>
		<td style="width:618px; padding-bottom:8px;">
			<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">С уважением,<br>#ORG_NAME#.</span>
		</td>
		<td width="20"> </td>
	</tr>
</table>

				</td>
			</tr>
		</table>

<table width="660" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#ffffff;" border="0">
	<tr>
		<td style="width:658px; padding:9px 0 5px 0;">
			<span style="font-size:12px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.5;">
				<a href="#UNSUBSCRIBE_URL#" target="_blank" style="color:#999999; text-decoration:underline;">отписаться от рассылки</a>
';

$MESS["TSZH_RECEIPT_TITLE"] = "Квитанция на оплату коммунальных услуг";
$MESS["TSZH_RECEIPT_TEXT"] = "#HEADER_SENDER# – Значение в заголовке Sender (служебное поле)
#EMAIL_FROM# – E-mail отправителя письма
#EMAIL_TO# – E-Mail получателя
#RECEIPT# – HTML с содержимым квитанции
#FIO# – Фамилия, имя, отчество получателя
#ORG_NAME# – Наименование объекта управления
#~ORG_NAME# – Наименование организации, не преобразованное функцией htmlspecialchars()
#RECEIPT_URL# – URL страницы квитанции личного кабинета
#UNSUBSCRIBE_URL# – URL отписки от рассылки
--------------------------------------------------------
";
$MESS["TSZH_RECEIPT_SUBJECT"] = "Платежный документ за услуги ЖКХ от #~ORG_NAME#";
$MESS["TSZH_RECEIPT_MESSAGE"] = '<h3>Уважаемый(ая) #FIO#.</h3>
<p>Высылаем вам платежный документ по коммунальным услугам, оказанным #ORG_NAME#.</p>

#RECEIPT#

<p style="color: #999999"><small>В случае, если квитанция отображается неправильно, вы всегда можете посмотреть ее в личном кабинете на сайте:</small><br>
	<a href="#RECEIPT_URL#" target="_blank">#RECEIPT_URL#</a>
</p>

<p>С уважением,<br>#ORG_NAME#.</p>
<br>
<br>
<a href="#UNSUBSCRIBE_URL#" target="_blank"><small style="color: #999999">Отписаться от рассылки</small></a>.
';

$MESS["TSZH_CONFIRM_EMAIL_TITLE"] = "Подтверждение смены e-mail'а";
$MESS["TSZH_CONFIRM_EMAIL_TEXT"] = "#USER_ID# - ID пользователя
#USER_EMAIL# - E-mail пользователя
#HASH# - Контрольная строка для смены e-mail'а
";
$MESS["TSZH_CONFIRM_EMAIL_SUBJECT"] = "#SITE_NAME#: Подтверждение смены e-mail'а";
$MESS["TSZH_CONFIRM_EMAIL_MESSAGE"] = "Информационное сообщение сайта #SITE_NAME#
------------------------------------------

Пожалуйста, подтвердите Ваш e-mail #USER_EMAIL#, перейдя по следующей ссылке:
http://#SERVER_NAME#/?tszh_confirm_email=y&user_id=#USER_ID#&hash=#HASH#

Сообщение сгенерировано автоматически.
";

$MESS["TSZH_FEEDBACK_FORM_TITLE"] = "Отправка сообщения через форму обратной связи с ТСЖ";
$MESS["TSZH_FEEDBACK_FORM_TEXT"] = "#HEADER_SENDER# - Значение в заголовке Sender (служебное поле)
#AUTHOR# - Автор сообщения
#AUTHOR_EMAIL# - Email автора сообщения
#TEXT# - Текст сообщения
#EMAIL_FROM# - Email отправителя письма
#EMAIL_TO# - Email получателя письма
";
$MESS["TSZH_FEEDBACK_FORM_SUBJECT"] = "#SITE_NAME#: Сообщение из формы обратной связи с ТСЖ";
$MESS["TSZH_FEEDBACK_FORM_MESSAGE"] = "Информационное сообщение сайта #SITE_NAME#
------------------------------------------

Вам было отправлено сообщение через форму обратной связи с ТСЖ

Автор: #AUTHOR#
E-mail автора: #AUTHOR_EMAIL#

Текст сообщения:
#TEXT#

Сообщение сгенерировано автоматически.
";
