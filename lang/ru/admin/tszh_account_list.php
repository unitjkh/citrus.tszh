<?
$MESS["CITRUS_TSZH_ACCOUNTS"] = "Лицевые счета";
$MESS["CITRUS_TSZH_ACCOUNTS_AUTH"] = "Авторизоваться";
$MESS["CITRUS_TSZH_ACCOUNTS_METERS"] = "Счетчики лицевого счета";
$MESS["CITRUS_TSZH_ACCOUNTS_PERIODS"] = "Квитанции лицевого счета";
$MESS["CITRUS_TSZH_VIEW_USER_PROFILE"] = "Перейти к просмотру профиля пользователя";
$MESS["CITRUS_TSZH_ADD_ACCOUNT"] = "Добавить лицевой счет";
$MESS["CITRUS_TSZH_ADD_ACCOUNT_TITLE"] = "Добавление лицевого счета";
$MESS["CITRUS_TSZH_EDIT_ACCOUNT"] = "Изменить лицевой счет";
?>