<?
$MESS["TSZH_CONTRACTORS"] = "Поставщики услуг";
$MESS["TSZH_F_ID"] = "ID";
$MESS["TSZH_F_XML_ID"] = "Внешний код";
$MESS["TSZH_F_TSZH_ID"] = "Объект управления";
$MESS["TSZH_F_EXECUTOR"] = "Исполнитель";
$MESS["TSZH_F_NAME"] = "Наименование";
$MESS["TSZH_F_ADDRESS"] = "Адрес";
$MESS["TSZH_F_SERVICES"] = "Услуги";
$MESS["TSZH_F_PHONE"] = "Телефон";
$MESS["TSZH_F_BILLING"] = "Платежные реквизиты";
?>