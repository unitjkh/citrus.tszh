<?
$MESS ['TSZH_EXPORT_ERROR'] = "Ошибка экспорта (недостаточно прав)";
$MESS['TSZH_EXPORT'] = "Экспорт...";
$MESS['TSZH_EXPORT_DICTIONARIES'] = "Экспорт словарей";
$MESS['TSZH_EXPORT_DICTIONARIES_PROGRESS'] = "Экспорт словарей";
$MESS['TSZH_EXPORT_DICTIONARIES_DONE'] = "Словари экспортированы";
$MESS ['TSZH_EXPORT_DONE'] = "Экспорт завершен";
$MESS ['TSZH_EXPORT_DONE_REQUESTS'] = "Экспортировано обращений: #COUNT#";
$MESS ['TSZH_EXPORT_REQUESTS'] = "Экспорт обращений";
$MESS ['TSZH_EXPORT_REQUESTS_PROGRESS'] = "Экспортировано обращений: #COUNT#";

$MESS ['TSZH_EXPORT_TITLE'] = "Экспорт обращений";
$MESS ['TSZH_EXPORT_TAB'] = "Экспорт";
$MESS ['TSZH_EXPORT_TAB_TITLE'] = "Настройки экспорта";

$MESS ['TSZH_EXPORT_ONLY_OPENED'] = "Экспортировать только открытые обращения";


$MESS ['TSZH_EXPORT_FILE_ERROR'] = "Ошибка открытия файла.";
$MESS ['TSZH_EXPORT_SECTIONS'] = "Экспорт разделов каталога.";
$MESS ['TSZH_EXPORT_SECTIONS_PROGRESS'] = "Экспортировано #COUNT# разделов каталога.";
$MESS ['TSZH_EXPORT_METADATA_DONE'] = "Метаданные каталога экспортированы.";
$MESS ['TSZH_EXPORT_URL_DATA_FILE'] = "Файл для выгрузки";
$MESS ['TSZH_EXPORT_OPEN'] = "Открыть...";
$MESS ['TSZH_EXPORT_IBLOCK_ID'] = "Информационный блок";
$MESS ['TSZH_EXPORT_START_EXPORT'] = "Экспортировать";
$MESS ['TSZH_EXPORT_STOP_EXPORT'] = "Остановить экспорт";
$MESS ['TSZH_EXPORT_INTERVAL'] = "Длительность шага в секундах (0 - выполнять экспорт за один шаг)";
$MESS ['TSZH_EXPORT_FILE_NAME_ERROR'] = "Имя файла (папки) может состоять только из символов латинского алфавита, цифр, пробела, а также символов: !#\$%&()[]{}+-.;=@^_~";
$MESS ['TSZH_EXPORT_ACCESS_DENIED'] = "Доступ запрещен.";


?>