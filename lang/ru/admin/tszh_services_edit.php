<?

$MESS ['TSE_TAB1'] = "Услуга";
$MESS ['TSE_TAB1_TITLE'] = "Услуга";
$MESS ['TSE_ERROR_NO_PERMISSIONS'] = "Недостаточно прав для редактирования услуг";
$MESS ['TSE_ERROR_NOT_FOUND'] = "Услуга #ID# не найдена";
$MESS ['TSE_ERROR_SAVE'] = "При сохранении услуги произошла ошибка";
$MESS ['TSE_PAGE_TITLE_EDIT'] = "Редактирование услуги №#ID#";
$MESS ['TSE_PAGE_TITLE_ADD'] = "Добавление услуги";
$MESS ['TSE_ERRORS_HAPPENED'] = "Произошли следующие ошибки";
$MESS ['TSE_M_SERVICES_LIST'] = "Список услуг";
$MESS ['TSE_M_SERVICES_LIST_TITLE'] = "Перейти к списку услуг";
$MESS ['TSE_M_ADD_SERVICE'] = "Добавить услугу";
$MESS ['TSE_M_ADD_SERVICE_TITLE'] = "Добавить новую услугу";
$MESS ['TSE_M_DELETE'] = "Удалить услугу";
$MESS ['TSE_M_DELETE_CONFIRM'] = "Вы действительно хотите удалить услугу?";
$MESS ['TSE_M_SETTINGS'] = "Настроить";
$MESS ['TSE_M_SETTINGS_TITLE'] = "Настроить поля формы";

$MESS ['TSE_F_ACTIVE'] = "Активность";
$MESS ['TSE_F_NAME'] = "Наименование";
$MESS ['TSE_F_XML_ID'] = "Символьный код";
$MESS ['TSE_F_UNITS'] = "Единицы измерения";
$MESS ['TSE_F_NORM'] = "Норма потребления";
$MESS ['TSE_TSZH_ID'] = "Объект управления";
$MESS ['TSE_F_TARIFF'] = "Тариф";
$MESS ['TSE_F_TARIFF2'] = "Тариф 2";
$MESS ['TSE_F_TARIFF3'] = "Тариф 3";

$MESS ['TSE_USER_FIELDS'] = "Дополнительные поля";
?>