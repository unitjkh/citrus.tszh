<?
$MESS["TSZH_CONFIRM_UNAUTH"] = "Перечень пользователей, давших согласие на обработку персональных данных";
$MESS["TSZH_F_ID"] = "ID";
$MESS["TSZH_F_USER_ID"] = "ID-пользователя";
$MESS["TSZH_F_ACCOUNT_NAME"] = "Имя пользователя";
$MESS["TSZH_F_CONFIRM_CHECK"] = "Подтверждение согласия";
$MESS["TSZH_F_DATE"] = "Дата";
$MESS["TSZH_F_URL"] = "Адрес страницы";
$MESS["TSZH_F_IP"] = "IP-адрес пользователя";

?>