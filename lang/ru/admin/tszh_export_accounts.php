<?
$MESS ['TEA_ERROR_EXPORT'] = "При экспорте произошла ошибка.";
$MESS ['TEA_PAGE_TITLE'] = "Экспорт параметров доступа владельцев лицевых счетов";
$MESS ['TEA_TAB1'] = "Параметры экпорта";
$MESS ['TEA_TAB1_TITLE'] = "Параметры экпорта";
$MESS ['TEA_TAB2'] = "Экспорт";
$MESS ['TEA_TAB2_TITLE'] = "Экспорт";
$MESS ['TEA_EXPORT_DONE'] = "Экспорт завершен";
$MESS ['TEA_DOWNLOAD_XML'] = "Скачать выгруженный файл";
$MESS ['TEA_NEXT_BTN'] = "Далее &gt;&gt;";
$MESS ['TEA_DELETE_FILE_BTN'] = "Удалить экспортированный файл";
$MESS ['TSZH_EXPORT_NO_TSZH_SELECTED'] = "Не выбран объект управления";
$MESS ['TSZH_EXPORT_TSZH'] = "ТСЖ";
$MESS ['TSZH_EXPORT_TEXT'] = "Выберите организацию, для которой необходимо выгрузить параметры доступа владельцев лицевых счетов и нажмите кнопку «<em>Далее</em>».";
$MESS ['TSZH_EXPORT_STEP_TIME'] = "Длительность шага в секундах<br />(0 - выполнять экспорт за один шаг)";
$MESS ['TSZH_EXPORT_PROGRESS'] = "Выгрузка параметров доступа владельцев лицевых счетов.<br />Осталось выгрузить: #TOTAL#...";

?>