<?php
$MESS ['MONETA_OFFER_PERSONAL_TAB'] = "Личные данные";
$MESS ['MONETA_OFFER_PERSONAL_TAB_TITLE'] = "Личные данные";
$MESS ['MONETA_OFFER_CONTACTS_TAB'] = "Контакты";
$MESS ['MONETA_OFFER_CONTACTS_TAB_TITLE'] = "Контакты";
$MESS ['MONETA_OFFER_HEAD_TAB'] = "Руководитель";
$MESS ['MONETA_OFFER_HEAD_TAB_TITLE'] = "Руководитель";
$MESS ['MONETA_OFFER_FINANCE_TAB'] = "Финансовое положение";
$MESS ['MONETA_OFFER_FINANCE_TAB_TITLE'] = "Финансовое положение";
$MESS ['MONETA_OFFER_BENEFICIARY_TAB'] = "Профиль бенефициара <span style='color:red'>*</span>";
$MESS ['MONETA_OFFER_BENEFICIARY_TAB_TITLE'] = "Профиль бенефициара";
$MESS ['MONETA_OFFER_ORGANIZATION_REGISTRATION_SECTION'] = "Данные о государственной регистрации";
$MESS ['MONETA_OFFER_DIRECTOR_IDENTITY_DOCUMENT_SECTION'] = "Паспорт";
$MESS ['MONETA_OFFER_BENEFICIARY_IDENTITY_DOCUMENT_SECTION'] = "Паспорт";
$MESS ['MONETA_OFFER_ERROR_MESSAGE'] = "Во время работы обнаружены ошибки:";
// $MESS ['MONETA_OFFER_SUCCESS_MESSAGE'] = "Информация успешно сохранена. Окно закроется автоматически через <span id=\"moneta_offer_dialog_close_timer\">10</span> с.";
$MESS ['MONETA_OFFER_SUCCESS_MESSAGE'] = "Информация успешно сохранена.";
$MESS ['MONETA_OFFER_FIELD_REQUIRE_NOTE'] = "<b>Внимание!</b> Необходимо заполнить все поля для подключения и проведения оплат через сервис Монета.ру";
$MESS ['MONETA_OFFER_TAB_BENEFICIARY_NOTE'] = "<span style='color:red'>*</span> Эту вкладку не обязательно заполнять для ИП";
