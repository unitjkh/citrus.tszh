<?
$MESS ['TSZH_FLT_ID'] = "ID";
$MESS ['TSZH_FLT_TSZH_ID'] = "Объект управления";
$MESS ['TSZH_F_ALL'] = "(все)";

$MESS ['TSZH_PAGE_TITLE'] = "Выбор периода";
$MESS ['TSZH_FLT_SEARCH'] = "Поиск";
$MESS ['TSZH_FLT_SEARCH_TITLE'] = "Поиск дома";

$MESS ['TSZH_SELECT'] = "Выбрать";
$MESS ['TSZH_EDIT_HOUSE'] = "Перейти к редактированию дома";

$MESS["CITRUS_TSZH_HOUSE_ADDRESS"] = "Адрес";
