<?
$MESS["citrus_tszh_TAB"] = "Лицевой счет";
$MESS["citrus_tszh_ACCOUNT_ID"] = "ID лицевого счета";
$MESS["citrus_tszh_NO_ACCOUNT"] = "Данный пользователь не привязан к лицевому счету";
$MESS["citrus_tszh_CREATE_ACCOUNT"] = "Создать лицевой счет";
$MESS["citrus_tszh_CREATE_ACCOUNT_TITLE"] = "Создать лицевой счет ЖКХ, ТСЖ для пользователя";
$MESS["citrus_tszh_EDIT_ACCOUNT"] = "Редактировать лицевой счет";
$MESS["citrus_tszh_EDIT_ACCOUNT_TITLE"] = "Перейти к редактированию лицевого счета";
$MESS["citrus_tszh_XML_ID"] = "Номер лицевого счета";
$MESS["citrus_tszh_NAME"] = "Имя владельца";
$MESS["citrus_tszh_ADDRESS"] = "Адрес";
$MESS["citrus_tszh_COMMON_AREA"] = "Общая площадь";
$MESS["citrus_tszh_LIVING_AREA"] = "Жилая площадь";
$MESS["citrus_tszh_PEOPLE"] = "Количество проживающих людей";
$MESS["citrus_tszh_TSZH_ID"] = "ТСЖ";
$MESS["citrus_tszh_VIEW_TSZH"] = "Просмотр ТСЖ";
?>