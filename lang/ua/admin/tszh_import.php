<?
$MESS["TI_ERROR_IBLOCK_NOT_INSTALLED"] = "Для роботи імпорту особових рахунків потрібно модуль інформаційних блоків.";
$MESS["TI_ERROR_AJAX_EMPTY_RESULT"] = "Сталася помилка на стороні сервера: отримано порожній результат. \\ \\ nІмпорт не виконаний. ";
$MESS["TI_ERROR_AJAX_GET_ERROR"] = "Помилка відправки запиту. \\ \\ NІмпорт не виконаний. ";
$MESS["TI_TABLE_CREATE_ERROR"] = "Помилка створення тимчасових таблиць для імпорту. ";
$MESS["TI_FILE_ERROR"] = "Помилка відкриття файла.";
$MESS["TI_INDEX_ERROR"] = "Сталася помилка створення індексу. ";
$MESS["TI_TABLES_DROPPED"] = "Тимчасові таблиці, які залишилися після попереднього імпорту, видалені. ";
$MESS["TI_TABLES_CREATION"] = "Створення тимчасових таблиць для імпорту. ";
$MESS["TI_TABLES_CREATED"] = "Тимчасові таблиці для імпорту створені. ";
$MESS["TI_FILE_READING"] = "Зчитування файлу імпорту в тимчасові таблиці. ";
$MESS["TI_FILE_PROGRESS"] = "Зчитування файлу імпорту в тимчасові таблиці. Прочитано: #PERCENT#%.";
$MESS["TI_FILE_READ"] = "Файл імпорта завантажений.";
$MESS["TI_INDEX_CREATION"] = "Створення допоміжних індексів.";
$MESS["TI_INDEX_CREATED"] = "Допоміжні індекси створені.";
$MESS["TI_METADATA"] = "Імпорт послуг.";
$MESS["TI_METADATA_DONE"] = "Послуги імпортовані.";
$MESS["TI_ELEMENTS"] = "Імпорт особових рахунків.";
$MESS["TI_ELEMENTS_PROGRESS"] = "Імпортовано особових рахунків: #DONE# из #TOTAL#.";
$MESS["TI_ELEMENTS_DONE"] = "Особові рахунки імпортовано.";
$MESS["TI_DEACTIVATION"] = "Деактивація особових рахунків і лічильників, відсутніх у файлі імпорту. ";
$MESS["TI_DEACTIVATION_PROGRESS"] = "Деактивовано особових рахунків і лічильників: # DONE #. ";
$MESS["TI_DEACTIVATION_DONE"] = "Особові рахунки і лічильники, які відсутні в файлі імпорту, деативовані.";
$MESS["TI_DELETE"] = "Видалення особових рахунків і лічильників, які відсутні в файлі імпорту.";
$MESS["TI_DELETE_PROGRESS"] = "Видалено особових рахунків і лічильників: #DONE#.";
$MESS["TI_DELETE_DONE"] = "Особові рахунки і лічильники, які відсутні в файлі імпорту, видалені.";
$MESS["TI_DONE"] = "Імпорт завершено";
$MESS["TI_ADDED"] = "Додано нових: # COUNT # ";
$MESS["TI_UPDATED"] = "Оновлено: # COUNT # ";
$MESS["TI_DELETED"] = "Вилучено: # COUNT # ";
$MESS["TI_DEACTIVATED"] = "Деактивовано: # COUNT # ";
$MESS["TI_WITH_ERRORS"] = "З помилками: # COUNT # ";
$MESS["TI_TITLE"] = "Імпорт особових рахунків.";
$MESS["TI_TAB"] = "Імпорт";
$MESS["TI_TAB_TITLE"] = "Налаштування імпорта";
$MESS["TI_URL_DATA_FILE"] = "Файл для завантажування";
$MESS["TI_OPEN"] = "Відкрити...";
$MESS["TI_IBLOCK_TYPE"] = "Тип інформаційного блока";
$MESS["TI_ACTION"] = "Дії над особовими рахунками і лічильниками, <br />які відсутні в файлі";
$MESS["TI_ACTION_NONE"] = "нічого";
$MESS["TI_ACTION_DEACTIVATE"] = "деактивувати";
$MESS["TI_ACTION_DELETE"] = "Видалити";
$MESS["TI_DEPERSONALIZE"] = "Знеособлювати персональні дані";
$MESS["TI_START_IMPORT"] = "Імпортувати";
$MESS["TI_STOP_IMPORT"] = "Зупинити імпорт";
$MESS["TI_INTERVAL"] = "Тривалість кроку в секундах <br /> (0 - виконувати імпорт за один крок) ";
$MESS["TI_ERROR_PROCESS_PERIOD"] = "При обробці періоду сталася помилка. ";
$MESS["TI_UPDATE_MODE"] = "Оновлення";
$MESS["TI_UPDATE_MODE_NO"] = "Створити новий період";
$MESS["TI_UPDATE_MODE_TITLE"] = "Оновити раніше завантажений період ";
$MESS["TI_UPDATE_MODE_ONLY_DEBT"] = "не завантажувати нарахування";
$MESS["TI_USERS"] = "Користувачі";
$MESS["TI_DONT_UPDATE_USERS_TITLE"] = "не змінювати E-Mail і пароль існуючих користувачів ";
$MESS["TI_CREATE_USERS"] = "Автоматично створювати користувачів для імпортованих особових рахунків ";
$MESS["TI_COUNTER_ACCOUNTS"] = "Особові рахунки";
$MESS["TI_COUNTER_USERS"] = "Користувачі на сайті";
$MESS["TI_COUNTER_CHARGES"] = "Нарахування";
$MESS["TI_COUNTER_METERS"] = "Лічильники";
$MESS["TI_COUNTER_METER_VALUES"] = "Показники лічильників";
$MESS["TI_C_ADD"] = "Додано нових: # COUNT # ";
$MESS["TI_C_UPD"] = "Оновлено: # COUNT # ";
$MESS["TI_C_DEL"] = "Вилучено: # COUNT # ";
$MESS["TI_C_DEA"] = "Деактивовано: # COUNT # ";
$MESS["TI_C_ERR"] = "З помилками: # COUNT # ";
$MESS["TI_C_USR_ADD"] = "Додано нових: # COUNT # ";
$MESS["TI_C_USR_UPD"] = "Оновлено: # COUNT # ";
$MESS["TI_C_USR_DEL"] = "Вилучено: # COUNT # ";
$MESS["TI_C_USR_DEA"] = "Деактивовано: # COUNT # ";
$MESS["TI_C_USR_ERR"] = "З помилками: # COUNT # ";
$MESS["TI_C_CHARGE_ADD"] = "Додано: # COUNT # ";
$MESS["TI_C_CHARGE_ERR"] = "З помилками: # COUNT # ";
$MESS["TI_C_METER_ADD"] = "Додано нових: # COUNT # ";
$MESS["TI_C_METER_UPD"] = "Оновлено: # COUNT # ";
$MESS["TI_C_METER_DEL"] = "Вилучено: # COUNT # ";
$MESS["TI_C_METER_DEA"] = "Деактивовано: # COUNT # ";
$MESS["TI_C_METER_ERR"] = "З помилками: # COUNT # ";
$MESS["TI_C_METER_VALUE_ADD"] = "Додано: # COUNT # ";
$MESS["TI_C_METER_VALUE_ERR"] = "З помилками: # COUNT # ";
$MESS["TI_NOTE_1"] = "У випадку, якщо на сайті вже є період, який міститься у файлі, цей період буде оновлено замість створення нового. ";
$MESS["TI_NOTE_2"] = "Будуть завантажені лише дані по заборгованості та оплати. Нарахування, послуги та лічильники завантажуватися не будуть. ";
$MESS["TI_NOTE_3"] = "E-Mail і пароль вже існуючих користувачів особових рахунків не будуть змінені на зазначені у файлі імпорту ";
$MESS["TI_TSZH"] = "ТСЖ";
$MESS["TI_SESSION_EXPIRED"] = "Ваша сесія скінчилася. Авторизуйтесь на сайті і повторіть імпорт.";
$MESS["TI_CREATE_TSZH_NAME"] = "Найменування";
$MESS["TI_CREATE_TSZH_SITE"] = "Сайт";
$MESS["TI_CREATE_TSZH_TEXT"] = "<div style=\"margin-bottom: 25px;\">Об'єкту управління з ІПН <strong>#INN#</strong>, зазначеним в імпортованому файлі, на сайті не існує.<br />
Імпорт може бути продовжено лише після створення об'єкта управління з вказаним ІПН.
<br />
<br />
<strong>Створити об'єкт управління?</strong>
</div>";
$MESS["TI_CREATE_TSZH_SITE_BUTTON_YES"] = "Так, створити і продовжити імпорт";
$MESS["TI_CREATE_TSZH_SITE_BUTTON_NO"] = "Ні, зупинити імпорт";
$MESS["TI_CREATE_TSZH_REQ_FIELD"] = "Поле &laquo;#FIELD#&raquo; обов'язкове для заповнення";
$MESS["TI_ACCOUNT_DELETE_PROGRESS"] = "Вилучено особових рахунків :#Вчинено#.";
$MESS["TI_METER_DELETE_PROGRESS"] = "Вилучено лічильників :#Вчинено#.";
?>