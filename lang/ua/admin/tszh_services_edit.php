<?
$MESS["TSE_TAB1"] = "Послуга";
$MESS["TSE_TAB1_TITLE"] = "Послуга";
$MESS["TSE_ERROR_NO_PERMISSIONS"] = "Недостатньо прав для редагування послуг";
$MESS["TSE_ERROR_NOT_FOUND"] = "Послуга #ID# не знайдена";
$MESS["TSE_ERROR_SAVE"] = "При збереженні послуги виникла помилка.";
$MESS["TSE_PAGE_TITLE_EDIT"] = "Редагування послуги №#ID#";
$MESS["TSE_PAGE_TITLE_ADD"] = "Додавання послуги";
$MESS["TSE_ERRORS_HAPPENED"] = "Відбулися наступні помилки";
$MESS["TSE_M_SERVICES_LIST"] = "Список послуг";
$MESS["TSE_M_SERVICES_LIST_TITLE"] = "Перейти до списку послуг ";
$MESS["TSE_M_ADD_SERVICE"] = "Додати послугу";
$MESS["TSE_M_ADD_SERVICE_TITLE"] = "Додати нову послугу";
$MESS["TSE_M_DELETE"] = "Видалити послугу";
$MESS["TSE_M_DELETE_CONFIRM"] = "Ви дійсно бажаєте видалити послугу?";
$MESS["TSE_M_SETTINGS"] = "Налаштувати";
$MESS["TSE_M_SETTINGS_TITLE"] = "Налаштувати поля форми";
$MESS["TSE_F_ACTIVE"] = "Активність";
$MESS["TSE_F_NAME"] = "Найменування";
$MESS["TSE_F_XML_ID"] = "Символьний код";
$MESS["TSE_F_UNITS"] = "Одиниця вимірювання";
$MESS["TSE_F_NORM"] = "Норма вжитку";
$MESS["TSE_F_TARIFF"] = "Тариф";
$MESS["TSE_F_TARIFF2"] = "Тариф 2";
$MESS["TSE_F_TARIFF3"] = "Тариф 3";
$MESS["TSE_TSZH_ID"] = "ТСЖ";

$MESS["TSE_USER_FIELDS"] = "Додаткові поля";
?>