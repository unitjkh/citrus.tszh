<?
$MESS["TSZH_RECEIPT_TITLE"] = "Квитанція на оплату комунальних послуг";
$MESS["TSZH_RECEIPT_TEXT"] = "#HEADER_SENDER# – Значення в заголовку Sender (службове поле)
#EMAIL_FROM# – E-mail відправника листа
#EMAIL_TO# - E-Mail одержувача
#RECEIPT# - HTML з вмістом квитанції
#FIO# - Прізвище, ім'я, по батькові одержувача
#ORG_NAME# - Найменування організації
#~ORG_NAME# – Найменування організації, що не перетворене функцією htmlspecialchars()
#RECEIPT_URL# – URL сторінки квитанції особистого кабінету
#UNSUBSCRIBE_URL# – URL відписки від розсилки
--------------------------------------------------------
";
$MESS["TSZH_RECEIPT_SUBJECT"] = "Платіжний документ за послуги ЖКГ від #~ORG_NAME#";
$MESS["TSZH_RECEIPT_MESSAGE"] = '<h3>Шановний(а) #FIO#.</h3>
<p>Висилаємо вам платіжний документ по комунальних послугах, наданим #ORG_NAME#.</p>

#RECEIPT#

<p style="color: #999999"><small>У випадку, якщо квитанція відображається неправильно, ви завжди можете подивитися її в особистому кабінеті на сайті:</small><br>
	<a href="#RECEIPT_URL#" target="_blank">#RECEIPT_URL#</a>
</p>

<p>З повагою,<br>#ORG_NAME#.</p>
<br>
<br>
<a href="#UNSUBSCRIBE_URL#" target="_blank"><small style="color: #999999">Відписатися від розсилки</small></a>.
';

$MESS["QUESTIONS_FORM_TITLE"] = "Заповнена форма « Поставити питання »";
$MESS["QUESTIONS_FORM_TEXT"] = "# NAME#- Автор запитання
# #ADDRESS#- Адреса
# #PHONE#- Телефон
# MAIL#- E- Mail
# #TEXT# - Текст питання
";
$MESS["QUESTIONS_ANSWER_TITLE"] = "Отримано відповідь на питання";
$MESS["QUESTIONS_ANSWER_TEXT"] = "# NAME#- Автор запитання
# #ADDRESS#- Адреса
# ТЕЛЕФОН#- Телефон
# MAIL#- E- Mail
# #TEXT# - Текст питання

# #ANSWER# - Відповідь
# ANSWER_AUTHOR#- Автор відповіді
";
$MESS["QUESTIONS_FORM_SUBJECT1"] = "# SITE_NAME#: Заповнена форма « Поставити питання »";
$MESS["QUESTIONS_FORM_MESSAGE1"] = "На сайті #SITE_NAME# отриманий питання через форму« Задати питання »
-------------------------------------------------- -------------------------

Номер питання :#ELEMENT_ID#
Автор :#NAME#
Адреса #ADDRESS#
Телефон :#ТЕЛЕФОН#
E - Mail :#MAIL#

Текст питання :
-------------------------------------------------- -------------------------
#TEXT#
-------------------------------------------------- -------------------------


Для перегляду і відповіді скористайтеся посиланням :
http://#SERVER_NAME#/bitrix/admin/iblock_element_edit.php?lang=ru&ID=#ELEMENT_ID#&type=#IBLOCK_TYPE#&IBLOCK_ID=#IBLOCK_ID#&filter_section=#SECTION_ID#

Лист згенеровано автоматично.
";
$MESS["QUESTIONS_FORM_SUBJECT2"] = "Ваше питання на сайті#SITE_NAME#";
$MESS["QUESTIONS_FORM_MESSAGE2"] = " Текст Вашого питання :
-------------------------------------------------- -------------------------
# Текст#
-------------------------------------------------- -------------------------

Дані , які Ви вказали :

Номер питання :#ELEMENT_ID#
Автор :#NAME#
Адреса :#адреса#
Телефон :#ТЕЛЕФОН#
E - Mail :#MAIL#


Відповідь буде відправлений на даний E - Mail.

Для перегляду поточного статусу питання скористайтеся посиланням :
http://#SERVER_NAME#/ питання - і -відповіді / ? перевірити =#ELEMENT_ID#


Лист згенеровано автоматично.
";
$MESS["QUESTIONS_ANSWER_SUBJECT"] = "Отримано відповідь на Ваше питання на сайті#SITE_NAME#";
$MESS["QUESTIONS_ANSWER_MESSAGE"] = " Текст Вашого питання :
-------------------------------------------------- -------------------------
# Текст#
-------------------------------------------------- -------------------------


відповідь :
-------------------------------------------------- -------------------------
# ВІДПОВІДЬ#
-------------------------------------------------- -------------------------

Автор відповіді :#ANSWER_AUTHOR#


Лист згенеровано автоматично.";

$MESS["TSZH_BEFORE_TSZH_DEMO_EXPIRED_NOTICE_TITLE"] = "Повідомлення про майбутнє закінчення демо-періоду продукту «1С:Сайт ЖКГ»";
$MESS["TSZH_BEFORE_TSZH_DEMO_EXPIRED_NOTICE_TEXT"] = "#HEADER_SENDER# - Значення в заголовку Sender (службове поле)
#EMAIL_FROM# - E-mail відправника листа
#EMAIL_TO# – E-mail одержувача
#EXPIRE_DATE# – Дата закінчення пробного періоду
#ORG_NAME# – Найменування організації
#~ORG_NAME# – Найменування організації, що не перетворене функцією htmlspecialchars()
#ORG_SITE_NAME# – Найменування сайту організації
#ORG_URL# – URL сайту організації
#VDGB_CONTACTS_HTML# – HTML таблиці контактів «ВДГБ Софт»
#UNSUBSCRIBE_URL# – URL відписки від розсилки
--------------------------------------------------------
";
$MESS["TSZH_BEFORE_TSZH_DEMO_EXPIRED_NOTICE_SUBJECT"] = "#~ORG_NAME#: термін доступу до демоверсії Сайту ЖКГ закінчується #EXPIRE_DATE#";
$MESS["TSZH_BEFORE_TSZH_DEMO_EXPIRED_NOTICE_MESSAGE"] = '
			<span style="line-height:1.13;">#ORG_NAME#:<br>термін доступу до демоверсії<br>Сайту ЖКГ закінчується #EXPIRE_DATE#.</span>
		</td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td width="20"> 
		</td>
		<td style="width:618px;">
			<table width="618" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0">
				<tr>
					<td style="width:618px;">
						<span style="font-size:18px; color:#333333; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif;">
							Добрий день.
						</span>
					</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Ви успішно активували демосайт <a href="#ORG_URL#" target="_blank" style="color:#ff6c00;">#ORG_SITE_NAME#</a> для вашої організації #ORG_NAME#.
						</span>
					</td>
				</tr>
				<tr height="14">
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Ознайомтеся докладніше з його можливостями, це дуже легко і не вимагає спеціальних знань. Можете розмістити новину чи заповнити інформацію про вашу організацію. 
							Якщо ви вирішите в підсумку придбати сайт, вам не потрібно буде заповнювати ці дані вдруге, ми допоможемо перенести всю інформацію на ваш сайт.
						</span>
					</td>
				</tr>
				<tr height="17">
					<td>&nbsp;</td>
				</tr>
			</table>

			<table width="618" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0;" border="0">
				<tr>
					<td colspan="2" style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">Додатково хотілося б нагадати, які вигоди несе для вас готовий сайт ЖКГ:</td>
				</tr>
				<tr>
					<td colspan="2" height="7">&nbsp;</td>
				</tr>
				<tr>
					<td style="width:17px; height:18px; vertical-align:top; padding-left:10px;"><img src="/bitrix/templates/citrus_tszh_subscribe/images/9.png" height="18" width="17" alt=""></td>
					<td height="18" style="padding-left:10px; vertical-align:top;" >
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Збільшення лояльності споживачів комунальних послуг - багато мешканців вже давно користуються інтернетом і хотіли б, щоб у обслуговуючої їх будинок керуючої компанії або ТСЖ був свій сайт з особистими кабінетами.
						</span>
					</td>
				</tr>
				<tr>
					<td colspan="2" height="15">&nbsp;</td>
				</tr>
				<tr>
					<td style="width:17px; height:18px; vertical-align:top; padding-left:10px;"><img src="/bitrix/templates/citrus_tszh_subscribe/images/9.png" height="18" width="17" alt=""></td>
					<td height="18" style="padding-left:10px; vertical-align:top;" >
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Ви зможете приймати платежі за комунальні послуги безпосередньо без посередників прямо через інтернет - за статистикою у ТСЖ і керуючих компаній мають свій сайт збирання платежів в середньому на 15.7% вище.
						</span>
					</td>
				</tr>
				<tr>
					<td colspan="2" height="15">&nbsp;</td>
				</tr>
				<tr>
					<td style="width:17px; height:18px; vertical-align:top; padding-left:10px;"><img src="/bitrix/templates/citrus_tszh_subscribe/images/9.png" height="18" width="17" alt=""></td>
					<td height="18" style="padding-left:10px; vertical-align:top;" >
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Ви зможете швидше збирати показання приладів обліку (лічильників) - так як мешканці будуть заповнювати їх онлайн на сайті, а ви завантажувати в один клік.
						</span>
					</td>
				</tr>
				<tr>
					<td colspan="2" height="15">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">
						<p style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">Зверніться до відділу продажів, і ми допоможемо вам визначитися з вибором редакції сайту.</p>
					</td>
				</tr>
			</table>
		</td>

		<td width="20"> </td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td colspan="3" style="height:50px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="20"> </td>
		<td style="width:618px; padding-bottom:8px;">
			<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">З повагою,<br>компанія «ВДГБ СОФТ».</span>
			#VDGB_CONTACTS_HTML#
		</td>
		<td width="20"> </td>
	</tr>
</table>

				</td>
			</tr>
		</table>

<table width="660" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#ffffff;" border="0">
	<tr>
		<td style="width:658px; padding:9px 0 5px 0;">
			<span style="font-size:12px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.5;">
				<a href="#UNSUBSCRIBE_URL#" target="_blank" style="color:#999999; text-decoration:underline;">відписатися від розсилки</a>
';

$MESS["TSZH_AFTER_TSZH_DEMO_EXPIRED_NOTICE_TITLE"] = "Повідомлення про закінчення демо-періоду продукту «1С:Сайт ЖКГ»";
$MESS["TSZH_AFTER_TSZH_DEMO_EXPIRED_NOTICE_TEXT"] = "#HEADER_SENDER# - Значення в заголовку Sender (службове поле)
#EMAIL_FROM# - E-mail відправника листа
#EMAIL_TO# – E-mail одержувача
#ORG_NAME# – Найменування організації
#~ORG_NAME# – Найменування організації, що не перетворене функцією htmlspecialchars()
#ORG_SITE_NAME# – Найменування сайту організації
#ORG_URL# – URL сайту організації
#VDGB_CONTACTS_HTML# – HTML таблиці контактів «ВДГБ Софт»
#UNSUBSCRIBE_URL# – URL відписки від розсилки
--------------------------------------------------------
";
$MESS["TSZH_AFTER_TSZH_DEMO_EXPIRED_NOTICE_SUBJECT"] = "#~ORG_NAME#: термін доступу до демоверсії Сайту ЖКГ закінчився";
$MESS["TSZH_AFTER_TSZH_DEMO_EXPIRED_NOTICE_MESSAGE"] = '
			<span style="line-height:1.13;">#ORG_NAME#:<br>термін доступу до демоверсії<br>Сайту ЖКГ закінчується #EXPIRE_DATE#.</span>
		</td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td width="20"> 
		</td>
		<td style="width:618px;">
			<table width="618" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0">
				<tr>
					<td style="width:618px;">
						<span style="font-size:18px; color:#333333; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif;">
							Добрий день.
						</span>
					</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Ви успішно активували демосайт <a href="#ORG_URL#" target="_blank" style="color:#ff6c00;">#ORG_SITE_NAME#</a> для вашої організації #ORG_NAME#,
							але термін доступу до демоверсії закінчився.
						</span>
					</td>
				</tr>
				<tr height="14">
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Якщо ви так і не встигли ознайомитися докладніше з його можливостями, зверніться у відділ продажів, ми допоможемо вам активувати його заново.
							Користуватися сайтом ЖКГ дуже легко і не вимагає спеціальних знань. Можете розмістити новину чи заповнити інформацію про вашу організацію.
							Якщо ви вирішите в підсумку придбати сайт, вам не потрібно буде заповнювати ці дані вдруге, ми допоможемо перенести всю інформацію на ваш сайт.
						</span>
					</td>
				</tr>
				<tr height="17">
					<td>&nbsp;</td>
				</tr>
			</table>

			<table width="618" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0;" border="0">
				<tr>
					<td colspan="2" style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">Додатково хотілося б нагадати, які вигоди несе для вас готовий сайт ЖКГ:</td>
				</tr>
				<tr>
					<td colspan="2" height="7">&nbsp;</td>
				</tr>
				<tr>
					<td style="width:17px; height:18px; vertical-align:top; padding-left:10px;"><img src="/bitrix/templates/citrus_tszh_subscribe/images/9.png" height="18" width="17" alt=""></td>
					<td height="18" style="padding-left:10px; vertical-align:top;" >
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Збільшення лояльності споживачів комунальних послуг - багато мешканців вже давно користуються інтернетом і хотіли б, щоб у обслуговуючої їх будинок керуючої компанії або ТСЖ був свій сайт з особистими кабінетами.
						</span>
					</td>
				</tr>
				<tr>
					<td colspan="2" height="15">&nbsp;</td>
				</tr>
				<tr>
					<td style="width:17px; height:18px; vertical-align:top; padding-left:10px;"><img src="/bitrix/templates/citrus_tszh_subscribe/images/9.png" height="18" width="17" alt=""></td>
					<td height="18" style="padding-left:10px; vertical-align:top;" >
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Ви зможете приймати платежі за комунальні послуги безпосередньо без посередників прямо через інтернет - за статистикою у ТСЖ і керуючих компаній мають свій сайт збирання платежів в середньому на 15.7% вище.
						</span>
					</td>
				</tr>
				<tr>
					<td colspan="2" height="15">&nbsp;</td>
				</tr>
				<tr>
					<td style="width:17px; height:18px; vertical-align:top; padding-left:10px;"><img src="/bitrix/templates/citrus_tszh_subscribe/images/9.png" height="18" width="17" alt=""></td>
					<td height="18" style="padding-left:10px; vertical-align:top;" >
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Ви зможете швидше збирати показання приладів обліку (лічильників) - так як мешканці будуть заповнювати їх онлайн на сайті, а ви завантажувати в один клік.
						</span>
					</td>
				</tr>
				<tr>
					<td colspan="2" height="15">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">
						<p style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">Зверніться до відділу продажів, і ми допоможемо вам визначитися з вибором редакції сайту.</p>
					</td>
				</tr>
			</table>
		</td>

		<td width="20"> </td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td colspan="3" style="height:50px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="20"> </td>
		<td style="width:618px; padding-bottom:8px;">
			<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">З повагою,<br>компанія «ВДГБ СОФТ».</span>
			#VDGB_CONTACTS_HTML#
		</td>
		<td width="20"> </td>
	</tr>
</table>

				</td>
			</tr>
		</table>

<table width="660" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#ffffff;" border="0">
	<tr>
		<td style="width:658px; padding:9px 0 5px 0;">
			<span style="font-size:12px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.5;">
				<a href="#UNSUBSCRIBE_URL#" target="_blank" style="color:#999999; text-decoration:underline;">відписатися від розсилки</a>
';

$MESS["TSZH_ADD_PAY_SYSTEM_NOTICE_TITLE"] = "Повідомлення із пропозицією підключити систему прийому платежів";
$MESS["TSZH_ADD_PAY_SYSTEM_NOTICE_TEXT"] = "#HEADER_SENDER# - Значення в заголовку Sender (службове поле)
#EMAIL_FROM# - E-mail відправника листа
#EMAIL_TO# – E-mail одержувача
#ORG_NAME# – Найменування організації
#~ORG_NAME# – Найменування організації, що не перетворене функцією htmlspecialchars()
#ORG_SITE_NAME# – Найменування сайту організації
#ORG_URL# – URL сайту організації
#ADD_PAY_SYSTEM_URL# – URL додавання платіжної системи
#VDGB_CONTACTS_HTML# – HTML таблиці контактів «ВДГБ Софт»
#UNSUBSCRIBE_URL# – URL відписки від розсилки
--------------------------------------------------------
";
$MESS["TSZH_ADD_PAY_SYSTEM_NOTICE_SUBJECT"] = "#~ORG_NAME#: приймайте платежі на своєму сайті без посередників";
$MESS["TSZH_ADD_PAY_SYSTEM_NOTICE_MESSAGE"] = '
			<span style="line-height:1.13;">#ORG_NAME#:<br>підключіть систему прийому платежів.</span>
		</td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td width="20"> 
		</td>
		<td style="width:618px;">
			<table width="618" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0">
				<tr>
					<td style="width:618px;">
						<span style="font-size:18px; color:#333333; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif;">
							Добрий день.
						</span>
					</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Нагадуємо, що ваша організація #ORG_NAME# досі ще не підключила прийом платежів на вашому сайті 
							<a href="#ORG_URL#" target="_blank" style="color:#ff6c00;">#ORG_SITE_NAME#</a>. Ви можете приймати платежі самі без посередників.
						</span>
					</td>
				</tr>
				<tr height="14">
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Дуже часто заборгованість по комунальних послугах накопичується тільки лише з причини того, що мешканцям колись оплатити комунальні послуги. 
							<a href="#ORG_URL#" target="_blank" style="color:#ff6c00;">Ваш сайт</a> допоможе вам збирати комунальні платежі швидше. 
							Поширеність банківських карт і доступність інтернету вже настільки велика, що багатьом зручно оплачувати послуги ЖКГ через інтернет.
						</span>
					</td>
				</tr>
				<tr height="14">
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							<a href="#ADD_PAY_SYSTEM_URL#" target="_blank" style="color:#ff6c00;">Підключіть самі</a> або зверніться в нашу службу підтримки,
							і ми допоможемо підключити прийом платежів вам абсолютно безкоштовно.
						</span>
					</td>
				</tr>
				<tr height="17">
					<td>&nbsp;</td>
				</tr>
			</table>
		</td>

		<td width="20"> </td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td colspan="3" style="height:50px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="20"> </td>
		<td style="width:618px; padding-bottom:8px;">
			<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">З повагою,<br>компанія «ВДГБ СОФТ».</span>
			#VDGB_CONTACTS_HTML#
		</td>
		<td width="20"> </td>
	</tr>
</table>

				</td>
			</tr>
		</table>

<table width="660" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#ffffff;" border="0">
	<tr>
		<td style="width:658px; padding:9px 0 5px 0;">
			<span style="font-size:12px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.5;">
				<a href="#UNSUBSCRIBE_URL#" target="_blank" style="color:#999999; text-decoration:underline;">відписатися від розсилки</a>
';

$MESS["TSZH_IMPORT_ACCOUNTS_NOTICE_TITLE"] = "Повідомлення із пропозицією завантажити особові рахунки";
$MESS["TSZH_IMPORT_ACCOUNTS_NOTICE_TEXT"] = "#HEADER_SENDER# - Значення в заголовку Sender (службове поле)
#EMAIL_FROM# - E-mail відправника листа
#EMAIL_TO# – E-mail одержувача
#ORG_NAME# – Найменування організації
#~ORG_NAME# – Найменування організації, що не перетворене функцією htmlspecialchars()
#ORG_SITE_NAME# – Найменування сайту організації
#ORG_URL# – URL сайту організації
#VDGB_CONTACTS_HTML# – HTML таблиці контактів «ВДГБ Софт»
#UNSUBSCRIBE_URL# – URL відписки від розсилки
--------------------------------------------------------
";
$MESS["TSZH_IMPORT_ACCOUNTS_NOTICE_SUBJECT"] = "#~ORG_NAME#: вивантажите дані по особових рахунках на сайт";
$MESS["TSZH_IMPORT_ACCOUNTS_NOTICE_MESSAGE"] = '
			<span style="line-height:1.13;">#ORG_NAME#:<br>вивантажите дані по особових рахунках<br>на сайт.</span>
		</td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td width="20"> 
		</td>
		<td style="width:618px;">
			<table width="618" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0">
				<tr>
					<td style="width:618px;">
						<span style="font-size:18px; color:#333333; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif;">
							Добрий день.
						</span>
					</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
						    Нагадування керівництву #ORG_NAME#.<br>
							Мешканці будинку(ів), які обслуговує ваша організація, знають про те, що у вас є сайт
							<a href="#ORG_URL#" target="_blank" style="color:#ff6c00;">#ORG_SITE_NAME#</a>. І вони дуже хочуть скористатися функціями особистого кабінету.
						</span>
					</td>
				</tr>
				<tr height="14">
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Якомога швидше, будь ласка, завантажте інформацію про особових рахунках на <a href="#ORG_URL#" target="_blank" style="color:#ff6c00;">ваш сайт</a> 
							і роздайте логіни і паролі мешканцям.
						</span>
					</td>
				</tr>
				<tr height="17">
					<td>&nbsp;</td>
				</tr>
			</table>

			<table width="618" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0;" border="0">
				<tr>
					<td colspan="2" style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">Завдяки цьому:</td>
				</tr>
				<tr>
					<td colspan="2" height="7">&nbsp;</td>
				</tr>
				<tr>
					<td style="width:17px; height:18px; vertical-align:top; padding-left:10px;"><img src="/bitrix/templates/citrus_tszh_subscribe/images/9.png" height="18" width="17" alt=""></td>
					<td height="18" style="padding-left:10px; vertical-align:top;" >
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Ви підвищите лояльність до вашої організації.
						</span>
					</td>
				</tr>
				<tr>
					<td colspan="2" height="15">&nbsp;</td>
				</tr>
				<tr>
					<td style="width:17px; height:18px; vertical-align:top; padding-left:10px;"><img src="/bitrix/templates/citrus_tszh_subscribe/images/9.png" height="18" width="17" alt=""></td>
					<td height="18" style="padding-left:10px; vertical-align:top;" >
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Зможете приймати платежі за послуги ЖКГ напряму, без посередників через ваш сайт.
						</span>
					</td>
				</tr>
				<tr>
					<td colspan="2" height="15">&nbsp;</td>
				</tr>
				<tr>
					<td style="width:17px; height:18px; vertical-align:top; padding-left:10px;"><img src="/bitrix/templates/citrus_tszh_subscribe/images/9.png" height="18" width="17" alt=""></td>
					<td height="18" style="padding-left:10px; vertical-align:top;" >
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Збирати свідчення лічильників автоматично.
						</span>
					</td>
				</tr>
				<tr>
					<td colspan="2" height="15">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2" style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">Вам допоможуть наступні інструкції:</td>
				</tr>
				<tr>
					<td colspan="2" height="7">&nbsp;</td>
				</tr>
				<tr>
					<td style="width:17px; height:18px; vertical-align:top; padding-left:10px;"><img src="/bitrix/templates/citrus_tszh_subscribe/images/9.png" height="18" width="17" alt=""></td>
					<td height="18" style="padding-left:10px; vertical-align:top;" >
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							<a href="http://www.vdgb-soft.ru/faq/faq_tsj_3_0/%D0%B8%D0%BD%D1%82%D0%B5%D0%B3%D1%80%D0%B0%D1%86%D0%B8%D1%8F_%D1%81_%D1%81%D0%B0%D0%B9%D1%82%D0%BE%D0%BC/" target="_blank" style="color:#ff6c00;">Вивантаження з 1С ЖКГ</a>
						</span>
					</td>
				</tr>
				<tr>
					<td colspan="2" height="15">&nbsp;</td>
				</tr>
				<tr>
					<td style="width:17px; height:18px; vertical-align:top; padding-left:10px;"><img src="/bitrix/templates/citrus_tszh_subscribe/images/9.png" height="18" width="17" alt=""></td>
					<td height="18" style="padding-left:10px; vertical-align:top;" >
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							<a href="http://www.vdgb-soft.ru/faq/faq_site_gkh/" target="_blank" style="color:#ff6c00;">Інструкції по роботі з сайтом</a>
						</span>
					</td>
				</tr>
			</table>
		</td>

		<td width="20"> </td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td colspan="3" style="height:50px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="20"> </td>
		<td style="width:618px; padding-bottom:8px;">
			<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">З повагою,<br>компанія «ВДГБ СОФТ».</span>
			#VDGB_CONTACTS_HTML#
		</td>
		<td width="20"> </td>
	</tr>
</table>

				</td>
			</tr>
		</table>

<table width="660" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#ffffff;" border="0">
	<tr>
		<td style="width:658px; padding:9px 0 5px 0;">
			<span style="font-size:12px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.5;">
				<a href="#UNSUBSCRIBE_URL#" target="_blank" style="color:#999999; text-decoration:underline;">відписатися від розсилки</a>
';

$MESS["TSZH_IMPORT_CHARGES_NOTICE_TITLE"] = "Повідомлення із пропозицією завантажити нарахування";
$MESS["TSZH_IMPORT_CHARGES_NOTICE_TEXT"] = "#HEADER_SENDER# - Значення в заголовку Sender (службове поле)
#EMAIL_FROM# - E-mail відправника листа
#EMAIL_TO# – E-mail одержувача
#ORG_NAME# – Найменування організації
#~ORG_NAME# – Найменування організації, що не перетворене функцією htmlspecialchars()
#ORG_SITE_NAME# – Найменування сайту організації
#ORG_URL# – URL сайту організації
#VDGB_CONTACTS_HTML# – HTML таблиці контактів «ВДГБ Софт»
#UNSUBSCRIBE_URL# – URL відписки від розсилки
--------------------------------------------------------
";
$MESS["TSZH_IMPORT_CHARGES_NOTICE_SUBJECT"] = "#~ORG_NAME#: завантажте квитанції на сайт";
$MESS["TSZH_IMPORT_CHARGES_NOTICE_MESSAGE"] = '
			<span style="line-height:1.13;">#ORG_NAME#:<br>завантажте квитанції на сайт.</span>
		</td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td width="20"> 
		</td>
		<td style="width:618px;">
			<table width="618" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0">
				<tr>
					<td style="width:618px;">
						<span style="font-size:18px; color:#333333; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif;">
							Добрий день.
						</span>
					</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
						    Нагадування керівництву #ORG_NAME#.<br>
							На вашому сайті <a href="#ORG_URL#" target="_blank" style="color:#ff6c00;">#ORG_SITE_NAME#</a> є особисті кабінети для мешканців, 
							але ви давно не завантажували на сайт нарахування. Людям дуже зручно отримувати цю інформацію через інтернет.
						</span>
					</td>
				</tr>
				<tr height="14">
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Зробіть вивантаження останніх нарахувань на сайт.
						</span>
					</td>
				</tr>
				<tr height="17">
					<td>&nbsp;</td>
				</tr>
			</table>

			<table width="618" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0;" border="0">
				<tr>
					<td colspan="2" style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">У цьому вам допоможуть наші інструкції:</td>
				</tr>
				<tr>
					<td colspan="2" height="7">&nbsp;</td>
				</tr>
				<tr>
					<td style="width:17px; height:18px; vertical-align:top; padding-left:10px;"><img src="/bitrix/templates/citrus_tszh_subscribe/images/9.png" height="18" width="17" alt=""></td>
					<td height="18" style="padding-left:10px; vertical-align:top;" >
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							<a href="http://www.vdgb-soft.ru/faq/faq_tsj_3_0/%D0%B8%D0%BD%D1%82%D0%B5%D0%B3%D1%80%D0%B0%D1%86%D0%B8%D1%8F_%D1%81_%D1%81%D0%B0%D0%B9%D1%82%D0%BE%D0%BC/" target="_blank" style="color:#ff6c00;">Вивантаження з 1С ЖКГ</a>
						</span>
					</td>
				</tr>
				<tr>
					<td colspan="2" height="15">&nbsp;</td>
				</tr>
				<tr>
					<td style="width:17px; height:18px; vertical-align:top; padding-left:10px;"><img src="/bitrix/templates/citrus_tszh_subscribe/images/9.png" height="18" width="17" alt=""></td>
					<td height="18" style="padding-left:10px; vertical-align:top;" >
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							<a href="http://www.vdgb-soft.ru/faq/faq_site_gkh/" target="_blank" style="color:#ff6c00;">Інструкції по роботі з сайтом</a>
						</span>
					</td>
				</tr>
			</table>
		</td>

		<td width="20"> </td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td colspan="3" style="height:50px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="20"> </td>
		<td style="width:618px; padding-bottom:8px;">
			<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">З повагою,<br>компанія «ВДГБ СОФТ».</span>
			#VDGB_CONTACTS_HTML#
		</td>
		<td width="20"> </td>
	</tr>
</table>

				</td>
			</tr>
		</table>

<table width="660" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#ffffff;" border="0">
	<tr>
		<td style="width:658px; padding:9px 0 5px 0;">
			<span style="font-size:12px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.5;">
				<a href="#UNSUBSCRIBE_URL#" target="_blank" style="color:#999999; text-decoration:underline;">відписатися від розсилки</a>
';

$MESS["TSZH_ADD_NEWS_NOTICE_TITLE"] = "Повідомлення із пропозицією опублікувати новину";
$MESS["TSZH_ADD_NEWS_NOTICE_TEXT"] = "#HEADER_SENDER# - Значення в заголовку Sender (службове поле)
#EMAIL_FROM# - E-mail відправника листа
#EMAIL_TO# – E-mail одержувача
#ORG_NAME# – Найменування організації
#~ORG_NAME# – Найменування організації, що не перетворене функцією htmlspecialchars()
#ORG_SITE_NAME# – Найменування сайту організації
#ORG_URL# – URL сайту організації
#ADD_NEWS_ITEM_URL# – URL додавання новини
#VDGB_CONTACTS_HTML# – HTML таблиці контактів «ВДГБ Софт»
#UNSUBSCRIBE_URL# – URL відписки від розсилки
--------------------------------------------------------
";
$MESS["TSZH_ADD_NEWS_NOTICE_SUBJECT"] = "#~ORG_NAME#: ви давно не ділилися новинами з вашими споживачами послуг ЖКГ";
$MESS["TSZH_ADD_NEWS_NOTICE_MESSAGE"] = '
			<span style="line-height:1.13;">#ORG_NAME#:<br>опублікуйте новина на сайті.</span>
		</td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td width="20"> 
		</td>
		<td style="width:618px;">
			<table width="618" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0">
				<tr>
					<td style="width:618px;">
						<span style="font-size:18px; color:#333333; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif;">
							Добрий день.
						</span>
					</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Нагадування керівництву #ORG_NAME#.<br>
							Ви давно не публікували новин на вашому сайті <a href="#ORG_URL#" target="_blank" style="color:#ff6c00;">#ORG_SITE_NAME#</a>.
						</span>
					</td>
				</tr>
				<tr height="14">
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Мешканці будинків, які(ий) обслуговує ваша організація, часто заходять на ваш сайт <a href="#ORG_URL#" target="_blank" style="color:#ff6c00;">#ORG_SITE_NAME#</a>.
							Найчастіше їх цікавлять різні новини та оголошення, що стосуються їхніх будинків, особливо часто вони заходять в дні, 
							коли трапляються якісь аварії або інші надзвичайні ситуації.
						</span>
					</td>
				</tr>
				<tr height="14">
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Вчасно публікуючи новини про різних аварійних ситуаціях і терміни їх усунення, а також інші важливі новини, 
							ви значно знизите навантаження на вашу аварійно-диспетчерську службу і люди будуть менше дзвонити з однотипними питаннями.<br>
							Почніть публікувати новини про діяльність #ORG_NAME# і про будівлі, що знаходяться на вашому управлінні, прямо сьогодні.
						</span>
					</td>
				</tr>
				<tr height="17">
					<td>&nbsp;</td>
				</tr>
			</table>
		</td>

		<td width="20"> </td>
	</tr>
</table>

<table cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0">
	<tr>
		<td colspan="3" height="20">&nbsp;</td>
	</tr>
	<tr>
		<td style="width: 210px;"> </td>
		<td width="240" align="center">
			<table align="center" cellpadding="0" cellspacing="0" style="border-collapse:separate !important; border-radius:3px; margin:0; padding:0; background:#ff9900;" border="0">
				<tr>
					<td style="background-color:#ff9900; width:240px; height:36px; border-radius:3px;" align="center">
						<a href="#ADD_NEWS_ITEM_URL#" target="_blank" style="display:block; width:240px; height:36px; padding:0; margin:0; font-size:18px; line-height:1.7; color:#ffffff; text-decoration:none; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; border-radius:3px;">Опублікувати новина</a>
					</td>
				</tr>
			</table>               
		</td>
		<td style="width: 210px;"> </td>
	</tr>
	<tr>
		<td colspan="3" height="25">&nbsp;</td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td colspan="3" style="height:50px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="20"> </td>
		<td style="width:618px; padding-bottom:8px;">
			<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">З повагою,<br>компанія «ВДГБ Софт».</span>
			#VDGB_CONTACTS_HTML#
		</td>
		<td width="20"> </td>
	</tr>
</table>
				</td>
			</tr>
		</table>

<table width="660" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#ffffff;" border="0">
	<tr>
		<td style="width:658px; padding:9px 0 5px 0;">
			<span style="font-size:12px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.5;">
				<a href="#UNSUBSCRIBE_URL#" target="_blank" style="color:#999999; text-decoration:underline;">відписатися від розсилки</a>
';

$MESS["TSZH_NEWS_ITEM_TITLE"] = "Повідомлення власникам про опублікованій новини";
$MESS["TSZH_NEWS_ITEM_TEXT"] = "#HEADER_SENDER# – Значення в заголовку Sender (службове поле)
#EMAIL_FROM# – E-mail відправника листа
#EMAIL_TO# – E-Mail одержувача
#ACCOUNT_NAME# – ПІБ власника особового рахунку
#ORG_NAME# – Найменування організації
#~ORG_NAME# – Найменування організації, що не перетворене функцією htmlspecialchars()
#NEWS_ITEM_DATE_ACTIVE_FROM# – Дата публікації новини
#NEWS_ITEM_NAME# – Найменування новини
#NEWS_ITEM_PREVIEW_TEXT# – Текст анонса новини
#NEWS_ITEM_DETAIL_PAGE_URL# – URL детальної сторінки новини
#UNSUBSCRIBE_URL# – URL відписки від розсилки
--------------------------------------------------------
";
$MESS["TSZH_NEWS_ITEM_SUBJECT"] = "На сайті #~ORG_NAME# опублікована новина";
$MESS["TSZH_NEWS_ITEM_MESSAGE"] = '
			<span style="line-height:1.13;">На сайті #ORG_NAME#<br>опублікована новина.</span>
		</td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td width="20"> 
		</td>
		<td style="width:618px;">
			<table width="618" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0">
				<tr>
					<td style="width:618px;">
						<span style="font-size:18px; color:#333333; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif;">
							Добрий день.
						</span>
					</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Повідомляємо Вам, що на сайті #ORG_NAME# опублікована наступна новина.
						</span>
					</td>
				</tr>
				<tr height="14">
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							#NEWS_ITEM_DATE_ACTIVE_FROM#<br>
							<a href="#NEWS_ITEM_DETAIL_PAGE_URL#" target="_blank" style="color:#ff6c00;">#NEWS_ITEM_NAME#</a>
							<div style="margin-top:5px;">#NEWS_ITEM_PREVIEW_TEXT#</div>
						</span>
					</td>
				</tr>
				<tr height="17">
					<td>&nbsp;</td>
				</tr>
			</table>
		</td>

		<td width="20"> </td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td colspan="3" style="height:50px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="20"> </td>
		<td style="width:618px; padding-bottom:8px;">
			<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">З повагою,<br>#ORG_NAME#.</span>
		</td>
		<td width="20"> </td>
	</tr>
</table>
				</td>
			</tr>
		</table>

<table width="660" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#ffffff;" border="0">
	<tr>
		<td style="width:658px; padding:9px 0 5px 0;">
			<span style="font-size:12px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.5;">
				<a href="#UNSUBSCRIBE_URL#" target="_blank" style="color:#999999; text-decoration:underline;">відписатися від розсилки</a>
';

$MESS["TSZH_DEBTOR_NOTICE_TITLE"] = "Повідомлення про заборгованості";
$MESS["TSZH_DEBTOR_NOTICE_TEXT"] = "#HEADER_SENDER# - Значення в заголовку Sender (службове поле)
#EMAIL_FROM# - E-mail відправника листа
#EMAIL_TO# – E-mail одержувача
#ACCOUNT_NAME# – ПІБ власника особового рахунку
#DEBT_END# – Заборгованість на кінець періоду за вирахуванням нарахувань за період
#ORG_NAME# – Найменування організації
#~ORG_NAME# – Найменування організації, що не перетворене функцією htmlspecialchars()
#PERSONAL_URL# – URL особистого кабінету
#PAYMENT_URL# – URL сторінки оплати в особистому кабінеті
#UNSUBSCRIBE_URL# – URL відписки від розсилки
--------------------------------------------------------
";
$MESS["TSZH_DEBTOR_NOTICE_SUBJECT"] = "Погасите заборгованість по комунальних послугах перед #~ORG_NAME#";
$MESS["TSZH_DEBTOR_NOTICE_MESSAGE"] = '
			<span style="line-height:1.13;">Погасите заборгованість<br>по комунальних послугах<br>перед #ORG_NAME#.</span>
		</td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td width="20"> 
		</td>
		<td style="width:618px;">
			<table width="618" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0">
				<tr>
					<td style="width:618px;">
						<span style="font-size:18px; color:#333333; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif;">
							Шановний(а) #ACCOUNT_NAME#.
						</span>
					</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							У вас є заборгованість по комунальних послугах у розмірі <b>#DEBT_END#</b> перед #ORG_NAME#.
						</span>
					</td>
				</tr>
				<tr height="14">
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Ви можете детально ознайомитися з квитанцією за комунальні послуги у своєму <a href="#PERSONAL_URL#" target="_blank" style="color:#ff6c00;">особистому кабінеті</a>,
							а також побачити історію розрахунків за минулі періоди.<br>
							Рекомендуємо вам оперативно сплатити сформувалася заборгованість.
						</span>
					</td>
				</tr>
				<tr height="17">
					<td>&nbsp;</td>
				</tr>
			</table>
		</td>

		<td width="20"> </td>
	</tr>
</table>

<?if (strlen($arParams["PAYMENT_URL"])):?>
<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td width="20"> </td>
		<td style="width:618px;">
			<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">Ви можете оплатити прямо на сайті #ORG_NAME# у вашому особистому кабінеті.</span>
		</td>
		<td width="20"> </td>
	</tr>
</table>

<table cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0">
	<tr>
		<td colspan="3" height="20">&nbsp;</td>
	</tr>
	<tr>
		<td style="width: 255px;"> </td>
		<td width="150" align="center">
			<table align="center" cellpadding="0" cellspacing="0" style="border-collapse:separate !important; border-radius:3px; margin:0; padding:0; background:#ff9900;" border="0">
				<tr>
					<td style="background-color:#ff9900; width:150px; height:36px; border-radius:3px;" align="center">
						<a href="#PAYMENT_URL#" target="_blank" style="display:block; width:150px; height:36px; padding:0; margin:0; font-size:18px; line-height:1.7; color:#ffffff; text-decoration:none; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; border-radius:3px;">Оплатити</a>
					</td>
				</tr>
			</table>               
		</td>
		<td style="width: 255px;"> </td>
	</tr>
	<tr>
		<td colspan="3" height="25">&nbsp;</td>
	</tr>
</table>
<?endif?>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td colspan="3" style="height:50px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="20"> </td>
		<td style="width:618px; padding-bottom:8px;">
			<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">З повагою,<br>#ORG_NAME#.</span>
		</td>
		<td width="20"> </td>
	</tr>
</table>

				</td>
			</tr>
		</table>

<table width="660" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#ffffff;" border="0">
	<tr>
		<td style="width:658px; padding:9px 0 5px 0;">
			<span style="font-size:12px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.5;">
				<a href="#UNSUBSCRIBE_URL#" target="_blank" style="color:#999999; text-decoration:underline;">відписатися від розсилки</a>
';

$MESS["TSZH_METERS_VALUES_NEED_NOTICE_TITLE"] = "Нагадування про необхідність введення показань лічильників";
$MESS["TSZH_METERS_VALUES_NEED_NOTICE_TEXT"] = "#HEADER_SENDER# - Значення в заголовку Sender (службове поле)
#EMAIL_FROM# - E-mail відправника листа
#EMAIL_TO# – E-mail одержувача
#PERIOD_NAME# – Період повідомлення
#ORG_NAME# – Найменування організації
#~ORG_NAME# – Найменування організації, що не перетворене функцією htmlspecialchars()
#METERS_URL# – URL сторінки введення показань лічильників особистого кабінету
#UNSUBSCRIBE_URL# – URL відписки від розсилки
--------------------------------------------------------
";
$MESS["TSZH_METERS_VALUES_NEED_NOTICE_SUBJECT"] = "#~ORG_NAME#: вкажіть показання лічильників по вашій квартирі";
$MESS["TSZH_METERS_VALUES_NEED_NOTICE_MESSAGE"] = '			
			<span style="line-height:1.13;">Вкажіть показання приладів обліку<br>на сайті #ORG_NAME#.</span>
		</td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td width="20"> 
		</td>
		<td style="width:618px;">
			<table width="618" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0">
				<tr>
					<td style="width:618px;">
						<span style="font-size:18px; color:#333333; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif;">
							Добрий день.
						</span>
					</td>
				</tr>
				<tr>
					<td>
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Нагадуємо, що Вам необхідно вказати актуальні показання лічильників для правильного нарахування квартплати.
						</span>
					</td>
				</tr>
				<tr height="17">
					<td>&nbsp;</td>
				</tr>
			</table>
		</td>

		<td width="20"> </td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td width="20"> </td>
		<td style="width:618px;">
			<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">Ви можете ввести свідчення прямо на сайті #ORG_NAME# у вашому особистому кабінеті.</span>
		</td>
		<td width="20"> </td>
	</tr>
</table>

<table cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0">
	<tr>
		<td colspan="3" height="20">&nbsp;</td>
	</tr>
	<tr>
		<td style="width: 225px;"> </td>
		<td width="210" align="center">
			<table align="center" cellpadding="0" cellspacing="0" style="border-collapse:separate !important; border-radius:3px; margin:0; padding:0; background:#ff9900;" border="0">
				<tr>
					<td style="background-color:#ff9900; width:210px; height:36px; border-radius:3px;" align="center">
						<a href="#METERS_URL#" target="_blank" style="display:block; width:210px; height:36px; padding:0; margin:0; font-size:18px; line-height:1.7; color:#ffffff; text-decoration:none; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; border-radius:3px;">Ввести свідчення</a>
					</td>
				</tr>
			</table>               
		</td>
		<td style="width: 225px;"> </td>
	</tr>
	<tr>
		<td colspan="3" height="25">&nbsp;</td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td colspan="3" style="height:50px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="20"> </td>
		<td style="width:618px; padding-bottom:8px;">
			<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">З повагою,<br>#ORG_NAME#.</span>
		</td>
		<td width="20"> </td>
	</tr>
</table>

				</td>
			</tr>
		</table>

<table width="660" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#ffffff;" border="0">
	<tr>
		<td style="width:658px; padding:9px 0 5px 0;">
			<span style="font-size:12px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.5;">
				<a href="#UNSUBSCRIBE_URL#" target="_blank" style="color:#999999; text-decoration:underline;">відписатися від розсилки</a>
';

$MESS["TSZH_METER_VERIFICATION_NOTICE_TITLE"] = "Нагадування про дату повірки лічильників";
$MESS["TSZH_METER_VERIFICATION_NOTICE_TEXT"] = "#HEADER_SENDER# - Значення в заголовку Sender (службове поле)
#EMAIL_FROM# - E-mail відправника листа
#EMAIL_TO# – E-mail одержувача
#VERIFICATION_DATE# – Дата повірки
#METERS# – Серіалізовані масив лічильників, що підлягають повірці
#ORG_NAME# – Найменування організації
#~ORG_NAME# – Найменування організації, не перетворено функцією htmlspecialchars()
#ORG_ADDRESS# – Адреса організації
#ORG_PHONE# – Телефон організації
#UNSUBSCRIBE_URL# – URL відписки від розсилки
--------------------------------------------------------
";
$MESS["TSZH_METER_VERIFICATION_NOTICE_SUBJECT"] = "#~ORG_NAME#: #VERIFICATION_DATE# — дата повірки лічильників";
$MESS["TSZH_METER_VERIFICATION_NOTICE_MESSAGE"] = '
			<span style="line-height:1.13;">Наближається дата повірки<br>Ваших лічильників.</span>
		</td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td width="20"> 
		</td>
		<td style="width:618px;">
			<table width="618" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0">
				<tr>
					<td colspan="2" style="width:618px;">
						<span style="font-size:18px; color:#333333; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif;">
							Добрий день.
						</span>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Закінчується міжповірочний термін у наступних лічильників:
						</span>
					</td>
				</tr>
				<tr height="7">
					<td colspan="2">&nbsp;</td>
				</tr>
				<?$arMeters = unserialize($arParams["METERS"]);
				foreach ($arMeters as $meterId => $arMeter):?>
					<tr>
						<td style="width:17px; height:18px; vertical-align:top; padding-left:10px;"><img src="/bitrix/templates/citrus_tszh_subscribe/images/9.png" height="18" width="17" alt=""></td>
						<td height="18" style="padding-left:10px; vertical-align:top;" >
							<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
								<?=$arMeter["NAME"]?> <?=strlen($arMeter["SERVICE_NAME"]) ? "(" . $arMeter["SERVICE_NAME"] . ")" : ""?>
							</span>
						</td>
					</tr>
					<tr>
						<td colspan="2" height="15">&nbsp;</td>
					</tr>
				<?endforeach?>
				<tr>
					<td colspan="2">
						<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">Необхідно їх повірити або замінити.<br>Зверніться в #ORG_NAME#:</span>
						<?if (strlen($arParams["ORG_ADDRESS"]) || strlen($arParams["ORG_PHONE"])):?>
							<table border="0" style="margin-top:8px; font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
								<?if (strlen($arParams["ORG_ADDRESS"])):?>
									<tr>
										<td><b>Адреса:</b></td>
										<td>#ORG_ADDRESS#</td>
									</tr>
								<?endif;
								if (strlen($arParams["ORG_PHONE"])):?>
									<tr>
										<td><b>Телефон:</b></td>
										<td>#ORG_PHONE#</td>
									</tr>
								<?endif?>
							</table>
						<?endif?>
					</td>
				</tr>
				<tr>
					<td colspan="2" height="15">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">
						<p style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">
							Повірка засобів вимірювань — сукупність операцій, що виконуються з метою підтвердження відповідності засобів вимірювань метрологічним вимогам.
						</p>
					</td>
				</tr>
			</table>
		</td>

		<td width="20"> </td>
	</tr>
</table>

<table width="658" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#fdfdfd;" border="0" >
	<tr>
		<td colspan="3" style="height:50px;">&nbsp;</td>
	</tr>
	<tr>
		<td width="20"> </td>
		<td style="width:618px; padding-bottom:8px;">
			<span style="font-size:14px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.3;">З повагою,<br>#ORG_NAME#.</span>
		</td>
		<td width="20"> </td>
	</tr>
</table>

				</td>
			</tr>
		</table>

<table width="660" align="center" cellpadding="0" cellspacing="0" style="border-collapse:collapse; margin:0; padding:0; background:#ffffff;" border="0">
	<tr>
		<td style="width:658px; padding:9px 0 5px 0;">
			<span style="font-size:12px; font-family:\'Segoe UI\',Arial,Helvetica,sans-serif; line-height:1.5;">
				<a href="#UNSUBSCRIBE_URL#" target="_blank" style="color:#999999; text-decoration:underline;">відписатися від розсилки</a>
';

$MESS["TSZH_CONFIRM_EMAIL_TITLE"] = "Підтвердження зміни e-mail'а";
$MESS["TSZH_CONFIRM_EMAIL_TEXT"] = "#USER_ID# - ID користувача
#USER_EMAIL# - E-mail користувача
#HASH# - Контрольний рядок для зміни e-mail'а
";
$MESS["TSZH_CONFIRM_EMAIL_SUBJECT"] = "#SITE_NAME#: Підтвердження зміни e-mail'а";
$MESS["TSZH_CONFIRM_EMAIL_MESSAGE"] = "Інформаційне повідомлення сайту #SITE_NAME#
------------------------------------------

Будь ласка, підтвердіть Ваш e-mail #USER_EMAIL#, перейшовши за наступним посиланням:
http://#SERVER_NAME#/?tszh_confirm_email=y&user_id=#USER_ID#&hash=#HASH#

Повідомлення згенеровано автоматично.
";

$MESS["TSZH_FEEDBACK_FORM_TITLE"] = "Відправлення повідомлення через форму зворотного зв'язку з ТВЖ";
$MESS["TSZH_FEEDBACK_FORM_TEXT"] = "#HEADER_SENDER# - Значення в заголовку Sender (службове поле)
#AUTHOR# - Автор повідомлення
#AUTHOR_EMAIL# - Email автора повідомлення
#TEXT# - Текст повідомлення
#EMAIL_FROM# - Email відправника листа
#EMAIL_TO# - Email одержувача листа
";
$MESS["TSZH_FEEDBACK_FORM_SUBJECT"] = "#SITE_NAME#: Повідомлення з форми зворотного зв'язку з ТВЖ";
$MESS["TSZH_FEEDBACK_FORM_MESSAGE"] = "Інформаційне повідомлення сайту #SITE_NAME#
------------------------------------------

Вам було відправлено повідомлення через форму зворотного зв'язку з ТВЖ

Автор: #AUTHOR#
E-mail автора: #AUTHOR_EMAIL#

Текст повідомлення:
#TEXT#

Повідомлення згенеровано автоматично.
";
