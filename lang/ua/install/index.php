<?
$MESS["TSZH_MEMBERS_GROUP_NAME"] = "Власники особових рахунків";
$MESS["C_MODULE_RIGHT_D"] = "[D] закритий ";
$MESS["C_MODULE_RIGHT_E"] = "[E] вивантаження";
$MESS["C_MODULE_RIGHT_R"] = "[R] перегляд всіх даних модуля";
$MESS["C_MODULE_RIGHT_U"] = "[U] імпорт";
$MESS["C_MODULE_RIGHT_W"] = "[W] запис";
$MESS["C_MODULE_NAME"] = "1С: Сайт ЖКГ";
$MESS["C_MODULE_DESCRIPTION"] = "базовий модуль";
$MESS["C_MODULE_PARTNER_NAME"] = "ВДГБ СОФТ";
$MESS["C_MODULE_PARTNER_URI"] = "http://www.vdgb-soft.ru/";
$MESS["CITRUS_TSZH_1C_EXCHANGE_GROUP_NAME"] = "Службова група для інтеграція з 1С";
$MESS["TSZH_INST_TITLE"] = "Установка модуля «1С: Сайт ЖКГ»";
$MESS["TSZH_UNINST_TITLE"] = "Видалення модуля «1С: Сайт ЖКГ»";
$MESS["TSZH_ERROR_INSTALLING_EVENT_TYPES"] = "Сталася помилка при установці типів поштових подій .";
$MESS["TSZH_ERROR_INSTALLING_EVENT_MESSAGES"] = "Сталася помилка при установці поштових шаблонів.";
$MESS["TSZH_ERROR_EDITION_NOT_FOUND"] = "Не визначена редакція продукту 1С: Сайт ЖКГ";
?>