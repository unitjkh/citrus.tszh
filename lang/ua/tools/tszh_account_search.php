<?
$MESS["TSZH_FLT_ID"] = "ID";
$MESS["TSZH_FLT_ACCOUNT_ID"] = "ID особового рахунку";
$MESS['TSZH_FLT_USER_ID'] = "Власник";
$MESS["TSZH_FLT_XML_ID"] = "Номер особового рахунку";
$MESS["TSZH_FLT_TSZH_ID"] = "ТСЖ";
$MESS["TSZH_FLT_NAME"] = "ПІБ";
$MESS["TSZH_FLT_CITY"] = "Місто";
$MESS["TSZH_FLT_DISTRICT"] = "Район";
$MESS["TSZH_FLT_REGION"] = "Регіон";
$MESS["TSZH_FLT_SETTLEMENT"] = "Населений пункт";
$MESS["TSZH_FLT_STREET"] = "Вулиця";
$MESS["TSZH_FLT_HOUSE"] = "Будинок";
$MESS["TSZH_FLT_FLAT"] = "Квартира";
$MESS["TSZH_FLT_AREA"] = "Загальна площа";
$MESS["TSZH_FLT_LIVING_AREA"] = "Житлова площа";
$MESS["TSZH_FLT_PEOPLE"] = "Кількість мешканців";
$MESS["TSZH_F_ALL"] = "(все)";
$MESS["TSZH_PAGE_TITLE"] = "Вибір особового рахунку";
$MESS["TSZH_FLT_SEARCH"] = "Пошук";
$MESS["TSZH_FLT_SEARCH_TITLE"] = "Пошук особового рахунку";
$MESS["TSZH_SELECT"] = "Вибрати";
$MESS["TSZH_EDIT_ACCOUNT"] = "Перейти до редагування особового рахунку ";
?>