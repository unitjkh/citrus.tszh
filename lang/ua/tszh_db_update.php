<?
$MESS["T_TAB1"] = "Оновлення бази дані";
$MESS["T_TAB1_TITLE"] = "Оновлення стуктури бази даних";
$MESS["CITRUS_TSZH_DB_UPDATE"] = "Оновлення бази дані";
$MESS["CITRUS_TSZH_ERROR_LIST"] = "Відбулися сдели помилки";
$MESS["CITRUS_TSZH_DB_UPDATE_NOTICE_TEXT"] = "<span class=\"required\"> Увага! </ span > Перед початком оновлення рекомендується зробити резервну копію.";
$MESS["CITRUS_TSZH_START_UPDATE"] = "оновити";
$MESS["CITRUS_UPDATE_DB_PROGRESS"] = "Йде оновлення бази даних ...";
$MESS["CITRUS_UPDATE_DB_DONE"] = "завершено";
$MESS["CITRUS_UPDATE_DB_DONE_TEXT"] = "Оновлення структури бази даних успішно завершено";
$MESS["CITRUS_UPDATE_DB_NOT_REQUIRED"] = "Оновлення структури бази даних не потрібно";
$MESS["CITRUS_UPDATE_GOTO_MAIN"] = "Перейти на сторінку модуля";
?>