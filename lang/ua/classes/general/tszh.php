<?
$MESS["TSZH_MONTH_1"] = "Січень";
$MESS["TSZH_MONTH_2"] = "Лютий";
$MESS["TSZH_MONTH_3"] = "Березень";
$MESS["TSZH_MONTH_4"] = "Квітень";
$MESS["TSZH_MONTH_5"] = "Травень";
$MESS["TSZH_MONTH_6"] = "Червень";
$MESS["TSZH_MONTH_7"] = "Липень";
$MESS["TSZH_MONTH_8"] = "Серпень";
$MESS["TSZH_MONTH_9"] = "Вересень";
$MESS["TSZH_MONTH_10"] = "Жовтень";
$MESS["TSZH_MONTH_11"] = "Листопад";
$MESS["TSZH_MONTH_12"] = "Грудень";
$MESS["TSZH_ERROR_NO_SITE_ID"] = "Не вказана прив'язка до сайту ";
$MESS["TSZH_ERROR_WRONG_SITE_ID"] = "Сайт не існує";
$MESS["TSZH_ERROR_NO_NAME"] = "Не вказано назву ТСЖ ";
$MESS["TSZH_ERROR_NO_TSZH_ID"] = "Не вказана прив'язка до ТСЖ";
$MESS["TSZH_ERROR_WRONG_SUBSCRIBE_DOLG_DATE"] = "Невірно вказана дата відправки повідомлень про заборгованість (число місяця)";
$MESS["TSZH_ERROR_WRONG_SUBSCRIBE_METERS_DATE"] = "Невірно вказана дата відправки повідомлень про необхідність введення показань лічильників (число місяця)";
$MESS["TSZH_ERROR_WRONG_SUBSCRIBE_RECEIPT_DATE"] = "Невірно вказана дата відправки квитанцій на оплату послуг ЖКГ (число місяця)";
$MESS["TSZH_ERROR_WRONG_METER_VALUES_START_DATE"] = "Невірно вказано дату початку періоду, в якому дозволений введення показань лічильників (число місяця)";
$MESS["TSZH_ERROR_WRONG_METER_VALUES_END_DATE"] = "Невірно вказана дата закінчення періоду, в якому дозволений введення показань лічильників (число місяця)";
$MESS["CITRUS_TSZH_ERROR_INCORRECT_RSCH"] = "Розрахунковий рахунок повинен містити 10-14 цифр";
$MESS["CITRUS_TSZH_ERROR_INCORRECT_BIK"] = "МФО повинен містити 6 цифр";
$MESS["CITRUS_TSZH_F_BIK"] = "МФО";
$MESS["TSZH_ERROR_WRONG_EMAIL"] = "E - Mail вказано не вірно";
$MESS["CITRUS_TSZH_ERORR_NOTIFY"] = "Значення деяких полів## ІМ'Я вказано невірно . Можливі проблеми з проведенням платежів . <a href=\"/bitrix/admin/tszh_edit.php?ID=#ID#&lang=ru\"> Виправити </a>";
$MESS["CITRUS_TSZH_ERROR_EMPTY_FIELD"] = "Не заповнено поле &laquo;#FIELD#&raquo;.";
$MESS["CITRUS_TSZH_ERROR_DEFAULT_FIELD"] = "У полі &laquo;#FIELD#&raquo; зазначено значення за замовчуванням.";
$MESS["CITRUS_TSZH_ERROR_INCORRECT_INN"] = "ІПН повинен містити 10 цифр";
$MESS["CITRUS_TSZH_ERROR_INCORRECT_KPP"] = "КПП повинен містити 9 цифр";
$MESS["CITRUS_TSZH_ERROR_INCORRECT_INN_CHECK"] = "ІПН вказано не вірно";
$MESS["CITRUS_TSZH_ERROR_INCORRECT_RSCH_CHECK"] = "Розрахунковий рахунок та / або МФО вказано невірно , перевірте правильність значень у цих полях";
$MESS["CITRUS_TSZH_ERROR_EMAIL_SUBJECT"] = "Будь ласка , вкажіть реквізити вашої організації ЖКГ на сайті";
$MESS["CITRUS_TSZH_ERROR_EMAIL_BODY"] = " Здравствуйте.

Реквізити однієї або декількох організацій на сайті вказані не вірно :

";
$MESS["CITRUS_TSZH_F_NAME"] = "Назва";
$MESS["CITRUS_TSZH_F_INN"] = "ІПН";
$MESS["CITRUS_TSZH_F_KPP"] = "КПП";
$MESS["CITRUS_TSZH_F_RSCH"] = "розрахунковий рахунок";
$MESS["CITRUS_TSZH_F_BANK"] = "банк";
$MESS["CITRUS_TSZH_F_EMAIL"] = "E-mail організації для відображення в контактах";
?>