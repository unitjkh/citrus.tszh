<?
$MESS["TSZH_ERROR_INSTALLMENTS_NO_ACCOUNT_PERIOD_ID"] = "Не вказана квитанція для розстрочки";
$MESS["TSZH_ERROR_INSTALLMENTS_NO_SERVICE"] = "Не вказана послуга розстрочки";
$MESS["TSZH_ERROR_INSTALLMENTS_NO_SUMM_PAYED"] = "Не вказана сума оплати за період розстрочки";
$MESS["TSZH_ERROR_INSTALLMENTS_NO_SUMM_PREV_PAYED"] = "Не вказана сума оплати за попередні періоди розстрочки";
$MESS["TSZH_ERROR_INSTALLMENTS_NO_PERCENT"] = "Не вказаний відсоток розстрочки";
$MESS["TSZH_ERROR_INSTALLMENTS_NO_SUMM_RATED"] = "Не вказана сума відсотків розстрочки";
$MESS["TSZH_ERROR_INSTALLMENTS_NO_SUMM2PAY"] = "Не вказана сума до оплати за розстрочку";
