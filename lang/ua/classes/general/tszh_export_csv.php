<?
$MESS["TSZH_ERROR_EXPORT_NO_DATA"] = "Немає даних для експорту.";
$MESS["TSZH_EXPORT_FIO"] = "ПІБ";
$MESS["TSZH_EXPORT_ACCOUNT"] = "Особовий рахунок";
$MESS["TSZH_EXPORT_ADDRESS"] = "Адреса";
$MESS["TSZH_EXPORT_TARIFF"] = "(тариф #N#)";
$MESS["TSZH_EXPORT_MNAME"] = "Назва лічильника";
$MESS["TSZH_EXPORT_MCODE"] = "Код лічильника";
$MESS["TSZH_EXPORT_MVALUES"] = "Кількість тарифів";
$MESS["TSZH_EXPORT_MDATE"] = "Дата";
$MESS["TSZH_EXPORT_MVAL1"] = "Показання (1)";
$MESS["TSZH_EXPORT_MVAL2"] = "Показання (2)";
$MESS["TSZH_EXPORT_MVAL3"] = "Показання (3)";
?>
