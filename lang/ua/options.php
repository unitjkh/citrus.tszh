<?
$MESS["MAIN_TAB_GROUPS"] = "Доступ";
$MESS["MAIN_TAB_TITLE_GROUPS"] = "Рівень доступа до модуля";
$MESS["MAIN_TAB_1C_EXCHANGE"] = "Інтеграція з 1С";
$MESS["TSZH_OPT_SYSTEM_SETTINGS"] = "Системні налаштування";
$MESS["TSZH_OPT_USER_GROUP"] = "Група власників особових рахунків";
$MESS["TSZH_OPT_METERS_BLOCK_EDIT"] = "Заборонити введення показань лічильників ";
$MESS["TSZH_OPT_AUTOSEND_MODIFIED_RECEIPTS"] = "Автоматично розсилати змінені квитанції";
$MESS["TSZH_OPT_IMPORT"] = "Імпорт даних";
$MESS["TSZH_OPT_IMPORT_TITLE"] = "Імпорт особових рахунків, нарахованих послуг та показників лічильників";
$MESS["TSZH_OPT_EXPORT"] = "Експорт даних";
$MESS["TSZH_OPT_EXPORT_TITLE"] = "Експорт показників лічильників";
$MESS["TI_IMPORT_TITLE"] = "Налаштування імпорту особових рахунків";
$MESS["TI_COMMON_TITLE"] = "Загальні налаштування";
$MESS["TI_ACTION"] = "Дії з особовими рахунками і лічильниками, <br />яких немає в файлі";
$MESS["TI_ACTION_NONE"] = "нічого";
$MESS["TI_ACTION_DEACTIVATE"] = "деактивувати";
$MESS["TI_ACTION_DELETE"] = "Видалити";
$MESS["TI_INTERVAL"] = "Тривалість кроку в секундах <br />(0 - виконувати імпорт за один крок) ";
$MESS["TI_UPDATE_MODE"] = "Оновлення";
$MESS["TI_UPDATE_MODE_NO"] = "створити новий період";
$MESS["TI_UPDATE_MODE_TITLE"] = "оновити раніше завантаженний період";
$MESS["TI_UPDATE_MODE_ONLY_DEBT"] = "не завантажувати нарахування";
$MESS["TI_USERS"] = "Користувачі";
$MESS["TI_DONT_UPDATE_USERS_TITLE"] = "не змінювати E-Mail і пароль існуючих користувачів";
$MESS["TI_CREATE_USERS"] = "Автоматично створювати користувачів для імпортування особових рахунків";
$MESS["TI_NOTE_1"] = "У випадку, якщо на сайті вже є період, який міститься у файлі, цей період буде оновлено замість створення нового. ";
$MESS["TI_NOTE_2"] = "Будуть завантажені лише дані по заборгованості та оплати. Нарахування, послуги та лічильники завантажуватися не будуть. ";
$MESS["TI_NOTE_3"] = "E-Mail і пароль вже існуючих користувачів особових рахунків не будуть змінені на зазначені у файлі імпорту ";
$MESS["TI_FILESIZE"] = "Розмір одноразово завантажується частини файлу (у байтах): ";
$MESS["TI_DEPERSONALIZE"] = "Знеособлювати персональні дані";

$MESS["CITRUS_TSZH_SUBSCRIBE_GROUP"] = "Налаштування розсилок";
$MESS["CITRUS_TSZH_SUBSCRIBE_MAX_EMAILS_PER_HIT"] = "Максимальна кількість листів для розсилки за один хіт";
$MESS["CITRUS_TSZH_SUBSCRIBE_POSTING_INTERVAL"] = "Інтервал в секундах для покрокових розсилок";
?>