<?
$MESS["citrus_tszh_TAB"] = "Особовий рахунок";
$MESS["citrus_tszh_ACCOUNT_ID"] = "ID особового рахунку";
$MESS["citrus_tszh_NO_ACCOUNT"] = "Даний користувач не прив'язаний до особового рахунку ";
$MESS["citrus_tszh_CREATE_ACCOUNT"] = "Створити особовий рахунок ";
$MESS["citrus_tszh_CREATE_ACCOUNT_TITLE"] = "Створити особовий рахунок ЖКГ, ТСЖ для користувача ";
$MESS["citrus_tszh_EDIT_ACCOUNT"] = "Редагувати особовий рахунок ";
$MESS["citrus_tszh_EDIT_ACCOUNT_TITLE"] = "Перейти до редагування особового рахунку ";
$MESS["citrus_tszh_XML_ID"] = "Номер особового рахунку";
$MESS["citrus_tszh_NAME"] = "Ім'я користувача";
$MESS["citrus_tszh_ADDRESS"] = "Адреса";
$MESS["citrus_tszh_COMMON_AREA"] = "Загальна площа";
$MESS["citrus_tszh_LIVING_AREA"] = "Житлова площа";
$MESS["citrus_tszh_PEOPLE"] = "Кількість мешканців";
$MESS["citrus_tszh_TSZH_ID"] = "ТСЖ";
$MESS["citrus_tszh_VIEW_TSZH"] = "Перегляд ТСЖ";
?>