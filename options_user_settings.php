<?
IncludeModuleLangFile(__FILE__);
//ClearVars("str_tszh_");
$ID = IntVal($ID);
if ($ID > 0 && CModule::IncludeModule("citrus.tszh"))
{
	$rsAccount = CTszhAccount::GetList(Array(), Array("USER_ID" => $ID), false, false, Array("*"));
	while ($arAccount = $rsAccount->GetNext())
	{

		$arFields = Array(
			"TSZH_ID" => Array(
				'title' => GetMessage("citrus_tszh_TSZH_ID"),
				'value' => "[<a href=\"/bitrix/admin/tszh_edit.php?ID=" . $arAccount["TSZH_ID"] . "&amp;lang=" . LANG . "\" title=\"" . GetMessage("citrus_tszh_VIEW_TSZH") . "\">" . $arAccount["TSZH_ID"] . "</a>] " . $arAccount["TSZH_NAME"],
			),
			"XML_ID" => GetMessage("citrus_tszh_XML_ID"),
			"NAME" => GetMessage("citrus_tszh_NAME"),
			"ADDRESS" => Array(
				'title' => GetMessage("citrus_tszh_ADDRESS"),
				'value' => CTszhAccount::GetFullAddress($arAccount),
			),
			"AREA" => GetMessage("citrus_tszh_COMMON_AREA"),
			"LIVING_AREA" => GetMessage("citrus_tszh_LIVING_AREA"),
			"PEOPLE" => GetMessage("citrus_tszh_PEOPLE"),
			"EDIT_LINK" => Array(
				'title' => '',
				'value' => '<a href="/bitrix/admin/tszh_account_edit.php?ID=' . $arAccount["ID"] . '&amp;lang=' . LANG . '">' . GetMessage("citrus_tszh_EDIT_ACCOUNT_TITLE") . '</a><br /><br />'
			)
		);
		?>
			<tr valign="top" class="heading">
				<td colspan="2"><?=("[<a href=\"/bitrix/admin/tszh_account_edit.php?ID=" . $arAccount["ID"] . "&amp;lang=" . LANG . "\" title=\"" . GetMessage("citrus_tszh_EDIT_ACCOUNT") . "\">" . $arAccount["ID"] . "</a>] " . $arAccount["NAME"])?></td>
			</tr>
		<?
		foreach ($arFields as $code => $field)
		{
			if (!is_array($field))
			{
				$field = Array(
					'title' => $field,
					'value' => htmlspecialcharsbx($arAccount[$code]),
				);
			}
			?>
			<tr valign="top">
				<td class="field-name"><?=(strlen($field['title']) > 0 ? $field['title'] . ':' : '')?></td>
				<td width="60%"><?=$field['value']?></td>
			</tr>
			<?
		}
	}
	if ($rsAccount->SelectedRowsCount() == 0)
	{

		?>
		<tr valign="top">
			<td class="field-name"><?=GetMessage("citrus_tszh_NO_ACCOUNT")?></td>
			<td width="60%"><a href="/bitrix/admin/tszh_account_edit.php?lang=<?=LANG?>&amp;for_user=<?=$ID?>" title="<?=GetMessage("citrus_tszh_CREATE_ACCOUNT_TITLE")?>"><?=GetMessage("citrus_tszh_CREATE_ACCOUNT")?></a></td>
		</tr>
		<?

	}
}
?>